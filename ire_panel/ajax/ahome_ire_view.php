<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

include("../../config.php");
$conn = db_connect1();
session_start();

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$flag = $_SESSION['flag'];

$startdate = date('Y-m-d', strtotime($_GET['startdate']));
$enddate = date('Y-m-d', strtotime($_GET['enddate']));
$city = $_GET['city'];
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;

function run_query1($conn, $query)
{
	$bookings_count = 0;
	$followups_count = 0;
	$cancelled_count = 0;

	$query_result = mysqli_query($conn, $query);

	while ($query_rows = mysqli_fetch_object($query_result)) {
		$status = $query_rows->status;
		if ($status == '0') {
			$bookings_count = $bookings_count + 1;
		} elseif ($status == '2') {
			$followups_count = $followups_count + 1;
		} elseif ($status == '3') {
			$cancelled_count = $cancelled_count + 1;
		}
	}
	$rtn['new'] = $bookings_count;
	$rtn['followup'] = $followups_count;
	$rtn['cancel'] = $cancelled_count;
	return $rtn;
}

function run_query2($conn, $query)
{
	$bookings_count = 0;
	$followups_count = 0;
	$cancelled_count = 0;

	$query_result = mysqli_query($conn, $query);

	while ($query_rows = mysqli_fetch_object($query_result)) {
		$status = $query_rows->status;
		if ($status == '0') {
			$bookings_count = $bookings_count + 1;
		} elseif ($status == '2') {
			$followups_count = $followups_count + 1;
		} elseif ($status == '3') {
			$cancelled_count = $cancelled_count + 1;
		}
	}
	$rtn['new'] = $bookings_count;
	$rtn['followup'] = $followups_count;
	$rtn['cancel'] = $cancelled_count;
	return $rtn;
}

$cond = '';

$cond = $cond . ($city == 'all' ? "" : "AND city='$city'");

if ($flag == '1') {
	$select_sql = "SELECT * FROM ire_user_tbl where booking_id!='' and date(crm_update_time) between '$startdate' and '$enddate' {$cond}";

	$select_sql2 = "SELECT * FROM ire_user_tbl where booking_id!='' and date(prospect_date) between '$startdate' and '$enddate' {$cond}";

	$goaxle_count_query = "SELECT COUNT(booking_id) as goaxle_count from user_booking_tb where date(log) between '$startdate' and '$enddate' {$cond} and ire_flag='1' and booking_status='1' and axle_flag!='1' and flag!='1' and override_flag!='1'";

	$goaxle_count_response = mysqli_query($conn, $goaxle_count_query);

	$goaxle_count_row = mysqli_fetch_assoc($goaxle_count_response);

	$goaxle_count = $goaxle_count_row['goaxle_count'];

	$no_leads_arr = run_query1($conn, $select_sql);

	$no_leads_arr1 = run_query2($conn, $select_sql2);

} else {

	$select_sql = "SELECT * FROM ire_user_tbl where booking_id!='' and date(crm_update_time) between '$startdate' and '$enddate' {$cond} and crm_update_id='$crm_log_id'";

	$select_sql2 = "SELECT * FROM ire_user_tbl where booking_id!='' and date(prospect_date) between '$startdate' and '$enddate' {$cond} and crm_update_id='$crm_log_id'";

	$goaxle_count_query = "SELECT booking_id from user_booking_tb where date(log) between '$startdate' and '$enddate' {$cond} and ire_flag='1' and crm_update_id='$crm_log_id' and booking_status='1' and axle_flag!='1' and flag!='1' and override_flag!='1'";

	$goaxle_count_response = mysqli_query($conn, $goaxle_count_query);

	$goaxle_count = mysqli_num_rows($goaxle_count_response);

	$no_leads_arr = run_query1($conn, $select_sql);

	$no_leads_arr1 = run_query2($conn, $select_sql2);
}

$goaxle_counts = $goaxle_count;
$ire_lead_counts = $no_leads_arr['new'];
$followup_counts = $no_leads_arr1['followup'];
$cancel_counts = $no_leads_arr1['cancel'];

echo $data = json_encode(
	array(
		'leadsCount' => $ire_lead_counts,
		'followupsCount' => $followup_counts,
		'cancelledCount' => $cancel_counts,
		'ac' => $goaxle_counts,
	)
);
<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

include '../config.php';

$conn = db_connect1();

session_start();

$user_id = $_POST['user_id'];
$veh_id = $_POST['veh_id'];
$type = $_POST['type'];
$booking_id = $_POST['booking_id'];
$log = $_POST['log'];
$pid = $_POST['pid'];

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$user_flag = $_SESSION['flag'];
$cluster_admin = $_SESSION['cluster_admin'];

$today = date('Y-m-d H:i:s');

if (!isset($_POST)) {
	header("Location:logout.php");
}

if ($pid == "follow_up") {
	
	$status = $_POST['followup_status'];
	$comments = $_POST['followup_comments'];
	$followup_date = $_POST['followup_date'];
	$followup_date_text = $_POST['followup_date_text'];
	$followup_date = $_POST['followup_date'];
	$ire_id = $_POST['ire_id'];
	$user_id = $_POST['user_id'];
	$booking_id = $_POST['booking_id'];
	$warm_query = "UPDATE go_bumpr.ire_user_tbl SET status='2',prospect_date='$followup_date', prospect_date_type='$followup_date_text', reason='$comments',crm_update_id='$crm_log_id',crm_update_time='$today',crm_log_id='$crm_log_id' where id='$ire_id'";
	$result_warm = mysqli_query($conn, $warm_query) or die(mysqli_error($conn));
	
	$followup_prospects = 'FollowUp - ' . $user_id . ' - ' . $status;
	$query = "INSERT INTO admin_comments_tbl(user_id,crm_log_id,category,comments,Follow_up_date,flag,log,status,book_id) VALUES ('$user_id','$crm_log_id','$followup_prospects','$comments','0000-00-00','0','$today','IRE FollowUp','$booking_id')";
	$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
	header("Location:aleads_ire.php");
	
} elseif ($pid == "cancel") {
	$status = $_POST['cancel_status'];
	$comments = $_POST['cancel_comments'];
	$prospect_date = date('Y-m-d', strtotime($_POST['cancel_prospects_date']));
	$ire_id = $_POST['ire_id'];
	$user_id = $_POST['user_id'];
	$booking_id = $_POST['booking_id'];
	$cold_query = "UPDATE go_bumpr.ire_user_tbl SET status='3',prospect_date='$prospect_date',reason='$comments',crm_update_id='$crm_log_id',crm_update_time='$today',crm_log_id='$crm_log_id' where id='$ire_id'";
	$result_warm = mysqli_query($conn, $cold_query) or die(mysqli_error($conn));
	
	$cold_prospects = 'Cancel Prospects - ' . $user_id . ' - ' . $status;
	$query = "INSERT INTO admin_comments_tbl(user_id,crm_log_id,category,comments,Follow_up_date,flag,log,status,book_id) VALUES ('$user_id','$crm_log_id','$cold_prospects','$comments','0000-00-00','0','$today','Cold','$booking_id')";
	$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
	if ($_SESSION['flag'] == '1') {
		header("Location:aleads_ire.php");
	} else {
		header("Location:aleads_ire.php");
	}
	
} elseif ($pid == "new_booking") {
	
	$today = date('Y-m-d H:i:s');
	
	$user_id = $_REQUEST['user_id'];
	if (isset($_REQUEST['type'])) {
		$type = $_REQUEST['type'];
	}
	$booking_id = $_REQUEST['book_id'];
	$override_flag = $_POST['override_flag'];
	$crm_log_id = $_SESSION['crm_log_id'];
	$crm_name = $_SESSION['crm_name'];
	$flag = $_SESSION['flag'];
	$cluster_admin = $_SESSION['cluster_admin'];
	$ire_id = $_POST['ire_id'];
	$log = date('Y-m-d H:i:s');
	$mec_id = mysqli_real_escape_string($conn, $_POST['mechanic']);
	$locality = mysqli_real_escape_string($conn, $_POST['locatione']);
	$pickup_location_data=mysqli_real_escape_string($conn,$_POST['location_pickup']);

	$locality_arr = explode(", ",$pickup_location_data);
	$pickup_location  = $locality_arr[0];
	$city  = $locality_arr[1];
	//$amt=mysqli_real_escape_string($conn,$_POST['amounte']);
	$pickup_full_address = mysqli_real_escape_string($conn, $_POST['pickup_full_address']);
	
	$language = mysqli_real_escape_string($conn, $_POST['language']);
	$fuel_type = mysqli_real_escape_string($conn, $_POST['fuel_type']);
	$v_usage = mysqli_real_escape_string($conn, $_POST['v_usage']);
	$m_year = mysqli_real_escape_string($conn, $_POST['m_year']);
	$sms = mysqli_real_escape_string($conn, $_POST['sms']);
	
	
	// $sql_loc = "SELECT city FROM localities WHERE localities='$pickup_location'";
	// $res_loc = mysqli_query($conn, $sql_loc);
	// $row_loc = mysqli_fetch_array($res_loc);
	// $city = $row_loc['city'];
	
	$vehicle_type = mysqli_real_escape_string($conn, $_POST['veh_b']);
	$user_vech_id = mysqli_real_escape_string($conn, $_POST['veh_no']);
	$service_description = mysqli_real_escape_string($conn, $_POST['description']);
	
	$service_type = mysqli_real_escape_string($conn, $_POST['service_type']);
	$amt = mysqli_real_escape_string($conn, $_POST['amount']);
	$service_date = date('Y-m-d', strtotime(mysqli_real_escape_string($conn, $_POST['service_date'])));
	$next_service_date = date('Y-m-d', strtotime(mysqli_real_escape_string($conn, $_POST['next_service_date'])));
// $pick_up=mysqli_real_escape_string($conn,$_POST['pickup']);
	$pick_up = 0;

//get shop name
	$sql_mec = "SELECT shop_name FROM admin_mechanic_table WHERE mec_id='$mec_id' ";
	$res_mec = mysqli_query($conn, $sql_mec);
	$row_mec = mysqli_fetch_object($res_mec);
	$shop_name = $row_mec->shop_name;
	
	$sql_veh = "SELECT vehicle_id,brand,model,type FROM user_vehicle_table WHERE id='$user_vech_id'";
	$res_veh = mysqli_query($conn, $sql_veh) or die(mysqli_error($conn));
	$row_veh = mysqli_fetch_object($res_veh);
	$vehicle_id = $row_veh->vehicle_id;
	$brand = $row_veh->brand;
	$model = $row_veh->model;
	$vehicle_type = $row_veh->type;
	$veh_no = $row_veh->reg_no;
	
	$sqluv = "UPDATE user_vehicle_table SET fuel_type='$fuel_type',year='$m_year',vehicle_usage='$v_usage' WHERE id='$user_vech_id' ";  
	$resuv = mysqli_query($conn,$sqluv) or die(mysqli_error($conn));

	$sqlins1 = "INSERT INTO user_booking_tb(user_id,mec_id,shop_name,rep_name,rep_number,vech_id,user_veh_id,vehicle_type,user_vech_no,service_type,initial_service_type,amt,service_description,service_date,status,source,pick_up,crm_log_id,crm_allocate_id,crm_update_time,log,flag,booking_status,crm_update_id,followup_date,axle_flag,locality,city,pickup_address,pickup_full_address,language_of_contact,ire_flag,crwn_overide_log,crwn_overide_id,estimate_amt) VALUES ('$user_id','$mec_id','$shop_name','karthik','+919941413598','$vehicle_id','$user_vech_id','$vehicle_type','$veh_no','$service_type','$service_type','$amt','$service_description','$service_date','admin','Re-Engagement Bookings','$pick_up','$crm_log_id','$crm_log_id','$log','$log','0','1','$crm_log_id','$next_service_date','0','$pickup_location','$city','$pickup_address','$pickup_full_address','$language','1', '$today','$crm_log_id','$amt')";
	
	$resins1 = mysqli_query($conn, $sqlins1) or die(mysqli_error($conn));
	
	$inserted_booking_id = mysqli_insert_id($conn);
	
	$prospect_query = "UPDATE go_bumpr.ire_user_tbl SET status='5',crm_update_id='$crm_log_id',crm_update_time='$today',crm_log_id='$crm_log_id' where id='$ire_id'";
	$result_warm = mysqli_query($conn, $prospect_query) or die(mysqli_error($conn));

	
//echo $sqlins1;exit;
//********sms start*****************
	$sql = mysqli_query($conn, "SELECT mobile_number,name FROM user_register WHERE reg_id='$user_id'");
	while ($row = mysqli_fetch_array($sql)) {
		$to = $row['mobile_number'];
		$user_name = $row['name'];
	}
//  $to="9505913666";
//$user_name="Sirisha";
	$sql_contact = "SELECT * FROM go_axle_service_price_tbl WHERE service_type = '$service_type' AND type='$vehicle_type'";
	$res_contact = mysqli_query($conn, $sql_contact);
	$row_contact = mysqli_fetch_array($res_contact);
	
	$column = ucwords($city) . '_support';
	
	$contact = $row_contact[$column];
	
	if ($contact == '') {
		$contact = '9003251754';
	}
	
	$sms = "Dear " . $user_name . ", thanks for booking with us. Our representative will get in touch with you shortly. For help, please call us at " . $contact . ". Let's GoBumpr!";
	
	$to = str_replace(' ', '%20', $to);
	$user_name = str_replace(' ', '%20', $user_name);
	$sms = str_replace(' ', '%20', $sms);


// $ch=curl_init("http://203.212.70.200/smpp/sendsms?username=northautohttp&password=north123&to=%22".$to."%22&from=GOBMPR&udh=&text=".$sms."");
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// $result=curl_exec($ch);
// curl_close($ch);
	// $ch=curl_init("http://203.212.70.200/smpp/sendsms?username=northautohttp&password=north123&to=".$to."&from=GOBMPR&text=Dear%20Customer,%20On%20account%20of%20Pooja%20Holidays%20our%20service%20partners%20will%20not%20be%20functioning%20until%20Oct%202nd.%20Your%20request%20will%20be%20handled%20on%20Oct%203rd.%20Thank%20you!");
	// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// $result=curl_exec($ch);
	// curl_close($ch);
	$post = '{"status":"' . $result . '"}';
	
//********sms end**************** 
//exit;
	if ($override_flag == '1') {
		echo json_encode(['mode'=> 'om', 'booking_id' => $inserted_booking_id]);
		die;
	}
	if ($cluster_admin == '1') {
		echo json_encode(['mode'=> 'lead']);
		die;
		// header("Location:prospect_goaxle.php");
	} else if ($flag == 1) {
		echo json_encode(['mode'=> 'lead']);
		die;
		// header("Location:prospect_goaxle.php");
	} else {
		echo json_encode(['mode'=> 'lead']);
		die;
		// header("Location:prospect_goaxle.php");
	}
	
}elseif($pid == 'add_vehicle'){
    $user_id = $_REQUEST['user_id'];
    $veh_id = $_REQUEST['veh_id'];
    $t = $_REQUEST['type'];
    $booking_id= $_REQUEST['book_id'];
    
    $log= date('Y-m-d H:i:s');
    $type=mysqli_real_escape_string($conn,$_POST['veh']);
    $brand_model=mysqli_real_escape_string($conn,$_POST['BrandModelid']);
    $fuel_type=mysqli_real_escape_string($conn,$_POST['fuel']);
    $reg_no=mysqli_real_escape_string($conn,$_POST['regno']);
    $year=mysqli_real_escape_string($conn,$_POST['year']);
    $km_driven=mysqli_real_escape_string($conn,$_POST['km']);
    
    $sql_b_m = "SELECT id,brand,model FROM admin_vehicle_table_new WHERE brand_s_model = '$brand_model' ";
    $quer_b_m = mysqli_query($conn,$sql_b_m);
    $row_b_m = mysqli_fetch_object($quer_b_m);
    $vehicle_id=$row_b_m->id;
    $brand=$row_b_m->brand;
    $model=$row_b_m->model;
    
    $sqlins="INSERT INTO user_vehicle_table(user_id,vehicle_id,brand,model,type,fuel_type,year,km_driven,reg_no,flag,crm_log_id,crm_update_time,log) VALUES('$user_id','$vehicle_id','$brand','$model','$type','$fuel_type','$year','$km_driven','$reg_no','1','$crm_log_id','$log','$log')";
    mysqli_query($conn,$sqlins) or die(mysqli_error($conn));
    
    mysqli_query($conn,"UPDATE user_register SET vehicle='1' WHERE reg_id='$user_id'");

//redirect
    $t= base64_encode($t);
    $veh = base64_encode($veh_id);
    $bi = base64_encode($booking_id);
    
    
    header("Location:user_details_ire.php?t=$t&v58i4=$veh&bi=$bi");

}elseif($pid == 'edit_user'){
    
    $log = date('Y-m-d H:i:s');
    
    $user_id = $_REQUEST['user_id'];
    $veh_id = $_REQUEST['veh_id'];
    $type = $_REQUEST['type'];
    $booking_id= $_REQUEST['book_id'];
    
    $mobile = mysqli_real_escape_string($conn,$_POST['mobile']);
    $mobile2 = mysqli_real_escape_string($conn,$_POST['mobile2']);
    $name = mysqli_real_escape_string($conn,$_POST['user_name']);
    $email_id = mysqli_real_escape_string($conn,$_POST['email']);
    $City = mysqli_real_escape_string($conn,$_POST['city']);
    $Locality_Home = mysqli_real_escape_string($conn,$_POST['location_home']);
    $Locality_Work = mysqli_real_escape_string($conn,$_POST['location_work']);
    $Next_service_date = date('Y-m-d',strtotime(mysqli_real_escape_string($conn,$_POST['next_service_date'])));
    $Last_service_date = date('Y-m-d',strtotime(mysqli_real_escape_string($conn,$_POST['last_service_date'])));
    $Last_called_on = date('Y-m-d',strtotime(mysqli_real_escape_string($conn,$_POST['last_called_on'])));
    $Follow_up_date = date('Y-m-d',strtotime(mysqli_real_escape_string($conn,$_POST['follow_up_date'])));
    $comments = mysqli_real_escape_string($conn,$_POST['comments']);
    $campaign = mysqli_real_escape_string($conn,$_POST['campaign']);
    $source = mysqli_real_escape_string($conn,$_POST['source']);
    
    $query="UPDATE user_register SET name='$name',mobile_number2='$mobile2',email_id='$email_id',crm_log_id='$crm_log_id',crm_update_time='$log',source='$source',campaign='$campaign', City='$City',Locality_Home='$Locality_Home',Locality_Work='$Locality_Work',Next_service_date='$Next_service_date',Last_service_date='$Last_service_date',Last_called_on='$Last_called_on',Follow_up_date='$Follow_up_date',comments='$comments' WHERE reg_id='$user_id'";
    
    $res=mysqli_query($conn,$query) or die(mysqli_error($conn));
    
    /*redirect */
    $t= base64_encode($type);
    $veh = base64_encode($veh_id);
    $bi = base64_encode($booking_id);
    
    header("Location:user_details_ire.php?t='$t'&v58i4='$veh'&bi='$bi' ");
}

?>

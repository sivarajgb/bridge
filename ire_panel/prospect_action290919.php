<?php

include '../config.php';

$conn = db_connect1();

session_start();

$user_id = $_POST['user_id'];
$veh_id = $_POST['veh_id'];
$type = $_POST['type'];
$booking_id = $_POST['booking_id'];
$log = $_POST['log'];
$pid = $_POST['pid'];

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$user_flag = $_SESSION['flag'];
$cluster_admin = $_SESSION['cluster_admin'];

$today = date('Y-m-d H:i:s');

if (!isset($_POST)) {
    header("Location:logout.php");
}

if ($pid == "follow_up") {

    $status = $_POST['followup_status'];
    $comments = $_POST['followup_comments'];
    $followup_date = $_POST['followup_date'];
    $followup_date_text = $_POST['followup_date_text'];
    $followup_date = $_POST['followup_date'];
    $ire_id = $_POST['ire_id'];
    $user_id = $_POST['user_id'];
    $booking_id = $_POST['booking_id'];
    $warm_query = "UPDATE go_bumpr.ire_user_tbl SET status='2',prospect_date='$followup_date', prospect_date_type='$followup_date_text', reason='$comments',crm_update_id='$crm_log_id',crm_update_time='$today',crm_log_id='$crm_log_id' where id='$ire_id'";
    $result_warm = mysqli_query($conn, $warm_query) or die(mysqli_error($conn));

    $followup_prospects = 'FollowUp - ' . $user_id . ' - ' . $status;
    $query = "INSERT INTO admin_comments_tbl(user_id,crm_log_id,category,comments,Follow_up_date,flag,log,status,book_id) VALUES ('$user_id','$crm_log_id','$followup_prospects','$comments','0000-00-00','0','$today','IRE FollowUp','$booking_id')";
    $result = mysqli_query($conn, $query) or die(mysqli_error($conn));
    header("Location:followup_prospect.php");

} elseif ($pid == "cancel") {
    $status = $_POST['cancel_status'];
    $comments = $_POST['cancel_comments'];
    $prospect_date = date('Y-m-d', strtotime($_POST['cancel_prospects_date']));
    $ire_id = $_POST['ire_id'];
    $user_id = $_POST['user_id'];
    $booking_id = $_POST['booking_id'];
    $cold_query = "UPDATE go_bumpr.ire_user_tbl SET status='3',prospect_date='$prospect_date',reason='$comments',crm_update_id='$crm_log_id',crm_update_time='$today',crm_log_id='$crm_log_id' where id='$ire_id'";
    $result_warm = mysqli_query($conn, $cold_query) or die(mysqli_error($conn));

    $cold_prospects = 'Cancel Prospects - ' . $user_id . ' - ' . $status;
    $query = "INSERT INTO admin_comments_tbl(user_id,crm_log_id,category,comments,Follow_up_date,flag,log,status,book_id) VALUES ('$user_id','$crm_log_id','$cold_prospects','$comments','0000-00-00','0','$today','Cold','$booking_id')";
    $result = mysqli_query($conn, $query) or die(mysqli_error($conn));
    header("Location:cancel_prospect.php");

} elseif ($pid == "new_booking") {

    $today = date('Y-m-d H:i:s');

    $user_id = $_REQUEST['user_id'];
    if (isset($_REQUEST['type'])) {
        $type = $_REQUEST['type'];
    }
    $booking_id = $_REQUEST['book_id'];

    $crm_log_id = $_SESSION['crm_log_id'];
    $crm_name = $_SESSION['crm_name'];
    $flag = $_SESSION['flag'];
    $cluster_admin = $_SESSION['cluster_admin'];
    $ire_id = $_POST['ire_id'];
    $log = date('Y-m-d H:i:s');
    $mec_id = mysqli_real_escape_string($conn, $_POST['mechanic']);
    $locality = mysqli_real_escape_string($conn, $_POST['location']);

    $sql_loc = "SELECT city FROM localities WHERE localities='$locality'";
    $res_loc = mysqli_query($conn, $sql_loc);
    $row_loc = mysqli_fetch_array($res_loc);
    $city = $row_loc['city'];

    $vehicle_type = mysqli_real_escape_string($conn, $_POST['veh_b']);
    $user_vech_id = mysqli_real_escape_string($conn, $_POST['veh_no']);
    $service_description = mysqli_real_escape_string($conn, $_POST['description']);

    $service_type = mysqli_real_escape_string($conn, $_POST['service_type']);
    $amt = mysqli_real_escape_string($conn, $_POST['amount']);
    $service_date = date('Y-m-d', strtotime(mysqli_real_escape_string($conn, $_POST['service_date'])));
    $next_service_date = date('Y-m-d', strtotime(mysqli_real_escape_string($conn, $_POST['next_service_date'])));
// $pick_up=mysqli_real_escape_string($conn,$_POST['pickup']);
    $pick_up = 0;

//get shop name
    $sql_mec = "SELECT shop_name FROM admin_mechanic_table WHERE mec_id='$mec_id' ";
    $res_mec = mysqli_query($conn, $sql_mec);
    $row_mec = mysqli_fetch_object($res_mec);
    $shop_name = $row_mec->shop_name;

    $sql_veh = "SELECT vehicle_id,brand,model,type FROM user_vehicle_table WHERE id='$user_vech_id'";
    $res_veh = mysqli_query($conn, $sql_veh) or die(mysqli_error($conn));
    $row_veh = mysqli_fetch_object($res_veh);
    $vehicle_id = $row_veh->vehicle_id;
    $brand = $row_veh->brand;
    $model = $row_veh->model;
    $vehicle_type = $row_veh->type;
    $veh_no = $row_veh->reg_no;

    $sqlins1 = "INSERT INTO user_booking_tb(user_id,mec_id,shop_name,rep_name,rep_number,vech_id,user_veh_id,vehicle_type,user_vech_no,service_type,initial_service_type,amt,service_description,service_date,status,source,pick_up,crm_log_id,crm_allocate_id,crm_update_time,log,flag,booking_status,crm_update_id,followup_date,axle_flag,locality,city,ire_flag) VALUES ('$user_id','$mec_id','$shop_name','karthik','+919941413598','$vehicle_id','$user_vech_id','$vehicle_type','$veh_no','$service_type','$service_type','$amt','$service_description','$service_date','admin','Hub Booking','$pick_up','$crm_log_id','$crm_log_id','$log','$log','0','1','$crm_log_id','$next_service_date','0','$locality','$city','1')";
    $resins1 = mysqli_query($conn, $sqlins1) or die(mysqli_error($conn));

    $prospect_query = "UPDATE go_bumpr.ire_user_tbl SET status='5',crm_update_id='$crm_log_id',crm_update_time='$today',crm_log_id='$crm_log_id' where id='$ire_id'";
    $result_warm = mysqli_query($conn, $prospect_query) or die(mysqli_error($conn));

//********sms start*****************
    $sql = mysqli_query($conn, "SELECT mobile_number,name FROM user_register WHERE reg_id='$user_id'");
    while ($row = mysqli_fetch_array($sql)) {
        $to = $row['mobile_number'];
        $user_name = $row['name'];
    }
//  $to="9505913666";
//$user_name="Sirisha";
    $sql_contact = "SELECT * FROM go_axle_service_price_tbl WHERE service_type = '$service_type' AND type='$vehicle_type'";
    $res_contact = mysqli_query($conn, $sql_contact);
    $row_contact = mysqli_fetch_array($res_contact);

    $column = ucwords($city) . '_support';

    $contact = $row_contact[$column];


    if ($contact == '') {
        $contact = '9003251754';
    }


    $sms = "Dear " . $user_name . ", thanks for booking with us. Our representative will get in touch with you shortly. For help, please call us at " . $contact . ". Let's GoBumpr!";

    $to = str_replace(' ', '%20', $to);
    $user_name = str_replace(' ', '%20', $user_name);
    $sms = str_replace(' ', '%20', $sms);


// $ch=curl_init("http://203.212.70.200/smpp/sendsms?username=northautohttp&password=north123&to=%22".$to."%22&from=GOBMPR&udh=&text=".$sms."");
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// $result=curl_exec($ch);
// curl_close($ch);
    // $ch=curl_init("http://203.212.70.200/smpp/sendsms?username=northautohttp&password=north123&to=".$to."&from=GOBMPR&text=Dear%20Customer,%20On%20account%20of%20Pooja%20Holidays%20our%20service%20partners%20will%20not%20be%20functioning%20until%20Oct%202nd.%20Your%20request%20will%20be%20handled%20on%20Oct%203rd.%20Thank%20you!");
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // $result=curl_exec($ch);
    // curl_close($ch);
    $post = '{"status":"' . $result . '"}';

//********sms end**************** 

    if ($cluster_admin == '1') {
        header("Location:prospect_goaxle.php");
    } else if ($flag == 1) {
        header("Location:prospect_goaxle.php");
    } else {
        header("Location:prospect_goaxle.php");
    }
}

?>

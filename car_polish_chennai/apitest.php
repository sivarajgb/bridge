<?php


$curl = curl_init();
$auth_data = array(
	'Name' 		=> 'Test',
	'EmailID' 	=> 'abc@gmail.com',
	'MobileNo' 		=> '9999999999',
	'CarBrand' 	=> 'Maruti',
	'CarModel' 	=> 'Alto',
	'RTO' 	=> 'Adyar'
);

curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $auth_data);
curl_setopt($curl, CURLOPT_URL, 'https://www.cholainsurance.com/campaign/CholaWebservice/CholaProductService.asmx/CholaCampaignService');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

$result = curl_exec($curl);
if(!$result){die("Connection Failure");}
curl_close($curl);
echo $result;

?>
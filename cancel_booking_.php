<?php
include('config.php');
$conn = db_connect1();
session_start();

$user_id = $_REQUEST['user_id'];
$veh_id = $_REQUEST['veh_id'];
$type = $_REQUEST['type'];
$booking_id= $_REQUEST['book_id'];

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$cluster_admin = $_SESSION['cluster_admin'];

$today = date('Y-m-d H:i:s');

$category=mysqli_real_escape_string($conn,$_POST['status']);
$comments=mysqli_real_escape_string($conn,$_POST['comments']);
 
$sql_act = mysqli_query($conn,"SELECT id FROM admin_activity_tbl WHERE activity='$category'");
$row_act = mysqli_fetch_object($sql_act);
$activity_status = $row_act->id;

if($category == "Duplicate Booking"){
	$original = 1;
	$yesterday = date('Y-m-d H:i:s',strtotime("-1 days"));
	$prev_booking_query  = "SELECT * FROM user_booking_tb WHERE user_id = '$user_id' AND booking_id < '$booking_id' and date(log) > '$yesterday' ORDER BY booking_id DESC LIMIT 1 ";
	$prev_booking_res = mysqli_query($conn,$prev_booking_query) or die(mysqli_error($conn));	
	while($prev_booking_row = mysqli_fetch_object($prev_booking_res))
	{
		$prev_booking_id = $prev_booking_row->booking_id;
		$booking_status = $prev_booking_row->booking_status;
		$flag = $prev_booking_row->flag;
		$flag_unwntd = $prev_booking_row->flag_unwntd;
	}
	$comments_action_sql = "SELECT * FROM admin_comments_tbl WHERE book_id = '$prev_booking_id' AND (activity_status NOT IN (9,10,11,13,14,24,26) OR activity_status IS NULL)";
	$comments_action_res = mysqli_query($conn,$comments_action_sql) or die(mysqli_error($conn));
	$action_count = mysqli_num_rows($comments_action_res);
	if($flag == 1 || $flag_unwntd == 1)
	{
		$original = 1;
	}
	else if($action_count > 0 || $booking_status == 2)
	{
		$original = 0;
	}
	
	if($original == 0)
	{
		$sql = "UPDATE user_booking_tb SET flag=1,flag_unwntd='1',flag_duplicate='1',crm_update_time='$today',activity_status='$activity_status' WHERE booking_id='$booking_id' ";
	}
	else
	{
		$sql = "UPDATE user_booking_tb SET flag=1,flag_unwntd='1',crm_update_time='$today',activity_status='$activity_status' WHERE booking_id='$booking_id' ";
	}
}
else{
    $sql = "UPDATE user_booking_tb SET flag=1,crm_update_time='$today',activity_status='$activity_status' WHERE booking_id='$booking_id' ";    
}

$res = mysqli_query($conn,$sql) or die(mysqli_error($conn));

$cancelled = 'Booking Cancelled - '.$booking_id.' - '.$category;
$query="INSERT INTO admin_comments_tbl(user_id,crm_log_id,category,comments,Follow_up_date,flag,log,status,activity_status,book_id) VALUES ('$user_id','$crm_log_id','$cancelled','$comments','0000-00-00','0','$today','Cancelled','$activity_status','$booking_id')";
$result=mysqli_query($conn,$query) or die(mysqli_error($conn));

if($cluster_admin == '1'){
    switch($type){
        case "l":header('Location:mleads.php');break;
        case "f":header('Location:mfollowups.php');break;
        case 'unwanted': header("Location:unwanted_leads.php"); break;
        default:header('Location:mcancelled.php');
    }
} 
else if($flag == 1){
    switch($type){
        case "l":header('Location:aleads.php');break;
        case "f":header('Location:afollowups.php');break;
        case 'unwanted': header("Location:unwanted_leads.php"); break;
        default:header('Location:acancelled.php');
    }
}
else{
    switch($type){
        case "l":header('Location:leads.php');break;
        case "f":header('Location:followups.php');break;
        default:header('Location:cancelled.php');
    }
}


?>
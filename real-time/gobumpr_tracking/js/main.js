$(function () {
	var filledStarTemplate = '<i class="fa fa-2x fa-star"></i>';
	var emptyStarTemplate = '<i class="fa fa-2x fa-star-o"></i>';
	$("#mechanic-rating").rating({
		displayOnly: true,
		filledStar: filledStarTemplate,
		emptyStar: emptyStarTemplate
	});
	$("#service-rating").rating({
		filledStar: filledStarTemplate,
		emptyStar: emptyStarTemplate,
		size: "sm",
		showCaption: false,
		showClear: false
	});
	$("#service-ratings").rating({
		filledStar: filledStarTemplate,
		emptyStar: emptyStarTemplate,
		size: "sm",
		showCaption: false,
		showClear: false
	});
});
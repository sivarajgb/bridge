<!doctype html>
<html>
<head>
<title>sort.php</title>
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/pop_up.css" rel="stylesheet">
 <style>
 .heading_tooltip {
	text-transform:uppercase;
	font-weight:bolder;
}
.leading_tooltip {
	color:#ffa800;
}
</style>        

</head>

<body>
<?php
include 'service_types.php';
$details = json_decode($_POST['data'], true);
$pick_up = json_decode($_POST['pick_up'], true);
$private = json_decode($_POST['private'], true);
$authorised = json_decode($_POST['authorised'], true);
$variant = json_decode($_POST['variant'], true);
$wash = json_decode($_POST['wash'], true);
$params = json_decode($_POST['params'], true);
$toggle = $_POST['toggle'];


$BrandModel = $params['BrandModel'];
$locality_s = $params['locality'];
$ServiceType = $params['ServiceType'];
$vehicle_type = $params['VehicleType'];
?> 
<input type="hidden" value="<?php echo $vehicle_type; ?>" id="VehicleType_hidden">
<?php
//error_reporting(-1);
//ini_set('display_errors', 1);

switch($toggle) {
case 'sort_price':
					if($variant == '0' || $variant == '1')
						$details = general_service($params);
					if($wash == '0' || $wash == '1')
						$details = water_wash($params);
					if($params['ServiceType']=="Tyre Puncture") {
						$details = tyre_puncture($params);
					$details = array_values($details);
						foreach ($details as $key => $row) {
							$price[$key]  = $row['price'];
							$distance[$key]  = $row['distance'];
						}
						array_multisort($price, SORT_ASC, $distance, SORT_ASC, $details);
					}//if
					else {
						foreach ($details as $key => $row) {
							$price[$key]  = $row['price'];
							$distance[$key]  = $row['distance'];
						}
						array_multisort($price, SORT_ASC, $distance, SORT_ASC, $details);
					}
					
					$sorted_array = array_values($details);
					
					if($pick_up == '1')
						$sorted_array = pick_up($sorted_array);
						
					if($private == '1' && $authorised == '1') 
						$sorted_array = $sorted_array;
					else {
						if($authorised == '1')  {
							//print_r($sorted_array);
							$sorted_array = est_type($sorted_array);
							//print_r($sorted_array);							
						}
						if($private == '1') { 
							$sorted_array = est_type_private($sorted_array);
							//print_r($sorted_array);							
						}
					}
					//print_r($sorted_array);
					
					break;
					
					
case 'sort_distance' :
					if($variant == '0' || $variant == '1')
						$details = general_service($params);
					if($wash == '0' || $wash == '1')
						$details = water_wash($params);

						uasort($details, function ($i, $j) {
							$a = $i['distance'];
							$b = $j['distance'];
							if ($a == $b) return 0;
							elseif ($a > $b) return 1;
							else return -1;
						});
						$sorted_array = array_values($details);
					
						if($pick_up == '1')
							$sorted_array = pick_up($sorted_array);
					if($private == '1' && $authorised == '1') 
						$sorted_array = $sorted_array;
					else {
						if($authorised == '1') 
							$sorted_array = est_type($sorted_array);
						if($private == '1') 
							$sorted_array = est_type_private($sorted_array);
					}
						//print_r($sorted_array);
						
						
						break;
						
case 'sort_ratings':
					if($variant == '0' || $variant == '1')
						$details = general_service($params);
					if($wash == '0' || $wash == '1')
						$details = water_wash($params);

						foreach ($details as $key => $row) {
							$rating[$key]  = $row['rating'];
							$distance[$key]  = $row['distance'];
						}
						array_multisort($rating, SORT_DESC, $distance, SORT_ASC, $details);
					$sorted_array = array_values($details);
					
					if($pick_up == '1')
						$sorted_array = pick_up($sorted_array);
					if($private == '1' && $authorised == '1') 
						$sorted_array = $sorted_array;
					else {
						if($authorised == '1') {
							$sorted_array = est_type($sorted_array);
						}
						if($private == '1') 
							$sorted_array = est_type_private($sorted_array);
					}
					//print_r($sorted_array);
					
					break;

}//switch


function pick_up($sorted_array) {
	foreach($sorted_array as $key => $value) {
		if($value['pickup_available'] === "0" || $value['pickup_available'] === "") {
			$keys = array();
			$i=$key;
			$keys[$i] = $key;
			//print_r($keys);
			$sorted_array[$i] = array_filter($sorted_array[$keys]);
			$sorted_array = array_filter($sorted_array);
			$i++;
		}
	}//foreach
	return $sorted_array;
}

$copy_details = array_values($details);
//print_r($copy_details);
function est_type($sorted_array) {
	foreach($sorted_array as $key => $value) {
		if($value['authorized_status'] === "0") {
			$keys = array();
			$i=$key;
			$keys[$i] = $key;
			//print_r($keys);
			$sorted_array[$i] = array_filter($sorted_array[$keys]);
			$sorted_array = array_filter($sorted_array);
			$i++;
		}
	}
	return $sorted_array;
}


function est_type_private($sorted_array) {
	foreach($sorted_array as $key => $value) {//taking opposite value to filter out the unnecessary values
		if($value['authorized_status'] == "1") { //null is same as private, 
			$keys = array();
			$i=$key;
			$keys[$i] = $key;
			//print_r($keys);
			$sorted_array[$i] = array_filter($sorted_array[$keys]);
			$sorted_array = array_filter($sorted_array);
			$i++;
		}
	}
	return $sorted_array;
}


if(empty($variant) && !empty($BrandModel) && $ServiceType == "General Service" && $vehicle_type == '4w') {
	$arr = $copy_details;
 $new_arr =array();
			foreach($arr as $key=>$row){
				if(isset($new_arr[$row['mec_id']])){
					$new_arr[$row['mec_id']]= (($new_arr[$row['mec_id']]<$row['price'])?($new_arr[$row['mec_id']].'-'.$row['price']) : ($row['price'].'-'.$new_arr[$row['mec_id']])) ;
				}
				else{
					$new_arr[$row['mec_id']]=$row['price'];
				}
			}
	
			foreach($new_arr as $key=>$row){
				$pPrice[$key] =$row;
			}

//print_r($pPrice);
//print_r($sorted_array);
$myarr= $details;

$your_unique_array = array();
	foreach ($myarr as $v) {
		  if (!isset($your_unique_array[$v['mec_id']])) {
			  $your_unique_array[$v['mec_id']] = $v;
		  }
	}
//print_r($your_unique_array);

			foreach($your_unique_array as $key=>$value) {
						foreach($pPrice as $k=>$v) {
									if($key == $k) {
										$your_unique_array[$key]['price'] = $pPrice[$k];
									}
						}
			}
//print_r($your_unique_array);
$sorted_array = array_values($your_unique_array);
}//IF

 $search_results_count = count($sorted_array);
if(count($details)!==''){

				foreach($sorted_array as $v)
				{
					
				?>
    			<div class="strip_all_tour_list fadeIn">
				
                   <div class="row">
                	<div class="col-lg-4 col-md-4 col-sm-4">
                    <?php 
					//to get url
					$service_centre_name = str_replace(' ', '-', $v['shop_name']);
					$service_centre_address = str_replace(' ', '-', $v['address4']);
					$url="$service_centre_name".'-'.'in'.'-'."$service_centre_address";
					//echo $url = urlencode($url);
					?>
                    	<div class="img_list"><a href="/gobumpr-test/chennai/book-now/<?php echo urlencode($url); ?>?price=<?php echo $v['price']; ?>"><img src="<?php echo $v['images'];?>" alt="<?php echo $url; ?>">
                        <div class="short_info" style="font-size:12.5px"><?php if($v['attributes']!='') { ?><i class="icon-trophy" style="font-size:10px"></i> <?php echo $v['attributes']; }?></div>
                        </a>
                        </div>
                    </div>
                     <div class="clearfix visible-xs-block"></div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                    		<div class="tour_list_desc">
                            <!--<div class="score"><span><?php //echo $v['rating'] ?></span></div>-->
                            <div  style="margin-top:15px;">
                    		<a href="/gobumpr-test/chennai/book-now/<?php echo urlencode($url); ?>?price=<?php echo $v['price']; ?>"><h3><strong><?php echo $v['shop_name']?></strong></h3></a>
                            </div>
                            <div class="rating">
                             <?php if($v['rating']==0) { ?>
                                <i class="icon-star-empty"></i>
                                <i class="icon-star-empty"></i>
                                <i class="icon-star-empty"></i>
                                <i class="icon-star-empty"></i>
                                <i class="icon-star-empty"></i>
                              <?php }//if 1 ?>
                                
                             <?php if($v['rating']==1) { ?>
                                <i class="icon-star voted"></i>
                                <i class="icon-star-empty"></i>
                                <i class="icon-star-empty"></i>
                                <i class="icon-star-empty"></i>
                                <i class="icon-star-empty"></i>
                              <?php }//if 1 ?>
                                
                             <?php if($v['rating']==2) { ?>
                                <i class="icon-star voted"></i>
                                <i class="icon-star voted"></i>
                                <i class="icon-star-empty"></i>
                                <i class="icon-star-empty"></i>
                                <i class="icon-star-empty"></i>
                              <?php }//if 1 ?>
                                
                             <?php if($v['rating']==3) { ?>
                                <i class="icon-star voted"></i>
                                <i class="icon-star voted"></i>
                                <i class="icon-star voted"></i>
                                <i class="icon-star-empty"></i>
                                <i class="icon-star-empty"></i>
                              <?php }//if 1 ?>
                                
                             <?php if($v['rating']==4) { ?>
                                <i class="icon-star voted"></i>
                                <i class="icon-star voted"></i>
                                <i class="icon-star voted"></i>
                                <i class="icon-star voted"></i>
                                <i class="icon-star-empty"></i>
                              <?php }//if 1 ?>
                                
                             <?php if($v['rating']==5) { ?>
                                <i class="icon-star voted"></i>
                                <i class="icon-star voted"></i>
                                <i class="icon-star voted"></i>
                                <i class="icon-star voted"></i>
                                <i class="icon-star voted"></i>
                              <?php }//if 1 ?>
                                
                                <small></small>
                            </div>
                            <p> <i class="icon-location-4" style="font-size:16px; margin-left:-5px;"></i><?php echo $v['address4']; ?> | <?php echo round($v['distance'], 1); ?> KMs away</p>
                            <p><?php //if($v['authorized_status'] == '1') ?></p> 
                            <p><i class="icon-clock-1"></i><?php echo $v['day_from']; ?> - <?php echo $v['day_to']; ?> | 
                                    	<?php echo date('h:i A', strtotime($v['time_from'])); ?> - <?php echo date('h:i A', strtotime($v['time_to'])); ?></p> 
                            <ul class="add_info">
                                <li>
                                     <a href="javascript:void(0);" class="tooltip-1" data-toggle="tooltip" data-placement="top" title="Since <?php echo $v['since']; ?>"><i class="icon_set_1_icon-86"></i></a>
                               </li>
                                <li>
                                     <a href="javascript:void(0);" class="tooltip-1" data-toggle="tooltip" data-placement="top" title="<?php echo $v['no_of_mechanic'];?> Mechanics"><i class="icon_set_1_icon-86"></i></a>
                               </li>
                                <li>
                                     <a href="javascript:void(0);" class="tooltip-1" data-toggle="tooltip" data-placement="top" title="<?php echo $v['shop_size'];?> sq ft"><i class="icon_set_1_icon-86"></i></a>
                               </li>
                                <li>
                                     <a href="javascript:void(0);" class="tooltip-1" data-toggle="tooltip" data-placement="top" title="<?php echo $v['address1'].','; ?> <?php echo $v['address2'].','; ?> <?php echo $v['address4'].','; ?> <?php echo $v['address5'].','; ?> <?php echo $v['pincode']; ?>"><i class="icon_set_1_icon-86"></i></a>
                               </li>
                                <li>
                                     <a href="javascript:void(0);" class="tooltip-1" data-toggle="tooltip" data-placement="top" title="Online Payment Avalilable"><i class="icon_set_1_icon-86"></i></a>
                               </li>
                            <!--  <li>
                            <div class="tooltip_styled tooltip-effect-4">
                            	<span class="tooltip-item"><i class="icon_set_1_icon-41"></i></span>
                                	<div class="tooltip-content"><span class="heading_tooltip">Since</span> 
                                    	<span class="leading_tooltip"></span>
                                </div>
                              </div>
                           </li> 
                            <li>
                            <div class="tooltip_styled tooltip-effect-4">
                            	<span class="tooltip-item"><i class="icon_set_1_icon-41"></i></span>
                                	<div class="tooltip-content"><span class="heading_tooltip">Number Of Mechanics </span>
                                    	
                                </div>
                              </div>
                           </li> 
                             <li>
                            <div class="tooltip_styled tooltip-effect-4">
                            	<span class="tooltip-item"><i class="icon_set_1_icon-41"></i></span>
                                	<div class="tooltip-content"><span class="heading_tooltip">Workshop Size </span>
                                    	
                                </div>
                              </div>
                           </li>
                           <li>
                            <div class="tooltip_styled tooltip-effect-4">
                            	<span class="tooltip-item"><i class="icon_set_1_icon-41"></i></span>
                                	<div class="tooltip-content"><h4>What's Included</h4>
                                    	
                                </div>
                              </div>
                           </li>
                           <li>
                            <div class="tooltip_styled tooltip-effect-4">
                            	<span class="tooltip-item"><i class="icon_set_1_icon-41"></i></span>
                                	<div class="tooltip-content"><h4>Address</h4>
                                    	<br>
                                </div>
                              </div>
                           </li> -->
                           
                            </ul>
                            </div>
                    </div>
					<div class="col-lg-2 col-md-2 col-sm-2">
                    	<div class="price_list"><div>
                        <?php  if(strlen($v['price'])>6) { ?>
                        <div style="font-size:18px">
                        <?php if($v['price']!=0 && $v['price']!=-1) { ?>
                        	&#8377; <?php echo $v['price'];?>
                        <?php }
						elseif($v['price']==-1) { ?>
								Labour Free *
						<?php }
						else { ?>
								<button onclick="focussing();" class="btn_1 hidden-xs hidden-sm" id="show_price" style="background-color:#909090; width:120px; font-size:11px">Show Price</button>
								<a data-toggle="modal" data-target="#myReview" class="btn_1 hidden-lg hidden-md mobile_show_price" id="show_price" style="background-color:#909090; width:120px; font-size:11px">Show Price</a>
						<?php } ?>
                        </div>
<?php }// if double price range
else { ?>

                        <?php if($v['price']!=0 && $v['price']!=-1) { ?>
                        	&#8377; <?php echo $v['price'];?>
                        <?php }
						elseif($v['price']==-1) { ?>
								Labour Free *
						<?php }
						else { ?>
								<button onclick="focussing();" class="btn_1 hidden-xs hidden-sm" id="show_price" style="background-color:#909090; width:120px; font-size:11px">Show Price</button>
								<a data-toggle="modal" data-target="#myReview" class="btn_1 hidden-lg hidden-md mobile_show_price" id="show_price" style="background-color:#909090; width:120px; font-size:11px">Show Price</a>
						<?php } ?>
<?php }//else not special cases for price ?>                       
						<br>
						<br>
                        <p><a href="/gobumpr-test/chennai/book-now/<?php echo urlencode($url); ?>?price=<?php echo $v['price']; ?>" class="btn_1">Book Now</a></p>
                        </div>
                        </div>
                    </div>
                    </div>
				</div><!--End strip -->
				<?php }//foreach
}//if null values are returned
//else
//{
?>
<div class="row">
<h4>We were unable to find services in your locality</h4>
<div class="col-md-6 col-md-offset-2">
<center><h2>Let us know</h2></center>
        <div class="box_style_1" id="fix_booking" >
            <h3 class="inner">- Book a Service -</h3>
<form role="form" method="post" id="1st_book" name="1st_book" action="">            
            <div class="row" id="1st_booking_form">
               <div class="col-md-12 col-sm-12" style="top:10px;">
                 <div class="form-group"> 
                      <label>Vehicle Model</label>
                      <input class="form-control autocomplete" id="BrandModelid" type="text" name="BrandModel" value="<?php echo $BrandModel; ?>" required placeholder="Select Model"><span id="message"></span>
  <span id="dropdown_icon" class="dd-pointer dd-pointer-down" style=" margin-right:15px; margin-top:1%"></span>
                </div>
               </div>
               <div class="col-md-12 col-sm-12">                  
                 <div class="form-group">   
                      <label>Locality</label>
                      <input class="form-control autocomplete" id="localityid" type="text" value="<?php echo $locality_s; ?>" name="locality" required placeholder="Select Locality"><span id="message"></span>
  <span id="dropdown_icon" class="dd-pointer dd-pointer-down" style=" margin-right:15px; margin-top:1%"></span>
                </div>
                </div>
	<div class="col-md-12 col-sm-12"> 
	<div class="form-group">
    <label> Service </label>
        <select id="ServiceType" class="ddslick form-control" name="ServiceType">
        <?php if($type=='4w') {?>
            <option <?php if($service_name_s == 'General Service') echo "selected='selected'"; ?>  value="General Service" data-imagesrc="/gobumpr-test/img/gobumpr/car_general_service.png">General Service</option>
            <option <?php if($service_name_s == 'Breakdown Assistance') echo "selected='selected'"; ?> value="Breakdown Assistance" data-imagesrc="/gobumpr-test/img/gobumpr/car_breakdown.png">Breakdown Assistance</option>
            <option <?php if($service_name_s == 'Car Wash') echo "selected='selected'"; ?>  value="Car Wash"  data-imagesrc="/gobumpr-test/img/gobumpr/car_wash.png">Car Wash</option>
            <option  <?php if($service_name_s == 'Tyre Puncture') echo "selected='selected'"; ?> value="Tyre Puncture" data-imagesrc="/gobumpr-test/img/gobumpr/car_tyre_puncture.png">Tyre Puncture</option>
            <option  <?php if($service_name_s == 'Vehicle Diagnostics') echo "selected='selected'"; ?> value="Vehicle Diagnostics" data-imagesrc="/gobumpr-test/img/gobumpr/car_vehicle_diagnostics.png">Vehicle Diagnostics</option>
            <option <?php if($service_name_s == 'Other Repairs') echo "selected='selected'"; ?> value="Other Repairs" data-imagesrc="/gobumpr-test/img/gobumpr/car_other_repairs.png">Other Repairs</option>
            
            
         <?php }  else {?>
            <option <?php if($service_name_s == 'General Service') echo "selected='selected'"; ?>  value="General Service" data-imagesrc="/gobumpr-test/img/gobumpr/bike_general_service.png">General Service</option>
            <option <?php if($service_name_s == 'Breakdown Assistance') echo "selected='selected'"; ?> value="Breakdown Assistance" data-imagesrc="/gobumpr-test/img/gobumpr/bike_breakdown.png">Breakdown Assistance</option>
            <option <?php if($service_name_s == 'Water Wash') echo "selected='selected'"; ?>  value="Water Wash"  data-imagesrc="/gobumpr-test/img/gobumpr/bike_wash.png">Water Wash</option>
            <option  <?php if($service_name_s == 'Tyre Puncture') echo "selected='selected'"; ?> value="Tyre Puncture" data-imagesrc="/gobumpr-test/img/gobumpr/bike_tyre_puncture.png">Tyre Puncture</option>
            <option  <?php if($service_name_s == 'Vehicle Diagnostics') echo "selected='selected'"; ?> value="Vehicle Diagnostics" data-imagesrc="/gobumpr-test/img/gobumpr/bike_vehicle_diag.png">Vehicle Diagnostics</option>
            <option <?php if($service_name_s == 'Other Repairs') echo "selected='selected'"; ?>  value="Other Repairs" data-imagesrc="/gobumpr-test/img/gobumpr/bike_other_repairs.png">Other Repairs</option>
         <?php } ?>
        </select>
	</div>
	</div>
               <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                        <label style="position: static"><i class="icon-calendar-7 "></i>Date of Service</label>
                        <input id="dos_book" class="date-pick form-control" data-error="Select a date" data-date-format="M d, D" type="text" name="dos_book">
                    </div>                                              
                </div>
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <label style="position: static">Pick up </label>
                    <select class="form-control" onchange="showDiv(this)" name="pick_up" id="pick_up">
                      <option value="1" selected>Yes</option>
                      <option value="0" selected>No</option>
                    </select>
                 </div>
                </div>
           <div id="click" style="display:none;">
               <div class="col-md-12 col-sm-12">                  
                    <label style="position: static"><i class=" icon-clock"></i>Pick Up Time</label>
                    <select class="form-control" id="pickup_time" name="pickup_time" required>
                    <option selected hidden value="0">Pick up Time</option>
                    <option value="09.00 AM - 11.00 AM">09.00 AM - 11.00 AM</option>
                    <option value="11.00 AM - 01.00 PM">11.00 AM - 01.00 PM</option>
                    <option value="01.00 PM - 03.00 PM">01.00 PM - 03.00 PM</option>
                    <option value="03.00 PM - 05.00 PM">03.00 PM - 05.00 PM</option>
                    <option value="05.00 PM - 07.00 PM">05.00 PM - 07.00 PM</option>
                    <option value="07.00 PM - 09.00 PM">07.00 PM - 09.00 PM</option>
                    </select>
                </div>
               <div class="col-md-12 col-sm-12">                  
                 <div class="form-group">
                      <label>Address</label>
                      <textarea class="form-control" name="pick_up_address" id="pick_up_address" placeholder="Enter Pickup Address"></textarea>
                </div>
                </div>
      
                    </div>

            <br>
            <center><button type="submit" class="btn_full" id="1st_submit"> Book Now </button></center>
            </div>
</form>
<form role="form" id="2nd_book" name="2nd_book" method="post" action="">
            <div class="row" id="2nd_booking_form" style="display:none;">
               <div class="col-md-12 col-sm-12" style="top:10px;">
                 <div class="form-group"> 
                      <label>Registration Number</label>
                      <input class="form-control" type="text" name="registration_num_book" id="registration_num_book" data-minlength="6" maxlength="13" required data-error="Enter Valid registration number" style="text-transform:uppercase" placeholder="tn/07/ar/6401">
                </div>
               </div>
               <div class="col-md-12 col-sm-12">
                 <div class="form-group">  
                      <label>Name</label>
                      <input class="form-control" type="text" name="name_book" id="name_book" pattern="^[a-Z\s]{3,}$"oninvalid="this.setCustomValidity('Enter atleast 3 alphabets')" 
    onchange="try{setCustomValidity('')}catch(e){}" required placeholder="Enter Name">
                </div>
               </div>
               <div class="col-md-12 col-sm-12">
                 <div class="form-group">  
                      <label>Mobile Number</label>
                      <input class="form-control" type="phone" name="mobile_num_book" id="mobile_num_book" pattern="^[789]\d{9}$" oninvalid="this.setCustomValidity('Enter valid phone number')" 
    onchange="try{setCustomValidity('')}catch(e){}"  required placeholder="Enter Mobile Number">
                </div>
               </div>
               <div class="col-md-12 col-sm-12">
                 <div class="form-group"> 
                      <label>Email</label>
                      <input class="form-control" type="email" name="email_book" id="email_book" required placeholder="Enter Email Id">
                </div>
               </div>
        <center><button class="btn_full" id="2nd_submit" type="submit" > Book Now </button></center>
            </div> <!--/box_style_1 --> 
</form>
<div class="row" id="thankyou" style="display:none">
<div  class="col-md-12" style="margin-top:25%; margin-bottom:25%;">
<h4> Thank you for sharing your details with us, We will get into touch with you soon! </h4>
</div>
</div>
</div>
</div>
<div class="modal fade" id="myReview" tabindex="-1" role="dialog" aria-labelledby="myReviewLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myReviewLabel">Enter Brand and Model</h4>
			</div>
			<div class="modal-body">
				<div id="message-review">
				</div>
<form method="get" accept-charset="utf-8" role="form" id="SearchForm" action="search.php" class="navbar-form">
<div class="col-md-6">
 <div class="form-group">
        <div class="form-inline header-form">
        <div class="ui-widget">
          <input id="BrandModel_pop_up" type="search" name="BrandModel" class="ui-autocomplete form-control" placeholder="Brand and Model" Value="<?php if(isset($_GET['BrandModel'])) echo $BrandModel; ?>"><span id="message"></span>
        </div>
        </div>
 </div>
</div>
<input type="hidden" value="<?php echo $_GET['VehicleType'];?>" id="VehicleType" name="VehicleType">
<input type="hidden" value="<?php echo $ServiceType;?>" id="ServiceType" name="ServiceType">
<input type="hidden" value="<?php echo $_GET['locality'];?>" id="locality" name="locality">
<div class="col-md-6">
 <div class="form-group" align="center">
    <button type="submit" class="btn_1">
    <i class="icon-search"></i>
    </button>
 </div>
</div>
</form>
			</div>
		</div>
	</div>
</div><!-- End modal review -->

    
<?php   
//}
if(empty($variant) && !empty($BrandModel) && $ServiceType == "General Service" && $vehicle_type == '4w') {
$dets = array_values($sorted_array);
	foreach($dets as $count_val) {
		if ($count_val['authorized_status']==='1') {
			$count_array_auth++;
		}
		if ($count_val['authorized_status']==='0') {
			$count_array_pri++;
		}
		if ($count_val['pickup_available']==='1') {
			 $pickup_yes++;
		}
		if ($count_val['variant']==='diesel') {
			$diesel_count++;
		}
		if ($count_val['variant']==='petrol') {
			$petrol_count++;
		}
	}
}
else {
//check the number of auth and multi branded
$dets = array_values($details);
foreach($dets as $count_val) {
	if ($count_val['authorized_status']==='1') {
		$count_array_auth++;
	}
	if ($count_val['authorized_status']==='0') {
		$count_array_pri++;
	}
	if ($count_val['pickup_available']==='1') {
		 $pickup_yes++;
	}
	if ($count_val['variant']==='diesel') {
		$diesel_count++;
	}
	if ($count_val['variant']==='petrol') {
		$petrol_count++;
	}
}
}//else

if($count_array_pri =='' || $count_array_pri==NULL)
	$count_array_pri='0';					
if($count_array_auth =='' || $count_array_auth==NULL)
	$count_array_auth='0';		
				
if($pickup_yes =='' || $pickup_yes==NULL)
	$pickup_yes='0';	
$pickup_no = sizeof($sorted_array);					
if($pickup_no =='' || $pickup_no==NULL)
	$pickup_no='0';		
				
if($petrol_count ==='' || $petrol_count===NULL)
	$petrol_count='0';					
if($diesel_count ==='' || $diesel_count===NULL)
	$diesel_count='0';			
				?>
  <script src="/gobumpr-test/js/jquery.ddslick.js"></script>
       
<script>
$("select.ddslick").each(function(){
            $(this).ddslick({
                showSelectedHTML: true,
			});
});
</script>      
<script> 
$(document).ready(function(){
	var count_array_pri = <?php echo json_encode($count_array_pri); ?>;
			jQuery("label[for='private_count']").html(count_array_pri);

	var count_array_auth = <?php echo json_encode($count_array_auth); ?>;
			jQuery("label[for='auth_count']").html(count_array_auth);
			
	var pickup_yes = <?php echo json_encode($pickup_yes); ?>;
			jQuery("label[for='pickup_yes']").html(pickup_yes);

	var search_results_count = <?php echo json_encode($search_results_count); ?>;
			jQuery("label[for='search_results_count']").html(search_results_count);

			
	var petrol_count = <?php echo json_encode($petrol_count); ?>;
			
	var diesel_count = <?php echo json_encode($diesel_count); ?>;
		
if( count_array_pri === '0' || count_array_pri==='' || count_array_pri===null)
	$("input[name=multi_branded]").attr("disabled", true);
if(count_array_auth === '0' || count_array_auth==='' || count_array_auth===null)
    $("input[name=authorised]").attr("disabled", true);
	
if(pickup_yes === '0' || pickup_yes==='' || pickup_yes===null)
    $("input[name=pick_up]#checkbox-3").attr("disabled", true);
	
if(diesel_count === '0' || diesel_count==='' || diesel_count===null)
    $("input[name=variant]#radio-4").attr("disabled", true);
if(petrol_count === '0' || petrol_count==='' || petrol_count===null)
    $("input[name=variant]#radio-3").attr("disabled", true);

});

</script>                
<script>
function focussing(){
    $('#BrandModel').focus();
}
</script>
	<link rel="stylesheet" href="/gobumpr-test/css/autocomplete.css">


<script>
$(function() {
		    $( "#locality" ).autocomplete({

source: function(request, response) {
            $.ajax({
                url: "locality.php",
                data: {
                    term: request.term
                },
                dataType: 'json'
            }).done(function(data) {
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
        delay: 2,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});
$(function() {
		    $( "#localityid" ).autocomplete({

source: function(request, response) {
            $.ajax({
                url: "locality.php",
                data: {
                    term: request.term
                },
                dataType: 'json'
            }).done(function(data) {
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
        delay: 2,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});
</script>
<script>
$(function() {
	//alert($("input[name=VehicleType]").val());
		    $( "#BrandModel" ).autocomplete({

source: function(request, response) {
            $.ajax({
                url: "getBrands.php",
                data: {
                    term: request.term,
					extraParams:$("input[name=VehicleType]").val()
                },
                dataType: 'json'
            }).done(function(data) {
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
        delay: 2,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});
$(function() {
	//alert($("input[name=VehicleType]").val());
		    $( "#BrandModel_pop_up" ).autocomplete({

source: function(request, response) {
            $.ajax({
                url: "getBrands.php",
                data: {
                    term: request.term,
					extraParams:$("input[name=VehicleType]").val()
                },
                dataType: 'json'
            }).done(function(data) {
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
        delay: 2,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
                        $(".ui-autocomplete").css("position", "absolute");
                        $(".ui-autocomplete").css("z-index", "2147483647");
                    },
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});
</script>
<script>
$(function() {
		    $( "#BrandModelid" ).autocomplete({

source: function(request, response) {
            $.ajax({
                url: "getBrands.php",
                data: {
                    term: request.term,
					extraParams:$('#VehicleType_hidden').val()
                },
                dataType: 'json'
            }).done(function(data) {
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
        delay: 2,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script>

 <!-- Date and time pickers -->
 <script src="/gobumpr-test/js/bootstrap-datepicker.js"></script>
 <script>
 var date = new Date();
date.setDate(date.getDate());

$('.date-pick').datepicker({ 
    startDate: date
});
 $('input.date-pick').datepicker('setDate', 'today');
   </script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>


    <script type="text/javascript" src="/gobumpr-test/js/inputmask/inputmask.js"></script>
	<script type="text/javascript" src="/gobumpr-test/js/inputmask/inputmask.extensions.js"></script>
	<script type="text/javascript" src="/gobumpr-test/js/inputmask/jquery.inputmask.js"></script>
	<script type="text/javascript" src="/gobumpr-test/null_handler/null_results.js"></script>
    <link href="/gobumpr-test/css/date_time_picker.css" rel="stylesheet">

<script>
$(document).ready(function(){
  $('#registration_num_book').inputmask("aa/9{1,2}/a{1,2}/9{1,4}"); //mask with dynamic syntax
});
</script><!-- Masking for registration number -->

<script type="text/javascript">
 function showDiv(elem){ 
    if(elem.value == 1){ 
        document.getElementById('click').style.display = "block";
}
else{
    document.getElementById('click').style.display = "none";
}
     }
</script>    
<script>
$(window).scroll(function(){
    'use strict';
        $('header').removeClass("sticky");
});
</script>
<script src="js/functions.js"></script>
</body>
</html>
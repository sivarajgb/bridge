﻿<?php
include 'config.php';
include 'service_types.php';

function parse_path() {
  $path = array();
  if (isset($_SERVER['REQUEST_URI'])) {
    $request_path = explode('?', $_SERVER['REQUEST_URI']);

    $path['base'] = rtrim(dirname($_SERVER['SCRIPT_NAME']), '\/');
    $path['call_utf8'] = substr(urldecode($request_path[0]), strlen($path['base']) + 1);
    $path['call'] = utf8_decode($path['call_utf8']);
    $path['call_parts'] = explode('/', $path['call']);

  }
return $path;
}
$city_name = $path_info['call_parts'][0];
$url_val = $path_info['call_parts'][1];
//header('Location: https://gobumpr.com/'.echo $city_name.'/'. echo $url_val);

//echo "chennai page......";
$path_info = parse_path();
//echo '<pre>'.print_r($path_info, true).'</pre>';


 $city = str_replace(".php", "", $path_info['call_parts'][0]);
//echo $path_info['call_parts'][0];
$path_url = $path_info['call_parts'][1];

$path=$path_info['call_parts'][2];
if($path_info['call_parts'][1]=='service-center'){
	header("Location: https://gobumpr.com/chennai/service-center.php?path=$path");
	exit;
}

elseif($path_info['call_parts'][1]=='thankyou'){
	header("Location: https://gobumpr.com/chennai/thankyou.php");
	exit(1);
}
$params_url = explode("-", $path_info['call_parts'][1]);
//print_r($params_url);

if(in_array("car", $params_url)){
	$vehicle_type = "4w";
	$index = array_search("car",$params_url);
	//echo '<br>';
	$left_array = array_slice($params_url, 0, $index);
	//print_r($left_array);
	//echo '<br>';

	end($params_url);
	$key = key($params_url);
	$right_array = array_slice($params_url, $index+1, $key);
	//print_r($right_array);
	//echo '<br>';
}//if car 
elseif(in_array("bike", $params_url)){
	$vehicle_type = "2w";
	$index = array_search("bike",$params_url);
	//echo '<br>';
	$left_array = array_slice($params_url, 0, $index);
	//print_r($left_array);
	//echo '<br>';

	end($params_url);
	$key = key($params_url);
	$right_array = array_slice($params_url, $index+1, $key);
	//print_r($right_array);
	//echo '<br>';
}//if bike

// =============================================== For right array to get service type and locality using "in"==========
if(in_array("in", $right_array)){
	$index = array_search("in",$right_array);
	//echo '<br>';
	$left_array2 = array_slice($right_array, 0, $index);
	//print_r($left_array2);
	//echo '<br>';
	
	end($right_array);
	$key = key($right_array);
	$right_array2 = array_slice($right_array, $index+1, $key);
	//print_r($right_array2);
	//echo '<br>';

}
else {
	$left_array2 = $right_array	;
}





if($vehicle_type == '4w') {
if(count($left_array)>0) {
if(count($left_array)==1) {
	
$brand_array = join(" ",$left_array); 
$query = "SELECT DISTINCT brand FROM admin_gs_vehicle_table WHERE admin_gs_vehicle_table.brand='$brand_array'";

//echo '<br><br>'.$query.'<br><br>';
$sql_res = mysqli_query($con, $query) or die("error".mysqli_error($con));
	
while ($row = mysqli_fetch_array($sql_res)) {
	$brand_table = $row['brand'];
	//echo '<br>';
}
//print_r($brand_table);

}//ONLY ONE WORD IS PRESENT AND THAT MUST BE BRAND NAME

elseif(count($left_array)==2) {
		$left_array_brand = array_slice($left_array, 0, 2);
	$left_array_model = array_slice($left_array, 1, count($left_array));
	$model_array = join("|",$left_array_model); 
	$brand_model = join("_", $left_array);
$brand_array = join(" ",$left_array); 
$query = "SELECT brand FROM admin_gs_vehicle_table WHERE admin_gs_vehicle_table.brand = ('".$brand_array."')";

//echo '<br><br>'.$query.'<br><br>';
$sql_res = mysqli_query($con, $query) or die("error".mysqli_error($con));
while($res=mysqli_fetch_object($sql_res)) {
		 $brand_table = strtolower($res->brand);
}
if(count($res)==0){
	$brand_array = join("|",$left_array_brand); 
$query = "SELECT brand, model FROM admin_gs_vehicle_table WHERE admin_gs_vehicle_table.brand REGEXP ('".$brand_array."') AND admin_gs_vehicle_table.model LIKE ('".$model_array."')";

//echo '<br><br>'.$query.'<br><br>';
$sql_res = mysqli_query($con, $query) or die("error".mysqli_error($con));

	while($res=mysqli_fetch_object($sql_res))
	 $brand_from_query = strtolower($res->brand);
	if(in_array("$brand_from_query", $left_array))
		 $index = array_search("$brand_from_query",$left_array);

	if(($left_array[$index+1])!=NULL) {
	$query = "SELECT brand,model FROM admin_gs_vehicle_table WHERE admin_gs_vehicle_table.brand_model= '$brand_model'";
	//echo '<br><br>'.$query.'<br><br>';
	$sql_res = mysqli_query($con, $query) or die("error".mysqli_error($con));
	
		while($res=mysqli_fetch_object($sql_res)){
		 $brand_table = strtolower($res->brand);
		 $model_table = strtolower($res->model);
		}
		$Brand_Model_table = $brand_table.' '.$model_table;
	}
}


}//if atleast, 2 WORDs are PRESENT 

elseif(count($left_array)>=3) {
		$left_array_brand = array_slice($left_array, 0, 2);
	$left_array_model = array_slice($left_array, 1, count($left_array));
	$brand_array = join("|",$left_array_brand); 
	$model_array = join("|",$left_array_model); 
	$brand_model = join("_", $left_array);

$query = "SELECT brand,model FROM admin_gs_vehicle_table WHERE admin_gs_vehicle_table.brand_model= '$brand_model'";


//echo '<br><br>'.$query.'<br><br>';
$sql_res = mysqli_query($con, $query) or die("error".mysqli_error($con));
	
while ($row = mysqli_fetch_object($sql_res)) {
	$brand_table = strtolower($row->brand);
	$model_table = strtolower($row->model);
}
$Brand_Model_table = $brand_table.' '.$model_table;

}// if more than 3 are present, it must contain model name too


//	echo $brand_table;
	//echo $model_table;
}//if brand model are given
}// if it is a car=====================================================================================

elseif($vehicle_type == '2w') {
if(count($left_array)>0) {
if(count($left_array)==1) {
	
$brand_array = join(" ",$left_array); 
$query = "SELECT brand, model FROM `admin_vehicle_table_new` WHERE `admin_vehicle_table_new`.type='".$vehicle_type."' AND `admin_vehicle_table_new`.brand='$brand_array'";

//echo '<br><br>'.$query.'<br><br>';
$sql_res = mysqli_query($con, $query) or die("error".mysqli_error($con));
	
while ($row = mysqli_fetch_assoc($sql_res)) {
	 $brand_table = $row['brand'];
}


}// THE ONLY WORD MUST BE A BRAND

elseif(count($left_array)==2) {
	$left_array_brand = array_slice($left_array, 0, 2);
	$left_array_model = array_slice($left_array, 1, count($left_array));
$model_array = join("|",$left_array_model); 

$brand_array = join(" ",$left_array); 
$query = "SELECT brand FROM `admin_vehicle_table_new` WHERE `admin_vehicle_table_new`.type='".$vehicle_type."' AND `admin_vehicle_table_new`.brand = ('".$brand_array."')";
//echo '<br>'.$query.'<br>';

$sql_res = mysqli_query($con, $query) or die("error".mysqli_error($con));
$res=mysqli_fetch_row($sql_res);

	
	while($res=mysqli_fetch_object($sql_res))
	 	$brand_table = strtolower($res->brand);
	

	if(count($res)==0) {//includes model name too
	$brand_array = join("|",$left_array_brand); 
	$query = "SELECT brand FROM `admin_vehicle_table_new` WHERE `admin_vehicle_table_new`.type='".$vehicle_type."' AND `admin_vehicle_table_new`.brand REGEXP ('".$brand_array."') AND `admin_vehicle_table_new`.model LIKE ('$model_array')";
	//echo $query;
	$sql_res = mysqli_query($con, $query) or die("error".mysqli_error($con));
	
	while($res=mysqli_fetch_object($sql_res)) {
	 	$brand_from_query = strtolower($res->brand);
	}
	if(in_array("$brand_from_query", $left_array))
		 $index = array_search("$brand_from_query",$left_array);
	
		if(($left_array[$index+1])!=NULL) {//if 2nd place is not null, that must be brand
			$brand_model = join("_", $left_array);
			$query = "SELECT brand, model FROM `admin_vehicle_table_new` WHERE `admin_vehicle_table_new`.`type`='".$vehicle_type."' AND `admin_vehicle_table_new`.`brand_model` = '$brand_model'";
		
		//echo '<br><br>'.$query.'<br><br>';
		$sql_res = mysqli_query($con, $query) or die("error".mysqli_error($con));
			
			while ($row = mysqli_fetch_assoc($sql_res)) {
				 $brand_table = strtolower($row['brand']);
				 $model_table = strtolower($row['model']);
			}
			$Brand_Model_table = $brand_table.' '.$model_table;
		}//2 words are brand and model
	}
} 

elseif(count($left_array)>=3) {
//mysqli_free_result($sql_res);
$brand_model = join("_", $left_array);
$queryq = "SELECT brand, model FROM admin_vehicle_table_new WHERE  admin_vehicle_table_new.type='".$vehicle_type."' AND admin_vehicle_table_new.brand_model = '$brand_model'";

//error_reporting(-1);
//ini_set('display_errors', 1);

//echo '<br><br>'.$queryq.'<br><br>';
$sql_resq = mysqli_query($con, $queryq) or die("error".mysqli_error($con));

//print_r($sql_resq);
while ($rowq = mysqli_fetch_array($sql_resq)) {
	$brand_table = $rowq['brand'];
	$model_table = $rowq['model'];
}


$Brand_Model_table = $brand_table.' '.$model_table;
}//IF atleast 3 WORDs are PRESENT IN THE ARRAY
}//if atleast one word is present
//echo $brand_table.'<br>';
//echo $model_table;
}// if it is a bike=====================================================================================

//echo $brand_table;
// joining the locality by spaces
$locality = join(" ", $right_array2);
//==========================================assigning service type 
$left_array2 = join(" ", $left_array2);
//echo '<br><br><br>';
$service_type = $left_array2;
//echo $path_url;
//echo $service_type;
if($toggle =='')
$toggle = 'sort_distance';

if(!empty($_GET)) {


$multi_branded = $_GET['multi_branded'];
$authorised = $_GET['authorised'];
$pick_up = $_GET['pick_up'];
$variant = $_GET['variant'];
$toggle = $_GET['sort_val'];
if($toggle =='')
	$toggle = 'sort_distance';
$multi_branded = $_GET['multi_branded'];
$authorised = $_GET['authorised'];
$pick_up = $_GET['pick_up']; 
$variant = $_GET['variant'];
$tube = $_GET['tube']; 
$at_shop = $_GET['at_shop'];
$wash = $_GET['wash'];

}



switch($service_type) {
	case 'services repair': 
							$params = array("city"=>"$city",
												"VehicleType"=>"$vehicle_type",
												"ServiceType"=>"$service_type",
												"BrandModel"=>"$Brand_Model_table",
												"brand"=>"$brand_table",
												"model"=>"$model_table",
												"variant"=>"$variant",
												"locality"=>"$locality",
												);
							check_url($params, $path_url);					
							
							$service_type = 'General Service';
							$params = array("city"=>"$city",
												"VehicleType"=>"$vehicle_type",
												"ServiceType"=>"$service_type",
												"BrandModel"=>"$Brand_Model_table",
												"brand"=>"$brand_table",
												"model"=>"$model_table",
												"variant"=>"$variant",
												"locality"=>"$locality",
												);
//print_r($params);
							
							$details = general_service($params);
							$ServiceType_model='services repair';
							break;

	case 'roadside breakdown assistance':  
							$params = array("city"=>"$city",
												"VehicleType"=>"$vehicle_type",
												"ServiceType"=>"$service_type",
												"BrandModel"=>"$Brand_Model_table",
												"brand"=>"$brand_table",
												"model"=>"$model_table",
												"locality"=>"$locality",
												);
							check_url($params, $path_url);					
							
								$service_type = 'Breakdown Assistance';
								
								$params = array("city"=>"$city",
													"VehicleType"=>"$vehicle_type",
													"ServiceType"=>"$service_type",
													"BrandModel"=>"$Brand_Model_table",
													"brand"=>"$brand_table",
													"model"=>"$model_table",
													"locality"=>"$locality",
													);
								$details = mechanic_breakdown_loc($params);
							$ServiceType_model='roadside breakdown assistance';
							break;

	case 'cleaning water wash services':$params = array("city"=>"$city",
												"VehicleType"=>"$vehicle_type",
												"ServiceType"=>"$service_type",
												"BrandModel"=>"$Brand_Model_table",
												"brand"=>"$brand_table",
												"model"=>"$model_table",
												"locality"=>"$locality",
												);
							check_url($params, $path_url);					

					$service_type = 'Water Wash';
								$params = array("city"=>"$city",
													"VehicleType"=>"$vehicle_type",
													"ServiceType"=>"$service_type",
													"BrandModel"=>"$Brand_Model_table",
													"brand"=>"$brand_table",
													"model"=>"$model_table",
													"locality"=>"$locality",
													);
								$details = water_wash($params);
							$ServiceType_model='cleaning water wash services';
							break;
					
	case 'tyre puncture repair shops': 
							$params = array("city"=>"$city",
												"VehicleType"=>"$vehicle_type",
												"ServiceType"=>"$service_type",
												"BrandModel"=>"$Brand_Model_table",
												"brand"=>"$brand_table",
												"model"=>"$model_table",
												"locality"=>"$locality",
												);
							check_url($params, $path_url);					
															
								$service_type = 'Tyre Puncture';
								$params = array("city"=>"$city",
													"VehicleType"=>"$vehicle_type",
													"ServiceType"=>"$service_type",
													"BrandModel"=>"$Brand_Model_table",
													"brand"=>"$brand_table",
													"model"=>"$model_table",
													"locality"=>"$locality",
													);
								$details = tyre_puncture($params);
							$ServiceType_model='tyre puncture repair shops';
							break;

	case 'other services':
							$params = array("city"=>"$city",
												"VehicleType"=>"$vehicle_type",
												"ServiceType"=>"$service_type",
												"BrandModel"=>"$Brand_Model_table",
												"brand"=>"$brand_table",
												"model"=>"$model_table",
												"locality"=>"$locality",
												);
							check_url($params, $path_url);					
							
								$service_type = 'Other Services';
								$params = array("city"=>"$city",
													"VehicleType"=>"$vehicle_type",
													"ServiceType"=>"$service_type",
													"BrandModel"=>"$Brand_Model_table",
													"brand"=>"$brand_table",
													"model"=>"$model_table",
													"locality"=>"$locality",
													);
								$details = other_services($params);
								break;

	case 'diagnostics inspection services':	
							$params = array("city"=>"$city",
												"VehicleType"=>"$vehicle_type",
												"ServiceType"=>"$service_type",
												"BrandModel"=>"$Brand_Model_table",
												"brand"=>"$brand_table",
												"model"=>"$model_table",
												"locality"=>"$locality",
												);
							check_url($params, $path_url);					
							
								$service_type = 'Vehicle Diagnostics';
								$params = array("city"=>"$city",
													"VehicleType"=>"$vehicle_type",
													"ServiceType"=>"$service_type",
													"BrandModel"=>"$Brand_Model_table",
													"brand"=>"$brand_table",
													"model"=>"$model_table",
													"locality"=>"$locality",
													);
								$details = diagnostics($params);
							$ServiceType_model='diagnostics inspection services';
							break;
							
	default: $params = array("city"=>"$city",
												"VehicleType"=>"$vehicle_type",
												"ServiceType"=>"$service_type",
												"BrandModel"=>"$Brand_Model_table",
												"brand"=>"$brand_table",
												"model"=>"$model_table",
												"locality"=>"$locality",
												);
							check_url($params, $path_url);					
							
				$service_type = 'General Service';
							$ServiceType_model='Repair Service Centers';
							break;
							
}
//==========================================assigning service type 


session_start();
$_SESSION['parameters'] = $params;



//echo $ServiceType_model;
function check_url($params, $path) {
$city = $params['city'];
$serviceType=$params['ServiceType'];
$VehicleType = $params['VehicleType'];
$BrandModel = $params['BrandModel'];
$locality = $params['locality'];
$brand = $params['brand'];
$model = $params['model'];

 $serviceType = str_replace(' ', '-', $serviceType);
 $brand = str_replace(' ', '-', $brand);
$model = str_replace(' ', '-', $model);
if($VehicleType == '4w') {
	if($locality!='') {
		$url = $brand.'-'.$model.'-'."car".'-'.$serviceType.'-'."in".'-'.$locality;
	if($model=='')
		$url = $brand.'-'."car".'-'.$serviceType.'-'."in".'-'.$locality;
	if($model==NULL && $brand==NULL)
		$url = "car".'-'.$serviceType.'-'."in".'-'.$locality;
	}
	else {
		$url = $brand.'-'.$model.'-'."car".'-'.$serviceType;
	if($model=='')
		$url = $brand.'-'."car".'-'.$serviceType;
	if($model=='' && $brand=='')
		$url = "car".'-'.$serviceType;
	}
}
if($VehicleType == '2w') {
	if($locality!='') {
		 $url = $brand.'-'.$model.'-'."bike".'-'.$serviceType.'-'."in".'-'.$locality;
	if($model=='')
		$url = $brand.'-'."bike".'-'.$serviceType.'-'."in".'-'.$locality;
	if($model==NULL && $brand==NULL)
		$url = "bike".'-'.$serviceType.'-'."in".'-'.$locality;
	}
	else {
		$url = $brand.'-'.$model.'-'."bike".'-'.$serviceType;
	if($model=='')
		 $url = $brand.'-'."bike".'-'.$serviceType;
	if($model=='' && $brand=='')
		$url = "bike".'-'.$serviceType;
	}
}
//echo '<br>'.$url;
if(strtolower($url) != strtolower($path)) {
	//header ("Location: https://gobumpr.com/chennai/$url");
	//exit();
}
}

//print_r($params);

if($vehicle_type=='4w' && $service_type =='Water Wash')
 $service_type = 'Car Wash';
function sentence_case($string) { 
    $sentences = preg_split('/([ -]+)/', $string, -1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE); 
    $new_string = ''; 
    foreach ($sentences as $key => $sentence) { 
        $new_string .= ($key & 1) == 0? 
            ucfirst(strtolower(trim($sentence))) : 
            $sentence.''; 
    } 
    return trim($new_string); 
} 

$VehicleType = $params['VehicleType'];
$ServiceType = sentence_case($params['ServiceType']);
$BrandModel = sentence_case($params['BrandModel']);
$brand = sentence_case($params['brand']);
$model = sentence_case($params['model']);
$locality = sentence_case($params['locality']);

if($toggle =='')
$toggle = 'sort_distance';

if(!empty($_GET)) {


$multi_branded = $_GET['multi_branded'];
$authorised = $_GET['authorised'];
$pick_up = $_GET['pick_up'];
$variant = $_GET['variant'];
$toggle = $_GET['sort_val'];
if($toggle =='')
	$toggle = 'sort_distance';
$multi_branded = $_GET['multi_branded'];
$authorised = $_GET['authorised'];
$pick_up = $_GET['pick_up']; 
$variant = $_GET['variant'];
$tube = $_GET['tube']; 
$at_shop = $_GET['at_shop'];
$wash = $_GET['wash'];

}

if($VehicleType=='4w')
	$VehicleType_model = 'car';
else
	$VehicleType_model = 'bike';

if($brand!=''&&$model!='')
$BrandModel = $brand." ".$model;

?>
<?php 


switch($toggle) {
case 'sort_price':
					if($params['ServiceType']=="Tyre Puncture") {
						$details = tyre_puncture($params);
					$details = array_values($details);
						foreach ($details as $key => $row) {
							$price[$key]  = $row['price_sort'];
							$distance[$key]  = $row['distance'];
						}
						array_multisort($price, SORT_ASC, $distance, SORT_ASC, $details);
					}//if
					else {
						foreach ($details as $key => $row) {
							$price[$key]  = $row['price'];
							$distance[$key]  = $row['distance'];
						}
						array_multisort($price, SORT_ASC, $distance, SORT_ASC, $details);
					}
					
					$sorted_array = array_values($details);
					
					if($pick_up == '1')
						$sorted_array = pick_up($sorted_array);
						
					if($private == '1' && $authorised == '1') 
						$sorted_array = $sorted_array;
					else {
						if($authorised == '1')  {
							//print_r($sorted_array);
							$sorted_array = est_type($sorted_array);
							//print_r($sorted_array);							
						}
						if($private == '1') { 
							$sorted_array = est_type_private($sorted_array);
							//print_r($sorted_array);							
						}
					}
					//print_r($sorted_array);
					
					break;
					
					
case 'sort_distance' :
						uasort($details, function ($i, $j) {
							$a = $i['distance'];
							$b = $j['distance'];
							if ($a == $b) return 0;
							elseif ($a > $b) return 1;
							else return -1;
						});
						$sorted_array = array_values($details);
					
						if($pick_up == '1')
							$sorted_array = pick_up($sorted_array);
					if($private == '1' && $authorised == '1') 
						$sorted_array = $sorted_array;
					else {
						if($authorised == '1') 
							$sorted_array = est_type($sorted_array);
						if($private == '1') 
							$sorted_array = est_type_private($sorted_array);
					}
						//print_r($sorted_array);
						
						
						break;
						
case 'sort_ratings':

						foreach ($details as $key => $row) {
							$rating[$key]  = $row['rating'];
							$distance[$key]  = $row['distance'];
						}
						array_multisort($rating, SORT_DESC, $distance, SORT_ASC, $details);
					$sorted_array = array_values($details);
					
					if($pick_up == '1')
						$sorted_array = pick_up($sorted_array);
					if($private == '1' && $authorised == '1') 
						$sorted_array = $sorted_array;
					else {
						if($authorised == '1') {
							$sorted_array = est_type($sorted_array);
						}
						if($private == '1') 
							$sorted_array = est_type_private($sorted_array);
					}
					//print_r($sorted_array);
					
					break;

}//switch


function pick_up($sorted_array) {
	foreach($sorted_array as $key => $value) {
		if($value['pickup_available'] === "0" || $value['pickup_available'] === "") {
			$keys = array();
			$i=$key;
			$keys[$i] = $key;
			//print_r($keys);
			$sorted_array[$i] = array_filter($sorted_array[$keys]);
			$sorted_array = array_filter($sorted_array);
			$i++;
		}
	}//foreach
	return $sorted_array;
}

$copy_details = array_values($details);
//print_r($copy_details);
function est_type($sorted_array) {
	foreach($sorted_array as $key => $value) {
		if($value['authorized_status'] === "0") {
			$keys = array();
			$i=$key;
			$keys[$i] = $key;
			//print_r($keys);
			$sorted_array[$i] = array_filter($sorted_array[$keys]);
			$sorted_array = array_filter($sorted_array);
			$i++;
		}
	}
	return $sorted_array;
}


function est_type_private($sorted_array) {
	foreach($sorted_array as $key => $value) {//taking opposite value to filter out the unnecessary values
		if($value['authorized_status'] == "1") { //null is same as private, 
			$keys = array();
			$i=$key;
			$keys[$i] = $key;
			//print_r($keys);
			$sorted_array[$i] = array_filter($sorted_array[$keys]);
			$sorted_array = array_filter($sorted_array);
			$i++;
		}
	}
	return $sorted_array;
}

if(empty($variant) && !empty($BrandModel) && $ServiceType == "General Service" && $VehicleType == '4w') {
	$arr = $sorted_array;
 $new_arr =array();
			foreach($arr as $key=>$row){
				if(isset($new_arr[$row['mec_id']])){
					if($new_arr[$row['mec_id']]!=$row['price'])
					$new_arr[$row['mec_id']]= (($new_arr[$row['mec_id']]<$row['price'])?($new_arr[$row['mec_id']].'-'.$row['price']) : ($row['price'].'-'.$new_arr[$row['mec_id']])) ;
					else
					$new_arr[$row['mec_id']]= $row['price'];
				}
				else{
					$new_arr[$row['mec_id']]=$row['price'];
				}
			}
	
			foreach($new_arr as $key=>$row){
				$pPrice[$key] =$row;
			}

//print_r($pPrice);
//print_r($sorted_array);
$myarrayyy= array_values($sorted_array);
$myarr= $myarrayyy;

$your_unique_array = array();
	foreach ($myarr as $v) {
		  if (!isset($your_unique_array[$v['mec_id']])) {
			  $your_unique_array[$v['mec_id']] = $v;
		  }
	}
//print_r($your_unique_array);

			foreach($your_unique_array as $key=>$value) {
						foreach($pPrice as $k=>$v) {
									if($key == $k) {
										$your_unique_array[$key]['price'] = $pPrice[$k];
									}
						}
			}
//print_r($your_unique_array);
$sorted_array = array_values($your_unique_array);
}//IF

//print_r($sorted_array);
//print_r($details);
 $search_results_count = count($sorted_array);


if(empty($variant) && !empty($BrandModel) && $ServiceType == "General Service" && $VehicleType == '4w') {
$dets = array_values($details);
	foreach($dets as $count_val) {
		if ($count_val['authorized_status']==='1') {
			$count_array_auth++;
		}
		if ($count_val['authorized_status']==='0') {
			$count_array_pri++;
		}
		if ($count_val['pickup_available']==='1') {
			 $pickup_yes++;
		}
		if ($count_val['variant']==='diesel') {
			$diesel_count++;
		}
		if ($count_val['variant']==='petrol') {
			$petrol_count++;
		}
	}
}
else {
//check the number of auth and multi branded
$dets = array_values($details);
foreach($dets as $count_val) {
	if ($count_val['authorized_status']==='1') {
		$count_array_auth++;
	}
	if ($count_val['authorized_status']==='0') {
		$count_array_pri++;
	}
	if ($count_val['pickup_available']==='1') {
		 $pickup_yes++;
	}
	if ($count_val['variant']==='diesel') {
		$diesel_count++;
	}
	if ($count_val['variant']==='petrol') {
		$petrol_count++;
	}
}
}//else

if($count_array_pri =='' || $count_array_pri==NULL)
	$count_array_pri='0';					
if($count_array_auth =='' || $count_array_auth==NULL)
	$count_array_auth='0';		
				
if($pickup_yes =='' || $pickup_yes==NULL)
	$pickup_yes='0';	
$pickup_no = sizeof($sorted_array);					
if($pickup_no =='' || $pickup_no==NULL)
	$pickup_no='0';		
				
if($petrol_count ==='' || $petrol_count===NULL)
	$petrol_count='0';					
if($diesel_count ==='' || $diesel_count===NULL)
	$diesel_count='0';			
				?>

<!DOCTYPE html>
<!--[if IE 8]><html class="ie ie8"> <![endif]-->
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Favicons-->
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">
    
    <?php 
	if($ServiceType=="General Service") {
		if($locality==''&&$model==''&&$brand=='') { 
			if($VehicleType=='4w') {?>
            <meta name="description" content="Find and Book Trusted Car Service Centers in Chennai. Choose from OEM Authorized, Multi Brand, Bosch, Car Nation, MyTVS, MFCS and Top Rated Service Centers">
            <title>Book Car Service at Best Car Service Centers in Chennai</title>
            <!-- <meta name="keywords" content="car service, car repair, car service in chennai, car repair in chennai, best car service centers in chennai, doorstep car service in chennai, car service centers chennai, car service centers in chennai"> -->
            <meta property="og:description" content="Find and Book Trusted Car Service Centers in Chennai. Choose from OEM Authorized, Multi Brand, Bosch, Car Nation, MyTVS, MFCS and Top Rated Service Centers">
		    <meta property="og:title" content="Book Car Service at Best Car Service Centers in Chennai">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model))?>">            
			<?php }//if bike
			elseif($VehicleType=='2w') {?>
            <meta name="description" content="Find and Book Trusted Bike Service Centers in Chennai. Choose from OEM Authorized Service Centers, Multi Brand Workshops and Top Rated Local Mechanics">
            <title>Book a Service at Best Bike Service Centers in Chennai</title>
            <!-- <meta name="keywords" content="bike service, scooter service, bike repair, bike service in chennai, bike repair in chennai, best bike service centers in chennai, doorstep bike service in chennai, bike service centers in chennai"> -->
            <meta property="og:description" content="Find and Book Trusted Bike Service Centers in Chennai. Choose from OEM Authorized Service Centers, Multi Brand Workshops and Top Rated Local Mechanics">
		    <meta property="og:title" content="Book a Service at Best Bike Service Centers in Chennai">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model))?>">            
			<?php }//if bike
		 }//nothing 
		 elseif($locality!=''&&$model==''&&$brand=='') { 
		 if($VehicleType=='4w') {?>
            <meta name="description" content="Find and Book Trusted Car Service Centers in <?php echo $locality; ?>. Choose from OEM Authorized, Multi Brand, Bosch, Car Nation, MyTVS and Top Rated Service Centers">
            <title>Best Car Service Centers in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="car service in <?php echo $locality; ?>, car repair in <?php echo $locality; ?>, best car service centers in <?php echo $locality; ?>, doorstep car service in <?php echo $locality; ?>, car service centers <?php echo $locality; ?>, car service centers in <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find and Book Trusted Car Service Centers in <?php echo $locality; ?>. Choose from OEM Authorized, Multi Brand, Bosch, Car Nation, MyTVS and Top Rated Service Centers">
		    <meta property="og:title" content="Best Car Service Centers in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
		<?php }//Car 
		 if($VehicleType=='2w') {?>
            <meta name="description" content="Find and Book Trusted Bike Service Centers in <?php echo $locality; ?>. Choose from OEM Authorized Service Centers, Multi Brand Workshops and Top Rated Local Mechanics">
            <title>Best Bike Service Centers in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="bike service in <?php echo $locality; ?>, bike repair in <?php echo $locality; ?>, best bike service centers in <?php echo $locality; ?>, doorstep bike service in <?php echo $locality; ?>, bike service centers in <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find and Book Trusted Bike Service Centers in <?php echo $locality; ?>. Choose from OEM Authorized Service Centers, Multi Brand Workshops and Top Rated Local Mechanics">
		    <meta property="og:title" content="Best Bike Service Centers in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
		<?php }//bike
		 }//only locality, no brand and model 
		 elseif($brand!=''&&$locality==''&&$model=='') { 
			 if($VehicleType=='4w') {?>
			<meta name="description" content="Find and Book Trusted <?php echo $brand; ?> Car Service Centers in Chennai. Choose from OEM Authorized, Multi Brand, Bosch, MyTVS and Top Rated Service Centers">
            <title>Best <?php echo $brand; ?> Car Service Centers in Chennai</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> car service, <?php echo $brand; ?> car repair, <?php echo $brand; ?> car service in chennai, <?php echo $brand; ?> car repair in chennai, best <?php echo $brand; ?> car service centers in chennai, doorstep <?php echo $brand; ?> car service in chennai, <?php echo $brand; ?> car service centers chennai, <?php echo $brand; ?> car service centers in chennai"> -->
            <meta property="og:description" content="Find and Book Trusted <?php echo $brand; ?> Car Service Centers in Chennai. Choose from OEM Authorized, Multi Brand, Bosch, MyTVS and Top Rated Service Centers">
		    <meta property="og:title" content="Best <?php echo $brand; ?> Car Service Centers in Chennai">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
		<?php
			 }//car
			 if($VehicleType=='2w') {?>
			<meta name="description" content="Find and Book Trusted <?php echo $brand; ?> Bike Service Centers in Chennai. Choose from OEM Authorized Service Centers, Multi Brand Workshops and Top Rated Local Mechanics">
            <title>Best <?php echo $brand; ?> Bike Service Centers in Chennai</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> bike service, <?php echo $brand; ?> bike repair, <?php echo $brand; ?> bike service in chennai, <?php echo $brand; ?> bike repair in chennai, best <?php echo $brand; ?> bike service centers in chennai, doorstep <?php echo $brand; ?> bike service in chennai, <?php echo $brand; ?> bike service centers in chennai"> -->
            <meta property="og:description" content="Find and Book Trusted <?php echo $brand; ?> Bike Service Centers in Chennai. Choose from OEM Authorized Service Centers, Multi Brand Workshops and Top Rated Local Mechanics">
		    <meta property="og:title" content="Best <?php echo $brand; ?> Bike Service Centers in Chennai">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
			<?php
			 }//bike
		 }//only brand
		 elseif($brand!=''&&$model!=''&&$locality=='') { 
			 if($VehicleType=='4w') {?>
			<meta name="description" content="Find and Book Trusted <?php echo $brand; ?> <?php echo $model; ?> Service Centers in Chennai. Choose from OEM Authorized, Multi Brand, Bosch, MyTVS and Top Rated Service Centers">
            <title>Best <?php echo $brand; ?> <?php echo $model; ?> Service Centers in Chennai</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> <?php echo $model; ?> car service, <?php echo $brand; ?> <?php echo $model; ?> car repair, <?php echo $brand; ?> <?php echo $model; ?> car service in chennai, <?php echo $brand; ?> <?php echo $model; ?> car repair in chennai, best <?php echo $brand; ?> <?php echo $model; ?> car service centers in chennai, doorstep <?php echo $brand; ?> <?php echo $model; ?> car service in chennai, <?php echo $brand; ?> <?php echo $model; ?> car service centers chennai, <?php echo $brand; ?> <?php echo $model; ?> car service centers in chennai"> -->
            <meta property="og:description" content="Find and Book Trusted <?php echo $brand; ?> <?php echo $model; ?> Service Centers in Chennai. Choose from OEM Authorized, Multi Brand, Bosch, MyTVS and Top Rated Service Centers">
		    <meta property="og:title" content="Best <?php echo $brand; ?> <?php echo $model; ?> Service Centers in Chennai">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
			<?php 
			 }//car
			 if($VehicleType=='2w') {?>
			<meta name="description" content="Find and Book Trusted <?php echo $brand.' '.$model;?> Service Centers in Chennai. Choose from OEM Authorized Service Centers, Multi Brand Workshops and Top Rated Local Mechanics">
            <title>Best <?php echo $brand; ?> <?php echo $model; ?> Service Centers in Chennai</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> <?php echo $model; ?> bike service, <?php echo $brand; ?> <?php echo $model; ?> bike repair, <?php echo $brand; ?> <?php echo $model; ?> bike service in chennai, <?php echo $brand; ?> <?php echo $model; ?> bike repair in chennai, best <?php echo $brand; ?> <?php echo $model; ?> bike service centers in chennai, doorstep <?php echo $brand; ?> <?php echo $model; ?> bike service in chennai, <?php echo $brand; ?> <?php echo $model; ?> bike service centers in chennai"> -->
            <meta property="og:description" content="Find and Book Trusted <?php echo $brand.' '.$model;?> Service Centers in Chennai. Choose from OEM Authorized Service Centers, Multi Brand Workshops and Top Rated Local Mechanics">
		    <meta property="og:title" content="Best <?php echo $brand; ?> <?php echo $model; ?> Service Centers in Chennai">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php 
			 }//bike
		}//no locality and brand and model
		 elseif($brand!=''&&$model!=''&&$locality!='') { 
			 if($VehicleType=='4w') {?>
			<meta name="description" content="Find and Book Trusted <?php echo $brand; ?> <?php echo $model; ?> Service Centers in <?php echo $locality; ?>. Choose from OEM Authorized, Multi Brand, Bosch, MyTVS and Top Rated Service Centers">
            <title>Best <?php echo $brand; ?> <?php echo $model; ?> Service Centers in <?php echo $locality; ?></title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> <?php echo $model; ?> car service, <?php echo $brand; ?> <?php echo $model; ?> car repair, <?php echo $brand; ?> <?php echo $model; ?> car service in <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> car repair in <?php echo $locality; ?>, best <?php echo $brand; ?> <?php echo $model; ?> car service centers in <?php echo $locality; ?>, doorstep <?php echo $brand; ?> <?php echo $model; ?> car service in <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> car service centers <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> car service centers in <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find and Book Trusted <?php echo $brand; ?> <?php echo $model; ?> Service Centers in <?php echo $locality; ?>. Choose from OEM Authorized, Multi Brand, Bosch, MyTVS and Top Rated Service Centers">
		    <meta property="og:title" content="Best <?php echo $brand; ?> <?php echo $model; ?> Service Centers in <?php echo $locality; ?>">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
			<?php 
			 }//car
			 elseif($VehicleType=='2w') {?>
			<meta name="description" content="Find and Book Trusted <?php echo $brand;?> <?php echo $model;?> Service Centers in <?php echo $locality;?>. Choose from OEM Authorized Service Centers, Multi Brand Workshops and Top Rated Local Mechanics">
            <title>Best <?php echo $brand; ?> <?php echo $model; ?> Service Centers in <?php echo $locality; ?></title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> <?php echo $model; ?> bike service, <?php echo $brand; ?> <?php echo $model; ?> bike repair, <?php echo $brand; ?> <?php echo $model; ?> bike service in <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> bike repair in <?php echo $locality; ?>, best <?php echo $brand; ?> <?php echo $model; ?> bike service centers in <?php echo $locality; ?>, doorstep <?php echo $brand; ?> <?php echo $model; ?> bike service in <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> bike service centers in <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find and Book Trusted <?php echo $brand;?> <?php echo $model;?> Service Centers in <?php echo $locality;?>. Choose from OEM Authorized Service Centers, Multi Brand Workshops and Top Rated Local Mechanics">
		    <meta property="og:title" content="Best <?php echo $brand; ?> <?php echo $model; ?> Service Centers in <?php echo $locality; ?>">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php 
			 }//bike
		}//loc, brand, model
		 elseif($brand!=''&&$model==''&&$locality!='') { 
			 if($VehicleType=='4w') {?>
			<meta name="description" content="Find and Book Trusted <?php echo $brand;?> Service Centers in <?php echo $locality; ?>. Choose from OEM Authorized, Multi Brand, Bosch, MyTVS and Top Rated Service Centers">
            <title>Best <?php echo $brand; ?> Car Service Centers in <?php echo $locality; ?></title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> car service, <?php echo $brand; ?> car repair, <?php echo $brand; ?> car service in <?php echo $locality; ?>, <?php echo $brand; ?> car repair in <?php echo $locality; ?>, best <?php echo $brand; ?> car service centers in <?php echo $locality; ?>, doorstep <?php echo $brand; ?> car service in <?php echo $locality; ?>, <?php echo $brand; ?> car service centers <?php echo $locality; ?>, <?php echo $brand; ?> car service centers in <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find and Book Trusted <?php echo $brand;?> Service Centers in <?php echo $locality; ?>. Choose from OEM Authorized, Multi Brand, Bosch, MyTVS and Top Rated Service Centers">
		    <meta property="og:title" content="Best <?php echo $brand; ?> Car Service Centers in <?php echo $locality; ?>">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php 
			 }//car
			 elseif($VehicleType=='2w') {?>
			<meta name="description" content="Find and Book Trusted <?php echo $brand;?> Service Centers in <?php echo $locality; ?>. Choose from OEM Authorized Service Centers, Multi Brand Workshops and Top Rated Local Mechanics">
            <title>Best <?php echo $brand; ?> Bike Service Centers in <?php echo $locality; ?></title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> bike service, <?php echo $brand; ?> bike repair, <?php echo $brand; ?> bike service in <?php echo $locality; ?>, <?php echo $brand; ?> bike repair in <?php echo $locality; ?>, best <?php echo $brand; ?> bike service centers in <?php echo $locality; ?>, doorstep <?php echo $brand; ?> bike service in <?php echo $locality; ?>, <?php echo $brand; ?> bike service centers in <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find and Book Trusted <?php echo $brand;?> Service Centers in <?php echo $locality; ?>. Choose from OEM Authorized Service Centers, Multi Brand Workshops and Top Rated Local Mechanics">
		    <meta property="og:title" content="Best <?php echo $brand; ?> Bike Service Centers in <?php echo $locality; ?>">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php 
			 }//bike
		}//locality and brand and  no model
	}//if general service
	
	elseif($ServiceType=="Water Wash" || $ServiceType=="Car Wash") {
		if($locality==''&&$model==''&&$brand=='') { 
			if($VehicleType=='4w') {?>
			<meta name="description" content="Find the Best Deal for Car Wash, Exterior Cleaning, Interior Detailing, Exterior Polishing and Complete Car Spa at Best Prices Near You in Chennai. Doorstep Service Available">
            <title>Car Wash, Detailing & Cleaning Services in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="car wash, car cleaning, doorstep car wash, doorstep car wash chennai, car cleaning chennai, car wash chennai, car interior detailing chennai, car polishing chennai, car spa chennai, car waxing chennai, car coating chennai"> -->
            <meta property="og:description" content="Find the Best Deal for Car Wash, Exterior Cleaning, Interior Detailing, Exterior Polishing and Complete Car Spa at Best Prices Near You in Chennai. Doorstep Service Available">
		    <meta property="og:title" content="Car Wash, Detailing & Cleaning Services in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model))?>">            
			<?php }//if bike
			if($VehicleType=='2w') {?>
            <meta name="description" content="Get your Bike Water Washed & Cleaned at the Best Price Near You in Chennai. Price Starts from Rs. 100. Doorstep Service Available. Book now">
            <title>Bike Wash, Detailing & Cleaning Services in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="doorstep bike wash, scooter wash, bike wash in chennai, doorstep bike wash chennai, bike cleaning chennai, bike wash chennai, bike detailing chennai, bike coating chennai"> -->
            <meta property="og:description" content="Get your Bike Water Washed & Cleaned at the Best Price Near You in Chennai. Price Starts from Rs. 100. Doorstep Service Available. Book now">
		    <meta property="og:title" content="Bike Wash, Detailing & Cleaning Services in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model))?>">            
			<?php }//if bike
		 }//ntg
		 elseif($locality!=''&&$model==''&&$brand=='') { 
			if($VehicleType=='4w') {?>
			<meta name="description" content="Find the Best Deal for Car Wash, Exterior Cleaning, Interior Detailing, Exterior Polishing and Complete Car Spa at Best Prices in <?php echo $locality;?>. Doorstep Service Available">
            <title>Car Wash, Detailing & Cleaning Services in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="doorstep car wash <?php echo $locality; ?>, car cleaning <?php echo $locality; ?>, car wash <?php echo $locality; ?>, car interior detailing <?php echo $locality; ?>, car polishing <?php echo $locality; ?>, car spa <?php echo $locality; ?>, car waxing <?php echo $locality; ?>, car coating <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find the Best Deal for Car Wash, Exterior Cleaning, Interior Detailing, Exterior Polishing and Complete Car Spa at Best Prices in <?php echo $locality;?>. Doorstep Service Available">
		    <meta property="og:title" content="Car Wash, Detailing & Cleaning Services in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php }//if car
			if($VehicleType=='2w') {?>
            <meta name="description" content="Get your Bike Water Washed & Cleaned at the Best Price in <?php echo $locality;?>. Price Starts from Rs. 100. Doorstep Service Available. Book now">
            <title>Bike Wash, Detailing & Cleaning Services in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="bike wash in <?php echo $locality; ?>, doorstep bike wash <?php echo $locality; ?>, bike cleaning <?php echo $locality; ?>, bike wash <?php echo $locality; ?>, bike detailing <?php echo $locality; ?>, bike coating <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Get your Bike Water Washed & Cleaned at the Best Price in <?php echo $locality;?>. Price Starts from Rs. 100. Doorstep Service Available. Book now">
		    <meta property="og:title" content="Bike Wash, Detailing & Cleaning Services in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php }//if bike
		 }//only locality
		 elseif($brand!=''&&$locality==''&&$model=='') { 
			if($VehicleType=='4w') {?>
			<meta name="description" content="Find the Best Deal for <?php echo $brand; ?> Car Wash, Exterior Cleaning, Interior Detailing, Exterior Polishing and Complete <?php echo $brand;?> Car Spa at Best Prices Near You in Chennai. Doorstep Service Available">
            <title><?php echo $brand; ?> Car Wash, Detailing & Cleaning Services in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> car wash, <?php echo $brand; ?> car cleaning, doorstep <?php echo $brand; ?> car wash, doorstep <?php echo $brand; ?> car wash chennai, <?php echo $brand; ?> car cleaning chennai, <?php echo $brand; ?> car wash chennai, <?php echo $brand; ?> car interior detailing chennai, <?php echo $brand; ?> car polishing chennai, <?php echo $brand; ?> car spa chennai, <?php echo $brand; ?> car waxing chennai, <?php echo $brand; ?> car coating chennai"> -->
            <meta property="og:description" content="Find the Best Deal for <?php echo $brand; ?> Car Wash, Exterior Cleaning, Interior Detailing, Exterior Polishing and Complete <?php echo $brand;?> Car Spa at Best Prices Near You in Chennai. Doorstep Service Available">
		    <meta property="og:title" content="<?php echo $brand; ?> Car Wash, Detailing & Cleaning Services in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
			<?php }//if bike
			if($VehicleType=='2w') {?>
            <meta name="description" content="Get your <?php echo $brand; ?> Bike Water Washed & Cleaned at the Best Price Near You in Chennai. Price Starts from Rs. 100. Doorstep Service Available. Book now">
            <title><?php echo $brand; ?> Bike Wash, Detailing & Cleaning Services in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="doorstep <?php echo $brand; ?> bike wash, <?php echo $brand; ?> bike wash in chennai, doorstep <?php echo $brand; ?> bike wash chennai, <?php echo $brand; ?> bike cleaning chennai, <?php echo $brand; ?> bike wash chennai, <?php echo $brand; ?> bike detailing chennai, <?php echo $brand; ?> bike coating chennai"> -->
            <meta property="og:description" content="Get your <?php echo $brand; ?> Bike Water Washed & Cleaned at the Best Price Near You in Chennai. Price Starts from Rs. 100. Doorstep Service Available. Book now">
		    <meta property="og:title" content="<?php echo $brand; ?> Bike Wash, Detailing & Cleaning Services in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
			<?php }//if bike
		 }//brand
		 elseif($brand!=''&&$model!=''&&$locality=='') { 
			if($VehicleType=='4w') {?>
			<meta name="description" content="Find the Best Deal for <?php echo $brand.' '.$model; ?> Water Wash, Exterior Cleaning, Interior Detailing, Exterior Polishing and Complete <?php echo $brand.' '.$model; ?> Spa at Best Prices Near You in Chennai. Doorstep Service Available">
            <title><?php echo $brand; ?> <?php echo $model; ?> Wash, Detailing & Cleaning Services in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> <?php echo $model; ?> car wash, <?php echo $brand; ?> <?php echo $model; ?> car cleaning, doorstep <?php echo $brand; ?> <?php echo $model; ?> car wash, doorstep <?php echo $brand; ?> <?php echo $model; ?> car wash chennai, <?php echo $brand; ?> <?php echo $model; ?> car cleaning chennai, <?php echo $brand; ?> <?php echo $model; ?> car wash chennai, <?php echo $brand; ?> <?php echo $model; ?> car interior detailing chennai, <?php echo $brand; ?> <?php echo $model; ?> car polishing chennai, <?php echo $brand; ?> <?php echo $model; ?> car spa chennai, <?php echo $brand; ?> <?php echo $model; ?> car waxing chennai, <?php echo $brand; ?> <?php echo $model; ?> car coating chennai"> -->
            <meta property="og:description" content="Find the Best Deal for <?php echo $brand.' '.$model; ?> Water Wash, Exterior Cleaning, Interior Detailing, Exterior Polishing and Complete <?php echo $brand.' '.$model; ?> Spa at Best Prices Near You in Chennai. Doorstep Service Available">
		    <meta property="og:title" content="<?php echo $brand; ?> <?php echo $model; ?> Wash, Detailing & Cleaning Services in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
			<?php }//if car
			if($VehicleType=='2w') {?>  
            <meta name="description" content="Get your <?php echo $brand.' '.$model; ?> Water Washed & Cleaned at the Best Price Near You in Chennai. Price Starts from Rs. 100. Doorstep Service Available. Book now">
            <title><?php echo $brand; ?> <?php echo $model; ?> Bike Wash, Detailing & Cleaning Services in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="doorstep <?php echo $brand; ?> <?php echo $model; ?> bike wash, <?php echo $brand; ?> <?php echo $model; ?> bike wash in chennai, doorstep <?php echo $brand; ?> <?php echo $model; ?> bike wash chennai, <?php echo $brand; ?> <?php echo $model; ?> bike cleaning chennai, <?php echo $brand; ?> <?php echo $model; ?> bike wash chennai, <?php echo $brand; ?> <?php echo $model; ?> bike detailing chennai, <?php echo $brand; ?> <?php echo $model; ?> bike coating chennai"> -->
            <meta property="og:description" content="Get your <?php echo $brand.' '.$model; ?> Water Washed & Cleaned at the Best Price Near You in Chennai. Price Starts from Rs. 100. Doorstep Service Available. Book now">
		    <meta property="og:title" content="<?php echo $brand; ?> <?php echo $model; ?> Bike Wash, Detailing & Cleaning Services in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
			<?php }//if bike
		 }//brand, model
		 elseif($brand!=''&&$model!=''&&$locality!='') { 
			if($VehicleType=='4w') {?>
			<meta name="description" content="Find the Best Deal for <?php echo $brand;?> <?php echo $model;?> Water Wash, Exterior Cleaning, Interior Detailing, Exterior Polishing and Complete Car Spa at Best Prices in <?php echo $locality;?>. Doorstep Service Available">
            <title><?php echo $brand; ?> <?php echo $model; ?> Wash, Detailing & Cleaning Services in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> <?php echo $model; ?> car wash, <?php echo $brand; ?> <?php echo $model; ?> car cleaning, doorstep <?php echo $brand; ?> <?php echo $model; ?> car wash, doorstep <?php echo $brand; ?> <?php echo $model; ?> car wash <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> car cleaning <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> car wash <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> car interior detailing <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> car polishing <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> car spa <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> car waxing <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> car coating <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find the Best Deal for <?php echo $brand;?> <?php echo $model;?> Water Wash, Exterior Cleaning, Interior Detailing, Exterior Polishing and Complete Car Spa at Best Prices in <?php echo $locality;?>. Doorstep Service Available">
		    <meta property="og:title" content="<?php echo $brand; ?> <?php echo $model; ?> Wash, Detailing & Cleaning Services in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php }//if bike
			if($VehicleType=='2w') {?>
            <meta name="description" content="Get your <?php echo $brand;?> <?php echo $model;?> Water Washed & Cleaned at the Best Price in <?php echo $locality;?>. Price Starts from Rs. 100. Doorstep Service Available. Book now">
            <title><?php echo $brand; ?> <?php echo $model; ?> Wash, Detailing & Cleaning Services in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="doorstep <?php echo $brand; ?> <?php echo $model; ?> bike wash, <?php echo $brand; ?> <?php echo $model; ?> bike wash in <?php echo $locality; ?>, doorstep <?php echo $brand; ?> <?php echo $model; ?> bike wash <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> bike cleaning <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> bike wash <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> bike detailing <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> bike coating <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Get your <?php echo $brand;?> <?php echo $model;?> Water Washed & Cleaned at the Best Price in <?php echo $locality;?>. Price Starts from Rs. 100. Doorstep Service Available. Book now">
		    <meta property="og:title" content="<?php echo $brand; ?> <?php echo $model; ?> Wash, Detailing & Cleaning Services in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php }//if bike
		 }//loc, brand, model
		 elseif($brand!=''&&$model==''&&$locality!='') { 
			if($VehicleType=='4w') {?>
			<meta name="description" content="Find the Best Deal for <?php echo $brand;?> Car Wash, Exterior Cleaning, Interior Detailing, Exterior Polishing and Complete <?php echo $brand;?> Car Spa at Best Prices in <?php echo $locality;?>. Doorstep Service Available">
            <title><?php echo $brand; ?> Car Wash, Detailing & Cleaning Services in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> car wash, <?php echo $brand; ?> car cleaning, doorstep <?php echo $brand; ?> car wash, doorstep <?php echo $brand; ?> car wash <?php echo $locality; ?>, <?php echo $brand; ?> car cleaning <?php echo $locality; ?>, <?php echo $brand; ?> car wash <?php echo $locality; ?>, <?php echo $brand; ?> car interior detailing <?php echo $locality; ?>, <?php echo $brand; ?> car polishing <?php echo $locality; ?>, <?php echo $brand; ?> car spa <?php echo $locality; ?>, <?php echo $brand; ?> car waxing <?php echo $locality; ?>, <?php echo $brand; ?> car coating <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find the Best Deal for <?php echo $brand;?> Car Wash, Exterior Cleaning, Interior Detailing, Exterior Polishing and Complete <?php echo $brand;?> Car Spa at Best Prices in <?php echo $locality;?>. Doorstep Service Available">
		    <meta property="og:title" content="<?php echo $brand; ?> Car Wash, Detailing & Cleaning Services in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php }//if bike
			if($VehicleType=='2w') {?>
            <meta name="description" content="Get your <?php echo $brand;?> Bike Water Washed & Cleaned at the Best Price in <?php echo $locality;?>. Price Starts from Rs. 100. Doorstep Service Available. Book now">
            <title><?php echo $brand; ?> Bike Wash, Detailing & Cleaning Services in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="doorstep <?php echo $brand; ?> bike wash, <?php echo $brand; ?> bike wash in <?php echo $locality; ?>, doorstep <?php echo $brand; ?> bike wash <?php echo $locality; ?>, <?php echo $brand; ?> bike cleaning <?php echo $locality; ?>, <?php echo $brand; ?> bike wash <?php echo $locality; ?>, <?php echo $brand; ?> bike detailing <?php echo $locality; ?>, <?php echo $brand; ?> bike coating <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Get your <?php echo $brand;?> Bike Water Washed & Cleaned at the Best Price in <?php echo $locality;?>. Price Starts from Rs. 100. Doorstep Service Available. Book now">
		    <meta property="og:title" content="<?php echo $brand; ?> Bike Wash, Detailing & Cleaning Services in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php }//if bike
		 }//brand, loc
	}//if Wash 
	
	elseif($ServiceType=="Breakdown Assistance") {
		if($locality==''&&$model==''&&$brand=='') { 
			if($VehicleType=='4w') {?>
			<meta name="description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Car Breakdown Roadside Assistance anywhere in Chennai. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
            <title>24 Hours Car Breakdown & Roadside Assistance in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="car breakdown service, car roadside assistance, car rsa, car breakdown in chennai, car breakdown assistance in chennai, breakdown assistance in chennai, 24 hours car breakdown service chennai"> -->
            <meta property="og:description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Car Breakdown Roadside Assistance anywhere in Chennai. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
		    <meta property="og:title" content="24 Hours Car Breakdown & Roadside Assistance in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model))?>">            
			<?php }//if bike
			if($VehicleType=='2w') {?>
             <meta name="description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Bike Breakdown Roadside Assistance anywhere in Chennai. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
            <title>24 Hours Bike Breakdown & Roadside Assistance in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="bike breakdown, scooter breakdown, bike breakdown in chennai, bike breakdown assistance in chennai, breakdown assistance in chennai, 24 hours bike breakdown service chennai"> -->
            <meta property="og:description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Bike Breakdown Roadside Assistance anywhere in Chennai. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
		    <meta property="og:title" content="24 Hours Bike Breakdown & Roadside Assistance in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model))?>">            
			<?php }//if bike
		 }//ntg
		 elseif($locality!=''&&$model==''&&$brand=='') { 
			if($VehicleType=='4w') {?>
            <meta name="description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Car Breakdown Roadside Assistance in <?php echo $locality;?>. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
            <title>24 Hours Car Breakdown & Roadside Assistance in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="car breakdown in <?php echo $locality; ?>, rsa <?php echo $locality; ?>, car breakdown assistance in <?php echo $locality; ?>, breakdown assistance in <?php echo $locality; ?>, 24 hours car breakdown service <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Car Breakdown Roadside Assistance in <?php echo $locality;?>. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
		    <meta property="og:title" content="24 Hours Car Breakdown & Roadside Assistance in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php }//if bike
			if($VehicleType=='2w') {?>
            <meta name="description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Bike Breakdown Roadside Assistance in <?php echo $locality;?>. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
            <title>24 Hours Bike Breakdown & Roadside Assistance in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="bike breakdown in <?php echo $locality; ?>, rsa <?php echo $locality; ?>, bike breakdown assistance in <?php echo $locality; ?>, breakdown assistance in <?php echo $locality; ?>, 24 hours bike breakdown service <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Bike Breakdown Roadside Assistance in <?php echo $locality;?>. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
		    <meta property="og:title" content="24 Hours Bike Breakdown & Roadside Assistance in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php }//if bike
		 }//only locality, no brand and model 
		 elseif($brand!=''&&$locality==''&&$model=='') { 
			 if($VehicleType=='4w') { ?>
			<meta name="description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Breakdown Roadside Assistance for <?php echo $brand;?> Car anywhere in Chennai. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
            <title>24 Hours <?php echo $brand; ?> Car Breakdown & Roadside Assistance in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> car breakdown service, <?php echo $brand; ?> car roadside assistance, <?php echo $brand; ?> car rsa, <?php echo $brand; ?> car breakdown in chennai, <?php echo $brand; ?> car breakdown assistance in chennai, breakdown assistance in chennai, 24 hours <?php echo $brand; ?> car breakdown service chennai"> -->
            <meta property="og:description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Breakdown Roadside Assistance for <?php echo $brand;?> Car anywhere in Chennai. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
		    <meta property="og:title" content="24 Hours <?php echo $brand; ?> Car Breakdown & Roadside Assistance in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
			<?php
			 }//car
			 if($VehicleType=='2w') { ?>
			<meta name="description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Breakdown Roadside Assistance for <?php echo $brand;?> Bike anywhere in Chennai. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
            <title>24 Hours <?php echo $brand; ?> Bike Breakdown & Roadside Assistance in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> bike breakdown, <?php echo $brand; ?> bike breakdown in chennai, <?php echo $brand; ?> bike breakdown assistance in chennai, breakdown assistance in chennai, 24 hours <?php echo $brand; ?> bike breakdown service chennai"> -->
            <meta property="og:description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Breakdown Roadside Assistance for <?php echo $brand;?> Bike anywhere in Chennai. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
		    <meta property="og:title" content="24 Hours <?php echo $brand; ?> Bike Breakdown & Roadside Assistance in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
			<?php
			 }//bike
		 }//no locality and brand
		 elseif($brand!=''&&$model!=''&&$locality=='') { 
			 if($VehicleType=='4w') {?>
			<meta name="description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Breakdown Roadside Assistance for <?php echo $brand.' '.$model;?> anywhere in Chennai. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
            <title>24 Hours <?php echo $brand; ?> <?php echo $model; ?> Breakdown & Roadside Assistance in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> <?php echo $model; ?> car breakdown service, <?php echo $brand; ?> <?php echo $model; ?> car roadside assistance, <?php echo $brand; ?> <?php echo $model; ?> car rsa, <?php echo $brand; ?> <?php echo $model; ?> car breakdown in chennai, <?php echo $brand; ?> <?php echo $model; ?> car breakdown assistance in chennai, breakdown assistance in chennai, 24 hours <?php echo $brand; ?> <?php echo $model; ?> car breakdown service chennai"> -->
            <meta property="og:description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Breakdown Roadside Assistance for <?php echo $brand.' '.$model;?> anywhere in Chennai. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
		    <meta property="og:title" content="24 Hours <?php echo $brand; ?> <?php echo $model; ?> Breakdown & Roadside Assistance in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
				<?php 
			 }//car
			 if($VehicleType=='2w') {?>
			<meta name="description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Breakdown Roadside Assistance for <?php echo $brand.' '.$model;?> anywhere in Chennai. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
            <title>24 Hours <?php echo $brand; ?> <?php echo $model; ?>  Breakdown & Roadside Assistance in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> <?php echo $model; ?> bike breakdown, <?php echo $brand; ?> <?php echo $model; ?> bike breakdown in chennai, <?php echo $brand; ?> <?php echo $model; ?> bike breakdown assistance in chennai, breakdown assistance in chennai, 24 hours <?php echo $brand; ?> <?php echo $model; ?> bike breakdown service chennai"> -->
            <meta property="og:description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Breakdown Roadside Assistance for <?php echo $brand.' '.$model;?> anywhere in Chennai. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
		    <meta property="og:title" content="24 Hours <?php echo $brand; ?> <?php echo $model; ?>  Breakdown & Roadside Assistance in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
				<?php 
			 }//bike
			 }//brand, model
		 elseif($brand!=''&&$model!=''&&$locality!='') { 
			 if($VehicleType=='4w') {?>
			<meta name="description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Breakdown Roadside Assistance for <?php echo $brand;?> <?php echo $model;?> anywhere in <?php echo $locality;?>. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
            <title>24 Hours <?php echo $brand; ?> <?php echo $model; ?> Breakdown & Roadside Assistance in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> <?php echo $model; ?> car breakdown service, <?php echo $brand; ?> <?php echo $model; ?> car roadside assistance, <?php echo $brand; ?> <?php echo $model; ?> car rsa, <?php echo $brand; ?> <?php echo $model; ?> car breakdown in <?php echo $locality; ?>, rsa <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> car breakdown assistance in <?php echo $locality; ?>, breakdown assistance in <?php echo $locality; ?>, 24 hours <?php echo $brand; ?> <?php echo $model; ?> car breakdown service <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Breakdown Roadside Assistance for <?php echo $brand;?> <?php echo $model;?> anywhere in <?php echo $locality;?>. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
		    <meta property="og:title" content="24 Hours <?php echo $brand; ?> <?php echo $model; ?> Breakdown & Roadside Assistance in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
				<?php 
			 }//car
			 if($VehicleType=='2w') {?>
			<meta name="description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Breakdown Roadside Assistance for <?php echo $brand;?> <?php echo $model;?> in <?php echo $locality;?>. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
            <title>24 Hours <?php echo $brand; ?> <?php echo $model; ?> Breakdown & Roadside Assistance in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> <?php echo $model; ?> bike breakdown, <?php echo $brand; ?> <?php echo $model; ?> bike breakdown in <?php echo $locality; ?>, rsa <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> bike breakdown assistance in <?php echo $locality; ?>, breakdown assistance in <?php echo $locality; ?>, 24 hours <?php echo $brand; ?> <?php echo $model; ?> bike breakdown service <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Breakdown Roadside Assistance for <?php echo $brand;?> <?php echo $model;?> in <?php echo $locality;?>. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
		    <meta property="og:title" content="24 Hours <?php echo $brand; ?> <?php echo $model; ?> Breakdown & Roadside Assistance in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
            	<?php 
			 }//bike
			 }//loc, brand, model
		 elseif($brand!=''&&$model==''&&$locality!='') {
			if($VehicleType=='4w') {?>
			<meta name="description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Breakdown Roadside Assistance for <?php echo $brand;?> Car anywhere in <?php echo $locality;?>. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
            <title>24 Hours <?php echo $brand; ?> Car Breakdown & Roadside Assistance in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> car breakdown service, <?php echo $brand; ?> car roadside assistance, <?php echo $brand; ?> car rsa, <?php echo $brand; ?> car breakdown in <?php echo $locality; ?>, rsa <?php echo $locality; ?>, <?php echo $brand; ?> car breakdown assistance in <?php echo $locality; ?>, breakdown assistance in <?php echo $locality; ?>, 24 hours <?php echo $brand; ?> car breakdown service <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Breakdown Roadside Assistance for <?php echo $brand;?> Car anywhere in <?php echo $locality;?>. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
		    <meta property="og:title" content="24 Hours <?php echo $brand; ?> Car Breakdown & Roadside Assistance in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
				<?php 
			 }//car
			 if($VehicleType=='2w') {?>
			<meta name="description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Breakdown Roadside Assistance for <?php echo $brand;?> Bike anywhere in <?php echo $locality;?>. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
            <title>24 Hours <?php echo $brand; ?> Bike Breakdown & Roadside Assistance in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> bike breakdown, <?php echo $brand; ?> bike breakdown in <?php echo $locality; ?>, rsa <?php echo $locality; ?>, <?php echo $brand; ?> bike breakdown assistance in <?php echo $locality; ?>, breakdown assistance in <?php echo $locality; ?>, 24 hours <?php echo $brand; ?> bike breakdown service <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find a Trusted Mechanic Near You. 24x7 Emergency Breakdown Roadside Assistance for <?php echo $brand;?> Bike anywhere in <?php echo $locality;?>. On the Spot Repair, Battery Jumpstart & Vehicle Towing. Doorstep Service Available">
		    <meta property="og:title" content="24 Hours <?php echo $brand; ?> Bike Breakdown & Roadside Assistance in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php 
				 }//bike
			}//brand, loc
	}//if Wash 
	
	elseif($ServiceType=="Vehicle Diagnostics") {
		if($locality==''&&$model==''&&$brand=='') { 
			if($VehicleType=='4w') { ?>
			<meta name="description" content="Find Trusted Mechanics for Doorstep Car Inspection and Diagnostics Services in Chennai Near You. Free On Call Diagnostics and Price Estimates for your Car">
            <title>Trusted Car Inspection Diagnostics Services in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="car inspection, car inspection chennai, used vehicle inspection services in chennai, used car inspection services in chennai, second hand car inspection services in chennai"> -->
            <meta property="og:description" content="Find Trusted Mechanics for Doorstep Car Inspection and Diagnostics Services in Chennai Near You. Free On Call Diagnostics and Price Estimates for your Car">
		    <meta property="og:title" content="Trusted Car Inspection Diagnostics Services in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model))?>">            
				<?php 
			}//if car
			if($VehicleType=='2w') { ?>
			<meta name="description" content="Find Trusted Mechanics for Doorstep Bike Inspection and Diagnostics Services in Chennai Near You. Free On Call Diagnostics and Price Estimates for your Bike">
            <title>Trusted Bike Inspection Diagnostics Services in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="bike inspection, scooter inspection, bike inspection chennai, Used vehicle inspection services in chennai, used bike inspection services in chennai, second hand bike inspection services in chennai"> -->
            <meta property="og:description" content="Find Trusted Mechanics for Doorstep Bike Inspection and Diagnostics Services in Chennai Near You. Free On Call Diagnostics and Price Estimates for your Bike">
		    <meta property="og:title" content="Trusted Bike Inspection Diagnostics Services in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model))?>">            
				<?php 
			}//if bike
		 }//no locality, brand, model 
		 elseif($locality!=''&&$model==''&&$brand=='') { 
			 if($VehicleType=='4w'){?>
			<meta name="description" content="Find Trusted Mechanics for Doorstep Car Inspection and Diagnostics Services in <?php echo $locality; ?> Free On Call Diagnostics and Price Estimates for your Car">
            <title>Trusted Car Inspection Diagnostics Services in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="car inspection <?php echo $locality; ?>, used vehicle inspection services in <?php echo $locality; ?>, used car inspection services in <?php echo $locality; ?>, second hand car inspection services in <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find Trusted Mechanics for Doorstep Car Inspection and Diagnostics Services in <?php echo $locality; ?> Free On Call Diagnostics and Price Estimates for your Car">
		    <meta property="og:title" content="Trusted Car Inspection Diagnostics Services in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php 
			 }//car
			 if($VehicleType=='2w'){?>
			<meta name="description" content="Find Trusted Mechanics for Doorstep Bike Inspection and Diagnostics Services in <?php echo $locality; ?>. Free On Call Diagnostics and Price Estimates for your Bike">
            <title>Trusted Bike Inspection Diagnostics Services in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="bike inspection chennai, Used vehicle inspection services in <?php echo $locality; ?>, used bike inspection services in <?php echo $locality; ?>, second hand bike inspection services in <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find Trusted Mechanics for Doorstep Bike Inspection and Diagnostics Services in <?php echo $locality; ?>. Free On Call Diagnostics and Price Estimates for your Bike">
		    <meta property="og:title" content="Trusted Bike Inspection Diagnostics Services in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            			
			<?php 
			 }//bike
		}//loc 
		 elseif($brand!=''&&$locality==''&&$model=='') { 
			 if($VehicleType=='4w') {?>
			<meta name="description" content="Find Trusted Mechanics for Doorstep <?php echo $brand;?> Car Inspection and Diagnostics Services in Chennai Near You. Free On Call Diagnostics and Price Estimates for your <?php echo $brand;?> Car">
            <title><?php echo $brand; ?> Car Inspection Diagnostics Services in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> car inspection, <?php echo $brand; ?> car inspection chennai, used vehicle inspection services in chennai, used <?php echo $brand; ?> car inspection services in chennai, second hand <?php echo $brand; ?> car inspection services in chennai"> -->
            <meta property="og:description" content="Find Trusted Mechanics for Doorstep <?php echo $brand;?> Car Inspection and Diagnostics Services in Chennai Near You. Free On Call Diagnostics and Price Estimates for your <?php echo $brand;?> Car">
		    <meta property="og:title" content="<?php echo $brand; ?> Car Inspection Diagnostics Services in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            		
			<?php 
			 }//car
			 if($VehicleType=='2w') {?>
			<meta name="description" content="Find Trusted Mechanics for Doorstep <?php echo $brand;?> Bike Inspection and Diagnostics Services in Chennai Near You. Free On Call Diagnostics and Price Estimates for your <?php echo $brand;?> Bike">
            <title><?php echo $brand; ?> Bike Inspection Diagnostics Services in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> bike inspection, <?php echo $brand; ?> bike inspection chennai, Used vehicle inspection services in chennai, used <?php echo $brand; ?> bike inspection services in chennai, second hand <?php echo $brand; ?> bike inspection services in chennai"> -->
            <meta property="og:description" content="Find Trusted Mechanics for Doorstep <?php echo $brand;?> Bike Inspection and Diagnostics Services in Chennai Near You. Free On Call Diagnostics and Price Estimates for your <?php echo $brand;?> Bike">
		    <meta property="og:title" content="<?php echo $brand; ?> Bike Inspection Diagnostics Services in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
			<?php 
			 }//bike
		}//brand
		 elseif($brand!=''&&$model!=''&&$locality=='') { 
			 if($VehicleType=='4w') {?>
			<meta name="description" content="Find Trusted Mechanics for Doorstep <?php echo $brand.' '.$model;?> Inspection and Diagnostics Services in Chennai Near You. Free On Call Diagnostics and Price Estimates for your <?php echo $brand;?> <?php echo $model;?>">
            <title><?php echo $brand; ?> <?php echo $model; ?> Inspection Services in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> <?php echo $model; ?> car inspection, <?php echo $brand; ?> <?php echo $model; ?> car inspection chennai, used vehicle inspection services in chennai, used <?php echo $brand; ?> <?php echo $model; ?> car inspection services in chennai, second hand <?php echo $brand; ?> <?php echo $model; ?> car inspection services in chennai"> -->
            <meta property="og:description" content="Find Trusted Mechanics for Doorstep <?php echo $brand.' '.$model;?> Inspection and Diagnostics Services in Chennai Near You. Free On Call Diagnostics and Price Estimates for your <?php echo $brand;?> <?php echo $model;?>">
		    <meta property="og:title" content="<?php echo $brand; ?> <?php echo $model; ?> Inspection Services in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
			<?php 
			 }//car
			 if($VehicleType=='2w') {?>
			<meta name="description" content="Find Trusted Mechanics for Doorstep <?php echo $brand.' '.$model;?> Inspection and Diagnostics Services in Chennai Near You. Free On Call Diagnostics and Price Estimates for your <?php echo $brand;?> <?php echo $model;?> ">
            <title><?php echo $brand; ?> <?php echo $model; ?> Inspection Services in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> <?php echo $model; ?> bike inspection, <?php echo $brand; ?> <?php echo $model; ?> bike inspection chennai, Used vehicle inspection services in chennai, used <?php echo $brand; ?> <?php echo $model; ?> bike inspection services in chennai, second hand <?php echo $brand; ?> <?php echo $model; ?> bike inspection services in chennai"> -->
            <meta property="og:description" content="Find Trusted Mechanics for Doorstep <?php echo $brand.' '.$model;?> Inspection and Diagnostics Services in Chennai Near You. Free On Call Diagnostics and Price Estimates for your <?php echo $brand;?> <?php echo $model;?>">
		    <meta property="og:title" content="<?php echo $brand; ?> <?php echo $model; ?> Inspection Services in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
			<?php 
			 }//bike
		}//brand, model
		 elseif($brand!=''&&$model!=''&&$locality!='') { 
			 if($VehicleType=='4w') {?>
			<meta name="description" content="Find Trusted Mechanics for Doorstep <?php echo $brand;?> <?php echo $model;?> Inspection and Diagnostics Services in <?php echo $locality;?>. Free On Call Diagnostics and Price Estimates for your <?php echo $brand;?> <?php echo $model;?> ">
            <title><?php echo $brand; ?> <?php echo $model; ?> Inspection Services in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> <?php echo $model; ?> car inspection, <?php echo $brand; ?> <?php echo $model; ?> car inspection <?php echo $locality; ?>, used vehicle inspection services in <?php echo $locality; ?>, used <?php echo $brand; ?> <?php echo $model; ?> car inspection services in <?php echo $locality; ?>, second hand <?php echo $brand; ?> <?php echo $model; ?> car inspection services in <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find Trusted Mechanics for Doorstep <?php echo $brand;?> <?php echo $model;?> Inspection and Diagnostics Services in <?php echo $locality;?>. Free On Call Diagnostics and Price Estimates for your <?php echo $brand;?> <?php echo $model;?> ">
		    <meta property="og:title" content="<?php echo $brand; ?> <?php echo $model; ?> Inspection Services in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php 
			 }//car
			 if($VehicleType=='2w') {?>
			<meta name="description" content="Find Trusted Mechanics for Doorstep <?php echo $brand;?> <?php echo $model;?> Inspection and Diagnostics Services in <?php echo $locality;?>. Free On Call Diagnostics and Price Estimates for your <?php echo $brand;?> <?php echo $model;?>">
            <title><?php echo $brand; ?> <?php echo $model; ?> Inspection Services in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> <?php echo $model; ?> bike inspection, <?php echo $brand; ?> <?php echo $model; ?> bike inspection chennai, Used vehicle inspection services in <?php echo $locality; ?>, used <?php echo $brand; ?> <?php echo $model; ?> bike inspection services in <?php echo $locality; ?>, second hand <?php echo $brand; ?> <?php echo $model; ?> bike inspection services in <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find Trusted Mechanics for Doorstep <?php echo $brand;?> <?php echo $model;?> Inspection and Diagnostics Services in <?php echo $locality;?>. Free On Call Diagnostics and Price Estimates for your <?php echo $brand;?> <?php echo $model;?>">
		    <meta property="og:title" content="<?php echo $brand; ?> <?php echo $model; ?> Inspection Services in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php 
			 }//bike
		}//loc, brand, model
		 elseif($brand!=''&&$model==''&&$locality!='') { 
			 if($VehicleType=='4w') {?>
			<meta name="description" content="Find Trusted Mechanics for Doorstep <?php echo $brand;?> Car Inspection and Diagnostics Services in <?php echo $locality;?>. Free On Call Diagnostics and Price Estimates for your <?php echo $brand;?> Car">
            <title><?php echo $brand; ?> Car Inspection Diagnostics Services in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> car inspection, <?php echo $brand; ?> car inspection <?php echo $locality; ?>, used vehicle inspection services in <?php echo $locality; ?>, used <?php echo $brand; ?> car inspection services in <?php echo $locality; ?>, second hand <?php echo $brand; ?> car inspection services in <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find Trusted Mechanics for Doorstep <?php echo $brand;?> Car Inspection and Diagnostics Services in <?php echo $locality;?>. Free On Call Diagnostics and Price Estimates for your <?php echo $brand;?> Car">
		    <meta property="og:title" content="<?php echo $brand; ?> Car Inspection Diagnostics Services in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php 
			 }//cae
			 if($VehicleType=='2w') {?>

			<meta name="description" content="Find Trusted Mechanics for Doorstep <?php echo $brand;?> Bike Inspection and Diagnostics Services in <?php echo $locality;?>. Free On Call Diagnostics and Price Estimates for your <?php echo $brand;?> Bike">
            <title><?php echo $brand; ?> Bike Inspection Diagnostics Services in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> bike inspection, <?php echo $brand; ?> bike inspection chennai, Used vehicle inspection services in <?php echo $locality; ?>, used <?php echo $brand; ?> bike inspection services in <?php echo $locality; ?>, second hand <?php echo $brand; ?> bike inspection services in <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find Trusted Mechanics for Doorstep <?php echo $brand;?> Bike Inspection and Diagnostics Services in <?php echo $locality;?>. Free On Call Diagnostics and Price Estimates for your <?php echo $brand;?> Bike">
		    <meta property="og:title" content="<?php echo $brand; ?> Bike Inspection Diagnostics Services in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php 
			 }//bike
		}//loc, brand
	}//if Wash 
	
	elseif($ServiceType=="Tyre Puncture") {
		if($locality==''&&$model==''&&$brand=='') { 
			if($VehicleType=='4w') {?>
			<meta name="description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Car Tyre Puncture Repair Shops in Chennai. For both Tube And Tubeless Tyres. Doorstep Service Available">
            <title>Trusted Car Tyre Puncture Services Repair in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="car tyre puncture, car tyre puncher shops, car tyre puncture in chennai, car tyre puncher shops chennai, chennai car tyre puncture shops , car tyre repair shops chennai, car tyre puncture service chennai, tubeless car tyre puncture in chennai"> -->
            <meta property="og:description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Car Tyre Puncture Repair Shops in Chennai. For both Tube And Tubeless Tyres. Doorstep Service Available">
		    <meta property="og:title" content="Trusted Car Tyre Puncture Services Repair in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model))?>">            
			<?php 
			}//if car
			if($VehicleType=='2w') {?>
			<meta name="description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Bike Tyre Puncture Repair Shops in Chennai. For both Tube And Tubeless Tyres. Doorstep Service Available">
            <title>Trusted Bike Tyre Puncture Services Repair in Chennai | GoBumpr</title>

            <!-- <meta name="keywords" content="bike tyre puncture, bike tyre puncher shops, bike tyre puncture chennai, bike tyre puncher shops chennai, chennai bike tyre puncture shops , bike tyre repair shops chennai, bike tyre puncture service chennai, tubeless bike tyre puncture in chennai"> -->
            <meta property="og:description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Bike Tyre Puncture Repair Shops in Chennai. For both Tube And Tubeless Tyres. Doorstep Service Available">
		    <meta property="og:title" content="Trusted Bike Tyre Puncture Services Repair in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model))?>">            
            	<?php 
			}//if BIKE
		 }//ntg
		 elseif($locality!=''&&$model==''&&$brand=='') { 
			 if($VehicleType=='2w') {?>
			<meta name="description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Bike Tyre Puncture Repair Shops in <?php echo $locality;?>. For both Tube And Tubeless Tyres. Doorstep Service Available">
            <title>Trusted Car Tyre Puncture Services Repair in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="car tyre puncture in <?php echo $locality; ?>, car tyre puncher shops <?php echo $locality; ?>, <?php echo $locality; ?> car tyre puncture shops , car tyre repair shops <?php echo $locality; ?>, car tyre puncture service <?php echo $locality; ?>, tubeless car tyre puncture in <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Bike Tyre Puncture Repair Shops in <?php echo $locality;?>. For both Tube And Tubeless Tyres. Doorstep Service Available">
		    <meta property="og:title" content="Trusted Car Tyre Puncture Services Repair in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php 
			 }//bike
			 if($VehicleType=='4w') {?>
			<meta name="description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Car Tyre Puncture Repair Shops in <?php echo $locality;?>. For both Tube And Tubeless Tyres. Doorstep Service Available">
            <title>Trusted Bike Tyre Puncture Services Repair in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="bike tyre puncture <?php echo $locality; ?>, bike tyre puncher shops <?php echo $locality; ?>, <?php echo $locality; ?> bike tyre puncture shops , bike tyre repair shops <?php echo $locality; ?>, bike tyre puncture service <?php echo $locality; ?>, tubeless bike tyre puncture in <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Car Tyre Puncture Repair Shops in <?php echo $locality;?>. For both Tube And Tubeless Tyres. Doorstep Service Available">
		    <meta property="og:title" content="Trusted Bike Tyre Puncture Services Repair in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            			
			<?php 
			 }//car
		}//loc 
		 elseif($brand!=''&&$locality==''&&$model=='') { 
			 if($VehicleType=='4w') {?>
			<meta name="description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Tyre Puncture Repair Shops for <?php echo $brand;?> Car in Chennai. For both Tube And Tubeless Tyres. Doorstep Service Available">
            <title>Trusted <?php echo $brand; ?> Car Tyre Puncture Services Repair in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> car tyre puncture, <?php echo $brand; ?> car tyre puncher shops, <?php echo $brand; ?> car tyre puncture in chennai, <?php echo $brand; ?> car tyre puncher shops chennai, chennai <?php echo $brand; ?> car tyre puncture shops , <?php echo $brand; ?> car tyre repair shops chennai, <?php echo $brand; ?> car tyre puncture service chennai, tubeless <?php echo $brand; ?> car tyre puncture in chennai"> -->
            <meta property="og:description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Tyre Puncture Repair Shops for <?php echo $brand;?> Car in Chennai. For both Tube And Tubeless Tyres. Doorstep Service Available">
		    <meta property="og:title" content="Trusted <?php echo $brand; ?> Car Tyre Puncture Services Repair in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
			<?php
			 }//car
			 if($VehicleType=='2w') {?>
			<meta name="description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Tyre Puncture Repair Shops for <?php echo $brand;?> Bike in Chennai. For both Tube And Tubeless Tyres. Doorstep Service Available">
            <title>Trusted <?php echo $brand; ?> Bike Tyre Puncture Services Repair in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> bike tyre puncture, <?php echo $brand; ?> bike tyre puncher shops, <?php echo $brand; ?> bike tyre puncture chennai, <?php echo $brand; ?> bike tyre puncher shops chennai, chennai <?php echo $brand; ?> bike tyre puncture shops , <?php echo $brand; ?> bike tyre repair shops chennai, <?php echo $brand; ?> bike tyre puncture service chennai, tubeless <?php echo $brand; ?> bike tyre puncture in chennai"> -->
            <meta property="og:description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Tyre Puncture Repair Shops for <?php echo $brand;?> Bike in Chennai. For both Tube And Tubeless Tyres. Doorstep Service Available">
		    <meta property="og:title" content="Trusted <?php echo $brand; ?> Bike Tyre Puncture Services Repair in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
			<?php
			 }//bike
		 }//brand
		 elseif($brand!=''&&$model!=''&&$locality=='') { 
			 if($VehicleType=='4w') {?>
			<meta name="description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Tyre Puncture Repair Shops for <?php echo $brand.' '.$model;?> in Chennai. For both Tube And Tubeless Tyres. Doorstep Service Available">
            <title>Trusted <?php echo $brand; ?> <?php echo $model; ?> Tyre Puncture Services in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> <?php echo $model; ?> car tyre puncture, <?php echo $brand; ?> <?php echo $model; ?> car tyre puncher shops, <?php echo $brand; ?> <?php echo $model; ?> car tyre puncture in chennai, <?php echo $brand; ?> <?php echo $model; ?> car tyre puncher shops chennai, chennai <?php echo $brand; ?> <?php echo $model; ?> car tyre puncture shops , <?php echo $brand; ?> <?php echo $model; ?> car tyre repair shops chennai, <?php echo $brand; ?> <?php echo $model; ?> car tyre puncture service chennai, tubeless <?php echo $brand; ?> <?php echo $model; ?> car tyre puncture in chennai"> -->
            <meta property="og:description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Tyre Puncture Repair Shops for <?php echo $brand.' '.$model;?> in Chennai. For both Tube And Tubeless Tyres. Doorstep Service Available">
		    <meta property="og:title" content="Trusted <?php echo $brand; ?> <?php echo $model; ?> Tyre Puncture Services in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
			<?php 
			 }//car
			 if($VehicleType=='2w') {?>
			<meta name="description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Tyre Puncture Repair Shops for <?php echo $brand.' '.$model;?> in Chennai. For both Tube And Tubeless Tyres. Doorstep Service Available">
            <title>Trusted <?php echo $brand; ?> <?php echo $model; ?> Tyre Puncture Services in Chennai | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> <?php echo $model; ?> bike tyre puncture, <?php echo $brand; ?> <?php echo $model; ?> bike tyre puncher shops, <?php echo $brand; ?> <?php echo $model; ?> bike tyre puncture chennai, <?php echo $brand; ?> <?php echo $model; ?> bike tyre puncher shops chennai, chennai <?php echo $brand; ?> <?php echo $model; ?> bike tyre puncture shops , <?php echo $brand; ?> <?php echo $model; ?> bike tyre repair shops chennai, <?php echo $brand; ?> <?php echo $model; ?> bike tyre puncture service chennai, tubeless <?php echo $brand; ?> <?php echo $model; ?> bike tyre puncture in chennai"> -->
            <meta property="og:description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Tyre Puncture Repair Shops for <?php echo $brand.' '.$model;?> in Chennai. For both Tube And Tubeless Tyres. Doorstep Service Available">
		    <meta property="og:title" content="Trusted <?php echo $brand; ?> <?php echo $model; ?> Tyre Puncture Services in Chennai | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>">            
			<?php 
			 }//bike
		}//brand, model
		 elseif($brand!=''&&$model!=''&&$locality!='') { 
			 if($VehicleType=='4w') {?>
			<meta name="description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Tyre Puncture Repair Shops for <?php echo $brand;?> <?php echo $model;?> Car in <?php echo $locality;?>. For both Tube And Tubeless Tyres. Doorstep Service Available">
            <title>Trusted <?php echo $brand; ?> <?php echo $model; ?> Tyre Puncture Services in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> <?php echo $model; ?> car tyre puncture, <?php echo $brand; ?> <?php echo $model; ?> car tyre puncher shops, <?php echo $brand; ?> <?php echo $model; ?> car tyre puncture in <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> car tyre puncher shops <?php echo $locality; ?>, <?php echo $locality; ?> <?php echo $brand; ?> <?php echo $model; ?> car tyre puncture shops , <?php echo $brand; ?> <?php echo $model; ?> car tyre repair shops <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> car tyre puncture service <?php echo $locality; ?>, tubeless <?php echo $brand; ?> <?php echo $model; ?> car tyre puncture in <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Tyre Puncture Repair Shops for <?php echo $brand;?> <?php echo $model;?> Car in <?php echo $locality;?>. For both Tube And Tubeless Tyres. Doorstep Service Available">
		    <meta property="og:title" content="Trusted <?php echo $brand; ?> <?php echo $model; ?> Tyre Puncture Services in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php 
			 }//car
			 if($VehicleType=='2w') {?>
			<meta name="description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Tyre Puncture Repair Shops for <?php echo $brand;?> <?php echo $model;?> in <?php echo $locality;?>. For both Tube And Tubeless Tyres. Doorstep Service Available">
            <title>Trusted <?php echo $brand; ?> <?php echo $model; ?> Tyre Puncture Services in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> <?php echo $model; ?> bike tyre puncture, <?php echo $brand; ?> <?php echo $model; ?> bike tyre puncher shops, <?php echo $brand; ?> <?php echo $model; ?> bike tyre puncture <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> bike tyre puncher shops <?php echo $locality; ?>, <?php echo $locality; ?> <?php echo $brand; ?> <?php echo $model; ?> bike tyre puncture shops , <?php echo $brand; ?> <?php echo $model; ?> bike tyre repair shops <?php echo $locality; ?>, <?php echo $brand; ?> <?php echo $model; ?> bike tyre puncture service <?php echo $locality; ?>, tubeless <?php echo $brand; ?> <?php echo $model; ?> bike tyre puncture in <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Tyre Puncture Repair Shops for <?php echo $brand;?> <?php echo $model;?> in <?php echo $locality;?>. For both Tube And Tubeless Tyres. Doorstep Service Available">
		    <meta property="og:title" content="Trusted <?php echo $brand; ?> <?php echo $model; ?> Tyre Puncture Services in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo strtolower(str_replace(" ","-", $model));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php 
			 }//bike
		}//loc, brand, model
		 elseif($brand!=''&&$model==''&&$locality!='') { 
			 if($VehicleType=='4w') {?>
			<meta name="description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Tyre Puncture Repair Shops for <?php echo $brand;?> Car in <?php echo $locality;?>. For both Tube And Tubeless Tyres. Doorstep Service Available">
            <title>Trusted <?php echo $brand; ?> Car Tyre Puncture Services Repair in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> car tyre puncture, <?php echo $brand; ?> car tyre puncher shops, <?php echo $brand; ?> car tyre puncture in <?php echo $locality; ?>, <?php echo $brand; ?> car tyre puncher shops <?php echo $locality; ?>, <?php echo $locality; ?> <?php echo $brand; ?> car tyre puncture shops , <?php echo $brand; ?> car tyre repair shops <?php echo $locality; ?>, <?php echo $brand; ?> car tyre puncture service <?php echo $locality; ?>, tubeless <?php echo $brand; ?> car tyre puncture in <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Tyre Puncture Repair Shops for <?php echo $brand;?> Car in <?php echo $locality;?>. For both Tube And Tubeless Tyres. Doorstep Service Available">
		    <meta property="og:title" content="Trusted <?php echo $brand; ?> Car Tyre Puncture Services Repair in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php 
			 }//car
			 if($VehicleType=='2w') {?>
			<meta name="description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Tyre Puncture Repair Shops for <?php echo $brand;?> Bike in <?php echo $locality;?>. For both Tube And Tubeless Tyres. Doorstep Service Available">
            <title>Trusted <?php echo $brand; ?> Bike Tyre Puncture Services Repair in <?php echo $locality; ?> | GoBumpr</title>
            <!-- <meta name="keywords" content="<?php echo $brand; ?> bike tyre puncture, <?php echo $brand; ?> bike tyre puncher shops, <?php echo $brand; ?> bike tyre puncture <?php echo $locality; ?>, <?php echo $brand; ?> bike tyre puncher shops <?php echo $locality; ?>, <?php echo $locality; ?> <?php echo $brand; ?> bike tyre puncture shops , <?php echo $brand; ?> bike tyre repair shops <?php echo $locality; ?>, <?php echo $brand; ?> bike tyre puncture service <?php echo $locality; ?>, tubeless <?php echo $brand; ?> bike tyre puncture in <?php echo $locality; ?>"> -->
            <meta property="og:description" content="Find a Trusted Puncture Specialist Near You. 24 Hours On The Spot Tyre Puncture Repair Shops for <?php echo $brand;?> Bike in <?php echo $locality;?>. For both Tube And Tubeless Tyres. Doorstep Service Available">
		    <meta property="og:title" content="Trusted <?php echo $brand; ?> Bike Tyre Puncture Services Repair in <?php echo $locality; ?> | GoBumpr">
            <meta property="og:url" content="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $brand));?>-<?php echo $VehicleType_model;?>-<?php echo strtolower(str_replace(" ","-",$ServiceType_model));?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>">            
			<?php 
			 }//bike
		}//loc, brand
	}//if Wash 
	?>
  


    <meta name="author" content="GoBumpr-Team">
    <meta property="og:site_name" content="GoBumpr">
    <meta property="og:site" content="https://gobumpr.com">
    <meta property="og:type" content="website">

<style type="text/css">
.c.form-control,.mob_radio_btn,.styled-select-filters select,button.close,input[type=search]{-webkit-appearance:none}*,.fake-input input,::after,::before,input[type=search]{box-sizing:border-box}.checkbox-custom+.checkbox-custom-label::before,.radio-custom+.radio-custom-label::before{background-position:initial initial;background-color:#fff;background-repeat:initial initial;content:'';vertical-align:middle;height:20px;display:inline-block;text-align:center}#dist,#price,#ratings,.col-xs-12,.col-xs-3,.col-xs-9{float:left}#social_footer,#social_footer ul,#social_footer ul li a,.btn,.img_list{text-align:center}#tools,.box_style_2,.btn_1,.radio-custom+.radio-custom-label::before,.styled-select-filters select,a.btn_1,body,button.close{background-repeat:initial initial}.btn_1,.modal,a,a.btn_1{outline:#000}.clearfix::after,.container::after,.content section::after,.modal-header::after,.row::after{clear:both}.radio-custom{opacity:0;position:absolute}.radio-custom,.radio-custom-label{display:inline-block;vertical-align:middle;margin:5px}.radio-custom-label{position:relative}.radio-custom+.radio-custom-label::before{border:2px solid #ddd;width:20px;padding:2px;margin-right:10px;border-radius:50%}.checkbox-custom{opacity:0;position:absolute}.checkbox-custom,.checkbox-custom-label{display:inline-block;vertical-align:middle;margin:5px}.checkbox-custom-label{position:relative}.checkbox-custom+.checkbox-custom-label::before{border:2px solid #ddd;width:22px;padding-bottom:18px;margin-right:10px}.content section{padding:40px 0 0;display:none;max-width:1230px;margin:0 auto}.content section::after,.content section::before{content:'';display:table}.ui-autocomplete{list-style:none;z-index:1000!important;overflow-y:auto;overflow-x:hidden}.ui-widget{background-color:#fff;width:100%}[class*=" icon-"]::before{speak:none;line-height:1;-webkit-font-smoothing:antialiased;font-family:gobumpr-icons!important;font-style:normal!important;font-weight:400!important;font-variant:normal!important;text-transform:none!important}@font-face{font-family:gobumpr-icons;src:url(/css/fonts/gobumpr-icons.eot?#iefix) format('embedded-opentype'),url(/css/fonts/gobumpr-icons.woff) format('woff'),url(/css/fonts/gobumpr-icons.ttf) format('truetype'),url(/css/fonts/gobumpr-icons.svg#gobumpr-icons) format('svg');font-weight:400;font-style:normal}.icon-email::before{content:'\67'}.icon-android::before{content:'\68'}.icon-blog::before{content:'\6b'}.icon-mobile::before{content:'\6c'}.icon-star::before{content:'\77'}.icon-since::before{content:'\79'}.icon-24x7-phone::before{content:'\44'}.icon-filter::before{content:'\47'}.icon-left-arrow::before{content:'\48'}.icon-map-pin::before{content:'\49'}.icon-search::before{content:'\4a'}.icon-badge::before{content:'\78'}.icon-sqft::before{content:'\43'}.icon-pay-online::before{content:'\4f'}.icon-number-mechanics::before{content:'\51'}.icon-half-star::before{content:'\34'}.icon-filled-star::before{content:'\35'}img{border:0;vertical-align:middle}body{margin:0}html{font-family:sans-serif;font-size:10px}aside,header,nav,section{display:block}[hidden]{display:none}a{background-color:transparent;color:#ffa800;text-decoration:none}strong{font-weight:700}h1{margin:.67em 0}h1,h2,nav{margin-top:20px}h1,h2,h4,h6,ul{margin-bottom:10px}button,input,select{margin:0;font-style:inherit;font-variant:inherit;font-weight:inherit;color:inherit;font-family:inherit;font-size:inherit;line-height:inherit}button{overflow:visible;-webkit-appearance:button}button,select{text-transform:none}input[type=checkbox],input[type=radio]{box-sizing:border-box;padding:0}input[type=search]::-webkit-search-cancel-button,input[type=search]::-webkit-search-decoration{-webkit-appearance:none}h1,h2,h4,h6{font-family:inherit;font-weight:500;line-height:1.1}h4,h6{margin-top:10px}h1{font-size:36px}h2{font-size:30px}h4{font-size:18px}h6{font-size:12px}ul{margin-top:0}@media (min-width:768px){.container{width:750px}}.container{margin-right:auto;margin-left:auto;padding-right:15px;padding-left:15px}@media (min-width:992px){.container{width:970px}}@media (min-width:1200px){.container{width:1170px}}.row{margin-right:-15px;margin-left:-15px}.col-lg-1,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-6,.col-lg-9,.col-md-1,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-6,.col-md-9,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-6,.col-sm-9,.col-xs-12,.col-xs-3,.col-xs-9{position:relative;min-height:1px;padding-right:15px;padding-left:15px}.col-xs-12{width:100%}.col-xs-9{width:75%}.col-xs-3{width:25%}@media (min-width:768px){.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-6,.col-sm-9{float:left}.col-sm-12{width:100%}.col-sm-9{width:75%}.col-sm-6{width:50%}.col-sm-4{width:33.33333333%}.col-sm-3{width:25%}.col-sm-2{width:16.66666667%}}@media (min-width:992px){.col-md-1,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-6,.col-md-9{float:left}.col-md-12{width:100%}.col-md-9{width:75%}.col-md-6{width:50%}.col-md-4{width:33.33333333%}.col-md-3{width:25%}.col-md-2{width:16.66666667%}.col-md-1{width:8.33333333%}}@media (min-width:1200px){.col-lg-1,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-6,.col-lg-9{float:left}.col-lg-9{width:75%}.col-lg-6{width:50%}.col-lg-4{width:33.33333333%}.col-lg-3{width:25%}.col-lg-2{width:16.66666667%}.col-lg-1{width:8.33333333%}}label{display:inline-block;max-width:100%;margin-bottom:5px}input[type=checkbox],input[type=radio]{margin:4px 0 0;line-height:normal}.btn,.form-control{background-image:none;padding:6px 12px;line-height:1.42857143}.form-control{display:block;width:100%;border:1px solid #ccc}.form-control::-webkit-input-placeholder{color:#999}.form-group{margin-bottom:15px}@media (min-width:768px){.form-inline .form-control{display:inline-block;vertical-align:middle}}.btn{display:inline-block;margin-bottom:0;font-size:14px;font-weight:400;white-space:nowrap;vertical-align:middle;border:1px solid transparent;border-radius:4px}.fade{opacity:0}.collapse{display:none}.navbar-form{padding:10px 15px;border-top-width:1px;border-top-style:solid;border-top-color:transparent;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:transparent;-webkit-box-shadow:rgba(255,255,255,.0980392) 0 1px 0 inset,rgba(255,255,255,.0980392) 0 1px 0;box-shadow:rgba(255,255,255,.0980392) 0 1px 0 inset,rgba(255,255,255,.0980392) 0 1px 0;margin:8px -15px}@media (min-width:768px){.navbar-form .form-group{display:inline-block;margin-bottom:0;vertical-align:middle}.navbar-form .form-control{display:inline-block;width:auto;vertical-align:middle}.navbar-form{width:auto;padding-top:0;padding-bottom:0;margin-right:0;margin-left:0;border:0;-webkit-box-shadow:none;box-shadow:none}}@media (max-width:767px){.navbar-form .form-group{margin-bottom:5px}.navbar-form .form-group:last-child{margin-bottom:0}}.close{font-weight:700;float:right;font-size:21px;line-height:1;color:#000;text-shadow:#fff 0 1px 0;opacity:.2}.modal{overflow:hidden;top:0;right:0;bottom:0;left:0;position:fixed;z-index:1050;display:none}.main-menu,.modal-body,.modal-content,.modal-dialog{position:relative}button.close{padding:0;border:0;background-position:0 0}#tools,.box_style_2,.btn_1,a.btn_1,body{background-position:initial initial}.modal.fade .modal-dialog{-webkit-transform:translate(0,-25%)}.modal-dialog{width:auto;margin:10px}.modal-content{-webkit-background-clip:padding-box;background-color:#fff;background-clip:padding-box;border:1px solid rgba(0,0,0,.2);outline:#000;-webkit-box-shadow:rgba(0,0,0,.498039) 0 3px 9px;box-shadow:rgba(0,0,0,.498039) 0 3px 9px;border-radius:6px}.modal-header{padding:15px;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#e5e5e5}.modal-header .close{margin-top:-2px}.modal-title{margin:0;line-height:1.42857143}.modal-body{padding:15px}@media (min-width:768px){.modal-dialog{width:600px;margin:30px auto}.modal-content{-webkit-box-shadow:rgba(0,0,0,.498039) 0 5px 15px;box-shadow:rgba(0,0,0,.498039) 0 5px 15px}}.clearfix::after,.clearfix::before,.container::after,.container::before,.modal-header::after,.modal-header::before,.row::after,.row::before{display:table;content:' '}.visible-xs-block{display:none!important}@media (max-width:767px){.visible-xs-block{display:block!important}.hidden-xs{display:none!important}}@media (min-width:1200px){.hidden-lg{display:none!important}}@media (max-width:991px) and (min-width:768px){.hidden-sm{display:none!important}}@media (max-width:1199px) and (min-width:992px){.hidden-md{display:none!important}}#header_menu,.layer{display:none}.btn_1,a.btn_1,body{font-size:12px}.form-control{-webkit-box-shadow:none;box-shadow:none;padding-left:8px;background-color:#fff}label{font-weight:600;line-height:14px}.main-menu{z-index:9;width:auto}.main-menu ul,.main-menu ul li{position:relative;margin:0;padding:0}.header-form{box-shadow:none!important}.layer{position:fixed;top:0;left:0;width:100%;min-width:100%;min-height:100%;background-color:#000;opacity:0;z-index:9999}@media only screen and (min-width:992px){.main-menu{width:auto}.main-menu ul li{display:inline-block}}@media only screen and (max-width:991px){#header_menu,.main-menu li{position:relative}#header_menu{text-align:center;padding:25px 15px 10px;display:block}.main-menu ul li{border-top-style:none;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#ededed;color:#fff}.main-menu li{display:block;color:#333!important}.main-menu ul>li{padding-bottom:0}.main-menu ul>li i{float:right}.main-menu{overflow:auto;left:-100%;bottom:0;width:55%;height:100%;opacity:0;position:fixed;background-color:#fff;z-index:999999;-webkit-box-shadow:rgba(50,50,50,.54902) 1px 0 5px 0;box-shadow:rgba(50,50,50,.54902) 1px 0 5px 0}.search_mobile{box-shadow:none;margin-left:20%;border-bottom-style:none!important}}@media only screen and (max-width:480px){.main-menu{width:100%;margin-bottom:20px!important}#menu_div{margin-bottom:.1px!important}}body,html{overflow-x:hidden}body,h1,h2,h4,h6{-webkit-font-smoothing:antialiased}.filter_type ul,footer ul,ul.add_info{list-style:none}body{background-color:#fff;line-height:20px;font-family:Montserrat,Arial,sans-serif;color:#565a5c}h1,h2,h4,h6{color:#333}h4 span{color:#ffa800}p{margin:0 0 20px}.btn_1,a.btn_1{border:none;font-family:inherit;color:#fff;background-color:#ffa800;padding:7px 20px;display:inline-block;text-transform:uppercase;font-weight:700;border-radius:3px}#logo{margin-top:10px;margin-bottom:10px}header{position:fixed;left:0;top:0;width:100%;z-index:99999;padding:10px 0}header #logo .logo_sticky{display:none}footer a{color:#fff}footer ul{margin:0;padding:0 0 20px}#social_footer ul{margin:0;padding:0 0 10px}#social_footer ul li{display:inline-block;margin:0 5px 10px}#social_footer ul li a{color:#fff;line-height:36px;display:block;font-size:16px;width:35px;height:35px;border:1px solid rgba(255,255,255,.298039);border-radius:50%}#tools{background-color:#eee;padding:5px;margin-bottom:15px}.strip_all_tour_list{margin-bottom:30px;background-color:#fff;display:block;color:#777;-webkit-box-shadow:rgba(0,0,0,.0980392) 0 0 5px 0;box-shadow:rgba(0,0,0,.0980392) 0 0 5px 0}.tour_list_desc .rating{margin:5px 0 5px -3px;font-size:15px}.img_list{overflow:hidden;min-height:220px;position:relative}#filter_btn_mob,.border_right,.dd-pointer,.img_list img,.ribbon,.short_info{position:absolute}.img_list img{width:auto;height:220px;left:-10%}.img_list a img{-webkit-transform:scale(1.2)}.img_list .short_info{padding:15px 5px 0;text-align:left;min-height:38px}.tour_list_desc{padding:10px 20px 0 0;border-right-width:1px;border-right-style:solid;border-right-color:#ededed;height:220px;line-height:17px}.tour_list_desc h2{text-transform:uppercase;font-size:18px;line-height:20px;margin-top:0;margin-bottom:10px}.price_list{display:table;height:220px;font-size:25px;color:#e74c3c;width:100%;margin-left:-15px}.price_list div{display:table-cell;vertical-align:middle;text-align:center;line-height:25px}.price_list p,ul.add_info{padding:0;margin:0}ul.add_info li{display:inline-block;margin-right:5px;border:1px solid #ededed;text-align:center;width:35px;height:35px;border-radius:3px}.filter_type h6{border-top-width:1px;border-top-style:solid;border-top-color:#ddd}ul.add_info li a{color:#555;width:35px;display:block}ul.add_info li i{font-size:22px;position:relative;display:inline-block;top:6px}#filters_col{background-color:#fff;padding:15px 10px 15px 15px;border:1px solid #ddd;margin-bottom:25px;border-radius:3px}#filters_col label{color:#777;font-weight:400}.filter_type h6{margin:15px 0;padding:15px 0 0}.filter_type ul{padding:0;margin:0 0 15px}.rating{font-size:18px}.rating .voted{color:#f90}.modal-dialog{margin-top:80px}.box_style_2{background-color:#fff;padding:20px;position:relative;text-align:center;margin-bottom:25px;border:1px solid #ddd;border-radius:3px}.box_style_2 i{font-size:52px;margin-top:10px;display:inline-block}.box_style_2 a.phone{font-size:26px;display:block;margin-bottom:20px}.form-control{font-size:12px;color:#333;height:40px;border-radius:3px}.styled-select-filters select{font-weight:400;width:115%;padding:7px 5px 5px 10px;border:0;height:31px;margin:0;font-size:12px;color:#888;background-position:0 0;border-radius:0}.styled-select-filters{width:100%;overflow:hidden;height:34px;background-image:url(../img/down_arrow_select_filters.png);background-color:#fff;margin:0;padding:0;border:1px solid #ddd;display:block;background-position:100% 50%;background-repeat:no-repeat no-repeat}.margin_60{padding-top:60px;padding-bottom:60px}.short_info{left:0;bottom:0;background-image:url(../img/shadow_tour.png);width:100%;padding:10px 10px 8px 5px;color:#fff;background-position:0 100%;background-repeat:repeat no-repeat}.short_info i{font-size:25px;display:inline-block;vertical-align:middle;font-weight:400;font-style:normal;padding:0;margin:0}.ribbon{top:0;left:-1px;width:78px;height:78px;z-index:1}.ribbon.authorized{background-image:url(/img/gobumpr/services_offered/authorized_ribbon.png);background-position:initial initial;background-repeat:no-repeat no-repeat}#position{background-color:transparent;padding:10px 0;font-size:12px;margin-top:60px!important;margin-bottom:-45px!important}#position ul{margin:0;padding:0;color:#777}#position ul li a{color:#777}#position ul li{display:inline-block;padding-right:8px;margin-right:3px;position:relative}#position ul li::after{font-family:fontello;content:'/';font-style:normal;font-weight:400;position:absolute;right:0;top:2px}#position ul li:last-child::after{content:''}.dd-pointer{width:0;height:0;right:10px;top:50%;margin-top:-3px}.dd-pointer-down{border-right-width:5px;border-bottom-width:5px;border-left-width:5px;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-right-color:transparent;border-bottom-color:transparent;border-left-color:transparent;border-top-style:solid!important;border-top-width:5px!important;border-top-color:#999!important}#filter_btn_mob,.sort_buttons{border-width:0 0 .1px;outline:#000;background-color:transparent;border-bottom-style:solid;border-bottom-color:#eee}@media (max-width:1199px) and (min-width:768px){.btn-responsive{padding:7px 15px!important;font-size:10px!important}}input{text-transform:capitalize!important}@media (max-width:1200px) and (min-width:768px){.img_list img{left:-25%}}@media (max-width:991px){nav{margin-top:15px}header #logo img.logo_normal,header #logo img.logo_sticky{width:auto;height:30px}}@media (max-width:767px){.price_list,.price_list div{display:block;text-align:center}.price_list,.price_list div,.strip_all_tour_list{text-align:center}.img_list{width:100%;margin:auto}.img_list img{left:0;width:100%;height:auto;top:-45%}.tour_list_desc{padding:0 15px;border-right-style:none;height:auto}.price_list{height:auto;margin-left:0;padding:25px 0 15px}.margin_60{padding-top:30px;padding-bottom:30px}}@media (max-width:480px){.img_list img{left:0;width:100%;height:auto;top:-5%}.styled-select-filters{margin-bottom:5px}}#mobile_sort{display:inline;margin-top:10px}.sort_buttons{height:45px;width:28%;margin:0;display:inline-block}.border_right{left:28%;top:50%;height:25px;border-right-width:1px;border-right-style:solid;border-right-color:#eee}#sorting_mobile{border-top-width:.1px;border-top-style:solid;border-top-color:#eee;margin-top:.9em}#filter_btn_mob{height:45px;width:16%;margin-left:-54%;top:0;right:0}.show_filters_mob{background-color:#fff;position:fixed;width:100%;height:100%;top:0;left:0;z-index:1000}.fake-input,.mob_radio_btn{position:relative}.fake-input input{display:block;width:100%}@media only screen and (max-width:991px){#dropdown_icon{margin-top:3%!important}#position{display:none}}@media only screen and (max-width:1199px) and (min-width:991px){#city_header{display:none}}
</style>

<!-- Google Analytics Code -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-1', 'auto');
  ga('send', 'pageview');

</script>


<style type="text/css">
.fade-box .lazyload,
     .fade-box .lazyloading {
        opacity: 0;
        transition: opacity 400ms;
    }

    .fade-box img.lazyloaded {
        opacity: 1;
    }
    .blur-up {
        -webkit-filter: blur(5px);
        filter: blur(5px);
        transition: filter 400ms, -webkit-filter 400ms;
    }

    .blur-up.lazyloaded {
        -webkit-filter: blur(0);
        filter: blur(0);
    }

ul.add_info li i {
	color:#777;
	font-size:20px !important;
}
.main-menu ul li {
	margin-left:-3px !important;
	margin-right: -1px !important;
}
li#search_mobile .form-control {
	border-left:0 !important;
	outline:0 !important;
}
.dd-option-image {
	width:25px !important;
	height:25px !important;
}
.dd-selected-image {
	width:25px !important;
	height:25px !important;
}
.dd-options ul {
	width:100px !important;
	margin-left: -3px !important;
}
.main-menu ul ul li {
	width:100%;
}
.main-menu ul ul, .main-menu ul .menu-wrapper {
	margin-left:-3px !important;
}
.main-menu ul ul, .main-menu ul .menu-wrapper  {
	min-width:101px !important;
	overflow:hidden !important;
}
.dd-option-text {
    line-height: 37px !important;
}
.ui-menu {
}
.heading_tooltip {
	text-transform:uppercase !important;
}
.fa-star-o:before, .fa-star:before, .fa-trophy:before, .fa-map-marker:before, .fa-clock-o:before {
	font-family:FontAwesome !important;
	font-style:normal !important;
	font-weight:400 !important;
	speak: none;
    display: inline-block;
    text-decoration: inherit;
    width: 1em;
    margin-right: .2em;
    text-align: center;
    font-variant: normal;
    text-transform: none;
    line-height: 1em;
    margin-left: .2em;
}
.icon icon-24x7-phone:before {
	font-size: 52px;
    margin-top: 10px;
	font-family:FontAwesome !important;
	font-style:normal !important;
	font-weight:400 !important;
	speak: none;
    display: inline-block;
    text-decoration: inherit;
    width: 1em;
    margin-right: .2em;
    text-align: center;
    font-variant: normal;
    text-transform: none;
    line-height: 1em;
    margin-left: .2em;
}

.dd-selected-text{
	line-height:37px !important;
}
.dd-option-text{
	line-height:37px !important;
}
#toTop:before {
	content: "\45" !important;
	font-family:"gobumpr-icons" !important;
}
#social_footer ul li a{
	line-height:36px !important;
}

</style>        
<script type="text/javascript" src="/js/jquery.ddslick.js"></script>
<script>
$(function() {
if($('input[name=VehicleType]').val()=='4w') {
		$('#bike_options_ddslick').hide();
		$('#car_options_ddslick').show();
}
else {
		$('#car_options_ddslick').hide();
		$('#bike_options_ddslick').show();
}
});
</script>
<script>
var script = document.createElement('script');

if(script.readyState) {  //IE
script.onreadystatechange = function() {
  if ( script.readyState === "loaded" || script.readyState === "complete" ) {
    script.onreadystatechange = null;
  }
};
 } else{//others
script.onload = function() {

	$("select.ddslick").each(function(){
				$(this).ddslick({
					showSelectedHTML: true,
					onSelected: function(data){
							//$('.dd-selected-value').attr('name', 'option[' + data.selectedData.val + ']');
							console.log(data.selectedData); 
						if($('input[name=VehicleType]').val()!=$('#vehicle_type').val()) {
							$("#BrandModel").val("");
							if($('input[name=VehicleType]').val()=='4w') {
								$('#bike_options_ddslick').hide();
								$('#car_options_ddslick').show();
								//$('input[name=ServiceType]').val(data.selectedData.value);
								console.log($('input[name=ServiceType]').val()); 
							}
							else {
								$('#car_options_ddslick').hide();
								$('#bike_options_ddslick').show();
								$('input[name=ServiceType]').val(data.selectedData.value);
								console.log($('input[name=ServiceType]').val());
							}
						}
						else {
							//$("#BrandModel").val("");
							if($('input[name=VehicleType]').val()=='4w') {
								$('#bike_options_ddslick').hide();
								$('#car_options_ddslick').show();
								//$('input[name=ServiceType]').val(data.selectedData.value);
								console.log($('input[name=ServiceType]').val()); 
							}
							else {
								$('#car_options_ddslick').hide();
								$('#bike_options_ddslick').show();
								$('input[name=ServiceType]').val(data.selectedData.value);
								console.log($('input[name=ServiceType]').val());
							}
						}
					}
				});
			});

}
}
script.src = "/js/jquery.ddslick.js"
document.documentElement.appendChild(script);
//to be executed after the script file is loaded
</script>

<!-- Common scripts -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!--[if lte IE 8]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="//browsehappy.com/">upgrade your browser</a>.</p>
<![endif]-->
<!--    <div id="preloader">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
    </div>
    <!-- End Preload -->


    <div class="layer"></div>
    <!-- Mobile menu overlay mask -->

    <!-- Header================================================== -->
    <header style="position:absolute">
        
        <div class="container">
            <div class="row" style="margin-bottom:-22px">
                <div class="col-md-3 col-sm-3 col-xs-3" id="menu_div" style="margin-bottom:-18px">
                    <div id="logo" style="margin-top:-2px;">
                        <a href="https://gobumpr.com/main.html"><img src="https://gobumpr.com/img/gobumpr/logo/2.png" width="160" height="34" alt="GoBumpr" data-retina="true" class="logo_normal"></a>
                    </div>
                </div>
                <nav class="col-md-9 col-sm-9 col-xs-9" style="margin-bottom:-18px; height:2px;">
                <div class="hidden-md hidden-lg" style="float:right; margin-top:-12px;"><a href="https://gobumpr.com/main.html"><i class="icon icon-search" style="font-size:20px;"></i></a></div>
                    <div class="main-menu hidden-xs hidden-sm">
<ul style="height:1px;">
<li class="search_mobile" style="float:left; top:-32px" >
<form method="get" accept-charset="utf-8" role="form" id="SearchForm" action="https://gobumpr.com/search.php" class="navbar-form" style="margin-bottom:-24px; margin-left:-10px; padding-bottom:-8px;">
   <li class="search_mobile"  id="city_header">     
        <div class="form-inline header-form" style="width:10em; display:inline-block; vertical-align:middle; margin-left:0px">
            <select class="ddslick form-control" id="city">
              <option data-imagesrc="https://gobumpr.com/img/gobumpr/icon_search/location.png" value="Chennai">Chennai</option> 
            </select>
        </div>
	</li>
   <li class="search_mobile">     
        <div class="form-inline header-form" style="width:8em; display:inline-block; vertical-align:middle">
            <select class="ddslick form-control" id="VehicleType" name="VehicleType">
              <option data-imagesrc="https://gobumpr.com/img/gobumpr/icon_search/bike.png" value="2w" <?php if($VehicleType == '2w') echo 'selected'; ?>> Bike </option> 
              <option data-imagesrc="https://gobumpr.com/img/gobumpr/icon_search/car.png" value="4w" <?php if($VehicleType == '4w') echo 'selected'; ?>> Car </option> 
            </select>
        </div>
	</li>
   <li class="search_mobile">     
        <div class="form-inline header-form">
        <div class="ui-widget">
          <input id="BrandModel" type="search" style="width:15em" name="BrandModel" value="<?php if($model!='') echo $BrandModel; else echo $brand;?>" class="ui-autocomplete form-control" placeholder="Select Model"><span id="message"></span>
  <span id="dropdown_icon" class="dd-pointer dd-pointer-down"></span>
        </div>
        </div>
	</li>       
    <li class="search_mobile">
       <div class="form-inline header-form" style="width:18em; display:inline-block; vertical-align:middle; margin-left:-2px;">
	<select class="ddslick" name="ServiceType" id="bike_options_ddslick">
            <option id="bike_options_ddslick" <?php if($ServiceType == 'General Service') echo "selected='selected'"; ?>  value="General Service" data-imagesrc="https://gobumpr.com/img/gobumpr/bike_general_service.png">General Service</option>
            <option id="bike_options_ddslick" <?php if($ServiceType == 'Breakdown Assistance') echo "selected='selected'"; ?> value="Breakdown Assistance" data-imagesrc="https://gobumpr.com/img/gobumpr/bike_breakdown.png">Breakdown Assistance</option>
            <option  id="bike_options_ddslick"<?php if($ServiceType == 'Water Wash') echo "selected='selected'"; ?>  value="Water Wash"  data-imagesrc="https://gobumpr.com/img/gobumpr/bike_wash.png">Water Wash</option>
            <option id="bike_options_ddslick" <?php if($ServiceType == 'Tyre Puncture') echo "selected='selected'"; ?> value="Tyre Puncture" data-imagesrc="https://gobumpr.com/img/gobumpr/bike_tyre_puncture.png">Tyre Puncture</option>
            <option id="bike_options_ddslick" <?php if($ServiceType == 'Vehicle Diagnostics') echo "selected='selected'"; ?> value="Vehicle Diagnostics" data-imagesrc="https://gobumpr.com/img/gobumpr/bike_vehicle_diag.png">Vehicle Diagnostics</option>
            
</select>
	<select class="ddslick" name="ServiceType" id="car_options_ddslick">
            <option id="car_options_ddslick" <?php if($ServiceType == 'General Service') echo "selected='selected'"; ?>  value="General Service" data-imagesrc="https://gobumpr.com/img/gobumpr/car_general_service.png">General Service</option>
            <option id="car_options_ddslick" <?php if($ServiceType == 'Breakdown Assistance') echo "selected='selected'"; ?> value="Breakdown Assistance" data-imagesrc="https://gobumpr.com/img/gobumpr/car_breakdown.png">Breakdown Assistance</option>
            <option id="car_options_ddslick" <?php if($ServiceType == 'Water Wash') echo "selected='selected'"; ?>  value="Water Wash"  data-imagesrc="https://gobumpr.com/img/gobumpr/car_wash.png">Car Wash</option>
            <option id="car_options_ddslick" <?php if($ServiceType == 'Tyre Puncture') echo "selected='selected'"; ?> value="Tyre Puncture" data-imagesrc="https://gobumpr.com/img/gobumpr/car_tyre_puncture.png">Tyre Puncture</option>
            <option id="car_options_ddslick" <?php if($ServiceType == 'Vehicle Diagnostics') echo "selected='selected'"; ?> value="Vehicle Diagnostics" data-imagesrc="https://gobumpr.com/img/gobumpr/car_vehicle_diagnostics.png">Vehicle Diagnostics</option>
            
	</select>
        </div>
   </li>
   
	<li class="search_mobile">                        
        <div class="form-inline header-form">
<div class="ui-widget">
<div class="fake-input">
    <!--<img src="img/gobumpr/icon_search/select location.png" width="25">-->

    <input id="locality" style="width:13em" name="locality" type="search" class="ui-autocomplete form-control" placeholder="Locality" Value="<?php echo $locality; ?>"><span id="message"></span>
  <span id="dropdown_icon" class="dd-pointer dd-pointer-down"></span>
  </div>
</div>
        </div>
	</li>
	<input type="hidden" id="city" name="city" value="Chennai">
    
        <li class="search_mobile" style="vertical-align:bottom"><button type="submit" class="btn_1" style="padding:10px 12px 10px 12px !important; width:40px; height:40px"><i class="icon icon-search" aria-hidden="true"style="font-size:18px"></i></button></li>
</form>
</li>
</ul>
</div><!-- End main-menu -->
                </nav>
            </div>
        </div><!-- container -->

<div id="mobile_sort" class="hidden-md hidden-lg">

<div id="sorting_mobile" style="position:relative">

<input class="mob_radio_btn" type="radio" name="mob_radio" id="r1" value="dist">
<button class="sort_buttons" type="button" onClick="r1();" id="dist">Distance</button><div class="border_right"></div>

<input class="mob_radio_btn" type="radio" name="mob_radio" id="r2" value="price">
<button class="sort_buttons" type="button" onClick="r2();" id="price">Price</button><div class="border_right" style="left:56% !important"></div>

<input class="mob_radio_btn" type="radio" name="mob_radio" id="r3" value="ratings">
<button class="sort_buttons" type="button" onClick="r3();" id="ratings">Ratings</button><div class="border_right" style="left:84% !important"></div>

<button class="filter_buttons" type="button" id="filter_btn_mob" style="float:right"><i class="icon icon-filter" style="font-size:18px;"></i></button>
</div>
<form role="form" action="" method="get">
    <select name="sort_select_mob" id="sort_id_mob" class="sort_select_mob" hidden/>
        <option value="sort_distance" onChange="this.form.submit()" <?php if($toggle=='sort_distance'||$toggle=='dist') echo 'selected="selected"'; ?>>Sort by Distance</option>
        <option value="sort_price" onChange="this.form.submit()" <?php if($toggle=='sort_price'||$toggle=='price') echo 'selected="selected"'; ?>>Sort by Price</option>
        <option value="sort_ratings" onChange="this.form.submit()" <?php if($toggle=='sort_ratings'||$toggle=='ratings') echo 'selected="selected"'; ?>>Sort by Ratings</option>
    </select>
</form>                                	

</div>


        
    </header><!-- End Header -->



<div id="position" class="hidden-xs hidden-sm">
    	<div class="container">
                	<ul>
                    <li><a href="https://gobumpr.com">Home</a></li>
                    <li><a href="https://gobumpr.com/<?php echo strtolower($city);?>"><?php echo ucwords($city);?></a></li>
                    <?php if($BrandModel=='' && $brand==''&&$locality=='') {?>
                    <li><?php echo $ServiceType;?></li>
                    <?php }//if brand and model are empty
					elseif($locality!=''&& $brand!='' && $BrandModel!='') {?>
                    <li><a href="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower($VehicleType_model);?>-
					<?php switch($ServiceType){
						case 'General Service':echo 'services-repair';
												break;
						case 'Breakdown Assistance':echo 'roadside-breakdown-assistance';
												break;
						case 'Water Wash':echo 'cleaning-water-wash-services';
												break;
						case 'Car Wash':echo 'cleaning-water-wash-services';
												break;
						case 'Tyre Puncture':echo 'tyre-puncture-repair-shops';
												break;
						case 'Vehicle Diagnostics':echo 'diagnostics-inspection-services';
												break;
						default:echo 'services-repair';
												break;
												
					} //switch ?>"><?php echo $ServiceType; ?></a></li>
                    <?php }//if brand and model are present 
					elseif($locality!=''&& $brand!='' && $BrandModel=='') {?>
                    <li><a href="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower($VehicleType_model);?>-
					<?php switch($ServiceType){
						case 'General Service':echo 'services-repair';
												break;
						case 'Breakdown Assistance':echo 'roadside-breakdown-assistance';
												break;
						case 'Water Wash':echo 'cleaning-water-wash-services';
												break;
						case 'Car Wash':echo 'cleaning-water-wash-services';
												break;
						case 'Tyre Puncture':echo 'tyre-puncture-repair-shops';
												break;
						case 'Vehicle Diagnostics':echo 'diagnostics-inspection-services';
												break;
						default:echo 'services-repair';
												break;
												
					} //switch ?>"><?php echo $ServiceType; ?></a></li>
                    <?php }//if brand alone is present 
					elseif($locality==''&& $brand!='' && $BrandModel=='') {?>
                    <li><a href="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower($VehicleType_model);?>-
					<?php switch($ServiceType){
						case 'General Service':echo 'services-repair';
												break;
						case 'Breakdown Assistance':echo 'roadside-breakdown-assistance';
												break;
						case 'Water Wash':echo 'cleaning-water-wash-services';
												break;
						case 'Car Wash':echo 'cleaning-water-wash-services';
												break;
						case 'Tyre Puncture':echo 'tyre-puncture-repair-shops';
												break;
						case 'Vehicle Diagnostics':echo 'diagnostics-inspection-services';
												break;
						default:echo 'services-repair';
												break;
												
					} //switch ?>"><?php echo $ServiceType; ?></a></li>
                    <?php }//if brand alone is present 
					elseif($locality==''&& $brand!='' && $BrandModel!='') {?>
                    <li><a href="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower($VehicleType_model);?>-
					<?php switch($ServiceType){
						case 'General Service':echo 'services-repair';
												break;
						case 'Breakdown Assistance':echo 'roadside-breakdown-assistance';
												break;
						case 'Water Wash':echo 'cleaning-water-wash-services';
												break;
						case 'Car Wash':echo 'cleaning-water-wash-services';
												break;
						case 'Tyre Puncture':echo 'tyre-puncture-repair-shops';
												break;
						case 'Vehicle Diagnostics':echo 'diagnostics-inspection-services';
												break;
						default:echo 'services-repair';
												break;
												
					} //switch ?>"><?php echo $ServiceType; ?></a></li>
                    <?php }//if brand and model are present 
					elseif($locality!=''&& $brand=='' && $BrandModel=='') {?>
                    <li><a href="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower($VehicleType_model);?>-
					<?php switch($ServiceType){
						case 'General Service':echo 'services-repair';
												break;
						case 'Breakdown Assistance':echo 'roadside-breakdown-assistance';
												break;
						case 'Water Wash':echo 'cleaning-water-wash-services';
												break;
						case 'Car Wash':echo 'cleaning-water-wash-services';
												break;
						case 'Tyre Puncture':echo 'tyre-puncture-repair-shops';
												break;
						case 'Vehicle Diagnostics':echo 'diagnostics-inspection-services';
												break;
						default:echo 'services-repair';
												break;
												
					} //switch ?>"><?php echo $ServiceType; ?></a></li>
                    <?php }//if brand and model are present ?>
	                <?php  if($locality!='' && $brand!='' && $BrandModel!='') { ?>
                    <li>
                    <a href="https://gobumpr.com/<?php echo strtolower($city);?>/<?php if($BrandModel!='') echo strtolower(str_replace(' ','-', $BrandModel)); else echo strtolower(str_replace(' ','-', $brand));?>-<?php echo strtolower($VehicleType_model);?>-
					<?php switch($ServiceType){
						case 'General Service' :echo 'services-repair'; break;
						case 'Breakdown Assistance':echo 'roadside-breakdown-assistance'; break;
						case 'Water Wash':echo 'cleaning-water-wash-services'; break;
						case 'Car Wash':echo 'cleaning-water-wash-services'; break;
						case 'Tyre Puncture':echo 'tyre-puncture-repair-shops'; break;
						case 'Vehicle Diagnostics':echo 'diagnostics-inspection-services'; break;
						default:echo 'services-repair'; break;
					}//switch?>"> <?php if($BrandModel!='') echo $BrandModel; else echo $brand;?></a></li>
                    <?php } //if locality is not null 
	                elseif($locality!='' && $brand!='' && $BrandModel=='') { ?>
                    <li>
                    <a href="https://gobumpr.com/<?php echo strtolower($city);?>/<?php if($BrandModel!='') echo strtolower(str_replace(' ','-', $BrandModel)); else echo strtolower(str_replace(' ','-', $brand));?>-<?php echo strtolower($VehicleType_model);?>-
					<?php switch($ServiceType){
						case 'General Service' :echo 'services-repair'; break;
						case 'Breakdown Assistance':echo 'roadside-breakdown-assistance'; break;
						case 'Water Wash':echo 'cleaning-water-wash-services'; break;
						case 'Car Wash':echo 'cleaning-water-wash-services'; break;
						case 'Tyre Puncture':echo 'tyre-puncture-repair-shops'; break;
						case 'Vehicle Diagnostics':echo 'diagnostics-inspection-services'; break;
						default:echo 'services-repair'; break;
					}//switch?>"> <?php if($BrandModel!='') echo $BrandModel; else echo $brand;?></a></li>
                    <?php } //if locality is not null 
					elseif($locality=='' && $brand!='' && $BrandModel!='') { ?>
                    <li><?php if($BrandModel!='') echo $BrandModel; 
							  else echo $brand; ?></li>
                    <?php } //else locality is present 
					elseif($locality=='' && $brand!='' && $BrandModel=='') { ?>
                    <li><?php if($BrandModel!='') echo $BrandModel; 
							  else echo $brand; ?></li>
                    <?php } //else locality is present ?>
                    <?php if($locality!='') { ?>
                    <li><?php echo $locality;?></li>
                    <?php }?>
                    </ul>
        </div>
    </div><!-- Position -->

<div  class="container margin_60">
            
    	<div class="row">
        	<aside class="col-lg-3 col-md-3 hidden-xs hidden-sm" id="fixthis">
        
		<div id="filters_col">

<div style="font-size:16px">Filters</div>
			<div class="collapse" id="collapseFilters">
				
<form role="form" id="filters_form" action="" method="get">

<input type="hidden" name="city" type="text" value="<?php echo $city; ?>" />
<input type="hidden" name="VehicleType" type="text" value="<?php echo $VehicleType; ?>" />
<input type="hidden" name="ServiceType" type="text" value="<?php echo $ServiceType; ?>" />
<input type="hidden" name="BrandModel" type="text" value="<?php if($BrandModel!= '') echo $BrandModel; else echo $brand;?>" />
<input type="hidden" name="locality" type="text" value="<?php echo $locality; ?>" />
<input type="hidden" name="sort_val" type="text" value="<?php echo $toggle; ?>" />
				<div class="filter_type">

					<h6 style="font-size:14px">Workshop Type</h6>
					<ul>
                   <li><input <?php if(isset($_GET['multi_branded']) && $_GET['multi_branded'] == '1') echo "checked='checked'"; ?> id="checkbox-1" class="checkbox-custom" name="multi_branded" 

type="checkbox" value="1" onClick="this.form.submit();" >
            <label for="checkbox-1" class="checkbox-custom-label">Multi-Brand Service Center</label>(<label for="private_count" id="private_count"><?php echo $count_array_pri;?></label>)</li>
                    <li><input <?php if(isset($_GET['authorised']) && $_GET['authorised'] == '1') echo "checked='checked'"; ?> id="checkbox-2" class="checkbox-custom" name="authorised" type="checkbox" 

value="1" onClick="this.form.submit();" >
            <label for="checkbox-2" class="checkbox-custom-label">Authorised Service Center</label>(<label for="auth_count" id="auth_count"><?php echo $count_array_auth;?></label>)</li>

					
                    <h6 style="font-size:14px">Pick Up</h6>       

                    <li><input <?php if(isset($_GET['pick_up']) && $_GET['pick_up'] == '1') echo "checked='checked'"; ?> id="checkbox-3" class="checkbox-custom" name="pick_up" type="checkbox" value="1" 

onClick="this.form.submit();" >
            <label for="checkbox-3" class="checkbox-custom-label">Yes</label></li>


                        
<?php if($ServiceType == 'General Service' && $VehicleType == '4w') { ?>
<h6 style="font-size:14px">Fuel Variant</h6>
                        <li><input  <?php if(isset($_GET['variant']) && $_GET['variant'] == '1') echo "checked='checked'"; ?> id="radio-3" class="radio-custom" name="variant" type="radio"  value="1" 

onChange="this.form.submit();">
            <label for="radio-3" class="radio-custom-label">Petrol</label></li>
                        <li><input  <?php if(isset($_GET['variant']) && $_GET['variant'] == '0') echo "checked='checked'"; ?> id="radio-4" class="radio-custom" name="variant" type="radio"  value="0" 

onChange="this.form.submit();">
            <label for="radio-4" class="radio-custom-label">Diesel</label></li>
<br>




<?php }//if the service type is general service for car
 ?>
					</ul>
				</div>
			</div><!--End collapse -->
			</form>

		</div><!--End filters col-->
        
        
		<div class="box_style_2">
			<i class="icon icon-24x7-phone"></i>
			<h4>Need <span>Help?</span></h4>
			<a href="tel://09003251754" class="phone">+91 90032 51754</a>
			<p>Available 24/7</p>
		</div>
		</aside><!--End aside -->
            <div class="col-lg-9 col-md-9 scrollit">
            
<div class="hidden-xs hidden-sm" id="tools">
<div class="row">
            	<div class="col-md-9 col-sm-9" style="left:5px; font-size:12px; bottom:8px; margin-bottom:-16px;">
                <h1 style="font-size:12px">
                	<label for="search_results_count"><?php echo $search_results_count;?></label> 
					<?php if(!empty($BrandModel)) { ?>
                    	 <label for="vehicle_results"><?php echo ucwords($BrandModel); ?></label> 
					<?php } else { ?>
                    	 <label for="vehicle_results"><?php echo ucwords($brand)." ";?><?php echo ucwords($VehicleType_model); ?></label> 
					<?php }//if brand model is not present ?>

					<?php if(!empty($ServiceType) && $ServiceType!='General Service') { ?>
                    	 <?php echo ucwords($ServiceType); ?> 
					<?php }//if service type is present ?>

                    Service Centers

                    <?php if(!empty($locality)) { ?>
                    	 in <label for="locality_results"><?php echo ucwords($locality); ?></label> 
                    <?php }// if locality is present 
					else { ?>
                    	 in <label for="locality_results"> <?php echo ucwords($city) ?> </label> 
                    <?php }// if locality is present ?>
                </h1>
                </div>
            	<div class="col-md-3 col-sm-3">
            	<div class="styled-select-filters" style="float:right">
                <form role="form" action="" method="get">
                    <input type="hidden" name="city" type="text" value="<?php echo $city; ?>" />
                    <input type="hidden" name="VehicleType" type="text" value="<?php echo $VehicleType; ?>" />
                    <input type="hidden" name="ServiceType" type="text" value="<?php echo $ServiceType; ?>" />
                    <input type="hidden" name="BrandModel" type="text" value="<?php if($BrandModel!= '') echo $BrandModel; else echo $brand;?>" />
                    <input type="hidden" name="locality" type="text" value="<?php echo $locality; ?>" />
                    
                    <input type="hidden" name="authorised" type="text" value="<?php echo $authorised; ?>" />
                    <input type="hidden" name="multi_branded" type="text" value="<?php echo $multi_branded; ?>" />
                    <input type="hidden" name="pick_up" type="text" value="<?php echo $pick_up; ?>" />
                    <input type="hidden" name="variant" type="text" value="<?php echo $variant; ?>" />

					<select name="sort_val" id="sort_id" class="sort_select" onChange="this.form.submit()"/>
						<option value="sort_distance" <?php if($toggle=='sort_distance') echo 'selected="selected"'; ?>>Sort by Distance</option>
						<option value="sort_price" <?php if($toggle=='sort_price') echo 'selected="selected"'; ?>>Sort by Price</option>
						<option value="sort_ratings" <?php if($toggle=='sort_ratings') echo 'selected="selected"'; ?>>Sort by Ratings</option>
					</select>
                </form>                                	
				</div>                
                </div>
</div>
</div><!--/tools -->



<div class="hidden-lg hidden-md" id="tools" style="margin-top:80px;">
<div class="row">
            	<div class="col-md-12 col-sm-12" style="left: 5px;margin-bottom: -16px;bottom: 8px;">
                <h1 style="font-size:12px">
                	<label for="search_results_count"><?php echo $search_results_count;?></label> 
					<?php if(!empty($BrandModel)) { ?>
                    	 <label for="vehicle_results"><?php echo ucwords($BrandModel); ?></label> 
					<?php } else { ?>
                    	 <label for="vehicle_results"><?php echo ucwords($brand)." ";?><?php echo ucwords($VehicleType_model); ?></label> 
					<?php }//if brand model is not present ?>

					<?php if(!empty($ServiceType) && $ServiceType!='General Service') { ?>
                    	 <?php echo ucwords($ServiceType); ?>
					<?php }//if service type is present ?>

                    Service Centers

                    <?php if(!empty($locality)) { ?>
                    	 in <label for="locality_results"><?php echo ucwords($locality); ?></label> 
                    <?php }// if locality is present 
					else { ?>
                    	 in <label for="locality_results"> <?php echo ucwords($city) ?> </label> 
                    <?php }// if locality is present ?>
                </h1>
                </div>
</div>
</div><!--/tools -->
        
    
    
<div class="show_filters_mob" style="display:none">

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3">
                   <a id="back_filters" style="position:relative"><i class="icon icon-left-arrow" style="margin:10px 10px 10px 10px;font-size:20px;"></i></a> 
                        <a href="https://gobumpr.com/main.html" style="position:absolute;"><img src="https://gobumpr.com/img/gobumpr/logo/2.png" width="160" height="34"></a>
                        
                </div>
               </div>

		<div id="filters_col">
<strong>Filters</strong>
				
<form role="form" id="filters_form_mob" action="" method="get">

<input type="hidden" name="city" type="text" value="<?php echo $city; ?>" />
<input type="hidden" name="VehicleType" type="text" value="<?php echo $VehicleType; ?>" />
<input type="hidden" name="ServiceType" type="text" value="<?php echo $ServiceType; ?>" />
<input type="hidden" name="BrandModel" type="text" value="<?php if($BrandModel!= '') echo $BrandModel; else echo $brand; ?>" />
<input type="hidden" name="locality" type="text" value="<?php echo $locality; ?>" />
<input type="hidden" name="sort_val" type="text" value="<?php echo $toggle; ?>" />
				<div class="filter_type">
<ul>
					<h6 style="font-size:14px">Workshop Type</h6>
                   <li><input <?php if(isset($_GET['multi_branded']) && $_GET['multi_branded'] == '1') echo "checked='checked'"; ?> id="checkbox-1" class="checkbox-custom" name="multi_branded" 

type="checkbox" value="1" onClick="return false;">
            <label for="checkbox-1" class="checkbox-custom-label">Multi-Brand Service Center</label>(<label for="private_count" id="private_count"><?php echo $count_array_pri;?></label>)</li>
                    <li><input <?php if(isset($_GET['authorised']) && $_GET['authorised'] == '1') echo "checked='checked'"; ?> id="checkbox-2" class="checkbox-custom" name="authorised" type="checkbox" 

value="1"  onClick="return false;" >
            <label for="checkbox-2" class="checkbox-custom-label">Authorised Service Center</label>(<label for="auth_count" id="auth_count"><?php echo $count_array_auth;?></label>)</li>

					
                    <h6 style="font-size:14px">Pick Up</h6>       

                    <li><input <?php if(isset($_GET['pick_up']) && $_GET['pick_up'] == '1') echo "checked='checked'"; ?> id="checkbox-3" class="checkbox-custom" name="pick_up" type="checkbox" value="1" 

onChange="return false;" >
            <label for="checkbox-3" class="checkbox-custom-label">Yes</label></li>


                        
<?php if($ServiceType == 'General Service' && $VehicleType == '4w') { ?>
<h6 style="font-size:14px">Fuel Variant</h6>
                        <li><input  <?php if(isset($_GET['variant']) && $_GET['variant'] == '1') echo "checked='checked'"; ?> id="radio-3" class="radio-custom" name="variant" type="radio"  value="1" 

onChange="return false;">
            <label for="radio-3" class="radio-custom-label">Petrol</label></li>
                        <li><input  <?php if(isset($_GET['variant']) && $_GET['variant'] == '0') echo "checked='checked'"; ?> id="radio-4" class="radio-custom" name="variant" type="radio"  value="0" 

onChange="return false;">
            <label for="radio-4" class="radio-custom-label">Diesel</label></li>
<br>




<?php }//if the service type is general service for car
 ?>
					</ul>
				</div>
			</div><!--End collapse -->
            
<button class="button btn" style="width:100%; bottom:0" onClick="this.form.submit();" type="submit">Apply</button>
			</form>

		</div><!--End filters col-->
        
        

<div id="content">
<?php
//print_r($sorted_array);
if(count($details)>0)
{

				foreach($sorted_array as $v)
				{
					
				?>
    			<div class="strip_all_tour_list fadeIn">
				
                   <div class="row">
                	<div class="col-lg-4 col-md-4 col-sm-4">
                    <?php 
					//to get url
					$service_centre_name = str_replace(' ', '-', $v['shop_name']);
					$service_centre_address = str_replace(' ', '-', $v['address4']);
					$url="$service_centre_name".'-'.'in'.'-'."$service_centre_address";
					$rating_val = round($v['rating'],1);
					//echo $url = urlencode($url);
					?>
                    
                    	<div class="img_list">
                        <a href="/chennai/service-center/<?php echo strtolower(urlencode($url)); ?>?price=<?php echo $v['price']; ?>">
                        <?php if($v['authorized_status'] == '1')  {?> 
                        <div class="ribbon authorized"></div>
                        <?php }//if authorized?>
                        <img src="https://gobumpr.com/img/lazyload.jpg" data-sizes="auto" data-srcset="<?php echo $v['image_300w'];?>" class="lazyload" alt="<?php echo strtolower($url); ?>">
                        <div class="short_info" style="font-size:12.5px"><?php if($v['attributes']!='') { ?><i class="icon icon-badge" style="font-size:10px"></i> <?php echo $v['attributes']; }?></div>
                        </a>
                        </div>
                    </div>
                     <div class="clearfix visible-xs-block"></div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                    		<div class="tour_list_desc">
                            <!--<div class="score"><span><?php //echo $v['rating'] ?></span></div>-->
							<?php if($v['time24X7'] == '1')  {?> 
                            <div style="display:table;" class="hidden-sm hidden-xs">
                    		<a style="display:table-cell; vertical-align: middle; width:80%;" href="/chennai/service-center/<?php echo urlencode($url); ?>?price=<?php echo $v['price']; ?>"><h2 style="display:table-cell; vertical-align:middle;"><strong><?php echo $v['shop_name']?></strong></h2></a>
                            <i class="icon icon-24x7-phone" style="float:right; color:#ffa800; display:table-cell; vertical-align:middle; margin-left:7vw; font-size:40px"></i>
                            </div>
                            <div style="margin-top:15px;" class="hidden-lg hidden-md">
                    		<a style="vertical-align: middle;" href="/chennai/service-center/<?php echo strtolower(urlencode($url)); ?>?price=<?php echo $v['price']; ?>"><h2><strong><?php echo $v['shop_name']?></strong></h2></a>
                            </div>
                            <?php }//if 24x7 
							else  {?> 
                            <div style="margin-top:15px;">
                    		<a style="vertical-align: middle;" href="/chennai/service-center/<?php echo strtolower(urlencode($url)); ?>?price=<?php echo $v['price']; ?>"><h2><strong><?php echo $v['shop_name']?></strong></h2></a>
                            </div>
                            <?php }//else ?> 
							<?php if($v['time24X7'] == '1')  {
								?> 
                            <div class="rating hidden-sm hidden-xs">
                                                             <?php if($rating_val==0) { ?>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1
                                                               elseif($rating_val>0 && $rating_val<=0.5) { ?>
                                                                <i class="icon icon-half-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1
                                                               elseif($rating_val>0.5 && $rating_val<=1) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 
                                                               elseif($rating_val>1 && $rating_val<=1.5) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-half-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 ?>
                                                                
                                                             <?php if($rating_val>1.5 && $rating_val<=2) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 
                                                               elseif($rating_val>2 && $rating_val<=2.5) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-half-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 ?>
                                                                
                                                             <?php if($rating_val>2.5 && $rating_val<=3) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 
                                                              elseif($rating_val>3 && $rating_val<=3.5) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-half-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 ?>
                                                                
                                                             <?php if($rating_val>3.5 && $rating_val<=4) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 ?>
                                                                
                                                             <?php if($rating_val>4 && $rating_val<=4.5) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-half-star voted"></i>
                                                              <?php }//if 1 ?>
                                                                
                                                             <?php if($rating_val>4.5 && $rating_val<=5) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                              <?php }//if 1 ?>
<?php 
$mec_id = $v['mec_id'];
$user_review2 = "SELECT COUNT(*) as reviews_count FROM user_rating_tbl WHERE mec_id = ('$mec_id') AND rating!='' ";

$user_review_res2 = mysqli_query($con, $user_review2);
$row2 = mysqli_fetch_array($user_review_res2);
$reviews_count = $row2['reviews_count'];
?>                    

                            <small>(<?php echo $reviews_count; ?>)</small>                                                      
                                                              
                                    </div>
                            <div style="display:table; width:100%;" class="hidden-md hidden-lg">
                            <div class="rating hidden-md hidden-lg" style="float:left; width:80%; display:table-cell; vertical-align:middle">
                                                             <?php if($rating_val==0) { ?>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1
                                                               elseif($rating_val>0 && $rating_val<=0.5) { ?>
                                                                <i class="icon icon-half-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1
                                                               elseif($rating_val>0.5 && $rating_val<=1) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 
                                                               elseif($rating_val>1 && $rating_val<=1.5) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-half-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 ?>
                                                                
                                                             <?php if($rating_val>1.5 && $rating_val<=2) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 
                                                               elseif($rating_val>2 && $rating_val<=2.5) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-half-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 ?>
                                                                
                                                             <?php if($rating_val>2.5 && $rating_val<=3) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 
                                                              elseif($rating_val>3 && $rating_val<=3.5) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-half-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 ?>
                                                                
                                                             <?php if($rating_val>3.5 && $rating_val<=4) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 ?>
                                                                
                                                             <?php if($rating_val>4 && $rating_val<=4.5) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-half-star voted"></i>
                                                              <?php }//if 1 ?>
                                                                
                                                             <?php if($rating_val>4.5 && $rating_val<=5) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                              <?php }//if 1 ?>
<?php 
$mec_id = $v['mec_id'];
$user_review2 = "SELECT COUNT(*) as reviews_count FROM user_rating_tbl WHERE mec_id = ('$mec_id') AND rating!='' ";

$user_review_res2 = mysqli_query($con, $user_review2);
$row2 = mysqli_fetch_array($user_review_res2);
$reviews_count = $row2['reviews_count'];
?>                    

                            <small>(<?php echo $reviews_count; ?>)</small>                                                      
                                                              
                                    </div>                            
                            <i class="icon icon-24x7-phone hidden-md hidden-lg" style="float:right; color:#ffa800; font-size:24px; display:table-cell; vertical-align:middle; "></i>
                            </div>
                            <?php }//if 24x7 
							else  {?> 
                            <div class="rating">
                                                             <?php if($rating_val==0) { ?>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1
                                                               elseif($rating_val>0 && $rating_val<=0.5) { ?>
                                                                <i class="icon icon-half-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1
                                                               elseif($rating_val>0.5 && $rating_val<=1) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 
                                                               elseif($rating_val>1 && $rating_val<=1.5) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-half-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 ?>
                                                                
                                                             <?php if($rating_val>1.5 && $rating_val<=2) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 
                                                               elseif($rating_val>2 && $rating_val<=2.5) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-half-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 ?>
                                                                
                                                             <?php if($rating_val>2.5 && $rating_val<=3) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 
                                                              elseif($rating_val>3 && $rating_val<=3.5) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-half-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 ?>
                                                                
                                                             <?php if($rating_val>3.5 && $rating_val<=4) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-star"></i>
                                                              <?php }//if 1 ?>
                                                                
                                                             <?php if($rating_val>4 && $rating_val<=4.5) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-half-star voted"></i>
                                                              <?php }//if 1 ?>
                                                                
                                                             <?php if($rating_val>4.5 && $rating_val<=5) { ?>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                                <i class="icon icon-filled-star voted"></i>
                                                              <?php }//if 1 ?>
<?php 
$mec_id = $v['mec_id'];
$user_review2 = "SELECT COUNT(*) as reviews_count FROM user_rating_tbl WHERE mec_id = ('$mec_id') AND rating!='' ";

$user_review_res2 = mysqli_query($con, $user_review2);
$row2 = mysqli_fetch_array($user_review_res2);
$reviews_count = $row2['reviews_count'];
?>                    

                            <small>(<?php echo $reviews_count; ?>)</small>                                                      
                                                              
                                    </div>                            
                            <?php }//else ?> 
                            <p> <i class="icon icon-map-pin" style="font-size:16px; margin-left:-5px;"></i><?php echo $v['address4']; ?> | <?php echo round($v['distance'], 1); ?> KMs away</p>
                            <p><i class="icon icon-since"></i>&nbsp;<?php echo ucwords($v['day_from']); ?> - <?php echo ucwords($v['day_to']); ?> | 
                                    	<?php echo ucwords(date('h:i A', strtotime($v['time_from']))); ?> - <?php echo ucwords(date('h:i A', strtotime($v['time_to']))); ?></p> 
                            <ul class="add_info">
                                <li>
                                     <a href="javascript:void(0);" class="tooltip-1" data-toggle="tooltip" data-placement="top" title="<?php if($v['since']==0) echo "10+ Yrs Exp."; else { ?>Since <?php echo $v['since']; }//else?>"><i class="icon icon-since"></i></a>
                               </li>
                                <li>
                                     <a href="javascript:void(0);" class="tooltip-1" data-toggle="tooltip" data-placement="top" title="<?php echo $v['no_of_mechanic'];?> Mechanics"><i class="icon icon-number-mechanics"></i></a>
                               </li>
                                <li>
                                     <a href="javascript:void(0);" class="tooltip-1" data-toggle="tooltip" data-placement="top" title="<?php echo $v['shop_size'];?> sq ft"><i class="icon icon-sqft"></i></a>
                               </li>
                                <li>
                                     <a href="javascript:void(0);" class="tooltip-1" data-toggle="tooltip" data-placement="top" title="<?php echo ucwords($v['address1']).','; ?> <?php echo ucwords($v['address2']).','; ?> <?php echo ucwords($v['address4']).','; ?> <?php echo ucwords($v['address5']).','; ?> <?php echo ucwords($v['pincode']); ?>"><i class="icon icon-map-pin"></i></a>
                               </li>
                                <li>
                                     <a href="javascript:void(0);" class="tooltip-1" data-toggle="tooltip" data-placement="top" title="Online Payment Avalilable"><i class="icon icon-pay-online"></i></a>
                               </li>
                            <!--  <li>
                            <div class="tooltip_styled tooltip-effect-4">
                            	<span class="tooltip-item"><i class="icon_set_1_icon-41"></i></span>
                                	<div class="tooltip-content"><span class="heading_tooltip">Since</span> 
                                    	<span class="leading_tooltip"></span>
                                </div>
                              </div>
                           </li> 
                            <li>
                            <div class="tooltip_styled tooltip-effect-4">
                            	<span class="tooltip-item"><i class="icon_set_1_icon-41"></i></span>
                                	<div class="tooltip-content"><span class="heading_tooltip">Number Of Mechanics </span>
                                    	
                                </div>
                              </div>
                           </li> 
                             <li>
                            <div class="tooltip_styled tooltip-effect-4">
                            	<span class="tooltip-item"><i class="icon_set_1_icon-41"></i></span>
                                	<div class="tooltip-content"><span class="heading_tooltip">Workshop Size </span>
                                    	
                                </div>
                              </div>
                           </li>
                           <li>
                            <div class="tooltip_styled tooltip-effect-4">
                            	<span class="tooltip-item"><i class="icon_set_1_icon-41"></i></span>
                                	<div class="tooltip-content"><h4>What's Included</h4>
                                    	
                                </div>
                              </div>
                           </li>
                           <li>
                            <div class="tooltip_styled tooltip-effect-4">
                            	<span class="tooltip-item"><i class="icon_set_1_icon-41"></i></span>
                                	<div class="tooltip-content"><h4>Address</h4>
                                    	<br>
                                </div>
                              </div>
                           </li> -->
                           
                            </ul>
                            </div>
                    </div>
					<div class="col-lg-2 col-md-2 col-sm-2">
                    	<div class="price_list" style="color:#000;"><div>
                        <?php  if(strlen($v['price'])>6) { ?>
                        <div style="font-size:18px">
                        <?php if($v['price']!=0 && $v['price']!=-1) { ?>
                        	Rs. <?php echo $v['price'];?>
                        <?php }
						elseif($v['price']==-1) { ?>
								Labour Free *
						<?php }
						else { ?>
								<a data-toggle="modal" data-target="#myReview" class="btn_1 btn-responsive" id="show_price" style="background-color:#909090; width:113px; font-size:11px">Show Price</a>
						<?php } ?>
                        </div>
<?php }// if double price range
else { ?>

                        <?php if($v['price']!=0 && $v['price']!=-1) { ?>
                        	Rs. <?php echo $v['price'];?>
                        <?php }
						elseif($v['price']==-1) { ?>
								Labour Free *
						<?php }
						else { ?>
								<a data-toggle="modal" data-target="#myReview" class="btn_1 btn-responsive" id="show_price" style="background-color:#909090; width:113px; font-size:11px">Show Price</a>
						<?php } ?>
<?php }//else not special cases for price ?>                       
						<br>
						<br>
                        <p><a href="/chennai/service-center/<?php echo strtolower(urlencode($url)); ?>?price=<?php echo $v['price']; ?>" class="btn_1 btn-responsive">Book Now</a></p>
                        </div>
                        </div>
                    </div>
                    </div>
				</div><!--End strip -->
				<?php }//foreach

                ?><hr><?php
}//if values are returned
else
{
?>

<div class="row">
<div class="col-md-6 hidden-xs hidden-sm" style="margin-top:100px;">
<h5 class="hidden-lg hidden-md" align="center" style="margin:0px 10px 10px 20px">Oops. Sorry! We are not able to find service providers for you :(<br><br>

<p> Please share your service requirements with us so that our team can assist you.</p></h5>
<h3 class="hidden-xs hidden-sm" align="center">Oops. Sorry! We are not able to find service providers for you :(<br><br>


<p> Please share your service requirements with us so that our team can assist you.</p></h3>
</div>

<div class="col-md-6">
        <div class="box_style_1" id="fix_booking" >
            <h3 class="inner">- Service Request -</h3>
<form role="form" method="post" id="1st_book" name="1st_book" action="">            
            <div class="row" id="1st_booking_form">
               <div class="col-md-12 col-sm-12" style="top:10px;">
                 <div class="form-group"> 
                      <label>Vehicle Model</label>
                      <input class="form-control autocomplete" id="BrandModelid" type="text" name="BrandModel" value="<?php echo $BrandModel; ?>" required placeholder="Select Model"><span id="message"></span>
  <span id="dropdown_icon" class="dd-pointer dd-pointer-down" style=" margin-right:15px; margin-top:1%"></span>
                </div>
               </div>
               <div class="col-md-12 col-sm-12">                  
                 <div class="form-group">   
                      <label>Locality</label>
                      <input class="form-control autocomplete" id="localityid" type="text" value="<?php echo $locality; ?>" name="locality" required placeholder="Select Locality"><span id="message"></span>
  <span id="dropdown_icon" class="dd-pointer dd-pointer-down" style=" margin-right:15px; margin-top:1%"></span>
                </div>
                </div>
	<div class="col-md-12 col-sm-12"> 
	<div class="form-group">
    <label> Service </label>
        <select id="ServiceType" class="ddslick form-control" name="ServiceType">
        <?php if($type=='4w') {?>
            <option <?php if($service_name_s == 'General Service') echo "selected='selected'"; ?>  value="General Service" data-imagesrc="/img/gobumpr/car_general_service.png">General Service</option>
            <option <?php if($service_name_s == 'Breakdown Assistance') echo "selected='selected'"; ?> value="Breakdown Assistance" data-imagesrc="/img/gobumpr/car_breakdown.png">Breakdown Assistance</option>
            <option <?php if($service_name_s == 'Water Wash') echo "selected='selected'"; ?>  value="Water Wash"  data-imagesrc="/img/gobumpr/car_wash.png">Car Wash</option>
            <option  <?php if($service_name_s == 'Tyre Puncture') echo "selected='selected'"; ?> value="Tyre Puncture" data-imagesrc="/img/gobumpr/car_tyre_puncture.png">Tyre Puncture</option>
            <option  <?php if($service_name_s == 'Vehicle Diagnostics') echo "selected='selected'"; ?> value="Vehicle Diagnostics" data-imagesrc="/img/gobumpr/car_vehicle_diagnostics.png">Vehicle Diagnostics</option>
            
            
         <?php }  else {?>
            <option <?php if($service_name_s == 'General Service') echo "selected='selected'"; ?>  value="General Service" data-imagesrc="/img/gobumpr/bike_general_service.png">General Service</option>
            <option <?php if($service_name_s == 'Breakdown Assistance') echo "selected='selected'"; ?> value="Breakdown Assistance" data-imagesrc="/img/gobumpr/bike_breakdown.png">Breakdown Assistance</option>
            <option <?php if($service_name_s == 'Water Wash') echo "selected='selected'"; ?>  value="Water Wash"  data-imagesrc="/img/gobumpr/bike_wash.png">Water Wash</option>
            <option  <?php if($service_name_s == 'Tyre Puncture') echo "selected='selected'"; ?> value="Tyre Puncture" data-imagesrc="/img/gobumpr/bike_tyre_puncture.png">Tyre Puncture</option>
            <option  <?php if($service_name_s == 'Vehicle Diagnostics') echo "selected='selected'"; ?> value="Vehicle Diagnostics" data-imagesrc="/img/gobumpr/bike_vehicle_diag.png">Vehicle Diagnostics</option>
           
         <?php } ?>
        </select>
	</div>
	</div>
               <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                        <label style="position: static"><i class="icon-calendar-7 "></i>Date of Service</label>
                        <input id="dos_book" class="date-pick form-control" data-error="Select a date" data-date-format="M d, D" type="text" name="dos_book">
                    </div>                                              
                </div>
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <label style="position: static">Pick up </label>
                    <select class="form-control" onchange="showDiv(this)" name="pick_up" id="pick_up">
                      <option value="1" selected>Yes</option>
                      <option value="0" selected>No</option>
                    </select>
                 </div>
                </div>
           <div id="click" style="display:none;">
               <div class="col-md-12 col-sm-12">                  
                    <label style="position: static"><i class=" icon-clock"></i>Pick Up Time</label>
                    <select class="form-control" id="pickup_time" name="pickup_time" required>
                    <option selected hidden value="0">Pick up Time</option>
                    <option value="09.00 AM - 11.00 AM">09.00 AM - 11.00 AM</option>
                    <option value="11.00 AM - 01.00 PM">11.00 AM - 01.00 PM</option>
                    <option value="01.00 PM - 03.00 PM">01.00 PM - 03.00 PM</option>
                    <option value="03.00 PM - 05.00 PM">03.00 PM - 05.00 PM</option>
                    <option value="05.00 PM - 07.00 PM">05.00 PM - 07.00 PM</option>
                    <option value="07.00 PM - 09.00 PM">07.00 PM - 09.00 PM</option>
                    </select>
                </div>
               <div class="col-md-12 col-sm-12">                  
                 <div class="form-group">
                      <label>Address</label>
                      <textarea class="form-control" name="pick_up_address" id="pick_up_address" placeholder="Enter Pickup Address"></textarea>
                </div>
                </div>
                <input type="hidden" value="<?php echo $VehicleType;?>" name="VehicleType">
      
                    </div>

            <br>
            <center><button type="submit" class="btn_full" id="1st_submit"> Book Now </button></center>
            </div>
</form>
<form role="form" id="2nd_book" name="2nd_book" method="post" action="">
            <div class="row" id="2nd_booking_form" style="display:none;">
               <div class="col-md-12 col-sm-12" style="top:10px;">
                 <div class="form-group"> 
                      <label>Registration Number</label>
                      <input class="form-control" type="text" name="registration_num_book" id="registration_num_book" data-minlength="6" maxlength="13" style="text-transform:uppercase" placeholder="TN/07/AR/6401">
                </div>
               </div>
               <div class="col-md-12 col-sm-12">
                 <div class="form-group">  
                      <label>Name</label>
                      <input class="form-control" type="text" name="name_book" id="name_book" pattern="^[a-Z\s]{3,}$"oninvalid="this.setCustomValidity('Enter atleast 3 alphabets')" 
    onchange="try{setCustomValidity('')}catch(e){}" required placeholder="Enter Name">
                </div>
               </div>
               <div class="col-md-12 col-sm-12">
                 <div class="form-group">  
                      <label>Mobile Number</label>
                      <input class="form-control" type="phone" name="mobile_num_book" id="mobile_num_book" pattern="^[789]\d{9}$" oninvalid="this.setCustomValidity('Enter valid phone number')" 
    onchange="try{setCustomValidity('')}catch(e){}"  required placeholder="Enter Mobile Number">
                </div>
               </div>
               <div class="col-md-12 col-sm-12">
                 <div class="form-group"> 
                      <label>Email</label>
                      <input class="form-control" type="email" name="email_book" id="email_book" required placeholder="Enter Email Id">
                </div>
               </div>
	<div class="col-md-12 col-sm-12">
                 <div class="form-group"> 
                      <label>Any Additional Repairs?</label>
	 <select class="form-control" onchange="showDivDesc(this)" name="desc_req" id="desc_req">
                      <option value="1" selected>Yes</option>
                      <option value="0" selected>No</option>
                    </select>                     
                </div>
               </div>
	<div class="col-md-12 col-sm-12" style="display:none;" id="description_id">
                 <div class="form-group"> 
                      <textarea class="form-control" style="height:65px" type="text" name="description" id="description" required placeholder="Optional - If there is anything that needs to be inspected/repaired additionally, please tell us here"></textarea>
                </div>
               </div>
        <center><button class="btn_full" id="2nd_submit" type="submit"> Book Now </button></center>
            </div> <!--/box_style_1 --> 
</form>
<div class="row" id="thankyou" style="display:none">
<div  class="col-md-12" style="margin-top:25%; margin-bottom:25%;">
<h4> Thank you for sharing your details with us, We will get into touch with you soon! </h4>
</div>
</div>
</div>
</div>
</div>
<?php   
}//no service provider found.
?>
</div>
                
        </div><!-- End col lg-9 -->
    </div><!-- End row -->
</div><!-- End container -->

<div class="container" style="margin-top:-55px; margin-bottom:30px;">
            <div class="row">
                <div class="col-md-3">
                <?php if($ServiceType=='') $ServiceType='General Service';?>
                	<h3><?php echo ucwords($ServiceType);?></h3>
                </div>
                <div class="col-md-9" style="margin-top:20px;">
                <?php if($ServiceType=='General Service') {
					if($VehicleType==='4w' || $type==='4w') { ?>
                    <p>
                       The price includes labour cost for general service and exterior water wash. Cost towards any extra repairs, spares/parts replacement & consumables (Engine Oil) incurred during service would be intimated prior to the start of service and will be charged accordingly 
                    </p>
                    
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <ul class="list_ok">
                            <li>Engine Oil Level &amp; Quality Check Leakage through Filter Element</li>
                            <li>Gear Oil Level &amp; Quality</li>
                            <li>Coolant Level</li>
                            <li>Brake Fluid Level &amp; Quality</li>
                            <li>Windshield Washer Fluid Level &amp; Quality</li>
                            <li>All Belts (AC, Compressor, Motor Belt etc.) Tension &amp; Condition</li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <ul class="list_ok">
                            <li>Battery Electrolyte Level &amp; Terminal</li>
                            <li>All Pedal Operation &amp; Play</li>
                            <li>All Light/Horn Operation, Switches &amp; Warning Lights</li>
                            <li>Wiper &amp; Spray Operation</li>
                            <li>Power Windows Operation</li>
                            <li>Parking Brake Operation</li>                                
                            </ul>
                        </div>
                    </div><!-- End row  -->                     
					<div class="row">
                        <div class="col-md-6 col-sm-6">
                            <ul class="list_ok">
                            <li>A/C Control Checks &amp; Cooling Effectiveness</li>
                            <li>Engine &amp; Transmission Mounting Condition</li>
                            <li>Any Oil &amp; Water Leakage</li>
                            <li>Front Drive Shaft Boot Condition</li>
                            <li>Underbody Damages / Deformation Particularly Oil Pan &amp; Fuel Tank</li>
                            <li>Exhaust System Leakage &amp; Mounting Condition</li>                                
                            </ul>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <ul class="list_ok">
                            <li>Condition of Tyres Including Spare Tyre</li>
                            <li>Tyre Pressure</li>
                            <li>Remote Control Operation For Doors, if Equipped</li>
                            <li>Child Lock Operation</li>
                            <li>Check All Doors Closing &amp; Any Noise In Doors &amp; Hinges</li>
                            <li>Door Operation</li>                                
                            </ul>
                        </div>
		
                    </div><!-- End row  -->                     
                <?php
					}//car
					elseif($VehicleType==='2w' || $type==='2w') { ?>
                    <p>
                       The price includes full general service labor cost, water wash, engine oil (&amp; gear oil) change/top-up. Costs towards any extra repair jobs & spares/parts replacement would be charged additionally 
                    </p>
                    
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <ul class="list_ok">
                            <li>Air Filter Check and Clean/Replace</li>
                            <li>Brake Shoe Check and Clean</li>
                            <li>Brake Cable Check</li>
                            <li>Carburettor / FI Check and Cleaning</li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <ul class="list_ok">
                            <li>Accelerator and Clutch Cable Check and Adjustment</li>
                            <li>Drive Belt / Chain Cleaning, Adjustment and Lubrication</li>
                            <li>Spark Plug Cleaning and Adjustment</li>
                            <li>Fuel Filter Cleaning</li>                                
                            </ul>
                        </div>
                    </div><!-- End row  -->                     
					<div class="row">
                        <div class="col-md-6 col-sm-6">
                            <ul class="list_ok">
                            <li>Head Light and Stop Light Focus Check &amp; Adjust</li>
                            <li>Battery Checkup</li>
                            <li>Battery Water Top Up</li>
                            <li>Tappet Clearance Adjustment</li>  
                            </ul>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <ul class="list_ok">
                            <li>Steering Head Bearing Check &amp; Adjust</li>
                            <li>Fork and wheel bend check</li>
                            <li>Engine Oil Change</li>
                            <li>Water Wash</li>
                            </ul>
                        </div>
		
                    </div><!-- End row  -->                     
                <?php
					}//bike
				 }// gs 
				elseif($ServiceType=='Breakdown Assistance') {
					if($VehicleType==='4w' || $type==='4w') { ?>
                    <p>
                       The price includes the labor charge for visiting your location for immediate assistance and diagnosing the problem. Costs towards any extra repair jobs & spares/parts replacement would be charged additionally 
                    </p>
                    
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <ul class="list_ok">
                            <li>Doorstep Service</li>
                            <li>On the Spot Repair Service for Minor Faults</li>
                            <li>Jumpstart In Case of Weak/Dead Battery</li>
                            <li>Towing available on demand</li>
                            </ul>
                        </div>
                    </div><!-- End row  -->
			<?php
					}//car
					elseif($VehicleType==='2w' || $type==='2w') { ?>
                    <p>
                       The price includes the labor charge for visiting your location for immediate assistance and diagnosing the problem. Costs towards any extra repair jobs & spares/parts replacement would be charged additionally 
                    </p>
                    
                    <div class="row">

                        <div class="col-md-12 col-sm-12">
                            <ul class="list_ok">
                            <li>Doorstep Service</li>
                            <li>On the Spot Repair Service for Minor Faults</li>
                            <li>Jumpstart In Case of Weak/Dead Battery</li>
                            <li>Towing available on demand</li>
                            </ul>
                        </div>

                    </div><!-- End row  -->
                <?php
					}//bike
				 }// break 
				elseif($ServiceType=='Water Wash') {
					if($VehicleType==='4w' || $type==='4w') { ?>
                    <p>
                       The price includes cost for complete exterior water wash. Costs towards any extra repair jobs & spares/parts replacement would be charged additionally 
                    </p>
                    
                    <div class="row">

                        <div class="col-md-6 col-sm-6">
                            <ul class="list_ok">
                            <li>Exterior Body Water Wash</li>
                            <li>Soft Cloth Dry Cleaning</li>
                            <li>Tyre Polish</li>
                            <li>General Interior Vacuuming</li>
                            </ul>
                        </div>

                    </div><!-- End row  -->			<?php
					}//car
					elseif($VehicleType==='2w' || $type==='2w') { ?>
                    <p>
                       The price includes cost for complete exterior water wash. Costs towards any extra repair jobs & spares/parts replacement would be charged additionally 
                    </p>
                    
                    <div class="row">

                        <div class="col-md-6 col-sm-6">
                            <ul class="list_ok">
                            <li>Exterior Body Water Wash</li>
                            <li>Soft Cloth Dry Cleaning</li>
                            <li>Tyre Polish</li>
                            <li>General Interior Vacuuming</li>
                            </ul>
                        </div>

                    </div><!-- End row  -->                
					<?php
					}//bike
					?>
                <?php
                }// wash  
				elseif($ServiceType=='Tyre Puncture') {
					if($VehicleType==='4w' || $type==='both') { ?>
                    <p>
                       The price includes the labor charge for visiting your location and fixing the puncture. Costs towards any extra repair jobs & spares/parts replacement would be charged additionally 
                    </p>
                    
                    <div class="row">

                        <div class="col-md-6 col-sm-6">
                            <ul class="list_ok">
                            <li>Doorstep Service</li>
                            <li>Tube/Tubeless Puncture</li>
                            </ul>
                        </div>

                    </div><!-- End row  -->			<?php
					}//car
					elseif($VehicleType==='2w' || $type==='both') { ?>
                    <p>
                       The price includes the labor charge for visiting your location and fixing the puncture. Costs towards any extra repair jobs & spares/parts replacement would be charged additionally 
                    </p>
                    
                    <div class="row">

                        <div class="col-md-6 col-sm-6">
                            <ul class="list_ok">
				<li>Doorstep Service</li>
				<li>Tube/Tubeless Puncture</li>
                            </ul>
                        </div>

                    </div><!-- End row  -->
					<?php
					}//bike
                     
                }// punctu
				elseif($ServiceType=='Vehicle Diagnostics') {
					if($VehicleType==='4w' || $type==='4w') { ?>
                    <p>The price includes cost for vehicle diagnostics i.e finding out the exact problem. Costs towards any extra repair jobs & spares/parts replacement would be charged additionally</p>
                    
                    <div class="row">

                        <div class="col-md-6 col-sm-6">
                            <ul class="list_ok">
                            <li>Doorstep Service</li>
                            <li>On the Spot Repair Service for Minor Faults</li>
                            </ul>
                        </div>

                    </div><!-- End row  -->			<?php
					}//car
					elseif($VehicleType==='2w' || $type==='2w') { ?>
                    <p>
                       The price includes cost for vehicle diagnostics i.e finding out the exact problem. Costs towards any extra repair jobs & spares/parts replacement would be charged additionally 
                    </p>
                    
                    <div class="row">

                        <div class="col-md-6 col-sm-6">
                            <ul class="list_ok">
                            <li>Doorstep Service</li>
                            <li>On the Spot Repair Service for Minor Faults</li>
                            </ul>
                        </div>

                    </div><!-- End row  -->               
					<?php
					}//bike
                     
                 }//Vehicle Diagonstics ?>
                </div><!-- End col-md-9  -->
            </div><!-- End row  -->
</div>
<div class="container" style="margin-top:-55px; margin-bottom:30px;">
<h3 style="font-size:16px;"> Related Searches </h3>
<ul class="related_links">
<?php

$services_url_car = array("services repair", "roadside breakdown assistance", "cleaning water wash services", "tyre puncture repair shops", "diagnostics inspection services");
$services_car_2 = array("doorstep detailing polishing services", "body repair tinkering painting services", "ac repair services", "wheel alignment balancing repair services", "get instant quotes price estimates", "buy new battery with warranty", "buy new tyres at best price");
$services_url_bike = array("services repair", "roadside breakdown assistance", "cleaning water wash services", "tyre puncture repair shops", "diagnostics inspection services");
$services_bike_2 = array( "engine oil change services", "brake chain adjustment repair services", "engine repair services", "clutch change overhaul repair services", "get instant quotes price estimates", "buy new battery with warranty", "buy new tyres at best price");
switch($ServiceType) {
	case "General Service":$ServiceType_url ='services repair';
							break;
							
	case "Breakdown Assistance":$ServiceType_url ='roadside breakdown assistance';
							break;
										
	case "Water Wash":$ServiceType_url ='cleaning water wash services';
							break;
						
	case "Car Wash":$ServiceType_url ='cleaning water wash services';
							break;
						
	case "Tyre Puncture":$ServiceType_url ='tyre puncture repair shops';
							break;
							
	case "Vehicle Diagnostics":$ServiceType_url ='diagnostics inspection services';
							break;
							
	default:$ServiceType_url ='services repair';
							break;
}
?>
<?php 
if($brand=='' && $BrandModel=='' && $locality=='') {
	$popular_brand = "SELECT distinct brand FROM vehicle_brand_model_priority WHERE type='$VehicleType' ORDER BY brand_rank LIMIT 0,16";
//echo $popular_brand;
	$sql_res = mysqli_query($con, $popular_brand) or die("error".mysqli_error($con));
//print_r($sql_res);
while ($row = mysqli_fetch_object($sql_res)) {
?>
<li class="related_links_li"><a href="https://gobumpr.com/<?php echo str_replace(" ","-", strtolower($city));?>/<?php echo str_replace(" ","-", strtolower($row->brand));?>-<?php echo strtolower($VehicleType_model);?>-<?php echo str_replace(" ", "-",$ServiceType_url);?>"> 
<?php echo $row->brand; ?> <?php echo ucwords($ServiceType_url);?>
</a>
</li>
<?php }//while 
}//if 
elseif($brand=='' && $BrandModel=='' && $locality!='') {
	$popular_brand = "SELECT distinct brand FROM vehicle_brand_model_priority WHERE type='$VehicleType' ORDER BY brand_rank LIMIT 0,16";
//echo $popular_brand;
	$sql_res = mysqli_query($con, $popular_brand) or die("error".mysqli_error($con));
while ($row = mysqli_fetch_object($sql_res)) {
?>
<li class="related_links_li">
<a href="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ","-", $row->brand));?>-<?php echo strtolower($VehicleType_model);?>-<?php echo str_replace(" ", "-",$ServiceType_url);?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>"> 
<?php echo $row->brand; ?> <?php echo ucwords($ServiceType_url);?> in <?php echo $locality;?></a>
</li>
<?php }//while 
}//if 
elseif($brand!='' && $BrandModel==''  && $locality!='') {


	$popular_brand = "SELECT brand, model FROM vehicle_brand_model_priority WHERE type='$VehicleType' AND brand='$brand' ORDER BY brand_model_rank LIMIT 0,16";
//echo $popular_brand;
	$sql_res = mysqli_query($con, $popular_brand) or die("error".mysqli_error($con));
while ($row = mysqli_fetch_object($sql_res)) {
?>
<li class="related_links_li">
<a href="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ", "-", $row->brand));?>-<?php echo strtolower(str_replace(" ", "-", $row->model));?>-<?php echo strtolower($VehicleType_model);?>-<?php echo str_replace(" ", "-",$ServiceType_url);?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>"> 
<?php echo $row->brand; ?> <?php echo $row->model; ?> <?php echo ucwords($ServiceType_url);?> in <?php echo $locality;?></a>
</li>
<?php }//while 
}//if only brand and loc are present

 
elseif($brand!='' && $BrandModel==''  && $locality=='') {


	$popular_brand = "SELECT brand, model FROM vehicle_brand_model_priority WHERE type='$VehicleType' AND brand='$brand' ORDER BY brand_model_rank LIMIT 0,16";
//echo $popular_brand;
	$sql_res = mysqli_query($con, $popular_brand) or die("error".mysqli_error($con));
while ($row = mysqli_fetch_object($sql_res)) {
?>
<li class="related_links_li">
<a href="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ", "-", $row->brand));?>-<?php echo strtolower(str_replace(" ", "-", $row->model));?>-<?php echo strtolower($VehicleType_model);?>-<?php echo str_replace(" ", "-",$ServiceType_url);?>"> 
<?php echo $row->brand; ?> <?php echo $row->model; ?> <?php echo ucwords($ServiceType_url);?></a>
</li>
<?php }//while 
}//if only brand and loc are present
elseif($brand!='' && $BrandModel!='' && $locality!='') {
if($VehicleType=='4w')
	$services_urls=$services_url_car;
else
	$services_urls=$services_url_bike;
	
	foreach($services_urls as $services) {


?>
<li class="related_links_li">
<a href="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ", "-", $brand));?>-<?php echo strtolower(str_replace(" ", "-", $model));?>-<?php echo strtolower($VehicleType_model);?>-<?php echo str_replace(" ", "-",$services);?>-in-<?php echo strtolower(str_replace(" ","-", $locality));?>"> 
<?php echo $brand; ?> <?php echo $model; ?> <?php echo ucwords($services);?> in <?php echo $locality;?></a>
</li>
<?php

	}//foreach
	
if($VehicleType=='4w')
	$services_urls_2=$services_car_2;
else
	$services_urls_2=$services_bike_2;
	
	foreach($services_urls_2 as $services) {


?>
<li class="related_links_li">
<a href="https://gobumpr.com/<?php echo strtolower($city);?>/book-now/<?php echo strtolower($VehicleType_model);?>-<?php echo str_replace(" ", "-",$services);?>"> 
<?php echo $brand; ?> <?php echo $model; ?> <?php echo ucwords($services);?> in <?php echo $locality;?></a>
</li>
<?php

	}//foreach
	
}//if 

elseif($brand!='' && $BrandModel!='' && $locality=='') {
if($VehicleType=='4w')
	$services_urls=$services_url_car;
else
	$services_urls=$services_url_bike;

	
	foreach($services_urls as $services) {


?>
<li class="related_links_li">
<a href="https://gobumpr.com/<?php echo strtolower($city);?>/<?php echo strtolower(str_replace(" ", "-", $brand));?>-<?php echo strtolower(str_replace(" ", "-", $model));?>-<?php echo strtolower($VehicleType_model);?>-<?php echo str_replace(" ", "-",$services);?>"> 
<?php echo $brand; ?> <?php echo $model; ?> <?php echo ucwords($services);?></a>
</li>
<?php

	}//foreach

if($VehicleType=='4w')
	$services_urls_2=$services_car_2;
else
	$services_urls_2=$services_bike_2;

	
	foreach($services_urls_2 as $services) {

?>
<li class="related_links_li">
<a href="https://gobumpr.com/<?php echo strtolower($city);?>/book-now/<?php echo strtolower($VehicleType_model);?>-<?php echo str_replace(" ", "-",$services);?>"> 
<?php echo $brand; ?> <?php echo $model; ?> <?php echo ucwords($services);?></a>
</li>
<?php

	}//foreach

}//if ?>

</ul>
</div>


<input id="vehicle_type" value="<?php echo $VehicleType; ?>"type="hidden">

<div class="modal fade" id="myReview" tabindex="-1" role="dialog" aria-labelledby="myReviewLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myReviewLabel" align="center">To serve you better, please select your vehicle model</h4>
			</div>
			<div class="modal-body">
				<div id="message-review">
				</div>
<form method="get" accept-charset="utf-8" role="form" id="SearchForm" action="/search.php" class="navbar-form">
<div class="row">
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
 <div class="form-group">
        <div class="form-inline header-form">
        <div class="ui-widget">
          <input id="BrandModel_pop_up" type="search" name="BrandModel" class="ui-autocomplete form-control" placeholder="Select Model" Value="<?php if($BrandModel!= '') echo $BrandModel; else echo $brand;?>"><span id="message"></span>
        </div>
        </div>
 </div>
</div>
<input type="hidden" value="<?php echo $vehicle_type;?>" id="VehicleType" name="VehicleType">
<input type="hidden" value="<?php echo $service_name_s;?>" id="ServiceType" name="ServiceType">
<input type="hidden" value="<?php echo $locality_s;?>" id="locality" name="locality"/>
<input type="hidden" value="<?php echo $city;?>" id="city" name="city"/>
<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12" style="margin-top:2px;">
 <div class="form-group" align="center">
    <button type="submit" class="btn_1" style="vertical-align:middle">
    <i class="icon icon-search"></i>
    </button>
 </div>
</div>
</div>
</form>
			</div>
		</div>
	</div>
</div><!-- End modal review -->
	    <footer>
        <div class="container hidden-sm hidden-xs">
            <div class="main_title">
                <h2 style="color:#fff"><span>Brands</span> Serviced by us</h2>
                <p>
                    <a href="https://gobumpr.com/chennai/">Choose from OEM Authorized Service Centers, Multi Brand Garages and Trusted Local Mechanics in Chennai</a>
                </p>
            </div>
	<div id="tabs2" class="tabs">
				<nav>
					<ul>
						<li><a href="#section-3"><i class="icon icon-car" style="font-size: 20px;color: #fff;"></i>&nbsp;<span style="color:#fff;">Car</span></a></li>
						<li><a href="#section-4"><i class="icon icon-motorbike" style="font-size: 20px;color: #fff;"></i>&nbsp;<span style="color:#fff;">Bike</span></a></li>
					</ul>
				</nav>
	<div class="content" style="margin-top:-20px; margin-bottom:-70px">
	<section id="section-3">
            <div class="row">
                <div class="col-md-2">
                    <ul>
                    <li><a href="https://gobumpr.com/chennai/maruti-car-services-repair">Maruti</a></li>
                    <li><a href="https://gobumpr.com/chennai/honda-car-services-repair">Honda</a></li>
                    <li><a href="https://gobumpr.com/chennai/hyundai-car-services-repair">Hyundai</a></li>
                    <li><a href="https://gobumpr.com/chennai/chevrolet-car-services-repair">Chevrolet</a></li>
                    <li><a href="https://gobumpr.com/chennai/ford-car-services-repair">Ford</a></li>
                    <li><a href="https://gobumpr.com/chennai/fiat-car-services-repair">Fiat</a></li>
                    <li><a href="https://gobumpr.com/chennai/volkswagen-car-services-repair">Volkswagen</a></li>
	</ul>
                </div>
                <div class="col-md-2">
                    <ul>
	  <li><a href="https://gobumpr.com/chennai/toyota-car-services-repair">Toyota</a></li>
                    <li><a href="https://gobumpr.com/chennai/tata-car-services-repair">Tata</a></li>
                    <li><a href="https://gobumpr.com/chennai/skoda-car-services-repair">Skoda</a></li>
                    <li><a href="https://gobumpr.com/chennai/renault-car-services-repair">Renault</a></li>
                    <li><a href="https://gobumpr.com/chennai/nissan-car-services-repair">Nissan</a></li>
                    <li><a href="https://gobumpr.com/chennai/mercedes-benz-car-services-repair">Mercedes Benz</a></li>
                    <li><a href="https://gobumpr.com/chennai/mahindra-car-services-repair">Mahindra</a></li>                    
	</ul>
                </div>

                <div class="col-md-2">
                    <ul>
                    <li><a href="https://gobumpr.com/chennai/mahindra-ssangyong-car-services-repair">Mahindra Ssangyong</a></li>
                    <li><a href="https://gobumpr.com/chennai/mitsubishi-car-services-repair">Mitsubishi</a></li>
                    <li><a href="https://gobumpr.com/chennai/audi-car-services-repair">Audi</a></li>
                    <li><a href="https://gobumpr.com/chennai/hindustan-motors-car-services-repair">Hindustan Motors</a></li>
                    <li><a href="https://gobumpr.com/chennai/bmw-car-services-repair">BMW</a></li>
                    <li><a href="https://gobumpr.com/chennai/bentley-car-services-repair">Bentley</a></li>
                    <li><a href="https://gobumpr.com/chennai/ashok-leyland-car-services-repair">Ashok Leyland</a></li>
	</ul>
                </div>
                <div class="col-md-2">
                    <ul>
	  <li><a href="https://gobumpr.com/chennai/opel-car-services-repair">Opel</a></li>
                    <li><a href="https://gobumpr.com/chennai/caterham-car-services-repair">Caterham</a></li>
                    <li><a href="https://gobumpr.com/chennai/datsun-car-services-repair">Datsun</a></li>
                    <li><a href="https://gobumpr.com/chennai/dc-car-services-repair">DC</a></li>
                    <li><a href="https://gobumpr.com/chennai/ferrari-car-services-repair">Ferrari</a></li>
                    <li><a href="https://gobumpr.com/chennai/force-car-services-repair">Force</a></li>
                    <li><a href="https://gobumpr.com/chennai/icml-car-services-repair">ICML</a></li> 
	</ul>
                </div>
                <div class="col-md-2">
                    <ul>
                    <li><a href="https://gobumpr.com/chennai/isuzu-car-services-repair">Isuzu</a></li>
                    <li><a href="https://gobumpr.com/chennai/jaguar-car-services-repair">Jaguar</a></li>
                    <li><a href="https://gobumpr.com/chennai/lamborghini-car-services-repair">Lamborghini</a></li>
                    <li><a href="https://gobumpr.com/chennai/land-rover-car-services-repair">Land Rover</a></li>
                    <li><a href="https://gobumpr.com/chennai/mahindra-renault-car-services-repair">Mahindra Renault</a></li>
                    <li><a href="https://gobumpr.com/chennai/maserati-car-services-repair">Maserati</a></li>
	</ul>
                </div>
                <div class="col-md-2">
                    <ul>
                    <li><a href="https://gobumpr.com/chennai/mini-car-services-repair">Mini</a></li>
                    <li><a href="https://gobumpr.com/chennai/aston-martin-car-services-repair">Aston Martin</a></li>
                    <li><a href="https://gobumpr.com/chennai/porsche-car-services-repair">Porsche</a></li>
                    <li><a href="https://gobumpr.com/chennai/premier-car-services-repair">Premier</a></li>
                    <li><a href="https://gobumpr.com/chennai/rolls-royce-car-services-repair">Rolls Royce</a></li>
                    <li><a href="https://gobumpr.com/chennai/san-motors-car-services-repair">San Motors</a></li>
                    <li><a href="https://gobumpr.com/chennai/volvo-car-services-repair">Volvo</a></li> 
	</ul>
                </div>
            </div><!-- End row -->
	</section>
	<section id="section-4">
            <div class="row">
                <div class="col-md-2">
                    <ul>
                    <li><a href="https://gobumpr.com/chennai/bajaj-bike-services-repair">Bajaj</a></li>
                    <li><a href="https://gobumpr.com/chennai/aprilia-bike-services-repair">Aprilia</a></li>
                    <li><a href="https://gobumpr.com/chennai/hero-motocorp-bike-services-repair">Hero MotoCorp</a></li>
                    <li><a href="https://gobumpr.com/chennai/honda-bike-services-repair">Honda</a></li>
                    <li><a href="https://gobumpr.com/chennai/ktm-bike-services-repair">KTM</a></li>
</ul>
</div>
        <div class="col-md-2">
                    <ul>
                    <li><a href="https://gobumpr.com/chennai/mahindra-bike-services-repair">Mahindra</a></li>
                    <li><a href="https://gobumpr.com/chennai/hero-honda-bike-services-repair">Hero Honda</a></li>
                    <li><a href="https://gobumpr.com/chennai/peugeot-bike-services-repair">Peugeot</a></li>
                    <li><a href="https://gobumpr.com/chennai/royal-enfield-bike-services-repair">Royal Enfield</a></li>
                    <li><a href="https://gobumpr.com/chennai/suzuki-bike-services-repair">Suzuki</a></li>
                    <li><a href="https://gobumpr.com/chennai/yamaha-bike-services-repair">Yamaha</a></li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul>
                    <li><a href="https://gobumpr.com/chennai/tvs-bike-services-repair">TVS</a></li>
                    <li><a href="https://gobumpr.com/chennai/bmw-bike-services-repair">BMW</a></li>
                    <li><a href="https://gobumpr.com/chennai/dsk-benelli-bike-services-repair">DSK Benelli</a></li>
                    <li><a href="https://gobumpr.com/chennai/ducati-bike-services-repair">Ducati</a></li>
                    <li><a href="https://gobumpr.com/chennai/eider-bike-services-repair">Eider</a></li>
</ul>
</div>
              <div class="col-md-2">
                    <ul>
                    <li><a href="https://gobumpr.com/chennai/harley-davidson-bike-services-repair">Harley Davidson</a></li>
                    <li><a href="https://gobumpr.com/chennai/hyosung-bike-services-repair">Hyosung</a></li>
                    <li><a href="https://gobumpr.com/chennai/indian-motorcycle-bike-services-repair">Indian Motorcycle</a></li>
                    <li><a href="https://gobumpr.com/chennai/kawasaki-bike-services-repair">Kawasaki</a></li>
                    <li><a href="https://gobumpr.com/chennai/keeway-bike-services-repair">Keeway</a></li>
                    <li><a href="https://gobumpr.com/chennai/kinetic-bike-services-repair">Kinetic</a></li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul>
                    <li><a href="https://gobumpr.com/chennai/lml-bike-services-repair">LML</a></li>
                    <li><a href="https://gobumpr.com/chennai/lohia-bike-services-repair">Lohia</a></li>
                    <li><a href="https://gobumpr.com/chennai/moto-guzzi-bike-services-repair">Moto Guzzi</a></li>
                    <li><a href="https://gobumpr.com/chennai/mv-agusta-bike-services-repair">MV Agusta</a></li>
                    <li><a href="https://gobumpr.com/chennai/palatino-bike-services-repair">Palatino</a></li>
</ul>
</div>
              <div class="col-md-2">
                    <ul>
                    <li><a href="https://gobumpr.com/chennai/regal-raptor-bike-services-repair">Regal Raptor</a></li>
                    <li><a href="https://gobumpr.com/chennai/terra-bike-services-repair">Terra</a></li>
                    <li><a href="https://gobumpr.com/chennai/triumph-bike-services-repair">Triumph</a></li>
                    <li><a href="https://gobumpr.com/chennai/um-motorcycles-bike-services-repair">UM Motorcycles</a></li>
                    <li><a href="https://gobumpr.com/chennai/victory-bike-services-repair">Victory</a></li>
                    </ul>
                </div>
            </div><!-- End row -->
	</section>
					
	</div><!-- /content -->
	</div><!-- End tabs -->
     
        </div><!-- End container -->
 
   <div class="container">
<div class="row" style="margin-top: 30px; border-top: 1px solid rgba(255,255,255,0.2); padding-top: 30px;">
	<div class="col-md-8 hidden-sm hidden-xs">
                    <div id="social_footer">
                 <ul>
    
	<li style="display:inline-block; float:left; margin-right:12em;"><a href="tel://09003251754"><i class="icon icon-mobile" aria-hidden="true"></i><span style="margin-left: 20px; position: absolute;font-size:12px; color:#fff !important;">+91 90032 51754</span></a></li>

	<li style="display:inline-block; float:left; margin-right:14em;"><a href="mailto:hello@gobumpr.com"><i class="icon icon-email" aria-hidden="true"></i><span style="margin-left: 20px; position: absolute;font-size:12px; color:#fff !important;">hello@gobumpr.com</span></a></li>
	
	<li style="display:table-cell; float:left; margin-right:10em;"><a href="https://gobumpr.com/blog" target="_blank"><i class="icon icon-blog" aria-hidden="true"></i><span style="margin-left: 20px; position: absolute;font-size:12px; color:#fff !important;">Read our Blog</span></a></li>

	<li style="display:table-cell; float:left; margin-right:10em;"><a href="https://play.google.com/store/apps/details?id=com.northerly.bumpr" target="_blank"><i class="icon icon-android" aria-hidden="true"></i><span style="margin-left: 20px; position: absolute;font-size:12px; color:#fff !important;"> Download App</span></a></li>

	</ul>
                 </div>
                </div>



                <div class="col-md-2 hidden-lg hidden-md">
                    <div id="social_footer">
                 <ul>
	<li style="display:table-cell;"><a href="tel://09003251754"><i class="icon icon-mobile" aria-hidden="true"></i><span style="margin-left: 20px; position: absolute;font-size:12px; color:#fff !important;">+91 90032 51754</span></a></li>
	</ul>
                 </div>
                </div>


                <div class="col-md-2 hidden-lg hidden-md">
                    <div id="social_footer">
                 <ul>
	<li style="display:table-cell;"><a href="mailto:hello@gobumpr.com"><i class="icon icon-email" aria-hidden="true"></i><span style="margin-left: 20px; position: absolute;font-size:12px; color:#fff !important;">hello@gobumpr.com</span></a></li>
	</ul>
                 </div>
                </div>

                <div class="col-md-2 hidden-lg hidden-md">
                    <div id="social_footer">
                 <ul>
	<li style="display:table-cell;"><a href="https://gobumpr.com/blog" target="_blank"><i class="icon icon-blog" aria-hidden="true"></i><span style="margin-left: 20px; position: absolute;font-size:12px; color:#fff !important;">Read our Blog</span></a></li>
	</ul>
                 </div>
                </div>

                <div class="col-md-2 hidden-lg hidden-md">
                    <div id="social_footer">
                 <ul>
	<li style="display:table-cell;"><a href="https://play.google.com/store/apps/details?id=com.northerly.bumpr" target="_blank"><i class="icon icon-android" aria-hidden="true"></i><span style="margin-left: 20px; position: absolute;font-size:12px; color:#fff !important;"> Download App</span></a></li>
	</ul>
                 </div>
                </div>
               
                <div class="col-md-4">
                    <div id="social_footer">
                        <ul>
                            <li><a href="https://facebook.com/gobumprapp" target="_blank"><i class="icon icon-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="https://twitter.com/gobumpr" target="_blank"><i class="icon icon-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="https://plus.google.com/+GobumprApp" target="_blank"><i class="icon icon-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="https://instagram.com/letsgobumpr/" target="_blank"><i class="icon icon-instagram" aria-hidden="true"></i></a></li>
                            <!--<li><a href="#"><i class="icon-pinterest"></i></a></li>
                            <li><a href="#"><i class="icon-vimeo"></i></a></li>-->
                            <li><a href="https://youtube.com/watch?v=rQqFgE191SA" target="_blank"><i class="icon icon-youtube" aria-hidden="true"></i></a></li>
                            <li><a href="https://linkedin.com/company/gobumpr-" target="_blank"><i class="icon icon-linkedin" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
	<p align="center" style="color:#8c8c8c; margin-bottom:10px !important;">© 2016 - Northerly Automotive Solutions Private Limited. All rights reserved. <a href="https://gobumpr.com/terms.html" title="GoBumpr Terms of Service"> Terms of Service</a></p>
            </div><!-- End row -->
        </div><!-- End container -->
    </footer><!-- End footer -->

	<script src="https://code.jquery.com/jquery-1.10.2.min.js" integrity="sha256-C6CB9UYIS9UJeqinPHWTHVqh/E1uhG5Twh+Y5qFQmYg=" crossorigin="anonymous"></script>
    <script>
$(window).load(function() {
	var vehicle_tab = <?php echo json_encode($VehicleType);?>;
	vechile_tab = vehicle_tab.replace(/\"/g, "");
	//alert(vehicle_tab.replace(/\"/g, ""));
	if(vehicle_tab === '4w') {
    	$("tabs2 ul li:first").addClass("tab-current");
	}
	if(vehicle_tab === '2w') {
    	$("tabs2 ul li:first").removeClass("tab-current");
	}
});
	
</script>
<script src="/js/lazysizes.min.js" async=""></script>
<script type="text/javascript">
$(window).on('load', function(){
     $('.big-bg').addClass('lazyload');
});
</script>

<script>
$("#filter_btn_mob").click(function(){
    $("header").hide();
    $(".show_filters_mob").show();
});

$("#back_filters").click(function(){
    $(".show_filters_mob").hide();
    $("header").show();
});
</script>
<script>
$(function() {
	var itemValue = $('select[name=sort_select_mob]').val();
	//alert("localstorage val : "+itemValue);
	switch(itemValue) {
		case 'sort_price' : var val_mob_radio = 'price';
							$('#price').addClass('select_mob').siblings().removeClass("select_mob");
							break;
		case 'sort_distance' : var val_mob_radio = 'dist';
							$('#dist').addClass('select_mob').siblings().removeClass("select_mob");
							break;
		case 'sort_ratings' : var val_mob_radio = 'ratings';
							$('#ratings').addClass('select_mob').siblings().removeClass("select_mob");
							break;
	}
	


	$('input[type="radio"].mob_radio_btn').on('change', function (e) {
		var val_mob_radio = $('input[name="mob_radio"]:checked').val();
		//alert(val_mob_radio);
			switch(val_mob_radio) {
			
				case 'dist':$('#sort_id').val('sort_distance').trigger('change');
							if($("#sort_id option[value='sort_distance']:selected"))
								$('#dist').addClass('select_mob').siblings().removeClass("select_mob");
								
							break;
				case 'price':$('#sort_id').val('sort_price').trigger('change');
						if($("#sort_id option[value='sort_price']:selected"))
							$('#price').addClass('select_mob').siblings().removeClass("select_mob");
						
						break;
				case 'ratings':$('#sort_id').val('sort_ratings').trigger('change');
						if($("#sort_id option[value='sort_ratings']:selected"))
							$('#ratings').addClass('select_mob').siblings().removeClass("select_mob");
						break;
			}
    });
});

$(window).scroll(function () {
   if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10) {
      //alert('end of page');
   }
});
</script>
<script type="text/javascript">
	
function r1() {
  $('#r1').trigger('click');
}
			
function r2() {
  $('#r2').trigger('click');
}
			
function r3() {
  $('#r3').trigger('click');
}

</script>

<script> 
$(document).ready(function(){
	var count_array_pri = <?php echo json_encode($count_array_pri); ?>;
			jQuery("label[for='private_count']").html(count_array_pri);

	var count_array_auth = <?php echo json_encode($count_array_auth); ?>;
			jQuery("label[for='auth_count']").html(count_array_auth);
			
	var pickup_yes = <?php echo json_encode($pickup_yes); ?>;
			jQuery("label[for='pickup_yes']").html(pickup_yes);

	var search_results_count = <?php echo json_encode($search_results_count); ?>;
			jQuery("label[for='search_results_count']").html(search_results_count);

			
	var petrol_count = <?php echo json_encode($petrol_count); ?>;
			
	var diesel_count = <?php echo json_encode($diesel_count); ?>;
		
if( count_array_pri === '0' || count_array_pri==='' || count_array_pri===null)
	$("input[name=multi_branded]").attr("disabled", true);
if(count_array_auth === '0' || count_array_auth==='' || count_array_auth===null)
    $("input[name=authorised]").attr("disabled", true);
	
if(pickup_yes === '0' || pickup_yes==='' || pickup_yes===null)
    $("input[name=pick_up]#checkbox-3").attr("disabled", true);
	
if(diesel_count === '0' || diesel_count==='' || diesel_count===null)
    $("input[name=variant]#radio-4").attr("disabled", true);
if(petrol_count === '0' || petrol_count==='' || petrol_count===null)
    $("input[name=variant]#radio-3").attr("disabled", true);

});

</script>                
 
 <script>
$(function() {
		    $( "#BrandModelid" ).autocomplete({

source: function(request, response) {
            $.ajax({
                url: "/getBrands.php",
                data: {
                    term: request.term,
					extraParams:$('#vehicle_type').val()
                },
                dataType: 'json'
            }).done(function(data) {
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
        delay: 2,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script>


<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>


<script>
$(function() {
	$.ajax({
                url: "/header.php",
                data: {
                    term: request.term
                },
                dataType: 'json'
	}).
	done(function(data) {
		//as
		});
}
</script>
    <script type="text/javascript" src="/js/inputmask/inputmask.js"></script>
	<script type="text/javascript" src="/js/inputmask/inputmask.extensions.js"></script>
	<script type="text/javascript" src="/js/inputmask/jquery.inputmask.js"></script>
	<script type="text/javascript" src="/null_handler/null_results.js"></script>

<script>
$(document).ready(function(){
  $('#registration_num_book').inputmask("aa/9{1,2}/a{1,2}/9{1,4}"); //mask with dynamic syntax
});
</script><!-- Masking for registration num
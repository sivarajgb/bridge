<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id']))) {
	header('location:logout.php');
	die();
}
?> 

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>GoBumpr Bridge</title>


  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

<!-- date picker -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<!-- date time picker -->
<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="css/bootstrap-datetimepicker-standalone.css">

<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- export data -->
<script type="text/javascript" src="js/jquery.table2excel.js"></script>

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>
<style>
/* home page blocks */
.floating-box {
 display: inline-block;
 margin: 22px;
 padding: 13px;
 width:140px;
 height:95px;
 box-shadow: 0px 1px 1px 0px rgba(0,0,0,0.2);
  font-size: 18px;
}
.card-color{
    position: relative;
    top: -14px;
    left: -13px;
    width: 140px;
    height: 6px;
    box-shadow: 0px 1px 1px 0px rgba(0,0,0,0.2);    
    border-radius: 2px 2px 0 0;
}
 .datepicker {
	  cursor:pointer;
  }
.datepicker:before {
content: '';
display: inline-block;
border-left: 7px solid transparent;
border-right: 7px solid transparent;
border-top: 7px solid #ccc;
border-bottom-color: transparent !important;
position: absolute;
top: -7px;
left: 190px;  // I made a change here 
}
/* Style the tab */
div.tab {
    align:center;
    width:80%;
    overflow: hidden;
    border: 1px solid #E0F2F1;
    background-color: #E0F2F1;
}

/* Style the buttons inside the tab */
div.tab button {

    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
}

/* Change background color of buttons on hover */
div.tab button:hover {
    background-color: #4DB6AC;
}

/* Create an active/current tablink class */
div.tab button.active {
    background-color: #26A69A;
    color:#fff;
}

/* Style the tab content */
.tabcontent {
    align:center;
    width:80%;
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}

.tabcontent {
    -webkit-animation: fadeEffect 1s;
    animation: fadeEffect 1s; /* Fading effect takes 1 second */
}
/* nested tabs  */
/* Style the inside tab */
div.tab1 {
    align:center;
    width:94%;
    overflow: hidden;
    border: 1px solid #E0F2F1;
    background-color: #E0F2F1;
}

/* Style the buttons inside the tab */
div.tab1 button {

    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
}

/* Change background color of buttons on hover */
div.tab1 button:hover {
    background-color: #4DB6AC;
}

/* Create an active/current tablink class */
div.tab1 button.active {
    background-color: #26A69A;
    color:#fff;
}

/* Style the tab content */
.tabcontent1 {
    align:center;
    width:94%;
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}

.tabcontent1 {
    -webkit-animation: fadeEffect 1s;
    animation: fadeEffect 1s; /* Fading effect takes 1 second */
}
/* nested tabs in services tab */
/* Style the inside tab */
div.tab2 {
    align:center;
    width:94%;
    overflow: hidden;
    border: 1px solid #E0F2F1;
    background-color: #E0F2F1;
}

/* Style the buttons inside the tab */
div.tab2 button {

    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
}

/* Change background color of buttons on hover */
div.tab2 button:hover {
    background-color: #4DB6AC;
}

/* Create an active/current tablink class */
div.tab2 button.active {
    background-color: #26A69A;
    color:#fff;
}

/* Style the tab content */
.tabcontent2 {
    align:center;
    width:94%;
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}

.tabcontent2 {
    -webkit-animation: fadeEffect 1s;
    animation: fadeEffect 1s; /* Fading effect takes 1 second */
}

@-webkit-keyframes fadeEffect {
    from {opacity: 0;}
    to {opacity: 1;}
}

@keyframes fadeEffect {
    from {opacity: 0;}
    to {opacity: 1;}
}
.label {
    line-height: 2 !important;
}
.bootstrap-tagsinput{
  display: inline-flex !important;
}
.label-info{
  background-color: #4DB6AC !important;
}

</style>

</head>
<body>
<?php include_once("header.php"); ?>
<script>
	$(document).ready(function(){
		$('#city').show();
	})
</script>
<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>
<div id="div1" class="navbar-fixed-top" style="margin-top:50px;padding-left:25px;background-color:#fff;padding-top:15px;" align="center" >
  <div class="form-group" style="float:left;width:90px; padding:8px;font-size:17px;">
  <label>From : </label>
  </div>

   <!-- start date -->
    <div class="form-group" style="float:left;width:150px; padding:5px;" title="Start Date" id="stdt">
       <input class="form-control datepicker " data-date-format='dd-mm-yyyy' type="text" id="start_date" name="start_date">
   </div>
   <div class="form-group" style="float:left;width:150px; padding:5px;" title="Start Time">
     <div class='input-group date' id='datetimepicker1'>
          <input type='text' class="form-control" name="start_time" id="start_time"/>
          <span class="input-group-addon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
      </div>
  </div>
 <div class="form-group" style="float:left;width:90px; padding:8px;font-size:17px;">
   <label>To : </label>
 </div>
 <!-- end date -->
  <div class="form-group" style="float:left;width:150px; padding:5px;" title="End Date" id="endt">
       <input class="form-control datepicker " data-date-format='dd-mm-yyyy' type="text" id="end_date" name="end_date">
  </div>
  <div class="form-group" style="float:left;width:150px; padding:5px;" title="End Time">
    <div class='input-group date' id='datetimepicker2'>
          <input type='text' class="form-control" name="end_time" id="end_time"/>
          <span class="input-group-addon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
    </div>
  </div>

<div class="form-group" style="float:left;width:100px; padding:5px;" title="Filter Out">
      <button class="btn btn-md" id="submit" style="background-color:#26A69A;color:#fff;">Go!</button>
</div>
<div class="form-group" style="float:left;width:200px; padding:5px;" title="Help">
      <button class="btn btn-md" id="help" data-toggle="modal" data-target="#myModal_help" style="background-color:#26A69A;color:#fff;"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Help</button>
</div>
<img src="images/icon-testing.png" width="40" height="40" alt="Beta Testing"/>

</div>
<!-- Modal -->
<div class="modal fade" id="myModal_help" role="dialog" >
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Conversion Panel</h3>
      </div>
      <div class="modal-body">
      <p style="font-size:16px;color:#4E342E;"><i class="fa fa-ravelry" aria-hidden="true" style="color:#BCAAA4;"></i> The Bridge conversion panel shows you the life cycle of Leads in three different views. Follow the description below <i class="fa fa-arrow-down" aria-hidden="true" style="color:#FFD54F;"></i> to understand the terminology used in this panel:</p>
      <ul>
      <li><p> <span style="color:#004D40;"><strong>Leads</strong></span> : <span style="color:#00838F;">Newly acquired bookings(from Facebook, Google, App, Website...)</span></p></li>
      <li><p> <span style="color:#004D40;"><strong>GoAxled</strong></span> : <span style="color:#00838F;">Leads which are pushed to Axle.</span></p></li>
      <li><p> <span style="color:#004D40;"><strong>Followups</strong></span> : <span style="color:#00838F;">Customers that require a call back/RNR.</span></p></li>
      <li><p> <span style="color:#004D40;"><strong>Cancelled</strong></span> : <span style="color:#00838F;">Includes test bookings, duplicates & Enquiries.</span></p></li>
      <li><p> <span style="color:#004D40;"><strong>Others</strong></span> : <span style="color:#00838F;">If the customer is not responding to the calls or he is not interested,the boooking is pushed to Others bucket.</span></p></li>
      <li><p> <span style="color:#004D40;"><strong>Idle</strong></span> : <span style="color:#00838F;">Bookings on which no action has been taken or left idle.</span></p></li>
      </ul>
      <p style="color:#4E342E;"><i class="fa fa-ravelry" aria-hidden="true" style="color:#BCAAA4;"></i> The Non-Conversion tab gives a detailed view on the reason behind each non converted booking.</p>
      <p style="color:#4E342E;"><i class="fa fa-ravelry" aria-hidden="true" style="color:#BCAAA4;"></i> Click on any count value to get the list of bookings corresponding to that particular value.</p>
      </div> <!-- modal body -->
    </div> <!-- modal content -->
  </div>  <!-- modal dailog -->
</div>  <!-- modal -->

<div id="view">
</div> 
  <!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>


<!-- jQuery library -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- date picker -->
<script>
var date = new Date();
date.setDate(date.getDate());
var date2 = new Date();
date2.setDate(date2.getDate()-1);
$('#start_date').datepicker({
    endDate: date,
    autoclose: true,
    orientation: 'auto'
});
$('#start_date').datepicker('setDate', date2);
$('#end_date').datepicker({
    endDate: date,
    autoclose: true,
    orientation: 'auto'
});
$('#end_date').datepicker('setDate', 'today');
</script>
<!-- date time picker -->
<script type="text/javascript">
   $(function() {
    var dateNow = new Date();
   /* $('#datetimepicker1').datetimepicker({ format: 'HH:mm:ss',step:15}).val("00:00:00");*/
    $('#datetimepicker1').datetimepicker({
      format: 'hh:mm a',
      //defaultDate: dateNow
    });
   $('#start_time').val('09:00 pm');
    /*$('#datetimepicker2').datetimepicker({
      format: 'HH:mm:ss',
      defaultDate:dateNow
    });*/
    $('#datetimepicker2').datetimepicker({
      format: 'hh:mm a',
      //defaultDate:dateNow
    });
    $('#end_time').val('09:00 pm');
  });

</script>

<script>
function openTab(evt, vehicle) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(vehicle).style.display = "block";
    evt.currentTarget.className += " active";
    //console.log(vehicle);
}
</script>

<script>
function defaultview(){
	var startDate = $('#start_date').val();
	var endDate = $('#end_date').val();
  var startTime = $('#start_time').val();
  var endTime = $('#end_time').val();
  var city = $('#city').val();
  
	//Make AJAX request, using the selected value as the POST

			  $.ajax({
	            url : "ajax/conversion_panel_view_test.php",  // create a new php page to handle ajax request
	            type : "POST",
	            data : {"startdate": startDate , "enddate": endDate , "starttime": startTime , "endtime": endTime, "city":city },
	            success : function(data) {
					    //alert(data);
					//console.log(data);
          $('#view').show();
					$("#loading").hide();
				  $('#view').html(data);
          totalcount();
       },
	          error: function(xhr, ajaxOptions, thrownError) {
	           // alert(xhr.status + " "+ thrownError);
	         }
				 });
}
</script>
<script>
function totalcount(){
	var startDate = $('#start_date').val();
	var endDate = $('#end_date').val();
  var startTime = $('#start_time').val();
  var endTime = $('#end_time').val();
  var city = $('#city').val();

   $.ajax({
	            url : "ajax/conversion_panel_count.php",  // create a new php page to handle ajax request
	            type : "POST",
	            data : {"startdate": startDate , "enddate": endDate , "starttime": startTime , "endtime": endTime, "city":city },
	            success : function(data) {
					    //alert(data);
					    //console.log(data);
				      $('#count').html(data);
       },
	          error: function(xhr, ajaxOptions, thrownError) {
	           // alert(xhr.status + " "+ thrownError);
	         }
				 });
}
</script>

<script>
$(document).ready( function (){
  $('#view').hide();
	$("#loading").show();
  defaultview();
});
</script>
<!-- on change of date/time -->
<script>
$(document).ready( function (e){
$("#submit").click(function(e) {
  $('#view').hide();
	$("#loading").show();
  defaultview();
	});
});
</script>
<script>
$(document).ready(function (){
  $('#city').change(function(){
    $('#view').hide();
	  $("#loading").show();
    defaultview();
  });
});
</script>
<!-- ------------------------------  pop ups -------------------------------------------- -->

<!-- ------------------------------ person tab start ------------------------------------------ -->
<!-- Modal -->
<div class="modal fade" id="myModalperson" role="dialog" >
  <div class="modal-dialog" style="width:65%;">

    <!-- Modal content-->
    <div class="modal-content" style="height:80%;overflow:auto;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Person View</h3>
      </div>
      <div class="modal-body">
        <div id="loadingperson" style="display:none; vertical-align:middle;margin-top:40px;" align="center">
          <div class='uil-default-css' style='transform:scale(0.58);'>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
          </div>
        </div>
        <div id="tableperson">
        
        </div>
      </div><!-- modal body -->
    </div><!-- modal content -->
  </div><!-- modal dailog -->
</div><!-- modal -->

<script type="text/javascript">

$("#myModalperson").on('hide.bs.modal', function(event){
    $("#table").addClass('body');
});
$("#myModalperson").on('show.bs.modal', function(event){
$("#loadingperson").show();
$("#tableperson").hide();
        
var button=$(event.relatedTarget);
loadModalperson(button);

});
          
function loadModalperson(button){
var crmlogid=button.data('crmlogid');
var startdate=button.data('startdate');
var enddate=button.data('enddate');
var status=button.data('status');
var city= $('#city').val();;

 $.ajax({
    url : "ajax/conversion_panel_data.php?tab=person",  // create a new php page to handle ajax request
    type : "POST",
    data : {"crmlogid":crmlogid, "startdate": startdate ,"enddate":enddate,"status" :status, "city":city},
    success : function(data) {
      //console.log(data);
        
      $("#loadingperson").hide();
      $("#tableperson").show();
      $('#tableperson').empty().append(data);
    },
    error: function (xhr, ajaxOptions, thrownError) {
      alert(xhr.status + " "+ thrownError);
    }
 });

}
</script>
<!-- ------------------------------ person tab end ------------------------------------------ -->
<!-- ------------------------------ all services tab start ------------------------------------------ -->
<!-- Modal -->
<div class="modal fade" id="myModalservice" role="dialog" >
  <div class="modal-dialog" style="width:65%;">

    <!-- Modal content-->
    <div class="modal-content" style="height:80%;overflow:auto;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Service View</h3>
      </div>
      <div class="modal-body">
        <div id="loadingservice" style="display:none; vertical-align:middle;margin-top:40px;" align="center">
          <div class='uil-default-css' style='transform:scale(0.58);'>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
          </div>
        </div>
        <div id="tableservice">
        
        </div>
      </div><!-- modal body -->
    </div><!-- modal content -->
  </div><!-- modal dailog -->
</div><!-- modal -->

<script type="text/javascript">

$("#myModalservice").on('hide.bs.modal', function(event){
    $("#table").addClass('body');
});
$("#myModalservice").on('show.bs.modal', function(event){
$("#loadingservice").show();
$("#tableservice").hide();
        
var button=$(event.relatedTarget);
loadModalservice(button);

});
          
function loadModalservice(button){
var masterservice=button.data('service');
var startdate=button.data('startdate');
var enddate=button.data('enddate');
var status=button.data('status');
var vehicle=button.data('vehicle');
var city= $('#city').val();;

 $.ajax({
    url : "ajax/conversion_panel_data.php?tab=service",  // create a new php page to handle ajax request
    type : "POST",
    data : {"masterservice": masterservice, "startdate": startdate ,"enddate":enddate,"status" :status ,"vehicle": vehicle, "city":city},
    success : function(data) {
      //console.log(data);
        
      $("#loadingservice").hide();
      $("#tableservice").show();
      $('#tableservice').empty().append(data);
    },
    error: function (xhr, ajaxOptions, thrownError) {
      alert(xhr.status + " "+ thrownError);
    }
 });

}
</script>
<!-- ------------------------------ services tab end ------------------------------------------ -->

<!-- ------------------------------ sources tab start ------------------------------------------ -->
<!-- Modal -->
<div class="modal fade" id="myModalsource" role="dialog" >
  <div class="modal-dialog" style="width:65%;">

    <!-- Modal content-->
    <div class="modal-content" style="height:80%;overflow:auto;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Source View</h3>
      </div>
      <div class="modal-body">
        <div id="loadingsource" style="display:none;vertical-align:middle;margin-top:40px;" align="center">
          <div class='uil-default-css' style='transform:scale(0.58);'>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
          </div>
        </div>
        <div id="tablesource">
        
        </div>
      </div><!-- modal body -->
    </div><!-- modal content -->
  </div><!-- modal dailog -->
</div><!-- modal -->

<script type="text/javascript">

$("#myModalsource").on('hide.bs.modal', function(event){
    $("#table").addClass('body');
});
$("#myModalsource").on('show.bs.modal', function(event){
$("#loadingsource").show();
$("#tablesource").hide();
        
var button=$(event.relatedTarget);
loadModalsource(button);

});
          
function loadModalsource(button){
var source=button.data('source');
var startdate=button.data('startdate');
var enddate=button.data('enddate');
var status=button.data('status');
var vehicle=button.data('vehicle');
var city= $('#city').val();;

 $.ajax({
    url : "ajax/conversion_panel_data.php?tab=source",  // create a new php page to handle ajax request
    type : "POST",
    data : {"source":source, "startdate": startdate ,"enddate":enddate,"status" :status ,"vehicle": vehicle, "city":city},
    success : function(data) {
      //console.log(data);
      //var modal = $(this)
        
      $("#loadingsource").hide();
      $("#tablesource").show();
      $('#tablesource').empty().append(data);
    },
    error: function (xhr, ajaxOptions, thrownError) {
      alert(xhr.status + " "+ thrownError);
    }
 });

}
</script>
<!-- ------------------------------  source tab end ------------------------------------------ -->

<!-- ------------------------------ non conversion tab start ------------------------------------------ -->
<!-- Modal -->
<div class="modal fade" id="myModalnonconv" role="dialog" >
  <div class="modal-dialog" style="width:65%;">

    <!-- Modal content-->
    <div class="modal-content" style="height:80%;overflow:auto;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Non Conversion View</h3>
      </div>
      <div class="modal-body">
        <div id="loadingnonconv" style="display:none; vertical-align:middle;margin-top:40px;" align="center">
          <div class='uil-default-css' style='transform:scale(0.58);'>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
          </div>
        </div>
        <div id="tablenonconv">
        </div>
      </div><!-- modal body -->
    </div><!-- modal content -->
  </div><!-- modal dailog -->
</div><!-- modal -->

<script type="text/javascript">

$("#myModalnonconv").on('hide.bs.modal', function(event){
    $("#table").addClass('body');
});
$("#myModalnonconv").on('show.bs.modal', function(event){
$("#loadingnonconv").show();
$("#tablenonconv").hide();
        
var button=$(event.relatedTarget);
loadModalnonconv(button);

});
          
function loadModalnonconv(button){
var activity=button.data('activity');
var startdate=button.data('startdate');
var enddate=button.data('enddate');
var vehicle=button.data('vehicle');
var city= $('#city').val();;

 $.ajax({
    url : "ajax/conversion_panel_data.php?tab=nonconv",  // create a new php page to handle ajax request
    type : "POST",
    data : {"activity":activity, "startdate": startdate ,"enddate":enddate ,"vehicle": vehicle, "city":city},
    success : function(data) {
      //console.log(data);
      //var modal = $(this)
        
      $("#loadingnonconv").hide();
      $("#tablenonconv").show();
      $('#tablenonconv').empty().append(data);
    },
    error: function (xhr, ajaxOptions, thrownError) {
      alert(xhr.status + " "+ thrownError);
    }
 });

}
</script>
<!-- ------------------------------ non conversion tab end ------------------------------------------ -->

<!-- ------------------------------ pop ups end ------------------------------------------ -->

</body>
</html>

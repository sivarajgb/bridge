<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if ((empty($_SESSION['crm_log_id']))) {
	header('location:logout.php');
	die();
}

date_default_timezone_set("Asia/Kolkata");
$today = date('Y-m-d h:i:s');
$today_time = date('d-m-Y H:i:s');

$admin_flag = $_SESSION['flag'];
$crm_log_id = $_SESSION['crm_log_id'];
?>

<!DOCTYPE html>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <meta charset="utf-8">
    <title>GoBumpr Bridge</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.css" rel="stylesheet">


    <!-- auto complete -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <link rel="stylesheet" href="css/jquery-ui.structure.min.css">
    <link rel="stylesheet" href="css/jquery-ui.theme.min.css">
    <!-- stylings -->
    <link rel="stylesheet" type="text/css" href="css/style.css"/>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <style>
        .select2-container .select2-selection--single {
            box-sizing: border-box;
            cursor: pointer;
            display: block;
            height: 38px;
            user-select: none;
            -webkit-user-select: none;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #444;
            line-height: 36px;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 36px;
            position: absolute;
            top: 1px;
            right: 1px;
            width: 20px;
        }

        .table tbody tr th {
            min-width: 200px;
            max-width: 200px;
        }
    </style>
    <!-- Facebook Pixel Code -->
    <script async>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '582926561860139');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" style="display:none"
             src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"/>
    </noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    <!-- Google Analytics Code -->
    <script async>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-67994843-2', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
<?php include_once("header.php"); ?>

<div class="overlay" data-sidebar-overlay></div>

<div class="padding"></div>

<!-- Garage Details -->
<div id="garage_details">
    <!-- Modal content-->
    <div class="container-fluid">
        <div class="modal-header">
            <h3 class="modal-title">Garage Details</h3>
        </div>
        <div class="modal-body">
            <div class="row" align="center">
                <div class="col-md-1 form-group"
                     style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
                    <label> Garage Name</label>
                </div>
                <div class="col-md-3 form-group">
                    <select style="width:100%;" class="form-control" id="mechanic" name="mechanic"
                            data-placeholder="Select Garage" required>
                        <option value="">Select Garage</option>
                    </select>
                </div>

                <div class="col-md-1 form-group"
                     style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
                    <label> PickUp Location</label>
                </div>
                <div class="col-md-2 form-group">
                    <select style="width:100%;" class="form-control" id="pickupLocation"
                            name="pickupLocation" data-placeholder="Select Pickup Location"
                            required>
                        <option value="">Select Pickup Locations</option>
                    </select>
                </div>

                <div class="col-xs-2 col-md-1 form-group"
                     style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
                    <label> Service</label>
                </div>
                <div class="col-xs-3 col-md-3 form-group">
                    <select style="width:100%;" class="form-control" id="service"
                            name="service" data-placeholder="Select Service"
                            required>
                        <option value="">Select Service</option>
                    </select>
                </div>
            </div>

            <div class="row" id="garage_distance"
                 style="display: none; margin-bottom: 5px; background-color: aquamarine;">
                <div style=" text-align: center;">
                    <h4>Distance Between Garage & PickUp Location is
                        <span style="font-size: 28px;" id="distance_km"></span></h4>
                </div>
            </div>

            <div class="row" id="available_service"
                 style="display: none; margin-top: 5px; background-color: yellowgreen;">
                <div style=" text-align: center;">
                    <h4 id="available_service_text"></h4>
                </div>
            </div>

            <div class="row" align="center" id="garage_details_view" style="padding: 24px;"></div>
        </div>
    </div>
</div>

<!-- jQuery library -->
<script src="https://code.jquery.com/jquery-2.2.4.js"
        integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<!-- side bar -->
<script src="js/sidebar.js"></script>

<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

    $(document).ready(function () {
        $('#city').show();
        get_shop_lists();
        get_localities();
        get_services();
    });

    $('#city').change(function () {
        $('#mechanic').html('<option value="">Select Garage</option>');
        $('#pickupLocation').html('<option value="">Select Pickup Locations</option>');
        $("#service").select2().val("").trigger("change");
        
        $('#garage_distance').hide();
        $('#garage_distance #distance_km').html();
        
        $('#available_service').hide();
        $('#available_service #available_service_text').text();

        $('#garage_details_view').html('');
        
        get_localities();
        get_shop_lists();
    });

    $('#mechanic').select2({
        // allowClear: true
    });//Select Init

    $('#pickupLocation').select2({
        // allowClear: true
    });//Select Init

    $('#service').select2({
        // allowClear: true
    });//Select Init

    //Get Locality
    function get_localities() {
        var city = $("#city").val();
        var postData = {
            'city': city
        };
        jQuery.ajax({
            url: 'ajax/get_garage_localities.php',
            method: 'post',
            data: postData,
            // dataType: "json",
            success: function (data) {
                $('#pickupLocation').append(data);//set option data to html
            }
        });
    }

    //Get Services List
    function get_services() {
        jQuery.ajax({
            url: 'ajax/get_garage_services.php',
            method: 'post',
            data: '',
            // dataType: "json",
            success: function (data) {
                $('#service').append(data);//set option data to html
            }
        });
    }

    //Get Shop Lists
    function get_shop_lists() {
        var city = $("#city").val();
        var postData = {
            'city': city
        };
        jQuery.ajax({
            url: 'ajax/get_shop_lists.php',
            method: 'post',
            data: postData,
            // dataType: "json",
            success: function (data) {
                $('#mechanic').append(data);//set option data to html
            }
        });
    }

    $('#mechanic').change(function () {
        get_garage_details();
        get_distance();
        check_garage_service();
    });

    $('#pickupLocation').change(function () {
        get_distance();
    });

    $('#service').change(function () {
        check_garage_service();
    });

    //Get Garage Details
    var get_garage_details = function () {
        var garage_id = $.trim($('#garage_details #mechanic').val());

        var postData = {
            'garage_id': garage_id
        };

        jQuery.ajax({
            url: 'ajax/get_garage_details.php',
            method: 'post',
            data: postData,
            dataType: "json",
            success: function (data) {
                //If Success
                // console.log(data.garageData);
                $('#garage_details_view').html(data.garageData);
            }
        });
    };

    //Get Garage Service
    var check_garage_service = function () {
        var garage_id = $.trim($('#garage_details #mechanic').val());
        var service = $.trim($('#garage_details #service').val());
        var city = $("#city").val();
        var postData = {
            'garage_id': garage_id,
            'service_type': service
        };

        jQuery.ajax({
            url: 'ajax/check_garage_service.php',
            method: 'post',
            data: postData,
            dataType: "json",
            success: function (data) {
                //If Success
                if (data.success) {
                    $('#available_service').show();
                    $('#available_service #available_service_text').text(data.service);
                }
            }
        });
    };

    //Get Distance
    var get_distance = function () {
        var garage_id = $.trim($('#garage_details #mechanic').val());
        var pickup_location = $.trim($('#garage_details #pickupLocation').val());
        var postData = {
            'garage_id': garage_id,
            'pickup_location': pickup_location
        };

        jQuery.ajax({
            url: 'ajax/get_distance.php',
            method: 'post',
            data: postData,
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $('#garage_distance').show();
                    $('#garage_distance #distance_km').html(data.distance);
                }
            }
        });
    }
</script>

</body>
</html>
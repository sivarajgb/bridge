<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if(empty($_SESSION['crm_log_id'])) {
	header('location:index.php');
	die();
}
?>

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
  <title>GoBumpr Bridge</title>

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

  <!-- Include Date Range Picker -->
  <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>
<!-- maps -->
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
</script>
<style>

#range > span:hover{cursor: pointer;}
.floating-box {
	 display: inline-block;
	 margin: 22px;
 padding: 22px;
 width:203px;
 height:105px;
 box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  font-size: 17px;
}
.floating-box1 {
    display: inline-block;
	margin-top:10px;
	float:left;
	clear:both;
 }
 /* table */
#tbody{
   font-size:15px !important;
  border:1.5px solid #c4b8b8 !important;
  
}

thead:hover{
	cursor:pointer;
 }

.results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px;
  color:#ccc;
}
#map-container {
        padding: 6px;
        border-width: 1px;
        border-style: solid;
        border-color: #ccc #ccc #999 #ccc;
        -webkit-box-shadow: rgba(64, 64, 64, 0.5) 0 2px 5px;
        -moz-box-shadow: rgba(64, 64, 64, 0.5) 0 2px 5px;
        box-shadow: rgba(64, 64, 64, 0.1) 0 2px 5px;
        width: 800px;
      }
      #map {
        width: 800px;
        height: 400px;
      }
      #actions {
        list-style: none;
        padding: 0;
      }
      #inline-actions {
        padding-top: 10px;
      }
      .item {
        margin-left: 20px;
      }
</style>

</head>
<body>
<?php include_once("header.php"); ?>
<script>
	$(document).ready(function(){
		$('#city').show();
	})
</script>
<div class="overlay" data-sidebar-overlay></div>

<div id="left" style="position:fixed;left:2%;top:20%;width:25%;">
    <!-- date range picker -->
    <div id="reportrange" style="cursor:pointer;">
    <div class=" floating-box1">
            <div id="range" class="form-control" style="min-width:312px;">
        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
        <span id="dateval"></span> <b class="caret"></b>
        </div>
    </div>
    </div>

    <!-- booking status filter -->
    <div  class="floating-box1" style="margin-top:12px;background-color:#fff;">
    <div style="max-width:100px;">
    <select id="status" name="status" class="form-control" style="width:250px;">
    <option value="all" selected>All Bookings</option>
    <option value="1">Leads</option>
    <option value="2">Bookings</option>
    <option value="3">Followups</option>
    <option value="4">RNR1</option>
    <option value="5">RNR2</option>
    <option value="0">Others</option>
    <option value="c">Cancelled</option>
    </select>
    </div>
    </div>

    <!-- vehicle type filter -->
    <div  class="floating-box1" style="margin-top:12px;background-color:#fff;">
    <div style="max-width:250px;">
      <select id="vehicle" name="vehicle" class="form-control" style="width:250px;">
      <option value="all" selected>All Vehicles</option>
      <option value="2w">2 Wheeler</option>
      <option value="4w">4 Wheeler</option>
      </select>   
    </div>
    </div>


    <!-- master service type filter -->
    <div  class="floating-box1" style="margin-top:12px;background-color:#fff;">
    <div style="max-width:250px;">
      <select id="master_service" name="master_service" class="form-control" style="width:250px;">
      <option value="all" selected>All Master Services</option>
      <?php 
      $sql_master_service = "SELECT DISTINCT(master_service) FROM go_axle_service_price_tbl WHERE bridge_flag='0' ORDER BY  master_service ASC";
      $res_master_service = mysqli_query($conn,$sql_master_service);
      while($row_master_service = mysqli_fetch_object($res_master_service)){
        $master_service = $row_master_service->master_service;
        ?>
        <option value="<?php echo $master_service; ?>"><?php echo $master_service; ?></option>
        <?php
      }
      ?>
    </select>    
    </div>
    </div>

    <!-- service type filter -->
    <div  class="floating-box1" style="margin-top:12px;background-color:#fff;">
    <div style="max-width:250px;">
      <select id="service_type" name="service_type" class="form-control" style="width:250px;">
      <option value="all" selected>All Services</option>
    </select>    
    </div>
    </div>

    <div  class="floating-box1" style="margin-top:14px;background-color:#fff;margin-left:24px;">
    &nbsp;&nbsp;&nbsp;<img src="/bridge/images/map/pin.png" width="20" height="30"/>&nbsp;&nbsp;&nbsp;Single booking
    </div>
    <div  class="floating-box1" style="margin-top:8px;background-color:#fff;margin-left:24px;">
    <img src="./images/map/m1.png" width="40" height="40"/>&nbsp;&nbsp;&nbsp;<10 bookings
    </div>

    <div  class="floating-box1" style="margin-top:8px;background-color:#fff;margin-left:24px;">
    <img src="./images/map/m2.png" width="40" height="40"/>&nbsp;&nbsp;&nbsp;<50 bookings
    </div>

    <div  class="floating-box1" style="margin-top:8px;background-color:#fff;margin-left:24px;">
    <img src="./images/map/m3.png" width="40" height="40" />&nbsp;&nbsp;&nbsp;>100 bookings
    </div>

    <div  class="floating-box1" style="margin-top:8px;background-color:#fff;margin-left:24px;">
    <img src="./images/map/m4.png" width="40" height="40" />&nbsp;&nbsp;&nbsp;>1000 bookings
    </div>

</div>
<div id="right" style="top:-20px;;width:60%;margin-left:30%">
  <!-- loading -->
  <div id="loading" style="display:none; margin-top:200px;" align="center">
    <div class='uil-default-css' style='transform:scale(0.58);'>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
  </div>
  </div>
<!-- table -->
<div align="center" id = "table" style="max-width:95%; margin-top:110px;margin-left:10px;margin-right:10px; display: none;">
</div>

</div>



<!-- jQuery library -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<!-- google maps --> 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBEFvPWpawLCHyQQQR_4Qx4kKeyojpDw7I"></script>
 
<script type="text/javascript" src="js/markerclusterer.js"></script>
<!-- date range picker -->
<script>
$('input[name="daterange"]').daterangepicker({
 locale: {
      format: 'DD-MM-YYYY'
    }
});
</script>
<script type="text/javascript">
$(function() {

  var start = moment().subtract(1, 'days');
	 // var start = moment();
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});
</script>

<!-- default view -->
<script>
function viewtable(){
	var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
	var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
	var service_type = $('#service_type').val();
  var master_service = $('#master_service').val();
	var vehicle = $('#vehicle').val();
  var status = $('#status').val();
  var city = $('#city').val();
	//console.log(startDate);
	//console.log(endDate);
	          //Make AJAX request, using the selected value as the POST

			  $.ajax({
	            url : "ajax/heatmap_view.php",  // create a new php page to handle ajax request
	            type : "POST",
	            data : {"startdate": startDate , "enddate": endDate,"service_type": service_type,"master_service": master_service,"vehicle": vehicle,"status":status , "city":city},
	            success : function(data) {
					    //alert(data);
					//console.log(data);
					$("#loading").hide();
                    $("#table").show();
      			    $('#table').html(data);		
          },
	          error: function(xhr, ajaxOptions, thrownError) {
	          //  alert(xhr.status + " "+ thrownError);
	         }
				 });
}
</script>

<script>
$(document).ready( function (){
	$("#table").hide();
	$("#loading").show();
	viewtable();
});
</script>
<!-- on change of date -->
<script>
$(document).ready( function (){
	$('#dateval').on("DOMSubtreeModified", function (){
		//console.log("date changed");
	   $("#table").hide();
	   $("#loading").show();
	   viewtable();
	 });
});
</script>
<!--  on Selecting vehicles -->
<script>
$(document).ready(function() {
$("#vehicle").change(function (){
  $("#table").hide();
  $("#loading").show();
  viewtable();
} );
});
</script>
<!-- on change of status -->
<script>
$(document).ready( function (){
	$('#status').change(function (){
		//console.log("date changed");
	   $("#table").hide();
	   $("#loading").show();
	   viewtable();
	 });
});
</script>
<!-- on change of service type -->
<script>
$(document).ready( function (){
	$('#service_type').change(function (){
      //console.log("date changed");
	   $("#table").hide();
	   $("#loading").show();
	   viewtable();
	 });
});
</script> 
<!-- on change of city -->
<script>
$(document).ready( function (){
	$('#city').change(function (){
      //console.log("date changed");
	   $("#table").hide();
	   $("#loading").show();
	   viewtable();
	 });
});
</script> 
<script>
function get_services(){
  var master = $("#master_service").val();
  console.log(master);
  $("#service_type").empty();
  $.ajax({
	    url : "ajax/get_services_master.php",  // create a new php page to handle ajax request
	    type : "POST",
	    data : {"master": master},
	    success : function(data) {
      	$('#service_type').append(data);	
        $("#table").hide();
    $("#loading").show();
    viewtable();	
      },
	    error: function(xhr, ajaxOptions, thrownError) {
	          //  alert(xhr.status + " "+ thrownError);
	    }
	});
}
</script>
<script>
$(document).ready( function (){
  get_services();
});
</script>
<script>
$(document).ready( function (){
  $("#master_service").change(function(){
    get_services();
    
  });
});
</script>
</body>
</html>

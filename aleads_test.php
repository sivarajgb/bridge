<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id'])) || $flag!="1") {
	header('location:index.php');
	die();
}
?>

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
  <title>GoBumpr Bridge</title>
<!-- validate -->
<script sr="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <!-- bread crumbs -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>

<style>
 #datepick > span:hover{cursor: pointer;}
  .top-box {
    display: inline-block;
	 z-index: 1;
	 height:30px;
	 width:90px;
	 background-color: #B6A3BF;
	  -webkit-display:button;
		box-shadow: 0px 8px 8px 0px rgba(0,0,0,0.2);
   }

 .floating-box {
    display: inline-block;
    margin: 10px;
	padding: 12px;
	width:183px;
	height:75px;
	box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
	z-index: 1;
 }
.floating-box1 {
    display: inline-block;
	margin-top:10px;
	float:left;
	clear:both;
 }

input[] {
    visibility:hidden;
}
label {
    cursor: pointer;
}
input:checked + label {
    background: red;
}
 /* table */
#tbody{
   font-size:15px !important;
  border:1.5px solid #c4b8b8 !important;
  
}
thead:hover{
	cursor:pointer;
}

.results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px;
  color:#ccc;
}
#tbody, tbody tr {
    -webkit-animation: opacity 5s ease-in-out;
    animation: opacity 5s ease-in-out;
}
</style>
</head>
<body>
<?php include_once("header.php"); ?>
<div class="overlay" data-sidebar-overlay></div>


<div class="padding">
</div>


<!-- get crm names -->
        <div  class="navbar-fixed-top" style="margin-top:52px; padding-bottom:20px; padding-top:2px; background-color:#fff;">
          <div  class="btn-group" data-toggle="buttons" id="rad_btn">

          <?php $sql_crm_people=mysqli_query($conn,"SELECT crm_log_id,name FROM crm_admin WHERE crm_flag='1'");
                            while ($row_crm_people=mysqli_fetch_object($sql_crm_people)) {
                              $crm_id_get = $row_crm_people->crm_log_id;
                              $crm_name_get = $row_crm_people->name; ?>
            <label class="btn btn-default"  style="background-color: #B6A3BF;color:#000; font-size:14px; box-shadow: 0px 8px 8px 0px rgba(0,0,0,0.2);">
             <strong> <input name="crm_people" value="<?php echo $crm_id_get; ?>" type="radio"><?php echo $crm_name_get; ?></strong>
            </label>

        <?php } ?>

        </div><!-- select box -->
        <!-- service type filter -->
 <div class="col-sm-2" style="margin-left:8px;">
<div style="max-width:130px;">
<select id="service" name="service" class="form-control" style="width:130px;">
   <option value="all" selected>All Services</option>
   <?php
   $sql_service = "SELECT service_type FROM go_axle_service_price_tbl WHERE bridge_flag='0' ORDER BY service_type ASC";
   $res_service = mysqli_query($conn,$sql_service);
   while($row_service = mysqli_fetch_object($res_service)){
	   $c_service = $row_service->service_type;
	   ?>
       <option value="<?php echo $c_service; ?>" > <?php echo $c_service; ?> </option>
       <?php
   }
   ?>
   </select>
</div>
</div>
</div>

<div class="navbar-fixed-top" style=" margin-top:92px;border-top:52px;padding-top:5px;background-color:#fff;z-index:1037;">
<p class="col-lg-offset-3 col-sm-offset-2" style="background-color:#ffa800;padding:7px;width:30px;border-radius:18px;float:left;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p><p style="width:150px;padding:6px;float:left;">New Lead.</p>
<p style="background-color:#69ED85;padding:7px;width:30px;border-radius:18px;float:left;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p><p style="width:150px;padding:6px;float:left;">Sent to axle.</p>
<p style="background-color:#6FA3DE;padding:7px;width:30px;border-radius:18px;float:left;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p><p style="width:150px;padding:6px;float:left;">Lead Accepted.</p>
<p style="background-color:#D25F34;padding:7px;width:30px;border-radius:18px;float:left;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p><p style="width:150px;padding:6px;float:left;">Lead Rejected.</p>
</div>

<div class="navbar-fixed-top" style=" margin-top:130px; padding-bottom:8px; background-color:#fff;">

<!-- source filter -->
<div class=" col-xs-2 col-sm-2 col-lg-1" style="margin-left:10px;">
<div  class="floating-box1" >
<div style="max-width:140px;">
     <select id="bsource" name="bsource" class="form-control" style="width:120px;">
      <option value="all" selected>All Bookings</option>
      <?php 
      $sql_sources = "SELECT user_source FROM user_source_tbl WHERE flag='0' ORDER BY  user_source ASC";
      $res_sources = mysqli_query($conn,$sql_sources);
      while($row_sources = mysqli_fetch_object($res_sources)){
        $source_name = $row_sources->user_source;
        ?>
        <option value="<?php echo $source_name; ?>"><?php echo $source_name; ?></option>
        <?php
      }
      ?>
    </select>
</div>
</div>
</div>

<!-- vehicle filter -->
<div class=" col-sm-2 col-lg-1" style="margin-left: 52px;">
<div  class="floating-box1" >
<div style="max-width:140px;">
    <select id="vehicle" name="vehicle" class="form-control" style="width:140px;">
      <option value="all" selected>All Vehicles</option>
      <option value="2w">2 Wheeler</option>
      <option value="4w">4 Wheeler</option>
    </select>
</div>
</div>
</div>

<!-- date range picker -->
<div id="reportrange" class=" col-sm-3 col-lg-3" style="cursor: pointer; margin-left:50px;">
   <div class=" floating-box1">
		 <div id="range" class="form-control" style="min-width:312px;">
     <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
    <span id="dateval"></span> <b class="caret"></b>
	</div>
    </div>
</div>

<!-- alloted to  filter -->
<div class=" col-sm-1 col-lg-1" style="margin-left:20px;">
<div  class="floating-box1" >
<div style="max-width:130px;">
<select id="person" name="person" class="form-control" style="width:130px;">
   <option value="all" selected>Alloted to All </option>
   <?php
   $sql_crm = "SELECT crm_log_id,name FROM crm_admin WHERE crm_flag='1' ";
   $res_crm = mysqli_query($conn,$sql_crm);
   while($row_crm = mysqli_fetch_object($res_crm)){
	   $c_id = $row_crm->crm_log_id;
	   $c_name = $row_crm->name;
	   ?>
       <option value="<?php echo $c_id; ?>" > <?php echo $c_name; ?> </option>
       <?php
   }
   ?>
   <option value="no">Yet to be Alloted</option>
   </select>
</div>
</div>
</div>

  <!-- search bar -->
  <div class=" col-sm-4 ">

  <div  class="floating-box1" >
    <form method="post" action="check_user.php" onsubmit="return ValidationEvent(event)">
    <button class="form-group pull-right btn btn-small" type="submit" id="search_submit" name="search_submit" style=" width:50px;height:34px; margin-left:6px;background-color:#B2DFDB;font-size:15px;"><i class="fa fa-search" aria-hidden="true"></i></button>

  <!--<div class="form-group pull-right">
      	<input type="text" class="search form-control" id="su" name="mobile" placeholder="Search">
       
  </div>-->
  <div class="form-group pull-right">
      	<input type="text" class="search form-control" id="su" name="searchuser" placeholder="Search">
        <input type="hidden" id="search_type" name="search_type" value="">
  </div>
  
  <span class="counter pull-right" style="margin-left:28px;"></span>
  </div>
  
</form>
  </div>
  </div>



 <div id="adding_user" style="display:inline-block; float:left; align-items:center;" >

<!-- Modal -->
<div class="modal fade" id="myModal_user" role="dialog" >
<div class="modal-dialog" style="width:400px;height:150px;margin-top:150px;">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Add User</h3>
</div>
<div class="modal-body" style="height:220px;">
<form id="check_user" class="form" method="post" action="check_user.php">
<div align="center">
<div class="row"><h4>Please enter Customer's Mobile number.</h4></div>
<div class="row"></br><div class="col-lg-6 col-sm-3 col-lg-offset-3 col-sm-offset-3" ><input type="text" maxlength="10" minlength="9" onchange="try{setCustomValidity('')}catch(e){}" pattern="^[0-9]{6}|[0-9]{8}|[0-9]{10}$" title="Mobile number must have 10 digits!" id="mobile" name="mobile" placeholder="Eg.9876543210" class="form-control" required></div></div>
<div class="row"> </br><input class="btn btn-md" style="background-color:#B6ED6F;font-size:16px;" type="submit" id="submit" name="submit" value="Verify"/></div>
</div>
</form>
</div>

</div>
</div></div>
</div>

<!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>

<div align="center" id = "table" style="max-width:95%; margin-top:140px;margin-left:10px;margin-right:10px; display: none;">
<table id="example" class="table table-striped table-bordered tablesorter table-hover results">
  <thead style="background-color: #D3D3D3;">
<th>No</th>
<th><input type="checkbox" id="selectall" name="selectall" /></th>
<th>BookingId</th>
<th>GoAxle</th>
<th>ServiceType</th>
<th>View</th>
<th>CustomerName</th>
<th>Mobile</th>
<th>Locality</th>
<th>Vehicle</th>
<th>ShopName</th>
<th>ServiceDate</th>
<th>AllotedTo</th>
<th>BookingType</th>
<th>Source</th>
<th>TimeStamp</th>
</thead>
<tbody id="tbody">
</tbody>
</table>
</div>


<!-- jQuery library -->
<!-- table sorter -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js"></script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

 <!-- table sorter -->
    <script>
$(document).ready(function()
    {
        $("#example").tablesorter( {sortList: [[0,0], [1,0]]} );
    }
);
</script>
<!-- search bar  -->
<script>
$(document).ready(function() {
  $(".search").keyup(function() {
    var searchTerm = $(".search").val();
    var listItem = $('.results tbody').children('tr');
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

  $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });

  $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','false');
  });

  $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','true');
  });

  var jobCount = $('.results tbody tr[visible="true"]').length;
    $('.counter').text(jobCount + ' item');

  if(jobCount == '0') {$('.no-result').show();}
    else {$('.no-result').hide();
	}
		  });
});
</script>

<!-- date range picker -->
<script>
$('input[name="daterange"]').daterangepicker({
 locale: {
      format: 'DD-MM-YYYY'
    }
});
</script>
<script type="text/javascript">
$(function() {

  var start = moment().subtract(1, 'days');
	 // var start = moment();
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});
</script>
<!-- automatic refresh -->
<script>
window.setInterval(function(){
	viewtable();
}, 60000);

//check online status
function when_online() {
	document.title = "GoBumpr Bridge";
	$("#table").hide();
	$("#loading").show();
	viewtable();
}
function when_offline() {
	document.title = "You're Offline!";
  alert("You're Offline! Please check your internet connection");
}

// Update the online status icon based on connectivity
window.addEventListener('online',  when_online);
window.addEventListener('offline', when_offline);
</script>
<!-- view table -->
<script>
function viewtable(){
	var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
	var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
	var person = $('#person').val();
  var source = $('#bsource').val();
  var vehicle = $('#vehicle').val();
  var service = $('#service').val();
	//console.log(startDate);
	//console.log(endDate);
	          //Make AJAX request, using the selected value as the POST

			  $.ajax({
	            url : "ajax/aleads_view_test.php",  // create a new php page to handle ajax request
	            type : "POST",
	            data : {"startdate": startDate , "enddate": endDate,"person": person ,"source": source , "vehicle": vehicle ,"service": service},
	            success : function(data) {
               // console.log(data);
					$("#loading").hide();
          $("#table").show();
          if(data === "no"){
            $("#tbody").html("<h2 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h2>");
          }
          else{
      			$('#tbody').html(data);		
            $("#example").trigger("update"); 
          }	  
          counterval();         
           if(document.getElementById('selectall').checked) {
                 document.getElementById('selectall').checked=false;
            }
	            },
	          error: function(xhr, ajaxOptions, thrownError) {
	          //  alert(xhr.status + " "+ thrownError);
	         }
				 });
}
</script>
<script>
function counterval(){
	var jobCount = $("#tbody tr").length;;
	 $('.counter').text(jobCount + ' item');
}
</script>
<!-- default view -->
<script>
$(document).ready( function (){
	$("#table").hide();
	$("#loading").show();
	viewtable();
});
</script>

<!--  on Selecting alloted person -->
<script>
$(document).ready(function() {
$("#person").change(function (){
  $("#table").hide();
  $("#loading").show();
  viewtable();
});
});
</script>

<!--  on Selecting source -->
<script>
$(document).ready(function() {
$("#bsource").change(function (){
  $("#table").hide();
  $("#loading").show();
  viewtable();
} );
});
</script>

<!--  on Selecting service type -->
<script>
$(document).ready(function() {
$("#service").change(function (){
  $("#table").hide();
  $("#loading").show();
  viewtable();
} );
});
</script>

<!--  on Selecting vehicles -->
<script>
$(document).ready(function() {
$("#vehicle").change(function (){
  $("#table").hide();
  $("#loading").show();
  viewtable();
} );
});
</script>


<!--  on changing date range -->
<script>
$(document).ready(function() {
$('#dateval').on("DOMSubtreeModified", function (){
  $("#table").hide();
  $("#loading").show();
  viewtable();
} );
});
</script>

<!-- allocate to a crm person -->
<!-- select all -->
<script>
$(document).ready(function(){
	$("#selectall").click(function(){
       if(document.getElementById('selectall').checked) {
      //console.log("checked");
       var check_boxes = [];
    $("input[name='check_veh[]']").each(function() {
      this.checked = true;    
    });
       }
       else{
        //  console.log("unchecked");
            var check_boxes = [];
    $("input[name='check_veh[]']").each(function() {
      this.checked = false;    
    });
       }
	});
})
</script>
 <script>
 $('#rad_btn label').on('click',function(){

     var crm_person_id = $(this).find('input').attr('value');
		 //alert(crm_person_id);
		// console.log(crm_person_id);
		 var check_boxes = [];
    $("input[name='check_veh[]']:checked").each(function() {
      check_boxes.push($(this).val());
    });
   //var check_boxes = document.querySelectorAll("#check_veh:checked").val();
  // console.log(check_boxes);
   $.ajax({
				url : "ajax/allocate_crm_person.php",  // create a new php page to handle ajax request
				type : "POST",
				data : {"check_boxes":check_boxes, "crm_person_id": crm_person_id},
				success : function(data) {
				alert(data);
        $("#loading").hide();
        $("#table").show();
        viewtable();
				},
			error: function (xhr, ajaxOptions, thrownError) {
			//	alert(xhr.status + " "+ thrownError);
		 }
	 });
 });
</script>
<!-- search/add user 
<script type="text/javascript">
function ValidationEvent() {
  var mobile = document.getElementById("su").value;
  var phoneno = /^\d{10}$/;  
  if(mobile.match(phoneno))  
        {  
      return true;  
        }  
      else  
        {  
        alert("Mobile number must have 10 digits!");  
        return false;  
        }  
}
</script> -->
<!-- search/add user -->
<script type="text/javascript">
function ValidationEvent(event) {
  var mobile = document.getElementById("su").value;
  var phoneno = /^[1-9][0-9]*$/;  
 // var emailid = /\S+@\S+\.\S+/;
  var emailid = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
  var name = /^[a-zA-Z\s]+$/;

  if(mobile.match(phoneno)){  
    if((mobile.length >= 7 && mobile.length <10) || mobile.length > 10 || mobile.length<=4){
      document.getElementById("search_type").value="";
      alert("Please enter a valid BookingId / Mobile number !");
      //alert(document.getElementById("search_type").value);
      return false;
    }
    else if(mobile.length<=7){
      document.getElementById("search_type").value="booking";
      //alert("booking id !");
      //alert(document.getElementById("search_type").value);
      return true;
    }
    else{
       document.getElementById("search_type").value="mobile";
      //alert("mobile number !");
      //alert(document.getElementById("search_type").value);
      return true;   
    }
  }
  else if(mobile.match(emailid)){
    document.getElementById("search_type").value="email";
    //alert("Email !");
    //alert(document.getElementById("search_type").value);
    return true;
  }
  else if(mobile.match(name)){
    document.getElementById("search_type").value="username";
    //alert("Name !");
    //alert(document.getElementById("search_type").value);
    return true;
  }
  else{  
    document.getElementById("search_type").value="";
    alert("Please enter a valid Booking Id / Mobile Number / Email / User Name !");
    //alert(document.getElementById("search_type").value);
    return false;  
  }  
}
</script>

            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Mechanic Select</h4>
        </div>
        <div class="col-xs-8 col-xs-offset-2 form-group">
          <select class="form-control" id="select-mechanic"></select>
                    <button type="submit" id="goaxle">Go Axle</button>


        </div>
        <div class="modal-footer">
        </div>
      </div>
      
    </div>
  </div>
          <script type="text/javascript">

var bookingid;

          $("#myModal").on('show.bs.modal', function(event){

var button=$(event.relatedTarget);
console.log(button.data)
var locality=button.data('locality');
var vehicletype=button.data('vehicletype');
var servicetype=button.data('servicetype');
 bookingid=button.data('id');

 $.ajax({
        url : "/ajax/ayudha_mec_list.php",  // create a new php page to handle ajax request
        type : "POST",
        data : {"selecttype":vehicletype, "selectloc": locality ,"servicetype":servicetype},
        success : function(data) {
          console.log(data);
        //var modal = $(this)
            $('#select-mechanic').empty().append(data);
        },
      error: function (xhr, ajaxOptions, thrownError) {
       alert(xhr.status + " "+ thrownError);
     }
   });

          });
          
$('#goaxle').click(function() {
        var selval=$('#select-mechanic').val();
        console.log(selval);
        console.log(bookingid);


         $.ajax({
        url : "/test.php",  // create a new php page to handle ajax request
        type : "POST",
        data : {"mecid":selval, "bookingid":bookingid},
        success : function(data) {
          console.log(data);
          sendEmail()
          window.location.href = 'aleads_test.php';
        //var modal = $(this)
        },
      error: function (xhr, ajaxOptions, thrownError) {
       alert(xhr.status + " "+ thrownError);
     }
   });

    });

function sendEmail() {
    $.ajax({
        async: true,
        type: 'GET',
        url: '/go_axle.php',
        data: {
            bi: bookingid,
            t:'b'
        },
        success: function(data_res) {},
    });
}


        </script>

<!-- <script type="text/javascript">
 

</script> -->

</body>
</html>

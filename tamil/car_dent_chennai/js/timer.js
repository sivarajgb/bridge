$(function() {
    function e() {
        r--, n = Math.floor((r - 84600) / 60), p = (r - 84600) % 60, r >= 84600 && (10 > n ? $(".minutes").text("0" + n) : $(".minutes").text(n), 10 > p ? $(".seconds").text("0" + p) : $(".seconds").text(p)), 84600 > r && $("#timer_section").fadeOut(), 0 === r ? ($(".loading").fadeOut(), a(), clearInterval(l)) : 0 === r % 30 && 0 !== r && t()
    }

    function t() {
        $(".lds-ring").hide(), $.ajax({
            url: "getData.php",
            type: "GET",
            data: {
                id: _
            },
            success: function(e) {
                u = JSON.parse(e), $("#auctioneers_list").empty()
                for (var i = e = "display_block", t = "display_none", a = "", l = "", n = h = 0; n < u.length; n++) i = e = "display_block", t = "display_none", l = a = "", 1 == u[n][7] && (i = e = "display_none", t = "display_block", a = "green_text"), d = n > 4 ? "show_more_item" : "", o = "undefined" !== u[n][9] ? "<br/><span style='float:left'></span>" : "", c = '<div class=" col-lg-6 col-sm-6 col-xs-12 auctioneers_list_item ' + d + " " + l + '" data-value=' + u[n][6] + ' data-id="' + u[n][10] + '"data-selected="' + u[n][7] + '"><div class="text"><div class="visible"><p><span class="deal_price ' + a + '">&#x20b9; ' + u[n][0] + "</span>" + o + '</p><div class="' + e + '"><h4 class="deal_place">' + u[n][1] + '</h4><p class="deal_distance"><img src="../images/Location.svg" class="location_icon">' + u[n][5] + 'kms away </p></div></div><div class="more_details"><div class="view_more_ins ' + i + '"><p class="click_select"><i class="fa fa-check-circle" aria-hidden="true"></i> click to select</p><button class="btn view_more"><i class="fa fa-plus" aria-hidden="true"></i> view details</button></div><div class="dealer_details ' + t + '"><h4 class="text-left">' + u[n][2] + '</h4><div class="selected_more"><div class="address"><div><img src="../images/Location.svg" class="location_icon"></div><div><p>' + u[n][4] + '</p></div></div><div><button class="btn contact_btn"><a href="tel:' + u[n][3] + '"><i class="fa fa-phone" aria-hidden="true"></i> Contact</button></a></div></div></div></div></div></div>', 0 === h % 2 ? (c = '<div class="row text-center">' + c, $("#auctioneers_list").append(c)) : (c += "</div>", $("#auctioneers_list .row").last().append(c)), 0 != u[n][11] && r > 0 ? $(".loading").fadeIn() : $(".loading").fadeOut(), h++, $(".show_more_item").hide()
                if (r > 0 && s(), 5 < u.length && $(".show-more").show(), 0 == r) {
                    e = Array.from($("#auctioneers_list .row .auctioneers_list_item"))
                    var p = 0
                    e.forEach(function(e) {
                        $(e).unbind("click"), 0 == $(e).attr("data-selected") && (p++, $(e).fadeOut())
                    }), p === e.length && $("#auction_details").append('<p>"You did not select any deal :( "</p>')
                }
            },
            error: function(e) {
                console.log(e.status)
            }
        })
    }

    function s() {
        $("#auctioneers_list .row .auctioneers_list_item").click(function(e) {
            if (0 == $(this).attr("data-selected")) {
                $(this).unbind("click"), i = $(this).attr("data-value"), e = $(this).children().children().first().children().last()
                var s = $(this).children().children().first().children().first().children().first(),
                    a = $(this).children().children().last().children().first(),
                    l = $(this).children().children().last().children().last()
                s.addClass("green_text"), e.addClass("display_none"), a.removeClass("display_block"), a.addClass("display_none"), l.removeClass("display_none"), l.addClass("display_block"), $.ajax({
                    url: "/FBP/confirmedDeals.php",
                    data: {
                        booking_id: i
                    },
                    success: function(e) {},
                    error: function(e) {
                        console.log(e.status + " error ocurred!")
                    }
                }), e = $(this).attr("data-id"), a = $(this).children().last().children().last().children().last().children().last().children().last().children().children().attr("href"), a = a.split(":"), a = a[1], l = $(this).children().first().children().first().children().first().text(), l = l.split(" ") 
//                $.ajax({
//                    url: "/sendMsg.php",
//                    data: {
//                        shop_id: e,
//                        booking_id: i,
//                        mobile: a,
//                        price: l[1]
//                    },
//                    success: function(e) {
//                        console.log("sent")
//                    }
//                })
            }
        })
    }

    function a() {
        var e = Array.from($("#auctioneers_list .row .auctioneers_list_item")),
            i = 0
        $(".loading").fadeOut(), e.forEach(function(e) {
            $(e).unbind("click"), 0 == $(e).attr("data-selected") && (i++, $(e).fadeOut())
        }), i === e.length && $("#auction_details").append('<p>"You did not select any deal :( "</p>')
    }
    $(".show-more").hide()
    var l, n, d, c, o, r = $("#user_details_display").data("value"),
        h = 0,
        u = [],
        p = n = r,
        _ = function(e) {
            return e = decodeURIComponent(window.location).split("/"), e[e.length - 1]
        }("id")
    $("#auction_details .show-more").click(function() {
        $(".show_more_item").show(), $(".show-more").hide()
    }), t(), 0 == r && (console.log("timer set to 0"), $(".loading").fadeOut()), r > 0 && 86400 >= r && (l = setInterval(e, 1e3))
})
var base_url = '';

// (function ($) {
// "use strict";

var map;
var windowHeight;
var windowWidth;
var contentHeight;
var contentWidth;

// calculations for elements that changes size on window resize
var windowResizeHandler = function () {
    windowHeight = window.innerHeight;
    windowWidth = $(window).width();
    contentHeight = windowHeight - $('.app-header').height();
    $('#mapView').height(contentHeight - $('.app-page-title').outerHeight() - $('.main-card').outerHeight() - $('.body-tabs').outerHeight() - 55);

    if (map) {
        google.maps.event.trigger(map, 'resize');
    }
};

windowResizeHandler();

var markerImg = "../../../public/assets/images/marker-green.png";

// Custom options for map
var options = {
    zoom: 20,
    disableDefaultUI: false, // or true
    mapTypeControlOptions: {
        mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain']
    }
};

var markers = [];

// custom infowindow object
var infobox = new InfoBox({
    disableAutoPan: false,
    maxWidth: 400,
    pixelOffset: new google.maps.Size(-150, -250),
    zIndex: null,
    boxClass: "infoBox",
    boxStyle: {
        // background: "url('" + base_url + "/l6/public/assets/images/infobox-bg.png') no-repeat",
        opacity: 1,
        width: "400px",
        height: "auto"
    },
    closeBoxMargin: "10px 10px 0px 0px",
    // closeBoxURL: "",
    infoBoxClearance: new google.maps.Size(1, 1),
    pane: "floatPane",
    enableEventPropagation: false
});

var addMarkers = function (props, map) {
    var bounds = new google.maps.LatLngBounds();
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    deleteMarkers();

    $.each(props, function (i, prop) {
        var latlng = new google.maps.LatLng(prop.b2b_lat, prop.b2b_lng);

        bounds.extend(latlng);
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            icon: new google.maps.MarkerImage(
                // base_url + '/l6/public/assets/images/' + prop.markerIcon,
                "http://maps.google.com/mapfiles/ms/micons/" + prop.marker + ".png",
                null,
                null,
                null,
                new google.maps.Size(24, 24)
            ),
            visible: true,
            marker: prop.marker,
            shop_id: prop.b2b_shop_id,
            draggable: false,
            animation: google.maps.Animation.DROP,
        });

        // Add info window to marker
        google.maps.event.addListener(marker, 'click', function () {
            // console.log(marker.get('shop_id'));
            // console.log(i);
            infobox.open(null, null);
            let infoboxContent = createInfoWindow(marker.get('shop_id'));
            infobox.setContent(infoboxContent);
            // console.log(infoContent);
            infobox.open(map, marker);
        });

        // google.maps.event.addListener(marker, 'click', (function (marker, i) {
        //     return function () {
        //         infobox.setContent(infoboxContent);
        //         infobox.open(map, marker);
        //         map.setZoom(14);
        //     }
        // })(marker, i));

        // google.maps.event.addListener(marker, 'mouseout', (function (marker, i) {
        //     return function () {
        //         infobox.open(null, null);
        //     }
        // })(marker, i));
        //
        // google.maps.event.addListener(marker, 'click', (function (marker, i) {
        //     return function () {
        //         infobox.setContent(infoboxContent);
        //         infobox.open(map, marker);
        //         //map.setZoom(14);
        //     }
        // })(marker, i));

        $('.body-tabs li a[href="#tab-content-0"]').on('click', function (e) {
            setTimeout(function () {
                google.maps.event.trigger(map, 'resize');
                console.log('resize');
                map.panToBounds(bounds);    //# auto-center
                map.fitBounds(bounds);      //# auto-zoom
            }, 300);
        })

        $(document).on('click', '.closeInfo', function () {
            infobox.open(null, null);
        });

        google.maps.event.addListener(map, "click", function (event) {
            // console.log(event);
            infobox.open(null, null);
        });

        markers.push(marker);
        // console.log(markers);
        map.panToBounds(bounds);    //# auto-center
        map.fitBounds(bounds);      //# auto-zoom
    });
};

$(window).resize(function () {
    windowResizeHandler();
});

filterMarkers = function (category) {
    console.log('ok');
    for (i = 0; i < markers.length; i++) {
        marker = markers[i];
        console.log(marker);
        if (category != 'all') {
            // If is same category or category not picked
            if (marker.marker == category) {
                console.log(1);
                marker.setVisible(true);
            }
            // Categories don't match
            else {
                console.log(2);
                marker.setVisible(false);
            }
        } else {
            marker.setVisible(true);
        }
    }
}

function deleteMarkers() {
    while (markers.length) {
        markers.pop().setMap(null);
    }
}

function closeInfoBox() {
    infobox.open(null, null);
}

function createInfoWindoww(prop) {
    return infoboxContent = '<div class="infoW" onclick="event.stopPropagation();" style="height: 100%">' +
        '<div class="propImg">' +
        '<img src="' + base_url + '/uploads/property_images/' + prop.image + '">' +
        '<div class="propBg">' +
        '<div class="propPrice">' + prop.price + '</div>' +
        '<div class="propType">' + prop.type + '</div>' +
        '</div>' +
        '</div>' +
        '<div class="paWrapper">' +
        '<div class="propTitle">' + prop.title + '</div>' +
        '<div class="propAddress">' + prop.address + '</div>' +
        '</div>' +
        '<ul class="propFeat">' +
        '<li><span class="fa fa-moon-o"></span> ' + prop.bedrooms + '</li>' +
        '<li><span class="icon-drop"></span> ' + prop.bathrooms + '</li>' +
        '<li><span class="icon-frame"></span> ' + prop.area + '</li>' +
        '</ul>' +
        '<div class="clearfix"></div>' +
        '<div class="infoButtons">' +
        '<a class="btn btn-sm btn-round btn-gray btn-o closeInfo">Close</a>' +
        '<a href="' + base_url + '/home/single/' + prop.id + '" class="btn btn-sm btn-round btn-green viewInfo btn-primary">View</a>' +
        '</div>' +
        '</div>';
}

//Initialize Google Map
function initialize() {
    map = new google.maps.Map(document.getElementById('mapView'), options);
    map.setCenter(new google.maps.LatLng('13.0827', '80.2707'));//Default Location
    map.setZoom(12);//Default Zoom
    //here also we can call add markers with lat & lan
}

//Initialize the map on page Load
google.maps.event.addDomListener(window, 'load', initialize);

//Marker Trigger while mouse enter on property
// $('.card').each(function (i) {
//     $(this).on('mouseenter', function () {
//         if (Map) {
//             google.maps.event.trigger(markers[i], 'click');
//         }
//     });
//     $(this).on('mouseleave', function () {
//         infobox.open(null, null);
//     });
// });
// })(jQuery);

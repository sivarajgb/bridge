<div class="row m-0">
    <div class="col-sm-12">
        <div class="main-card card">
            <div class="card-body">
                <h5 class="card-title text-primary text-center">{{$shopDetails->b2b_shop_name}}</h5>
                <h6 class="card-subtitle" style="color: inherit; font-weight: 500;">Address
                    : {{$shopDetails->b2b_address4}}, {{$shopDetails->b2b_address5}}</h6>
                <h6 class="card-subtitle" style="color: inherit; font-weight: 500;">Vehicle Type :
                    @if($shopDetails->b2b_vehicle_type == '2w')
                        2 Wheeler
                    @elseif($shopDetails->b2b_vehicle_type == '4w')
                        4 Wheeler
                    @endif
                </h6>
                <h6 class="card-subtitle" style="color: inherit; font-weight: 500;">Available Credits
                    : {{$shopDetails->b2b_credits}}</h6>
                @if($shopDetails->b2b_vehicle_type == "4w")
                    <h6 class="card-subtitle" style="color: inherit; font-weight: 500;">Available Leads
                        : {{$shopDetails->b2b_leads}}</h6>
                @endif

                @if($shopDetails->b2b_vehicle_type == "2w")
                    <h6 class="card-subtitle" style="color: inherit; font-weight: 500;">RE Leads
                        : {{$shopDetails->b2b_re_leads}}</h6>
                    <h6 class="card-subtitle" style="color: inherit; font-weight: 500;">Non-RE Leads
                        : {{$shopDetails->b2b_non_re_leads}}</h6>
                @endif
                <h6 class="card-subtitle" style="color: inherit; font-weight: 500;">Lead Cap
                    : {{$shopDetails->wkly_counter}}</h6>

                <p style="position: relative; top:20px;font-size: 14px;"><i class="metismenu-icon fa fa-map-marked" style="font-size: 14px;"></i>
                    <span class="pr-1">{{$shopDetails->b2b_lat}}, {{$shopDetails->b2b_lng}}</span></p>
            </div>
        </div>
    </div>
</div>

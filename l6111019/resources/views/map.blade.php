@extends('layouts.mainlayout')

@section('content')
    <div class="app-main__outer">
        <div class="app-main__inner">
            {{--Page Title--}}
            <div class="app-page-title mb-0 pb-3">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-car icon-gradient bg-mean-fruit">
                            </i>
                        </div>
                        <div>
                            Shop Availability
                            {{--                            <div class="page-title-subheading">This is an example dashboard created using build-in--}}
                            {{--                                elements and components.--}}
                            {{--                            </div>--}}
                        </div>
                    </div>

                    {{--                    <div class="page-title-actions">--}}
                    {{--                        <button type="button" data-toggle="tooltip" title="Example Tooltip" data-placement="bottom"--}}
                    {{--                                class="btn-shadow mr-3 btn btn-dark">--}}
                    {{--                            <i class="fa fa-star"></i>--}}
                    {{--                        </button>--}}
                    {{--                        <div class="d-inline-block dropdown">--}}
                    {{--                            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"--}}
                    {{--                                    class="btn-shadow dropdown-toggle btn btn-info">--}}
                    {{--                                            <span class="btn-icon-wrapper pr-2 opacity-7">--}}
                    {{--                                                <i class="fa fa-business-time fa-w-20"></i>--}}
                    {{--                                            </span>--}}
                    {{--                                Buttons--}}
                    {{--                            </button>--}}
                    {{--                            <div tabindex="-1" role="menu" aria-hidden="true"--}}
                    {{--                                 class="dropdown-menu dropdown-menu-right">--}}
                    {{--                                <ul class="nav flex-column">--}}
                    {{--                                    <li class="nav-item">--}}
                    {{--                                        <a href="javascript:void(0);" class="nav-link">--}}
                    {{--                                            <i class="nav-link-icon lnr-inbox"></i>--}}
                    {{--                                            <span>--}}
                    {{--                                                            Inbox--}}
                    {{--                                                        </span>--}}
                    {{--                                            <div class="ml-auto badge badge-pill badge-secondary">86</div>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                    <li class="nav-item">--}}
                    {{--                                        <a href="javascript:void(0);" class="nav-link">--}}
                    {{--                                            <i class="nav-link-icon lnr-book"></i>--}}
                    {{--                                            <span>--}}
                    {{--                                                            Book--}}
                    {{--                                                        </span>--}}
                    {{--                                            <div class="ml-auto badge badge-pill badge-danger">5</div>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                    <li class="nav-item">--}}
                    {{--                                        <a href="javascript:void(0);" class="nav-link">--}}
                    {{--                                            <i class="nav-link-icon lnr-picture"></i>--}}
                    {{--                                            <span>--}}
                    {{--                                                            Picture--}}
                    {{--                                                        </span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                    <li class="nav-item">--}}
                    {{--                                        <a disabled href="javascript:void(0);" class="nav-link disabled">--}}
                    {{--                                            <i class="nav-link-icon lnr-file-empty"></i>--}}
                    {{--                                            <span>--}}
                    {{--                                                            File Disabled--}}
                    {{--                                                        </span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                </ul>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
            </div>

            {{--Search Form--}}
            <div class="main-card mb-2 card">
                <div class="card-body pb-0">
                    <form id="search_form" class="needs-validation" novalidate>
                        <div class="form-row">
                            {{--City--}}
                            <div class="col-md-3 mb-2">
                                <div class="position-relative form-group">
                                    <label for="city" class="">City</label>
                                    <select name="city" id="city" data-placeholder="Select City" class="form-control"
                                            required>

                                    </select>
                                    <div class="invalid-feedback">
                                        Select City
                                    </div>
                                </div>
                            </div>

                            {{--Locality--}}
                            <div class="col-md-3 mb-2">
                                <div class="position-relative form-group">
                                    <label for="locality" class="">Locality</label>
                                    <select name="locality" id="locality" data-placeholder="Select Locality"
                                            class="form-control" required>

                                    </select>
                                    <div class="invalid-feedback">
                                        Select Locality
                                    </div>
                                </div>
                            </div>

                            {{--Vehicle Type--}}
                            <div class="col-md-2 mb-2">
                                <div class="position-relative form-group">
                                    <label for="vehicle_type" class="">Vehicle Type</label>
                                    <select name="vehicle_type" id="vehicle_type" data-placeholder="Select Vehicle Type"
                                            class="form-control" required>
                                        <option value="all">ALL Vehicles</option>
                                        <option value="2w">2 wheeler</option>
                                        <option value="4w">4 Wheeler</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Select Vehicle Type
                                    </div>
                                </div>
                            </div>

                            {{--RE/NRE--}}
                            <div id="is_two_wheeler" class="col-md-2 mb-2" style="display: none;">
                                <div class="position-relative form-group">
                                    <label for="re_nre" class="">RE/NRE</label>
                                    <select name="re_nre" id="re_nre" data-placeholder="Select RE/NRE"
                                            class="form-control" required>
                                        <option value="all">ALL</option>
                                        <option value="re">RE</option>
                                        <option value="nre">NRE</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Select RE/NRE
                                    </div>
                                </div>
                            </div>

                            {{--Submit Action--}}
                            <div class="col-md-2 mb-2 my-2" style="margin-top: 29px!important;">
                                <button class="btn btn-primary w-100" style="height: calc(2.25rem + 2px);"
                                        type="submit">Search
                                </button>
                            </div>
                        </div>
                    </form>

                    <script>
                        // Example starter JavaScript for disabling form submissions if there are invalid fields
                        (function () {
                            'use strict';
                            window.addEventListener('load', function () {
                                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                var forms = document.getElementsByClassName('needs-validation');
                                // Loop over them and prevent submission
                                var validation = Array.prototype.filter.call(forms, function (form) {
                                    form.addEventListener('submit', function (event) {
                                        if (form.checkValidity() === false) {
                                            event.preventDefault();
                                            event.stopPropagation();

                                            if (($.trim($('#search_form #city').val()) == '') || ($.trim($('#search_form #locality').val()) == '')) {
                                                $.toast({
                                                    heading: 'Error',
                                                    text: 'Please Choose City & Locality',
                                                    showHideTransition: 'fade',
                                                    position: 'top-center',
                                                    icon: 'error'
                                                })
                                            }
                                        } else {
                                            event.preventDefault();
                                            event.stopPropagation();
                                            get_shop_availability();
                                        }
                                        form.classList.add('was-validated');
                                    }, false);
                                });
                            }, false);
                        })();
                    </script>
                </div>
            </div>

            {{--Tab List--}}
            <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
                <li class="nav-item">
                    <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0">
                        <span>Map View</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1">
                        <span>Table View</span>
                    </a>
                </li>
            </ul>

            {{--Tab Contents--}}
            <div class="tab-content">
                <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                    <div class="mb-3">
                        <span>Instructions & Filter :</span>
                        <span class="mr-3 pointer" onclick="filterMarkers('green')">
                            <img src="http://maps.google.com/mapfiles/ms/micons/green.png"
                                 style="height: 20px;">Available
                            <span class="greenCount"></span>
                        </span>
                        <span class="mr-3 pointer" onclick="filterMarkers('yellow')">
                            <img src="http://maps.google.com/mapfiles/ms/micons/yellow.png"
                                 style="height: 20px;">Not Available
                        <span class="yellowCount"></span>
                        </span>
                        <span class="mr-3 pointer" onclick="filterMarkers('red')">
                            <img src="http://maps.google.com/mapfiles/ms/micons/red.png"
                                 style="height: 20px;">Available, But No credits
                            <span class="redCount"></span>
                        </span>
                        <span class="mr-3 pointer" onclick="filterMarkers('all')">
                            <img src="http://maps.google.com/mapfiles/ms/micons/info.png"
                                 style="height: 20px;">All
                            <span class="redCount"></span>
                        </span>
                    </div>

                    <div id="mapView" class="w-100"
                         style="min-height: 300px;border: 2px solid rgb(211, 211, 211);box-shadow: rgba(211, 211, 211, 0.05) 1px 1px 0px 1px;">
                        <div class="mapPlaceholder">
                            <span class="fa fa-spin fa-spinner"></span> Loading map...
                        </div>
                    </div>
                </div>

                <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel">
                    <div class="row">
                        <div class="col-12">
                            <div class="main-card mb-3 card">
                                <div class="card-body">
                                    {{--Filter Table--}}
                                    <div class="row">
                                        <div class="col-12">
                                            <form class="form-inline">
                                                <div class="mb-2 mr-sm-2 mb-2 position-relative form-group">
                                                    <label for="vehicle_type" class="mr-sm-2">FILTER : </label>
                                                    <select name="table_filter" id="table_filter"
                                                            data-placeholder="Select Filter"
                                                            class="form-control" required>
                                                        <option value="all">ALL</option>
                                                        <option value="green">Available</option>
                                                        <option value="red">No Credits</option>
                                                        <option value="yellow">Not Available</option>
                                                    </select>
                                                </div>
                                            </form>

                                            <div class="mb-3">
                                                <span>Instructions :</span>
                                                <span class="mr-3">
                                                <span class="square-box bg-green"></span>Available
                                                <span class="greenCount"></span>
                                            </span>
                                                <span class="mr-3">
                                                <span class="square-box bg-yellow"></span>Not Available
                                                <span class="yellowCount"></span>
                                            </span>
                                                <span class="mr-3">
                                                <span class="square-box bg-red"></span>Available, But No credits
                                                <span class="redCount"></span>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="mb-0 table table-bordered" id="shopAvailabilityTableView">
                                            <thead>
                                            <tr>
                                                <th>Mec ID</th>
                                                <th>Shop Name</th>
                                                <th>Veh. Type</th>
                                                <th>Locality</th>
                                                <th>credits</th>
                                                <th>Leads</th>
                                                <th>Lead Cap</th>
                                                <th>Attendance</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footerscripts')
    @parent
    <script type="text/javascript">
        $(function () {
            'use strict';
            $('#is_two_wheeler').hide();
            $('select').select2();
            $('select#vehicle_type').select2({
                minimumResultsForSearch: Infinity
            });
            $('select#re_nre').select2({
                minimumResultsForSearch: Infinity
            });

            $('#search_form #vehicle_type').change(function () {
                var vehicle_type = $(this).val();
                switch (vehicle_type) {
                    case '2w':
                        $('#is_two_wheeler').show();
                        get_shop_availability();
                        break;
                    case '4w':
                        $('#is_two_wheeler').hide();
                        $('#re_nre').val('all'); //reset selected value
                        get_shop_availability();
                        break;
                    default:
                        $('#is_two_wheeler').hide();
                        get_shop_availability();
                }
            });

            $('#search_form #city').change(function () {
                get_locality_by_city();
            });

            $('#search_form #re_nre').change(function () {
                get_shop_availability();
            });

            $('#search_form #locality').change(function () {
                get_shop_availability();
            });

            $('#table_filter').change(function () {
                if ($(this).val() == 'all') {
                    $("#shopAvailabilityTableView tr").show();
                } else {
                    // $("#shopAvailabilityTableView td.col1:contains('" + $(this).val() + "')").parent().show();
                    // $("#shopAvailabilityTableView td.col1:not(:contains('" + $(this).val() + "'))").parent().hide();

                    $("#shopAvailabilityTableView td.col1[data-marker*='" + $(this).val() + "']").parent().show();
                    $("#shopAvailabilityTableView td.col1:not([data-marker*='" + $(this).val() + "'])").parent().hide();
                }
            });

            get_cities();
        });

        var get_cities = function () {
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            jQuery.ajax({
                url: "{{ route('getCities') }}",
                method: 'get',
                data: '',
                success: function (data) {
                    if ('errors' in data) {
                        jQuery.each(data.errors, function (key, value) {

                        });
                    }

                    if ('success' in data) {
                        // console.log(data.city);
                        let cityOption = '<option value="" selected disabled>Select City</option>';
                        jQuery.each(data.city, function (key, value) {
                            // console.log(value);
                            cityOption += '<option value="' + value + '">' + value + '</option>';
                        });
                        $('#city').html(cityOption);
                    }
                }
            });
        };

        var get_locality_by_city = function () {
            var city = $.trim($('#search_form #city').val());

            var postData = {"_token": "{{ csrf_token() }}", 'city': city};

            // console.log(postData);

            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            jQuery.ajax({
                url: "{{ route('getLocalitiesByCity') }}",
                method: 'post',
                data: postData,
                success: function (data) {
                    if ('errors' in data) {
                        jQuery.each(data.errors, function (key, value) {

                        });
                    }

                    if ('success' in data) {
                        let localityOption = '<option value="" selected disabled>Select Locality</option>';
                        jQuery.each(data.locality, function (key, value) {
                            // console.log(value);
                            localityOption += '<option value="' + value + '">' + value + '</option>';
                        });
                        $('#locality').html(localityOption);
                    }
                }
            });
        };

        var get_shop_availability = function () {
            var city = $.trim($('#search_form #city').val());
            var locality = $.trim($('#search_form #locality').val());
            var vehicle_type = $.trim($('#search_form #vehicle_type').val());
            var re_nre = $.trim($('#search_form #re_nre').val());

            if (city == '' || locality == '' || vehicle_type == '' || re_nre == '') {
                $.toast({
                    heading: 'Error',
                    text: 'Please Choose City & Locality',
                    showHideTransition: 'fade',
                    position: 'top-center',
                    icon: 'error'
                })
                return false;
            }

            closeInfoBox();

            var postData = {
                "_token": "{{ csrf_token() }}",
                'city': city,
                'locality': locality,
                'vehicle_type': vehicle_type,
                're_nre': re_nre
            };

            // console.log(postData);

            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            jQuery.ajax({
                url: "{{ route('getShopAvailability') }}",
                method: 'post',
                data: postData,
                success: function (data) {
                    if ('errors' in data) {
                        jQuery.each(data.errors, function (key, value) {

                        });
                    }

                    if ('success' in data) {
                        var lat_lng = data.availableShops.filter(function(prop) {
                            return prop.b2b_lat != 0;
                        });
                        addMarkers(lat_lng, map);
                        $('#shopAvailabilityTableView').html(data.tableData);
                        var green = data.availableShops.filter(function (d) {
                            if (d.marker == 'green')
                                return d;
                        });
                        $('.greenCount').html('(' + green.length + ')');

                        var yellow = data.availableShops.filter(function (d) {
                            if (d.marker == 'yellow')
                                return d;
                        });
                        $('.yellowCount').html('(' + yellow.length + ')');

                        var red = data.availableShops.filter(function (d) {
                            if (d.marker == 'red')
                                return d;
                        });
                        $('.redCount').html('(' + red.length + ')');
                    }
                }
            });
        };

        // $.extend({
        createInfoWindow = function (shop_id) {
            // jQuery.ajaxSetup({
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            //     }
            // });
            var postData = {"_token": "{{ csrf_token() }}", 'shop_id': shop_id};

            var templateResponse = null;
            jQuery.ajax({
                url: "{{ route('getShopDetails') }}",
                method: 'post',
                data: postData,
                async: false,
                success: function (data) {
                    templateResponse = data.template;
                }
            });
            return templateResponse;
        }
        // });


    </script>
@endsection

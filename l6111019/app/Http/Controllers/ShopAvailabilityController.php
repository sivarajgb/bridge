<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\ShopAvailabilityInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShopAvailabilityController extends Controller
{
    protected $shopAvailability;

    public function __construct(ShopAvailabilityInterface $shopAvailability)
    {
        $this->shopAvailability = $shopAvailability;
    }

    public function index()
    {
        return view('map');
    }

    public function getCities(Request $request)
    {
        $crm_city = $request->session()->get('cc');
        $data = DB::table('go_bumpr.localities as gl')
            // ->where('bm.b2b_address5', '<>', '')
            ->when(($crm_city != 'all'), function ($query) use ($crm_city) {
                return $query->where('city', $crm_city);
            })
            ->distinct('gl.localities')
            ->orderBy('gl.city', 'asc')
            ->pluck('gl.city');
        return ['success' => true, 'city' => $data->toArray()];
        // return response()->json(['success' => true, ]);
    }

    public function getLocalitiesByCity(Request $request)
    {
        $data = DB::table('go_bumpr.localities as gl')
            ->where('gl.city', $request->get('city'))
            ->distinct('gl.localities')
            ->pluck('gl.localities');
        return ['success' => true, 'locality' => $data->toArray()];
    }

    public function getShopAvailability(Request $request)
    {
        $availableShops = DB::table('b2b.b2b_mec_tbl as bm')
            ->where(function ($query) use ($request) {
                if ($request->get('locality') != 'all') {
                    $query->where('bm.b2b_address4', $request->get('locality'));
                }
                if ($request->get('vehicle_type') != 'all') {
                    $query->where('bm.b2b_vehicle_type', $request->get('vehicle_type'));
                }
                if ($request->get('vehicle_type') != '4w') {
                    if ($request->get('re_nre') == 're') {
                        $query->where('bc.b2b_re_leads', '>', 0);
                    }

                    if ($request->get('re_nre') == 'nre') {
                        $query->where('bc.b2b_non_re_leads', '>', 0);
                    }
                }
            })
            ->where(function ($query) use ($request) {
                $query->where('bc.b2b_leads', '>=', 1);
                $query->orWhere('bc.b2b_credits', '>', 2);
            })
            ->leftJoin('b2b.b2b_credits_tbl as bc', 'bm.b2b_shop_id', '=', 'bc.b2b_shop_id')
            ->leftJoin('go_bumpr.admin_mechanic_table as gam', 'bm.gobumpr_id', '=', 'gam.mec_id')
            ->select('bm.b2b_shop_id', 'bm.b2b_lat', 'bm.b2b_lng', 'bm.b2b_vehicle_type', 'bm.b2b_shop_name', 'bm.b2b_address4')
            ->addSelect('bc.b2b_credits', 'bc.b2b_leads','bc.b2b_re_leads','bc.b2b_non_re_leads')
            ->addSelect('gam.mec_id', 'gam.wkly_counter')
            ->addSelect(DB::raw('
                CASE WHEN bm.b2b_avail = 1 
                    THEN 
	                    CASE WHEN bc.b2b_credits > 2 OR bc.b2b_leads >= 1
	                        THEN "green" 
	                        ELSE "red" 
	                    END 
                    ELSE 
                        CASE WHEN bc.b2b_credits > 2 OR bc.b2b_leads >= 1
	                        THEN "yellow" 
	                    END
                END AS marker
            '))
            ->get();

        $tableView = view('shopAvailabilityTableView', compact('availableShops'))->render();

        return ['success' => true, 'availableShops' => $availableShops->toArray(), 'tableData' => $tableView];
    }

    public function getShopDetails(Request $request)
    {
        $shopDetails = DB::table('b2b.b2b_mec_tbl as bm')
            ->where('bm.b2b_shop_id', $request->get('shop_id'))
            // ->where('bm.b2b_shop_id', '1010')
            ->leftJoin('b2b.b2b_credits_tbl as bc', 'bm.b2b_shop_id', '=', 'bc.b2b_shop_id')
            ->leftJoin('go_bumpr.admin_mechanic_table as gam', 'bm.gobumpr_id', '=', 'gam.mec_id')
            ->select('bm.b2b_shop_id', 'bm.b2b_lat', 'bm.b2b_lng', 'bm.b2b_vehicle_type', 'bm.b2b_shop_name', 'bm.b2b_address4', 'bm.b2b_address3', 'bm.b2b_address5')
            ->addSelect('bc.b2b_credits', 'bc.b2b_leads', 'bc.b2b_re_leads', 'bc.b2b_non_re_leads')
            ->addSelect('gam.mec_id', 'gam.wkly_counter')
            ->first();

        $html = view('markerinfobox', compact('shopDetails'))->render();
        return ['success' => true, 'template' => $html];
    }
}

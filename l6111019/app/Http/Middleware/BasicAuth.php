<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;

class BasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @param array $guards
     * @return mixed
     */
    public function handle1($request, Closure $next)
    {
        // session_start();
        // if (isset($_SESSION['crm_admin_id'])) {
        //     $request->session()->put('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d', $_SESSION['crm_admin_id']);
        //     dd(\Auth::check());
        // }else{
        //     exit;
        // }

        // return $next($request);
    }

    public function handle($request, Closure $next, $guard = null)
    {
        session_start();
        if (isset($_SESSION['crm_admin_id'])) {
            //Set Auth User ID Into Session
           $request->session()->put('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d', $_SESSION['crm_admin_id']);

            //Set All Session into Into Request Session
           $request->session()->put($_SESSION);
            // dd($request->session()->all());
            // dd($request->session()->get('cc'));
            return $next($request);
        }
        return Redirect::to(config('custom.p_url'));
    }
}

<?php

namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;

interface ShopAvailabilityInterface
{
    public function getCities();

    public function getLocalitiesByCity(Request $request);

}

<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
 if((empty($_SESSION['crm_log_id'])) || $flag!="1") {
	
	header('location:logout.php');
	die();
} 
$_SESSION['modal_sess'] = 'open';
$city = $_SESSION['crm_city'];
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>GoBumpr Bridge</title>

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>

<style>
.borderless td, .borderless th {
    border: none !important;
}
*{box-sizing:border-box;}html{min-height:100%;}body{color:black;margin:0px;min-height:inherit;background:rgb(255, 255, 255) !important;}[data-sidebar-overlay]{display:none;position:fixed;top:0px;bottom:0px;left:0px;opacity:0;width:100%;min-height:inherit;}.overlay{background-color:rgb(222, 214, 196);z-index:999990 !important;}aside{position:relative;height:100%;width:200px;top:0px;left:0px;background-color:rgb(236, 239, 241);box-shadow:rgba(0, 0, 0, 0.8) 0px 6px 6px 0px;z-index:999999 !important;}[data-sidebar]{display:none;position:absolute;height:100%;z-index:100;}.padding{padding:2em;}.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6{font-family:inherit;font-weight:500;line-height:1.1;color:inherit;}.h4, .h5, .h6, h4, h5, h6{margin-top:10px;margin-bottom:10px;}.h4, h4{font-size:18px;}img{border:0px;vertical-align:middle;}a{text-decoration:none;color:black;}aside a{color:rgb(0, 0, 0);font-size:16px;text-decoration:none;}.fa{display:inline-block;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;line-height:1;font-family:FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;}nav, ol{font-size:18px;margin-top:-4px;background:rgb(0, 150, 136) !important;}.navbar-fixed-top{z-index:100 !important;}.container-fluid{padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto;}ol, ul{margin-top:0px;margin-bottom:10px;}.breadcrumb > li{display:inline-block;}ol ol, ol ul, ul ol, ul ul{margin-bottom:0px;}.nav{padding-left:0px;margin-bottom:0px;list-style:none;}.navbar-nav{margin:0px;float:left;}.navbar-right{margin-right:-15px;float:right !important;}.nav > li{position:relative;display:block;}.navbar-nav > li{float:left;}.dropdown{position:relative;display:inline-block;}.form-group{margin-bottom:15px;}button, input, optgroup, select, textarea{margin:0px;font-style:inherit;font-variant:inherit;font-weight:inherit;font-stretch:inherit;font-size:inherit;line-height:inherit;font-family:inherit;color:inherit;}button, select{text-transform:none;}button, input, select, textarea{font-family:inherit;font-size:inherit;line-height:inherit;}.form-control{display:block;width:100%;height:34px;padding:6px 12px;font-size:14px;line-height:1.42857;color:rgb(85, 85, 85);background-color:rgb(255, 255, 255);background-image:none;border:1px solid rgb(204, 204, 204);border-radius:4px;box-shadow:rgba(0, 0, 0, 0.075) 0px 1px 1px inset;transition:border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;}p{margin:0px 0px 10px;}b, strong{font-weight:700;}.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9{position:relative;min-height:1px;padding-right:15px;padding-left:15px;}.col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9{float:left;}.col-sm-2{width:16.6667%;}.col-sm-offset-1{margin-left:8.33333%;}.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9{float:left;}.col-lg-3{width:25%;}.col-lg-offset-1{margin-left:8.33333%;}.col-sm-3{width:25%;}.glyphicon{position:relative;top:1px;display:inline-block;font-family:"Glyphicons Halflings";font-style:normal;font-weight:400;line-height:1;-webkit-font-smoothing:antialiased;}.caret{display:inline-block;width:0px;height:0px;margin-left:2px;vertical-align:middle;border-top:4px dashed;border-right:4px solid transparent;border-left:4px solid transparent;}.floating-box{display:inline-block;margin:22px;padding:22px;width:203px;height:105px;box-shadow:rgba(0, 0, 0, 0.2) 0px 8px 16px 0px;font-size:20px;}.uil-default-css{position:relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(1){animation:uil-default-anim 1s linear -0.5s infinite;}.uil-default-css > div:nth-of-type(2){animation:uil-default-anim 1s linear -0.416667s infinite;}.uil-default-css > div:nth-of-type(3){animation:uil-default-anim 1s linear -0.333333s infinite;}.uil-default-css > div:nth-of-type(4){animation:uil-default-anim 1s linear -0.25s infinite;}.uil-default-css > div:nth-of-type(5){animation:uil-default-anim 1s linear -0.166667s infinite;}.uil-default-css > div:nth-of-type(6){animation:uil-default-anim 1s linear -0.0833333s infinite;}.uil-default-css > div:nth-of-type(7){animation:uil-default-anim 1s linear 0s infinite;}.uil-default-css > div:nth-of-type(8){animation:uil-default-anim 1s linear 0.0833333s infinite;}.uil-default-css > div:nth-of-type(9){animation:uil-default-anim 1s linear 0.166667s infinite;}.uil-default-css > div:nth-of-type(10){animation:uil-default-anim 1s linear 0.25s infinite;}.uil-default-css > div:nth-of-type(11){animation:uil-default-anim 1s linear 0.333333s infinite;}.uil-default-css > div:nth-of-type(12){animation:uil-default-anim 1s linear 0.416667s infinite;}.dropdown-menu{position:absolute;top:100%;left:0px;z-index:1000;display:none;float:left;min-width:160px;padding:5px 0px;margin:2px 0px 0px;font-size:14px;text-align:left;list-style:none;background-color:rgb(255, 255, 255);-webkit-background-clip:padding-box;background-clip:padding-box;border:1px solid rgba(0, 0, 0, 0.15);border-radius:4px;box-shadow:rgba(0, 0, 0, 0.176) 0px 6px 12px;}button, html input[type="button"], input[type="reset"], input[type="submit"]{-webkit-appearance:button;cursor:pointer;}button{overflow:visible;}.btn{display:inline-block;padding:6px 12px;margin-bottom:0px;font-size:14px;font-weight:400;line-height:1.42857;text-align:center;white-space:nowrap;vertical-align:middle;touch-action:manipulation;cursor:pointer;user-select:none;background-image:none;border:1px solid transparent;border-radius:4px;}.btn-success{color:rgb(255, 255, 255);background-color:rgb(92, 184, 92);border-color:rgb(76, 174, 76);}.btn-group-sm > .btn, .btn-sm{padding:5px 10px;font-size:12px;line-height:1.5;border-radius:3px;}button[disabled], html input[disabled]{cursor:default;}.btn.disabled, .btn[disabled], fieldset[disabled] .btn{cursor:not-allowed;box-shadow:none;opacity:0.65;}.btn-default{color:rgb(51, 51, 51);background-color:rgb(255, 255, 255);border-color:rgb(204, 204, 204);}input{line-height:normal;}
</style>

<style>

form {
	width: 30%;
	margin: 6% 0% 1% 17%;
	padding: 3em 2em 2em 2em;
	background: #fafafa;
	border: 1px solid #ebebeb;
	box-shadow: rgba(0,0,0,0.14902) 0px 1px 1px 0px,rgba(0,0,0,0.09804) 0px 1px 2px 0px;
}

.group { 
	position: relative; 
	margin-bottom: 45px; 
}

input {
	font-size: 18px !important;
	padding: 10px 10px 10px 5px;
	-webkit-appearance: none;
	display: block;
	background: #fafafa;
	color: #636363;
	width: 100%;
	border: none;
	border-radius: 0;
	border-bottom: 1px solid #757575;
}

input:focus { outline: none; }


/* Label */

label {
	color: #999; 
	font-size: 18px;
	font-weight: normal!important;
	position: absolute;
	pointer-events: none;
	left: 5px;
	top: 10px;
	transition: all 0.2s ease;
}
	

/* active */

input:focus ~ label, input.used ~ label {
	top: -20px;
  transform: scale(.75); left: -2px;
	/* font-size: 14px; */
	color: #4a89dc;
}


/* Underline */

.bar {
	position: relative;
	display: block;
	width: 100%;
}

.bar:before, .bar:after {
	content: '';
	height: 2px; 
	width: 0;
	bottom: 1px; 
	position: absolute;
	background: #4a89dc; 
	transition: all 0.2s ease;
}

.bar:before { left: 50%; }

.bar:after { right: 50%; }


/* active */

input:focus ~ .bar:before, input:focus ~ .bar:after { width: 50%; }


/* Highlight */

.highlight {
	position: absolute;
	height: 60%; 
	width: 100px; 
	top: 25%; 
	left: 0;
	pointer-events: none;
	opacity: 0.5;
}


/* active */

input:focus ~ .highlight {
	animation: inputHighlighter 0.3s ease;
}


/* Animations */

@keyframes inputHighlighter {
	from { background: #4a89dc; }
	to 	{ width: 0; background: transparent; }
}


/* Button */

.button {
  position: relative;
  display: inline-block;
  padding: 12px 24px;
  margin: .3em 0 1em 0;
  width: 100%;
  vertical-align: middle;
  color: #fff;
  font-size: 16px;
  line-height: 20px;
  -webkit-font-smoothing: antialiased;
  text-align: center;
  letter-spacing: 1px;
  background: transparent;
  border: 0;
  border-bottom: 2px solid #3160B6;
  cursor: pointer;
  transition: all 0.15s ease;
}
.button:focus { outline: 0; }


/* Button modifiers */

.buttonBlue {
  background: #4a89dc;
  text-shadow: 1px 1px 0 rgba(39, 110, 204, .5);
}

.buttonBlue:hover { background: #357bd8; }


.buttonBlue:disabled {
	background-color: #4a89dc9c;
    border-color: #4a89dc9c;
	cursor:initial;
}
/* Ripples container */

.ripples {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
  background: transparent;
}


/* Ripples circle */

.ripplesCircle {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  opacity: 0;
  width: 0;
  height: 0;
  border-radius: 50%;
  background: rgba(255, 255, 255, 0.25);
}

.ripples.is-active .ripplesCircle {
  animation: ripples .4s ease-in;
}


/* Ripples animation */

@keyframes ripples {
  0% { opacity: 0; }

  25% { opacity: 1; }

  100% {
    width: 200%;
    padding-bottom: 200%;
    opacity: 0;
  }
}
</style>
</head>
<body>
<?php include_once("header.php"); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>



 <!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>

<form class='form-validate' id="form" style="display:none">
	<div class="group">
		<input type="text" tabindex="1" class ="preview" data-copy="#name" id ="title" maxlength="50" name="title"><span class="highlight"></span><span class="bar" ></span>
		<label><b style="color:red">*</b> Title </label>
	</div>
	<div class="group">
		<input type="meassage" class ="preview" data-copy="#message" id = "messagediv" maxlength="200" name="message" tabindex="2"><span class="highlight"></span><span class="bar" ></span>
		<label><b style="color:red">*</b> Message </label>
	</div>
	<div id="first" class="group" style="height:20px;">
		<select id="offer" class="select" size="1" expandto="1" tabindex="3" style="width: 100%;padding: 6px 	12px;font-size: 19px;color: #555;border: 1px solid #ccc;border-radius: 4px;">
			<option value="" selected="selected">Choose Offer</option>
			<?php
				$offer_sql = "SELECT id,title_page,image_app_thumb FROM offers_content WHERE app_live=1" ;
				$offer_get = mysqli_query($conn,$offer_sql);
				while($res_get = mysqli_fetch_object($offer_get)){
						echo '<option value='.$res_get->image_app_thumb.' id = '.$res_get->id.'>'.$res_get->title_page.'</option>';
					}
			?>  
		</select>
	</div>
	<input type="button" value="Submit" tabindex="4" class="button buttonBlue"disabled name="submit" data-controls-modal="mi-modal" data-backdrop="static" data-keyboard="false">
</form>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal" style="margin-top:10%;display:none">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"></button>
        <h5 class="modal-title" id="myModalLabel">Are you sure you want to send notifications</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="modal-btn-si">Yes</button>
        <button type="button" class="btn btn-danger" id="modal-btn-no">No</button>
      </div>
    </div>
  </div>
</div>

<div class="previewdiv" style="width: 48%; float: right; margin-top: -30%;display:block">
<div style="font-size: 22px;text-align: justify;" id="name"></div>
<div style="color:#708090;margin-top: 5px;font-size: 18px;text-align: justify;" id="message"></div>
<div id = "imagediv" style="margin-top:5px">
</div>
</div>


<noscript id="async-styles">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="css/style.css" />

</noscript>
<script> // The CSS mentioned in "async-styles" will be loaded and styles will be applied.
    var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("async-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
        webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function() {
        window.setTimeout(loadDeferredStyles, 0);
    });
    else window.addEventListener('load', loadDeferredStyles);
</script>

<script async src="js/jquery-3.2.1.min.js" onLoad="loadOtherScripts();"></script>

<script>
    function loadScript(src) {
        return new Promise(function (resolve, reject) {
            var s;
            s = document.createElement('script');
            s.src = src;
            s.onload = resolve;
            s.onerror = reject;
            document.head.appendChild(s);
        });
    }

    function loadOtherScripts() {
      loadScript('js/moment.min.js')
      .then(function() {
      Promise.all([loadScript('js/bootstrap.min.js'),loadScript('js/sidebar.js')]).then(function () {

      console.log('scripts are loaded');
      intialFunction();
      }).catch(function (error) {
      console.log('some error!' + error)
      })
      }).catch(function (error) {
      console.log('Moment call error!' + error)
      })
      }
</script>

<script>
  function intialFunction(){
  // automatic refresh
  window.setInterval(function(){
  }, 240000); // milli seconds

  //check online status
  function when_online() {
    document.title = "GoBumpr Bridge";
    $("#form").hide();
    $("#loading").show();
  }
  function when_offline() {
    document.title = "You're Offline!";
    alert("You're Offline! Please check your internet connection");
  }

  // Update the online status icon based on connectivity
  window.addEventListener('online',  when_online);
  window.addEventListener('offline', when_offline);
  
function defaultview(){
   setInterval(function(){  
   $('#form').show();
   $("#loading").hide(); }, 500);
	}
  //on page load
  $(document).ready( function (){
    $('#form').hide();
    $("#loading").show();
	defaultview();
  });
}
</script>
<script>
$(document).ready(function() {

	$('select').on('change', function() {
		$("#imagediv").empty();
		$("#imagediv").css('display','block');
		var image = this.value ;
		if(image != ""){
		$("#imagediv").append('<img src=' + image + ' alt="image" style="height: 45%; width: 85%;"/>');
		}
		var selected_image_path = $("#offer").val();

		if($("#title").val() == '' || $("#messagediv").val() == '' || selected_image_path === "undefined" || selected_image_path == ''){
           $('.button').prop('disabled', true);
		}
		if($("#title").val() != '' && $("#messagediv").val() != ''  && $("#offer").val() != '' && $("#offer").val() != undefined) {
           $('.button').prop('disabled', false);
		   
    }
		
	});
	
	$("#closebtn").click(function(){
		$('.alert').hide();		
	});
	
	$(".preview").on('keyup', function() {
		$($(this).data('copy')).html($(this).val());
			if($("#title").val() != ''){
				$("#name").css('display', 'block');	
			}

			if($("#messagediv").val() != ''){
				$("#message").css('display', 'block');	
			}
			var selected_image_path = $("#offer").val();
		if($("#title").val() == '' || $("#messagediv").val() == '' || selected_image_path === "undefined" || selected_image_path == ''){
           $('.button').prop('disabled', true);
		}
		if($("#title").val() != '' && $("#messagediv").val() != ''  && $("#offer").val() != '' && $("#offer").val() != undefined) {
           $('.button').prop('disabled', false);
		   
    }
  });
});
	
$(window, document, undefined).ready(function() {
  $('input').blur(function() {
    var $this = $(this);
    if ($this.val())
      $this.addClass('used');
    else
      $this.removeClass('used');
  });

  var $ripples = $('.ripples');

  $ripples.on('click.Ripples', function(e) {

    var $this = $(this);
    var $offset = $this.parent().offset();
    var $circle = $this.find('.ripplesCircle');

    var x = e.pageX - $offset.left;
    var y = e.pageY - $offset.top;

    $circle.css({
      top: y + 'px',
      left: x + 'px'
    });

    $this.addClass('is-active');

  });

  $ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function(e) {
  	$(this).removeClass('is-active');
  });

});


var modalConfirm = function(callback){

  $('.buttonBlue').on("click", function(){
	 $("#mi-modal").modal({backdrop: 'static', keyboard: false});
    $("#mi-modal").modal('show');

  });

  $("#modal-btn-si").on("click", function(){
    callback(true);
    $("#mi-modal").modal('hide');
  });
  
  $("#modal-btn-no").on("click", function(){
    callback(false);
    $("#mi-modal").modal('hide');
  });
};
modalConfirm(function(confirm){

  if(confirm){
	var offer_id = $("#offer").children(":selected").attr("id");
	var image_thumb = $("#offer").children(":selected").attr("value");
	var offer_name = $("#offer").children(":selected").text();
	var title = $("#title").val();
	var message = $("#messagediv").val();
   $.ajax({
	  type: "POST",
	  url: "send_notification.php",
	  data:{'offer_id':offer_id,'offer_name':offer_name,'image_thumb':image_thumb,'title':title,'message':message},
	  success : function(data) {
		// window.location.href = "view_notifications.php?title="+title;
	  }
	});
		
  }else{
   return false;
  }
});

</script>

<script>
$(document).ready(function(){
    $('select').focus(function(){
        $(this).attr("size",$(this).attr("expandto")).css('z-index',2);        
    });
    $('select').blur(function(){
        $(this).attr("size",1).css('z-index','1');         
    });
    $('select').change(function(){
        $(this).attr("size",1).css('z-index','1');
    }); 
});
</script>

</body>
</html>

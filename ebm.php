<?php

include("sidebar.php");
// include("config.php");
$conn = db_connect2();
// login or not
if(empty($_SESSION['crm_log_id'])) {
 header('location:logout.php');
 die();
}

?>

<!DOCTYPE html>
<html>
<head>
  <!-- jQuery library -->
  <script src="https://code.jquery.com/jquery-1.10.2.min.js" integrity="sha256-C6CB9UYIS9UJeqinPHWTHVqh/E1uhG5Twh+Y5qFQmYg=" crossorigin="anonymous"></script>
<meta charset="utf-8">

  <title>GoBumpr Bridge</title>

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<!-- table sorter-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.28.9/css/theme.grey.min.css" />
<!-- masking -->
<script type="text/javascript" src="js/inputmask.js"></script>
<script type="text/javascript" src="js/jquery.inputmask.js"></script>
  <script type="text/javascript" src="js/inputmask.extensions.js"></script>

  <!-- auto complete -->
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/jquery-ui.structure.min.css">
<link rel="stylesheet" href="css/jquery-ui.theme.min.css">

<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>
  <style>
   /* axle page cursor */
#range > span:hover{cursor: pointer;}
 #sl{
  cursor:pointer;
}

.floating-box {
  display: inline-block;
 width:44%;
 height:260px;
 box-shadow: 0px 8px 10px 0px rgba(0,0,0,0.2);
  font-size: 15px;
}
.floating-box1 {
    display: inline-block;
  margin:10px;
  float:left;
  clear:both;
 }
 .floating-box2 {
  display: inline-block;
  float: left;
  margin: 12px;
  padding: 12px;
  width:412px;
  height:53px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
   font-size: 15px;
   cursor:pointer;
 }
 .box1
 {
 margin: 12px;
 width:100%;
 height:110px;
 margin-left: 35px;
 box-shadow: 0px 0px 10px 4px rgba(0,0,0,0.2);
 }
 .box_title
 {
  text-align: center;
  padding-bottom: 5px;
 }
.rad input {visibility: hidden; width: 0px; height: 0px;}
label{
    cursor:pointer;
  }
  /*auto complete */
@charset "utf-8";.ui-autocomplete{z-index:1000 !important;cursor:default;list-style:none;}
.ui-widget{}
.ui-autocomplete{overflow-y:auto;overflow-x:hidden;}
.ui-menu{width:0px;display:none;}
.ui-autocomplete > li{padding:10px;padding-left:10px;}
ul{margin-bottom:0;}
.ui-autocomplete > li.ui-state-focus{background-color:#DDD;}
.ui-autocomplete .ui-front .ui-menu .ui-widget .ui-widget-content{border:0;}
.ui-helper-hidden-accessible{display:none;}
.gobumpr-icon{font-style:normal;font-weight:normal;speak:none;display:inline-block;text-decoration:inherit;text-align:center;font-variant:normal;text-transform:none;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
.ui-widget{background-color:white;width:100%;}
.ui-widget-content{padding-left:1px;display:block;width:20px;position:relative;line-height:12px;max-height:210px;border:.5px solid #DADADA;}
.ui-widget{}
.ui-autocomplete { position: absolute; cursor: default;}

/* table */
thead:hover{
  cursor:pointer;
}

.results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px;
  color:#ccc;
}

</style>

</head>
<body>
<?php include_once("header.php"); ?>
<script>
  $(document).ready(function(){
    $('#city').show();
  })
</script>

<div class="overlay" data-sidebar-overlay></div>

<div class="padding"></div>

  <div  id="div1" style="margin-top:10px;float:left;margin-left:10px; border:2px solid #4CB192; border-radius:5px;width:34%;height:650px;">
    <!-- vehicle filter -->
    <div class=" col-xs-1" style="margin-top: 10px;float:left; margin-right:40px;">
        <select id="veh_type" name="veh_type" class="form-control" style="width:69px;">
          <option value="all" selected>All</option>
          <option value="2w">2W</option>
          <option value="4w">4W</option>
        </select>
    </div>

    <!-- Search Bar -->
    <div class=" col-xs-2 form-group" style="margin-top: 10px;float:left; margin-right:40px;">
              <input type="text" class="searchs form-control" placeholder="Search" style="width:135px;">
    </div>
    
<!-- location filter -->
<div class=" col-xs-2  col-lg-offset-1" style="margin-top:10px;float:left;">
    <div class=" form-group" id="loc" style="width:125px;">
            <div class="ui-widget">
              <input class="form-control autocomplete" id="location" type="text" name="location"  required placeholder=" Location">
            </div>
    </div>
 </div>
 <div id="sl" class="form-group col-xs-1 col-lg-offset-1 col-xs-offset-0" style="margin-top:-6px;margin-left:69px;font-size:16px;color:#95A186;">
 &nbsp;<i class="fa fa-search" aria-hidden="true"></i>
 </div>



 <!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>
<!-- table -->
  <div id = "table" align="center" style="max-width:88%;margin-top:60px; margin-left:20px; display:none;height:580px !important;overflow:auto !important;">
  <table id="example" class="table table-striped table-bordered tablesorter table-hover results"  >
  <thead style="background-color: #D3D3D3;">
  <th>No</th>
  <th>Shop Name</th>
  <th>Credits</th>
  </thead>
  <tbody id="tbody">
  </tbody>
  </table>
  </div>


  </div> <!-- div 1 -->
  <div id="div2" style="margin-top:10px;margin-left:30px; float:left; border:2px solid #DFA64E; border-radius:5px;width:61%;height:650px;">

    <!-- date range picker -->
   <div id="reportrange" class=" col-xs-2 col-lg-3" style="cursor: pointer;">
      <div class=" floating-box1">
        <div id="range" class="form-control" style="min-width:282px;">
        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
       <span id="dateval"></span> <b class="caret"></b>
     </div>
       </div>
   </div>

   <div class="col-xs-2 col-lg-offset-1" style="cursor: pointer;margin-left: 80px;">
  <div class=" floating-box1">
    <select id="service" name="service" class="form-control" style="width:240px;margin-left: 20px;cursor: pointer;">
        <option value="all" selected>All Services</option>
        <?php
        $sql_service = "SELECT DISTINCT master_service FROM go_bumpr.go_axle_service_price_tbl WHERE bridge_flag='0' ORDER BY service_type ASC";
        $res_service = mysqli_query($conn,$sql_service);
        while($row_service = mysqli_fetch_object($res_service)){
            $c_service = $row_service->master_service;
            ?>
            <option value="<?php echo $c_service; ?>" > <?php echo $c_service; ?> </option>
            <?php
        }
        ?>
    </select>
    </div>
  </div>
  <div class=" col-xs-2 col-lg-offset-1">
      <div  class="floating-box1" >
          <select id="reason" name="reason" class="form-control" style="width:220px;cursor: pointer; margin-left:75px;">
           <option value="all" selected>All reasons</option>
            <?php
            $sql_reason = "SELECT DISTINCT exception_reason FROM go_bumpr.exception_mechanism_track where exception_reason!='' AND exception_reason IS NOT NULL";
            $res_reason = mysqli_query($conn,$sql_reason);
            while($row_reason = mysqli_fetch_object($res_reason)){
                ?>
                <option value="<?php echo $row_reason->exception_reason; ?>"><?php echo $row_reason->exception_reason; ?></option>
                <?php
            }
            ?>
          </select>
      </div>
  </div>
   <!-- loading -->
   <div id="loading1" style="display:none; margin-top:140px;" align="center">
    <div class='uil-default-css' style='transform:scale(0.58);'>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
   </div>
   </div>


    <div id="div3" style="margin-top:83px;border-radius:5px;width: 90%;">
    
  <div class="box1" id="total_credits">
   <div class="row">
    <div id="credits_deducted" class="col-xs-6 col-md-6"  style="padding-top: 3px;">
      <div id="box" style="height: 80px; border-right: 1px solid gray;">
          <h4 style="background-color: rgba(37, 33, 57, 0.84);width: 220px;padding:6px;color: white;text-align: center;border-radius: 0px 30px 30px 0px;">Total credits deducted</h4>
          <p id="tbill" style="font-size: 35px;text-align: center;">0</p>
       </div>
    </div>
    <div id="exception_count" class="col-xs-6 col-md-6">
       <h4 style="background-color: rgba(37, 33, 57, 0.84);width: 220px;padding:6px;color: white;float: right;text-align: center;border-radius: 30px 0px 0px 30px;">Total exceptions</h4>
       <p id="tbill" style="font-size: 35px;text-align: center;padding-top:55px;padding-left: 40px;">0</p>
    </div>
    </div>
  </div>

   <div class="box1" id="reason_credits" style="margin-top: 20px;">
 <div class="col-xs-4 col-md-4" align="center">
  <div style="height: 60px;padding-left: 10px;padding-top: 10px;">
      <span style="font-size: 18px; color: rgba(37, 33, 57, 0.84);width:100%;vertical-align: center;">Estimate not given</span>
    </div>
      <span style="text-align: center;font-size: 25px;"> 
      0</span>

  </div>
  <div class="col-xs-4 col-md-4" align="center">
  <div style="height: 60px;padding-left: 10px;padding-top: 10px;">
      <span style="font-size: 18px; color: rgba(37, 33, 57, 0.84);width:100%;vertical-align: center;">Check-in report not updated</span>
    </div>
      <span style="text-align: center;font-size: 25px;"> 
      0</span>

  </div>
  <div class="col-xs-4 col-md-4" align="center">
  <div style="height: 60px;padding-top: 10px;">
      <span style="font-size: 16px; color: rgba(37, 33, 57, 0.84);width:100%;vertical-align: center;">Ready stage not updated</span>
    </div>
      <span style="text-align: center;font-size: 25px;"> 
      0</span>

  </div>
</div>

 <div id="box" style=";width: 100%;margin-left: 15px;">
  <div class="row">
 <div class="floating-box col-xs-6 col-md-6" id="exception_services"  style="background-color:rgba(210, 209, 218, 0.14);float: left;margin-left: 50px;">
  <h3 class="box_title" style="color: rgba(37, 33, 57, 0.84);">Frequent exception services</h3>
      <p style="font-size: 17px;text-align: center;margin-top: 20px;">No results found...!</p>
    </div>
      <div class="floating-box col-xs-6 col-md-6" id="exception_garages" style="background-color:rgba(210, 209, 218, 0.14);float: right;">
    <h3 class="box_title" style="color: rgba(37, 33, 57, 0.84);">Frequent exception garages</h3>
      <p style="font-size: 17px;text-align: center;margin-top: 20px;">No  results found...!</p>
    </div>
  </div>
</div>
</div>
  <!-- table sorter -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.28.9/js/jquery.tablesorter.min.js"></script> <!-- date picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<!-- search bar  -->
<script>
$(document).ready(function() {

  $(".searchs").keyup(function () {
    var searchTerm = $(".searchs").val();
    var listItem = $('.results tbody').children('tr');
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

  $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });

  $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','false');
  });

  $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','true');
  });

  var jobCount = $('.results tbody tr[visible="true"]').length;
    $('.counter').text(jobCount + ' item');

  if(jobCount == '0') {$('.no-result').show();}
    else {$('.no-result').hide();
  }
      });
});
</script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- date range picker -->
<script>
$('input[name="daterange"]').daterangepicker({
 locale: {
      format: 'DD-MM-YYYY'
    }
});
</script>
<script type="text/javascript">
$(function() {

  var start = moment().subtract(200, 'days');
   // var start = moment();
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});
</script>

<!-- select Location of service center -->
<script>
function locality() {
  var city = $('#city').val();
          $( "#location" ).autocomplete({
source: function(request, response) {
  
            $.ajax({
                url: "ajax/get_b2b_loc.php",
                data: {
                    term: request.term,
          city:city,
           },
                dataType: 'json'
            }).done(function(data) {
        //alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
          //console.log(data);
                    }));
                } else {
          response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
     open: function(event, ui) {
            $(".ui-autocomplete").css("position", "absolute");
            $(".ui-autocomplete").css("z-index", "99999");
    },
     change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
       $(this).autocomplete("search");
    });
}
</script>
 <script>
$(
  locality()
);
</script>
<script>
function view_shops(){
  var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
  var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
  var veh = $("#veh_type").val();
  var loc = $("#location").val();
  var city = $("#city").val();
  $.ajax({
        url : "ajax/shop_view_ebm.php",  // create a new php page to handle ajax request
        type : "POST",
        data : {"startdate": startDate , "enddate": endDate,"veh": veh ,"loc":loc, "city": city },
        success : function(data) {
    $("#loading").hide();
    $("#table").show();
    if(data == 'no'){
      $('#table').html("<h3 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h3>");
    }
    else{
       $('#tbody').html(data);
       $("#example").trigger("update");
    }
    },
      error: function(xhr, ajaxOptions, thrownError) {
        //alert(xhr.status + " "+ thrownError);
     }
   });
}
</script>
<!-- default shops view -->
<script>
$(document).ready(function(){
  $("#table").hide();
  $("#loading").show();
    view_shops();
});
</script>
<!-- shops view on changing veh type -->
<script>
$(document).ready(function(){
  $("#veh_type").change(function(){
    $("#table").hide();
    $("#loading").show();
    view_shops();
  });
});
</script>
<!-- shops view on changing location -->
<script>
$(document).ready(function(){
$('#dateval').on("DOMSubtreeModified", function (){
   $("#table").hide();
        $("#loading").show();
        view_shops();
      });
});
</script>
<!-- shops view on changing date -->
<script>
$(document).ready(function(){
 $("#location").on("autocompletechange", function(){
      $("#table").hide();
      $("#loading").show();
      view_shops();
  });
});
</script>
<!-- shops view on changing city -->
<script>

$(document).ready(function(){
 $("#city").change(function(){
      $("#table").hide();
      $("#loading").show();
      $("#location").autocomplete('close').val('');     
      locality();
      view_shops();
  });
});
</script>
<script>
function view_exceptions(){
  var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
  var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
  var service = $('#service').val();
   var reason = $('#reason').val();
   var city = $("#city").val();
     //console.log(endDate);
  //Make AJAX request, using the selected value as the POST
  $.ajax({
    url : "ajax/ebm_view.php",  // create a new php page to handle ajax request
    type : "GET", json : false,
    data : {"startdate": startDate , "enddate": endDate,"service": service ,"reason":reason,"city":city},
    success : function(data) {
      //alert(data);
            $("#loading1").hide();
        $("#div3").show();
        $("#div3").html(data);
    },
    error: function(xhr, ajaxOptions, thrownError) {
        // alert(xhr.status + " "+ thrownError);
    }
  });
}
</script>

<script>
function get_count(id){
   var x= id;
   var str='shop_id';
   var encode_str = btoa(str);
  $('input[name="radio_shop"]').not(':checked').next('label').css('color', 'black');
  $('input[name="radio_shop"]:checked').next('label').css('color', '#ffa800');
  window.location.href="ebm_details.php?"+encode_str+"="+x;
}
</script>
<!-- get count on changing dates -->
<script>
$(document).ready(function() {
   $("#div3").hide();
        $("#loading1").show();
        view_exceptions();
$('#dateval').on("DOMSubtreeModified", function (){
   $("#div3").hide();
        $("#loading1").show();
        view_exceptions();
      });
$('#reason').on("change",function (){
        $("#div3").hide();
        $("#loading1").show();
        view_exceptions();
      });

      $("#service").on("change" , function (){
        $("#div3").hide();
        $("#loading1").show();
        view_exceptions();
      });

      $("#city").on("change" , function (){
        $("#div3").hide();
        $("#loading1").show();
        view_exceptions();
      });

});
</script>
<script>
$(document).ready(function()
  {
      $("#example").tablesorter(  {sortList: [[0,2], [0,0]]} );
  }
);
</script>

</body>
</html>

<?php
date_default_timezone_set('Asia/Kolkata');
$today = date('Y-m-d H:i:s');
$hostname = "briaxpgbmpr.cx4twoxoumct.ap-southeast-1.rds.amazonaws.com";
$username = "awsgobumpr";
$password = "gobumpr123";
$database = "b2b";

$conn = mysqli_connect($hostname, $username, $password,$database) or die(mysqli_error($conn));
if(!$conn){
  echo "Database Connection Error!";
}
else{
 $shop_id = base64_decode($_GET['id']);
 $payment_id = base64_decode($_GET['pay_id']);

  $sql_fetch = "SELECT b2b_credits,b2b_credit_amt,b2b_approve_flag,b2b_cus_name FROM b2b_credits_payment WHERE b2b_id = '$payment_id'";
  $res_fetch = mysqli_query($conn,$sql_fetch);
  $row_fetch = mysqli_fetch_array($res_fetch);
  $credits = $row_fetch['b2b_credits'];
  $credit_amt = $row_fetch['b2b_credit_amt'];
  $approve_flag = $row_fetch['b2b_approve_flag'];
  $shop_name = $row_fetch['b2b_cus_name'];
}
 ?>
 <!DOCTYPE html>
 <html lang="en">
 <head>
 <meta charset="utf-8">
 <title>GoBumpr Bridge</title>
 <style>
 body{
   align-content: center;
   background-color: #fafafa;
   font-family: sans-serif;
   color: #000;
   font-style: normal;
 }

 </style>
 </head>
 <body>
<div align="center" style="margin-top:50px;">
  <?php if($approve_flag != '1'){ ?>
    <p style="font-size:37px; color:#000;"><?php echo $credits; ?> Credits has already been added to "<?php echo strtoupper($shop_name); ?>" !</p>
  <?php } else{ 
      //get credits
      $sql_get_credits = "SELECT b2b_credits,b2b_cash_remaining FROM b2b_credits_tbl WHERE b2b_shop_id='$shop_id'";
      $res_get_credits = mysqli_query($conn,$sql_get_credits);
      $row_get_credits = mysqli_fetch_object($res_get_credits);
      $avl_credits = $row_get_credits->b2b_credits;
      $avl_amount = $row_get_credits->b2b_cash_remaining;
      //add credits
      $total_credits = $avl_credits+$credits;
      $total_amount = $avl_amount+$credit_amt;

      $sql_updt_credits = "UPDATE b2b_credits_tbl SET b2b_credits='$total_credits',b2b_cash_remaining='$total_amount',b2b_mod_log='$today' WHERE b2b_shop_id='$shop_id' ";
      $res_updt_credits = mysqli_query($conn,$sql_updt_credits);

      //remove flag
      $sql_updt_payment = "UPDATE b2b_credits_payment SET b2b_approve_flag='0',b2b_mod_log='$today',b2b_pmt_scs_flg='1',b2b_pmt_scs_date='$today' WHERE b2b_id = '$payment_id'";
      $res_updt_payment = mysqli_query($conn,$sql_updt_payment);

  ?>
    <p style="font-size:37px; color:#000;"><?php echo $credits; ?> Credits has been added to "<?php echo strtoupper($shop_name); ?>" !</p>
  <?php } ?>
</div>

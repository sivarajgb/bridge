<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['basicAuth'])->group(function () {
    //All the routes are placed in here
    Route::get('/', 'ShopAvailabilityController@index');

    Route::get('/getCities', 'ShopAvailabilityController@getCities')->name('getCities');
    Route::get('/getServiceTypes', 'ShopAvailabilityController@getServiceTypes')->name('getServiceTypes');
    Route::post('/getLocalitiesByCity', 'ShopAvailabilityController@getLocalitiesByCity')->name('getLocalitiesByCity');
    Route::post('/getShopAvailability', 'ShopAvailabilityController@getShopAvailability')->name('getShopAvailability');
    Route::post('/getShopDetails', 'ShopAvailabilityController@getShopDetails')->name('getShopDetails');
});

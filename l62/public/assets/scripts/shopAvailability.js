//Loading UI
var blockUi = '<div class = "loader mx-auto" > <div class="line-scale-pulse-out"> <div class="bg-success"> </div><div class = "bg-success"> </div> <div class = "bg-success" > </div><div class = "bg-success"> </div> <div class = "bg-success" > </div> </div> </div>';

$(function () {
    'use strict';

    $('select').select2();//Select Init

    //Select Init without search
    $('select#vehicle_type').select2({
        minimumResultsForSearch: Infinity
    });

    //DatePicker Init
    $('input[name="date_range"]').daterangepicker({
        startDate: moment(),
        endDate: moment(),
        maxDate: moment(),
        locale: {
            format: 'MMM D, YYYY'
        },
        singleDatePicker: true,
        alwaysShowCalendars: false,
    }, cb);

    //DatePicker Callback
    function cb(start, end) {
        // console.log(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    //DatePicker On Change
    $('input[name="date_range"]').on('apply.daterangepicker', function (ev, picker) {
        // console?.log(picker.startDate.format('YYYY-MM-DD'));
        // console.log(picker.endDate.format('YYYY-MM-DD'));
        get_shop_availability();
    });

    //Vehicle OnChange
    $('#search_form #vehicle_type').change(function () {
        get_shop_availability();
    });

    //City OnChange
    $('#search_form #city').change(function () {
        get_locality_by_city();
        get_shop_availability();
    });

    //ServiceType OnChange
    $('#search_form #service_type').change(function () {
        get_shop_availability();
    });

    //Locality OnChange
    $('#search_form #locality').change(function () {
        get_shop_availability();
    });

    //Filter Table Rows Based On data-marker
    $('#table_filter').change(function () {
        if ($(this).val() == 'all') {
            $("#shopAvailabilityTableView tr").show();
        } else {
            // $("#shopAvailabilityTableView td.col1:contains('" + $(this).val() + "')").parent().show();
            // $("#shopAvailabilityTableView td.col1:not(:contains('" + $(this).val() + "'))").parent().hide();

            $("#shopAvailabilityTableView td.col1[data-marker*='" + $(this).val() + "']").parent().show();
            $("#shopAvailabilityTableView td.col1:not([data-marker*='" + $(this).val() + "'])").parent().hide();
        }
    });

    get_cities();//Get Cities On page load
    get_service_types();//get service types on page load
});

//Get Cities
var get_cities = function () {
    jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    jQuery.ajax({
        url: getCitiesUrl,
        method: 'get',
        data: '',
        success: function (data) {
            //if any validation errors from laravel
            if ('errors' in data) {
                jQuery.each(data.errors, function (key, value) {

                });
            }

            //if Success
            if ('success' in data) {
                let cityOption = '<option value="" selected disabled>Select City</option>';
                jQuery.each(data.city, function (key, value) {
                    cityOption += '<option value="' + value + '">' + value + '</option>';
                });

                $('#city').html(cityOption);//set option data to html

                //Choose default City once loaded from api
                if (data.defaultCity != 'all') {
                    $('#search_form #city').val(data.defaultCity).change();
                } else {
                    $('#search_form #city').val('Chennai').change();
                }
            }
        }
    });
};

//Get Service Types
var get_service_types = function () {
    jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    jQuery.ajax({
        url: getServiceTypesUrl,
        method: 'get',
        data: '',
        success: function (data) {
            //if any validation errors from laravel
            if ('errors' in data) {
                jQuery.each(data.errors, function (key, value) {

                });
            }

            //If Success
            if ('success' in data) {
                let serviceTypeOption = '<option value="all" selected>All Services</option>';
                jQuery.each(data.serviceTypes, function (key, value) {
                    // console.log(value);
                    serviceTypeOption += '<option value="' + value + '">' + value + '</option>';
                });
                $('#service_type').html(serviceTypeOption);//set option data to html
            }
        }
    });
};

//Get Locality Based On City Change
var get_locality_by_city = function () {
    var city = $.trim($('#search_form #city').val());

    var postData = {"_token": csrf_token, 'city': city};

    jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    jQuery.ajax({
        url: getLocalitiesByCityUrl,
        method: 'post',
        data: postData,
        success: function (data) {
            //if any validation errors from laravel
            if ('errors' in data) {
                jQuery.each(data.errors, function (key, value) {

                });
            }

            //If Success
            if ('success' in data) {
                let localityOption = '<option value="all" selected>All Locality</option>';
                jQuery.each(data.locality, function (key, value) {
                    localityOption += '<option value="' + value + '">' + value + '</option>';
                });
                $('#locality').html(localityOption);//set option data to html
            }
        }
    });
};

//Get Available Shops
var get_shop_availability = function () {
    var city = $.trim($('#search_form #city').val()) != '' ? $.trim($('#search_form #city').val()) : 'all';
    var locality = $.trim($('#search_form #locality').val()) != '' ? $.trim($('#search_form #locality').val()) : 'all';
    var vehicle_type = $.trim($('#search_form #vehicle_type').val()) != '' ? $.trim($('#search_form #vehicle_type').val()) : 'all';
    var date_range = $.trim($('#search_form #date_range').val());
    var startDate = $('input[name="date_range"]').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var endDate = $('input[name="date_range"]').data('daterangepicker').endDate.format('YYYY-MM-DD');
    var service_type = $.trim($('#search_form #service_type').val()) != '' ? $.trim($('#search_form #service_type').val()) : 'all';

    if (city == '') {
        $.toast({
            heading: 'Error',
            text: 'Please Choose City',
            showHideTransition: 'fade',
            position: 'top-center',
            icon: 'error'
        });
        return false;
    }

    closeInfoBox();//Close InfoBox

    var postData = {
        "_token": csrf_token,
        'city': city,
        'locality': locality,
        'vehicle_type': vehicle_type,
        'service_type': service_type,
        'date_range': date_range,
        'start_Date': startDate,
        'end_Date': endDate
    };

    jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    $('.tab-pane').block({message: blockUi});//Show Loader on particular element Section

    jQuery.ajax({
        url: getShopAvailabilityUrl,
        method: 'post',
        data: postData,
        success: function (data) {
            //if any validation errors from laravel
            if ('errors' in data) {
                jQuery.each(data.errors, function (key, value) {

                });
            }

            //If Success
            if ('success' in data) {
                //Filter & get lat != to 0, Because data has 0,0 lat,lng
                var lat_lng = data.availableShops.filter(function (prop) {
                    return prop.b2b_lat != 0;
                });

                //add markers to map
                addMarkers(lat_lng, map);

                $('#shopAvailabilityTableView').html(data.tableData);//set data to table

                //get total shops Count
                $('.allCount').html('(' + data.availableShops.length + ')');

                //Get Count Only specified marker
                var green = data.availableShops.filter(function (d) {
                    if (d.marker == 'green')
                        return d;
                });
                $('.greenCount').html('(' + green.length + ')');//set count to ui

                //Get Count Only specified marker
                var yellow = data.availableShops.filter(function (d) {
                    if (d.marker == 'yellow')
                        return d;
                });
                $('.yellowCount').html('(' + yellow.length + ')');//set count to ui

                //Get Count Only specified marker
                var red = data.availableShops.filter(function (d) {
                    if (d.marker == 'red')
                        return d;
                });
                $('.redCount').html('(' + red.length + ')');//set count to ui

                $('.tab-pane').unblock();//hide loader
            }
        }
    });
};

//Create Marker InfoBox Window While Click on Marker by Ajax
//This Function is called from shopAvailabilityMap.js file
// $.extend({
var createInfoWindow = function (shop_id) {
    var postData = {"_token": csrf_token, 'shop_id': shop_id};

    var templateResponse = null;
    jQuery.ajax({
        url: getShopDetailsUrl,
        method: 'post',
        data: postData,
        async: false,
        success: function (data) {
            templateResponse = data.template;
        }
    });
    //return response window to shopAvailabilityMap.js file
    return templateResponse;
};
// });

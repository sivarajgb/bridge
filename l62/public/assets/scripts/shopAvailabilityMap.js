var base_url = '';

// (function ($) {
// "use strict";

var map;
var windowHeight;
var windowWidth;
var contentHeight;
var contentWidth;

// calculations for elements that changes size on window resize
var windowResizeHandler = function () {
    windowHeight = window.innerHeight;
    windowWidth = $(window).width();
    contentHeight = windowHeight - $('.app-header').height();
    $('#mapView').height(contentHeight - $('.app-page-title').outerHeight() - $('.main-card').outerHeight() - $('.body-tabs').outerHeight() - 30);

    if (map) {
        google.maps.event.trigger(map, 'resize');
    }
};

windowResizeHandler();

var markerImg = "../../../public/assets/images/marker-green.png";

// Custom options for map
var options = {
    zoom: 20,
    disableDefaultUI: false, // or true
    mapTypeControlOptions: {
        mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain']
    }
};

var markers = [];

// custom infowindow object
var infobox = new InfoBox({
    disableAutoPan: false,
    maxWidth: 400,
    pixelOffset: new google.maps.Size(-150, -250),
    zIndex: null,
    boxClass: "infoBox",
    boxStyle: {
        // background: "url('" + base_url + "/l6/public/assets/images/infobox-bg.png') no-repeat",
        opacity: 1,
        width: "400px",
        height: "auto"
    },
    closeBoxMargin: "10px 10px 0px 0px",
    // closeBoxURL: "",
    infoBoxClearance: new google.maps.Size(1, 1),
    pane: "floatPane",
    enableEventPropagation: false
});

var addMarkers = function (props, map) {
    var bounds = new google.maps.LatLngBounds();
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    deleteMarkers();//remove markers before add new

    $.each(props, function (i, prop) {
        var latlng = new google.maps.LatLng(prop.b2b_lat, prop.b2b_lng);

        bounds.extend(latlng);//Extend visible map boundry while each marker adding
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            icon: new google.maps.MarkerImage(
                "http://maps.google.com/mapfiles/ms/micons/" + prop.marker + ".png",
                null,
                null,
                null,
                new google.maps.Size(24, 24)
            ),
            visible: true,
            marker: prop.marker,
            shop_id: prop.shop_id,
            draggable: false,
            animation: google.maps.Animation.DROP,
        });

        // Add info window to marker
        google.maps.event.addListener(marker, 'click', function () {
            // console.log(marker.get('shop_id'));
            // console.log(i);
            infobox.open(null, null);
            let infoboxContent = createInfoWindow(marker.get('shop_id'));
            infobox.setContent(infoboxContent);
            // console.log(infoContent);
            infobox.open(map, marker);
        });

        // google.maps.event.addListener(marker, 'click', (function (marker, i) {
        //     return function () {
        //         infobox.setContent(infoboxContent);
        //         infobox.open(map, marker);
        //         map.setZoom(14);
        //     }
        // })(marker, i));

        // google.maps.event.addListener(marker, 'mouseout', (function (marker, i) {
        //     return function () {
        //         infobox.open(null, null);
        //     }
        // })(marker, i));
        //
        // google.maps.event.addListener(marker, 'click', (function (marker, i) {
        //     return function () {
        //         infobox.setContent(infoboxContent);
        //         infobox.open(map, marker);
        //         //map.setZoom(14);
        //     }
        // })(marker, i));

        //While we nevigate between tabs reinit map
        $('.body-tabs li a[href="#tab-content-0"]').on('click', function (e) {
            setTimeout(function () {
                google.maps.event.trigger(map, 'resize');
                console.log('resize');
                map.panToBounds(bounds);    //# auto-center
                map.fitBounds(bounds);      //# auto-zoom
            }, 300);
        });

        $(document).on('click', '.closeInfo', function () {
            infobox.open(null, null);
        });

        //if we click anywhere on map infobox will close
        google.maps.event.addListener(map, "click", function (event) {
            // console.log(event);
            infobox.open(null, null);
        });

        markers.push(marker);
        // console.log(markers);
        map.panToBounds(bounds);    //# auto-center
        map.fitBounds(bounds);      //# auto-zoom
    });
};

$(window).resize(function () {
    windowResizeHandler();
});

//Filter Markers
filterMarkers = function (category) {
    var bounds = new google.maps.LatLngBounds();
    for (i = 0; i < markers.length; i++) {
        marker = markers[i];
        if (category != 'all') {
            // If is same category or category not picked
            if (marker.marker == category) {
                marker.setVisible(true);
                bounds.extend(marker.getPosition());
            }
            // Categories don't match
            else {
                marker.setVisible(false);
            }
        } else {
            marker.setVisible(true);
            bounds.extend(marker.getPosition());
        }
        console.log(map.getBounds());
        map.panToBounds(bounds);
        map.fitBounds(bounds);
    }
};

function deleteMarkers() {
    while (markers.length) {
        markers.pop().setMap(null);
    }
}

function closeInfoBox() {
    infobox.open(null, null);
}

function createInfoWindoww(prop) {
    return infoboxContent = '<div class="infoW" onclick="event.stopPropagation();" style="height: 100%">' +
        '</div>';
}

//Initialize Google Map
function initialize() {
    map = new google.maps.Map(document.getElementById('mapView'), options);
    map.setCenter(new google.maps.LatLng('13.0827', '80.2707'));//Default Location
    map.setZoom(12);//Default Zoom
    //here also we can call add markers with lat & lan
}

//Initialize the map on page Load
google.maps.event.addDomListener(window, 'load', initialize);

//Marker Trigger while mouse enter on property
// $('.card').each(function (i) {
//     $(this).on('mouseenter', function () {
//         if (Map) {
//             google.maps.event.trigger(markers[i], 'click');
//         }
//     });
//     $(this).on('mouseleave', function () {
//         infobox.open(null, null);
//     });
// });
// })(jQuery);

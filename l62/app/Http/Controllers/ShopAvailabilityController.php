<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\ShopAvailabilityInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShopAvailabilityController extends Controller
{
    protected $shopAvailability;

    public function __construct(ShopAvailabilityInterface $shopAvailability)
    {
        $this->shopAvailability = $shopAvailability;
    }

    public function index()
    {
        return view('map');
    }

    public function getCities(Request $request)
    {
        $crm_city = $request->session()->get('cc');
        $data = DB::table('go_bumpr.localities as gl')
            // ->where('bm.b2b_address5', '<>', '')
            ->when(($crm_city != 'all'), function ($query) use ($crm_city) {
                return $query->where('city', $crm_city);
            })
            ->distinct('gl.localities')
            ->orderBy('gl.city', 'asc')
            ->pluck('gl.city');
        return ['success' => true, 'city' => $data->toArray(), 'defaultCity' => $crm_city];
        // return response()->json(['success' => true, ]);
    }

    public function getServiceTypes(Request $request)
    {
        $data = DB::table('go_bumpr.go_axle_service_price_tbl as st')
            ->where('st.bridge_flag', 0)
            ->orderBy('st.service_type', 'asc')
            ->pluck('st.service_type');
        return ['success' => true, 'serviceTypes' => $data->toArray()];
        // return response()->json(['success' => true, ]);
    }

    public function getLocalitiesByCity(Request $request)
    {
        $data = DB::table('go_bumpr.localities as gl')
            ->where('gl.city', $request->get('city'))
            ->distinct('gl.localities')
            ->pluck('gl.localities');
        return ['success' => true, 'locality' => $data->toArray()];
    }

    public function getShopAvailability(Request $request)
    {

        // dd($request->all());

        $city = $request->get('city');
        $locality = $request->get('locality');
        $veh = $request->get('vehicle_type');
        $service = $request->get('service_type');
        $start_date = $request->get('start_Date');
        $end_date = $request->get('end_Date');

        $shop_status = 'active';

        $cond = '';
        $cond_avail = '';
        $cond1 = '';
        $cond2 = '';
        $cond = $cond . ($veh == "all" ? "" : "AND m.type='$veh'");
        //$cond2=$cond2.($service='all'?"": AND service)
        $cond2 = $cond2 . ($veh == "all" ? "" : " AND type='$veh'");
        //$cond = $cond.($service == 'all' ? "" : "AND st.service_type='$service'");

        //$cond = $cond.($shop_status == "active" ? "AND b2b.b2b_avail='1'" : "AND b2b.b2b_avail='0'");

        $qry_service = "SELECT service_type_column,type FROM go_axle_service_price_tbl WHERE service_type='$service' $cond2 ";
        DB::enableQueryLog();
        $data = DB::select("SELECT service_type_column,type FROM go_axle_service_price_tbl WHERE service_type='$service' $cond2");
        // dd(DB::getQueryLog());
        $res = $data;

        // dd($res);
        $servicetype = '';
        $type = '';
        if (count($res)) {
            $servicetype = $res[0]->service_type_column;
            $type = $res[0]->type;
        }
        if (!empty($servicetype)) {
            $cond = $cond . ($service == 'all' ? "" : "AND st.$servicetype='1'");
        } else {
            $cond = $cond . ($service == 'all' ? "" : "");
        }
        $cond = $cond . ($service == 'all' ? "" : "AND st.type='$type'");
        $cond1 = $cond1 . ($shop_status == 'active' ? "status='1'" : "status='0'");

        $condLocality = ($locality == 'all' ? "" : "AND m.address4='$locality'");

        // DB::select
        $availableShops = ("SELECT tbn.*, bc.b2b_credits, bc.b2b_leads, bc.b2b_re_leads, bc.b2b_non_re_leads, 
CASE WHEN tbn.status = 1 
                    THEN 
	                    CASE WHEN bc.b2b_credits > 2 OR bc.b2b_leads >= 1
	                        THEN 'green' 
	                        ELSE 'red' 
	                    END 
                    ELSE 
                        CASE WHEN bc.b2b_credits > 2 OR bc.b2b_leads >= 1
	                        THEN 'yellow' 
	                    END
                END AS marker FROM(
(SELECT COUNT(*) AS COUNT,b2b_avail AS STATUS,shop_name AS b2b_shop_name,address4 AS b2b_address4,address5 AS b2b_address5,axle_id,mec_id, lat AS b2b_lat, lng AS b2b_lng, shop_id,vehicle_type AS b2b_vehicle_type,wkly_counter
FROM(
	SELECT b2b.b2b_avail_tbl.*, b2b_avail AS STATUS, shop_name, mec_id, address4,address5, axle_id, lat, lng, shop_id,vehicle_type,wkly_counter
	FROM b2b.b2b_avail_tbl
	INNER JOIN (
		SELECT MAX(b2b.b2b_mod_log) AS dates, m.shop_name, m.lat, m.lng, b2b.b2b_avail AS STATUS, b2b.b2b_shop_id AS shop_id,m.wkly_counter, m.type AS vehicle_type, m.mec_id, m.address4,m.address5, b2b_reason, m.axle_id
		FROM b2b.b2b_avail_tbl b2b
		INNER JOIN admin_mechanic_table m ON m.axle_id=b2b.b2b_shop_id
		LEFT JOIN admin_service_type AS st ON m.mec_id=st.mec_id
		WHERE m.status='0' AND m.type!='' AND m.address5='$city' $condLocality AND m.type!='tyres' $cond AND DATE(b2b.b2b_mod_log) BETWEEN '$start_date' AND '$end_date'
		GROUP BY DATE(b2b.b2b_mod_log), m.mec_id
		) AS tb ON tb.dates=b2b_avail_tbl.b2b_mod_log AND tb.axle_id=b2b_avail_tbl.b2b_shop_id
	) AS tb1
WHERE $cond1
GROUP BY mec_id) 

UNION ALL

SELECT 0 AS COUNT, b2b.b2b_avail AS STATUS, shop_name AS b2b_shop_name, address4 AS b2b_address4,address5 AS b2b_address5, axle_id, m.mec_id,m.lat AS b2b_lat,m.lng AS b2b_lng,b2b_shop_id AS shop_id,m.type AS b2b_vehicle_type,m.wkly_counter
FROM b2b.b2b_mec_tbl b2b
INNER JOIN admin_mechanic_table m ON b2b.b2b_shop_id=m.axle_id
INNER JOIN admin_service_type st ON m.mec_id=st.mec_id
WHERE m.address5='$city' $condLocality AND m.axle_id>1000 AND m.type!='' AND m.type!='both' AND m.type!='tyres' AND m.status='0' $cond
HAVING b2b.b2b_avail='0' AND (m.axle_id,m.mec_id) NOT IN (
	SELECT axle_id,mec_id
	FROM(
		SELECT b2b.b2b_avail_tbl.*,shop_name,mec_id,address4,address5,axle_id,b2b_avail AS STATUS
		FROM b2b.b2b_avail_tbl
		INNER JOIN (
			SELECT MAX(b2b.b2b_mod_log) AS dates,m.shop_name, b2b_avail AS STATUS,b2b_shop_id,m.mec_id,m.address4,m.address5,b2b_reason,m.axle_id,m.lat,m.lng
			FROM b2b.b2b_avail_tbl b2b
			INNER JOIN admin_mechanic_table m ON m.axle_id=b2b.b2b_shop_id
			LEFT JOIN admin_service_type AS st ON m.mec_id=st.mec_id
			WHERE m.status='0' AND m.type!='' AND m.type!='tyres' AND m.address5='$city' $condLocality $cond AND DATE(b2b.b2b_mod_log) BETWEEN '$start_date' AND '$end_date'
			GROUP BY DATE(b2b.b2b_mod_log),m.mec_id
		) AS tb ON tb.dates=b2b_avail_tbl.b2b_mod_log AND tb.axle_id=b2b_avail_tbl.b2b_shop_id) AS tb1
	GROUP BY mec_id) UNION ALL (
SELECT COUNT(*) AS COUNT, STATUS,shop_name,address4,address5,axle_id,mec_id,lat,lng,shop_id,vehicle_type,wkly_counter
FROM(
	SELECT b2b.b2b_avail_tbl.*,shop_name,mec_id,address4,address5,axle_id,b2b_avail AS STATUS,lat,lng,shop_id,vehicle_type,wkly_counter
	FROM b2b.b2b_avail_tbl
	INNER JOIN (
		SELECT MAX(b2b.b2b_mod_log) AS dates,m.shop_name, b2b_avail AS STATUS,b2b_shop_id AS shop_id,m.wkly_counter,m.type AS vehicle_type,m.mec_id,m.address4,m.address5,b2b_reason,m.axle_id,m.lat,m.lng
		FROM b2b.b2b_avail_tbl b2b
		INNER JOIN admin_mechanic_table m ON m.axle_id=b2b.b2b_shop_id
		LEFT JOIN admin_service_type AS st ON m.mec_id=st.mec_id
		WHERE m.status='0' AND m.type!='' AND m.type!='tyres' AND m.address5='$city' $condLocality $cond AND DATE(b2b.b2b_mod_log) BETWEEN '$start_date' AND '$end_date'
		GROUP BY DATE(b2b.b2b_mod_log),m.mec_id
	) AS tb ON tb.dates=b2b_avail_tbl.b2b_mod_log AND tb.axle_id=b2b_avail_tbl.b2b_shop_id
) AS tb1
WHERE STATUS='0'
GROUP BY mec_id)
ORDER BY b2b_shop_name) AS tbn 
LEFT JOIN b2b.b2b_credits_tbl AS bc ON tbn.shop_id=bc.b2b_shop_id
WHERE bc.b2b_leads >= 1 OR bc.b2b_credits > 2");

        // $tableView = view('shopAvailabilityTableView', compact('availableShops'))->render();
        // return ['success' => true, 'availableShops' => $availableShops, 'tableData' => $tableView];
        // exit;

        // DB::enableQueryLog();
        $availableShops = DB::table('b2b.b2b_mec_tbl as bm')
            ->whereDate('ba.b2b_mod_log', $start_date)
            ->where('bm.b2b_address5', $request->get('city'))
            ->where(function ($query) use ($request, $servicetype) {
                if ($request->get('locality') != 'all') {
                    $query->where('bm.b2b_address4', $request->get('locality'));
                }
                if ($request->get('vehicle_type') != 'all') {
                    $query->where('bm.b2b_vehicle_type', $request->get('vehicle_type'));
                }
                if ($request->get('service_type') != 'all') {
                    if (!empty($servicetype)) {
                        $query->where('gast.' . $servicetype, 1);
                    }
                }
            })
            ->where(function ($query) use ($request) {
                $query->where('bc.b2b_leads', '>=', 1);
                $query->orWhere('bc.b2b_credits', '>', 2);
            })
            ->leftJoin('b2b.b2b_credits_tbl as bc', 'bm.b2b_shop_id', '=', 'bc.b2b_shop_id')
            ->leftJoin('go_bumpr.admin_mechanic_table as gam', 'bm.gobumpr_id', '=', 'gam.mec_id')
            ->leftJoin('go_bumpr.admin_service_type as gast', 'gam.mec_id', '=', 'gast.mec_id')
            ->leftJoin('b2b.b2b_avail_tbl as ba', 'bm.b2b_shop_id', '=', 'ba.b2b_shop_id')
            ->select('bm.b2b_shop_id', 'bm.b2b_lat', 'bm.b2b_lng', 'bm.b2b_vehicle_type', 'bm.b2b_shop_name', 'bm.b2b_address4')
            ->addSelect('bc.b2b_credits', 'bc.b2b_leads', 'bc.b2b_re_leads', 'bc.b2b_non_re_leads')
            ->addSelect('gam.mec_id', 'gam.wkly_counter')
            ->addSelect(DB::raw('
                CASE WHEN bm.b2b_avail = 1 
                    THEN 
	                    CASE WHEN bc.b2b_credits > 2 OR bc.b2b_leads >= 1
	                        THEN "green" 
	                        ELSE "red" 
	                    END
                    ELSE 
                        CASE WHEN bc.b2b_credits > 2 OR bc.b2b_leads >= 1
	                        THEN "yellow" 
	                    END
                END AS marker
            '))
            ->groupBy('gam.mec_id')
            ->get();
        // dd(DB::getQueryLog());
        $tableView = view('shopAvailabilityTableView', compact('availableShops'))->render();

        return ['success' => true, 'availableShops' => $availableShops->toArray(), 'tableData' => $tableView];
    }

    public function getShopDetails(Request $request)
    {
        $shopDetails = DB::table('b2b.b2b_mec_tbl as bm')
            ->where('bm.b2b_shop_id', $request->get('shop_id'))
            // ->where('bm.b2b_shop_id', '1010')
            ->leftJoin('b2b.b2b_credits_tbl as bc', 'bm.b2b_shop_id', '=', 'bc.b2b_shop_id')
            ->leftJoin('go_bumpr.admin_mechanic_table as gam', 'bm.gobumpr_id', '=', 'gam.mec_id')
            ->select('bm.b2b_shop_id', 'bm.b2b_lat', 'bm.b2b_lng', 'bm.b2b_vehicle_type', 'bm.b2b_shop_name', 'bm.b2b_address4', 'bm.b2b_address3', 'bm.b2b_address5')
            ->addSelect('bc.b2b_credits', 'bc.b2b_leads', 'bc.b2b_re_leads', 'bc.b2b_non_re_leads')
            ->addSelect('gam.mec_id', 'gam.wkly_counter')
            ->first();

        $html = view('markerinfobox', compact('shopDetails'))->render();
        return ['success' => true, 'template' => $html];
    }
}

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('partials.header-scripts')
</head>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header closed-sidebar">
    @if (Auth::check())
        @include('partials.header')
        <div class="app-main">
            @include('partials.nav')

            @yield('content')
        </div>
</div>
{{--@include('partials.footer')--}}
@include('partials.footer-scripts')
@else
@yield('content')
@include('partials.footer-scripts')
@endif

</body>
</html>

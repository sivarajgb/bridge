<!-- Bootstrap core JavaScript
================================================= -->

@section('footerscripts')
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-2.2.4.js"
            integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
    <script src="{{asset('public/assets/libs/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{asset('public/assets/scripts/main.js')}}"></script>
    <script src="{{asset('public/assets/libs/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('public/assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
    <script src="{{asset('public/assets/libs/blockui-master/jquery.blockUI.js')}}"></script>
    <script src="{{asset('public/assets/libs/datepicker-master/dist/datepicker.js')}}"></script>
    <script src="{{asset('public/assets/libs/daterangepicker-master/moment.min.js')}}"></script>
    <script src="{{asset('public/assets/libs/daterangepicker-master/daterangepicker.js')}}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBEFvPWpawLCHyQQQR_4Qx4kKeyojpDw7I&libraries=geometry&libraries=places"></script>
    <script src="{{asset('public/assets/scripts/infobox.js')}}"></script>
@show

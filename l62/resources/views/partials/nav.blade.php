<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                        data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar ps ps--active-y">
        <div class="app-sidebar__inner">
            @if(Session::get('crm_se') != '1')
                <ul class="vertical-nav-menu">
                    @if(Session::get('flag') == '1')
                        <li class="app-sidebar__heading"></li>
                        <li>
                            <a href="{{config('custom.p_url')}}/aleads.php?t={{base64_encode('l')}}"
                               class="{{ (request()->is('/aleads')) ? 'mm-active' : '' }}">
                                <i class="metismenu-icon fa fa-exclamation-circle"></i>
                                New Bookings
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/abookings.php?t={{base64_encode('b')}}">
                                <i class="metismenu-icon fa fa-check-circle"></i>
                                GoAxled
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/afollowups.php?t={{base64_encode('f')}}">
                                <i class="metismenu-icon fa fa-calendar"></i>
                                FollowUps
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/aothers.php?t={{base64_encode('o')}}">
                                <i class="metismenu-icon fa fa-tasks"></i>
                                Others
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/aaxle.php">
                                <i class="metismenu-icon fab fa-adn">
                                </i>Axle
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/overrides.php?t={{base64_encode('ov')}}">
                                <i class="metismenu-icon fa fa-exchange-alt">
                                </i>overrides
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/int_re_eng_panel.php">
                                <i class="metismenu-icon fa fa-shopping-basket">
                                </i>IRP
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/partner_usage.php">
                                <i class="metismenu-icon fa fa-chart-line">
                                </i>Partners
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/allocation_interface.php">
                                <i class="metismenu-icon fa fa-sliders-h">
                                </i>Allocation
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/conversion_panel.php">
                                <i class="metismenu-icon fa fa-chart-bar">
                                </i>Conversion
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/conversion_panel_feedback.php">
                                <i class="metismenu-icon fa fa-chart-bar">
                                </i>Conversion Feedback
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/conversion_panel_re_engagement.php">
                                <i class="metismenu-icon fa fa-chart-bar">
                                </i>Conversion Re_Engagement
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/sc_stats.php">
                                <i class="metismenu-icon fa fa-signal">
                                </i>SC-Stats
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/daily_reports_v2.php?t={{base64_encode('reports')}}">
                                <i class="metismenu-icon fa fa-list-alt">
                                </i>Reports {{ Session::get('crm_city')}}
                            </a>
                        </li>

                        @if(Session::get('super_flag') == '1')
                            <li>
                                <a href="{{config('custom.p_url')}}/credits_history.php">
                                    <i class="metismenu-icon fa fa-money-check">
                                    </i>CreditsHistory
                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/shop_list.php">
                                    <i class="metismenu-icon fa fa-list-alt">
                                    </i>ST-Toggle
                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/axle_sales_portal.php">
                                    <i class="metismenu-icon fa fa-check-circle">
                                    </i>AxleSales
                                </a>
                            </li>
                        @endif

                        @if(Session::get('feedback_admin') == '1')
                            <li>
                                <a href="{{config('custom.p_url')}}/afdbkhome.php">
                                    <i class="metismenu-icon fa fa-comments">
                                    </i>FeedBack
                                </a>
                            </li>
                        @elseif(Session::get('crm_feedback') == '1')
                            <li>
                                <a href="{{config('custom.p_url')}}/fdbkhome.php">
                                    <i class="metismenu-icon fa fa-comments">
                                    </i>FeedBack
                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/axle.php">
                                    <i class="metismenu-icon fa fa-check-circle">
                                    </i>Axle
                                </a>
                            </li>
                        @endif

                    @elseif(Session::get('crm_cluster') == '1')

                        <li>
                            <a href="{{config('custom.p_url')}}/mleads.php?t={{base64_encode('l')}}">
                                <i class="metismenu-icon fa fa-exclamation-circle">
                                </i>NewBookings
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/mfollowups.php?t={{base64_encode('f')}}">
                                <i class="metismenu-icon fa fa-calendar-alt">
                                </i>FollowUps
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/mothers.php?t={{base64_encode('o')}}">
                                <i class="metismenu-icon fa fa-tasks">
                                </i>Others
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/int_re_eng_panel.php">
                                <i class="metismenu-icon fa fa-shopping-basket">
                                </i>IRP
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/stats_master_service.php">
                                <i class="metismenu-icon fa fa-compass">
                                </i>Service-Stats
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/allocation_interface.php">
                                <i class="metismenu-icon fa fa-sliders-h"></i>
                                Allocation
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/conversion_panel.php">
                                <i class="metismenu-icon fa fa-chart-bar">
                                </i>Conversion
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/conversion_panel_feedback.php">
                                <i class="metismenu-icon fa fa-chart-bar">
                                </i>Conversion Feedback
                            </a>
                        </li>

                        @if(Session::get('super_flag') == '1')
                            <li>
                                <a href="{{config('custom.p_url')}}/credits_history.php">
                                    <i class="metismenu-icon fa fa-money-check">
                                    </i>CreditsHistory
                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/shop_list.php">
                                    <i class="metismenu-icon fa fa-list-alt">
                                    </i>ST-Toggle
                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/axle_sales_portal.php">
                                    <i class="metismenu-icon fa fa-check-circle-o">
                                    </i>AxleSales
                                </a>
                            </li>
                        @endif

                        @if(Session::get('feedback_admin') == '1')
                            <li>
                                <a href="{{config('custom.p_url')}}/afdbkhome.php">
                                    <i class="metismenu-icon fa fa-comments">
                                    </i>FeedBack
                                </a>
                            </li>
                        @elseif(Session::get('crm_feedback') == '1')
                            <li>
                                <a href="{{config('custom.p_url')}}/fdbkhome.php">
                                    <i class="metismenu-icon fa fa-comments">
                                    </i>FeedBack
                                </a>
                            </li>
                        @endif

                    @else
                        <li>
                            <a href="{{config('custom.p_url')}}/leads.php?t={{base64_encode('l')}}">
                                <i class="metismenu-icon fa fa-exclamation-circle">
                                </i>NewBookings
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/followups.php?t={{base64_encode('f')}}">
                                <i class="metismenu-icon fa fa-calendar-alt">
                                </i>FollowUps
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/others.php?t={{base64_encode('o')}}">
                                <i class="metismenu-icon fa fa-tasks">
                                </i>Others
                            </a>
                        </li>

                        <li>
                            <a href="{{config('custom.p_url')}}/int_re_eng_panel.php">
                                <i class="metismenu-icon fa fa-shopping-basket">
                                </i>IRP
                            </a>
                        </li>

                        @if(Session::get('feedback_admin') == '1')

                            <li>
                                <a href="{{config('custom.p_url')}}/afdbkhome.php">
                                    <i class="metismenu-icon fa fa-comments">
                                    </i>FeedBack
                                </a>
                            </li>

                        @elseif(Session::get('crm_feedback') == '1')
                            <li>
                                <a href="{{config('custom.p_url')}}/fdbkhome.php">
                                    <i class="metismenu-icon fa fa-comments">
                                    </i>FeedBack
                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/axle.php">
                                    <i class="metismenu-icon fa fa-exclamation-circle">
                                    </i>Axle
                                </a>
                            </li>
                        @endif

                    @endif

                    <li>
                        <a href="{{config('custom.p_url')}}/heatmap.php">
                            <i class="metismenu-icon fa fa-map">
                            </i>HeatMap
                        </a>
                    </li>

                    <li>
                        <a href="{{config('custom.p_url')}}/stats_master_service.php">
                            <i class="metismenu-icon fa fa-compass">
                            </i>Service-Stats
                        </a>
                    </li>

                    <li>
                        <a href="{{config('custom.p_url')}}/details_update.php">
                            <i class="metismenu-icon fa fa-smile">
                            </i>GoFinder
                        </a>
                    </li>

                    <li>
                        <a href="{{config('custom.p_url')}}/logout.php">
                            <i class="metismenu-icon fa fa-sign-out-alt">
                            </i>LogOut
                        </a>
                    </li>
                </ul>
            @endif

            {{--Header Bar--}}
            @if(Session::get('crm_se') == '1')
                <ul class="vertical-nav-menu">
                    <li class="app-sidebar__heading"></li>
                    <?php
                    // $type = base64_decode($_GET['t']);
                    $type = '';
                    ?>
                    @if ($type != "eb")
                        @if ($type != "back")
                            @if((Session::get('cluster_admin') == '1') && (Session::get('crm_se') != '1'))

                                <li>
                                    <a href="{{config('custom.p_url')}}/mhome.php" class="mm-active">
                                        <i class="metismenu-icon fa fa-home"></i>
                                        Home
                                    </a>
                                </li>

                            @elseif((Session::get('flag') == '1') && (Session::get('crm_se') != '1'))

                                <li>
                                    <a href="{{config('custom.p_url')}}/ahome.php">
                                        <i class="metismenu-icon fa fa-home"></i>
                                        Home
                                    </a>
                                </li>

                            @elseif((Session::get('feedback_admin') == '1') && (Session::get('crm_feedback') == '1')  && (Session::get('crm_se') != '1'))

                                <li>
                                    <a href="{{config('custom.p_url')}}/afdbkhome.php">
                                        <i class="metismenu-icon fa fa-comments"></i>
                                        Home
                                    </a>
                                </li>

                            @elseif((Session::get('crm_feedback') == '1'))

                                <li>
                                    <a href="{{config('custom.p_url')}}/fdbkhome.php">
                                        <i class="metismenu-icon fa fa-comments"></i>
                                        Home
                                    </a>
                                </li>

                            @else
                                @if(Session::get('crm_se') != '1')

                                    <li>
                                        <a href="{{config('custom.p_url')}}/home.php">
                                            <i class="metismenu-icon fa fa-home"></i>
                                            Home
                                        </a>
                                    </li>

                                @endif
                            @endif
                        @endif

                        @switch ($type)
                            @case("l")
                            @if (isset($_GET['tt']))
                                <li>
                                    <i class="fa fa-ban" aria-hidden="true"></i>&nbsp;&nbsp;UnWanted
                                </li>
                            @else
                                <li>
                                    <i class="fa fa-exclamation-circle"
                                       aria-hidden="true"></i>&nbsp;&nbsp;New Bookings
                                </li>
                            @endif
                            @break

                            @case("b")
                            <li>
                                <i class="fa fa-check-circle"
                                   aria-hidden="true"></i>&nbsp;&nbsp;GoAxled Bookings
                            </li>
                            @break

                            @case("f")
                            <li>
                                <i class="fa fa-calendar"
                                   aria-hidden="true"></i>&nbsp;&nbsp;FollowUps
                            </li>
                            @break

                            @case("o")
                            <li>
                                <i class="fa fa-tasks" aria-hidden="true"></i>&nbsp;&nbsp;Others
                            </li>
                            @break

                            @case("c")
                            <li>
                                <i class="fa fa-times-circle"
                                   aria-hidden="true"></i>&nbsp;&nbsp;Cancelled
                            </li>
                            @break

                            @case("reports")
                            <li>
                                <i class="fa fa-list-alt"
                                   aria-hidden="true"></i>&nbsp;&nbsp;Reports
                            </li>
                            @break

                            @case("feedback")
                            <li>
                                <i class="fa fa-comments"
                                   aria-hidden="true"></i>
                                FeedBack
                            </li>
                            @break

                            @case("unwanted")
                            <li>
                                <i class="fa fa-ban" aria-hidden="true"></i>&nbsp;&nbsp;UnWanted
                            </li>
                            @break

                            @case("incomplete")
                            <li>
                                <i class="fa fa-question-circle"
                                   aria-hidden="true"></i>&nbsp;&nbsp;InComplete
                            </li>
                            @break

                            @case("overall")
                            <li>
                                <i class="fa fa-globe" aria-hidden="true"></i>&nbsp;&nbsp;Overall
                            </li>
                            @break

                            @case("irp")
                            <li>
                                <i class="fa fa-shopping-basket"
                                   aria-hidden="true"></i>&nbsp;&nbsp;IRP
                            </li>
                            @break

                            @case("eb")
                            <li>
                                <a href="{{config('custom.p_url')}}/external_bookings_history.php?t={{base64_encode('back')}}">
                                    <i class="metismenu-icon fa fa-history"></i>
                                    History
                                </a>
                            </li>
                            @break

                            @case("back")
                            <li>
                                <a href="{{config('custom.p_url')}}/external-bookings.php?t={{base64_encode('eb')}}">
                                    <i class="metismenu-icon fa fa-arrow-circle-left"></i>
                                    History
                                </a>
                            </li>
                            @break

                            @case("ov")
                            <li>
                                <i class="fa fa-exchange" aria-hidden="true"></i>&nbsp;&nbsp;Overrides
                            </li>
                            @break
                        @endswitch
                    @else

                        @if((Session::get('cluster_admin') == '1') && (Session::get('crm_se') != '1'))
                            <li>
                                <a href="{{config('custom.p_url')}}/mhome.php">
                                    <i class="metismenu-icon fa fa-home"></i>
                                    Home
                                </a>
                            </li>

                        @elseif((Session::get('flag') == '1') && (Session::get('crm_se') != '1'))
                            <li>
                                <a href="{{config('custom.p_url')}}/ahome.php">
                                    <i class="metismenu-icon fa fa-home"></i>
                                    Home
                                </a>
                            </li>

                        @elseif((Session::get('feedback_admin') == '1') && (Session::get('crm_se') != '1'))
                            <li>
                                <a href="{{config('custom.p_url')}}/afdbkhome.php">
                                    <i class="metismenu-icon fa fa-comments"></i>
                                    Home
                                </a>
                            </li>

                        @elseif((Session::get('crm_feedback') == '1') && (Session::get('crm_se') != '1'))
                            <li>
                                <a href="{{config('custom.p_url')}}/fdbkhome.php">
                                    <i class="metismenu-icon fa fa-comments"></i>
                                    Home
                                </a>
                            </li>
                        @else
                            @if(Session::get('crm_se') != '1')
                                <li>
                                    <a href="{{config('custom.p_url')}}/home.php">
                                        <i class="metismenu-icon fa fa-home"></i>
                                        Home
                                    </a>
                                </li>
                            @endif
                        @endif

                        <li>
                            <a href="{{config('custom.p_url')}}/external_bookings_history.php?t={{base64_encode('back')}}">
                                <i class="metismenu-icon fa fa-history"></i>
                                History
                            </a>
                        </li>
                    @endif

                    <ul class="nav navbar-nav navbar-right">
                        <li style=" margin-top:11px; font-size:20px; ">
                        @if(Session::get('crm_se') == '1')
                            <?php $_SESSION['crm_se'] != "1" ?>
                            <li>
                                <a href="{{config('custom.p_url')}}/sc_stats.php">
                                    <i class="metismenu-icon fa fa-signal"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/afdbkhome.php">
                                    <i class="metismenu-icon fa fa-comments"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/ticket_tracker.php">
                                    <i class="metismenu-icon fa fa-ticket"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/lead_packages_test.php">
                                    <i class="metismenu-icon fa-sort-amount-down"></i>

                                </a>
                            </li>
                        @endif

                        @if(Session::get('crm_log_id') == 'crm015')
                            <li>
                                <a href="{{config('custom.p_url')}}/ebm.php">
                                    <i class="metismenu-icon fa-exclamation-triangle"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_availability.php">
                                    <i class="metismenu-icon fa-power-off"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/abookings.php">
                                    <i class="metismenu-icon fa fa-check-circle"></i>
                                </a>
                            </li>
                        @endif

                        @if(Session::get('crm_log_id') == 'crm024')
                            <li>
                                <a href="{{config('custom.p_url')}}/aaxle.php">
                                    <i class="metismenu-icon fa-adn"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/credits_history.php">
                                    <i class="metismenu-icon fa-history"></i>

                                </a>
                            </li>
                        @endif

                        @if(Session::get('crm_log_id') == 'crm224')
                            <li>
                                <a href="{{config('custom.p_url')}}/aaxle.php">
                                    <i class="metismenu-icon fa-adn"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/credits_history.php">
                                    <i class="metismenu-icon fa-history"></i>

                                </a>
                            </li>

                        @endif

                        @if(Session::get('crm_log_id') == 'crm093')
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_availability.php">
                                    <i class="metismenu-icon fa fa-power-off"></i>

                                </a>
                            </li>
                        @endif

                        @if((Session::get('crm_log_id') == 'crm135') || (Session::get('crm_log_id') == 'crm232') || (Session::get('crm_log_id') == 'crm237') || (Session::get('crm_log_id') == 'crm239') || (Session::get('crm_log_id') == 'crm240'))

                            <li>
                                <a href="{{config('custom.p_url')}}/ebm.php">
                                    <i class="metismenu-icon fa fa-exclamation-triangle"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/shop_availability.php">
                                    <i class="metismenu-icon fa fa-power-off"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/abookings.php">
                                    <i class="metismenu-icon fa fa-check-circle"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/aleads.php">
                                    <i class="metismenu-icon fa fa-exclamation-circle"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/credits_history.php">
                                    <i class="metismenu-icon fa fa-history"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/shop_list.php">
                                    <i class="metismenu-icon fa fa-toggle-on"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/sc_stats.php">
                                    <i class="metismenu-icon fa fa-signal"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/lead_packages_test.php">
                                    <i class="metismenu-icon fa fa-sort-amount-down"></i>

                                </a>
                            </li>
                        @endif

                        @if(Session::get('crm_log_id') == 'crm144')
                            <li>
                                <a href="{{config('custom.p_url')}}/ebm.php">
                                    <i class="metismenu-icon fa fa-exclamation-triangle"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_availability.php">
                                    <i class="metismenu-icon fa fa-power-off"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/abookings.php">
                                    <i class="metismenu-icon fa fa-check-circle"></i>

                                </a>
                            </li>
                        @endif

                        @if(Session::get('crm_log_id') == 'crm145')
                            <li>
                                <a href="{{config('custom.p_url')}}/ebm.php">
                                    <i class="metismenu-icon fa fa-exclamation-triangle"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_availability.php">
                                    <i class="metismenu-icon fa fa-power-off"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/abookings.php">
                                    <i class="metismenu-icon fa fa-check-circle"></i>

                                </a>
                            </li>
                        @endif

                        @if(Session::get('crm_log_id') == 'crm148')
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_availability.php">
                                    <i class="metismenu-icon fa fa-power-off"></i>

                                </a>
                            </li>
                        @endif

                        @if(Session::get('crm_log_id') == 'crm161')
                            <li>
                                <a href="{{config('custom.p_url')}}/ebm.php">
                                    <i class="metismenu-icon fa fa-exclamation-triangle"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_availability.php">
                                    <i class="metismenu-icon fa fa-power-off"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/abookings.php">
                                    <i class="metismenu-icon fa fa-check-circle"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/aleads.php">
                                    <i class="metismenu-icon fa fa-exclamation-circle"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/aaxle.php">
                                    <i class="metismenu-icon fa fa-adn"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/credits_history.php">
                                    <i class="metismenu-icon fa fa-history"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_list.php">
                                    <i class="metismenu-icon fa fa-toggle-on"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/sc_stats.php">
                                    <i class="metismenu-icon fa fa-signal"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/lead_packages_test.php">
                                    <i class="metismenu-icon fa fa-sort-amount-down"></i>

                                </a>
                            </li>
                        @endif

                        @if(Session::get('crm_log_id') == 'crm202')
                            <li>
                                <a href="{{config('custom.p_url')}}/ebm.php">
                                    <i class="metismenu-icon fa fa-exclamation-triangle"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_availability.php">
                                    <i class="metismenu-icon fa fa-power-off"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/abookings.php">
                                    <i class="metismenu-icon fa fa-check-circle"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/aleads.php">
                                    <i class="metismenu-icon fa fa-exclamation-circle"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/aaxle.php">
                                    <i class="metismenu-icon fa fa-adn"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/credits_history.php">
                                    <i class="metismenu-icon fa fa-history"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_list.php">
                                    <i class="metismenu-icon fa fa-toggle-on"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/sc_stats.php">
                                    <i class="metismenu-icon fa fa-signal"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/lead_packages_test.php">
                                    <i class="metismenu-icon fa fa-sort-amount-down"></i>

                                </a>
                            </li>
                        @endif

                        @if(Session::get('crm_log_id') == 'crm162')
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_availability.php">
                                    <i class="metismenu-icon fa fa-power-off"></i>

                                </a>
                            </li>
                        @endif

                        @if(Session::get('crm_log_id') == 'crm163')
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_availability.php">
                                    <i class="metismenu-icon fa fa-power-off"></i>

                                </a>
                            </li>
                        @endif

                        @if(Session::get('crm_log_id') == 'crm169')
                            <li>
                                <a href="{{config('custom.p_url')}}/ebm.php">
                                    <i class="metismenu-icon fa fa-exclamation-triangle"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_availability.php">
                                    <i class="metismenu-icon fa fa-power-off"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/abookings.php">
                                    <i class="metismenu-icon fa fa-check-circle"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/sc_stats.php">
                                    <i class="metismenu-icon fa fa-signal"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/credits_history.php">
                                    <i class="metismenu-icon fa fa-history"></i>

                                </a>
                            </li>
                        @endif

                        @if(Session::get('crm_log_id') == 'crm174')
                            <li>
                                <a href="{{config('custom.p_url')}}/ebm.php">
                                    <i class="metismenu-icon fa fa-exclamation-triangle"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_availability.php">
                                    <i class="metismenu-icon fa fa-power-off"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/abookings.php">
                                    <i class="metismenu-icon fa fa-check-circle"></i>

                                </a>
                            </li>
                        @endif

                        @if(Session::get('crm_log_id') == 'crm178')
                            <li>
                                <a href="{{config('custom.p_url')}}/ebm.php">
                                    <i class="metismenu-icon fa fa-exclamation-triangle"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_availability.php">
                                    <i class="metismenu-icon fa fa-power-off"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/abookings.php">
                                    <i class="metismenu-icon fa fa-check-circle"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/aleads.php">
                                    <i class="metismenu-icon fa fa-exclamation-circle"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/aaxle.php">
                                    <i class="metismenu-icon fa fa-adn"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/credits_history.php">
                                    <i class="metismenu-icon fa fa-history"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_list.php">
                                    <i class="metismenu-icon fa fa-toggle-on"></i>

                                </a>
                            </li>
                        @endif

                        @if(Session::get('crm_log_id') == 'crm187')
                            <li>
                                <a href="{{config('custom.p_url')}}/ebm.php">
                                    <i class="metismenu-icon fa fa-exclamation-triangle"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_availability.php">
                                    <i class="metismenu-icon fa fa-power-off"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/abookings.php">
                                    <i class="metismenu-icon fa fa-check-circle"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/aleads.php">
                                    <i class="metismenu-icon fa fa-exclamation-circle"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/aaxle.php">
                                    <i class="metismenu-icon fa fa-adn"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/credits_history.php">
                                    <i class="metismenu-icon fa fa-history"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_list.php">
                                    <i class="metismenu-icon fa fa-toggle-on"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/sc_stats.php">
                                    <i class="metismenu-icon fa fa-signal"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/lead_packages_test.php">
                                    <i class="metismenu-icon fa fa-sort-amount-down"></i>

                                </a>
                            </li>
                        @endif

                        @if(Session::get('crm_log_id') == 'crm189')
                            <li>
                                <a href="{{config('custom.p_url')}}/ebm.php">
                                    <i class="metismenu-icon fa fa-exclamation-triangle"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_availability.php">
                                    <i class="metismenu-icon fa fa-power-off"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/abookings.php">
                                    <i class="metismenu-icon fa fa-check-circle"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/aleads.php">
                                    <i class="metismenu-icon fa fa-exclamation-circle"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/aaxle.php">
                                    <i class="metismenu-icon fa fa-adn"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/credits_history.php">
                                    <i class="metismenu-icon fa fa-history"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_list.php">
                                    <i class="metismenu-icon fa fa-toggle-on"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/sc_stats.php">
                                    <i class="metismenu-icon fa fa-signal"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/lead_packages_test.php">
                                    <i class="metismenu-icon fa fa-sort-amount-down"></i>

                                </a>
                            </li>
                        @endif

                        @if(Session::get('crm_log_id') == 'crm198')
                            <li>
                                <a href="{{config('custom.p_url')}}/ebm.php">
                                    <i class="metismenu-icon fa fa-exclamation-triangle"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_availability.php">
                                    <i class="metismenu-icon fa fa-power-off"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/abookings.php">
                                    <i class="metismenu-icon fa fa-check-circle"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/aleads.php">
                                    <i class="metismenu-icon fa fa-exclamation-circle"></i>

                                </a>
                            </li>

                            <li>
                                <a href="{{config('custom.p_url')}}/aaxle.php">
                                    <i class="metismenu-icon fa fa-adn"></i>

                                </a>
                            </li>
                        @endif

                        @if(Session::get('flag') == '1')
                            <li>
                                <a href="{{config('custom.p_url')}}/ebm.php">
                                    <i class="metismenu-icon fa fa-exclamation-triangle"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/shop_availability.php">
                                    <i class="metismenu-icon fa fa-power-off"></i>

                                </a>
                            </li>
                            <li>
                                <a href="{{config('custom.p_url')}}/lead_packages_test.php">
                                    <i class="metismenu-icon fa fa-sort-amount-desc"></i>

                                </a>
                            </li>
                        @endif

                        <li>
                            <a href="{{config('custom.p_url')}}/premium_branding.php">
                                <i class="metismenu-icon fa fa-grav"></i>

                            </a>
                        </li>

                        @if ($type != "eb")
                            @if($type != "back")
                                @if($crm_flag ?? '' != 1)
                                    <li>
                                        <a href="{{config('custom.p_url')}}/daily_reports_v2.php">
                                            <i class="metismenu-icon fa fa-list-alt"></i>

                                        </a>
                                    </li>
                                @endif
                            @endif
                        @endif

                        @if($type == "eb" || $type == "back")
                            <li>
                                <a href="{{config('custom.p_url')}}/logout.php">
                                    <i class="metismenu-icon fa fa-sign-out"></i>
                                    LogOut
                                </a>
                            </li>
                        @endif
                    </ul>
                </ul>
            @endif
        </div>
    </div>
</div>

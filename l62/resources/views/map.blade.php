@extends('layouts.mainlayout')

@section('content')
    <div class="app-main__outer">
        <div class="app-main__inner">
            {{--Search Form--}}
            <div class="main-card mb-2 card">
                <div class="card-body pb-0">
                    <form id="search_form" class="needs-validation" novalidate>
                        <div class="d-flex form-row">
                            {{--Date Range--}}
                            <div class="col-md-2">
                                <div class="position-relative form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend datepicker-trigger">
                                            <div class="input-group-text">
                                                <i class="fa fa-calendar-alt"></i>
                                            </div>
                                        </div>
                                        <input type="text" class="form-control" id="date_range" name="date_range"
                                               value="" readonly/>
                                    </div>
                                </div>
                            </div>

                            {{--City--}}
                            <div class="col-md-2">
                                <div class="position-relative form-group">
                                    <select name="city" id="city" data-placeholder="Select City" class="form-control"
                                            required>

                                    </select>
                                </div>
                            </div>

                            {{--Locality--}}
                            <div class="col-md-3">
                                <div class="position-relative form-group">
                                    <select name="locality" id="locality" data-placeholder="Select Locality"
                                            class="form-control" required>
                                        <option value="all">ALL</option>
                                    </select>
                                </div>
                            </div>

                            {{--Vehicle Type--}}
                            <div class="col-md-2">
                                <div class="position-relative form-group">
                                    <select name="vehicle_type" id="vehicle_type" data-placeholder="Select Vehicle Type"
                                            class="form-control" required>
                                        <option value="all">All Vehicles</option>
                                        <option value="2w">2 wheeler</option>
                                        <option value="4w">4 Wheeler</option>
                                    </select>
                                </div>
                            </div>

                            {{--Service Type--}}
                            <div class="col-md-3">
                                <div class="position-relative form-group">
                                    <select name="service_type" id="service_type" data-placeholder="Select Service"
                                            class="form-control" required>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            {{--Tab List--}}
            <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
                <li class="nav-item">
                    <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0">
                        <span>Map View</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1">
                        <span>Table View</span>
                    </a>
                </li>
            </ul>

            {{--Tab Contents--}}
            <div class="tab-content">
                <div class="tab-pane tabs-animation fade show active" style="position: relative;" id="tab-content-0"
                     role="tabpanel">
                    <div class="mb-3" style="position: absolute;top:-38px; right: 0;">
                        <span>Instructions & Filter :</span>
                        <span class="mr-3 pointer" onclick="filterMarkers('green')">
                            <img src="http://maps.google.com/mapfiles/ms/micons/green.png"
                                 style="height: 20px;">Available
                            <span class="greenCount"></span>
                        </span>
                        <span class="mr-3 pointer" onclick="filterMarkers('yellow')">
                            <img src="http://maps.google.com/mapfiles/ms/micons/yellow.png"
                                 style="height: 20px;">Not Available
                        <span class="yellowCount"></span>
                        </span>
                        <span class="mr-3 pointer" onclick="filterMarkers('red')">
                            <img src="http://maps.google.com/mapfiles/ms/micons/red.png"
                                 style="height: 20px;">Available, But No credits
                            <span class="redCount"></span>
                        </span>
                        <span class="mr-3 pointer" onclick="filterMarkers('all')">
                            <img src="http://maps.google.com/mapfiles/ms/micons/info.png"
                                 style="height: 20px;">All
                            <span class="allCount"></span>
                        </span>
                    </div>

                    <div id="mapView" class="w-100"
                         style="min-height: 300px;border: 2px solid rgb(211, 211, 211);box-shadow: rgba(211, 211, 211, 0.05) 1px 1px 0px 1px;">
                        <div class="mapPlaceholder">
                            <span class="fa fa-spin fa-spinner"></span> Loading map...
                        </div>
                    </div>
                </div>

                <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel">
                    <div class="row">
                        <div class="col-12">
                            <div class="main-card mb-3 card">
                                <div class="card-body">
                                    {{--Filter Table--}}
                                    <div class="row">
                                        <div class="col-12">
                                            <form class="form-row">
                                                <div class="col-md-2 mb-2 position-relative form-group">
                                                    {{--                                                    <label for="vehicle_type" class="mr-sm-2">FILTER : </label>--}}
                                                    <select name="table_filter" id="table_filter"
                                                            data-placeholder="Select Filter"
                                                            class="form-control" required>
                                                        <option value="all">ALL</option>
                                                        <option value="green">Available</option>
                                                        <option value="red">No Credits</option>
                                                        <option value="yellow">Not Available</option>
                                                    </select>
                                                </div>
                                            </form>

                                            <div class="mb-3" style="position: absolute; right: 0; top: 10px;">
                                                <span>Instructions :</span>

                                                <span class="mr-3">
                                                    <span class="square-box bg-green"></span>Available
                                                    <span class="greenCount"></span>
                                                </span>
                                                <span class="mr-3">
                                                    <span class="square-box bg-yellow"></span>Not Available
                                                    <span class="yellowCount"></span>
                                                </span>
                                                <span class="mr-3">
                                                    <span class="square-box bg-red"></span>Available, But No credits
                                                    <span class="redCount"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="mb-0 table table-bordered" id="shopAvailabilityTableView">
                                            <thead>
                                            <tr>
                                                <th>Mec ID</th>
                                                <th>Shop Name</th>
                                                <th>Veh. Type</th>
                                                <th>Locality</th>
                                                <th>credits</th>
                                                <th>Leads</th>
                                                <th>Lead Cap</th>
                                                <th>Attendance</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footerscripts')
    @parent
    <script>
        var csrf_token = "{{ csrf_token() }}";
        var getCitiesUrl = "{{ route('getCities') }}";
        var getServiceTypesUrl = "{{ route('getServiceTypes') }}";
        var getLocalitiesByCityUrl = "{{ route('getLocalitiesByCity') }}";
        var getShopAvailabilityUrl = "{{ route('getShopAvailability') }}";
        var getShopDetailsUrl = "{{ route('getShopDetails') }}";
    </script>
    <script src="{{asset('public/assets/scripts/shopAvailabilityMap.js')}}"></script>
    <script src="{{asset('public/assets/scripts/shopAvailability.js')}}"></script>
@endsection

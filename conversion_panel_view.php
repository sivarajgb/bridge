<?php
ini_set("precision", 3);
include("../config.php");
include ("../DBController.php");

$conn = db_connect1();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$super_flag = $_SESSION['super_flag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$starttime = date('H:i:s',strtotime($_POST['starttime']));
$endtime =  date('H:i:s',strtotime($_POST['endtime']));
$city = $_POST['city'];
$target = $_POST['target'];


$_SESSION['crm_city'] = $city; 


$start = $startdate.' '.$starttime;
$end = $enddate.' '.$endtime;
?>
<?php

switch($target)
{
	case "#Service" :   
	$conn_db=new DB_connection();       						
	$vehicle_arr=array("all","2w","4w");
	$i=1;
	$res_sr_all="";
	foreach($vehicle_arr as $vehicle_type){
		//$end_conv=$flw_go=$end_conv_per=0;
					//$category="service";
					$res_sr_all = $conn_db->ConversionPanelReport($start,$end,$vehicle_type,$city,$target);

					$total_cancel=$total_lead=$total_followup=$total_goaxle=$total_others=$total_duplicate=$total_endconv=$total_followup_goaxle=$followup_goaxle=0;
					while($row_sr_all = mysqli_fetch_object($res_sr_all)){
						$end_conv=$flw_go=$end_conv_per=0;
						if($vehicle_type=="all"){
							$total_lead_count=$total_lead_count+$row_sr_all->Leads;
						}
						$total_goaxle=$total_goaxle+$row_sr_all->Goaxle;
						$total_lead=$total_lead+$row_sr_all->Leads;
						$total_followup=$total_followup+$row_sr_all->Followup;
						$total_others=$total_others+$row_sr_all->Others;
						$total_cancel=$total_cancel+$row_sr_all->cancel;
						$total_idle=$total_idle+$row_sr_all->Idle;
						$total_duplicate=$total_duplicate+$row_sr_all->duplicate;
						//$total_endconv=$total_endconv+$row_sr_all->EndConversion;
						//$total_followup_goaxle=$total_followup_goaxle+$row_sr_all->FollowupGoaxle;

						
						if(is_null($row_sr_all->EndConversion)){
							$end_conv=0;
						}else{
							$end_conv=$row_sr_all->EndConversion;	
						}

						if(is_null($row_sr_all->FollowupGoaxle)){
							$flw_go=0;
						}else{
							$flw_go=$row_sr_all->FollowupGoaxle;	
						}

						$total_endconv=$total_endconv+$end_conv;
						$total_followup_goaxle=$total_followup_goaxle+$flw_go;
						$end_conv_per=($row_sr_all->Goaxle)+($flw_go);
						
							//print_r($row_sr_all);
						$tr_sr.= "<tr>
						<td>".$row_sr_all->service_type."</td>
						<td style='text-align:center;font-size:13px;'>".$row_sr_all->vehicle_type."</td>
						<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$row_sr_all->service_type."' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalservice'>".$row_sr_all->Leads."</button></td>
						<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$row_sr_all->service_type."' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalservice'>".$row_sr_all->Goaxle."(".number_format((($row_sr_all->Goaxle/$row_sr_all->Leads)*100),2)."%)</button></td>
						<td style='text-align:center;font-size:13px;'>".$flw_go."(".number_format((($flw_go/$row_sr_all->Followup)*100),2)."%)</td>
						<td style='text-align:center;font-size:13px;'>".$end_conv."(".number_format((($end_conv/$end_conv_per)*100),2)."%)</td>
						<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$row_sr_all->service_type."' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalservice'>".$row_sr_all->Followup."(".number_format((($row_sr_all->Followup/$row_sr_all->Leads)*100),2)."%)</button></td>
						
						<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$row_sr_all->service_type."' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalservice'>".$row_sr_all->cancel."(".number_format((($row_sr_all->cancel/$row_sr_all->Leads)*100),2)."%)</button></td>
						<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$row_sr_all->service_type."' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalservice'>".$row_sr_all->Others."(".number_format((($row_sr_all->Others/$row_sr_all->Leads)*100),2)."%)</button></td>
						<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$row_sr_all->service_type."' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalservice'>".$row_sr_all->Idle."(".number_format((($row_sr_all->Idle/$row_sr_all->Leads)*100),2)."%)</button></td>
						
						
						<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$row_sr_all->service_type."' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalservice'>".$row_sr_all->duplicate."</button></td>

						</tr>";
					}
					$tr_sr.="<tr class='avoid-sortSer".$i."' style='display:none'>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);' colspan='2'>Total</td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalservice'>".$total_lead."</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalservice'>".$total_goaxle."(".number_format((($total_goaxle/$total_lead)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>".$total_followup_goaxle."(".number_format((($total_followup_goaxle/$total_followup)*100),2)."%)</td>

					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>".$total_endconv."(".number_format((($total_endconv/($total_goaxle+$total_followup_goaxle))*100),2)."%)</td>

					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalservice'>".$total_followup."(".number_format((($total_followup/$total_lead)*100),2)."%)</button></td>
					
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalservice'>".$total_cancel."(".number_format((($total_cancel/$total_lead)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalservice'>".$total_others."(".number_format((($total_others/$total_lead)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalservice'>".$total_idle."(".number_format((($total_idle/$total_lead)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalservice'>".$total_duplicate."</button></td>
					<tr>";
					$data[$i][]= array('tr'=>$tr_sr);
					$table="table".$i;
					$result[$table] = $data[$i];
					$i++;
	}
					$result['count'] = $total_lead_count;
					//print_r($result);
					echo json_encode($result);
					break;
	case "#Person" :
	$conn_db=new DB_connection();
	//$category="person";
	$city = $_POST['city'];
	$sql_per_res="";
	$sql_per_res=$conn_db->ConversionPanelReport($start,$end,$vehicle_type,$city,$target);

	$total_cancel=$total_lead=$total_followup=$total_goaxle=$total_others=$total_duplicate=$total_endconv=$total_followup_goaxle=0;

	while($row_pr_all=mysqli_fetch_object($sql_per_res)){
			//print_r($row_pr_all);
			$end_conv=$flw_go=$end_conv_per=0;
		$total_goaxle=$total_goaxle+$row_pr_all->Goaxle;
						$total_lead=$total_lead+$row_pr_all->Leads;
						$total_followup=$total_followup+$row_pr_all->Followup;
						$total_others=$total_others+$row_pr_all->Others;
						$total_cancel=$total_cancel+$row_pr_all->cancel;
						$total_idle=$total_idle+$row_pr_all->Idle;
						$total_duplicate=$total_duplicate+$row_pr_all->duplicate;
						// $total_endconv=$total_endconv+$row_pr_all->EndConversion;
						// $total_followup_goaxle=$total_followup_goaxle+$row_pr_all->FollowupGoaxle;
						if($row_pr_all->EndConversion==""){
							$end_conv=0;
						}else{
							$end_conv=$row_pr_all->EndConversion;	
						}

						if($row_pr_all->FollowupGoaxle==""){
							$flw_go=0;
						}else{
							$flw_go=$row_pr_all->FollowupGoaxle;	
						}

						$total_endconv=$total_endconv+$end_conv;
						$total_followup_goaxle=$total_followup_goaxle+$flw_go;
						$end_conv_per=($row_pr_all->Goaxle)+($flw_go);

						$tr_pr.="<tr>
							<td>".$row_pr_all->name."</td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$row_pr_all->crm_update_id."' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalperson'>".$row_pr_all->Leads."</button></td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$row_pr_all->crm_update_id."' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalperson'>".$row_pr_all->Goaxle." (".number_format((($row_pr_all->Goaxle/$row_pr_all->Leads)*100),2)."%)</button></td>
							<td style='text-align:center;font-size:13px;'>".$flw_go."(".number_format((($flw_go/$row_pr_all->Followup)*100),2)."%)</td>
							<td style='text-align:center;font-size:13px;'>".$end_conv."(".number_format((($end_conv/$end_conv_per)*100),2)."%)</td>

							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$row_pr_all->crm_update_id."' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalperson'>".$row_pr_all->Followup." (".number_format((($row_pr_all->Followup/$row_pr_all->Leads)*100),2)."%)</button></td>
							

							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$row_pr_all->crm_update_id."' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalperson'>".$row_pr_all->cancel." (".number_format((($row_pr_all->cancel/$row_pr_all->Leads)*100),2)."%)</button></td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$row_pr_all->crm_update_id."' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalperson'>".$row_pr_all->Others." (".number_format((($row_pr_all->Others/$row_pr_all->Leads)*100),2)."%)</button></td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$row_pr_all->crm_update_id."' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalperson'>".$row_pr_all->Idle." (".number_format((($row_pr_all->Idle/$row_pr_all->Leads)*100),2)."%)</button></td>
							
							
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$row_pr_all->crm_update_id."' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalperson'>".$row_pr_all->duplicate."</button></td>
							<tr>";
					}
					$tr_pr.="<tr class = 'avoid-sortperson' style='display:none'>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>Total</td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalperson'>".$total_lead."</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalperson'>".$total_goaxle."(".number_format((($total_goaxle/$total_lead)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>".$total_followup_goaxle."(".number_format((($total_followup_goaxle/$total_followup)*100),2)."%)</td>

					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>".$total_endconv."(".number_format((($total_endconv/($total_goaxle+$total_followup_goaxle))*100),2)."%)</td>

					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalperson'>".$total_followup."(".number_format((($total_followup/$total_lead)*100),2)."%)</button></td>
					
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalperson'>".$total_cancel."(".number_format((($total_cancel/$total_lead)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalperson'>".$total_others."(".number_format((($total_others/$total_lead)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalperson'>".$total_idle."(".number_format((($total_idle/$total_lead)*100),2)."%)</button></td>
					
					
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalperson'>".$total_duplicate."</button></td>
					<tr>";
					$person_data[] = array('tr'=>$tr_pr);
					$result['person_data'] = $person_data;
					echo json_encode($result);
					break; 
	case "#Source" :
	$conn_db=new DB_connection();
	$vehicle_arr=array("all","2w","4w");
	$i=1;
	$res_src_all="";
	foreach($vehicle_arr as $vehicle_type){
	//$category="source";
	 
					$res_src_all = $conn_db->ConversionPanelReport($start,$end,$vehicle_type,$city,$target);

					$total_cancel=$total_lead=$total_followup=$total_goaxle=$total_others=$total_duplicate=$total_endconv=$total_followup_goaxle=$followup_goaxle=0;
					while($row_src_all = mysqli_fetch_object($res_src_all)){
						$end_conv=$flw_go=$end_conv_per=0;
						if($vehicle_type=="all"){
							$total_lead_count=$total_lead_count+$row_sr_all->Lead;
						}
						$total_goaxle=$total_goaxle+$row_src_all->Goaxle;
						$total_lead=$total_lead+$row_src_all->Leads;
						$total_followup=$total_followup+$row_src_all->Followup;
						$total_others=$total_others+$row_src_all->Others;
						$total_cancel=$total_cancel+$row_src_all->cancel;
						$total_idle=$total_idle+$row_src_all->Idle;
						$total_duplicate=$total_duplicate+$row_src_all->duplicate;
						// $total_endconv=$total_endconv+$row_src_all->EndConversion;
						// $total_followup_goaxle=$total_followup_goaxle+$row_src_all->FollowupGoaxle;
						if($row_src_all->EndConversion==""){
							$end_conv=0;
						}else{
							$end_conv=$row_src_all->EndConversion;	
						}

						if($row_src_all->FollowupGoaxle==""){
							$flw_go=0;
						}else{
							$flw_go=$row_src_all->FollowupGoaxle;	
						}

						$total_endconv=$total_endconv+$end_conv;
						$total_followup_goaxle=$total_followup_goaxle+$flw_go;
						$end_conv_per=($row_src_all->Goaxle)+($flw_go);

							//print_r($row_src_all);
								$tr_src.= "<tr>
								<td>".$row_src_all->source."</td>
								<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='".$row_src_all->source."' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalsource'>".$row_src_all->Leads."</button></td>
								<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='".$row_src_all->source."' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalsource'>".$row_src_all->Goaxle."(".number_format((($row_src_all->Goaxle/$row_src_all->Leads)*100),2)."%)</button></td>
								<td style='text-align:center;font-size:13px;'>".$flw_go."(".number_format((($flw_go/$row_src_all->Followup)*100),2)."%)</td>
								<td style='text-align:center;font-size:13px;'>".$end_conv."(".number_format((($end_conv/$end_conv_per)*100),2)."%)</td>

								<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='".$row_src_all->source."' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalsource'>".$row_src_all->Followup."(".number_format((($row_src_all->Followup/$row_src_all->Leads)*100),2)."%)</button></td>
								
								<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='".$row_src_all->source."' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalsource'>".$row_src_all->cancel."(".number_format((($row_src_all->cancel/$row_src_all->Leads)*100),2)."%)</button></td>
								<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='".$row_src_all->source."' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalsource'>".$row_src_all->Others."(".number_format((($row_src_all->Others/$row_src_all->Leads)*100),2)."%)</button></td>
								<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='".$row_src_all->source."' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalsource'>".$row_src_all->Idle."(".number_format((($row_src_all->Idle/$row_src_all->Leads)*100),2)."%)</button></td>

								
								<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='".$row_src_all->source."' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalsource'>".$row_src_all->duplicate."</button></td>
							</tr>";
						}
						$tr_src.= "<tr class='avoid-sortsrc".$i."' style='display:none'>
						<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>Total</td>
						<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalsource'>".$total_lead."</button></td>
						<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalsource'>".$total_goaxle."(".number_format((($total_goaxle/$total_lead)*100),2)."%)</button></td>
						<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>".$total_followup_goaxle."(".number_format((($total_followup_goaxle/$total_followup)*100),2)."%)</td>
	
						<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>".$total_endconv."(".number_format((($total_endconv/($total_goaxle+$total_followup_goaxle))*100),2)."%)</td>
						<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalsource'>".$total_followup."(".number_format((($total_followup/$total_lead)*100),2)."%)</button></td>
						
						<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalsource'>".$total_cancel."(".number_format((($total_cancel/$total_lead)*100),2)."%)</button></td>
						<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalsource'>".$total_others."(".number_format((($total_others/$total_lead)*100),2)."%)</button></td>
						<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalsource'>".$total_idle."(".number_format((($total_idle/$total_lead)*100),2)."%)</button></td>
						
						
						<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalsource'>".$total_duplicate."</button></td>
						<tr>";

						$data[$i][]= array('tr'=>$tr_src);
					$table="src_table".$i;
					$result[$table] = $data[$i];
					$i++;
				}
						echo json_encode($result);
						
						break;
						
	case "#NonConversion" : 
	$cond_nc ='';
	$cond_nc = $cond_nc.($city == 'all' ? "" : "AND b.city='$city'");

	$sql_conv = "SELECT id,activity FROM admin_activity_tbl WHERE flag='1' ORDER BY activity ASC";
	$res_conv = mysqli_query($conn,$sql_conv) or die(mysqli_error($conn));
	
	while($row_conv = mysqli_fetch_object($res_conv)){
		$act_id = $row_conv->id;
		$activity = $row_conv->activity;
		
		
		$nc_cancelled_count_2w = 0 ;
		$nc_cancelled_count_4w = 0 ;

			$sql_get_type = $city == "Chennai" ? "SELECT b.booking_id,b.vehicle_type,b.flag_duplicate FROM user_booking_tb as b WHERE b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.mec_id NOT IN(400001,200018,200379,400974)and b.flag='1' and b.activity_status='$act_id' and b.log BETWEEN '$start' AND '$end' {$cond_nc}" : "SELECT b.vehicle_type,b.flag_duplicate FROM user_booking_tb as b WHERE b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.mec_id NOT IN(400001,200018,200379,400974)and b.flag='1' and b.activity_status='$act_id' and b.log  BETWEEN '$start' AND '$end' {$cond_nc}";
			
			$res_get_type = mysqli_query($conn,$sql_get_type);
			
			while($row_get_type = mysqli_fetch_object($res_get_type)){
			//print_r($row_get_type);
			$vehicle_type = $row_get_type->vehicle_type;
			$flag_duplicate = $row_get_type->flag_duplicate;
			$flag_cancel = $row_get_type->flag;

			if($act_id == '26')
			{
				if($vehicle_type == '2w'){
					$nc_cancelled_count_2w=$nc_cancelled_count_2w+1;
				}
				else if($vehicle_type == '4w'){
					$nc_cancelled_count_4w=$nc_cancelled_count_4w+1;				
				}
			}
			else
			{
				if($vehicle_type == '2w'){
					
					$nc_cancelled_count_2w=$nc_cancelled_count_2w+1;
				}
				else if($vehicle_type == '4w'){
					$nc_cancelled_count_4w=$nc_cancelled_count_4w+1;				
				}
			}
		}
	$tr2 = "<tr>";
	$td1 = "<td style='text-align:left;width:50%;word-break: break-word;'>".$activity."</td>";
	$td2 = "<td style='text-align:left;width:25%;word-break: break-word;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-activity='".$act_id."' data-startdate='".$start."' data-enddate='".$end."' data-target='#myModalnonconv'>".$nc_cancelled_count_2w."</button></td>";
	$td3 = "<td style='text-align:left;width:25%;word-break: break-word;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-activity='".$act_id."' data-startdate='".$start."' data-enddate='".$end."' data-target='#myModalnonconv'>".$nc_cancelled_count_4w."</button></td>";
	$tr2_l = "</tr>";

	$str1 = $tr2.$td1.$td2.$td3.$tr2_l;
	$cancel_data[]=$str1;
	//echo $str1;
	}						
						$sql_conv = "SELECT id,activity FROM admin_activity_tbl WHERE flag='2' ORDER BY activity ASC";
						$res_conv = mysqli_query($conn,$sql_conv) or die(mysqli_error($conn));
						while($row_conv = mysqli_fetch_object($res_conv)){
							$activity_id = $row_conv->id;
							$activity = $row_conv->activity;
							 
							$nc_others_count_2w = 0;
							$nc_others_count_4w = 0;  

								$sql_get_type = $city == "Chennai" ? "SELECT b.booking_id,b.vehicle_type FROM user_booking_tb as b WHERE b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.mec_id NOT IN(400001,200018,200379,400974) and b.activity_status='$activity_id' and b.log BETWEEN '$start' AND '$end' {$cond_nc}" : "SELECT b.vehicle_type,b.flag_duplicate FROM user_booking_tb as b WHERE b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.mec_id NOT IN(400001,200018,200379,400974)and b.activity_status='$activity_id' and b.log  BETWEEN '$start' AND '$end' {$cond_nc}";
								//$sql_get_type = $city == "all" ? "SELECT vehicle_type FROM user_booking_tb WHERE booking_id='$others_booking_id' AND mec_id NOT IN(400001,200018,200379,400974) " : "SELECT vehicle_type FROM user_booking_tb WHERE booking_id='$others_booking_id' AND city='$city' AND mec_id NOT IN(400001,200018,200379,400974) ";
								$res_get_type = mysqli_query($conn,$sql_get_type) or die($mysqli_error($conn));
								
								while($row_get_type = mysqli_fetch_object($res_get_type)){
								$vehicle_type = $row_get_type->vehicle_type;

								if($vehicle_type == '2w'){
									$nc_others_count_2w=$nc_others_count_2w+1;
								}
								else if($vehicle_type == '4w'){
									$nc_others_count_4w=$nc_others_count_4w+1;				
								}  
							}
							$tr4 = "<tr>";
							$td4 = "<td style='text-align:left;width:50%;word-break: break-word;'>".$activity."</td>";
							$td5 = "<td style='text-align:left;width:25%;word-break: break-word;'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='2w' data-activity='".$activity_id."' data-startdate='".$start."' data-enddate='".$end."' data-target='#myModalnonconv'>".$nc_others_count_2w."</button></td>";
							$td6 = "<td style='text-align:left;width:25%;word-break: break-word;'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='4w' data-activity='".$activity_id."' data-startdate='".$start."' data-enddate='".$end."' data-target='#myModalnonconv'>".$nc_others_count_4w."</button></td>";
							$tr4_l = "</tr>";
							
							$str2 = $tr4.$td4.$td5.$td6.$tr4_l;
							$other_data[]=$str2;
						}
						$nonconv_data['cancel'] = $cancel_data;
						$nonconv_data['other'] = $other_data;						
						echo json_encode($nonconv_data);
						break;
}

?>
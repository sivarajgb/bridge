<?php
include("sidebar.php");
$conn = db_connect2();
//echo "login or not";die;
if(empty($_SESSION['crm_log_id'])) {
	header('location:logout.php');
	die();
}

if ($_SESSION['flag'] !=1 && $_SESSION['crm_feedback'] !=1 && $_SESSION['crm_se'] !=1) {
	header('location:logout.php');
	die();
}

?>

<!DOCTYPE html>
<html>
<head>


<meta charset="utf-8">
  <title>GoBumpr Bridge</title>


  <style>
   /* axle page cursor */

#range > span:hover{cursor: pointer;}
 #sl{
	cursor:pointer;
}

.floating-box {
	display: inline-block;
 float: right;
 margin: 12px;
 padding: 12px;
 width:112px;
 height:63px;
 box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  font-size: 13px;
  cursor:pointer;
}
.floating-box1 {
    display: inline-block;
	margin:10px;
	float:left;
	clear:both;
 }
 .floating-box2 {
 	display: inline-block;
  float: left;
	margin: 12px;
  padding: 12px;
	width:412px;
  height:53px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
   font-size: 15px;
   cursor:pointer;
 }
.rad input {visibility: hidden; width: 0px; height: 0px;}
label{
		cursor:pointer;
	}
	/*auto complete */
@charset "utf-8";.ui-autocomplete{z-index:1000 !important;cursor:default;list-style:none;}
.ui-widget{}
.ui-autocomplete{overflow-y:auto;overflow-x:hidden;}
.ui-menu{width:0px;display:none;}
.ui-autocomplete > li{padding:10px;padding-left:10px;}
ul{margin-bottom:0;}
.ui-autocomplete > li.ui-state-focus{background-color:#DDD;}
.ui-autocomplete .ui-front .ui-menu .ui-widget .ui-widget-content{border:0;}
.ui-helper-hidden-accessible{display:none;}
.gobumpr-icon{font-style:normal;font-weight:normal;speak:none;display:inline-block;text-decoration:inherit;text-align:center;font-variant:normal;text-transform:none;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
.ui-widget{background-color:white;width:100%;}
.ui-widget-content{padding-left:1px;display:block;width:20px;position:relative;line-height:12px;max-height:210px;border:.5px solid #DADADA;}
.ui-widget{}
.ui-autocomplete { position: absolute; cursor: default;}

/* table */
thead:hover{
	cursor:pointer;
}

.results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px;
  color:#ccc;
}

input:checked + label {
      background: red;
  }

.btn{
    background:rgba(121, 189, 189, 0.95);;
}
.btn.active,.btn:active{
    background:#439898;
    color:#fff;
}
.btn.focus,.btn:focus{
    background:#439898;
    color:#fff;
}

span.make-switch.switch-radio {
    float: left;
}
.bootstrap-switch-container
{
	height:30px;
}

</style>

</head>
<body>
<?php include_once("header.php"); ?>
<!-- <script>
	$(document).ready(function(){
		$('#city').show();
	})
</script> -->
<div class="overlay" data-sidebar-overlay></div>

<div class="padding"></div>

		<!-- vehicle filter -->
		<div class=" col-xs-2" style="margin-top: 10px;float:left; margin-left:100px;">
		    <select id="veh_type" name="veh_type" class="form-control" style="width:69px;">
		      <option value="all" selected>All</option>
		      <option value="2w">2W</option>
		      <option value="4w">4W</option>
		    </select>
		</div>

		<!-- Search Bar -->
		<div class=" col-xs-4 form-group" style="margin-top: 10px;float:left; margin-left:40px;">
      	      <input type="text" class="searchs form-control" placeholder="Search" style="width:200px;">
		</div>
		
<!-- location filter
<div class=" col-xs-2  col-lg-offset-1" style="margin-top:10px;float:left;">
		<div class=" form-group" id="loc" style="width:125px;">
		        <div class="ui-widget">
		        	<input class="form-control autocomplete" id="location" type="text" name="location"  required placeholder=" Location">
		        </div>
		</div>
 </div>
 <div id="sl" class="form-group col-xs-1 col-lg-offset-1 col-xs-offset-0" style="margin-top:15px;margin-left:25px;font-size:16px;color:#95A186;">
 &nbsp;<i class="fa fa-search" aria-hidden="true"></i>
 </div> -->

 <!-- service types -->
<div class=" col-sm-3">
<div class="btn-group" data-toggle="buttons"  style="cursor: pointer; margin-top:10px; margin-left: 15px;">
<label class="btn btn-md">
        <input type="radio" name="master" id="master1" value="all"> ALL DAYS
      </label>
      <label class="btn btn-md">
        <input type="radio" name="master" id="master2" value="sunday"> SUNDAY
      </label>
</div>
</div>



 <!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
	<div class='uil-default-css' style='transform:scale(0.58);'>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
</div>
</div>
<!-- table -->
  <div id = "table" align="center" style="max-width:90%;margin-top:60px; margin-left:40px; display:none;height:600px !important;overflow:auto !important;">
  <button class="btn btn-md" id="sunday_update" style="background-color:#009688;font-size:16px;color:#F3E5F5;position:fixed;top:50%;right:20px;">Update</button>
  <table id="example" class="table table-striped table-bordered tablesorter table-hover results"  >
  <thead style="background-color: #D3D3D3;" id="example-head">
  
  </thead>
  <tbody id="tbody">
  </tbody>
  </table>
  </div>


  <noscript id="async-styles">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.0.1/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" crossorigin="anonymous">
</noscript>

<script> // The CSS mentioned in "async-styles" will be loaded and styles will be applied.
    var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("async-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
        webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function() {
        window.setTimeout(loadDeferredStyles, 0);
    });
    else window.addEventListener('load', loadDeferredStyles);
</script>

<script async src="js/jquery-3.2.1.min.js" onLoad="loadOtherScripts();"></script>

<script>
    function loadScript(src) {
        return new Promise(function (resolve, reject) {
            var s;
            s = document.createElement('script');
            s.src = src;
            s.onload = resolve;
            s.onerror = reject;
            document.head.appendChild(s);
        });
    }

    function loadOtherScripts() {
      loadScript('js/moment.min.js')
      .then(function() {
      Promise.all([ loadScript('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'),loadScript('js/daterangepicker.js'),loadScript('js/sidebar.js'),loadScript('https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js'),loadScript('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.0.1/js/bootstrap-switch.js')]).then(function () {
   // console.log('scripts are loaded');
      dateRangePicker();
      momentInPicker();

     
      intialFunction();
     
      tablesort();
       searchbar();
       loc_auto();
      //modal();
      }).catch(function (error) {
    //  console.log('some error!' + error)
      })
      }).catch(function (error) {
    //  console.log('Moment call error!' + error)
      })
      }
</script>

<div id="metrics" style="display: none;margin-top: 50px;">
</div>

<script type="text/javascript">
function searchbar(){

$(document).ready(function() {
  $(".searchs").keyup(function () {

    var searchTerm = $(".searchs").val();
    var listItem = $('.results tbody').children('tr');
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

  $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });

  $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','false');
  });

  $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','true');
  });

  var jobCount = $('.results tbody tr[visible="true"]').length;
    $('.counter').text(jobCount + ' item');

  if(jobCount == '0') {$('.no-result').show();}
    else {$('.no-result').hide();
	}
		  });
});
}
</script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- date range picker -->
<script>
function dateRangePicker(){
$('input[name="daterange"]').daterangepicker({
 locale: {
      format: 'DD-MM-YYYY'
    }
});
}
</script>
<script type="text/javascript">
function momentInPicker(){
$(function() {

  var start = moment().subtract(30, 'days');
	 // var start = moment();
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});
}
</script>

<!-- select Location of service center -->
<script>
function locality() {
	var city = $('#city').val();
			    $( "#location" ).autocomplete({
source: function(request, response) {
	
            $.ajax({
                url: "ajax/get_b2b_loc.php",
                data: {
                    term: request.term,
					city:city,
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
					//console.log(data);
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
}

</script>

<script>
function view_shops(){
	var veh = $("#veh_type").val();
	var loc = $("#location").val();
	var city = $("#city").val();
  var sunday = $('input:radio[name=master]:checked').val();
  
	$.ajax({
				url : "ajax/sunday_view.php",  // create a new php page to handle ajax request
				type : "POST",
				data : {"veh": veh ,"loc":loc, "city": city,"sunday":sunday },
				success : function(data) {

		$("#loading").hide();
		$("#table").show();
			$("#example-head").html("<th style='text-align:center;height:40px;'>No</th><th style='text-align:center;'>Shop Name</th><th style='text-align:center;'>Location</th><th style='text-align:center;'>Mode of operation</th><th  style='text-align:center;'>Sunday toggle</th>");
		if(data == 'no'){
			$('#table').html("<h3 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h3>");
		}
		else{
           	 $('#tbody').html(data);
			 $("#example").trigger("update");
       var arr = [];
       $('input:radio[name=master]').change(function (){
        arr.splice(0,arr.length)
	    });
       if(arr.length > 0){
        $('#sunday_update').show();
       }else {
        $('#sunday_update').hide();
       }
$('.remove-address').hide();


$('.use-address').click(function () {
$(this).hide();
$(this).next('button').show();
    var id = $(this).closest("tr").find('td:eq(1)').text();
    arr.push({id:id});
    if(arr.length > 0){
        $('#sunday_update').show();
       }else {
        $('#sunday_update').hide();
       }
    // console.log(arr);
    // sunday_update(arr);
});


$('.remove-address').click(function () {
$(this).hide();
$(this).prev('button').show();
    var id = $(this).closest("tr").find('td:eq(1)').text();
    arr.splice($.inArray(id, arr), 1);
    if(arr.length > 0){
        $('#sunday_update').show();
       }else {
        $('#sunday_update').hide();
       }
    // console.log(arr);
    // sunday_update(arr);
});
$("#sunday_update").on("click", function(){
  sunday_update(arr);
        });
		}
	  },
			error: function(xhr, ajaxOptions, thrownError) {
				//alert(xhr.status + " "+ thrownError);
		 }
	 });
}
</script>
<!-- default shops view -->

<script>

function sunday_update(arr){
 
//  $("#sunday_update").on("click", function(){
  console.log('hi',arr);
        // });
        $.ajax({
				url : "ajax/sunday_update.php",  // create a new php page to handle ajax request
				type : "POST",
				data : {"sunday_id": arr },
				success : function(data) {
          $("#table").hide();
          $("#loading").show();
          view_shops();
        }
        
	 });
}
</script>

<script>
function intialFunction(){

$(document).ready(function(){
    $('.alert-status').bootstrapSwitch();

    var $radios = $('input:radio[name=master]');
  if($radios.is(':checked') === false) {
      $radios.filter('[value=master]').prop('checked', true);
      $("#master1").parent().button('toggle');
  }
        $('#city').show();

	    $("#table").hide();
	    $("#loading").show();
		view_shops();

    $('#location').focus(function(){
    console.log('foc');
	locality()
  });
        $("#veh_type").change(function(){
		$("#table").hide();
		$("#loading").show();
		view_shops();
        });

        $("#location").on("autocompletechange", function(){
	    $("#table").hide();
			$("#loading").show();
	 		view_shops();
        });

        $("#city").change(function(){
	    $("#table").hide();
		$("#loading").show();
		$("#location").autocomplete('close').val('');	
    locality();		
 		view_shops();
	    });
    
        $('input:radio[name=master]').change(function (){
		$("#table").hide();
		$("#loading").show();
		view_shops();
	    });
});
}
</script>
<div id="popup"></div>

<script>
function tablesort(){
$(document).ready(function()
	{
			$("#example").tablesorter(  {sortList: [[0,2], [0,0]]} );
	}
);
}
</script>

</body>
</html>

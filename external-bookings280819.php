<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id'])) || $_SESSION['marketing_flag']!='1') {
    header('location:logout.php');
    die();
}

if(!empty($_POST)){

    $log= date('Y-m-d H-i-s');
    
    $mobile_number = mysqli_real_escape_string($conn, $_POST['mobile_number']);
    $mobile_number = "+91".$mobile_number;
    $source = mysqli_real_escape_string($conn, $_POST['source']);


    $name = mysqli_real_escape_string($conn, $_POST['name']);
    $name = $name=='' ? "" : $name;

    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $email = $email=='' ? "" : $email;
    
    $city = mysqli_real_escape_string($conn, $_POST['citye']);
    $city = $city=='' ? "" : $city;    
    
    $locality = mysqli_real_escape_string($conn, $_POST['locality']);
    $locality = $locality=='' ? "" : $locality;    
    
    $medium = mysqli_real_escape_string($conn, $_POST['medium']);
    $medium = $medium=='' ? "" : $medium;    
    
    $vehicle_type = mysqli_real_escape_string($conn, $_POST['vehicle_type']);
    $vehicle_type = $vehicle_type=='' ? "" : $vehicle_type;
    
    $brand_model = mysqli_real_escape_string($conn, $_POST['brand_model']);
    $brand_model = $brand_model=='' ? "" : $brand_model;
    
    $service_date = mysqli_real_escape_string($conn, $_POST['service_date']);
    $service_date = date("Y-m-d", strtotime($service_date));
    $service_date = $service_date=='' ? "" : $service_date;
    
    $service_type = mysqli_real_escape_string($conn, $_POST['service_type']);
    $service_type = $service_type=='' ? "" : $service_type;
    
    $description = mysqli_real_escape_string($conn, $_POST['description']);
    $description = $description=='' ? "" : $description;


    $exists = "SELECT reg_id FROM user_register WHERE mobile_number = '$mobile_number' ORDER BY NULL LIMIT 1";
    $exists_res = mysqli_query($conn,$exists) or die(mysqli_error($conn));
    if(mysqli_num_rows($exists_res)>=1){
        $does_row =mysqli_fetch_array($exists_res);
        $reg_id = $does_row['reg_id'];
    }
    else{
        $insert1 = "INSERT INTO user_register (name,email_id, mobile_number, password, vehicle, source, status, flag, log, Locality_Home, city, crm_log_id)
        VALUES('$name','$email','$mobile_number','', '1', '$source', '0', '-1', '$log', '$locality', '$city', '$crm_log_id')";//if brand model is present
        $insert2 = "INSERT INTO user_register (name,email_id, mobile_number, password, vehicle, source, status, flag, log, Locality_Home, city, crm_log_id)
        VALUES('$name','$email','$mobile_number','', '0', '$source', '0', '-1', '$log', '$locality','$city', '$crm_log_id')";//if brand model is not given

        $insert = $brand_model!='' ? $insert1 : $insert2; //make vehicle '1' only if the brand model is given

        $insert_result = mysqli_query($conn,$insert) or die(mysqli_error($conn));

        $last_id_query = "SELECT LAST_INSERT_ID() as id";
        error_reporting(-1);
        ini_set('display_errors', 1);

        $id_query = mysqli_query($conn,$last_id_query) or die(mysqli_error($conn));

        //echo $last_id_query;
        $id = mysqli_fetch_array($id_query);
        $reg_id = $id['id'];
    }

    if($brand_model!=''){
        $veh_id = "SELECT id,brand,model,type from admin_vehicle_table_new WHERE brand_s_model='$brand_model'";
        $veh_id_res = mysqli_query($conn,$veh_id) or die(mysqli_error($conn));
        $vehicle_id_row = mysqli_fetch_object($veh_id_res);
        $veh_id_db = $vehicle_id_row->id;
        $brand = $vehicle_id_row->brand;
        $model = $vehicle_id_row->model;
        $vehicle_type = $vehicle_id_row->type;

        //insert only if the vehicle doesn't exit
        $veh_exists = "SELECT id FROM user_vehicle_table WHERE vehicle_id = '$veh_id_db' AND user_id = '$reg_id' and flag='1'";
        $veh_exists_res = mysqli_query($conn,$veh_exists) or die(mysqli_error($conn));
        if(mysqli_num_rows($veh_exists_res)>=1){
            $veh_exists_row =mysqli_fetch_object($veh_exists_res);
            $user_vehicle_id = $veh_exists_row->id;
        }
        else{
            $user_vehicle = "INSERT INTO `user_vehicle_table` (user_id, vehicle_id, brand, model, type, reg_no, flag, log, status, crm_log_id)
                                VALUES ('$reg_id', '$veh_id_db', '$brand', '$model', '$vehicle_type','', '1', '$log', '1', '$crm_log_id')";

            $user_vehicle_result = mysqli_query($conn,$user_vehicle) or die(mysqli_error($conn));

            $last_id_query_veh = "SELECT id from user_vehicle_table where user_id = '$reg_id' AND vehicle_id = '$veh_id_db'";
            $id_query_veh = mysqli_query($conn,$last_id_query_veh) or die(mysqli_error($conn));

            $veh_id = mysqli_fetch_object($id_query_veh);
            $user_vehicle_id =  $veh_id->id;
        }

    }//if brand model is present

    $user_vehicle_id = $user_vehicle_id=='' ? "0" : $user_vehicle_id;
    $veh_id_db = empty($veh_id_db) ? "0" : $veh_id_db;

//crm alloc id
    if($vehicle_type=="2w"){
        if($brand=="Royal Enfield"){
            $cond="And vehicle_type='RE'";
        }
        else{
            $cond="And vehicle_type!='RE'";
        }
    }
    else{
        $cond="";
    }
$crm_query = "SELECT ".ucfirst($city)."_crm_id as crm_id FROM go_axle_service_price_tbl WHERE service_type = '$service_type' AND type = '$vehicle_type' $cond LIMIT 1";
$crm_res = mysqli_query($conn, $crm_query);
while($crm_row = mysqli_fetch_object($crm_res)){
     $crm_ids = $crm_row->crm_id;
}
//$crm_ids_join = implode(',', $crm_ids);
$crm_ids_array = explode(",", $crm_ids);
$crm_ids_array = array_unique($crm_ids_array);//ids that are supposed to be allocated

$crm_ids_count = count($crm_ids_array);

if($crm_ids_count>1){
    $crm_ids_count_query = $crm_ids_count-1;
    $last_crm = "SELECT crm_allocate_id FROM user_booking_tb WHERE service_type = '$service_type' AND vehicle_type = '$vehicle_type' ORDER BY booking_id DESC LIMIT 0, $crm_ids_count_query";
    $last_crm_res = mysqli_query($conn, $last_crm);
    while($last_crm_row = mysqli_fetch_object($last_crm_res)){
        $assigned_ids[] = $last_crm_row->crm_allocate_id;
    }

    $assigned_ids = array_unique($assigned_ids); //assigned crm ids used in previous bookings

    $to_be_assigned = array_diff($crm_ids_array, $assigned_ids); // ids that can be assigned for current booking
    $to_be_assigned = array_values($to_be_assigned);
    $crm_alloc_id = $to_be_assigned[0];//can be used for insertion.
}
else{
    $crm_alloc_id = $crm_ids_count !='0' ? array_shift($crm_ids_array) : "";
}
//crm alloc id
    
    // if (ucwords($city)=='Bangalore') {
    
    //   $crm_alloc_id="crm049";
    //   # code...
    // }
    
    // if (ucwords($city)=='Hyderabad') {
    
    //   $crm_alloc_id="crm060";
    //   # code...
    // }
if($source=="Re-Engagement Bookings"){
    $booking_query = "INSERT INTO `user_booking_tb` (user_id, mec_id, shop_name, vech_id, user_veh_id, vehicle_type, booking_status,
    user_vech_no, service_type,initial_service_type, description, details, amt, pick_up, pickup_time, pickup_address,
    service_date, device_details, payment_type, lat, lng, service_description, status, flag, coupon_code,
    crm_update_time, crm_log_id, source, log, axle_flag, booking_otp_confirm, crm_allocate_id, crm_update_id,
    slot_time, time_comm, mode_comm,city, locality, utm_campaign, utm_medium, utm_source)

    VALUES ('$reg_id', '0', '$service_type', '$veh_id_db', '$user_vehicle_id', '$vehicle_type', '1',
    '-', '$service_type','$service_type', '-', '-', '', '0', '0', '-',
    '$service_date', '', '-', '-', '-', '$description', '0', '0', '-',
    '$log', '$crm_log_id', 'Re-Engagement Bookings', '$log', '0', '0', '$crm_alloc_id', '$crm_alloc_id',
    '', '-', '-', '$city','$locality', '', '$medium', '$source')";
}

else{
    $booking_query = "INSERT INTO `user_booking_tb` (user_id, mec_id, shop_name, vech_id, user_veh_id, vehicle_type, booking_status,
    user_vech_no, service_type,initial_service_type, description, details, amt, pick_up, pickup_time, pickup_address,
    service_date, device_details, payment_type, lat, lng, service_description, status, flag, coupon_code,
    crm_update_time, crm_log_id, source, log, axle_flag, booking_otp_confirm, crm_allocate_id, crm_update_id,
    slot_time, time_comm, mode_comm,city, locality, utm_campaign, utm_medium, utm_source)

    VALUES ('$reg_id', '0', '$service_type', '$veh_id_db', '$user_vehicle_id', '$vehicle_type', '1',
    '-', '$service_type','$service_type', '-', '-', '', '0', '0', '-',
    '$service_date', '', '-', '-', '-', '$description', '0', '0', '-',
    '$log', '$crm_log_id', 'External Bookings', '$log', '0', '0', '$crm_alloc_id', '$crm_alloc_id',
    '', '-', '-', '$city','$locality', '', '$medium', '$source')";
    }


    $booking_result = mysqli_query($conn,$booking_query) or die(mysqli_error($conn));


    $last_id_query = "SELECT LAST_INSERT_ID() as id_value";
    $id_query = mysqli_query($conn,$last_id_query) or die(mysqli_error($conn));

    //echo $last_id_query;
    $id_val = mysqli_fetch_array($id_query);
    $id = $id_val['id_value'];

}

?>
<!DOCTYPE html>
<html>
<head>
<title>External Bookings</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>
<!-- jQuery library -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">    
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style>
    <!-- auto complete -->
    @charset "utf-8";.ui-autocomplete{z-index:9999999999 !important;cursor:default;list-style:none;}
    .ui-widget{}
    .ui-autocomplete{overflow-y:auto;overflow-x:hidden;}
    .ui-menu{width:0px;display:none;}
    .ui-autocomplete > li{padding:10px;padding-left:10px;}
    ul{margin-bottom:0;}
    .ui-autocomplete > li.ui-state-focus{background-color:#DDD;}
    .ui-autocomplete .ui-front .ui-menu .ui-widget .ui-widget-content{border:0;}
    .ui-helper-hidden-accessible{display:none;}
    .gobumpr-icon{font-style:normal;font-weight:normal;speak:none;display:inline-block;text-decoration:inherit;text-align:center;font-variant:normal;text-transform:none;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
    .ui-widget{background-color:white;width:100%;}
    .ui-widget-content{padding-left:1px;display:block;width:20px;position:relative;line-height:12px;max-height:210px;border:.5px solid #DADADA;}
    .ui-widget{}
    .ui-autocomplete { position: absolute; cursor: default;}

</style>

</head>
<body style="overflow-x: hidden">
<?php include_once("header.php"); ?>
<script>
    $(document).ready(function(){
        $('#city').hide();
    })
</script>
<div class="row" style="margin-top:12%">
    <div class="container">
    <?php if(!empty($id)){?>
        <p align="center" style="font-size:16px">Booking added! Booking ID: <?php echo $id; ?> </p>
    <?php }//if ?>
        <form action='' method='post' id="external_sources">
        <div class="row form-group">
            <div class="col-md-10 col-md-offset-1">
            <label style="display:block">Mandatory Inputs:</label>
                <div class="col-md-3 form-group">
                    <input class="form-control" type="tel" minilength="9" maxlength="10" name='mobile_number' onchange="try{setCustomValidity('')}catch(e){}" pattern="^[0-9]{6}|[0-9]{8}|[0-9]{10}$" title="Mobile number must have 10 digits!" required placeholder="Mobile Number">
                </div>
                <div class="col-md-3 form-group">
                    <input type="text" class=" form-control autocomplete " name="source" id="source" required placeholder="Source">
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
            <label style="display:block">Optional Inputs:</label>
                <div class="col-md-3 form-group">
                    <select class="form-control" name='vehicle_type' id="vehicle_type">
                        <option value="" selected>Select Vehicle Type</option>
                        <option value="2w">Bike</option>
                        <option value="4w">Car</option>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <input class="form-control autocomplete" type="text" name='brand_model' id="brand_model" placeholder="Brand Model">
                </div>
                <div class="col-md-3 form-group">
                    <input class="form-control date-pick datepicker" type="text" id=
                    "service_date" name='service_date' placeholder="Service Date">
                </div>
                <div class="col-md-3 form-group">
                    <input class="form-control autocomplete" type="text" name='service_type' id="service_type" placeholder="Service Type">
                </div>                
                <div class="col-md-3 form-group">
                    <input class="form-control" type="text" name='name' placeholder="Name">
                </div>
                <div class="col-md-3 form-group">
                    <input class="form-control" type="email" name='email' placeholder="Email">
                </div>
                <div class="col-md-3 form-group">
                    <select class="form-control" name='citye' id="citye" required>
                        <option value="chennai" selected>Chennai</option>
                        <?php 
                        $sql_city = "SELECT DISTINCT city FROM localities WHERE city!='Chennai' ";
                        $res_city = mysqli_query($conn,$sql_city);
                        while($row_city = mysqli_fetch_array($res_city)){
                            ?>
                             <option value="<?php echo $row_city['city']; ?>"><?php echo $row_city['city']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>                
                <div class="col-md-3 form-group">
                    <input class="form-control autocomplete" type="text" name='locality' id="locality" placeholder="Locality">
                </div>
                <div class="col-md-3 form-group">
                    <input class="form-control" type="text" name='medium' placeholder="Medium">
                </div>
                <div class="col-md-3 form-group">
                    <textarea class="form-control" name='description' placeholder="Description"></textarea>
                </div>
            </div><!--col-->
        </div><!--row-->
            <br><div align="center"><button type="submit" id="submit" class="btn btn-warning">Add Booking</button></div>
        </form>
    </div>
</div>

<style>
/*! * Datepicker for Bootstrap */
 .datepicker-dropdown:after,.datepicker-dropdown:before{content:'';display:inline-block;border-top:0;position:absolute}.datepicker{padding:4px;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;direction:ltr}.datepicker-inline{width:220px}.datepicker.datepicker-rtl{direction:rtl}.datepicker.datepicker-rtl table tr td span{float:right}.datepicker-dropdown{top:0;left:0}.datepicker-dropdown:before{border-left:7px solid transparent;border-right:7px solid transparent;border-bottom:7px solid #ccc;border-bottom-color:rgba(0,0,0,.2)}.datepicker-dropdown:after{border-left:6px solid transparent;border-right:6px solid transparent;border-bottom:6px solid #fff}.datepicker-dropdown.datepicker-orient-left:before{left:6px}.datepicker-dropdown.datepicker-orient-left:after{left:7px}.datepicker-dropdown.datepicker-orient-right:before{right:6px}.datepicker-dropdown.datepicker-orient-right:after{right:7px}.datepicker-dropdown.datepicker-orient-top:before{top:-7px}.datepicker-dropdown.datepicker-orient-top:after{top:-6px}.datepicker-dropdown.datepicker-orient-bottom:before{bottom:-7px;border-bottom:0;border-top:7px solid #999}.datepicker-dropdown.datepicker-orient-bottom:after{bottom:-6px;border-bottom:0;border-top:6px solid #fff}.datepicker>div{display:none}.datepicker.days div.datepicker-days,.datepicker.months div.datepicker-months,.datepicker.years div.datepicker-years{display:block}.datepicker table{margin:0;-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.datepicker td,.datepicker th{text-align:center;width:20px;height:20px;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;border:none}.table-striped .datepicker table tr td,.table-striped .datepicker table tr th{background-color:transparent}.datepicker table tr td.day.focused,.datepicker table tr td.day:hover{background:#eee;cursor:pointer}.datepicker table tr td.new,.datepicker table tr td.old{color:#999}.datepicker table tr td.disabled,.datepicker table tr td.disabled:hover{background:0 0;color:#999;cursor:default}.datepicker table tr td.today,.datepicker table tr td.today.disabled,.datepicker table tr td.today.disabled:hover,.datepicker table tr td.today:hover{background-color:#fde19a;background-image:-moz-linear-gradient(top,#fdd49a,#fdf59a);background-image:-ms-linear-gradient(top,#fdd49a,#fdf59a);background-image:-webkit-gradient(linear,0 0,0 100%,from(#fdd49a),to(#fdf59a));background-image:-webkit-linear-gradient(top,#fdd49a,#fdf59a);background-image:-o-linear-gradient(top,#fdd49a,#fdf59a);background-image:linear-gradient(top,#fdd49a,#fdf59a);background-repeat:repeat-x;border-color:#fdf59a #fdf59a #fbed50;border-color:rgba(0,0,0,.1) rgba(0,0,0,.1) rgba(0,0,0,.25);filter:progid: DXImageTransform.Microsoft.gradient(enabled=false);color:#000}.datepicker table tr td.today.active,.datepicker table tr td.today.disabled,.datepicker table tr td.today.disabled.active,.datepicker table tr td.today.disabled.disabled,.datepicker table tr td.today.disabled:active,.datepicker table tr td.today.disabled:hover,.datepicker table tr td.today.disabled:hover.active,.datepicker table tr td.today.disabled:hover.disabled,.datepicker table tr td.today.disabled:hover:active,.datepicker table tr td.today.disabled:hover:hover,.datepicker table tr td.today.disabled:hover[disabled],.datepicker table tr td.today.disabled[disabled],.datepicker table tr td.today:active,.datepicker table tr td.today:hover,.datepicker table tr td.today:hover.active,.datepicker table tr td.today:hover.disabled,.datepicker table tr td.today:hover:active,.datepicker table tr td.today:hover:hover,.datepicker table tr td.today:hover[disabled],.datepicker table tr td.today[disabled]{background-color:#fdf59a}.datepicker table tr td.today.active,.datepicker table tr td.today.disabled.active,.datepicker table tr td.today.disabled:active,.datepicker table tr td.today.disabled:hover.active,.datepicker table tr td.today.disabled:hover:active,.datepicker table tr td.today:active,.datepicker table tr td.today:hover.active,.datepicker table tr td.today:hover:active{background-color:#fbf069\9}.datepicker table tr td.today:hover:hover{color:#000}.datepicker table tr td.today.active:hover{color:#fff}.datepicker table tr td.range,.datepicker table tr td.range.disabled,.datepicker table tr td.range.disabled:hover,.datepicker table tr td.range:hover{background:#eee;-webkit-border-radius:0;-moz-border-radius:0;border-radius:0}.datepicker table tr td.range.today,.datepicker table tr td.range.today.disabled,.datepicker table tr td.range.today.disabled:hover,.datepicker table tr td.range.today:hover{background-color:#f3d17a;background-image:-moz-linear-gradient(top,#f3c17a,#f3e97a);background-image:-ms-linear-gradient(top,#f3c17a,#f3e97a);background-image:-webkit-gradient(linear,0 0,0 100%,from(#f3c17a),to(#f3e97a));background-image:-webkit-linear-gradient(top,#f3c17a,#f3e97a);background-image:-o-linear-gradient(top,#f3c17a,#f3e97a);background-image:linear-gradient(top,#f3c17a,#f3e97a);background-repeat:repeat-x;border-color:#f3e97a #f3e97a #edde34;border-color:rgba(0,0,0,.1) rgba(0,0,0,.1) rgba(0,0,0,.25);filter:progid: DXImageTransform.Microsoft.gradient(enabled=false);-webkit-border-radius:0;-moz-border-radius:0;border-radius:0}.datepicker table tr td.range.today.active,.datepicker table tr td.range.today.disabled,.datepicker table tr td.range.today.disabled.active,.datepicker table tr td.range.today.disabled.disabled,.datepicker table tr td.range.today.disabled:active,.datepicker table tr td.range.today.disabled:hover,.datepicker table tr td.range.today.disabled:hover.active,.datepicker table tr td.range.today.disabled:hover.disabled,.datepicker table tr td.range.today.disabled:hover:active,.datepicker table tr td.range.today.disabled:hover:hover,.datepicker table tr td.range.today.disabled:hover[disabled],.datepicker table tr td.range.today.disabled[disabled],.datepicker table tr td.range.today:active,.datepicker table tr td.range.today:hover,.datepicker table tr td.range.today:hover.active,.datepicker table tr td.range.today:hover.disabled,.datepicker table tr td.range.today:hover:active,.datepicker table tr td.range.today:hover:hover,.datepicker table tr td.range.today:hover[disabled],.datepicker table tr td.range.today[disabled]{background-color:#f3e97a}.datepicker table tr td.range.today.active,.datepicker table tr td.range.today.disabled.active,.datepicker table tr td.range.today.disabled:active,.datepicker table tr td.range.today.disabled:hover.active,.datepicker table tr td.range.today.disabled:hover:active,.datepicker table tr td.range.today:active,.datepicker table tr td.range.today:hover.active,.datepicker table tr td.range.today:hover:active{background-color:#efe24b\9}.datepicker table tr td.selected,.datepicker table tr td.selected.disabled,.datepicker table tr td.selected.disabled:hover,.datepicker table tr td.selected:hover{background-color:#9e9e9e;background-image:-moz-linear-gradient(top,#b3b3b3,grey);background-image:-ms-linear-gradient(top,#b3b3b3,grey);background-image:-webkit-gradient(linear,0 0,0 100%,from(#b3b3b3),to(grey));background-image:-webkit-linear-gradient(top,#b3b3b3,grey);background-image:-o-linear-gradient(top,#b3b3b3,grey);background-image:linear-gradient(top,#b3b3b3,grey);background-repeat:repeat-x;border-color:grey grey #595959;border-color:rgba(0,0,0,.1) rgba(0,0,0,.1) rgba(0,0,0,.25);filter:progid: DXImageTransform.Microsoft.gradient(enabled=false);color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,.25)}.datepicker table tr td.selected.active,.datepicker table tr td.selected.disabled,.datepicker table tr td.selected.disabled.active,.datepicker table tr td.selected.disabled.disabled,.datepicker table tr td.selected.disabled:active,.datepicker table tr td.selected.disabled:hover,.datepicker table tr td.selected.disabled:hover.active,.datepicker table tr td.selected.disabled:hover.disabled,.datepicker table tr td.selected.disabled:hover:active,.datepicker table tr td.selected.disabled:hover:hover,.datepicker table tr td.selected.disabled:hover[disabled],.datepicker table tr td.selected.disabled[disabled],.datepicker table tr td.selected:active,.datepicker table tr td.selected:hover,.datepicker table tr td.selected:hover.active,.datepicker table tr td.selected:hover.disabled,.datepicker table tr td.selected:hover:active,.datepicker table tr td.selected:hover:hover,.datepicker table tr td.selected:hover[disabled],.datepicker table tr td.selected[disabled]{background-color:grey}.datepicker table tr td.selected.active,.datepicker table tr td.selected.disabled.active,.datepicker table tr td.selected.disabled:active,.datepicker table tr td.selected.disabled:hover.active,.datepicker table tr td.selected.disabled:hover:active,.datepicker table tr td.selected:active,.datepicker table tr td.selected:hover.active,.datepicker table tr td.selected:hover:active{background-color:#666\9}.datepicker table tr td.active,.datepicker table tr td.active.disabled,.datepicker table tr td.active.disabled:hover,.datepicker table tr td.active:hover{background-color:#006dcc;background-image:-moz-linear-gradient(top,#08c,#04c);background-image:-ms-linear-gradient(top,#08c,#04c);background-image:-webkit-gradient(linear,0 0,0 100%,from(#08c),to(#04c));background-image:-webkit-linear-gradient(top,#08c,#04c);background-image:-o-linear-gradient(top,#08c,#04c);background-image:linear-gradient(top,#08c,#04c);background-repeat:repeat-x;border-color:#04c #04c #002a80;border-color:rgba(0,0,0,.1) rgba(0,0,0,.1) rgba(0,0,0,.25);filter:progid: DXImageTransform.Microsoft.gradient(enabled=false);color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,.25)}.datepicker table tr td.active.active,.datepicker table tr td.active.disabled,.datepicker table tr td.active.disabled.active,.datepicker table tr td.active.disabled.disabled,.datepicker table tr td.active.disabled:active,.datepicker table tr td.active.disabled:hover,.datepicker table tr td.active.disabled:hover.active,.datepicker table tr td.active.disabled:hover.disabled,.datepicker table tr td.active.disabled:hover:active,.datepicker table tr td.active.disabled:hover:hover,.datepicker table tr td.active.disabled:hover[disabled],.datepicker table tr td.active.disabled[disabled],.datepicker table tr td.active:active,.datepicker table tr td.active:hover,.datepicker table tr td.active:hover.active,.datepicker table tr td.active:hover.disabled,.datepicker table tr td.active:hover:active,.datepicker table tr td.active:hover:hover,.datepicker table tr td.active:hover[disabled],.datepicker table tr td.active[disabled]{background-color:#04c}.datepicker table tr td.active.active,.datepicker table tr td.active.disabled.active,.datepicker table tr td.active.disabled:active,.datepicker table tr td.active.disabled:hover.active,.datepicker table tr td.active.disabled:hover:active,.datepicker table tr td.active:active,.datepicker table tr td.active:hover.active,.datepicker table tr td.active:hover:active{background-color:#039\9}.datepicker table tr td span{display:block;width:23%;height:54px;line-height:54px;float:left;margin:1%;cursor:pointer;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px}.datepicker table tr td span:hover{background:#eee}.datepicker table tr td span.disabled,.datepicker table tr td span.disabled:hover{background:0 0;color:#999;cursor:default}.datepicker table tr td span.active,.datepicker table tr td span.active.disabled,.datepicker table tr td span.active.disabled:hover,.datepicker table tr td span.active:hover{background-color:#006dcc;background-image:-moz-linear-gradient(top,#08c,#04c);background-image:-ms-linear-gradient(top,#08c,#04c);background-image:-webkit-gradient(linear,0 0,0 100%,from(#08c),to(#04c));background-image:-webkit-linear-gradient(top,#08c,#04c);background-image:-o-linear-gradient(top,#08c,#04c);background-image:linear-gradient(top,#08c,#04c);background-repeat:repeat-x;border-color:#04c #04c #002a80;border-color:rgba(0,0,0,.1) rgba(0,0,0,.1) rgba(0,0,0,.25);filter:progid: DXImageTransform.Microsoft.gradient(enabled=false);color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,.25)}.datepicker table tr td span.active.active,.datepicker table tr td span.active.disabled,.datepicker table tr td span.active.disabled.active,.datepicker table tr td span.active.disabled.disabled,.datepicker table tr td span.active.disabled:active,.datepicker table tr td span.active.disabled:hover,.datepicker table tr td span.active.disabled:hover.active,.datepicker table tr td span.active.disabled:hover.disabled,.datepicker table tr td span.active.disabled:hover:active,.datepicker table tr td span.active.disabled:hover:hover,.datepicker table tr td span.active.disabled:hover[disabled],.datepicker table tr td span.active.disabled[disabled],.datepicker table tr td span.active:active,.datepicker table tr td span.active:hover,.datepicker table tr td span.active:hover.active,.datepicker table tr td span.active:hover.disabled,.datepicker table tr td span.active:hover:active,.datepicker table tr td span.active:hover:hover,.datepicker table tr td span.active:hover[disabled],.datepicker table tr td span.active[disabled]{background-color:#04c}.datepicker table tr td span.active.active,.datepicker table tr td span.active.disabled.active,.datepicker table tr td span.active.disabled:active,.datepicker table tr td span.active.disabled:hover.active,.datepicker table tr td span.active.disabled:hover:active,.datepicker table tr td span.active:active,.datepicker table tr td span.active:hover.active,.datepicker table tr td span.active:hover:active{background-color:#039\9}.datepicker table tr td span.new,.datepicker table tr td span.old{color:#999}.datepicker th.datepicker-switch{width:145px}.datepicker tfoot tr th,.datepicker thead tr:first-child th{cursor:pointer}.datepicker tfoot tr th:hover,.datepicker thead tr:first-child th:hover{background:#eee}.datepicker .cw{font-size:10px;width:12px;padding:0 2px 0 5px;vertical-align:middle}.datepicker thead tr:first-child th.cw{cursor:default;background-color:transparent}.input-append.date .add-on i,.input-prepend.date .add-on i{cursor:pointer;width:16px;height:16px}.input-daterange input{text-align:center}.input-daterange input:first-child{-webkit-border-radius:3px 0 0 3px;-moz-border-radius:3px 0 0 3px;border-radius:3px 0 0 3px}.input-daterange input:last-child{-webkit-border-radius:0 3px 3px 0;-moz-border-radius:0 3px 3px 0;border-radius:0 3px 3px 0}.input-daterange .add-on{display:inline-block;width:auto;min-width:16px;height:20px;padding:4px 5px;font-weight:400;line-height:20px;text-align:center;text-shadow:0 1px 0 #fff;vertical-align:middle;background-color:#eee;border:1px solid #ccc;margin-left:-5px;margin-right:-5px}.datepicker.dropdown-menu{position:absolute;top:100%;left:0;z-index:1000;float:left;display:none;min-width:160px;list-style:none;background-color:#fff;border:1px solid #ccc;border:1px solid rgba(0,0,0,.2);-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;-webkit-box-shadow:0 5px 10px rgba(0,0,0,.2);-moz-box-shadow:0 5px 10px rgba(0,0,0,.2);box-shadow:0 5px 10px rgba(0,0,0,.2);-webkit-background-clip:padding-box;-moz-background-clip:padding;background-clip:padding-box;color:#333;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:13px;line-height:20px}.datepicker.dropdown-menu td,.datepicker.dropdown-menu th{padding:4px 5px}
 th.next:after {
    content: ">>";
}
 th.prev:after {
    content: "<<";
}
 </style>
    <link rel="stylesheet" href="css/style.css">
        <!-- Date and time pickers -->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
      <script>
        var date = new Date();
        date.setDate(date.getDate());

        $('.date-pick').datepicker({
            startDate: date
        });
        $('input.date-pick').datepicker('setDate', 'today');

    </script>

<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" integrity="sha256-xNjb53/rY+WmG+4L6tTl9m6PpqknWZvRt0rO1SRnJzw=" crossorigin="anonymous"></script>
<!-- auto complete -->
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/jquery-ui.structure.min.css">
<link rel="stylesheet" href="css/jquery-ui.theme.min.css">

<script>
$(function() {
    $( "#locality" ).autocomplete({
    source: function(request, response) {
            $.ajax({
                url: "ajax/get_work_locality.php",
                data: {
                    term: request.term,
                    type:"GET",
                    city: $('#citye').val()
                },
                dataType: 'json'
            }).done(function(data) {
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
                    response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
        delay: 2,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
         change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
             $(this).autocomplete("search");
    });
});  
$(function() {
            $( "#brand_model" ).autocomplete({

source: function(request, response) {
            $.ajax({
                url: "ajax/get_brandmodel.php",
                data: {
                    term: request.term,
                    extraParams:$('#vehicle_type option:selected').val()
                },
                dataType: 'json'
            }).done(function(data) {
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
                    response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
        delay: 2,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
        change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
             $(this).autocomplete("search");
        });
});  
$(function() {
    $( "#source" ).autocomplete({
    source: function(request, response) {
            $.ajax({
                url: "ajax/sources.php",
                data: {
                    term: request.term
                },
                dataType: 'json'
            }).done(function(data) {
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
                    response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
        delay: 2,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
         change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
             $(this).autocomplete("search");
    });
});  
$(function() {
    $( "#service_type" ).autocomplete({
    source: function(request, response) {
            $.ajax({
                url: "ajax/get_service_type.php",
                data: {
                    term: request.term,
                    type : $('#vehicle_type option:checked').val()
                },
                dataType: 'json'
            }).done(function(data) {
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
                    response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
        delay: 2,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
         change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
             $(this).autocomplete("search");
    });
});  
</script>

<script>
  $(document).ready(function(){
    $('#city').change(function(){
      $('#locality').empty();
      $('#locality').autocomplete('close').val('');
    });
  })
  </script>
  <script>
  $(document).ready(function(){
    $('#citye').change(function(){
      $('#locality').empty();
      $('#locality').autocomplete('close').val('');
    });
  })
  </script>
</body>
</html>
#!/bin/bash
set -e
# Lets write the public key of our aws instance
eval $(ssh-agent -s)
# echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
echo "-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAnx+JCWVME4QECrerPugeWXyir6pGj1oBcWozBAHRKE8xxRJV7XvNmN12NmPe
342J0r2hgUXpPleFLmfsm/YDnY5iFsQvWboAl5AMBffVPsf7u412cMVmoeYbAMuhdT7H5uCnUX8p
HkzY3eckyDneixyzh3ovfeGyVI1SX2ptxP2eXn2na62uStjvjWdEim4aMclC6Ee9/AgFbmi+R7mf
pkFTvKSuVjFcnmIKg/+U3oZEcgwRHDABKXteWUH/V6hybB7y0v2re0d5HXmZeGQPQpCLD32Y68EB
bxiwUqeiz3ESvzHl+VtNkK9YYTtxZTjHMCVhWr8qOBDqEyFExFWa5wIDAQABAoIBAQCASads+Ms1
7Lhpf5zDkdZuJSrwX2KVzmXdb1aEA4cCbZ7dmKPIXIjslpfoz+nu6CjWy+X62Eds6JjKSiUbtGND
0nhIjJD0UR6LEypuYVn+TLMqiamUz8GoA2F/6axZX21BOZwL+25GLISnmuQvvWArXc9sCVliruTc
gDeKD79CEkXj5GDNmsk+VSm5b5tefIICx2Jl4MnuKWHZ+ExBV18VOE/p+b5s2xFNdnAGnMgcwHZ1
UPbc04RpdUEVbbuIgERJ4gZpkoI8lD9XY+sUDUoMdkN3g7lCr3vJxS6j98fWn6Y2yW7HBAc4fygC
V67jCJbpLI45TT+omd5xSBKTYJiBAoGBAM66TvzkBnLXZHBL1YH0URI6FKpHnCFIfaP4tsDKPD1y
eYww/fd8Pt81OjdVU4eUZhINGJM5Uj0DDew+ZPB6GtwmSfpuV/VtocKHteOFtCeju4Nyc4r6w0sS
y3XbFojszhMitYmthbNMXX5kogLSzrGXpIQlXDd/jCEKSn5ZxoPBAoGBAMUMlnu8cUcuLZHOGX8J
IuzZJoDe8YIgUiw5nE/qpsXtBo++Q8LUklab1OTd3JxZEfELest7N+D+OEDyvMQG7R+dpcwMSay8
W+EY+5i4+blcWKgZMZsjzEQSxw+fBywK8M563g8cnXYetdeOkaAYT5xuTOYJd+TFwfz+Fw45H6in
AoGBAJBbQTUO9gHTrutLyTgKJ2KW9D9D6h7DxBhwYQ2XKyGtgnsG6xGazTyIqrJg/wc59Vy9kF+e
M3MvYtWJbGsQmRkKE7awvvM/LynsInNStk7H4vUTQuPV3/HYmgMejknnMq/iGOqfJTHjLZZJJ2jZ
VERnJBazEAYhJvUbfpFkl18BAoGAIuCngh9IraCu0C/6s1GarPGCryzG8pFl+g34bOzLuBzMHIVA
2Stm+415kUjz03iwhEnT4lBdA6X7rwNIBipzrjTpM8S5BaoRWTZi0hVrnjOKPba34Q3lCeeI9Grh
iholpWZ6B5K5DD9DUHQbsoqbDMU5Nz5Vylh2jaEr04io4eECgYAJCuCTYrxLmfqJrm9TYOAPHYUq
LC7xn+ZACwPUObr6+Rdjf0poIdHyHDlX0BG5w5kDnqblrzAXwTmACrxD5YToioLq2PBcpdTW+sAw
Jti9WdbZLEW61toV8DdxkQg5mb4DzPS/rq5yyRtex1gtkGTqCNSsbyr4TTqVe7S/tz9SWw==
-----END RSA PRIVATE KEY-----" | tr -d '\r' | ssh-add - > /dev/null
# ** Alternative approach
# echo -e "$PRIVATE_KEY" > /root/.ssh/id_rsa
# chmod 600 /root/.ssh/id_rsa
# ** End of alternative approach

# disable the host key checking.
./deploy/disableHostKeyChecking.sh

# echo "deploying to ${DEPLOY_SERVER}"
# ssh ${SERVER_NAME}@${DEPLOY_SERVER} <<EOF
echo "deploying to 13.233.238.146"
ssh ubuntu@13.233.238.146 <<EOF

cd /var/www/html

if [ -d "bridge" ]
then
   echo "Folder bridge exist."
   cd bridge
   git pull https://$USER_NAME:$PASSWORD@gitlab.com/gobumpr-tech/bridge-test.git
else
   echo "Folder bridge does not exist"
   git clone https://$USER_NAME:$PASSWORD@gitlab.com/gobumpr-tech/bridge-test.git bridge
fi

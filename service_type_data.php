<?php
include("config.php");
$conn = db_connect1();

$sql_service_type = "SELECT service_type FROM go_bumpr.go_axle_service_price_tbl WHERE type='4w'";
$res_service_type = mysqli_query($conn,$sql_service_type);
$sql_data = "SELECT m.mec_id,m.shop_name,m.address4,SUM(CASE WHEN b.final_bill_amt !='' THEN 1 ELSE 0 END) as Total_Billed_Customer,SUM(b.final_bill_amt) as Total_Bill,SUM(b.final_bill_amt)/SUM(CASE WHEN b.final_bill_amt !='' THEN 1 ELSE 0 END) as Avg_Bill_Value,AVG(CASE WHEN b.rating !=0 AND ((b.service_status = 'In Progress' OR b.service_status = 'Completed') OR (b2b_b.b2b_check_in_report = 1)) THEN b.rating END) as Avg_Rating,COUNT(b.booking_id) as Total_Booking,SUM(CASE WHEN ((b.service_status = 'In Progress' OR b.service_status = 'Completed') OR (b2b_b.b2b_check_in_report = 1)) THEN 1 ELSE 0 END) as Total_Completed,SUM(CASE WHEN ((b.service_status = 'In Progress' OR b.service_status = 'Completed') OR (b2b_b.b2b_check_in_report = 1)) THEN 1 ELSE 0 END)/COUNT(b.booking_id)*100 as Conversion_Rate";
$i = 0;
$count = mysqli_num_rows($res_service_type);
while($row_service_type = mysqli_fetch_object($res_service_type))
{
	$i++;
	$service_type = $row_service_type->service_type;
	$arr = explode(" ",$service_type);
	$service_type_colm = implode('_',$arr);
	$service_type_colm_completed = $service_type_colm."_completed";
	$service_type_colm_billed_cx = $service_type_colm."_billed_customers";
	$service_type_colm_bill_value = $service_type_colm."_bill_value";
	$service_type_colm_avg_bill_value = $service_type_colm."_avg_bill_value";
	$service_type_colm_avg_rating = $service_type_colm."_avg_rating";
	$sql_data = $sql_data."SUM(CASE WHEN b.service_type = '$service_type' THEN 1 ELSE 0 END) as $service_type_colm,";
	$sql_data = $sql_data."SUM(CASE WHEN b.service_type = '$service_type' AND ((b.service_status = 'In Progress' OR b.service_status = 'Completed') OR (b2b_b.b2b_check_in_report = 1)) THEN 1 ELSE 0 END) as $service_type_colm_completed,";
	$sql_data = $sql_data."SUM(CASE WHEN b.service_type = '$service_type' THEN b.final_bill_amt ELSE 0 END) as $service_type_colm_billed_cx,";
	$sql_data = $sql_data."SUM(CASE WHEN b.final_bill_amt !='' AND b.service_type = '$service_type' THEN 1 ELSE 0 END) as $service_type_colm_bill_value,";
	$sql_data = $sql_data."SUM(CASE WHEN b.final_bill_amt !='' AND b.service_type = '$service_type' THEN 1 ELSE 0 END)/SUM(CASE WHEN b.service_type = '$service_type' THEN b.final_bill_amt ELSE 0 END) as $service_type_colm_avg_bill_value,";
	$sql_data = $sql_data."AVG(CASE WHEN b.service_type='$service_type' AND b.rating !=0 AND ((b.service_status = 'In Progress' OR b.service_status = 'Completed') OR (b2b_b.b2b_check_in_report = 1)) THEN b.rating END) as $service_type_colm_avg_rating";
	if($i != $count)
	{
		$sql_data = $sql_data.",";
	}
}
$sql_data = $sql_data." FROM admin_mechanic_table m LEFT JOIN user_booking_tb b ON b.mec_id = m.mec_id WHERE m.address5 = 'Chennai' AND m.status = 1 AND type = '4w' AND DATE(b.log) BETWEEN '2018-01-01' AND '2018-01-31' GROUP BY b.mec_id;";
echo $sql_data;
?>
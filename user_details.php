<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if ((empty($_SESSION['crm_log_id']))) {
	header('location:logout.php');
	die();
}

date_default_timezone_set("Asia/Kolkata");
$today = date('Y-m-d h:i:s');
$today_time = date('d-m-Y H:i:s');
$crm_se = $_SESSION['crm_se'];
$admin_flag = $_SESSION['flag'];
$crm_log_id = $_SESSION['crm_log_id'];
$booking_id = base64_decode($_GET['bi']);
$type = base64_decode($_GET['t']);

if ($booking_id == '' || $type == '') {
	header('location:somethingwentwrong.php');
	die();
}
?>

<!DOCTYPE html>
<html>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<meta charset="utf-8">
	<title>GoBumpr Bridge</title>
	
	<link rel="stylesheet"
		  href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"/>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
		  integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.0.1/css/bootstrap3/bootstrap-switch.css"
		  rel="stylesheet" crossorigin="anonymous">
	
	
	<!-- auto complete -->
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="css/jquery-ui.min.css">
	<link rel="stylesheet" href="css/jquery-ui.structure.min.css">
	<link rel="stylesheet" href="css/jquery-ui.theme.min.css">
	<!-- stylings -->
	<link rel="stylesheet" type="text/css" href="css/style.css"/>
	
	<!-- Facebook Pixel Code -->
	<script async>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '582926561860139');
        fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
				   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
		/></noscript>
	<!-- DO NOT MODIFY -->
	<!-- End Facebook Pixel Code -->
	<!-- Google Analytics Code -->
	<script async>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-67994843-2', 'auto');
        ga('send', 'pageview');
	
	</script>
	<!-- date time picker -->
	<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript"
			src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.0.1/js/bootstrap-switch.js"></script>
	<link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
	<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="css/bootstrap-datetimepicker-standalone.css">
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBEFvPWpawLCHyQQQR_4Qx4kKeyojpDw7I&amp;libraries=places"></script>
         
<script>
  google.maps.event.addDomListener(window, 'load', initialize);
    function initialize() {
      var options = {
    componentRestrictions: {country: "ind"}
     };
      var input = document.getElementById('location_pickup');
      var autocomplete = new google.maps.places.Autocomplete(input, options);
      autocomplete.addListener('place_changed', function ()  {

      var place = autocomplete.getPlace();
   
      // place variable will have all the information you are looking for.
      $('#lat').val(place.geometry['location'].lat());
      $('#long').val(place.geometry['location'].lng());
    });
  }
</script>
<style type="text/css">
  .pac-container {
    z-index: 10000 !important;
}
</style>
	<style>
		span.make-switch.switch-radio {
			float: left;
		}
		
		.bootstrap-switch-container {
			height: 30px;
		}
	</style>
	<style>
		
		
		.socket {
			display: none;
			width: 57%;
			height: 57%;
			position: absolute;
			left: 37%;
			margin-left: -17%;
			top: 33%;
			margin-top: -17%;
		}
		
		.hex-brick {
			background: #FFA800;
			width: 30px;
			height: 17px;
			position: absolute;
			top: 5px;
			animation-name: fade;
			animation-duration: 2s;
			animation-iteration-count: infinite;
			-webkit-animation-name: fade;
			-webkit-animation-duration: 2s;
			-webkit-animation-iteration-count: infinite;
		}
		
		.h2 {
			transform: rotate(60deg);
			-webkit-transform: rotate(60deg);
		}
		
		.h3 {
			transform: rotate(-60deg);
			-webkit-transform: rotate(-60deg);
		}
		
		.gel {
			height: 30px;
			width: 30px;
			transition: all .3s;
			-webkit-transition: all .3s;
			position: absolute;
			top: 50%;
			left: 50%;
		}
		
		.center-gel {
			margin-left: -15px;
			margin-top: -15px;
			
			animation-name: pulse;
			animation-duration: 2s;
			animation-iteration-count: infinite;
			-webkit-animation-name: pulse;
			-webkit-animation-duration: 2s;
			-webkit-animation-iteration-count: infinite;
		}
		
		.c1 {
			margin-left: -47px;
			margin-top: -15px;
		}
		
		.c2 {
			margin-left: -31px;
			margin-top: -43px;
		}
		
		.c3 {
			margin-left: 1px;
			margin-top: -43px;
		}
		
		.c4 {
			margin-left: 17px;
			margin-top: -15px;
		}
		
		.c5 {
			margin-left: -31px;
			margin-top: 13px;
		}
		
		.c6 {
			margin-left: 1px;
			margin-top: 13px;
		}
		
		.c7 {
			margin-left: -63px;
			margin-top: -43px;
		}
		
		.c8 {
			margin-left: 33px;
			margin-top: -43px;
		}
		
		.c9 {
			margin-left: -15px;
			margin-top: 41px;
		}
		
		.c10 {
			margin-left: -63px;
			margin-top: 13px;
		}
		
		.c11 {
			margin-left: 33px;
			margin-top: 13px;
		}
		
		.c12 {
			margin-left: -15px;
			margin-top: -71px;
		}
		
		.c13 {
			margin-left: -47px;
			margin-top: -71px;
		}
		
		.c14 {
			margin-left: 17px;
			margin-top: -71px;
		}
		
		.c15 {
			margin-left: -47px;
			margin-top: 41px;
		}
		
		.c16 {
			margin-left: 17px;
			margin-top: 41px;
		}
		
		.c17 {
			margin-left: -79px;
			margin-top: -15px;
		}
		
		.c18 {
			margin-left: 49px;
			margin-top: -15px;
		}
		
		.c19 {
			margin-left: -63px;
			margin-top: -99px;
		}
		
		.c20 {
			margin-left: 33px;
			margin-top: -99px;
		}
		
		.c21 {
			margin-left: 1px;
			margin-top: -99px;
		}
		
		.c22 {
			margin-left: -31px;
			margin-top: -99px;
		}
		
		.c23 {
			margin-left: -63px;
			margin-top: 69px;
		}
		
		.c24 {
			margin-left: 33px;
			margin-top: 69px;
		}
		
		.c25 {
			margin-left: 1px;
			margin-top: 69px;
		}
		
		.c26 {
			margin-left: -31px;
			margin-top: 69px;
		}
		
		.c27 {
			margin-left: -79px;
			margin-top: -15px;
		}
		
		.c28 {
			margin-left: -95px;
			margin-top: -43px;
		}
		
		.c29 {
			margin-left: -95px;
			margin-top: 13px;
		}
		
		.c30 {
			margin-left: 49px;
			margin-top: 41px;
		}
		
		.c31 {
			margin-left: -79px;
			margin-top: -71px;
		}
		
		.c32 {
			margin-left: -111px;
			margin-top: -15px;
		}
		
		.c33 {
			margin-left: 65px;
			margin-top: -43px;
		}
		
		.c34 {
			margin-left: 65px;
			margin-top: 13px;
		}
		
		.c35 {
			margin-left: -79px;
			margin-top: 41px;
		}
		
		.c36 {
			margin-left: 49px;
			margin-top: -71px;
		}
		
		.c37 {
			margin-left: 81px;
			margin-top: -15px;
		}
		
		.r1 {
			animation-name: pulse;
			animation-duration: 2s;
			animation-iteration-count: infinite;
			animation-delay: .2s;
			-webkit-animation-name: pulse;
			-webkit-animation-duration: 2s;
			-webkit-animation-iteration-count: infinite;
			-webkit-animation-delay: .2s;
		}
		
		.r2 {
			animation-name: pulse;
			animation-duration: 2s;
			animation-iteration-count: infinite;
			animation-delay: .4s;
			-webkit-animation-name: pulse;
			-webkit-animation-duration: 2s;
			-webkit-animation-iteration-count: infinite;
			-webkit-animation-delay: .4s;
		}
		
		.r3 {
			animation-name: pulse;
			animation-duration: 2s;
			animation-iteration-count: infinite;
			animation-delay: .6s;
			-webkit-animation-name: pulse;
			-webkit-animation-duration: 2s;
			-webkit-animation-iteration-count: infinite;
			-webkit-animation-delay: .6s;
		}
		
		.r1 > .hex-brick {
			animation-name: fade;
			animation-duration: 2s;
			animation-iteration-count: infinite;
			animation-delay: .2s;
			-webkit-animation-name: fade;
			-webkit-animation-duration: 2s;
			-webkit-animation-iteration-count: infinite;
			-webkit-animation-delay: .2s;
		}
		
		.r2 > .hex-brick {
			animation-name: fade;
			animation-duration: 2s;
			animation-iteration-count: infinite;
			animation-delay: .4s;
			-webkit-animation-name: fade;
			-webkit-animation-duration: 2s;
			-webkit-animation-iteration-count: infinite;
			-webkit-animation-delay: .4s;
		}
		
		.r3 > .hex-brick {
			animation-name: fade;
			animation-duration: 2s;
			animation-iteration-count: infinite;
			animation-delay: .6s;
			-webkit-animation-name: fade;
			-webkit-animation-duration: 2s;
			-webkit-animation-iteration-count: infinite;
			-webkit-animation-delay: .6s;
		}
		
		
		@keyframes pulse {
			0% {
				-webkit-transform: scale(1);
				transform: scale(1);
			}
			
			50% {
				-webkit-transform: scale(0.01);
				transform: scale(0.01);
			}
			
			100% {
				-webkit-transform: scale(1);
				transform: scale(1);
			}
		}
		
		@keyframes fade {
			0% {
				background: #FFA800;
			}
			
			50% {
				background: #f9cc70;
			}
			
			100% {
				background: #FFA800;
			}
		}
		
		@-webkit-keyframes pulse {
			0% {
				-webkit-transform: scale(1);
				transform: scale(1);
			}
			
			50% {
				-webkit-transform: scale(0.01);
				transform: scale(0.01);
			}
			
			100% {
				-webkit-transform: scale(1);
				transform: scale(1);
			}
		}
		
		@-webkit-keyframes fade {
			0% {
				background: #FFA800;
			}
			
			50% {
				background: #ffc759;
			}
			
			100% {
				background: #FFA800;
			}
		}
		
		/*.loader_overlay{
	display:none;
    height: 100%;
    width: 100%;
    margin-top: -113%;
    background: rgba(221, 221, 221,15);
    opacity: 0.5;
  }
  .loader {
	display:none;
	margin-left: 40%;
    position: fixed;
    margin-top: -65%;
    border: 16px solid #f3f3f3; /* Light grey */
		border-top:
		
		16
		px solid #3498db
		
		; /* Blue */
		border-radius:
		
		50
		%
		;
		width:
		
		90
		px
		
		;
		height:
		
		90
		px
		
		;
		animation: spin
		
		0.5
		s linear infinite
		
		;
		z-index:
		
		9999
		;
		}
		
		@keyframes spin {
			0% {
				transform: rotate(0deg);
			}
			100% {
				transform: rotate(360deg);
			}
		}
		
		*
		
		/
		
		
		nav, ol {
			background: #009688 !important;
			font-size: 18px;
			margin-top: -4px;
		}
		
		.navbar-fixed-top {
			top: 0;
			border-width: 0 0 0px;
		}
		
		body {
			background: #fff !important;
			color: black;
		}
		
		<!--
		auto complete
		
		-->
		@charset "utf-8";
		.ui-autocomplete {
			z-index: 1000 !important;
			cursor: default;
			list-style: none;
		}
		
		.ui-widget {
		}
		
		.ui-autocomplete {
			overflow-y: auto;
			overflow-x: hidden;
		}
		
		.ui-menu {
			width: 0px;
			display: none;
		}
		
		.ui-autocomplete > li {
			padding: 10px;
			padding-left: 10px;
		}
		
		ul {
			margin-bottom: 0;
		}
		
		.ui-autocomplete > li.ui-state-focus {
			background-color: #DDD;
		}
		
		.ui-autocomplete .ui-front .ui-menu .ui-widget .ui-widget-content {
			border: 0;
		}
		
		.ui-helper-hidden-accessible {
			display: none;
		}
		
		.gobumpr-icon {
			font-style: normal;
			font-weight: normal;
			speak: none;
			display: inline-block;
			text-decoration: inherit;
			text-align: center;
			font-variant: normal;
			text-transform: none;
			-webkit-font-smoothing: antialiased;
			-moz-osx-font-smoothing: grayscale;
		}
		
		.ui-widget {
			background-color: white;
			width: 100%;
		}
		
		.ui-widget-content {
			padding-left: 1px;
			display: block;
			width: 20px;
			position: relative;
			line-height: 12px;
			max-height: 210px;
			border: .5px solid #DADADA;
		}
		
		.ui-widget {
		}
		
		.ui-autocomplete {
			position: absolute;
			cursor: default;
		}
		
		
		<!--
		vehicle type
		
		-->
		.bike,
		.car {
			cursor: pointer;
			user-select: none;
			-webkit-user-select: none;
			-webkit-touch-callout: none;
		}
		
		.bike > input,
		.car > input { /* HIDE ORG RADIO & CHECKBOX */
			visibility: hidden;
			position: absolute;
		}
		
		/* RADIO & CHECKBOX STYLES */
		.bike > i,
		.car > i { /* DEFAULT <i> STYLE */
			display: inline-block;
			vertical-align: middle;
			width: 16px;
			height: 16px;
			border-radius: 50%;
			transition: 0.2s;
			box-shadow: inset 0 0 0 8px #fff;
			border: 1px solid gray;
			background: gray;
		}
		
		label > input { /* HIDE RADIO */
			visibility: hidden; /* Makes input not-clickable */
			position: absolute; /* Remove input from document flow */
		}
		
		label > input + img { /* IMAGE STYLES */
			cursor: pointer;
			border: 2px solid transparent;
		}
		
		label > input:checked + img { /* (RADIO CHECKED) IMAGE STYLES */
			border-radius: 12px;
			padding: 5px;
			background-color: #ffa800;
			box-shadow: 0 0 3px 0 #394;
		}
		
		.borderless td, .borderless th {
			border: none !important;
		}
		
		/* anchor tags */
		a {
			text-decoration: none;
			color: black;
		}
		
		a:hover {
			text-decoration: none;
			color: #4B436A;
		}
		
		.datepicker {
			cursor: pointer;
			z-index: 9999 !important;
		}
		
		.datepicker:before {
			content: '';
			display: inline-block;
			border-left: 7px solid transparent;
			border-right: 7px solid transparent;
			border-bottom: 7px solid #ccc;
			border-bottom-color: transparent !important;
			position: absolute;
			top: -7px;
			left: 190px;
		/ / I made a change here
		}
		
		.datepicker:after {
			content: '';
			display: inline-block;
			border-left: 6px solid transparent;
			border-right: 6px solid transparent;
			border-top-color: transparent !important;
			border-top: 6px solid #ffffff;
			position: absolute;
			bottom: -6px;
			left: 191px;
		/ / I made a change here
		}
		
		#datepick > span:hover {
			cursor: pointer;
		}
		
		.floating-box {
			display: inline-block;
			margin: 22px;
			padding: 22px;
			width: 203px;
			height: 105px;
			box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
			font-size: 17px;
		}
		
		/* vertical menu */
		/* Mixin */
		@mixin vertical-align($position: relative) {
			position: $ position;
			top: 50%;
			-webkit-transform: translateY(-50%);
			-ms-transform: translateY(-50%);
			transform: translateY(-50%);
		}
		
		.ver_menu {
		@include vertical-align();
		}
		
		select {
			color: red;
		}
		
		option {
			height: 25px;
		}
		
		option:hover {
			box-shadow: 0 0 10px 100px #ddd inset;
		}
		
		.eupraxia.btn {
			padding: 2px 12px;
		}
	</style>

</head>
<body>
<?php include_once("header.php"); ?>
<script>
    $(document).ready(function () {
        $('#city').hide();
    });

    function getmasg(argument) {
        alert('you are not a authorized person');
    }
</script>
<div class="overlay" data-sidebar-overlay></div>

<div class="padding"></div>
<?php
// get user id  from user_booking_table
$sql_user_bk = "SELECT user_id,user_veh_id,user_vech_no,booking_status,rnr_flag,flag,service_description,pickup_full_address,log,pickup_date_time,LOWER(pick_up) as pick_up,source,initial_service_type FROM user_booking_tb WHERE booking_id='$booking_id' ";
$res_user_bk = mysqli_query($conn, $sql_user_bk);
$row_user_bk = mysqli_fetch_object($res_user_bk);
$user_id = $row_user_bk->user_id;
$veh_id = $row_user_bk->user_veh_id;
$log = $row_user_bk->log;

$reg_no = $row_user_bk->user_vech_no;
$status = $row_user_bk->booking_status;
$rnr_flag = $row_user_bk->rnr_flag;
$flag = $row_user_bk->flag;
$pickup_fulladdress = $row_user_bk->pickup_full_address;
$pickup_datetime = $row_user_bk->pickup_date_time;
$service_description = $row_user_bk->service_description;
$source = $row_user_bk->source;
$pickup_flag = $row_user_bk->pick_up;
$initial_service_type = $row_user_bk->initial_service_type;
if ($pickup_fulladdress == "") {
	$pickup_fulladdress = "-";
}

if ($pickup_datetime == "" || $pickup_flag == '0' || $pickup_flag == 'no') {
	$pickup_datetime = "-";
} else {
	$pickup_datetime = date('d-m-Y h:i a', strtotime($pickup_datetime));
}

if ($service_description == "") {
	$service_description = "-";
}

if ($status == 1 && $rnr_flag == 1 && $flag != 1) {
	if ($type == 'or') {
		$type = 'or';
	} elseif ($type == 'ol') {
		$type = 'ol';
	} else {
		$type = 'ur';
	}
}
if ($status == 6 && $rnr_flag == 2) {
	if ($type == 'or') {
		$type = 'or';
	} elseif ($type == 'ol') {
		$type = 'ol';
	} else {
		$type = 'ur';
	}
}

// get vehicle details from user_vehicle_table
$sql_user_veh = "SELECT v.reg_no,v.brand,v.model,v.flag,v.freedom_pass,f.log FROM user_vehicle_table v LEFT JOIN freedom_pass_booking f ON f.user_veh_id = v.id AND f.flag = 0 WHERE v.id='$veh_id'";
$res_user_veh = mysqli_query($conn, $sql_user_veh);
$row_user_veh = mysqli_fetch_object($res_user_veh);

$veh_number = $row_user_veh->reg_no;
$veh_brand = $row_user_veh->brand;
$veh_model = $row_user_veh->model;
$veh_flag = $row_user_veh->flag;
$freedom_pass = $row_user_veh->freedom_pass;
$start_date = $row_user_veh->log;
$valid_till = date('d-M-Y', strtotime('+ 1 year', strtotime($start_date)));


// get user details from user register table using user id
$sql_user = "SELECT name,email_id,mobile_number,mobile_number2,lat_lng,Locality_Home,referral_count FROM user_register where reg_id='$user_id' ";
$res_user = mysqli_query($conn, $sql_user);
$row_user = mysqli_fetch_object($res_user);

$user_name = $row_user->name;
$user_mail = $row_user->email_id;
$user_mobile = $row_user->mobile_number;
$user_alt_mobile = $row_user->mobile_number2;
$user_location = $row_user->lat_lng;
if ($user_location == '') {
	$user_location = $row_user->Locality_Home;
}
$referral = $row_user->referral_count;

?>
<script>
    $(document).ready(function () {
        // $('#city').hide();
        var veh_type = $('[name="veh_be"]:checked').val();
        var source = '<?php echo $source;?>';
        console.log(source);
        if (veh_type === '2w') {
            if (source != 'Hub Booking')
                $("#service_typee").val("");
        }

    })
</script>
<div id="user"
	 style=" margin-left:20px;border:2px solid #708090; border-radius:8px; width:27%;height:490px; padding:20px; margin-top:18px; float:left;overflow-y:auto;">
	<table id="table1" class="table borderless">
		<tr>
			<td><strong>User Id</strong></td>
			<td><?php echo $user_id; ?></td>
		</tr>
		<tr>
			<td><strong>Name</strong></td>
			<td><?php echo $user_name; ?></td>
		</tr>
		<tr>
			<td><strong>Phn No.</strong></td>
			<td>
				<?php
				if ($_SESSION['eupraxia_flag']) { ?>
					<button id="start_call" class="eupraxia btn btn-sm"
							onclick="startCallEupraxia(<?php echo $user_id; ?>, <?php echo $booking_id; ?>);"
							style="background-color:green; color:#fff;">
						<i class="fa fa-phone" aria-hidden="true"></i>&nbsp;Call
					</button>
					
					<button id="end_call" class="eupraxia btn btn-sm"
							onclick="endCallEupraxia(<?php echo $user_id; ?>, <?php echo $booking_id; ?>);"
							style="background-color:red; color:#fff;" disabled>
						<i class="fa fa-phone" aria-hidden="true"></i>&nbsp;End
					</button>
				<?php } else {
					echo $user_mobile;
				}
				?>
			</td>
		</tr>
		<tr>
			<td><strong>Alt Phn No.</strong></td>
			<td>
				<?php
				if ($_SESSION['eupraxia_flag']) {
					echo $user_alt_mobile;
				}
				?>
			</td>
		</tr>
		<tr>
			<td><strong>E-mail</strong></td>
			<td><?php echo $user_mail; ?></td>
		</tr>
		<tr>
			<td><strong>Veh No.</strong></td><?php if ($veh_flag == '0') { ?>
				<td style="color:red;"><?php echo $reg_no; ?></td> <?php } else { ?>
				<td><?php echo $reg_no; ?></td> <?php } ?></tr>
		<tr>
			<td><strong>Brand</strong></td><?php if ($veh_flag == '0'){ ?>
				<td style="color:red;"><?php echo $veh_brand; ?>,<?php echo $veh_model; ?></td> <?php } else{ ?>
			<td><?php echo $veh_brand; ?>,<?php echo $veh_model;
				}
				if ($freedom_pass == 1) { ?> <i class="fa fa-gift" style=" font-size: x-large;color: #ffa800;"
												aria-hidden="true"></i><br><?php echo '[' . $valid_till . ']';
				} ?></td>
		</tr>
		<tr>
			<td><strong>Current Location<strong></td>
			<td><?php echo $user_location; ?></td>
		</tr>
		<tr>
			<td><strong>Referrals Made<strong></td>
			<td><?php echo $referral; ?></td>
		</tr>
		<tr>
			<td><strong>Initial Service Type<strong></td>
			<td><?php echo $initial_service_type; ?></td>
		</tr>
	</table>
	
	
	<?php
	$sql_get_crm = "SELECT crm_allocate_id FROM user_booking_tb WHERE booking_id ='$booking_id' ";
	$get_crm_id = mysqli_query($conn, $sql_get_crm);
	
	$assigned_crm_id = mysqli_fetch_object($get_crm_id);
	$allotted_crm = $assigned_crm_id->crm_allocate_id;
	//echo '$allotted_crm'.$allotted_crm;
	
	//print_r($assigned_crm_id);
	?>
	
	<!-- Edit User -->
	<?php
	
	if (($allotted_crm == $crm_log_id) || ($admin_flag == 1) || ($crm_se == 1) || $crm_log_id == 'crm036' || $crm_log_id == 'crm003' || $crm_log_id == 'crm020' || $crm_log_id == 'crm043' || $crm_log_id == 'crm158' || $crm_log_id == 'crm032' || $crm_log_id == 'crm160' || $crm_log_id == 'crm123' || $crm_log_id == 'crm135' || $crm_log_id == 'crm017' || $crm_log_id == 'crm161' || $crm_log_id == 'crm175' || $crm_log_id == 'crm187' || $crm_log_id == 'crm193' || $crm_log_id == 'crm178' || $crm_log_id == 'crm174' || $crm_log_id == 'crm018' || $crm_log_id == 'crm158' || $crm_log_id == 'crm227' || $crm_log_id == 'crm202' || $crm_log_id == 'crm239' || $crm_log_id == 'crm237' || $crm_log_id == 'crm225' || $crm_log_id == 'crm261' || $crm_log_id == 'crm263' || $crm_log_id == 'crm073' || $crm_log_id == 'crm108' || $crm_log_id == 'crm189' || $crm_log_id == 'crm189' || is_null($allotted_crm)) {
		
		?>
		<div style="float:left; margin-left:30px;">
			<div id="edting_user" style="display:inline-block; align-items:center;">
				<!-- Trigger the modal with a button -->
				<button id="eu" type="button" class="btn btn-sm" data-toggle="modal" data-target="#myModal_user"
						style="background-color:rgb(156, 197, 202);"><i class="fa fa-pencil-square-o"
																		aria-hidden="true"></i>&nbsp;&nbsp;Edit User
				</button>
				
				<!-- Modal -->
				<div class="modal fade" id="myModal_user" role="dialog">
					<div class="modal-dialog" style="width:860px;">
						
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h3 class="modal-title">Edit User</h3>
							</div>
							<div class="modal-body">
								<?php
								$sql_edit_user = "SELECT name,mobile_number,mobile_number2,email_id,City,Locality_Home,Locality_Work,source,campaign,Last_service_date,Next_service_date,Last_called_on,Follow_up_date,comments FROM user_register WHERE reg_id='$user_id'";
								$res_edit_user = mysqli_query($conn, $sql_edit_user);
								$row_edit_user = mysqli_fetch_object($res_edit_user);
								$u_name = $row_edit_user->name;
								$u_mobile = $row_edit_user->mobile_number;
								$u_alt_mobile = $row_edit_user->mobile_number2;
								$u_mail = $row_edit_user->email_id;
								$u_city = $row_edit_user->City;
								$u_loc_home = $row_edit_user->Locality_Home;
								$u_loc_work = $row_edit_user->Locality_Work;
								$u_source = $row_edit_user->source;
								$u_campaign = $row_edit_user->campaign;
								$u_last_serviced = $row_edit_user->Last_service_date;
								$u_next_serviced = $row_edit_user->Next_service_date;
								$u_last_called = $row_edit_user->Last_called_on;
								$u_followup = $row_edit_user->Follow_up_date;
								$u_comments = $row_edit_user->comments;
								?>
								<form id="edit_user" class="form" method="post" action="edit_user.php">
									<div class="row">
										<br>
										<div class="col-xs-4 col-xs-offset-1 form-group">
											<input class="form-control" type="text" id="mobile" name="mobile"
												   placeholder="Mobile" readonly maxlength="10"
												   value="<?php echo $u_mobile; ?>">
										</div>
										
										<div class="col-xs-4 form-group">
											<input class="form-control" type="text" id="mobile2" name="mobile2"
												   placeholder="Alternate Mobile" maxlength="10"
												   value="<?php echo $u_alt_mobile; ?>">
										</div>
									</div>
									
									<div class="row">
										
										<div class="col-xs-5 col-xs-offset-1 form-group">
											<input class="form-control" type="text" id="user_name" name="user_name"
												   pattern="^[a-zA-Z0-9\s]+$"
												   onchange="try{setCustomValidity('')}catch(e){}"
												   placeholder="User Name" required value="<?php echo $u_name; ?>">
										</div>
										
										<div class="col-xs-5 form-group">
											<input type="email" class="form-control" id="email" name="email"
												   placeholder="E-Mail" value="<?php echo $u_mail; ?>">
										</div>
									</div>
									<div class="row"></div>
									<div class="row">
										<div id="city_u" class="col-xs-4 col-xs-offset-1 form-group">
											<div class="ui-widget">
												<select class="form-control" id="cityu" name="city" required>
													<?php
													
													$sql_city = "SELECT DISTINCT city FROM go_bumpr.localities ORDER BY city ASC";
													$res_city = mysqli_query($conn, $sql_city);
													while ($row_city = mysqli_fetch_object($res_city)) {
														?>
														<option value="<?php echo $row_city->city; ?>"><?php echo $row_city->city; ?></option>
														<?php
													}
													?>
												
												</select>
												<script>
                                                    $(document).ready(function () {
                                                        var city = '<?php echo $u_city; ?>';
                                                        $('#cityu option[value=' + city + ']').attr('selected', 'selected');
                                                    })
												</script>
											</div>
										</div>
										<div id="loc_home" class="col-xs-3 form-group">
											<div class="ui-widget" id="loc_home">
												<input class="form-control autocomplete" id="location_home" type="text"
													   name="location_home" placeholder="Home Locality"
													   value="<?php echo $u_loc_home; ?>">
											</div>
										</div>
										
										<div id="loc_work" class="col-xs-3 form-group">
											<div class="ui-widget" id="loc_work">
												<input class="form-control autocomplete" id="location_work" type="text"
													   name="location_work" placeholder="Work Locality"
													   value="<?php echo $u_loc_work; ?>">
											</div>
										</div>
									</div>
									<div class="row"></div>
									<div class="row">
										<div id="source_u" class="col-xs-5 col-xs-offset-1 form-group">
											<div class="ui-widget">
												<select class="form-control" id="source" name="source">
													<option value="<?php echo $u_source; ?>"
															selected><?php echo $u_source; ?></option>
													<?php
													$sql_sources = "SELECT user_source FROM user_source_tbl WHERE flag='0' AND user_source !='$u_source' ORDER BY  user_source ASC";
													$res_sources = mysqli_query($conn, $sql_sources);
													while ($row_sources = mysqli_fetch_object($res_sources)) {
														$source_name = $row_sources->user_source;
														?>
														<option value="<?php echo $source_name; ?>"><?php echo $source_name; ?></option>
														<?php
													}
													?>
												</select>
											</div>
										</div>
										
										<div id="cam" class="col-xs-5 form-group">
											<div class="ui-widget">
												<input class="form-control" id="campaign" type="text" name="campaign"
													   placeholder="Campaign" value="<?php echo $u_campaign; ?>">
											</div>
										</div>
									</div>
									<div class="row"></div>
									<div class="row">
										
										<div class="col-xs-2 col-xs-offset-1 form-group">
											<label> Last Serviced</label></div>
										<div class="col-xs-3  form-group">
											<?php
											if ($u_last_serviced == $today) {
												?>
												<input class="form-control datepicker" data-date-format='dd-mm-yyyy'
													   type="text" id="last_service_date" name="last_service_date"
													   value="<?php echo date('d-m-Y', strtotime($u_last_serviced)); ?>">
											<?php } else {
												?>
												<input class="form-control" data-date-format='dd-mm-yyyy' type="text"
													   id="last_service_date" name="last_service_date"
													   value="<?php echo date('d-m-Y', strtotime($u_last_serviced)); ?>"
													   readonly>
											<?php } ?>
										</div>
										<div class="col-xs-2 form-group">
											<label> Next Service On</label>
										</div>
										<div class="col-xs-3  form-group">
											<input class="form-control datepicker" data-date-format='dd-mm-yyyy'
												   type="text" id="next_service_date" name="next_service_date"
												   value="<?php echo date('d-m-Y', strtotime($u_next_serviced)); ?>">
										</div>
									</div>
									<div class="row"></div>
									<div class="row">
										<div class="col-xs-2 col-xs-offset-1 form-group">
											<label> Last Called On</label>
										</div>
										<div class="col-xs-3  form-group">
											<?php
											if ($u_last_called == $today) {
												?>
												<input class="form-control datepicker" data-date-format='dd-mm-yyyy'
													   type="text" id="last_called_on" name="last_called_on"
													   value="<?php echo date('d-m-Y', strtotime($u_last_called)); ?>">
											<?php } else {
												?>
												<input class="form-control" data-date-format='dd-mm-yyyy' type="text"
													   id="last_called_on" name="last_called_on"
													   value="<?php echo date('d-m-Y', strtotime($u_last_called)); ?>"
													   readonly>
											<?php } ?>
										</div>
										<div class="col-xs-2 form-group">
											<label> FollowUp Date</label>
										</div>
										<div class="col-xs-3  form-group">
											<input class="form-control datepicker" data-date-format='dd-mm-yyyy'
												   type="text" id="follow_up_date" name="follow_up_date"
												   value="<?php echo date('d-m-Y', strtotime($u_followup)); ?>">
										</div>
									</div>
									<div class="row"></div>
									<div class="row">
										<div class="col-xs-10 col-xs-offset-1 form-group">
											<textarea class="form-control" maxlength="100" id="comments" name="comments"
													  placeholder="Comments..."><?php echo $u_comments; ?></textarea>
										</div>
									</div>
									<div class="row"></div>
									<div class="row">
										<br>
										<div class="form-group" align="center">
											<input class="form-control" type="submit" id="edit_user_submit"
												   name="edit_user_submit" value="Update"
												   style=" width:90px; background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
										</div>
									</div>
									<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>">
									<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>">
									<input type="hidden" id="type" name="type" value="<?php echo $type; ?>">
									
									<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>">
								</form>
							</div> <!-- modal body -->
						</div> <!-- modal content -->
					</div>  <!-- modal dailog -->
				</div>  <!-- modal -->
			
			</div>
		</div> <!-- edit user -->
		<?php
	}
	?>
	
	
	<div style="float:left; margin-left:30px;">
		<!-- Add Vehicle -->
		<!-- Trigger the modal with a button -->
		<?php
		if (($allotted_crm == $crm_log_id) || ($admin_flag == 1) || ($crm_se == 1) || $crm_log_id == 'crm036' || $crm_log_id == 'crm003' || $crm_log_id == 'crm020' || $crm_log_id == 'crm043' || $crm_log_id == 'crm158' || $crm_log_id == 'crm032' || $crm_log_id == 'crm160' || $crm_log_id == 'crm123' || $crm_log_id == 'crm135' || $crm_log_id == 'crm017' || $crm_log_id == 'crm161' || $crm_log_id == 'crm175' || $crm_log_id == 'crm187' || $crm_log_id == 'crm193' || $crm_log_id == 'crm178' || $crm_log_id == 'crm174' || $crm_log_id == 'crm018' || $crm_log_id == 'crm158' || $crm_log_id == 'crm227' || $crm_log_id == 'crm202' || $crm_log_id == 'crm239' || $crm_log_id == 'crm237' || $crm_log_id == 'crm225' || $crm_log_id == 'crm261' || $crm_log_id == 'crm263' || $crm_log_id == 'crm073' || $crm_log_id == 'crm108' || $crm_log_id == 'crm189' || is_null($allotted_crm)) {
			//echo 'allotted_crm: '. $allotted_crm.'crm_log_id:'.$crm_log_id;
			?>
			
			<div id="edting_vehicle" style="display:inline-block; align-items:center;">
				<button data-bid="<?php echo $booking_id; ?>" type="button" class="btn btn-md" data-toggle="modal"
						data-target="#myModal_vehicle" style="background-color:rgb(156, 197, 202);"><i class="fa fa-car"
																									   aria-hidden="true"></i>&nbsp;&nbsp;Add
					Vehicle
				</button>
			</div>
			<?php
		}
		?>
	
	</div>
</div>

<div style=" margin-top:19px; margin-left:14px; width:46%; max-height:420px; overflow-y:auto; float:left;">
	<table id="table2" class="table table-bordered table-responsive " style="border-collapse:collapse; ">
		<thead style="background-color: #D3D3D3;">
		<th>No</th>
		<th>Activity</th>
		<th>ServiceLogDate</th>
		<th>Support</th>
		<th>Service</th>
		</thead>
		<tbody>
		<?php
		$reg_array = array();
		
		//echo "SELECT * FROM user_booking_tb WHERE user_id='$user_id' ORDER by log DESC";
		$sql1 = mysqli_query($conn, "SELECT * FROM user_booking_tb WHERE user_id='$user_id' ORDER by log DESC");
		while ($row1 = mysqli_fetch_object($sql1)) {
			$bking_id = $row1->booking_id;
			if (($row1->flag == 1) && ($row1->flag_unwntd == 1) && ($row1->activity_status == '')) {
				continue;
			}
			$reg_array[] = $row1;
			
			// if($row1->axle_flag==1)
			// {
			$sql6 = mysqli_query($conn, "SELECT b.gb_booking_id,g.sent_log as log,m.b2b_shop_name,u.service_type,s.b2b_acpt_flag,s.b2b_deny_flag,u.crm_update_id FROM b2b.b2b_booking_tbl b LEFT JOIN user_booking_tb u ON b.gb_booking_id = u.booking_id LEFT JOIN b2b.b2b_mec_tbl as m ON b.b2b_shop_id = m.b2b_shop_id  LEFT JOIN b2b.b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id  join go_bumpr.goaxle_track as g on g.b2b_booking_id = b.b2b_booking_id WHERE b.gb_booking_id ='$bking_id'");
			//$sql6=mysqli_query($conn,"SELECT g.go_booking_id,g.b2b_shop_id,g.sent_log as log,b.b2b_shop_name,u.service_type FROM goaxle_track g,b2b.b2b_mec_tbl b,user_booking_tb u WHERE g.b2b_shop_id = b.b2b_shop_id AND g.go_booking_id = '$bking_id' AND u.booking_id = '$bking_id'");
			while ($row6 = mysqli_fetch_object($sql6)) {
				$reg_array[] = $row6;
			}
			
			
			//}
			
			// else
			// {
			
			$sql8 = mysqli_query($conn, "SELECT g.go_booking_id,g.b2b_shop_id,g.sent_crm_id,g.undo_crm_id,g.undo_log as log,m.b2b_shop_name,u.service_type,g.undo_log,b2b.b2b_acpt_flag,b2b.b2b_deny_flag FROM go_bumpr.goaxle_track g LEFT JOIN b2b.b2b_mec_tbl as m ON g.b2b_shop_id = m.b2b_shop_id LEFT JOIN user_booking_tb u ON g.go_booking_id = u.booking_id join b2b.b2b_status b2b on b2b.b2b_booking_id=g.b2b_booking_id  where g.go_booking_id='$bking_id'");
			
			while ($row8 = mysqli_fetch_object($sql8)) {
				$reg_array[] = $row8;
			}
			//}
		}
		$sql2 = mysqli_query($conn, "SELECT * FROM user_register WHERE reg_id='$user_id' ORDER by log DESC");
		while ($row2 = mysqli_fetch_object($sql2)) {
			$reg_array[] = $row2;
		}
		
		$sql3 = mysqli_query($conn, "SELECT * FROM user_vehicle_table WHERE user_id='$user_id' ORDER by log DESC");
		while ($row3 = mysqli_fetch_object($sql3)) {
			$reg_array[] = $row3;
		}
		$sql4 = mysqli_query($conn, "SELECT * FROM user_activity_tbl WHERE user_id='$user_id' ORDER by log DESC");
		$status_count = mysqli_num_rows($sql4);
		while ($row4 = mysqli_fetch_object($sql4)) {
			$reg_array[] = $row4;
		}
		$sql5 = mysqli_query($conn, "SELECT c.*,c.Follow_up_date AS cmnt_followup_date,b.rating as rating,b.feedback as feedback FROM admin_comments_tbl as c LEFT JOIN user_booking_tb as b ON c.book_id=b.booking_id WHERE c.user_id='$user_id' ORDER by c.log DESC");
		$status_comments = mysqli_num_rows($sql5);
		while ($row5 = mysqli_fetch_object($sql5)) {
			$reg_array[] = $row5;
		}
		//$sql7=mysqli_query($conn,"SELECT override_id,reason,later_rsn,status as override_status,override_later,comments,later_cmnts,booking_id AS o_booking_id,log,later_log,update_log FROM override_tbl as o WHERE o.user_id='$user_id' ORDER by o.log DESC");
		//while($row7=mysqli_fetch_object($sql7)){
		//$reg_array[]=$row7;
		//}
		function do_compare($item1, $item2)
		{
			$ts1 = strtotime($item1->log);
			$ts2 = strtotime($item2->log);
			return $ts2 - $ts1;
		}
		
		usort($reg_array, 'do_compare');
		
		$arr_count = count($reg_array);
		
		$x = 0;
		$axle = false;
		for ($i = 0; $i < $arr_count; $i++) {
			$x = $x + 1;
			if (isset($reg_array[$i]->gb_booking_id)) {
				$activity = '';
				$accept = $reg_array[$i]->b2b_acpt_flag;
				$deny = $reg_array[$i]->b2b_deny_flag;
				if ($accept == 1 && $deny == 0) {
					// $activity = $activity.'GoAxled to <p style="background-color:#6FA3DE;padding:7px;width:30px;border-radius:18px;float:left;margin-right:3px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
					$activity = $activity . 'GoAxled to <p style="background-color:#6FA3DE;padding:7px;width:30px;border-radius:18px;float:left;margin-right:3px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
				} else if ($accept == 0 && $deny == 0) {
					// $activity = $activity.'GoAxled to <p style="background-color:#69ED85;padding:7px;width:30px;border-radius:18px;float:left;margin-right:3px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
					$activity = $activity . 'Goaxled to <p style="background-color:#69ED85;padding:7px;width:30px;border-radius:18px;float:left;margin-right:3px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
				}
				//  else if($accept==0 && $deny==1){
				//   $activity = $activity.'Rejected by <p style="background-color:#D25F34;padding:7px;width:30px;border-radius:18px;float:left;margin-right:3px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
				
				// }
				$activity = $activity . $reg_array[$i]->b2b_shop_name . " - " . $reg_array[$i]->gb_booking_id;
				$support = $reg_array[$i]->crm_update_id;
				$service_type = $reg_array[$i]->service_type;
				if ($reg_array[$i]->log == '0000-00-00 00:00:00')
					$log_date = date("d-m-Y h:i a");
				else
					$log_date = date("d-m-Y h:i a", strtotime($reg_array[$i]->log));
			} else if (isset($reg_array[$i]->go_booking_id)) {
				
				$activity = '';
				$accept = $reg_array[$i]->b2b_acpt_flag;
				$deny = $reg_array[$i]->b2b_deny_flag;
				if ($accept == 1 && $deny == 0) {
					
					if ($reg_array[$i]->undo_log != '') {
						$activity = $activity . 'Reverted From <p style="background-color:#6FA3DE;padding:7px;width:30px;border-radius:18px;float:left;margin-right:3px;"><i class="fa fa-reply" aria-hidden="true" ></i></p>';
					}
					// else
					// {
					
					//       $activity = $activity.'GoAxled to <p style="background-color:#6FA3DE;padding:7px;width:30px;border-radius:18px;float:left;margin-right:3px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
					//     }
					
				} else if ($accept == 0 && $deny == 0) {
					if ($reg_array[$i]->undo_log != '') {
						$activity = $activity . 'Reverted From <p style="background-color:#69ED85;padding:7px;width:30px;border-radius:18px;float:left;margin-right:3px;"><i class="fa fa-reply" aria-hidden="true" ></i></p>';
						$support = $reg_array[$i]->undo_crm_id;
					}
					// else
					// {
					// $activity = $activity.'Goaxled to <p style="background-color:#69ED85;padding:7px;width:30px;border-radius:18px;float:left;margin-right:3px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
					// }
					
				} else if ($accept == 0 && $deny == 1) {
					$activity = $activity . 'Rejected by <p style="background-color:#D25F34;padding:7px;width:30px;border-radius:18px;float:left;margin-right:3px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
				}
				
				$name_undo = $reg_array[$i]->undo_crm_id;
				$undocrm_name = mysqli_query($conn, "SELECT name FROM crm_admin WHERE crm_log_id = '$name_undo' ");
				$row_undo_name = mysqli_fetch_array($undocrm_name);
				$undo_name = $row_undo_name['name'];
				
				if ($reg_array[$i]->undo_log != '') {
					$undo_date = '<br><strong>Revert By</strong>- ' . $undo_name . ' ' . date("d-M-Y h:i a", strtotime($reg_array[$i]->undo_log));
					$support = $reg_array[$i]->undo_crm_id;
				} else {
					$undo_date = "";
				}
				
				
				$activity = $activity . $reg_array[$i]->b2b_shop_name . " - " . $reg_array[$i]->go_booking_id;
				
				$service_type = $reg_array[$i]->service_type;
				if ($reg_array[$i]->log == '0000-00-00 00:00:00')
					$log_date = date("d-m-Y h:i a");
				else
					$log_date = date("d-m-Y h:i a", strtotime($reg_array[$i]->log));
			} elseif (isset($reg_array[$i]->booking_id)) {
				$activity = 'BOOKING -' . $reg_array[$i]->booking_id . ' -' . $reg_array[$i]->shop_name;
				$service_type = $reg_array[$i]->service_type;
				$support = $reg_array[$i]->crm_update_id;
				$log_date = date("d-m-Y h:i a", strtotime($reg_array[$i]->log));
				
			} elseif (isset($reg_array[$i]->reg_id)) {
				
				$activity = 'USER REGISTER -' . $reg_array[$i]->reg_id . '-' . $reg_array[$i]->name . '-' . $reg_array[$i]->email_id . '-' . $reg_array[$i]->mobile_number;
				$service_type = '';
				$support = $reg_array[$i]->crm_log_id;
				$log_date = date("d-m-Y h:i a", strtotime($reg_array[$i]->log));
			} elseif (isset($reg_array[$i]->vehicle_id)) {
				
				$activity = 'VEHICLE REGISTER -' . $reg_array[$i]->brand . '-' . $reg_array[$i]->model . '-' . $reg_array[$i]->type;
				$service_type = '';
				$support = $reg_array[$i]->crm_log_id;
				$log_date = date("d-m-Y h:i a", strtotime($reg_array[$i]->log));
			}
			// elseif(isset($reg_array[$i]->override_id)){
			
			// 	$o_status = $reg_array[$i]->override_status;
			// 	$later_rsn = $reg_array[$i]->later_rsn;
			// 	$later_log = $reg_array[$i]->later_log;
			// 	$update_log = $reg_array[$i]->update_log;
			// 	//$log = $reg_array[$i]->log;
			// 	$cmnts=$reg_array[$i]->comments;
			// 	$later_cmnts=$reg_array[$i]->later_cmnts;
			// 	$o_booking_id = $reg_array[$i]->o_booking_id;
			// 	$o_later=$reg_array[$i]->override_later;
			// 	$rsn=$reg_array[$i]->reason;
			
			// 	if($o_status == "2"){
			// 		$activity =  'Override- '.$o_later.' - '.$o_booking_id.'<br>'.$later_rsn.'-'.$later_cmnts;
			// 		$service_type = '';
			// 	}
			// 	else if($o_status == "1"){
			// 				$activity = 'Override- '.$o_later.' - '.date("d/m/Y",strtotime($later_log)).'-'.$o_booking_id.'<br>'.$later_rsn.'-'.$later_cmnts;
			// 				$service_type = '';
			// 	}
			// 	else if(($o_status == '0' || $o_status=='3') && $later_rsn != 'none'){
			// 		$activity =$o_booking_id." Again - Pushed to override -".$rsn."-".$cmnts."<br> Previous:";
			// 		$activity = $activity.'Override-' .$o_later. '-'.date("d/m/Y",strtotime($later_log)).'-'.$later_rsn.'-<br>'.$later_cmnts;
			// 		$service_type = '';
			//  }else {
			// 		$activity ="For internal reference - Pushed to override -".$rsn."-<br>".$cmnts."- ".$o_booking_id;
			// 		$service_type = '';
			//  }
			
			// 		$support= $reg_array[$i]->crm_log_id;
			// $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->update_log));
			// $service_type = '';
			// 		}
			elseif (isset($reg_array[$i]->user_activity)) {
				if ($status_count <= 1) {
					
					$activity = 'User Activity-' . 'Prospect to ' . $reg_array[$i]->user_activity;
					$service_type = '';
					$support = $reg_array[$i]->crm_log_id;
					$log_date = date("d-m-Y h:i a", strtotime($reg_array[$i]->log));
				} else {
					
					$activity = 'User Activity-' . $reg_array[$i]->user_activity;
					$service_type = '';
					$support = $reg_array[$i]->crm_log_id;
					$log_date = date("d-m-Y h:i a", strtotime($reg_array[$i]->log));
					
				}
			} elseif (isset($reg_array[$i]->com_id)) {
				$cmt = $reg_array[$i]->comments;
				$support = $reg_array[$i]->crm_log_id;
				if ($reg_array[$i]->cmnt_followup_date != '0000-00-00') {
					$followup_on = '<br><strong>Next Follow Up On</strong>- ' . date("d-M-Y", strtotime($reg_array[$i]->cmnt_followup_date));
				} else {
					$followup_on = "";
				}
				if ($cmt == '') {
					$cmt = "";
				}
				$act_status = $reg_array[$i]->status;
				if ($act_status == "") {
					$act_status = "FollowUp";
				}
				if ($act_status == "Cancelled") {
					$activity = $reg_array[$i]->category;
					if ($cmt != '') {
						$activity = $activity . '-' . $cmt;
					}
					$activity = $activity . $followup_on;
					$log_date = date("d-m-Y h:i a", strtotime($reg_array[$i]->log));
				} else if ($act_status == "Reverted") {
					$activity = $reg_array[$i]->category . $followup_on;
					$log_date = date("d-m-Y h:i a", strtotime($reg_array[$i]->log));
				} else if ($act_status == "Reverted Back") {
					$revertback = '<strong>Override Reverted Without Any Action</strong>';
					$activity = $revertback . $reg_array[$i]->category;
					$log_date = date("d-m-Y h:i a", strtotime($reg_array[$i]->log));
				} else if ($act_status == "FeedBack") {
					$activity = '';
					$rating = $reg_array[$i]->rating;
					$feedback = $reg_array[$i]->feedback;
					$activity = $activity . $reg_array[$i]->category;
					if ($cmt != '') {
						$activity = $activity . '<br><strong>Comments</strong>-' . $cmt;
					}
					$activity = $activity . $followup_on;
					if ($rating > 0) {
						$activity = $activity . '<br><strong>Rating</strong>-';
						switch ($rating) {
							case '1':
								$activity = $activity . '<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i>';
								break;
							case '2':
								$activity = $activity . '<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i>';
								break;
							case '3':
								$activity = $activity . '<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i>';
								break;
							case '4':
								$activity = $activity . '<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i>';
								break;
							case '5':
								$activity = $activity . '<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i>';
								break;
							case '0.5':
								$activity = $activity . '<i class="fa fa-star-half" aria-hidden="true" style="color:#ffa800;"></i>';
								break;
							case '1.5':
								$activity = $activity . '<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star-half" aria-hidden="true" style="color:#ffa800;"></i>';
								break;
							case '2.5':
								$activity = $activity . '<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star-half" aria-hidden="true" style="color:#ffa800;"></i>';
								break;
							case '3.5':
								$activity = $activity . '<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star-half" aria-hidden="true" style="color:#ffa800;"></i>';
								break;
							case '4.5':
								$activity = $activity . '<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star-half" aria-hidden="true" style="color:#ffa800;"></i>';
								break;
						}
						
					}
					$log_date = date("d-m-Y h:i a", strtotime($reg_array[$i]->log));
				} else {
					$activity = $act_status . '-' . $reg_array[$i]->category;
					if ($cmt != '') {
						$activity = $activity . '-' . $cmt;
					}
					$activity = $activity . $followup_on;
					$log_date = date("d-m-Y h:i a", strtotime($reg_array[$i]->log));
				}
				
				// $o_status = $reg_array[$i]->status;
				// $later_rsn = $reg_arraay[$i]->later_rsn;
				// if($o_status == "1"){
				//   $activity = "Cannot be connected".$later_rsn;
				// }
				// else if($o_status == "0"){
				//       $activity = "can be connected";
				// }
				$service_type = '';
				
			}
			/*if(isset($reg_array[$i]->booking_id)){
			$bkid = $reg_array[$i]->booking_id;
      if($booking_id == $bkid){
      ?>
      <tr style="background-color:#B2DFDB;">
      <?php
      }
      else{
      ?>
      <tr>
      <?php
      }
    }*/
			//$veh_no = $activity;
			if (strlen(stristr($activity, 'BOOKING -')) > 0) {
				
				$bkid = explode("-", $activity);
				$v = explode(" ", $bkid[1]);
				$bookid = $v[0];
				
				if ($booking_id === $bookid) {
					?>
					<tr style="background-color:#B2DFDB;">
					<?php
				} else {
					?>
					<tr>
					<?php
				}
			} else {
				?>
				<tr>
				<?php
			}
			?>
			<td><?php echo $x; ?></td>
			<td><?php echo $activity; ?></td>
			<td><?php echo $log_date; ?></td>
			<td><?php
				//echo "SELECT crm_lod_id FROM admin_comments_tbl WHERE log='$x' ";
				//echo "SELECT name FROM crm_admin WHERE crm_log_id = '$support' ";
				$sql_crm_name = mysqli_query($conn, "SELECT name FROM crm_admin WHERE crm_log_id = '$support' ");
				$row_sup_name = mysqli_fetch_array($sql_crm_name);
				echo $sup_name = $row_sup_name['name']; ?></td>
			<td><?php echo $service_type; ?></td>
			</tr>
			<?php
		}
		?>
		</tbody>
	</table>
</div>
<?php if ($pickup_fulladdress == "-" && $pickup_datetime == "-" && $service_description == "-"){ ?>
<div id="details"
	 style=" margin-left:14px;border:2px solid #708090; border-radius:8px; width:22%;height:490px; padding:20px; margin-top:18px; float:left;overflow-y:auto;display:none;">
	<?php }else{ ?>
	<div id="details"
		 style=" margin-left:14px;border:2px solid #708090; border-radius:8px; width:22%;height:490px; padding:20px; margin-top:18px; float:left;overflow-y:auto;">
		<?php } ?>
		<table id="table3" class="table borderless">
			<tbody>
			<tr>
				<td><strong>Service Description</strong></td>
				<td><?php echo $service_description ?></td>
			</tr>
			<tr>
				<td><strong>Pick Up Address</strong></td>
				<td><?php echo $pickup_fulladdress ?></td>
			</tr>
			<tr>
				<td><strong>Pick Up Time</strong></td>
				<td><?php echo $pickup_datetime ?></td>
			</tr>
			</tbody>
		</table>
	</div>
	
	
	<!-- cancel booking -->
	
	<?php
	
	
	if (($allotted_crm == $crm_log_id) || ($admin_flag == 1) || ($crm_se == 1) || $crm_log_id == 'crm036' || $crm_log_id == 'crm003' || $crm_log_id == 'crm020' || $crm_log_id == 'crm043' || $crm_log_id == 'crm158' || $crm_log_id == 'crm032' || $crm_log_id == 'crm160' || $crm_log_id == 'crm123' || $crm_log_id == 'crm135' || $crm_log_id == 'crm017' || $crm_log_id == 'crm161' || $crm_log_id == 'crm175' || $crm_log_id == 'crm187' || $crm_log_id == 'crm193' || $crm_log_id == 'crm178' || $crm_log_id == 'crm174' || $crm_log_id == 'crm018' || $crm_log_id == 'crm158' || $crm_log_id == 'crm227' || $crm_log_id == 'crm202' || $crm_log_id == 'crm239' || $crm_log_id == 'crm237' || $crm_log_id == 'crm225' || $crm_log_id == 'crm261' || $crm_log_id == 'crm263' || $crm_log_id == 'crm073' || $crm_log_id == 'crm108' || $crm_log_id == 'crm189' || is_null($allotted_crm)) {
		
		?>
		<!-- action button -->
		<div id="cb" class="action_button" style="position:fixed; margin-left:12.5%; bottom:20px;display:none;">
			<button class="btn btn-md" data-crm-id="<?php echo $allotted_crm; ?>" data-bid="<?php echo $booking_id; ?>"
					id="cancel" data-toggle="modal" data-target="#myModal_cancel_booking"
					style="background-color:#FF7043;"><i class="fa fa-ban" aria-hidden="true"></i>&nbsp;&nbsp;Cancel
				Booking
			</button>
		</div>
		<?php
	} ?>
	
	
	<!-- edit booking booking -->
	<?php
	if (($allotted_crm == $crm_log_id) || ($admin_flag == 1) || ($crm_se == 1) || $crm_log_id == 'crm036' || $crm_log_id == 'crm003' || $crm_log_id == 'crm020' || $crm_log_id == 'crm043' || $crm_log_id == 'crm158' || $crm_log_id == 'crm032' || $crm_log_id == 'crm160' || $crm_log_id == 'crm123' || $crm_log_id == 'crm135' || $crm_log_id == 'crm017' || $crm_log_id == 'crm161' || $crm_log_id == 'crm175' || $crm_log_id == 'crm187' || $crm_log_id == 'crm193' || $crm_log_id == 'crm178' || $crm_log_id == 'crm174' || $crm_log_id == 'crm018' || $crm_log_id == 'crm158' || $crm_log_id == 'crm227' || $crm_log_id == 'crm202' || $crm_log_id == 'crm239' || $crm_log_id == 'crm237' || $crm_log_id == 'crm283' || $crm_log_id == 'crm284' || $crm_log_id == 'crm320' || $crm_log_id == 'crm274' || $crm_log_id == 'crm320' || $crm_log_id == 'crm321' || $crm_log_id == 'crm073' || $crm_log_id == 'crm108' || $crm_log_id == 'crm189' || is_null($allotted_crm)) {
		?>
		<!-- Action Button -->
		<div id="edb" class="action_button" style="position:fixed;margin-left:62.5%;bottom:20px;display:none;">
			<div id="ebook" sytyle="position:fixed; margin-left:62.5%; bottom:20px;display:none;">
				<button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_edit_booking"
						style="background-color:#bfbbf1;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Edit
					Bookings
				</button>
			</div>
		</div>
		
		<?php
	}
	?>
	
	<?php
	if (($allotted_crm == $crm_log_id) || ($admin_flag == 1) || ($crm_se == 1) || $crm_log_id == 'crm036' || $crm_log_id == 'crm003' || $crm_log_id == 'crm020' || $crm_log_id == 'crm043' || $crm_log_id == 'crm158' || $crm_log_id == 'crm032' || $crm_log_id == 'crm160' || $crm_log_id == 'crm123' || $crm_log_id == 'crm135' || $crm_log_id == 'crm017' || $crm_log_id == 'crm161' || $crm_log_id == 'crm175' || $crm_log_id == 'crm187' || $crm_log_id == 'crm193' || $crm_log_id == 'crm178' || $crm_log_id == 'crm174' || $crm_log_id == 'crm018' || $crm_log_id == 'crm158' || $crm_log_id == 'crm227' || $crm_log_id == 'crm202' || $crm_log_id == 'crm239' || $crm_log_id == 'crm237' || $crm_log_id == 'crm283' || $crm_log_id == 'crm284' || $crm_log_id == 'crm320' || $crm_log_id == 'crm274' || $crm_log_id == 'crm320' || $crm_log_id == 'crm321' || $crm_log_id == 'crm073' || $crm_log_id == 'crm108' || $crm_log_id == 'crm189' || is_null($allotted_crm)) {
		?>
		<!-- action button -->
		<div class="action_button" style="position:fixed;margin-left:81.25%;bottom:20px;">
			<div id="addbook" sytyle="position:fixed; margin-left:81.25%; bottom:20px;display:none;">
				<button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_add_booking"
						style="background-color:#B5A5C3;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;New
					Booking
				</button>
			</div>
		</div>
		
		<?php
	}
	?>
	
	
	<div id="var_menu6" style="display:none;">
		
		<?php
		if (($allotted_crm == $crm_log_id) || ($admin_flag == 1) || ($crm_se == 1) || $crm_log_id == 'crm036' || $crm_log_id == 'crm003' || $crm_log_id == 'crm020' || $crm_log_id == 'crm043' || $crm_log_id == 'crm158' || $crm_log_id == 'crm032' || $crm_log_id == 'crm160' || $crm_log_id == 'crm123' || $crm_log_id == 'crm135' || $crm_log_id == 'crm017' || $crm_log_id == 'crm161' || $crm_log_id == 'crm175' || $crm_log_id == 'crm187' || $crm_log_id == 'crm193' || $crm_log_id == 'crm178' || $crm_log_id == 'crm174' || $crm_log_id == 'crm018' || $crm_log_id == 'crm158' || $crm_log_id == 'crm227' || $crm_log_id == 'crm202' || $crm_log_id == 'crm239' || $crm_log_id == 'crm237' || $crm_log_id == 'crm283' || $crm_log_id == 'crm284' || $crm_log_id == 'crm320' || $crm_log_id == 'crm274' || $crm_log_id == 'crm320' || $crm_log_id == 'crm321' || $crm_log_id == 'crm073' || $crm_log_id == 'crm108' || $crm_log_id == 'crm189' || is_null($allotted_crm)) {
			?>
			
			<!-- action button -->
			<div class="action_button" style="position:fixed; margin-left:31.25%; bottom:20px;">
				<button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_others"
						style="background-color:#D0D854;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Others
				</button>
			</div>
			
			<?php
		} ?>
		
		<?php
		if (($allotted_crm == $crm_log_id) || ($admin_flag == 1) || ($crm_se == 1) || $crm_log_id == 'crm036' || $crm_log_id == 'crm003' || $crm_log_id == 'crm020' || $crm_log_id == 'crm043' || $crm_log_id == 'crm158' || $crm_log_id == 'crm032' || $crm_log_id == 'crm160' || $crm_log_id == 'crm123' || $crm_log_id == 'crm135' || $crm_log_id == 'crm017' || $crm_log_id == 'crm161' || $crm_log_id == 'crm175' || $crm_log_id == 'crm187' || $crm_log_id == 'crm193' || $crm_log_id == 'crm178' || $crm_log_id == 'crm174' || $crm_log_id == 'crm018' || $crm_log_id == 'crm158' || $crm_log_id == 'crm227' || $crm_log_id == 'crm202' || $crm_log_id == 'crm239' || $crm_log_id == 'crm237' || $crm_log_id == 'crm283' || $crm_log_id == 'crm284' || $crm_log_id == 'crm320' || $crm_log_id == 'crm274' || $crm_log_id == 'crm320' || $crm_log_id == 'crm321' || $crm_log_id == 'crm073' || $crm_log_id == 'crm108' || $crm_log_id == 'crm189' || is_null($allotted_crm)) {
			?>
			<!-- Action Button -->
			<div class="action_button" style="position:fixed; margin-left:45%; bottom:20px;">
				<button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_unlimited_RNR"
						style="background-color:#6ed0c7;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;FollowUp
				</button>
			</div>
			<?php
		}
		?>
	
	</div>
	
	<!-- push others to bookings -->
	<?php
	if (($allotted_crm == $crm_log_id) || ($admin_flag == 1) || ($crm_se == 1) || $crm_log_id == 'crm036' || $crm_log_id == 'crm003' || $crm_log_id == 'crm020' || $crm_log_id == 'crm043' || $crm_log_id == 'crm158' || $crm_log_id == 'crm032' || $crm_log_id == 'crm160' || $crm_log_id == 'crm123' || $crm_log_id == 'crm135' || $crm_log_id == 'crm017' || $crm_log_id == 'crm161' || $crm_log_id == 'crm175' || $crm_log_id == 'crm187' || $crm_log_id == 'crm193' || $crm_log_id == 'crm178' || $crm_log_id == 'crm174' || $crm_log_id == 'crm018' || $crm_log_id == 'crm158' || $crm_log_id == 'crm227' || $crm_log_id == 'crm202' || $crm_log_id == 'crm239' || $crm_log_id == 'crm237' || $crm_log_id == 'crm283' || $crm_log_id == 'crm284' || $crm_log_id == 'crm320' || $crm_log_id == 'crm274' || $crm_log_id == 'crm320' || $crm_log_id == 'crm321' || $crm_log_id == 'crm073' || $crm_log_id == 'crm108' || $crm_log_id == 'crm189' || is_null($allotted_crm)) {
		?>
		<!-- action button -->
		<div class="action_button" id="pbook" style="position:fixed; margin-left:55%; bottom:20px;display:none;">
			<button data-bid="<?php echo $booking_id; ?>" type="button" class="btn btn-md" data-toggle="modal"
					data-target="#myModal_unlimited_RNR"
					style="position:relative;background-color:#58da80;width:180px;margin:10px; margin-bottom:10px;"><i
						class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Push To FollowUp
			</button>
			<button data-bid="<?php echo $booking_id; ?>" type="button" class="btn btn-md" data-toggle="modal"
					data-target="#myModal_add_booking"
					style="position:relative;background-color:#39B8AC;width:180px;margin:10px; margin-bottom:10px;"><i
						class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Push To Bookings
			</button>
		</div>
		<?php
	}
	?>
	<!-- revert cancelled bookings -->
	
	<?php
	if (($allotted_crm == $crm_log_id) || ($admin_flag == 1) || ($crm_se == 1) || $crm_log_id == 'crm036' || $crm_log_id == 'crm003' || $crm_log_id == 'crm020' || $crm_log_id == 'crm043' || $crm_log_id == 'crm158' || $crm_log_id == 'crm032' || $crm_log_id == 'crm160' || $crm_log_id == 'crm123' || $crm_log_id == 'crm135' || $crm_log_id == 'crm017' || $crm_log_id == 'crm161' || $crm_log_id == 'crm175' || $crm_log_id == 'crm187' || $crm_log_id == 'crm193' || $crm_log_id == 'crm178' || $crm_log_id == 'crm174' || $crm_log_id == 'crm018' || $crm_log_id == 'crm158' || $crm_log_id == 'crm227' || $crm_log_id == 'crm202' || $crm_log_id == 'crm239' || $crm_log_id == 'crm237' || $crm_log_id == 'crm283' || $crm_log_id == 'crm284' || $crm_log_id == 'crm320' || $crm_log_id == 'crm274' || $crm_log_id == 'crm320' || $crm_log_id == 'crm321' || $crm_log_id == 'crm073' || $crm_log_id == 'crm108' || $crm_log_id == 'crm189' || is_null($allotted_crm)) {
		?>
		
		<!-- action button -->
		<div class="action_button" id="revert" style="position:fixed; margin-left:55%; bottom:20px;display:none;">
			<a href="revert_cancel_booking.php?id=<?php echo base64_encode($booking_id); ?>&t=<?php echo base64_encode($type); ?>">
				<button type="button" class="btn btn-md"
						style="position:relative;background-color:#39B8AC;width:130px;margin:10px; margin-bottom:10px;">
					<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>&nbsp;Revert Back
				</button>
			</a>
		</div>
		
		<!-- action button -->
		<div class="action_button" id="push_new" style="position:fixed; margin-left:55%; bottom:20px;display:none;">
			<a href="push_new_or.php?id=<?php echo base64_encode($booking_id); ?>&t=<?php echo base64_encode($type); ?>">
				<button type="button" class="btn btn-md"
						style="position:relative;background-color:#FFA800;width:130px;margin:10px; margin-bottom:10px;">
					<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>&nbsp;Push Back
				</button>
			</a>
		</div>
		<?php
	}
	?>
	<!-- alert popup modal -->
	<div class="modal fade" id="authorized_user_alert" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h1 class="modal-title text-danger">Warning...!</h1>
				</div>
				<div class="modal-body">
					<p style="text-align:center; font-size:18px"><strong>You are not an authorized person.</strong></p>
				</div>
			</div>
		</div>
	</div>
	
	
	<!-- jQuery library -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
			integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
			crossorigin="anonymous"></script>
	
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
	
	<!-- side bar -->
	<script src="js/sidebar.js"></script>
	<script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-36251023-1']);
        _gaq.push(['_setDomainName', 'jqueryscript.net']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();
	
	</script>
	
	<script>

        $(document).ready(function () {

            var t = "<?php echo $type; ?>";
            var crm = "<?php echo $crm_log_id ?>";
            var se = '<?php echo $_SESSION['crm_se']; ?>';
            var eupraxia_flag = '<?php echo $_SESSION['eupraxia_flag']; ?>';

            console.log(t);

            var t = "<?php echo $type; ?>";
            var crm = "<?php echo $crm_log_id ?>";
            var se = '<?php echo $_SESSION['crm_se']; ?>';

            switch (t) {
                case 'o':
                    $("#av").hide();
                    $("#pb").hide();
                    $("#cb").hide();
                    $("#pbook").show();
                    $("#ebook").hide();
                    $("#addbook").hide();
                    $("#push_new").hide();
                    break;
                case 'c':
                    $("#av").hide();
                    $("#pb").hide();
                    $("#pbook").hide();
                    $("#cb").hide();
                    $("#revert").show();
                    $("#edb").hide();
                    $("#ebook").hide();
                    $("#addbook").hide();
                    $("#push_new").hide();
                    break;
                case 'ol':
                    $("#av").hide();
                    $("#pb").hide();
                    $("#pbook").hide();
                    $("#cb").hide();
                    $("#revert").hide();
                    $("#edb").hide();
                    $("#ebook").hide();
                    $("#addbook").hide();
                    $("#push_new").show();
                    break;
                case 'l':
                    $("#av").show();
                    $("#pb").show();
                    $("#cb").show();
                    $("#edb").show();
                    $("#ebook").show();
                    $("#addbook").hide();
                    $("#push_new").hide();
                    break;
                default:
                    $("#av").show();
                    $("#pb").show();
                    $("#cb").show();
                    $("#edb").show();
                    $("#ebook").show();
                    $("#addbook").show();
                    $("#push_new").hide();
            }

            var axle_flag = "<?php $sql_get_axle = mysqli_query($conn, "SELECT axle_flag FROM user_booking_tb WHERE booking_id='$booking_id'");
				$row_get_axle = mysqli_fetch_object($sql_get_axle);
				$axle_flag = $row_get_axle->axle_flag;
				echo $axle_flag; ?>";
            //console.log(axle_flag);
            if (axle_flag == 1 && t == "f") {

                $("#cb").hide();
                $("#edb").hide();
                $("#ebook").hide();
                $("#addbook").show();
                $("#push_new").hide();
            }

            if (axle_flag == 1 && t == "b") {
                $("#cb").hide();
                $("#ebook").hide();
                $("#addbook").hide();
                $("#push_new").hide();
            }
            if (t == "eb") {
                $("#eu").hide();
                $("#av").hide();
                $("#pb").hide();
                $("#cb").hide();
                $("#edb").hide();
                $("#ebook").hide();
                $("#addbook").hide();
                $("#push_new").hide();
            }
            if (t == "unwanted") {
                $("#av").show();
                $("#pb").show();
                $("#cb").show();
                $("#eu").show();
                $("#revert").hide();
                $("#pbook").hide();
                $("#edb").show();
                $("#ebook").show();
                $("#addbook").hide();
                $("#push_new").hide();
            }
            if (t == "irp") {
                $("#av").show();
                $("#cb").hide();
                $("#edb").hide();
                $("#ebook").hide();
                $("#addbook").show();
                $("#push_new").hide();
            }
            if (t == "or") {
                if (se != '1' && crm != 'crm020' && crm != 'crm001' && crm != 'crm043' && crm != 'crm003' && crm != 'crm158' && crm != 'crm160' && crm != 'crm123' && crm != 'crm135' && crm != 'crm161' && crm != 'crm175' && crm != 'crm187' && crm != 'crm193' && crm != 'crm178' && crm != 'crm174' && crm != 'crm036' && crm != 'crm018' && crm != 'crm032' && crm != 'crm227' && crm != 'crm239' && crm != 'crm237' && crm != 'crm202' && crm != 'crm225' && crm != 'crm263' && crm != 'crm261' && crm != 'crm231' && crm != 'crm139' && crm != 'crm017' && crm != 'crm283' && crm != 'crm284' && crm != 'crm231' && crm != 'crm139' && crm != 'crm017' && crm != 'crm263' && crm != 'crm186' && crm != 'crm018' && crm != 'crm274' && crm != 'crm269' && crm != 'crm320' && crm != 'crm321' && crm != 'crm189' && crm != 'crm434' && crm != 'crm440' && crm != 'crm279' && crm != 'crm231' && crm != 'crm012') {
                    $("#cb").hide();
                    $("#edb").hide();
                    $("#push_new").hide();
                } else {
                    $("#cb").hide();
                    $("#edb").show();
                    $("#push_new").hide();
                }
            }

            //eupraxia_flag
            if (eupraxia_flag) {
                $(".action_button button").prop('disabled', true);
            }
            //eupraxia_flag

            $('#revertBack').on('click', function () {
                $('#revertBack').prop('disabled', true);
            });
        });

        //start call
        function startCallEupraxia(user_id, booking_id) {
            console.log(user);

            //Disable Call btn
            $("button#start_call").prop('disabled', true);

            setTimeout(function () {
                $(".action_button button").prop('disabled', false);
            }, 2000);

            setTimeout(function () {
                $("button#end_call").prop('disabled', false);
            }, 2000);

            $.ajax({
                url: "callEupraxiaOut.php",  // create a new php page to handle ajax request
                type: "POST",
                data: {"user_id": user_id, "booking_id": booking_id},
                success: function (response) {
                    //enable action buttons
                    if (response.status == "Answered") {

                    }

                    if (response.status == "No Answer") {

                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    // alert(xhr.status + " "+ thrownError);
                }
            });
        }

        //end call
        function endCallEupraxia(user) {
            console.log(user);
            $("button#start_call").prop('disabled', false);
            $("button#end_call").prop('disabled', true);
        }

        //Sivaraj
	</script>
	
	<!-- push button -->
	<script>
        $(document).ready(function () {
            var t = "<?php echo $type; ?>";
            var s = "<?php echo $status; ?>";
            if (t == "l" || t == "unwanted") {
                //	$("#var_menu1").slideToggle("slow");
                $("#var_menu6").show();
            }
            if (t == "irp") {
                //	$("#var_menu1").slideToggle("slow");
                $("#var_menu6").hide();
            }
            if (t == "b") {
                //	$("#var_menu2").slideToggle("slow");
                $("#var_menu6").hide();
            }
            if (t == "f") {
                if (s == "4") {
                    //	$("#var_menu6").slideToggle("slow");
                    $("#var_menu6").show();
                }
                if (s == "5") {
                    //$("#var_menu6").slideToggle("slow");
                    $("#var_menu6").show();
                }
                if (s == "3") {
                    //$("#var_menu6").slideToggle("slow");
                    $("#var_menu6").show();
                }
                if (s == "6") {
                    //$("#var_menu6").slideToggle("slow");
                    $("#var_menu6").show();
                }
            }
            if (t == "ur") {
                //$("#var_menu6").slideToggle("slow");
                $("#var_menu6").show();
            }
        });
	
	</script>
	
	<!-- add vehicle -->
	<!-- Modal -->
	<div class="modal fade" id="myModal_vehicle" role="dialog">
		<div class="modal-dialog">
			
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3 class="modal-title">Add Vehicle (<?php echo $user_id; ?>)</h3>
				</div>
				<div class="modal-body">
					<form id="add_vehicle" class="form" method="post" action="add_vehicle.php">
						<div class="row">
							<div class="col-xs-6 col-xs-offset-3 form-group">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<label class="bike">
									<input id="veh" type="radio" name="veh" value="2w"/>
									<img id="bike" src="images/bike.png">
								</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<label class="car">
									<input id="veh" type="radio" name="veh" value="4w"/>
									<img id="car" src="images/car.png">
								</label>
							</div>
						</div>
						<div class="row"></div>
						<div class="row">
							<div id="b_m" class="col-xs-6 col-xs-offset-3 form-group">
								<div class="ui-widget">
									<input class="form-control autocomplete" id="BrandModelid" type="text"
										   name="BrandModelid" required placeholder="Select Model">
								</div>
							</div>
						</div>
						
						<div class="row"></div>
						<div class="row">
							<div class="col-xs-6 col-xs-offset-3 form-group">
								<select class="form-control" id="fuel" name="fuel">
									<option selected>Fuel Type</option>
									<option data-imagesrc="images/bike.png" value="Diesel">Diesel</option>
									<option data-imagesrc="images/car.png" value="Petrol">Petrol</option>
								</select>
							</div>
						</div>
						<div class="row"></div>
						<div class="row">
							<div class="col-xs-6 col-xs-offset-3 form-group">
								<input class="form-control" type="text" id="regno" name="regno" data-mask-reverse="true"
									   maxlength="13" placeholder="Vehicle No">
							</div>
						</div>
						<div class="row"></div>
						<div class="row">
							<div class="col-xs-6 col-xs-offset-3 form-group">
								<input class="form-control" type="text" id="year" name="year" placeholder="Year">
							</div>
						</div>
						<div class="row"></div>
						<div class="row">
							<div class="col-xs-6 col-xs-offset-3 form-group">
								<input class="form-control" type="number" id="km" name="km" placeholder="Km Driven">
							</div>
						</div>
						<div class="row"></div>
						<div class="row">
							<div class="col-xs-2 col-xs-offset-5 form-group">
								<br>
								<input class="form-control" type="submit" id="vehicle_submit" name="vehicle_submit"
									   value="Submit"
									   style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
							</div>
						</div>
						<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>"/>
						<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>"/>
						<input type="hidden" id="type" name="type" value="<?php echo $type; ?>"/>
						<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>"/>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<!-- add booking -->
	<!-- Modal -->
	<div class="modal fade" id="myModal_add_booking" role="dialog">
		<div class="modal-dialog">
			
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3 class="modal-title">Add Booking(<?php echo $user_id; ?>)</h3>
				</div>
				<div class="modal-body">
					<form id="add_booking" class="form" method="post" action="add_booking.php">
						<div class="row" align="center">
							<div id="veh_t" class="col-xs-6 col-xs-offset-3 form-group">
								
								<label class="bike">
									<input class="veh_b" id="veh_b" type="radio" name="veh_b" value="2w" checked/>
									<img id="veh_b" src="images/bike.png">
								</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<label class="car">
									<input class="veh_b" id="car" type="radio" name="veh_b" value="4w"/>
									<img id="veh_b" src="images/car.png">
								</label>
							</div>
						</div>
						
						<div class="row"></div>
						
						<div class="row">
							<div id="b_m" class="col-xs-5 col-xs-offset-1 form-group">
								<select class="form-control brandmodel" id="veh_no" name="veh_no" required>
									<option selected value="">Vehicle</option>
								</select>
							</div>
							
							
							<div class="col-xs-5 form-group" id="service">
								<div class="ui-widget" id="serv">
									<input class="form-control autocomplete servicetype" id="service_type" type="text"
										   name="service_type" placeholder="Service Type" required>
								</div>
							</div>
						</div>
						
						<div class="row"></div>
						
						<div class="row">
							<div class="col-xs-8 col-xs-offset-1 form-group" id="loc">
								<div class="ui-widget">
									<input class="form-control autocomplete" id="location" type="text" name="location"
										   placeholder="Start typing Location..." required>
								</div>
							</div>
							<div><p class="btn btn-sm" id="go" style="margin-left:-8px;background-color:#B2EBF2;">Go</p>
							</div>
						</div>
						
						<div class="row"></div>
						
						<div class="row">
							<div class="col-xs-6 col-xs-offset-1 form-group" id="mec">
								
								<select class="form-control  mechanic" id="mechanic" name="mechanic" required>
									<option selected>Select Mechanic</option>
								</select>
							</div>
							
							<div class="col-xs-4 form-group">
								<input class="form-control" type="number" id="amount" name="amount"
									   placeholder="Amount">
							</div>
						</div>
						
						
						<div class="row"></div>
						<div class="row">
							<div class="col-xs-2 col-xs-offset-1 form-group">
								<label> Service Date</label></div>
							<div class="col-xs-3  form-group">
								<input class="form-control service_date" data-date-format='dd-mm-yyyy'
									   type=" FollowUp Datetext" id="service_date" name="service_date" required
									   style="margin-top:10px;">
							</div>
							
							<div class="col-xs-2 form-group">
								<label> Next Service Date</label></div>
							<div class="col-xs-3  form-group">
								<input class="form-control next_service_date" data-date-format='dd-mm-yyyy' type="text"
									   id="next_service_date" name="next_service_date" required
									   style="margin-top:10px;">
							</div>
						</div>
						<div class="row"></div>
						
						<div class="row">
							<div class="col-xs-10 col-xs-offset-1 form-group">
								<textarea class="form-control" maxlength="100" id="description" name="description"
										  placeholder="Description..."></textarea>
							</div>
						</div>
						
						<div class="row"></div>
						<div class="row">
							<div class="col-xs-4 col-xs-offset-4 form-group">
								<!--<label>PickUp</label>
  <input class="form-group" type="radio" id="pickup" name="pickup" value="1" >&nbsp;Yes&nbsp;</input>
  <input class="form-group" type="radio" id="pickup" name="pickup" value="0" checked>&nbsp;No</input>
</div>

<div class="col-xs-4  form-group">-->
								<label>SMS</label>
								<input class="form-group" type="radio" id="sms" name="sms" value="1" checked>&nbsp;Yes&nbsp;</input>
								<input class="form-group" type="radio" id="sms" name="sms" value="0">&nbsp;No</input>
							</div>
						</div>
						
						<div class="row"></div>
						
						<div class="row"></div>
						<div class="row">
							<div class="col-xs-2 col-xs-offset-5 form-group">
								<input class="form-control" type="submit" id="booking_submit" name="booking_submit"
									   value="Submit"
									   style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
							</div>
						</div>
						<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>">
						<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>">
						<input type="hidden" id="type" name="type" value="<?php echo $type; ?>">
						<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>">
					</form>
				
				</div>
			
			</div>
		</div>
	</div>
	
	
	
	
<!-- edit booking -->
<!-- Modal -->
<div class="modal fade" id="myModal_edit_booking" role="dialog">
<div class="modal-dialog" style="width:1050px;">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Edit Booking(<?php echo $booking_id; ?>)
</h3>
</div>
<div class="modal-body">
<form id="edit_booking" class="form" method="post" action="">
  <?php
$sql_book = "SELECT b.vech_id,b.user_veh_id,b.vehicle_type,b.user_vech_no,b.service_type,b.mec_id,b.shop_name,b.service_description,b.pickup_full_address,b.estimate_amt,b.service_date,b.pick_up,b.pickup_address,b.amt,b.followup_date,b.locality,b.pickup_date_time,v.year,v.fuel_type,v.vehicle_usage,b.ire_flag FROM user_booking_tb as b LEFT JOIN user_vehicle_table as v on b.user_veh_id=v.id WHERE b.booking_id = '$booking_id'";
$res_book = mysqli_query($conn,$sql_book);
$row_book = mysqli_fetch_object($res_book);
$b_veh_id=$row_book->vech_id;
$b_user_veh_id = $row_book->user_veh_id;
$b_veh = $row_book->vehicle_type;
$b_veh_no = $row_book->user_vech_no;
$b_service_type = $row_book->service_type;
$b_mec_id = $row_book->mec_id;
$b_shop_name = $row_book->shop_name;
$b_desc = $row_book->service_description;
$b_service_date = $row_book->service_date;
$b_pickup = $row_book->pick_up;
$b_amount = $row_book->amt;
$estimate_amt = $row_book->estimate_amt;
$next_service_date = $row_book->followup_date;
$b_locality = $row_book->locality;
$pickup_location = $row_book->pickup_address;
$pickup_full_address = $row_book->pickup_full_address;
$year = $row_book->year;
$fuel = $row_book->fuel_type;
$usage = $row_book->vehicle_usage;
$ire_flag=$row_book->ire_flag;
$pickup_date_time = date('d-m-Y H:i',strtotime($row_book->pickup_date_time));

// get brand model
$sql_model = "SELECT id,brand,model,reg_no FROM user_vehicle_table WHERE id='$b_veh_id'";
$res_model = mysqli_query($conn,$sql_model);
$row_model = mysqli_fetch_object($res_model);
$brand_id = $row_model->id;
$brandmodel = $row_model->brand." ".$row_model->model." ".$row_model->reg_no;

if($b_mec_id == "" || $b_mec_id==0){
  $shop_address = "";
}
else{
//get shop address
$sql_shop_loc = mysqli_query($conn,"SELECT address4 FROM admin_mechanic_table WHERE mec_id='$b_mec_id' ");
$row_shop_loc= mysqli_fetch_object($sql_shop_loc);
$shop_address = $row_shop_loc->address4;
}

    switch($b_service_type){
      case 'general_service' :
      if($b_veh =='2w'){
       $b_service_type = "General Service";
      }
      else{
       $b_service_type = "Car service and repair";
      }
      break;
      case 'break_down': $b_service_type = "Breakdown Assistance"; break;
      case 'tyre_puncher':$b_service_type = "Tyre Puncture";  break;
      case 'other_service':$b_service_type = "Repair Job"; break;
      case 'car_wash':
      if($b_veh == '2w'){
       $b_service_type = "water Wash";
      }
      else{
       $b_service_type = "Car wash exterior";
      }break;
      case 'engine_oil':$b_service_type = "Repair Job"; break;
      case 'free_service':$b_service_type = "General Service"; break;
      case 'car_wash_both':
      case 'completecarspa':$b_service_type = "Complete Car Spa"; break;
      case 'car_wash_ext':$b_service_type = "Car wash exterior"; break;
      case 'car_wash_int':$b_service_type = "Interior Detailing"; break;
      case 'Doorstep car spa':$b_service_type = "Doorstep Car Spa"; break;
      case 'Doorstep_car_wash':$b_service_type = "Doorstep Car Wash"; break;
      case 'free_diagnastic':
      case 'free_diagnostic':
      case 'diagnostics':
      if($b_veh == '2w'){
       $b_service_type = "Diagnostics/Check-up";
     }
     else{
      $b_service_type = "Vehicle Diagnostics";
     }
         break;
      case 'water_wash':$b_service_type = "water Wash"; break;
      case 'exteriorfoamwash':$b_service_type = "Car wash exterior"; break;
      case 'interiordetailing':$b_service_type = "Interior Detailing"; break;
      case 'rubbingpolish':$b_service_type = "Car Polish"; break;
      case 'underchassisrustcoating':$b_service_type = "Underchassis Rust Coating"; break;
      case 'headlamprestoration':$b_service_type = "Headlamp Restoration"; break;
      default:$b_service_type = $b_service_type;
      }

if($b_veh == "2w"){
  ?>
  <div class="row" align="center">
  <div class="col-xs-1" style="margin-top: 6%;border-right: solid 1px #ddd;width: 13%;">
<div class="row" align="center">
  <div class="col-xs-1 form-group">
  <label class="bike">
  <input id="veh_be" type="radio" name="veh_be" value="2w" checked/>
  <img id="veh_be" src="images/bike.png">
  </label>
  </div></div>
  <div class="row" align="center">
  <div class="col-xs-1  form-group">
  <label class="car">
  <input id="car" type="radio" name="veh_be" value="4w" />
  <img id="veh_be" src="images/car.png">
  </label>
</div></div>
</div>
<?php
}
else{
  ?>
  <div class="row" align="center">
  <div class="col-xs-1" style="margin-top: 6%;border-right: solid 1px #ddd;width: 13%;">
<div class="row" align="center">
  <div class="col-xs-1 form-group">
  <label class="bike">
  <input id="veh_be" type="radio" name="veh_be" value="2w" />
  <img id="veh_be" src="images/bike.png">
  </label>
  </div></div>
  <div class="row" align="center">
  <div class="col-xs-1  form-group">
  <label class="car">
  <input id="car" type="radio" name="veh_be" value="4w" checked/>
  <img id="veh_be" src="images/car.png">
  </label>
</div></div>
</div>
<?php
}?>
<div class="col-xs-10" style="margin-left: 3%;">
<div class="row" align="center">
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
<label> Brand Model</label> 
</div>
<div id="b_me" class="col-xs-4 form-group">
<select class="form-control" id="veh_noe" name="veh_noe">
<option selected value="<?php echo $brand_id; ?>"><?php echo $brandmodel; ?></option>
 </select>
</div>
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
<label> Service Type</label> 
</div>
<div class="col-xs-3 form-group" id="service">
<div class="ui-widget">
        	<input class="form-control autocomplete" id="service_typee" type="text" name="service_typee" placeholder="Service Type" value="<?php echo $b_service_type; ?>" required>
</div>
</div>
</div>

<div class="row" align="center">

	<div id="re_toggle">
	<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
		<label>RE Specialist</label>
	</div>
	<div class="col-xs-4 form-group" >
		<span class="make-switch switch-radio">
		  <input type="radio" data-on-text="Yes" data-off-text="No" data-on-color="success" data-off-color="danger" class="alert-status" id="re_switch">
		</span>
	</div>
	</div>

	<div id="doorstep_toggle">
	<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
		<label>Doorstep</label>
	</div>
	<div class="col-xs-4 form-group" >
		<span class="make-switch switch-radio">
		  <input type="radio" data-on-text="Yes" data-off-text="No" data-on-color="success" data-off-color="danger" class="alert-status" id="doorstep_switch">
		</span>
	</div>
	</div>

<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
<label> Customer Location</label> 
</div>
<div class="col-xs-3 form-group" id="loce">
        <div class="ui-widget">
        	<input class="form-control autocomplete" id="locatione" type="text" name="locatione" placeholder="Start typing Location..." value="<?php echo $b_locality; ?>" readonly>
        </div>
        
</div>


</div>


<div class="row" align="center">
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
<label> PickUp Address</label> 
</div>
<div class="col-xs-8 form-group" id="locpickup">
	<div class="ui-widget">
		<!-- <input class="form-control autocomplete" id="location_pickup" type="text" name="location_pickup" placeholder="PickUp Location..." value="<?php if($pickup_location == "" || $pickup_location == "-"){ echo $b_locality;} else { echo $pickup_location;} ?>" required > -->
		<textarea class="form-control" maxlength="100" id="location_pickup" name="location_pickup" placeholder="Address..." ><?php echo $pickup_full_address;?></textarea>
		 <input type="hidden" name="lat" id="lat">
         <input type="hidden" name="long" id="long">
         <!-- <label id="test"></label> -->
		<!--<input class="form-control autocomplete" id="location_pickup" type="text" name="location_pickup" placeholder="PickUp Location..." required>-->
	</div>        
</div>

<div class="col-xs-1 form-group" id="go-div"><p class="btn btn-sm" id="go" style="margin-left:-8px;background-color:#B2EBF2;margin-top: 4px;padding: 8px 18px;font-size: 17px;">Go</p></div>
</div>

<div class="row" align="left">
<div class="col-md-12" id="over_show">
<h5><b>Sample Format: Garage Name (Leads/Credits Remaining)[G : GoAxled , C : Lead Cap ,S : Time Since Last Goaxle]</b></h5>
</div>
</div>
<br>
<div class="row" align="center">
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
<label> Service Center</label> 
</div>
<div class="col-xs-4 form-group" id="mece">

<select class="form-control" id="mechanice" name="mechanice" required>
<?php if($b_shop_name == '' || $b_shop_name == '0'){ ?>
<option selected value="">Select Mechanic</option>
<?php } 
else{ ?>
<option selected value="<?php echo $b_mec_id; ?>"><?php echo $b_shop_name; ?></option>
<?php } ?>
</select>
</div>
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
<label> Service Date</label>
</div>
<div class="col-xs-3  form-group">
<input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="service_datee" name="service_datee" value="<?php if($b_service_date=='0000-00-00'){ echo date("d-m-Y",strtotime($today));} else {  echo date("d-m-Y",strtotime($b_service_date));} ?>" required>
</div>
</div>




<div class="row" align="center">
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
<label> Service Description</label> 
</div>
<div class="col-xs-4 form-group" >
<textarea class="form-control" maxlength="2000" id="descriptione" name="descriptione" placeholder="Description..." ><?php echo $b_desc; ?></textarea>
</div>
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
<label>PickUp</label>
</div>
<div class="col-xs-4 form-group" >
<span class="make-switch switch-radio">
  <input type="radio" <?php  if($b_pickup=="yes"||$b_pickup=="1"){ echo checked;} ?> data-on-text="Yes" data-off-text="No" data-on-color="success" data-off-color="danger" class="alert-status" id="pickup_switch">
</span>
<?php
// if($b_pickup == "yes"){
  ?>
<!--  <input class="form-group" type="radio" id="pickupe" name="pickupe" value="1" checked>&nbsp;Yes&nbsp;</input>
  <input class="form-group" type="radio" id="pickupe" name="pickupe" value="0">&nbsp;No</input>-->
  <?php
// }
// else{
  ?>
  <!--<input class="form-group" type="radio" id="pickupe" name="pickupe" value="1">&nbsp;Yes&nbsp;</input>
  <input class="form-group" type="radio" id="pickupe" name="pickupe" value="0" checked>&nbsp;No</input>-->
  <?php
// } ?>
</div>

</div>
<div class="row" align="center">
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
<label> PickUp Date</label> 
</div>
<div class="col-xs-4 form-group" >
<div class='input-group date' id='datetimepicker7'>		
		<input type='text' class="form-control" id="pickup_date" name="pickup_date" value="<?php if($pickup_date_time == '01-01-1970 05:30') { echo $today_time; }?>"/>		
		<span class="input-group-addon">		
		<span class="glyphicon glyphicon-calendar"></span>		
		</span>		
	</div>
</div>

<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
<label> Estimate Quoted</label> 
</div>
<div class="col-xs-3 form-group">
<input class="form-control" type="number" id="amounte" name="amounte" placeholder="Amount" value="<?php echo $estimate_amt; ?>">
</div>		

</div>
<div class="row" align="center">

<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
<label>Preferred Language</label>
</div>
<div class="col-xs-4 form-group" id="language">		
	<select class="form-control" id="languages" name="language">
		<option selected value="English">English</option>
		<option value="Tamil">Tamil</option>
		<option value="Hindi">Hindi</option>
		<option value="Kannada">Kannada</option>
		<option value="Malayalam">Malayalam</option>
		<option value="Telugu">Telugu</option>
	</select>		
</div>	
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
	<label>Fuel type</label> 
</div>
<div class="col-xs-3 form-group" id="fuel" >
	<select class="form-control" id="fuel_type" name="fuel_type">
		<option selected value="petrol">Petrol</option>
		<option value="diesel">Diesel</option>
		<option value="hybrid">Hybrid</option>
	</select>
</div>
</div>
<div class="row" align="center">


<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
	<label>Vehicle usage</label> 
</div>
<div class="col-xs-4 form-group" id="vehicle_usage" >
	<select class="form-control" id="v_usage" name="v_usage">
		<option selected value="passenger">Passenger</option>
		<option value="Commercial">Commercial</option>
		<option value="own use">Own use</option>
	</select>
</div>
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
	<label>Manufactured year</label> 
</div>
<div class="col-xs-3 form-group" id="fuel" >
	<input class="form-control" type="number" min="1950" max="2100" id="m_year" name="m_year" placeholder="year" value="<?php echo $year; ?>" required>
</div>
</div>

</div>
</div>
<?php
//}?>
<div class="row">
<div class="col-xs-2 col-xs-offset-4 form-group">
<input class="form-control" type="submit" id="edit_booking_submit" name="edit_booking_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div>

<?php
$sql_override = "SELECT o.booking_id from override_tbl as o LEFT JOIN user_booking_tb as u on o.booking_id=u.booking_id WHERE o.status ='0' AND u.booking_id='$booking_id' ";
$res_override = mysqli_query($conn,$sql_override);
$row_override = mysqli_fetch_object($res_override);
//if($row_override > 0 && ($crm_log_id == 'crm036' || $crm_log_id == 'crm003' || $crm_log_id == 'crm020' || $crm_log_id == 'crm043' || $crm_log_id == 'crm158' || $crm_log_id == 'crm032' || $crm_log_id == 'crm160' || $crm_log_id == 'crm123'|| $crm_log_id == 'crm135' || $crm_log_id == 'crm017'|| $crm_log_id == 'crm161' || $crm_log_id == 'crm175' || $crm_log_id == 'crm187' || $crm_log_id == 'crm193' || $crm_log_id == 'crm178' || $crm_log_id == 'crm174' || $crm_log_id== 'crm018' || $crm_log_id== 'crm158' || $crm_log_id== 'crm227' || $crm_log_id== 'crm202' || $crm_log_id== 'crm239' || $crm_log_id== 'crm237' || $crm_log_id== 'crm283' || $crm_log_id== 'crm284' || $crm_log_id== 'crm320' || $crm_log_id== 'crm274' || $crm_log_id== 'crm320' || $crm_log_id== 'crm321' || $crm_log_id== 'crm225' || $crm_log_id== 'crm018' || $crm_log_id== 'crm189' || $crm_log_id== 'crm434')){
	if(( $crm_se=='1' || $crm_log_id == 'crm036' || $crm_log_id == 'crm003' || $crm_log_id == 'crm020' || $crm_log_id == 'crm043' || $crm_log_id == 'crm158' || $crm_log_id == 'crm032' || $crm_log_id == 'crm160' || $crm_log_id == 'crm123'|| $crm_log_id == 'crm135' || $crm_log_id == 'crm017'|| $crm_log_id == 'crm161' || $crm_log_id == 'crm175' || $crm_log_id == 'crm187' || $crm_log_id == 'crm193' || $crm_log_id == 'crm178' || $crm_log_id == 'crm174' || $crm_log_id== 'crm018' || $crm_log_id== 'crm158' || $crm_log_id== 'crm227' || $crm_log_id== 'crm202' || $crm_log_id== 'crm239' || $crm_log_id== 'crm237' || $crm_log_id== 'crm283' || $crm_log_id== 'crm284' || $crm_log_id== 'crm320' || $crm_log_id== 'crm274' || $crm_log_id== 'crm320' || $crm_log_id== 'crm321' || $crm_log_id== 'crm225' || $crm_log_id== 'crm018' || $crm_log_id== 'crm189' || $crm_log_id== 'crm434' || $crm_log_id== 'crm261') && $row_override > 0){ ?>
	<div id="ob" class="col-xs-2 col-xs-offset-1">
<!-- <input class="form-control" type="submit" id="override_booking_submit" name="override_booking_submit" value="Push to Overrides" style="background-color:#f78371; color:black; box-shadow:0 3px 3px 0 #000;"/> -->
			<button class="btn btn-md" data-bid="<?php echo $booking_id; ?>"  id="late_override_btn" data-toggle="modal" style="background-color:#efbe0b;outline:none;"><i class="fa fa-exchange" aria-hidden="true"></i>&nbsp;&nbsp;Override later</button>
</div>
<?php } 
else{ ?>
<div id="ob" class="col-xs-2 col-xs-offset-1">
<input class="form-control" type="submit" id="override_booking_submit" name="override_booking_submit" value="Push to Overrides" style="background-color:#efbe0b; color:black; box-shadow:0 3px 3px 0 #000;outline:none;"/>
			<!-- <button class="btn btn-md" data-bid="<?php echo $booking_id; ?>"  id="override_modal" data-toggle="modal" data-target="#myModal_override_booking" style="background-color:#f78371;display:none;"><i class="fa fa-exchange" aria-hidden="true"></i>&nbsp;&nbsp;Push to Override</button> -->
</div>
<?php } ?>

</div>
<input type="hidden" id="override_flag" name="override_flag" value="0" >
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
<input type="hidden" id="pickupe" name="pickupe" <?php  if($b_pickup=="yes"){ echo "value='1'";}else{ echo "value='0'";} ?>>
<input type="hidden" id="doorstep_only" name="doorstep_only" value="0">
<input type="hidden" id="re_specialist" name="re_specialist" value="0">
</form>
<!--<div class="loader_overlay">
</div>
<div class="loader">
</div>-->
		<div class="socket loader">
			<div class="gel center-gel">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c1 r1">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c2 r1">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c3 r1">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c4 r1">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c5 r1">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c6 r1">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			
			<div class="gel c7 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			
			<div class="gel c8 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c9 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c10 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c11 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c12 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c13 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c14 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c15 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c16 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c17 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c18 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c19 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c20 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c21 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c22 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c23 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c24 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c25 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c26 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c28 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c29 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c30 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c31 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c32 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c33 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c34 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c35 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c36 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c37 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			
		</div>
</div>

</div>
</div>
</div>
	<!-- add unlimited RNR -->
	<!-- Modal -->
	<div class="modal fade" id="myModal_unlimited_RNR" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3 class="modal-title">Add Follow Up (Id : <?php echo $user_id; ?>)</h3>
				</div>
				<div class="modal-body">
					<?php
					if ($type == "l"){
					?>
					<form id="add_rnr" role="form" method="post" action="add_followup.php">
						<?php
						}
						else{
						?>
						<form id="add_rnr" role="form" method="post" action="add_rnr.php">
							<?php
							} ?>
							
							<div class="row">
								<div class="col-xs-8 col-xs-offset-2 form-group">
									<br>
									<select class="form-control" id="followup_status" name="status" required>
										<option selected value="">Select Reason</option>
										<?php
										$sql = "SELECT activity FROM admin_activity_tbl WHERE flag='0'";
										$query = mysqli_query($conn, $sql);
										while ($rows = mysqli_fetch_array($query)) { ?>
											<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="row"></div>
							<div class="row">
								<div class="col-xs-8 col-xs-offset-2 form-group">
									<textarea class="form-control" maxlength="1000" id="comments" name="comments"
											  style="min-height:100px;" placeholder="Comments..."></textarea>
								</div>
							</div>
							<dir></dir>
				</div>
				<div class="row">
					<div class="col-xs-3 col-xs-offset-2 form-group">
						<label>&nbsp;FollowUp Date</label>
					</div>
					<div class="col-xs-5">
						<div class="form-group">
							<div class='input-group date' id='datetimepicker6'>
								<input type='text' class="form-control" name="follow_date"/>
								<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
					</span>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-3 col-xs-offset-2 form-group">
						<label>&nbsp;Priority</label>
					</div>
					<div class="col-xs-5">
						<div class="form-group">
							<input type="radio" name="priority" value="1" style="width:30px;">Low</input>
							<input type="radio" name="priority" value="2" style="width:30px;">Medium</input>
							<input type="radio" name="priority" value="3" style="width:30px;" checked>High</input>
						</div>
					</div>
				</div>
				<div class="row"></div>
				<div class="row">
					<div class="col-xs-2 col-xs-offset-5 form-group">
						<br>
						<input class="form-control" type="submit" id="followup_submit" name="followup_submit"
							   value="Submit"
							   style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
					</div>
				</div>
				<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>">
				<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>">
				<input type="hidden" id="type" name="type" value="<?php echo $type; ?>">
				<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>">
				</form>
			</div>
		
		</div>
	</div>
</div>

<!-- move to  others -->
<!-- Modal -->
<div class="modal fade" id="myModal_others" role="dialog">
	<div class="modal-dialog">
		
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Move to Others (Id : <?php echo $user_id; ?>)</h3>
			</div>
			<div class="modal-body">
				<form id="add_others" class="form" method="post" action="add_others.php">
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2 form-group">
							<br>
							<select class="form-control" id="others_status" name="status" required>
								<option selected value="">Select Reason</option>
								<?php
								$sql = "SELECT activity FROM admin_activity_tbl WHERE flag='2'";
								$query = mysqli_query($conn, $sql);
								
								while ($rows = mysqli_fetch_array($query)) { ?>
									<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="row"></div>
					
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2 form-group">
							<textarea class="form-control" maxlength="1000" id="comments" name="comments"
									  style="min-height:100px;" placeholder="Comments..."></textarea>
						</div>
					</div>
					<div class="row"></div>
					<div class="row">
						<div class="col-xs-2 col-xs-offset-5 form-group">
							<br>
							<input class="form-control" type="submit" id="others_submit" name="others_submit"
								   value="Submit"
								   style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
						</div>
					</div>
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>">
					<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>">
					<input type="hidden" id="type" name="type" value="<?php echo $type; ?>">
					<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>">
				</form>
			</div>
		</div>
	</div>
</div>
<!-- add RNR 1 -->
<!-- Modal -->
<div class="modal fade" id="myModal_reschedule" role="dialog">
	<div class="modal-dialog">
		
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Reschedule (Id : <?php echo $booking_id; ?>)</h3>
			</div>
			<div class="modal-body">
				<form id="reschedule_booking" class="form" method="post" action="reschedule_booking.php">
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2 form-group">
							<br>
							<select class="form-control" id="reschedule_status" name="status" required>
								<option selected value="">Select Reason</option>
								<?php
								$sql = "SELECT activity FROM admin_activity_tbl WHERE flag='4'";
								$query = mysqli_query($conn, $sql);
								
								while ($rows = mysqli_fetch_array($query)) { ?>
									<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="row"></div>
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2 form-group">
							<textarea class="form-control" maxlength="1000" id="comments" name="comments"
									  style="min-height:100px;" placeholder="Comments..."></textarea>
						</div>
					</div>
					<div class="row"></div>
					<div class="row">
						<div class="col-xs-3 col-xs-offset-2 form-group">
							<label>&nbsp;Rechedule To</label>
						</div>
						<div class="col-xs-5  form-group">
							<div class="form-group">
								<div class='input-group date' id='datetimepicker4'>
									<input type='text' class="form-control" name="reschedule_to"/>
									<span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
								</div>
							</div>
						</div>
					</div>
					<div class="row"></div>
					<div class="row">
						<div class="col-xs-2 col-xs-offset-5 form-group">
							<br>
							<input class="form-control" type="submit" id="reschedule_submit" name="reschedule_submit"
								   value="Submit"
								   style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
						</div>
					</div>
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>">
					<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>">
					<input type="hidden" id="type" name="type" value="<?php echo $type; ?>">
					<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>">
				
				</form>
			</div>
		</div>
	</div>

</div>

<!-- Cancel Bookings -->
<!-- Modal -->
<div class="modal fade" id="myModal_cancel_booking" role="dialog">
	<div class="modal-dialog">
		
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Cancel Booking (Id : <?php echo $booking_id; ?>)</h3>
			</div>
			<div class="modal-body">
				<form id="cancel_booking" class="form" method="post" action="cancel_booking.php">
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2 form-group">
							<br>
							<select class="form-control" id="cancel_status" name="status" required>
								<option selected value="">Select Reason</option>
								<?php
								$sql = "SELECT activity FROM admin_activity_tbl WHERE flag='1'";
								$query = mysqli_query($conn, $sql);
								while ($rows = mysqli_fetch_array($query)) { ?>
									<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>
								<?php } ?>
								<?php
								if ($crm_log_id == 'crm029') { ?>
									
									<option value="Duplicate Booking">Duplicate Booking</option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="row"></div>
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2 form-group">
							<textarea class="form-control" maxlength="1000" id="cancel_comments" name="comments"
									  style="min-height:100px;" placeholder="Comments..." required></textarea>
						</div>
					</div>
					<div class="row"></div>
					<div class="row">
						<div class="col-xs-2 col-xs-offset-5 form-group">
							<br>
							<input class="form-control" type="submit" id="cancel_submit" name="cancel_submit"
								   value="Submit"
								   style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
						</div>
					</div>
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>">
					<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>">
					<input type="hidden" id="log" name="log" value="<?php echo $log; ?>">
					<input type="hidden" id="type" name="type" value="<?php echo $type; ?>">
					<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>">
				</form>
			</div>
		</div>
	</div>
</div>


<!-- Override Bookings -->
<!-- Modal -->
<div class="modal fade" id="myModal_override_booking" role="dialog">
	<div class="modal-dialog" style="width:1050px;">
		
		<!-- Modal content-->
		<div class="modal-content" style="height:611px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Override Booking (Id : <?php echo $booking_id; ?>)</h3>
			</div>
			<div class="modal-body">
				<form id="override_booking" class="form override_form" method="" action="">
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2 form-group">
							<br>
							<select class="form-control" id="override_reason" name="reason" required>
								<option selected value="">Select Reason</option>
								<option value="Recommended garage is too far">Recommended garage is too far</option>
								<option value="Customer wants Insurance Claim">Customer wants Insurance Claim</option>
								<option value="Customer wants Service on Sunday">Customer wants Service on Sunday
								</option>
								<option value="Customer Needs Dent Inspection">Customer Needs Dent Inspection</option>
								<option value="Customer wants to drop for dent removal">Customer wants to drop for dent
									removal
								</option>
								<option value="Customer need Insurance for dent">Customer need Insurance for dent
								</option>
								<option value="Customer wants garage with good infrastructure">Customer wants garage
									with good infrastructure
								</option>
								<option value="Customer wants a garage in pickup location">Customer wants a garage in
									pickup location
								</option>
								<option value="Customer wants a specific service">Customer wants a specific service
								</option>
								<option value="Customer wants drop for Dent Removal">Customer wants drop for Dent
									Removal
								</option>
								<option value="Customer wants Inspection">Customer wants Inspection</option>
								<option value="No Garage Recommended">No Garage Recommended</option>
							
							</select>
						</div>
					</div>
					<div class="row"></div>
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2 form-group">
							<textarea class="form-control" maxlength="1000" id="override_comments" name="comments"
									  style="min-height:100px;" placeholder="Comments..." required></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2 form-group" align="center">
							<p class="override_msg" style="color:gray;font-size:18px;"></p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-2 col-xs-offset-5 form-group">
							<br>
							<input class="form-control" type="button" onClick="override_confirm('1')"
								   id="override_submit" value="Submit"
								   style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
						</div>
					</div>
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>">
					<input type="hidden" id="override_log" name="log" value="<?php echo $today_time; ?>">
					<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>">
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="override_push_confirm" role="dialog">
	<div class="modal-dialog">
		
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Push to override (ID: <?php echo $booking_id; ?>)</h3>
			</div>
			<div class="modal-body" align="center">
				<h4>Are you sure you want to push to overrides ? </h4>
				<div class="modal-footer" style="border-top:10px solid #fff;">
					<button type="button" class="btn" data-dismiss="modal"
							style="background-color:rgba(255, 82, 0, 0.67);">No
					</button>
					<button type="button" class="btn" onClick="override_request('1')" style="background-color:#69ED85;">
						Yes
					</button>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Override Later -->
<!-- Modal -->
<div class="modal fade" id="myModal_override_later" role="dialog">
	<div class="modal-dialog" style="width:1050px;">
		<!-- Modal content-->
		<div class="modal-content" style="height:611px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Override Booking (Id : <?php echo $booking_id; ?>)</h3>
			</div>
			<div class="modal-body">
				<form id="override_later" class="form override_form" method="" action="">
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2 form-group">
							<br>
							<select class="form-control" id="override_unable" name="reason" required>
								<option selected value="">Select Reason</option>
								<option value="Cannot be connected">Cannot be connected</option>
								<option value="Can be connected">Can be connected</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2 form-group">
							<select class="form-control" id="override_later_reason" name="reason" required>
								<option selected value="">Select Reason</option>
								<option value="All garages in the region are capped">All garages in the region are
									capped
								</option>
								<option value="No attendance in location">No attendance in location</option>
								<option value="No garage available in the location">No garage available in the
									location
								</option>
								<option value="Garages in the region do not deal with selected service type">Garages in
									the region do not deal with selected service type
								</option>
							</select>
							<select class="form-control" id="override_cancel_reason" name="reason" required>
								<option selected value="">Select Reason</option>
								<option value="No garage in the vicinity">No garage in the vicinity</option>
								<option value="No garages deal with service type">No garages deal with service type
								</option>
								<option value="Garages do not deal with brand/model">Garages do not deal with
									brand/model
								</option>
								<option value="Out of serviceable region">Out of serviceable region</option>
								<option value="No garage does Insurance Claim">No garage does Insurance Claim</option>
								<option value="No garage does  Service on Sunday">No garage does Service on Sunday
								</option>
								<option value="No garage with good infrastructure">No garage with good infrastructure
								</option>
							</select>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2 form-group">
							<textarea class="form-control" maxlength="1000" id="override_later_comments" name="comments"
									  style="min-height:100px;" placeholder="Reason for later override"
									  required></textarea>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-4 col-md-offset-4 input-group date override_date" align="center"
							 style="margin-top:10px;">
							<!-- <label> Service Date</label></div>
			<label> Service Date</label></div>
<div class="col-xs-3  form-group">
	<div class="col-xs-3  form-group"> -->
							<input class="form-control datepicker" placeholder="Override date"
								   data-date-format='dd-mm-yyyy' type="text" id="override_date" name="override_date"
								   autocomplete="off" required>
							<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar "></span>
				<span>
						
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2 form-group" align="center">
							<p class="override_msg" style="color:gray;font-size:18px;"></p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-2 col-xs-offset-5 form-group">
							<br>
							<input class="form-control" type="button" onClick="override_confirm('2')"
								   id="override_submit1" value="Submit"
								   style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
						</div>
					</div>
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>">
					<input type="hidden" id="override_log" name="log" value="<?php echo $today_time; ?>">
					<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>">
				</form>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="override_later_confirm" role="dialog">
	<div class="modal-dialog">
		
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Override later (ID:<?php echo $booking_id; ?>)</h3>
			</div>
			<div class="modal-body" align="center">
				<h4 id="modal_msg">Are you sure you want to connect this override booking later? </h4>
				<div class="modal-footer" style="border-top:10px solid #fff;">
					<button type="button" class="btn" data-dismiss="modal"
							style="background-color:rgba(255, 82, 0, 0.67);">No
					</button>
					<button type="button" class="btn" onClick="override_request('2')" style="background-color:#69ED85;">
						Yes
					</button>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- brand and model -->
<script>
    $(function () {
        $("#BrandModelid").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "ajax/get_brandmodel.php",
                    data: {
                        term: request.term,
                        extraParams: $('#veh:checked').val()
                    },
                    dataType: 'json'
                }).done(function (data) {
                    //console.log(data);
                    if (data != null) {
                        response($.map(data, function (item) {
                            return item;
                        }));
                    } else {
                        response($.map(data, function (item) {
                            return "No Results";
                        }));
                    }
                });
            },
            appendTo: $("#BrandModelid").next(),
            delay: 0,
            minLength: 0,
            response: function (event, ui) {
                if (!ui.content.length) {
                    var noResult = {value: "", label: "No results found"};
                    ui.content.push(noResult);
                } else {
                    $("#message").empty();
                }
            },
            open: function (event, ui) {
                $(".ui-autocomplete").css("position", "absolute");
                $(".ui-autocomplete").css("z-index", "9999999");
            },
            change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function () {
            $(this).autocomplete("search");
        });
    });

</script>
<!-- user location -->
<script>
    $("#location_home").click(function () {
        var c = $('#cityu').val();
        //console.log(c);
        if (c == '') {
            alert("Plese select City to get localityies!");
        } else {
            $(function () {

                $("#location_home").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "ajax/get_user_locality.php",
                            type: "GET",
                            data: {
                                term: request.term,
                                city: $('#cityu').val()
                            },
                            dataType: 'json'
                        }).done(function (data) {
                            //alert(data);
                            if (data != null) {
                                response($.map(data, function (item) {
                                    return item;
                                }));
                            } else {
                                response($.map(data, function (item) {
                                    return "No Results";
                                }));
                            }
                        });
                    },
                    appendTo: $("#location_home").next(),
                    delay: 0,
                    minLength: 0,
                    response: function (event, ui) {
                        if (!ui.content.length) {
                            var noResult = {value: "", label: "No results found"};
                            ui.content.push(noResult);
                        } else {
                            $("#message").empty();
                        }
                    },
                    open: function (event, ui) {
                        $(".ui-autocomplete").css("position", "absolute");
                        $(".ui-autocomplete").css("z-index", "99999");
                    },
                    change: function (event, ui) {
                        if (!(ui.item)) event.target.value = "";
                    }
                }).focus(function () {
                    $(this).autocomplete("search");
                });
            });

        }
    });

</script>
<!-- locality work -->
<script>
    $("#location_work").click(function () {
        var c = $('#cityu').val();
        //console.log(c);
        if (c == '') {
            alert("Plese select City to get localityies!");
        } else {
            $(function () {
                $("#location_work").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "ajax/get_work_locality.php",
                            type: "GET",
                            data: {
                                term: request.term,
                                city: $('#cityu').val()
                            },
                            dataType: 'json'
                        }).done(function (data) {
                            //alert(data);
                            if (data != null) {
                                response($.map(data, function (item) {
                                    return item;
                                }));
                            } else {
                                response($.map(data, function (item) {
                                    return "No Results";
                                }));
                            }
                        });
                    },
                    appendTo: $("#location_work").next(),
                    delay: 0,
                    minLength: 0,
                    response: function (event, ui) {
                        if (!ui.content.length) {
                            var noResult = {value: "", label: "No results found"};
                            ui.content.push(noResult);
                        } else {
                            $("#message").empty();
                        }
                    },
                    open: function (event, ui) {
                        $(".ui-autocomplete").css("position", "absolute");
                        $(".ui-autocomplete").css("z-index", "99999");
                    },
                    change: function (event, ui) {
                        if (!(ui.item)) event.target.value = "";
                    }
                }).focus(function () {
                    $(this).autocomplete("search");
                });
            });
        }
    });
</script>
<!-- empty autocompletes localities on city change -->
<script>
    $(document).ready(function () {
        $('.alert-status').bootstrapSwitch();

        var a = "<?php echo "$b_pickup";?>";
        $("#pickupe").val(a);
        $('#pickup_switch').on('switchChange.bootstrapSwitch', function (event, state) {
            //console.log(state);
            var s;
            if (state == true)
                s = 1;
            else
                s = 0;
            $("#pickupe").val(s);
        });
        $('#doorstep_switch').on('switchChange.bootstrapSwitch', function (event, state) {
            //console.log(state);
            var s;
            if (state == true)
                s = 1;
            else
                s = 0;
            $("#doorstep_only").val(s);
        });
        $('#re_switch').on('switchChange.bootstrapSwitch', function (event, state) {
            //console.log(state);
            var s;
            if (state == true)
                s = 1;
            else
                s = 0;
            $("#re_specialist").val(s);
        });

        $('#cityu').change(function () {
            $('#location_work').empty();
            $('#location_work').autocomplete('close').val('');
            $('#location_home').empty();
            $('#location_home').autocomplete('close').val('');
        });
    })
</script>

<!-- reg number -->
<!-- masking -->
<script type="text/javascript" src="js/inputmask.js"></script>
<script type="text/javascript" src="js/jquery.inputmask.js"></script>
<script type="text/javascript" src="js/inputmask.extensions.js"></script>

<script>
    Inputmask("A{2}/9{2}/A{2}/9{4}").mask($("#regno"));
</script>

<!-- select Vehicle Number  -->
<script>
    $(document).ready(function ($) {
        $(document).on('change', '[name="veh_b"]', function () {
            var selectvalue = $('[name="veh_b"]:checked').val();
            var selectuser = <?php echo $user_id; ?>;
            var veh_id = <?php echo $veh_id;?>;
            $("#veh_no").empty();
            $("div.veh").show();
            //Make AJAX request, using the selected value as the POST
            $.ajax({
                url: "ajax/get_veh.php",  // create a new php page to handle ajax request
                type: "POST",
                data: {"selectvalue": selectvalue, "selectuser": selectuser, "veh_id": veh_id},
                success: function (data) {
                    // alert(data);
                    $('#veh_no').append(data);
                    $("#location").autocomplete('close').val('');
                    $("#service_type").autocomplete('close').val('');
                    $("#mechanic").empty();
                    $("#mechanic").append('<option value="">Select Mechanic</option>');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    // alert(xhr.status + " "+ thrownError);
                }

            });

        });
    });
</script>
<!-- select vehicle on page load -->
<script>
    $(document).ready(function ($) {
        var selectvalue = $('[name="veh_b"]:checked').val();
        var selectuser = <?php echo $user_id; ?>;
        var veh_id = <?php echo $veh_id;?>;
        var veh_no = $('#veh_no').val();
        $("#veh_no").empty();
        $("div.veh").show();
        // $("#location").autocomplete('close').val('');
        // $("#mechanic").empty();
        // $("#mechanic").append('<option value="">Select Mechanic</option>');
        //Make AJAX request, using the selected value as the POST
        $.ajax({
            url: "ajax/get_veh.php",  // create a new php page to handle ajax request
            type: "POST",
            data: {"selectvalue": selectvalue, "selectuser": selectuser, "veh_no": veh_no, "veh_id": veh_id},
            success: function (data) {
                // alert(data);
                $('#veh_no').append(data);

            },
            error: function (xhr, ajaxOptions, thrownError) {
                // alert(xhr.status + " "+ thrownError);
            }

        });

    });
</script>

<!-- select Service type -->
<!-- select Service type -->
<script>
    $(function () {
        $("#service_type").autocomplete({
            source: function (request, response) {
                var selectloc_data = $("#location_pickup").val();
                var res = selectloc_data.split(", ");
                var selectloc = res[0];
                var city = res[1];
                $.ajax({
                    url: "ajax/get_service_type.php",
                    data: {
                        term: request.term,
                        //type : $('#veh_b:checked').val()
                        type: $('#veh_b:checked').val(),
                        vtype: $('#veh_no').val(),
                        book_id: $('#book_id').val(),
                        city: $('#city').val()
                    },
                    dataType: 'json'
                }).done(function (data) {
                    //alert(data);
                    if (data != null) {
                        response($.map(data, function (item) {
                            return item;
                        }));
                    } else {
                        response($.map(data, function (item) {
                            return "No Results";
                        }));
                    }
                });
            },
            appendTo: $("#service_type").next(),
            delay: 0,
            minLength: 0,
            response: function (event, ui) {
                if (!ui.content.length) {
                    var noResult = {value: "", label: "No results found"};
                    ui.content.push(noResult);
                } else {
                    $("#message").empty();
                }
            },
            open: function (event, ui) {
                $(".ui-autocomplete").css("position", "absolute");
                $(".ui-autocomplete").css("z-index", "99999");
            },
            change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function () {
            $(this).autocomplete("search");
        });
    });

</script>

<!-- select Mechanic Based On Location -->
<script>
    $(document).ready(function ($) {
        $("#late_override_btn").hide();
        $("#location").on("autocompletechange", function () {
            //	var selectservice = $("#service_type").val();
            var selecttype = $('[name="veh_b"]:checked').val();
            var selectloc = $("#location").val();
            var veh_no = $("#veh_no").val();
            var service = $("#service_type").val();
            var book_id = $("#book_id").val();
            var city = $("#city").val();
            var doorstep_only = $("#doorstep_only").val();
            var re_specialist = $("#re_specialist").val();
            var crm = '<?php echo $crm_log_id; ?>';
            var type = '<?php echo $type; ?>';
            var service_date = $("#service_date").val();
            var se = '<?php echo $_SESSION['crm_se']; ?>';

            console.log(se);
            $("#mechanic").empty();
            $("div.mec").show();
            //Make AJAX request, using the selected value as the POST
            // if(crm != 'crm036' && crm != 'crm012' && crm != 'crm041' && crm != 'crm018')
            if (se != '1' && crm != 'crm020' && crm != 'crm043' && crm != 'crm003' && crm != 'crm158' && crm != 'crm160' && crm != 'crm123' && crm != 'crm135' && crm != 'crm161' && crm != 'crm175' && crm != 'crm187' && crm != 'crm193' && crm != 'crm178' && crm != 'crm174' && crm != 'crm036' && crm != 'crm018' && crm != 'crm032' && crm != 'crm227' && crm != 'crm239' && crm != 'crm237' && crm != 'crm202' && crm != 'crm225' && crm != 'crm263' && crm != 'crm261' && crm != 'crm231' && crm != 'crm139' && crm != 'crm017' && crm != 'crm186' && crm != 'crm018' && crm != 'crm274' && crm != 'crm269' && crm != 'crm320' && crm != 'crm321' && crm != 'crm365' && crm != 'crm014' && crm != 'crm361' && crm != 'crm189') {
                // if(crm != 'crm036'  && crm != 'crm043' && crm !='crm018' && crm !='crm003' && crm !='crm064')
                // if((selecttype === '2w' || service === 'Express Car Service 999' || service === 'Car dent / scratch removal' || service === 'Dent Removal 2499' || service === 'Dent 2999 1-2 Panels' || service === 'Dent 2999 3-4 Panels' || service === 'Dent 2999 5-6 Panels' || service === 'Dent 2999 7-8 Panels' || service === 'Dent Removal 2999') && crm != 'crm036' && city == 'Chennai' && crm != 'crm012' && crm != 'crm041')
                // if(selecttype === '2w' && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
                $('.loader').show();
                $('.loader_overlay').show();
                $.ajax({
                    url: "ajax/project_crown.php",  // create a new php page to handle ajax request
                    type: "POST",
                    data: {
                        "selecttype": selecttype,
                        "selectloc": selectloc,
                        "veh_no": veh_no,
                        "service": service,
                        "book_id": book_id,
                        "crm_log_id": crm,
                        "doorstep_only": doorstep_only,
                        "re_specialist": re_specialist,
                        "service_date": service_date
                    },
                    success: function (data) {
                        $('.loader').hide();
                        $('.loader_overlay').hide();
                        // console.log(data);
                        //alert(data);
                        $('#mechanic').append(data);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        // alert(xhr.status + " "+ thrownError);
                    }

                });

            } else {
                $.ajax({
                    url: "ajax/get_mechanic_override.php",  // create a new php page to handle ajax request
                    type: "POST",
                    data: {
                        "selecttype": selecttype,
                        "selectloc": selectloc,
                        "veh_no": veh_no,
                        "service": service,
                        "crm": crm,
                        "city": city
                    },
                    success: function (data) {
                        //console.log(data);
                        //alert(data);
                        $('#mechanic').append(data);
                        $("#over_show").show();
                        var a = new Array();
                        $("#mechanic").children("option").each(function (x) {
                            test = false;
                            b = a[x] = $(this).val();
                            for (i = 0; i < a.length - 1; i++) {
                                if (b == a[i]) {
                                    test = true;
                                }
                            }
                            if (test) {
                                $(this).remove();
                            }
                        });

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        // alert(xhr.status + " "+ thrownError);
                    }

                });
            }

        });
    });
</script>


<!-- --------------------------------------- edit booking ------------------------------------------------------ -->
<!-- select Vehicle Number  -->
<script>


    $(document).on('change', '[name="veh_be"]', function () {
        var selectvalue = $('[name="veh_be"]:checked').val();
        if (selectvalue === '2w') {
            $("#doorstep_toggle").hide();
            $("#re_toggle").show();
        } else {
            $("#doorstep_toggle").show();
            $("#re_toggle").hide();
        }
        if (selectvalue != '<?php echo "$b_veh"; ?>') {
            var selectuser = <?php echo $user_id; ?>;
            var veh_id = <?php echo $veh_id;?>;
            $("#veh_noe").empty();

            $("div.veh").show();
            //Make AJAX request, using the selected value as the POST
            $.ajax({
                url: "ajax/get_veh.php",  // create a new php page to handle ajax request
                type: "POST",
                data: {"selectvalue": selectvalue, "selectuser": selectuser, "veh_id": veh_id},
                success: function (data) {
                    // alert(data);
                    $('#veh_noe').append(data);
                    // $("#locatione").autocomplete('close').val('');
                    $("#location_pickup").autocomplete('close').val('');
                    $("#service_typee").autocomplete('close').val('');
                    $("#mechanice").empty();
                    $("#mechanice").append('<option value="">Select Mechanic</option>');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    // alert(xhr.status + " "+ thrownError);
                }

            });
        } else {
            var selectuser = <?php echo $user_id; ?>;
            var veh_id = <?php echo $veh_id;?>;
            $("#veh_noe").empty();

            $("div.veh").show();
            //Make AJAX request, using the selected value as the POST
            $.ajax({
                url: "ajax/get_veh.php",  // create a new php page to handle ajax request
                type: "POST",
                data: {"selectvalue": selectvalue, "selectuser": selectuser, "veh_id": veh_id},
                success: function (data) {
                    // alert(data);
                    $('#veh_noe').append(data);
                    $("#location_pickup").val('<?php if ($pickup_location == "" || $pickup_location == "-") {
						echo $b_locality;
					} else {
						echo $pickup_location;
					} ?>');
                    var source = '<?php echo $source;?>';
                    if (selectvalue == '2w' && source != 'Hub Booking') {
                        $("#service_typee").val("");
                    } else {
                        $("#service_typee").val('<?php echo $b_service_type; ?>');
                    }
                    $("#mechanice").empty();
                    $("#mechanice").append('<?php if($b_shop_name == "" || $b_shop_name == "0"){ ?>
                        < option
                    selected
                    value = "" > Select
                    Mechanic < /option><?php } else{ ?><option selected value="<?php echo $b_mec_id; ?>"><?php echo $b_shop_name; ?></
                    option > <?php } ?>');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    // alert(xhr.status + " "+ thrownError);
                }

            });
        }
    });
    //});
</script>
<!-- select vehicle on page loaad -->
<script>
    $(document).ready(function ($) {
        var selectvalue = $('[name="veh_be"]:checked').val();
        if (selectvalue === '2w') {
            $("#doorstep_toggle").hide();
            $("#re_toggle").show();
        } else {
            $("#doorstep_toggle").show();
            $("#re_toggle").hide();
        }
        var selectuser = <?php echo $user_id; ?>;
        var veh_id = <?php echo $veh_id;?>;
        $("#veh_noe").empty();
        $("div.veh").show();
        //Make AJAX request, using the selected value as the POST
        $.ajax({
            url: "ajax/get_veh.php",  // create a new php page to handle ajax request
            type: "POST",
            data: {"selectvalue": selectvalue, "selectuser": selectuser, "veh_id": veh_id},
            success: function (data) {
                // alert(data);
                $('#veh_noe').append(data);
                // $("#locatione").autocomplete('close').val('');
                // $("#mechanice").empty();
                // $("#mechanice").append('<option valu="">Select Mechanic</option>');

            },
            error: function (xhr, ajaxOptions, thrownError) {
                // alert(xhr.status + " "+ thrownError);
            }

        });

    });
</script>

<!-- select Service type -->
<script>
    $(function () {
        $("#service_typee").autocomplete({
            source: function (request, response) {
                var selectloc_data = $("#location_pickup").val();
                console.log(selectloc_data);
                var res = selectloc_data.split(", ");
                var selectloc = res[0];
                var city = res[1];
                $.ajax({
                    url: "ajax/get_service_type.php",
                    data: {
                        term: request.term,
                        //type : $('#veh_b:checked').val()
                        type: $('#veh_be:checked').val(),
                        vtype: $('#veh_noe').val(),
                        book_id: $('#book_id').val(),
                        city: city
                    },
                    dataType: 'json'
                }).done(function (data) {
                    //alert(data);
                    if (data != null) {
                        response($.map(data, function (item) {
                            return item;
                        }));
                    } else {
                        response($.map(data, function (item) {
                            return "No Results";
                        }));
                    }
                });
            },
            appendTo: $("#service_typee").next(),
            delay: 0,
            minLength: 0,
            response: function (event, ui) {
                if (!ui.content.length) {
                    var noResult = {value: "", label: "No results found"};
                    ui.content.push(noResult);
                } else {
                    $("#message").empty();
                }
            },
            open: function (event, ui) {
                $(".ui-autocomplete").css("position", "absolute");
                $(".ui-autocomplete").css("z-index", "99999");
            },
            change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function () {
            $(this).autocomplete("search");
        });
    });

</script>
<!-- select Location of service center -->
<script>
    $(function () {
        $("#location").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "ajax/get_location.php",
                    data: {
                        term: request.term,
                        //type : $('#veh_b:checked').val()
                        vtype: $('[name="veh_b"]:checked').val(),
                    },
                    dataType: 'json'
                }).done(function (data) {
                    //alert(data);
                    if (data != null) {
                        response($.map(data, function (item) {
                            return item;
                        }));
                    } else {
                        response($.map(data, function (item) {
                            return "No Results";
                        }));
                    }
                });
            },
            appendTo: $("#location").next(),
            delay: 0,
            minLength: 0,
            response: function (event, ui) {
                if (!ui.content.length) {
                    var noResult = {value: "", label: "No results found"};
                    ui.content.push(noResult);
                } else {
                    $("#message").empty();
                }
            },
            open: function (event, ui) {
                $(".ui-autocomplete").css("position", "absolute");
                $(".ui-autocomplete").css("z-index", "99999");
            },
            change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function () {
            $(this).autocomplete("search");
        });
    });

</script>

<!-- edit booking select Location of service center -->
<script>
    /*$(function() {
			    $( "#locatione" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_location.php",
                data: {
                    term: request.term,
					//type : $('#veh_b:checked').val()
					vtype: $('[name="veh_be"]:checked').val(),
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#locatione").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});*/

    //Pickup location
    $(function () {
        $("#location_pickup").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "ajax/get_location_pickup.php",
                    data: {
                        term: request.term,
                        //type : $('#veh_b:checked').val()
                        vtype: $('[name="veh_be"]:checked').val(),
                        booking_id: $("#book_id").val(),
                    },
                    dataType: 'json'
                }).done(function (data) {
                    //alert(data);
                    if (data != null) {
                        response($.map(data, function (item) {
                            return item;
                        }));
                    } else {
                        response($.map(data, function (item) {
                            return "No Results";
                        }));
                    }
                });
            },
            appendTo: $("#location_pickup").next(),
            delay: 0,
            minLength: 0,
            response: function (event, ui) {
                if (!ui.content.length) {
                    var noResult = {value: "", label: "No results found"};
                    ui.content.push(noResult);
                } else {
                    $("#message").empty();
                }
            },
            open: function (event, ui) {
                $(".ui-autocomplete").css("position", "absolute");
                $(".ui-autocomplete").css("z-index", "99999");
            },
            change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function () {
            $(this).autocomplete("search");
        });
    });

</script>

<script>

    $(document).ready(function ($) {
        $("#override_cancel_reason").hide();
        $("#override_later_reason").hide();
        $("#override_unable").change(function () {
            var selectvalue = $("#override_unable").val();

            if (selectvalue == "Cannot be connected") {
                $('.override_date').hide();
                $('#override_date').val('Null');
                $("#override_cancel_reason").show();
                $("#override_later_reason").hide();
                var later_rsn = document.getElementById('override_cancel_reason').value
            } else {
                $('#override_date').val('');
                $('.override_date').show();
                $("#override_later_reason").show();
                $("#override_cancel_reason").hide();
                var later_rsn = document.getElementById('override_later_reason').value
            }
        });
    });

    function override_confirm(id) {
        var reason = document.getElementById("override_reason").value
        var comments = document.getElementById('override_comments').value
        var user_id = document.getElementById('user_id').value
        var book_id = document.getElementById('book_id').value
        var log = document.getElementById('override_log').value
        var override_later = document.getElementById('override_unable').value;
        if (override_later == "Cannot be connected") {
            var later_rsn = document.getElementById('override_cancel_reason').value;
            var status = "2";
            $("#modal_msg").html("Are you sure you will not be able to connect the lead again");
        } else if (override_later == "Can be connected") {
            var later_rsn = document.getElementById('override_later_reason').value;
            $("#modal_msg").html("Are you sure you can connect this lead again.");
            var status = "1";
        } else {
            var status = "0";
            var later_rsn = 'none';
            var later_cmnts = 'none';
        }
        var later_cmnts = document.getElementById('override_later_comments').value;
        var override_update_date = document.getElementById('override_date').value;
        if (id == 1) {
            if (reason == '' || comments == '') {
                $('.override_msg').html("All fields are required ");
            } else {
                $('#override_push_confirm').modal('show');
            }
        } else if (override_later == '' || later_rsn == '' || later_cmnts == '' || override_update_date == '') {
            $('.override_msg').html("All fields are required");
        } else {
            $("#override_later_confirm").modal('show');
        }
    }

    function override_request() {
        $('.override_msg').empty();
        var reason = document.getElementById("override_reason").value
        var comments = document.getElementById('override_comments').value
        var user_id = document.getElementById('user_id').value
        var book_id = document.getElementById('book_id').value
        var log = document.getElementById('override_log').value
        var override_later = document.getElementById('override_unable').value;
        if (override_later == "Cannot be connected") {
            var later_rsn = document.getElementById('override_cancel_reason').value;
            var status = "2";
        } else if (override_later == "Can be connected") {
            var later_rsn = document.getElementById('override_later_reason').value;
            var status = "1";
        } else {
            var status = "0";
            var later_rsn = 'none';
            var later_cmnts = 'none';
        }
        var later_cmnts = document.getElementById('override_later_comments').value;
        var override_update_date = document.getElementById('override_date').value;
        console.log(override_update_date);
        // if(later_rsn == ''){
        //  var later_rsn= 'none';
        //  var later_cmnts = 'none';
        // }
        var type = '<?php echo $type ?>';
        var flag = '<?php echo $_SESSION['flag'] ?>';
        var ire = '<?php echo $ire_flag; ?>';
        // if(id== 1){
        //     if(reason == '' || comments == ''){
        //         $('.override_msg').append("All fields are required ");
        //     }
        //     else {
        var data = {
            reason: reason,
            comments: comments,
            user_id: user_id,
            book_id: book_id,
            log: log,
            override_later: override_later,
            later_rsn: later_rsn,
            status: status,
            later_cmnts: later_cmnts,
            override_update_date: override_update_date
        };
        jQuery.ajax({
            url: 'override_booking.php',
            type: 'POST',
            data: data,
            success: function (data) {
                if (data == "inserted") {
                    $('.override_msg').append("Booking pushed to overrides");
                } else if (data == "updated") {
                    $('.override_msg').append("Override successfully updated");
                } else if (data == "exists") {
                    $('.override_msg').append("Override already exists");
                }
                if (type == "or") {
                    window.location.href = "overrides.php";
                } else {
                    if (flag == '0') {
                        if (ire == '1') {
                            window.location.href = "ire_panel/prospect_goaxle.php";
                        } else {
                            window.location.href = "leads.php";
                        }

                    } else {
                        if (ire == '1') {
                            window.location.href = "ire_panel/prospect_goaxle.php";
                        } else {
                            window.location.href = "leads.php";
                        }
                    }
                }
                $('#override_booking')[0].reset();
                $('#override_later')[0].reset();
                $('.override_msg').delay(9000).fadeOut('slow');
            }
        });
        // }
        // }
        //         else if(id == 2){
        //             if(override_later == '' || later_rsn == ''  || later_cmnts == ''  || override_update_date == ''){
        //                 $('.override_msg').append("All fields are required");
        //             }
        //             else {
        //                 var data = {reason: reason, comments: comments, user_id: user_id, book_id: book_id, log: log,override_later:override_later,later_rsn:later_rsn,status:status,later_cmnts:later_cmnts,override_update_date:override_update_date};
        //   jQuery.ajax({
        //         url: 'override_booking.php',
        //         type: 'POST',
        //         data: data,
        //         success: function(data) {
        //                         if(data == "inserted"){
        //                             $('.override_msg').append("Booking pushed to overrides");
        //                         }
        //                         else if(data == "updated"){
        //                             $('.override_msg').append("Override successfully updated");
        //                         }
        //                         else if(data== "exists"){
        //                             $('.override_msg').append("Override already exists");
        //                         }
        //                         if(type == "or"){
        //                             window.location.href = "overrides.php";
        //                         }
        //                         else {
        //                             window.location.href = "aleads.php";
        //                         }
        //                         $('#override_booking')[0].reset();
        //                         $('#override_later')[0].reset();
        //                         $('.override_msg').delay(9000).fadeOut('slow');
        //         }
        //   });
        //             }
        //         }

    }
</script>

<!-- select Mechanic Based On Location -->
<script>
    $(document).ready(function ($) {
        $("#service_typee").on("autocompletechange", function () {
            console.log('he');
            $('#location_pickup').attr('value', '');
            $("#mechanice").empty();
        });

        /*$("#location_pickup").on("autocompletechange", function(){
  	//var selectservice = $("#service_typee").val();
	var selecttype = $('[name="veh_be"]:checked').val();
	var selectloc = $("#location_pickup").val();
  var veh_no = $("#veh_noe").val();
  var service = $("#service_typee").val();
  var book_id = $("#book_id").val();
  var city = $("#city").val();
  var doorstep_only = $("#doorstep_only").val();
  var re_specialist = $("#re_specialist").val();
  var crm = '<?php echo $crm_log_id; ?>';
  var type = '<?php echo $type; ?>';
  console.log(crm);
	$("#mechanice").empty();

		 $("div.mece").show();
          //Make AJAX request, using the selected value as the POST
		  // if(crm != 'crm036' && crm != 'crm012' && crm != 'crm041' && crm != 'crm018')

		  			  if(crm != 'crm036' && crm != 'crm046' && crm != 'crm043' && crm != 'crm018' && crm !='crm003')
		  // if((selecttype === '2w' || service === 'Express Car Service 999' || service === 'Car dent / scratch removal' || service === 'Dent Removal 2499' || service === 'Dent 2999 1-2 Panels' || service === 'Dent 2999 3-4 Panels' || service === 'Dent 2999 5-6 Panels' || service === 'Dent 2999 7-8 Panels' || service === 'Dent Removal 2999') && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
		  // if(selecttype === '2w' && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
		  {
			  $('.loader').show();
			  $('.loader_overlay').show();
			  $.ajax({
            url : "ajax/project_crown.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc ,"veh_no": veh_no ,"service":service,"book_id":book_id,"crm_log_id":crm,"doorstep_only":doorstep_only,"re_specialist":re_specialist},
            success : function(data) {
				// console.log(data);
				$('.loader').hide();
			  $('.loader_overlay').hide();
				//alert(data);
                $('#mechanice').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrown	Error);
          }

        });
			
		  }
		  else
		  {
		  $.ajax({
            url : "ajax/get_mechanic_loc.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc ,"veh_no": veh_no ,"service":service},
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanice').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });
		}
    });*/
    });
</script>
<!-- select Mechanic Based On Location  on page load-->
<!--<script>
$(document).ready(function($) {
 //$("#location_pickup").on("autocompletechange", function(){
  	//var selectservice = $("#service_typee").val();
	var selecttype = $('[name="veh_be"]:checked').val();
	var selectloc = $("#location_pickup").val();
  var veh_no = $("#veh_noe").val();
  var service = $("#service_typee").val();
  var book_id = $("#book_id").val();
  var city = $("#city").val();
  var crm = '<?php echo $crm_log_id; ?>';
  var type = '<?php echo $type; ?>';
  console.log(crm);
  if(selectloc != ""){
    $("#mechanice").empty();

		 $("div.mece").show();
          //Make AJAX request, using the selected value as the POST
		  if((selecttype === '2w' || service === 'Express Car Service 999' || service === 'Car dent / scratch removal' || service === 'Dent Removal 2499' || service === 'Dent 2999 1-2 Panels' || service === 'Dent 2999 3-4 Panels' || service === 'Dent 2999 5-6 Panels' || service === 'Dent 2999 7-8 Panels' || service === 'Dent Removal 2999') && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
		  // if(selecttype === '2w' && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
		  {
			  $('.loader').show();
			  $('.loader_overlay').show();
			  $.ajax({
            url : "ajax/project_crown.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc ,"veh_no": veh_no ,"service":service,"book_id":book_id},
            success : function(data) {
				// console.log(data);
				$('.loader').hide();
				$('.loader_overlay').hide();
				//alert(data);
                $('#mechanice').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });
			
		  }
		  else
		  {
		  $.ajax({
            url : "ajax/get_mechanic_loc.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc,"veh_no": veh_no , "service":service},
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanice').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });
		  }
  }
});
</script>-->


<!-- --------------------------------------- edit booking ----------------------------------------------- -->
<!-- validation -->
<script>
    var userinput = document.getElementById('user_name');
    userinput.oninvalid = function (event) {
        document.getElementById("user_name").style.borderColor = "#E42649";
        event.target.setCustomValidity('Only aplhabets and digits are allowed!');
    }
</script>
<script type="text/javascript">
    $(function () {
        var date = new Date();

//  $(".next_service").focusin(function() {
// 	var service_date = $("#service_date").val();
// 	console.log(service_date);
// 	$('.next_service').datepicker({
// 			autoclose: true,
//    startDate: service_date
//     // startDate: date
// });

//  });

        $(".service_date").datepicker({
            todayBtn: 1,
            autoclose: true,
        }).on('changeDate', function (selected) {
            var minDate = new Date(selected.date.valueOf());
            $('.next_service_date').val('');
            $('.next_service_date').datepicker('setStartDate', minDate);
        });

        $(".next_service_date").datepicker({
            autoclose: true,
        });
        // .on('changeDate', function (selected) {
        //     var minDate = new Date(selected.date.valueOf());
        //     $('.service').datepicker('setEndDate', minDate);
        // });
// 		$('.next_service').datepicker({
// 			autoclose: true,
//    startDate: service_date
//     // startDate: date
// });


// console.log(service_date);


        date.setDate(date.getDate());

        $('.datepicker').datepicker({
            autoclose: true,
            startDate: date,
            todayHighlight: true
            // startDate: date
        });

        $(".datepicker").focus(function () {
            //console.log('in');
        }).blur(function () {
            // 	var service_date = $("#service_date").val();
            // console.log(service_date);
            if ($("#service_datee").val() == "") {
                var s_date = "<?php echo "$b_service_date";?>";
                var rs_date = s_date.split("-").reverse().join("-");
                $("#service_datee").datepicker().val(rs_date);
            }

            if ($("#next_service_date").val() == "") {
                var ns_date = "<?php echo "$u_next_serviced";?>";
                var rns_date = ns_date.split("-").reverse().join("-");
                $("#next_service_date").datepicker().val(rns_date);
            }
            if ($("#follow_up_date").val() == "") {
                var fu_date = "<?php echo "$u_followup";?>";
                var rfu_date = fu_date.split("-").reverse().join("-");
                $("#follow_up_date").datepicker().val(rfu_date);
            }

        })
// if($('input.datepicker').val()){
//     $('input.datepicker').datepicker('setDate', 'today');
//  }
    });
</script>
<!-- date time picker -->
<script type="text/javascript">
    $(function () {
        var dateNow = new Date();
        var pc_date = "<?php if ($pickup_date_time == '01-01-1970 05:30') {
			echo "$today_time";
		} else {
			echo "$pickup_date_time";
		}?>";
        $('#datetimepicker7').datetimepicker({
            format: 'DD-MM-YYYY HH:mm',
            minDate: dateNow
        });
        $("#datetimepicker7").focusin(function () {
            var pcdate = $("#pickup_date").val();
            if (pcdate == "") {
                $("#pickup_date").val(pc_date);
            }
        });
        $("#datetimepicker7").focusout(function () {
            if (pcdate == "") {
                $("#pickup_date").val(pc_date);
            }
        });
        $("#pickup_date").val(pc_date);
        $('#datetimepicker6').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            minDate: dateNow,
            defaultDate: dateNow
        });
        $('#datetimepicker1').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            defaultDate: dateNow
        });
        $('#datetimepicker2').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            defaultDate: dateNow
        });
        $('#datetimepicker3').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            defaultDate: dateNow
        });
        $('#datetimepicker4').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            defaultDate: dateNow
        });
    });

</script>
<!-- select veh before fetching models -->
<script>
    $(document).ready(function () {
        $("#BrandModelid").click(function () {
            var veh = $('input[name=veh]:checked').val();
            //console.log(veh);
            if (veh == null) {
                //console.log(veh);
                alert("Please select vehicle type to get vehicle models!");
            }
        });
    })
</script>
<script>
    $(document).ready(function () {
        $('input[name=veh]').change(function () {
            $("#BrandModelid").val("");
        });
        $('#veh_noe').change(function () {
            $("#service_typee").val("");
        });
    });
</script>
<!-- disable buttons on submit -->
<script>
    $(document).ready(function () {
        $("#over_show").hide();
        $("#add_followup").submit(function (e) {
            $('#followup_submit').attr('disabled', 'disabled');
            var option = document.getElementById('followup_status');
            option.oninvalid = function (event) {
                document.getElementById("followup_status").style.borderColor = "#E42649";
                event.target.setCustomValidity('Please select a reason!');
                $('#followup_submit').removeAttr('disabled');
            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $("#add_rnr1").submit(function (e) {
            $('#rnr1_submit').attr('disabled', 'disabled');
            var option = document.getElementById('rnr1_status');
            option.oninvalid = function (event) {
                document.getElementById("rnr1_status").style.borderColor = "#E42649";
                event.target.setCustomValidity('Please select a reason!');
                $('#rnr1_submit').removeAttr('disabled');
            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $("#add_rnr2").submit(function (e) {
            $('#rnr2_submit').attr('disabled', 'disabled');
            var option = document.getElementById('rnr2_status');
            option.oninvalid = function (event) {
                document.getElementById("rnr2_status").style.borderColor = "#E42649";
                event.target.setCustomValidity('Please select a reason!');
                $('#rnr2_submit').removeAttr('disabled');
            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $("#add_others").submit(function (e) {
            $('#others_submit').attr('disabled', 'disabled');
            var option = document.getElementById('others_status');
            userinput.oninvalid = function (event) {
                document.getElementById("others_status").style.borderColor = "#E42649";
                event.target.setCustomValidity('Please select a reason!');
                $('#others_submit').removeAttr('disabled');
            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $("#cancel_submit").click(function (event) {
            event.preventDefault();
            user_id = $("#user_id").val();
            veh_id = $("#veh_id").val();
            type = $("#type").val();
            booking_id = $("#book_id").val();
            status = $("#cancel_status").val();
            comments = $("#cancel_comments").val();
            log = $("#log").val();
            if (status == "") {
                alert("Please select a reason!");
            } else if (status != "Duplicate Booking" && comments == "") {
                alert("Please explain the reason to cancel!");
            } else {
                $.ajax({
                    data: {
                        user_id: user_id,
                        veh_id: veh_id,
                        type: type,
                        booking_id: booking_id,
                        status: status,
                        comments: comments,
                        log: log
                    },
                    method: "POST",
                    url: "cancel_booking.php",
                    success: function (data) {

                        data = JSON.parse(data);
                        // console.log(data['original']);
                        if (data['original'] == 0) {
                            window.location.href = data['location'];
                            // console.log(data['location']);
                        } else {
                            alert("This is not a Duplicate Booking!!");
                        }
                    },
                    error: function () {
                        alert("Error");
                    }
                });
            }
        });
    });
</script>
<!-- check if locality is empty -->
<script>
    $(document).ready(function ($) {
        $("#mechanic").click(function () {
            if ($("#location").val() == '') {
                $("#mechanic").empty();
                $("#mechanic").append('<option value="">Select Mechanic</option>');
                alert("Please select a Location to get mechanics!!!");
            }
        });
    });
</script>
<script>
    $(document).ready(function ($) {
        $("#mechanice").click(function () {
            if ($("#location_pickup").val() == '') {
                $("#mechanice").empty();
                $("#mechanice").append('<option value="">Select Mechanic</option>');
                alert("Please select a Location to get mechanics!!!");
            }
        });
    });
</script>
<script>
    $(document).ready(function ($) {

        $("#go-div").click(function () {
            if ($("#location_pickup").val() == '') {
                alert("Oops no location has been selected!!!");
            } else {
                var selecttype = $('[name="veh_be"]:checked').val();
                var selectloc_data = $("#location_pickup").val();
                var res = selectloc_data.split(", ");
                var selectloc = res[0];
                var veh_no = $("#veh_noe").val();
                var latitude        = $("#lat").val();
	            var longtitude      = $("#long").val();

                var service = $("#service_typee").val();
                var book_id = $("#book_id").val();
                var city = $("#city").val();
                var city = res[1];
                var doorstep_only = $("#doorstep_only").val();
                var re_specialist = $("#re_specialist").val();
                var crm = '<?php echo $crm_log_id; ?>';
                var type = '<?php echo $type; ?>';
                var service_date = $("#service_datee").val();
                var se = '<?php echo $_SESSION['crm_se']; ?>';
                console.log(crm);
                $("#mechanice").empty();

                $("div.mece").show();
                //Make AJAX request, using the selected value as the POST
                // if(crm != 'crm036' && crm != 'crm012' && crm != 'crm041' && crm != 'crm018')

                if (se != '1' && crm != 'crm020' && crm != 'crm043' && crm != 'crm003' && crm != 'crm158' && crm != 'crm160' && crm != 'crm123' && crm != 'crm135' && crm != 'crm161' && crm != 'crm175' && crm != 'crm187' && crm != 'crm193' && crm != 'crm178' && crm != 'crm174' && crm != 'crm036' && crm != 'crm018' && crm != 'crm032' && crm != 'crm227' && crm != 'crm239' && crm != 'crm237' && crm != 'crm231' && crm != 'crm202' && crm != 'crm225' && crm != 'crm261' && crm != 'crm263' && crm != 'crm139' && crm != 'crm017' && crm != 'crm186' && crm != 'crm018' && crm != 'crm274' && crm != 'crm269' && crm != 'crm320' && crm != 'crm321' && crm != 'crm365' && crm != 'crm014' && crm != 'crm361' && crm != 'crm265' && crm != 'crm189' && crm != 'crm283' && crm != 'crm042' && crm != 'crm012' && crm != 'crm434' && crm != 'crm440' && crm != 'crm279' && crm != 'crm522')
                    // if(crm != 'crm036' && crm != 'crm043' && crm != 'crm018' && crm !='crm003' && crm !='crm064')
                    // if((selecttype === '2w' || service === 'Express Car Service 999' || service === 'Car dent / scratch removal' || service === 'Dent Removal 2499' || service === 'Dent 2999 1-2 Panels' || service === 'Dent 2999 3-4 Panels' || service === 'Dent 2999 5-6 Panels' || service === 'Dent 2999 7-8 Panels' || service === 'Dent Removal 2999') && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
                    // if(selecttype === '2w' && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
                {
                    $('.loader').show();
                    $('.loader_overlay').show();
                    $.ajax({
                        url: "ajax/project_crown.php",  // create a new php page to handle ajax request
                        type: "POST",
                        data: {
                            "selecttype": selecttype,
                            "selectloc": selectloc,
                            "veh_no": veh_no,
                            "service": service,
                            "book_id": book_id,
                            "crm_log_id": crm,
                            "doorstep_only": doorstep_only,
                            "re_specialist": re_specialist,
                            "city": city,
                            "service_date": service_date,
                            "latitude":latitude,
                            "longtitude":longtitude
                        },
                        success: function (data) {
                            // console.log(data);
                            $('.loader').hide();
                            $('.loader_overlay').hide();
                            //alert(data);
                            $('#mechanice').append(data);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            // alert(xhr.status + " "+ thrown	Error);
                        }

                    });

                } else {
                    $.ajax({
                        url: "ajax/get_mechanic_override.php",  // create a new php page to handle ajax request
                        type: "POST",
                        data: {
                            "selecttype": selecttype,
                            "selectloc": selectloc,
                            "veh_no": veh_no,
                            "service": service,
                            "crm": crm,
                            "book_id": book_id,
                            "city": city,
                            "latitude":latitude,
                            "longtitude":longtitude
                        },
                        success: function (data) {
                            //console.log(data);
                            //alert(data);
                            //$("#mechanice").removeAttr('required');
                            //document.getElementById("mechanice").removeAttribute("required");
                            $('#mechanice').append(data);
                            $("#over_show").show();
                            $("#late_override_btn").show();
                            var a = new Array();
                            $("#mechanice").children("option").each(function (x) {
                                test = false;
                                b = a[x] = $(this).val();
                                for (i = 0; i < a.length - 1; i++) {
                                    if (b == a[i]) {
                                        test = true;
                                    }
                                }
                                if (test) {
                                    $(this).remove();
                                }
                            });
                            if (crm == 'crm017' || crm == 'crm139' || crm == 'crm265' || crm == 'crm014' || crm == 'crm361' || crm == 'crm042' || crm == 'crm522') {
                                $("#edit_booking_submit").hide();
                            }

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            // alert(xhr.status + " "+ thrownError);
                        }

                    });
                }
            }
        });
    });
    $('.btn').on("click", function () {
        var id = $(this).data("bid");
        if (id != null) {
            //alert(id);
            $.ajax({
                type: 'GET',
                url: 'ajax/firstresponse1.php',
                data: {
                    bi: id
                }
            });
        }
    });

    $('#edit_booking_submit').on("click", function () {
        document.getElementById("override_flag").value = "0";
        $("#mechanice").attr("required", true);
    });

    $('#override_booking_submit').on("click", function () {
        document.getElementById("override_flag").value = "1";
        $("#mechanice").removeAttr("required");
    });

    $('#late_override_btn').on("click", function () {
        document.getElementById("override_flag").value = "2";
        $("#mechanice").removeAttr("required");
    });


    $(document).ready(function () {
        $('#override_push_confirm').modal('hide');
        $('#override_later_confirm').modal('hide');
    });


    $("#edit_booking").submit(function (event) {
        event.preventDefault();
        var type = '<?php echo $type ?>';
        $.ajax({
            data: {
                user_id: $("#user_id").val(),
                type: $("#type").val(),
                book_id: $("#book_id").val(),
                mechanice: $("#mechanice").val(),
                veh_be: $('[name="veh_be"]:checked').val(),
                locatione: $('#locatione').val(),
                location_pickup: $('#location_pickup').val(),
                veh_noe: $('#veh_noe').val(),
                descriptione: $('#descriptione').val(),
                service_typee: $('#service_typee').val(),
                amounte: $('#amounte').val(),
                service_datee: $('#service_datee').val(),
                next_service_datee: $('#next_service_datee').val(),
                pickup_date: $('#pickup_date').val(),
                pickupe: $("#pickupe").val(),
                pickup_full_addresse: $("#pickup_full_addresse").val(),
                fuel_type: $("#fuel_type").val(),
                vehicle_usage: $("#v_usage").val(),
                manufactured_year: $("#m_year").val(),
                override_flag: $("#override_flag").val(),
                latitude:$("#lat").val(),
                longtitude:$("#long").val()
            },
            type: "POST",
            url: "edit_booking.php",
            success: function (data) {
                if (data == "no") {
                    alert("The booking is already goaxled !!");
                } else if (data == "om") {
                    $('#myModal_override_booking').modal('show');
                } else if (data == "ol") {
                    $('#myModal_override_later').modal('show');
                } else if (type == "or") {
                    window.location.href = 'overrides.php';
                } else {
                    window.location.href = data;
                }
            }
        });
    });
</script>
</body>
</html>
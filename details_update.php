<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if(empty($_SESSION['crm_log_id'])) {
	header('location:logout.php');
	die();
}

?>

<!DOCTYPE html>
<html>
<head>
	<!-- jQuery library -->
	<script src="https://code.jquery.com/jquery-1.10.2.min.js" integrity="sha256-C6CB9UYIS9UJeqinPHWTHVqh/E1uhG5Twh+Y5qFQmYg=" crossorigin="anonymous"></script>
<meta charset="utf-8">
  <title>GoBumpr Bridge</title>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<!-- table sorter-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.28.9/css/theme.grey.min.css" />

<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>
  <style>


.floating-box {
	display: inline-block;
 float: right;
 margin: 12px;
 padding: 12px;
 width:112px;
 height:63px;
 box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  font-size: 13px;
  cursor:pointer;
}
.floating-box1 {
    display: inline-block;
	margin:10px;
	float:left;
	clear:both;
 }
 .floating-box2 {
 	display: inline-block;
  float: left;
	margin: 12px;
  padding: 12px;
	width:412px;
  height:53px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
   font-size: 15px;
   cursor:pointer;
 }
.rad input {visibility: hidden; width: 0px; height: 0px;}
label{
		cursor:pointer;
	}

/* table */
thead:hover{
	cursor:pointer;
}

.results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px;
  color:#ccc;
}

@media only screen and (max-width: 768px) {
	#div1{
		width:100%!important;
		margin-left:0px!important;	
		margin-top:50px!important;	
	}
	#div2{
		width:100%!important;
		margin-left:0px!important;
	}
	
}

</style>

</head>
<body>
<?php include_once("header.php"); ?>
<script>
	$(document).ready(function(){
		$('#city').show();
	})
</script>
<div class="overlay" data-sidebar-overlay></div>

<div class="padding"></div>
	<div style="margin-left:45px;margin-top:20px;font-size:16px;">
	<strong><p>Overall Points earned: </strong><span style="margin-left: 5px;margin-right: 35px;" id="total_points"></span><strong>Required Points for next Reward: </strong><span style="margin-left: 5px;margin-right: 35px;" id="nearest"></span><strong>Rewards claimed so far: </strong><i class='fa fa-inr' aria-hidden='true' style="padding-right: 2px;padding-left: 1px;"></i><span style="margin-left: 5px;margin-right: 5px;" id="rewards_claimed"></span></p>
	</div>
  <div  id="div1" style="margin-top:10px;float:left;margin-left:43px; border:2px solid #4CB192; border-radius:5px;width:55%;height:530px;">

 <!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
	<div class='uil-default-css' style='transform:scale(0.58);'>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
</div>
</div>
<!-- table -->
  <div id = "table" align="center" style="margin-top:10px; margin-right:20px; margin-left:20px; display:none;height:506px !important;overflow:auto !important;">
  <table id="example" class="table table-striped table-bordered tablesorter table-hover results"  >
  <thead style="background-color: #D3D3D3;">
	<th>Mobile No.</th>
  <th></th>
  </thead>
  <tbody id="tbody">
  </tbody>
  </table>
  </div>
<div class="modal fade" id="myModal_user" role="dialog" >
 <div class="modal-dialog" style="width:860px;top: 60px;">
 <!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Edit User</h3>
</div>
 <div class="modal-body" style="height:300px;">
								
	<form id="edit_user" class="form edit_user" method="post" action="">
    <div class="row">
    <br>
     <div class="col-xs-4 col-xs-offset-1 form-group">
     <input class="form-control" type="text" id="mobile" name="mobile" placeholder="Mobile" readonly maxlength="10" value="">
     </div>
	  <div class="col-xs-6 form-group">
      <input  class="form-control" type="text" id="user_name" name="user_name"  pattern="^[a-zA-Z0-9\s]+$" onchange="try{setCustomValidity('')}catch(e){}" placeholder="User Name" required >
      </div>
	 </div>
	 
	 <div class="row">
    

      <div class="col-xs-4 col-xs-offset-1 form-group">
      <input type="email" class="form-control" id="email" name="email" placeholder="E-Mail"  >
      </div>
         <div class="col-xs-3 form-group">
			    <div class="ui-widget">
                  <select class="form-control" id="user_city" name="user_city" required>
							<option value="">- Select -</option>
                             <?php

                            $sql_city = "SELECT DISTINCT city FROM go_bumpr.localities ORDER BY city ASC";
                            $res_city = mysqli_query($conn,$sql_city);
                            while($row_city = mysqli_fetch_object($res_city)){
                                ?>
                                <option value="<?php echo $row_city->city; ?>"><?php echo $row_city->city; ?></option>
                                <?php
                            }
                            ?>
                            <option value="others">Rest of India</option> 
                    </select>
					<script>
                    
                    </script>
                    </div>
                      </div>
                      <div id="oth_city" class="col-xs-3 form-group">
                        <div class="ui-widget">
        	                <input class="form-control autocomplete" id="other_city" type="text" name="other_city" placeholder="City" value="" disabled>
                       </div>
                      </div>
				</div>
                  <div class="row"></div>
          <div class="row">
           <br>
           <div class="form-group" align="center">
            <input class="form-control" type="submit" id="edit_user_submit" name="edit_user_submit" value="Update" style=" width:90px; background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
			<label><input type="checkbox" name="nodata" id="nodata" value="nodata">Data not available in TrueCaller</label>
          </div>
				</div>
    </form>
</div> <!-- modal body -->
</div> <!-- modal content -->
</div>  <!-- modal dailog -->
</div>  <!-- modal -->

  </div> <!-- div 1 -->
  <div id="div2" style="margin-top:10px;margin-left:50px; float:left; border:2px solid #DFA64E; border-radius:5px;width:34%;height:530px;">

	 <!-- loading -->
	 <div id="loading1" style="display:none; margin-top:140px;" align="center">
	 	<div class='uil-default-css' style='transform:scale(0.58);'>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 </div>
	 </div>
	 <!-- table -->
  <div id = "table1" align="center" style="margin-top:10px; margin-right:5px; margin-left:5px; display:none;height:506px !important;overflow:auto !important;">
  <table id="example1" class="table table-striped table-bordered table-hover"  >
  <thead style="background-color: #D3D3D3;">
  <th>Name</th>
  <th>Points</th>
  <th>Rewards</th>
  </thead>
  <tbody id="tbody1">
  </tbody>
  </table>
  </div>

</div>
  <!-- table sorter -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.28.9/js/jquery.tablesorter.min.js"></script>	<!-- date picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<!-- search bar  -->
<script>
function onAjax(){
	
	var user_id = 0;
	var mob_num = 0;
	
	$('#nodata').on('change',function(){
		if ($('#nodata').is(":checked"))
		{
		  $('#other_city').prop('disabled',true);
		  $('#user_name').prop('disabled',true);
		  $('#email').prop('disabled',true);
		  $('#user_city').prop('disabled',true);
		}
	});
	
	$('.fa-pencil').on('click',function(){
		
		user_id = $(this).attr('id');
		console.log("pencil clikc"+user_id);
		var selector = '#myModal_user'+user_id;
		var mob_numSel = '#mob'+user_id;
		mob_num = $(mob_numSel).text()
		$('#myModal_user').modal("show");
		var url = "https://www.truecaller.com/search/in/"+mob_num;
		setTimeout(function () { window.open(url, '_blank');}, 1000);
	});
	
	$('#myModal_user').on("shown.bs.modal", function() {
		$('#user_name').val('');
		$('#email').val('');
		$('#user_city').val('');
		console.log("shown modal"+user_id);
		$('#mobile').val(mob_num);
		$('#user_city').on("change",function(){
			if($('#user_city').val()=='others')
			{
				$('#other_city').prop('required',true);
				$('#other_city').prop('disabled',false);
			}
			else
			{
				$('#other_city').prop('required',false);
				$('#other_city').prop('disabled',true);
			}
		});
	});
	
	$('.edit_user').submit(function(){
		console.log("submit"+user_id);
		//var user_id = $(this).data('id');
		/* mobilesel = '#mobile'+user_id;
		mobile2sel = '#mobile2'+user_id;
		user_namesel = '#user_name'+user_id;
		emailsel = '#email'+user_id;
		citysel = '#city'+user_id;
		other_citysel = '#other_city'+user_id; */
		
		//mobile = $(mobilesel).val();
		if ($('#nodata').is(":checked"))
		{
			no_data = $('#nodata').val();
		}
		else
		{
			no_data = 'yes';
		}
		user_name = $('#user_name').val();
		email = $('#email').val();
		city = $('#user_city').val();
		if(city == "others")
		{
			city = $('#other_city').val();
		}
		
		$.ajax({
			url : "ajax/submit_missing_details.php",
			type : "POST",
			data : {'user_id':user_id,'user_name':user_name,'email':email,'city':city,'nodata':no_data},
			success : function(data)
			{
				view_rewards();
				view_points();
				//alert(data);
				//console.log(data);
			}
		});
	});
	
}
</script>
<script>
function view_users(){
	$.ajax({
		url : "ajax/missing_users_details.php",  // create a new php page to handle ajax request
		type : "POST",
		success : function(data) {
		$("#loading").hide();
		$("#table").show();
		if(data == 'no'){
			$('#table').html("<h3 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h3>");
		}
		else{
			 $('#tbody').html(data);
			 //$("#example").trigger("update");
			 onAjax();
		}
	  },
			error: function(xhr, ajaxOptions, thrownError) {
				//alert(xhr.status + " "+ thrownError);
		 }
	 });
}

function view_rewards(){
	$.ajax({
		url : "ajax/truecaller_rewards.php",  // create a new php page to handle ajax request
		type : "POST",
		success : function(data) {
		$("#loading1").hide();
		$("#table1").show();
			 $('#tbody1').html(data);
			 //$("#example1").trigger("update");
			 //onAjax();
		},
			error: function(xhr, ajaxOptions, thrownError) {
				//alert(xhr.status + " "+ thrownError);
		 }
	 });
}

function view_points(){
	$.ajax({
		url : "ajax/truecaller_points.php",
		type : "POST",
		dataType: 'json',
		success : function(data) {
			var total_points = data.total_points;
			var near100 = Math.ceil( total_points / 100 ) * 100;
			var nearest = near100 - total_points;
			
			$("#total_points").text(total_points);
			$("#nearest").text(nearest);
			$("#rewards_claimed").text(data.rewards_claimed);
		}
	});
}

$(document).ready(function(){
	$("#table").hide();
	$("#table1").hide();
	$("#loading").show();
	$("#loading1").show();
		view_users();
		view_rewards();
		view_points();
});
</script>
<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


<div id="popup"></div>

<script>
$(document).ready(function()
	{
			//$("#example").tablesorter(  {sortList: [[0,2], [0,0]]} );
			
	}
);
</script>

</body>
</html>

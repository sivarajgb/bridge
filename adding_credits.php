<?php
include("config.php");
$conn = db_connect3();

error_reporting(E_ALL); 
ini_set('display_errors', 1);
$today = date('Y-m-d H:i:s');
session_start();
$crm_log_id = $_SESSION['crm_log_id'];

//existing or new recharge
$customer = mysqli_real_escape_string($conn,$_POST['customer']);

if($customer == "existing"){
    $purpose = "Lead Credits";
}
else{
    $purpose = "Lead Credits First Recharge";
}
$shop_id = mysqli_real_escape_string($conn,$_POST['shop_id']);

$sql_shop = "SELECT b2b_shop_name,b2b_mobile_number_1 FROM b2b_mec_tbl WHERE b2b_shop_id='$shop_id'";
$res_shop = mysqli_query($conn,$sql_shop) or die(mysqli_error($conn));
$row_shop = mysqli_fetch_object($res_shop);
$shop_name = $row_shop->b2b_shop_name;
$mobile_number = $row_shop->b2b_mobile_number_1;

$amount_paid = mysqli_real_escape_string($conn,$_POST['amount_paid']);

$total = $amount_paid;
$amount = $total/1.18;
$tax = $amount_paid-$amount;
$credits = floor($amount/100);
/*
$amount = mysqli_real_escape_string($conn,$_POST['amount']);
$tax = mysqli_real_escape_string($conn,$_POST['tax']);
$credits = mysqli_real_escape_string($conn,$_POST['no_of_credits']);
*/
$payment_mode = mysqli_real_escape_string($conn,$_POST['payment_mode']);
//cash
$receipt_no = mysqli_real_escape_string($conn,$_POST['receipt_no']);
//cheque
$cheque_no = mysqli_real_escape_string($conn,$_POST['cheque_no']);
if(isset($_POST['cheque_no'])){
	$cheque_date = date('Y-m-d',strtotime(mysqli_real_escape_string($conn,$_POST['cheque_date'])));
	$bank_name = mysqli_real_escape_string($conn,$_POST['bank_name']);
}
else{
	$cheque_date = '0000-00-00';
	$bank_name = '';
}
//neft
$transaction_id = mysqli_real_escape_string($conn,$_POST['transaction_id']);
//instamojo
$instamojo_id = mysqli_real_escape_string($conn,$_POST['instamojo_id']);
//others
$reference = mysqli_real_escape_string($conn,$_POST['reference']);

$updated_by = mysqli_real_escape_string($conn,$_POST['updated_by']);
$comments = mysqli_real_escape_string($conn,$_POST['comments']);

$sql_get_prev_credits= "SELECT b2b_credits FROM b2b_credits_tbl WHERE b2b_shop_id='$shop_id'";
$res_get_prev_credits= mysqli_query($conn,$sql_get_prev_credits);
$row_get_prev_credits= mysqli_fetch_array($res_get_prev_credits);
$prev_credits_blnc = $row_get_prev_credits['b2b_credits'];


$sql_ins = "INSERT INTO b2b_credits_payment (b2b_shop_id,b2b_cus_name,b2b_cus_no,b2b_credits,b2b_amt,b2b_credit_amt,b2b_purpose,instmj_pmt_id,b2b_receipt_no,b2b_cheque_no,b2b_cheque_date,b2b_cheque_bank,b2b_neft_trn_id,b2b_other_ref,b2b_crm_update_id,b2b_comments,b2b_approve_flag,b2b_log,b2b_crm_log_id,b2b_payment_mode,b2b_prev_crdt_blnc) VALUES ('$shop_id','$shop_name','$mobile_number','$credits','$amount_paid','$amount','$purpose','$instamojo_id','$receipt_no','$cheque_no','$cheque_date','$bank_name','$transaction_id','$reference','$updated_by','$comments','1','$today','$crm_log_id','$payment_mode','$prev_credits_blnc')";
$res_ins = mysqli_query($conn,$sql_ins) or die(mysqli_error($conn));
/* 
$sql_g = "UPDATE go_bumpr.admin_mechanic_table SET status = '' WHERE axle_id = '$shop_id'";
$res_g = mysqli_query($conn,$sql_g); */
if($customer=='new'){
    // new entry - add flag
    $sql_flag = "UPDATE b2b_mec_tbl SET b2b_cred_model='1' WHERE b2b_shop_id='$shop_id'";
    $res_flag = mysqli_query($conn,$sql_flag) or die(mysqli_error($conn));
}
if($res_ins){
	//echo "hello";
    $sql_get = "SELECT max(b2b_id) as b2b_id FROM b2b_credits_payment WHERE b2b_shop_id='$shop_id' AND b2b_approve_flag='1'";
    $res_get = mysqli_query($conn,$sql_get) or die(mysqli_error($conn));
    $row_get = mysqli_fetch_object($res_get);
    $payment_id = $row_get->b2b_id;
	?>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

</head>
<body align="center">
      <!-- loading -->
  <div id="loading" style="margin-top:140px;" align="center">
    <div class='uil-default-css' style='transform:scale(0.58);'>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
  </div>
 
  </div>
  <div style="margin-top:50px;">
   <h3>Please wait for a while, details are being updated...</h3>
   </div>

<script>
var shop_id = <?php echo $shop_id; ?>;
var amount_paid = <?php echo $amount_paid; ?>;
var credits = <?php echo floor($credits); ?>;
var updated_by = '<?php echo $crm_log_id; ?>';
var money_collected_by = '<?php echo $updated_by; ?>';
var payment_mode = '<?php echo $payment_mode; ?>';
var payment_id = <?php echo $payment_id; ?>;
console.log(shop_id);
   // alert(shop_id);
	 $.ajax({
        url: 'send_credits_approval_mail.php',
        type: "POST",
        data: { 'shop_id': shop_id,'amount_paid': amount_paid,'credits':credits,'money_collected_by':money_collected_by,'updated_by':updated_by,'payment_mode':payment_mode,'payment_id':payment_id}           
    }).done(function(){
						//console.log(shop_id);
    });
    window.location.href = 'shop_credits_history.php?si=<?php echo base64_encode($shop_id); ?>&t=<?php echo base64_encode($credits); ?>';
    console.log(shop_id);
</script>
</body>
</html>
<?php
}

?>
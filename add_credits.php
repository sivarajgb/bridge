<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id'])) || $flag!="1") {
	
	header('location:logout.php');
	die();
}
?>

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>GoBumpr Bridge</title>


  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<!-- date time picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>

<style>
/*home page blocks */
.floating-box {
 display: inline-block;
 margin: 2px;
 padding-top: 12px;
 padding-left: 12px;
 width:300px;
}
.navbar-fixed-top{
  z-index:100 !important;
}
.upper-div{
  z-index:999999 !important;
}
#range > span:hover{cursor: pointer;}
 /* table */
#tbody{
   font-size:15px !important;
  border:1.5px solid #c4b8b8 !important;
  
}
thead:hover{
	cursor:pointer;
}
.borderless td, .borderless th {
    border: none !important;
}
.results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px;
  color:#9E9E9E;
}
#tbody, tbody tr {
    -webkit-animation: opacity 5s ease-in-out;
    animation: opacity 5s ease-in-out;
}
.datepicker {
	  cursor:pointer;
  }
.datepicker:before {
content: '';
display: inline-block;
border-left: 7px solid transparent;
border-right: 7px solid transparent;
border-bottom: 7px solid #ccc;
border-bottom-color: transparent !important;
position: absolute;
top: -7px;
left: 190px;  // I made a change here 
}

.datepicker:after {
content: '';
display: inline-block;
border-left: 6px solid transparent;
border-right: 6px solid transparent;
border-top-color: transparent !important;
border-top: 6px solid #ffffff;
position: absolute;
bottom: -6px;
left: 191px;  // I made a change here 
}
</style>
</head>
<body id="body">
<?php include_once("header.php"); ?>
<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>
<div style="float:left;width:60%;">
<div class="col-lg-offset-2 col-sm-offset-1 col-md-offset-1" style="margin-top:28px;max-width:70%;">
<form class="form" action="adding_credits.php" method="post">
<div class="row" style="margin-top:18px;margin-bottom:20px;">
<input class="from-control" type="radio" name="customer" id="existing" value="existing" style="margin-left:80px;" checked> Existing</input>
<input class="from-control" type="radio" name="customer" id="new" value="new" style="margin-left:80px;"> First Recharge</input>
</div>

<div class="row">
    <div class="col-lg-3 col-sm-2 col-md-3">
    <label style="padding:5px;">Shop Name</label>
    </div>
    <div class=" col-lg-5 col-sm-3 col-md-4">
    <select class="form-control" type="text" name="shop_id" id="shop_id" required>
    <option value="" selected>Select Garage Name</option>
    </select>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-3 col-sm-2 col-md-3">
    <label style="padding:5px;">Amount Paid</label>
    </div>
    <div class="col-lg-4 col-sm-3 col-md-3">
    <input class="form-control" type="number" name="amount_paid" id="amount_paid" pattern="[0-9]" placeholder="Eg. 1150" required/>
    </div>
    <p style="color:#4DB6AC;font-size:22px;padding:5px;"><i class="fa fa-calculator" aria-hidden="true"  id="calculate"></i></p>
</div>
<div class="row" id="calculations">
    <div class="col-lg-3 col-sm-3 col-md-3" style="float:left;">
        <label style="padding:5px;" for="amount">Amount</label>
        <input class="form-control" type="number" name="amount" id="amount" pattern="[0-9]" placeholder="Eg.1000" style="width:96px;" required/>
    </div>
    <div class="col-lg-3 col-sm-3 col-md-3" style="float:left;margin-left:-10px;">
        <label style="padding:5px;" for="tax">Tax</label>
        <input class="form-control" type="number" name="tax" id="tax" pattern="[0-9]" placeholder="Eg.150" style="width:96px;" required/>
    </div>
    <div class="col-lg-3 col-sm-3 col-md-3" style="float:left;margin-left:-10px;">
        <label style="padding:5px;" for="tax">Credits</label>
        <input class="form-control" type="number" name="no_of_credits" id="no_of_credits" pattern="[0-9]" placeholder="Eg.150" style="width:96px;" required/>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-3 col-sm-2 col-md-3">
    <label style="padding:5px;">Payment Mode</label>
    </div>
    <div class="col-lg-5 col-sm-3 col-md-4">
    <select class="form-control" id="payment_mode" name="payment_mode" required>
    <option value="" selected> Select mode of Payment</option>
    <option value="Cash">Cash</option>
    <option value="Cheque">Cheque</option>
    <option value="NEFT">NEFT</option>
    <option value="Instamojo">Instamojo</option>
    <option value="Others">Others</option>
    </select>
    </div>
</div>
<div id="cheque_div" style="display:none;">
<br>
  <div class="row" >
      <div class="col-lg-3 col-sm-2 col-md-3">
      <label style="padding:5px;">Cheque No.</label>
      </div>
      <div class="col-lg-5 col-sm-3 col-md-4">
      <input class="form-control" type="text" name="cheque_no" id="cheque_no" placeholder="Eg. Chq1223332332"/>
      </div>
  </div>
  <div class="row" >
      <div class="col-lg-4 col-sm-3 col-md-4">
      <label style="padding:5px;" for="cheque_date">Cheque Date</label>
      <input class="form-control datepicker"  data-date-format='dd-mm-yyyy' type="text" name="cheque_date" id="cheque_date"/>
      </div>
      <div class="col-lg-4 col-sm-3 col-md-4">
      <label style="padding:5px;" for="bank_name">Bank Name</label>
      <input class="form-control" type="text" name="bank_name" id="bank_name" placeholder="Eg. Vijaya Bank"/>
      </div>
  </div>
</div>
<div id="cash_div" style="display:none;">
<br>
  <div class="row">
      <div class="col-lg-3 col-sm-2 col-md-3">
      <label style="padding:5px;">Receipt No.</label>
      </div>
      <div class="col-lg-5 col-sm-3 col-md-4">
      <input class="form-control" type="text" name="receipt_no" id="receipt_no" placeholder="Eg. Rec112"/>
      </div>
  </div>
</div>
<div id="neft_div" style="display:none;">
<br>
    <div class="row">
      <div class="col-lg-3 col-sm-2 col-md-3">
      <label style="padding:5px;">Transaction Id</label>
      </div>
      <div class="col-lg-5 col-sm-3 col-md-4">
      <input class="form-control" type="text" name="transaction_id" id="transaction_id" placeholder="Eg. Trn0202020202"/>
      </div>
  </div>
</div>
<div id="instamojo_div" style="display:none;">
<br>
    <div class="row">
      <div class="col-lg-3 col-sm-2 col-md-3">
      <label style="padding:5px;">Instamojo Id</label>
      </div>
      <div class="col-lg-5 col-sm-3 col-md-4">
      <input class="form-control" type="text" name="instamojo_id" id="instamojo_id" placeholder="Eg. MOJO12121212112"/>
      </div>
  </div>
</div>
<div id="others_div" style="display:none;">
<br>
    <div class="row">
      <div class="col-lg-3 col-sm-2 col-md-3">
      <label style="padding:5px;">Reference</label>
      </div>
      <div class="col-lg-5 col-sm-3 col-md-4">
      <input class="form-control" type="text" name="reference" id="reference" placeholder="Eg. Customer's bill"/>
      </div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-lg-3 col-sm-2 col-md-3">
  <label style="padding:5px;">Collected By</label>
  </div>
  <div class="col-lg-5 col-sm-3 col-md-4">
  <select class="form-control" type="text" name="updated_by" id="updated_by" required>
  <option selected value="">Select a person</option>
  <?php 
  $sql_crm = "SELECT crm_log_id,name FROM crm_admin where password!='' ORDER BY name ASC";
  $res_crm = mysqli_query($conn,$sql_crm);
  while($row_crm = mysqli_fetch_object($res_crm)){
    ?>
    <option value="<?php echo $row_crm->crm_log_id; ?>"><?php echo $row_crm->name; ?></option>
    <?php
  }
  ?>
  </select>
  </div>
</div>
<br>
<div class="row">
  <div class="col-lg-3 col-sm-2 col-md-3">
  <label style="padding:5px;">Comments</label>
  </div>
  <div class="col-lg-5 col-sm-3 col-md-4">
  <input class="form-control" type="text" name="comments" id="comments" placeholder="Eg. Some text to remind" required/>
  </div>
</div>
<br>
<div class="row">
    <div align="center">
    <input type="submit" class="btn btn-md" value="Submit" style="background-color:#00796B;color:#fff;"/>
    </div>
</div>
</form>
</div>  
</div>
<div id="show" style="float:left; width:34%;" align="center">
<div id="garage_details" style="margin-top:30px;margin-left:30px;border:0.5px solid #009688;padding:12px;border-radius:12px;">
<h4> Selected Garage Details</h4>
</div>
<!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>
</div>

<!-- jQuery library -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- date picker -->
<script>
var date = new Date();
date.setDate(date.getDate());

$('.datepicker').datepicker({
   autoclose: true,
    startDate: date
});
$('input.datepicker').datepicker('setDate', 'today');
</script>

<!-- load Mechanics -->
<script>
  function loadmec(){
    var selecttype = $('[name="customer"]:checked').val();
    var city = $('#city').val();

    $("#shop_id").empty();
    $("#garage_details").empty();
    $("#garage_details").html('<h4> Selected Garage Details</h4>');
            //Make AJAX request, using the selected value as the POST
        $.ajax({
              url : "ajax/get_garage_names.php",  // create a new php page to handle ajax request
              type : "POST",
              data : { "selecttype": selecttype,"city":city },
              success : function(data) {
          //console.log(data);
          //alert(data);
                  $('#shop_id').append(data);
  
              },
            error: function (xhr, ajaxOptions, thrownError) {
             // alert(xhr.status + " "+ thrownError);
            }
  
          });
  
  
  }
</script>
<!-- select Mechanic on page load-->
<script>
$(document).ready(function($) {
  loadmec();
});
</script>
<!-- select Mechanic on change -->
<script>
$(document).ready(function($) {
 $(document).on('change', '[name="customer"]' , function(){
  loadmec();
    });
});
</script>

<!-- select Mechanic on city change -->
<script>
$(document).ready(function($) {
 $('#city').change(function(){
  loadmec();
    });
});
</script>

<!-- view selected shop details -->
<script>
$(document).ready(function($) {
  $("#shop_id").change(function(){
    var shop_id = $("#shop_id").val();
    $("#garage_details").hide();
    $("#loading").show();
    //console.log(shop_id);
    $.ajax({
        url : "ajax/show_garage_details.php",  // create a new php page to handle ajax request
        type : "POST",
        data : { "shop_id": shop_id },
        success : function(data) {
				//console.log(data);
				//alert(data);
         $('#loading').hide();
         $("#garage_details").show();
         $("#garage_details").html(data);
        },
        error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
        }
    });
  });
});
</script>
<!-- calculate amount and tax -->
<script>
$(document).ready(function($) {
  $("#calculate").click(function(){
    var amountpaid = $("#amount_paid").val();
    var amount = 0;
    var tax = 0;
    var credits = 0;
    var total = amountpaid;
    if(amountpaid === ''){
      alert("Enter the AMOUNT PAID to get actual amount and tax");
    }
    else{
      amount = Math.floor(total / 1.18);
      tax = Math.floor(amountpaid-amount);
      credits = Math.floor(amount/100);
      $("#amount").val(amount);
      $("#tax").val(tax);
      $("#no_of_credits").val(credits);
    }

  });
});
</script>
<!-- on change of payment mode -->
<script>
$(document).ready(function($) {
  $("#payment_mode").change(function(){
    var mode = $("#payment_mode").val();
    switch(mode){
      case "Cash": $("#cash_div").show();$("#receipt_no").prop('required',true);
                   $("#cheque_div").hide();$("#cheque_no").prop('required',false);$("#cheque_date").prop('required',false);$("#bank_name").prop('required',false);
                   $("#neft_div").hide();$("#transaction_id").prop('required',false);
                   $("#instamojo_div").hide();$("#instamojo_id").prop('required',false);
                   $("#others_div").hide();$("#reference").prop('required',false); 
                   break;
      case "Cheque": $("#cash_div").hide();$("#receipt_no").prop('required',false);
                     $("#cheque_div").show();$("#cheque_no").prop('required',true);$("#cheque_date").prop('required',true);$("#bank_name").prop('required',true);
                     $("#neft_div").hide();$("#transaction_id").prop('required',false);
                     $("#instamojo_div").hide();$("#instamojo_id").prop('required',false);
                     $("#others_div").hide();$("#reference").prop('required',false);
                     break;
      case "NEFT": $("#cash_div").hide();$("#receipt_no").prop('required',false);
                   $("#cheque_div").hide();$("#cheque_no").prop('required',false);$("#cheque_date").prop('required',false);$("#bank_name").prop('required',false); 
                   $("#neft_div").show();$("#transaction_id").prop('required',true); 
                   $("#instamojo_div").hide();$("#instamojo_id").prop('required',false);
                   $("#others_div").hide();$("#reference").prop('required',false); 
                   break;
      case "Instamojo": $("#cash_div").hide();$("#receipt_no").prop('required',false);
                        $("#cheque_div").hide();$("#cheque_no").prop('required',false);$("#cheque_date").prop('required',false);$("#bank_name").prop('required',false);
                        $("#neft_div").hide();$("#transaction_id").prop('required',false); 
                        $("#instamojo_div").show();$("#instamojo_id").prop('required',true);
                        $("#others_div").hide();$("#reference").prop('required',false); 
                        break;
      case "Others": $("#cash_div").hide();$("#receipt_no").prop('required',false);
                     $("#cheque_div").hide();$("#cheque_no").prop('required',false);$("#cheque_date").prop('required',false);$("#bank_name").prop('required',false); 
                     $("#neft_div").hide();$("#transaction_id").prop('required',false); 
                     $("#instamojo_div").hide();$("#instamojo_id").prop('required',false);
                     $("#others_div").show();$("#reference").prop('required',true); 
                     break;
      default:$("#cash_div").hide();$("#receipt_no").prop('required',false);
              $("#cheque_div").hide();$("#cheque_no").prop('required',false);$("#cheque_date").prop('required',false);$("#bank_name").prop('required',false); 
              $("#neft_div").hide();$("#transaction_id").prop('required',false); 
              $("#instamojo_div").hide();$("#instamojo_id").prop('required',false);
              $("#others_div").hide();$("#reference").prop('required',false);
    }
  });
});
</script>
</body>
</html>

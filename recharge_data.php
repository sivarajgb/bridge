<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id']))) {
	
	header('location:logout.php');
	die();
}
?>

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15" />
  <title>GoBumpr Bridge</title>


  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


  <!-- Include Date Range Picker -->
  <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

  <!-- table sorter -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js"></script>

<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>
<style>
/*home page blocks */
.floating-box1 {
 display: inline-block;
}
.navbar-fixed-top{
  z-index:100 !important;
}
.upper-div{
  z-index:999999 !important;
}
#range > span:hover{cursor: pointer;}
 /* table */
#tbody{
   font-size:15px !important;
  border:1.5px solid #c4b8b8 !important;
  
}
thead:hover{
	cursor:pointer;
}

.results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px;
  color:#9E9E9E;
}
#tbody, tbody tr {
    -webkit-animation: opacity 5s ease-in-out;
    animation: opacity 5s ease-in-out;
}

.selected-rating{
   text-align: center;
    color: #27a97e;
    font-weight: bold;
    font-size: 22px;
}
</style>

<style>
.checked {
  color: #ff9800;
}
</style>

</head>
<body id="body">
<?php include_once("header.php"); ?>
<script>
	$(document).ready(function(){
		$('#city').show();
	})
</script>
<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>

<!-- date range picker -->

<!--<div id="reportrange" class="col-sm-3 " style="cursor: pointer; margin-top:28px; margin-left:170px;max-width:351px;">
<div class=" floating-box1">
    <div id="range" class="form-control" >
    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
    <span id="dateval"></span> <b class="caret"></b>
    </div>
</div>
</div>-->
<div  class=" col-sm-3 " style="cursor: pointer; margin-top:28px;margin-left:170px; max-width:315px;">
<div class=" floating-box1">

     <select id="Range" name="range" class="form-control">
       <option value="all">All</option>
       <option value="Active">Active</option>
       <option value="Inactive for last 3 month">Inactive for last 3 month</option>
       <option value="Inactive for last month">Inactive for last month</option>
      </select>
    
</div>
</div>
<div class=" col-sm-2 col-lg-1" style="cursor: pointer;margin-left:-25px; margin-top:28px; max-width:315px; ">
<div  class="floating-box1" >
<div style="max-width:140px;">
    <select id="vehicle" name="vehicle" class="form-control" style="width:140px;">
      <option value="all" selected>All Vehicles</option>
      <option value="2w">2 Wheeler</option>
      <option value="4w">4 Wheeler</option>
    </select>
</div>
</div>
</div>



<div id="show" style="margin-top:82px;width:99%;">
</div>  
<!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>
<div align="center" id = "table" style="max-width:95%; margin-top:78px;margin-left:10px;margin-right:10px; display: none;">
  <!--<table id="example" class="table table-striped table-bordered tablesorter table-hover results">
    <thead style="background-color: #D3D3D3;" id="thead">
      
    </thead>
    <tbody id="tbody">
    </tbody>
  </table>-->
</div>


  
  <script async src="js/jquery-3.2.1.min.js" onLoad="loadOtherScripts();"></script>

  <script>
    function loadScript(src) {
        return new Promise(function (resolve, reject) {
            var s;
            s = document.createElement('script');
            s.src = src;
            s.onload = resolve;
            s.onerror = reject;
            document.head.appendChild(s);
        });
    }

    function loadOtherScripts() {
      loadScript('js/moment.min.js')
      .then(function() {
      Promise.all([ loadScript('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'),loadScript('js/daterangepicker.js'),loadScript('js/sidebar.js'),loadScript('https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap2-toggle.min.js'),loadScript('js/jquery.table2excel.js')]).then(function () {
   // console.log('scripts are loaded');
      daterangepickerangePicker();
      momentInPicker();
      intialFunction();
      }).catch(function (error) {
    //  console.log('some error!' + error)
      })
      }).catch(function (error) {
    //  console.log('Moment call error!' + error)
      })
      }
  </script>

  <!-- table sorter -->
  

<!-- jQuery library -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  
<!-- piechart -->
<script src="js/google-visualization.js"></script>
<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


<script>
function get_shops(){
  var city = $("#city").val();
  $.ajax({
      url : "ajax/get_shops_fb_panel.php",  // create a new php page to handle ajax request
      type : "POST",
      data : {"city": city},
      success : function(data) {
    $("#shop_list").empty();
        $('#shop_list').append(data);
        $('#show').hide();
    $("#loading").show();
    defaultview();
    },
      error: function(xhr, ajaxOptions, thrownError) {
            //  alert(xhr.status + " "+ thrownError);
      }
  });
}
</script>


<!-- date range picker -->
<script>
  function daterangepickerangePicker(){
$('input[name="daterange"]').daterangepicker({
 locale: {
      format: 'DD-MM-YYYY'
    }
});
}
</script>
<script type="text/javascript">
  function momentInPicker(){
$(function() {

  //var start = moment().subtract(1, 'days');
  var start = moment().startOf('month');
	 // var start = moment();
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});
}
</script>
<script>
function defaultview(){
	//var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
	//var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
  var city = $('#city').val();
  var Range = $('#Range').val();
  var vehicle = $('#vehicle').val();
  var service = $('#service').val();
 //var master_service = $('#master_service').val();
 // console.log(city);
 
	//Make AJAX request, using the selected value as the POST
	$.ajax({
	    url : "ajax/recharge_data_view.php",  // create a new php page to handle ajax request
	    type : "POST",
        data : {"city": city, "vehicle": vehicle,"Range":Range},
        success : function(data) {
			//alert(data);
		    //console.log(data);  
            $('#show').show();
			$("#loading").hide();
			$('#show').html(data);
      
  
         $("#excel").unbind().click(function(){
       
       
              $("#table").table2excel({
   
            filename:  "Feedback Data Panel"
                });
           });
  
   
     
       },
	   error: function(xhr, ajaxOptions, thrownError) {
	        // alert(xhr.status + " "+ thrownError);
	    }
	});
}
</script>

<script>
$(document).ready( function (){
   $('#editor').hide();
   $('#show').hide();
  $("#loading").show();
  defaultview();
});
</script>


<!-- <script>
function get_services(){
  var master = $("#master_service").val();
  //console.log(master);
  $("#service").empty();
  $.ajax({
      url : "ajax/get_services_master.php",  // create a new php page to handle ajax request
      type : "POST",
      data : {"master": master},
      success : function(data) {
        $('#service').append(data); 
      $("#loading").show();
      //console.log("get services");
      defaultview();  
    },
      error: function(xhr, ajaxOptions, thrownError) {
            //  alert(xhr.status + " "+ thrownError);
      }
  });
}
</script> -->

<!-- on change of date -->
<script>
$(document).ready( function (){
	$('#dateval').on("DOMSubtreeModified", function (){
   $('#editor').hide();
   $('#show').hide();
	$("#loading").show();
  defaultview();
	 });
});
</script>

<!-- on change of city -->
<script>
  function intialFunction(){
$(document).ready( function (){

var $radios = $('input:radio[name=master]');
  if($radios.is(':checked') === false) {
      $radios.filter('[value=master]').prop('checked', true);
      $("#master1").parent().button('toggle');
  }



    $('#city').show();
    $('#cluster').hide();
    $("#editor").hide();
    $("#loading").show();
    get_shops();


    $('input:radio[name=master]').change(function (){
    $('#editor').hide();
    $('#show').hide();
    $("#loading").show();
    defaultview();
  });


    //console.log('hi');
	$('#city').on("change", function (){
   $('#editor').hide();
   $('#show').hide();
	$("#loading").show();
  get_shops();
	 });


  $('#vehicle').on("change", function (){
   $('#editor').hide();
   $('#show').hide();
  $("#loading").show();
  defaultview();
   });

   $('#Range').on("change", function (){
   $('#editor').hide();
   $('#show').hide();
  $("#loading").show();
  defaultview();
   });
 


   //  $("#master_service").on("change", function (){
   //  $('#editor').hide();
   // $('#show').hide();
   // $("#loading").show();
   // defaultview();
   //  });

          

});
}
</script>
</body>
</html>

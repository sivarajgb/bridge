<?php

$conn = mysqli_connect("localhost","root","devprcs1@3db","go_bumpr");
//error_reporting(1);
//$conn1 = mysqli_connect("localhost","root","devprcs1@3db","b2b") or die(mysqli_error($conn1));

//ini_set('max_execution_time', 300); //300 seconds = 5 minutes

include_once('fpdf.php');
 
class PDF extends FPDF
{
// Page header
function Header()
{
     if ($this->page == 1)
          {
    // Logo
    // $this->Image('logo.png',10,-1,70);
    $this->SetFont('Arial','B',13);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(38,10,'Feeback Data Panel List',0,0,'C');
    // Line break
    $this->Ln(20);
    }
}
 
// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

$startdate = date('Y-m-d',strtotime($_POST['startDate']));
$enddate =  date('Y-m-d',strtotime($_POST['endDate']));
//echo $startdate;
$city = $_POST['city'];
$shopname=$_POST['shopname'];
$vehicle = $_POST['vehicle'];
$service = $_POST['service'];
$master_service = $_POST['master_service'];
//print_r($_POST);
$cond='';

$cond = $cond.($city == 'all' ? "" : "AND g.city='$city'");
$cond = $cond.($shopname == 'all' ? "" : "AND f.shop_name='$shopname'");
$cond = $cond.($vehicle == 'all' ? "" : "AND g.vehicle_type='$vehicle'");
$cond = $cond.($service == 'all' ? "" : "AND g.service_type='$service'");


$result = mysqli_query($conn, "SELECT f.booking_id,b.b2b_customer_name,f.shop_name,g.vehicle_type,f.service_status,g.rating,g.feedback_status FROM go_bumpr.feedback_track as f JOIN go_bumpr.user_booking_tb as g ON f.booking_id = g.booking_id join b2b.b2b_booking_tbl b on b.b2b_booking_id=f.b2b_booking_id WHERE f.flag='0'  AND f.shop_id NOT IN (1014,1035,1670) AND DATE(f.log) BETWEEN '$startdate' and '$enddate' {$cond} ORDER BY b.b2b_log ASC ");


//header("Content-type:application/pdf");
$pdf = new PDF();
$pdf->SetLeftMargin(7);
//header
$pdf->AddPage();
//foter page
$pdf->AliasNbPages();

$width_cell=array(25,30,42,23,42,35);
//$pdf->SetFont('Arial','B',16);

//Background color of header//
$pdf->SetFillColor(976,245,458);

$pdf->SetFont('Arial','B',10);

// Header starts /// 
//First header column //
$pdf->Cell($width_cell[0],10,'Booking Id',1,0,'C');
//Second header column//
$pdf->Cell($width_cell[1],10,'Customer Name',1,0,'C');
//Third header column//
$pdf->Cell($width_cell[2],10,'Shop Name',1,0,'C'); 
//Fourth header column//
$pdf->Cell($width_cell[3],10,'Vehicle Type',1,0,'C');
//Third header column//
$pdf->Cell($width_cell[4],10,'Service Status',1,0,'C'); 

$pdf->Cell($width_cell[5],10,'Rating',1,0,'C'); 




// //Background color of header//
// $pdf->SetFillColor(193,229,252);
// foreach($header as $heading) {
// $pdf->Cell(40,12,$display_heading[$heading['Field']],1);
// }

// foreach($result as $row) {
// $pdf->Ln();
// foreach($row as $column)
// $pdf->Cell(35,10,$column,1,0,'C');
// }
$pdf->SetFont('Arial','',10);
$pdf->SetFillColor(193,229,252);

 while($row = mysqli_fetch_array($result)) { 

    //print_r($row);die;
    $pdf->Ln();
    $booking_id=$row['booking_id'];
    $b2b_customer_name=$row['b2b_customer_name'];
    $shop_name=$row['shop_name'];
    $vehicle_type=$row['vehicle_type'];
    $service_status=$row['service_status'];
    $rating=$row['rating'];
    $feedback_status=$row['feedback_status'];
$pdf->Cell(25,10,$booking_id,1,0,'C');
$pdf->Cell(30,10,$b2b_customer_name,1,0);
$pdf->Cell(42,10,$shop_name,1,0);
$pdf->Cell(23,10,$vehicle_type,1,0,'C');
$pdf->Cell(42,10,$service_status,1,0,'C');
$pdf->Cell(35,10,$rating,1,0,'C');


 }
  $filename="FeedBackDataPanel.pdf"; 
  $pdf->Output($filename,'F');
  echo $filename;
  
  
?>


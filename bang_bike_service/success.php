<?php
include("config.php");
$conn = dbconnect();

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bike General Service & Polish @ Rs.999</title>
        <meta name="description" content="Get Bike General Service Done in Bangalore at the Cheapest Price. Avail Offers from Bike Garages near you & Save up to 60% on Bike Servicing Cost - Grab the Deal Now. ">

        <!-- Latest compiled and minified JavaScript -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800" >
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="icon" type="image/png" sizes="16x16" href="https://static.gobumpr.com/web-app/fav-icons/16.png">
        <link rel="icon" type="image/png" sizes="32x32" href="https://static.gobumpr.com/web-app/fav-icons/32.png">
        <link rel="icon" type="image/png" sizes="57x57" href="https://static.gobumpr.com/web-app/fav-icons/57.png">
        <link rel="icon" type="image/png" sizes="76x76" href="https://static.gobumpr.com/web-app/fav-icons/76.png">
        <link rel="icon" type="image/png" sizes="96x96" href="https://static.gobumpr.com/web-app/fav-icons/96.png">
        <link rel="icon" type="image/png" sizes="114x114" href="https://static.gobumpr.com/web-app/fav-icons/114.png">
        <link rel="icon" type="image/png" sizes="144x144" href="https://static.gobumpr.com/web-app/fav-icons/144.png">
        <link rel="icon" type="image/png" sizes="152x152" href="https://static.gobumpr.com/web-app/fav-icons/152x152.png">
        <link rel="stylesheet" href="./css/confirmationPageCss.css">
        <!-- Google Analytics Code -->
        <script async>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://google-analytics.com/analytics.js','ga');

          ga('create', 'UA-67994843-4', 'auto');
          ga('send', 'pageview');

        </script>
        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '582926561860139');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
        /></noscript>
        <!-- DO NOT MODIFY -->
        <!-- End Facebook Pixel Code -->
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WDK2CML');</script>
        <!-- End Google Tag Manager -->

    <style type="text/css">
        #terms
        {
         line-height: 2.8!important; 
        }
        .tc
        {
            background-color: #fff;
        }
        @media(min-width: 1024px)  
        {
           #enquiry_received
           {
            margin:0px 0px 0px 90px;  border-radius: 8px; padding-bottom: :80px; width: 85%
           } 
           .tc
           {
            background-color: #fff; margin:0px 0px 0px 90px; border-radius: 8px; width: 85%;line-height: 3;
           }
           #head_tc > span>img
           {
            display: inline; width: 7%; vertical-align:top !important;
           } 
                     
        }
        @media (max-width: 767px)
        {
            .terms_conds > h3
           {
            font-size:20px;
           }
           .terms
           {
            line-height: 3;
           }
           ul > li
           {
            line-height: 1.8;
           }
           #head_tc > span>img
           {            
            display: inline; width: 21%; vertical-align:top !important;
           }
        }
    </style>
    </head>
    <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WDK2CML"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- Google Code for Google_Web_Conversion_Tracking Conversion Page -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 865788067;
    var google_conversion_label = "foFcCP6U-3AQo8HrnAM";
    var google_remarketing_only = false;
    /* ]]> */
    </script>

    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/865788067/?label=foFcCP6U-3AQo8HrnAM&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>

    <?php 
    if(isset($_GET))
    {            
        $id = $_GET['id'];
        $booking_id = base64_decode($id);

        
        $search_query  = "SELECT user_id FROM user_booking_tb where booking_id='$booking_id'";
        $result        = mysqli_query($conn,$search_query);
        $user_id_arr   = mysqli_fetch_array($result);
        $user_id       = $user_id_arr[0];


        $get_number     = "SELECT mobile_number FROM user_register where reg_id='$user_id'";
        $result_num     = mysqli_query($conn,$get_number);
        $mobile_arr     = mysqli_fetch_array($result_num);
        $mobile         = $mobile_arr[0];

        if($booking_id!='')
        {
            echo '<nav>
           <div class="text-center">
               <div class="container">
                   <a href="./index.php"><img src="https://static.gobumpr.com/img/logo-new-gb.svg" class="gobumpr_logo" alt="GoBumpr car painting services Online"></a>
               </div>
            </div> 
        </nav> 
        <div class=""><br/><br/></div>
        <div id="enquiry_received"> 
            <div class="container text-center">
                <img src="https://static.gobumpr.com/tyres/confirmation/check.svg" class="check" alt="confirmed booking image">
            </div>
            <div class="text-center booking_details">
                <h4><strong>Thanks for your interest! We will share the details shortly!</strong></h4>
            </div>            
        </div>
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 text-right">
                        <img src="https://static.gobumpr.com/tyres/confirmation/relax.svg" class="relax_image" alt="Hassle free booking img">
                    </div>
                    <div class="col-xs-6 text-left">
                        <h3 class="text_content">SIT BACK & RELAX</h3>
                        <h4>YOU ARE DONE FOR NOW.</h4>
                        <p>You will soon receive a text message with the price details</p>
                    </div>
                </div>
            </div>
        </footer>

        <div class="container tc" >
            <div class=" text-center terms_conds">
                <h3 id="head_tc"> Terms and conditions for Rs.300 <span><img src="./images/Paytm_logo.png" style=""> </span>cashback</h3>
            </div>
            <hr/>
            <div class="row col-md-12 terms">
                <div class="col-md-3"><p><strong>Validity</strong></p>
                </div>
                <div class="col-md-8"><p>Until 31<sup>st</sup> December 2019</p>
                </div>
            </div><br/>
            <div class="row col-md-12 terms">
                <div class="col-md-3"><p><strong>Applicability </strong></p>
                </div>
                <div class="col-md-8"><p>Applicable only for users who book their service via a festive landing page and not through any other marketing medium.</p>
                </div>
            </div><br/>
            <div class="row col-md-12 terms">
                <div class="col-md-3"><p><strong>Eligibility for Cashback</strong> </p>
                </div>
                <div class="col-md-8"><p>Users who book and complete their service are eligible for the 300 cashback.</p>
                </div>
            </div><br/><br/>

            <div class="row col-md-12 terms">
                <div class="col-md-3"><p style="line-height: 3.4"><strong>Cashback Disbursal</strong> </p>
                </div> 
            </div>               
            <div class="row col-md-12">
                <ul>
                    <li>A Survey link will be sent to the customers who have successfully completed service via SMS on every Wednesday.</li>
                    <li>Cashback will be processed every Friday on or after 4 PM only for only those who filled in the survey link before Friday 2 PM.</li>
                    <li> For those who fill in the survey link after 2 PM, the cashback will be processed the following Friday.</li>
                    <li>Note that the cashback will be processed only to the customers who provide a valid Paytm number. No other mode of payment shall be entertained.</li>
                </ul>                
            </div>                
        </div>
        <br/>
        <br/>
                <div class="frame" style="visibility:hidden">
                    <iframe src="https://tracking.icubeswire.co/aff_a?offer_id=567&adv_sub1='.$booking_id.'&adv_sub2='.$mobile.'" width="1" height="1" id="pixelcodeurl" ></iframe>
                </div>';
        }
    }

        ?>
    
        
    </body>
</html>
<!DOCTYPE HTML>

<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>GoBumpr Bridge</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="sirisha@gobumpr" />
	<meta name="google-signin-client_id" content="1048683085897-93l56i7l1ko54634j3mivacsn93b3r3j.apps.googleusercontent.com">

		<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- google sigin -->
	<!--<script src="js/signinplatform.js"></script> -->
	<script src="https://apis.google.com/js/platform.js"></script>

		<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

  </script>
  <script>
  var googleUser = {};
  var startApp = function() {
    gapi.load('auth2', function(){
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      auth2 = gapi.auth2.init({
        client_id: '1048683085897-93l56i7l1ko54634j3mivacsn93b3r3j.apps.googleusercontent.com',
		cookiepolicy: 'single_host_origin',
		
        // Request scopes in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
      attachSignin(document.getElementById('customBtn'));
    });
  };

  function attachSignin(element) {
    //console.log(element.id);
    auth2.attachClickHandler(element, {},
        function(googleUser) {
         // document.getElementById('name').innerText = "Signed in: " + googleUser.getBasicProfile().getName();
			  var logout = <?php if(isset($_GET['lo'])){ echo $_GET['lo']; } else{ echo 0; } ?>;
			//  var url = "<?php if(isset($_GET['url'])){ echo $_GET['url']; } else{ echo ""; } ?>";			  
	if(logout === 1){
		var auth2 = gapi.auth2.getAuthInstance();
    	auth2.signOut().then(function () {
      //	console.log('User signed out.');
		///  window.location.href = 'index.php?url='+url;  
		});
	}
	//else{
		
		var profile = googleUser.getBasicProfile();
  var id = profile.getId();
  var name = profile.getName();
  var image = profile.getImageUrl();
  var email = profile.getEmail();
  var url1 = "<?php if(isset($_GET['url'])){ echo $_GET['url']; } else{ echo ""; } ?>";
 // console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
 //console.log('Name: ' + profile.getName());
 // console.log('Image URL: ' + profile.getImageUrl());
 // console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
 $.ajax({
	url : "login.php",  // create a new php page to handle ajax request
	type : "POST",
	data : {"id":id,"name":name,"image":image,"email":email,"url1":url1},
	success : function(data) {
		//alert(data);
		//console.log(data);
		if(data === "no"){
			alert("Access Denied!");
		}
		else{
			window.location.href = data;
		}

    },
	error: function(xhr, ajaxOptions, thrownError) {
	           // alert(xhr.status + " "+ thrownError);
	}
  });
//}
        }, function(error) {
         // alert("An Undefined Error Occured!");
        });
  }

</script>
  
<style type = "text/css">

#customBtn {
	display: inline-block;
	background: #009688;
	color: white;
	white-space: nowrap;
}
#customBtn:hover {
	cursor: pointer;
}
span.buttonText {
	display: inline-block;
	vertical-align: middle;
	padding: 15px 40px 15px 40px;
	font-size: 18px;
	font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;
}
@media only screen and (max-width: 500px) {
    .img{
		width : 20em;
		height : 12em;
	}
	.p{
		margin-top:12em;
	}
}
@media only screen and (max-width: 750px) {
    .img{
		width : 20em;
		height : 12em;
	}
	.p{
		margin-top:12em;
	}
}
body {
	background-image: url(https://static.gobumpr.com/img/bridge-bg.jpg);
    background-repeat: no-repeat;
    background-size: cover;
	height: -webkit-fill-available;
}
</style> 

</head>
<body style="background-color:#fafafa;">

<div style="margin-top:50px;" align="center">
	<img class = "img" src="https://static.gobumpr.com/img/logo-new-gb.svg" width="550" height="200" />
		<div id="gSignInWrapper">
			<div id="customBtn" class="customGPlusSignIn">
				<span class="buttonText">Login With Google</span>
			</div>
		</div>
		<div id="name"></div>
	<script>startApp();</script>
	<div style="margin-top:40px;font-size:12px;color:#667377;	font-family: Segoe UI, Frutiger, Frutiger Linotype,Dejavu Sans, Helvetica Neue, Arial, sans-serif;">
		<p class="p">© 2018 - Northerly Automotive Solutions Private Limited. All rights reserved. </p>
	</div>
</div>
</body>
</html>

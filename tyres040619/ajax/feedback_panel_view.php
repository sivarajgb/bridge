<?php
error_reporting(E_ALL);
include("../config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();
$today=date('Y-m-d');

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

 $startdate = date('Y-m-d',strtotime($_POST['startdate']));
 $enddate =  date('Y-m-d',strtotime($_POST['enddate']));
 $shop_id = $_POST['shop_id'];
 $city = $_POST['city'];
 $followup = $_POST['followup'];
 $ratings = $_POST['ratings'];

 $_SESSION['crm_city'] = $city;


 $cond_city_all ='';
 $cond_city_all = $cond_city_all.($city == 'all' ? "" : "AND b.city='$city'");

 // $cond_shop_all ='';
 // $cond_shop_all = $cond_shop_all.($city == 'all' ? "" : "AND b2b_b.b2b_shop_id='$shop_id'");
 
 
 // $cond = "sh$shop_id_val_c$city_val"();

if($ratings == 'all')
{
	$cond1 = "";
}
if($ratings == '1')
{
	$cond1 = "AND b2b_b.b2b_Rating IN (0.5,1)";
}
if($ratings == '2')
{
	$cond1 = "AND b2b_b.b2b_Rating IN (1.5,2)";
}
if($ratings == '3')
{
	$cond1 = "AND b2b_b.b2b_Rating IN (2.5,3)";
}
if($ratings == '4')
{
	$cond1 = "AND b2b_b.b2b_Rating IN (3.5,4)";
}
if($ratings == '5')
{
	$cond1 = "AND b2b_b.b2b_Rating IN (4.5,5)";
}
if($ratings == 'none')
{
	$cond1 = "AND b2b_b.b2b_Rating = 0 OR b2b_b.b2b_Rating IS NULL";
}


$sql_partner = "SELECT b2b_shop_id FROM b2b.b2b_credits_tbl WHERE b2b_partner_flag = 2;";
//echo $sql_partner;

$res_partner = mysqli_query($conn2,$sql_partner);
while($row_partner = mysqli_fetch_object($res_partner)){
	$premium_partner_arr[] = $row_partner->b2b_shop_id;
}
if($followup=="yes")
{
	$sql_booking = "SELECT b2b_b.b2b_shop_id,b2b_b.b2b_tyre_brand,b2b_b.b2b_tyre_count,b2b_b.b2b_log, b2b_b.gb_booking_id, b2b_b.b2b_booking_id,b2b_b.b2b_customer_name as name, b2b_b.b2b_cust_phone as mobile_number,amt.axle_id,amt.shop_name,b2b_b.brand,b2b_b.model,b.service_type,b.feedback_status,b.service_status,b.feedback_followup,b2b_b.b2b_check_in_report,b2b_b.b2b_vehicle_at_garage,b2b_b.b2b_vehicle_ready,b2b_b.b2b_acpt_flg,b2b_b.b2b_bid_amt,b2b_b.b2b_Feedback,b2b_b.b2b_Rating,b.final_bill_amt,b.feedback AS comments,b.service_status_reason,b.user_id FROM go_bumpr.tyre_booking_tb b LEFT JOIN b2b.b2b_booking_tbl_tyres b2b_b ON b.booking_id = b2b_b.gb_booking_id LEFT JOIN go_bumpr.admin_mechanic_table as amt ON amt.axle_id=b2b_b.b2b_shop_id WHERE b.axle_flag= 1 AND b2b_b.b2b_acpt_flg= 1 AND b.service_status!= 'Purchased' AND b.feedback_status IN (2,3,11,12,21,22,31,41,51) AND b2b_b.b2b_flag = 0 AND DATE(b.feedback_followup) BETWEEN '$startdate' and '$enddate' AND b2b_b.b2b_shop_id NOT IN (1014,1035,1670) $cond1 {$cond_city_all} ORDER BY b.feedback_followup ASC";
	//echo $sql_booking;
}
else
{
	$sql_booking = "SELECT b2b_b.b2b_shop_id,b2b_b.b2b_tyre_brand,b2b_b.b2b_tyre_count,b2b_b.b2b_log, b2b_b.gb_booking_id, b2b_b.b2b_booking_id, b2b_b.b2b_customer_name as name, b2b_b.b2b_cust_phone as mobile_number,amt.axle_id,amt.shop_name,b2b_b.brand,b2b_b.model,b.service_type,b.feedback_status,b.service_status,b.feedback_followup,b2b_b.b2b_check_in_report,b2b_b.b2b_vehicle_at_garage,b2b_b.b2b_vehicle_ready,b2b_c.b2b_partner_flag,b2b_b.b2b_bid_amt,b2b_b.b2b_Feedback,b2b_b.b2b_Rating,b.final_bill_amt,b.feedback AS comments,b.service_status_reason,b.user_id,b2b_b.b2b_acpt_flg FROM go_bumpr.tyre_booking_tb b LEFT JOIN b2b.b2b_booking_tbl_tyres b2b_b ON b.booking_id = b2b_b.gb_booking_id  LEFT JOIN b2b.b2b_credits_tbl b2b_c ON b2b_b.b2b_shop_id = b2b_c.b2b_shop_id LEFT JOIN go_bumpr.admin_mechanic_table as amt ON amt.axle_id=b2b_b.b2b_shop_id WHERE b.axle_flag= 1 AND b2b_b.b2b_acpt_flg= 1 AND b.feedback_status IN (0 , 1) AND b2b_b.b2b_flag = 0 AND DATE(b2b_b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b2b_b.b2b_shop_id NOT IN (1014,1035,1670) $cond1 {$cond_city_all} ORDER BY b2b_c.b2b_partner_flag DESC,b2b_b.b2b_log ASC";
//	echo $sql_booking;
}
$res_booking = mysqli_query($conn2,$sql_booking);
$no = 0;
// echo $sql_booking;

if($followup == 'no')
{
	if(mysqli_num_rows($res_booking) >=1){

while($row_booking = mysqli_fetch_object($res_booking)){
	$go_axle_date = $row_booking->b2b_log;
	$booking_id = $row_booking->gb_booking_id;
	$user_id = $row_booking->user_id;
	$b2b_booking_id = $row_booking->b2b_booking_id;
	$user_name = $row_booking->name;
	$user_mobile = $row_booking->mobile_number;
	$shop_name = $row_booking->shop_name;
	$shop_id = $row_booking->b2b_shop_id;
	$brand = $row_booking->brand;
	$model = $row_booking->model;
	//$service_type = $row_booking->service_type;
	$inspection_stage = $row_booking->b2b_check_in_report;
	$estimate_stage = $row_booking->b2b_vehicle_at_garage;
	$deliver_stage = $row_booking->b2b_vehicle_ready;
	$premium = $row_booking->b2b_partner_flag;
	$acpt = $row_booking->b2b_acpt_flg;
	$garage_bill = $row_booking->b2b_bid_amt;
	$rtt_rating = $row_booking->b2b_Rating;
	$rtt_feedback = $row_booking->b2b_Feedback;
	$final_paid_bill = $row_booking->final_bill_amt;
	$comments = $row_booking->comments;
	$feedback_status = $row_booking->feedback_status;
	$service_status = $row_booking->service_status;
	$service_status_reason = $row_booking->service_status_reason;
	$b2b_tyre_brand = $row_booking->b2b_tyre_brand;
	$b2b_tyre_count = $row_booking->b2b_tyre_count;

	if($row_booking->feedback_followup == '0000-00-00 00:00:00'){ $feedback_followup_date = 'Not Updated'; } else { $feedback_followup_date = date('d-m-Y', strtotime($row_booking->feedback_followup));};

	if($inspection_stage == 1 || $estimate_stage == 1 || $deliver_stage == 1)
	{
		$rtt_updates = 1;
	}
	else
	{
		$rtt_updates = 0;
	}

	$rnr_status_arr = array(11,12,21,22,31,41,51);
	if(in_array($feedback_status,$rnr_status_arr))
	{
		$fdbck_status = 'RNR';
	}
	if($feedback_status== 1 || $feedback_status == -1)
	{
		$fdbck_status = 'Purchased';
	}
	if($feedback_status== 2 || $feedback_status == -2)
	{
		$fdbck_status = 'Yet to Purchased';
	}
	if($feedback_status== 0)
	{
		$fdbck_status = 'new';
	}

	$date1 = date_create(date('Y-m-d',time()));
	$date2 = date_create(date('Y-m-d', strtotime($go_axle_date)));
	$difference = date_diff($date1,$date2);
	$feedback_cycle = 0;
	if(($rtt_rating != '' || $rtt_rating != 0) && ($service_status == 'Purchased' || $service_status == 'Purchased') && ($final_paid_bill != 0 || $final_paid_bill != ''))
	{
		$feedback_cycle = 1;
	}
	elseif(($difference->format("%a") > 30) && ($inspection_stage == 1 && $deliver_stage != 1))  //(($inspection_stage == 1 && $deliver_stage != 1) && ($difference->format("%a") > 30))
	{
		$feedback_cycle = -1;
	}
	
	if($feedback_cycle == -1)
	{
		$tr1 = "<tr style = 'background-color:#f0e68cc2;'>";
	}
	else
	{
		$tr1 = "<tr>";
	}
	$td1 = "<td style='vertical-align: middle;'>".$no=$no+1 ; 
	if($feedback_cycle ==1) { 
		$td1 = $td1."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Done!'></i>";
	}
	if($feedback_status == '11'){ $td1 = $td1."<i class='fa fa-phone' style='font-size:20px;color:#305D02' aria-hidden='true'></i> ";}
	if($feedback_status == '12'){ $td1 = $td1."<i class='fa fa-phone' style='font-size:20px;color:#5BA829' aria-hidden='true'></i> ";}
	if($feedback_status == '21'){ $td1 = $td1."<i class='fa fa-phone' style='font-size:20px;color:#9ACD32' aria-hidden='true'></i> ";}
	if($feedback_status == '22'){ $td1 = $td1."<i class='fa fa-phone' style='font-size:20px;color:#CDD614' aria-hidden='true'></i> ";}
	if($feedback_status == '31'){ $td1 = $td1."<i class='fa fa-phone' style='font-size:20px;color:#FFBA00' aria-hidden='true'></i> ";}
	if($feedback_status == '41'){ $td1 = $td1."<i class='fa fa-phone' style='font-size:20px;color:#FF7800' aria-hidden='true'></i> ";}
	if($feedback_status == '51'){ $td1 = $td1."<i class='fa fa-phone' style='font-size:20px;color:#C60714' aria-hidden='true'></i> ";}
	$td2 = "<td style='vertical-align: middle;'>".date('d M Y', strtotime($go_axle_date))."</td>";
	$td3 = "<td style='vertical-align: middle;'>";
	if($acpt == '1')
	{
		$td3 = $td3."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Accepted!'></i>";
	}
	else if($acpt =='0')
	{
		$td3 = $td3."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Denied!'></i>";
	}
	else
	{
		$td3 = $td3."<i class='fa fa-exclamation-circle' aria-hidden='true' style='font-size:24px;color:#ffa800!important;' title='Not Accepted!'></i>";
	}
	$td3 = $td3."</td>";
	$td4 = "<td style='vertical-align: middle;'>".$booking_id."</td>";
	$td5 = "<td style='vertical-align: middle;'>".$user_name."</td>";
	$td6 = "<td style='vertical-align: middle;'>".$user_mobile."</td>";
	$td7 = "<td style='vertical-align: middle;'>".$shop_name;
	if ($premium == 2) {  $td7 = $td7."<img src='images/authorized.png' style='width:22px;' title='Premium Garage'>";}
	$td7 = $td7."</td>";
	$td8 = "<td style='vertical-align: middle;'>".$brand." ".$model."</td>";
	$td9 = "<td style='vertical-align: middle;'>".$b2b_tyre_brand."</td>";
	$td20 = "<td style='vertical-align: middle;'>".$b2b_tyre_count."</td>";
	$td10 = "<td style='vertical-align: middle;'>";
	$garage_bill!='' ? $td10 = $td10.'<i class="fa fa-inr" aria-hidden="true" style="padding-right: 2px;padding-left: 1px;"></i>'.$garage_bill : $td10 = $td10.'Not updated';
	$td10 = $td10."</td>";
	$td11 = "<td style='vertical-align: middle;'>";
	$rtt_rating!='' ? $td11 = $td11.$rtt_rating : $td11 = $td11.'Not updated';
	$td11 = $td11."</td>";
	$td12 = "<td style='vertical-align: middle;'>";
	$rtt_feedback!='' ? $td12 = $td12.$rtt_feedback : $td12 = $td12.'Not updated';
	$td12 = $td12."</td>";
	$td13 = "<td style='vertical-align: middle;'>";
	$final_paid_bill!='' ? $td13 = $td13.'<i class="fa fa-inr" aria-hidden="true" style="padding-right: 2px;padding-left: 1px;"></i>'.$final_paid_bill : $td13 = $td13.'Not updated';
	$td13 = $td13."</td>";
	$td14 = "<td style='vertical-align: middle;'>";
	$comments!='' ? $td14 = $td14.$comments : $td14 = $td14.'Not updated';
	$td14 = $td14."</td>";
	$td16 = "<td style='vertical-align: middle;'>";
	$acpt == '1' ? $td16 = $td16."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Done!'></i>" : $td16 = $td16."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Not updated!'></i>";
	$td16 = $td16."</td>";
	$td17 = "<td style='vertical-align: middle;'>";
	$feedback_status == '1' ? $td17 = $td17."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Done!'></i>" : $td17 = $td17."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Not updated!'></i>";
	$td17 = $td17."</td>";
	$td18 = "<td style='vertical-align: middle;'>";
	if($service_status != 'Purchased') { $service_status_reason!='' ? $td18 = $td18.$service_status_reason : $td18 = $td18.'Not updated'; }
	else {
		$td18 = $td18.'Not updated';
	}
	
	$td18 = $td18."</td>";
	$td19 = "<td style='vertical-align: middle;'>";
	if($service_status != 'Purchased') {
		$td19 = $td19.$service_status;
		$td19 = $td19."<button class='btn btn-sm' style='background-color:#9fe88d;' data-toggle='modal' data-target='#myModal".$booking_id."' title='Edit his Service Status!'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>&nbsp;Edit</button>";
	 	$td19 = $td19."<div class='modal fade' id='myModal".$booking_id."' role='dialog' >";
		$td19 = $td19."<div class='modal-dialog' align='center' style='width:90%;'>";
		$td19 = $td19."<div class='modal-content'  style='width:100%;'>";
		$td19 = $td19."<div class='modal-header'>";
		$td19 = $td19."<button type='button' class='close' data-dismiss='modal'>&times;</button>";
		$td19 = $td19."<h3 class='modal-title' align='left'>Booking Id (".$booking_id.")</h3>";
		$td19 = $td19."</div>";
		$td19 = $td19."<div class='modal-body'><div class='container'><div class='div1 col-md-7'>";
		$sql_feedback_history = "SELECT com.category,com.comments,com.Follow_up_date,com.log,crm.name as crm_name FROM tyres_comments_tbl com LEFT JOIN crm_admin crm ON com.crm_log_id = crm.crm_log_id WHERE com.book_id = '$booking_id' AND com.status='FeedBack' ORDER BY com.log DESC";
		$res_feedback_history = mysqli_query($conn1, $sql_feedback_history);
		if(mysqli_num_rows($res_feedback_history)>0)
		{
			$td19 = $td19."<table class='table table-hover table-striped'><thead><tr><th>Date</th><th>Status</th><th>Support Person</th></tr></thead><tbody>";
			while($row_feedback_history = mysqli_fetch_object($res_feedback_history))
			{
				$prev_status = $row_feedback_history->category." - ".$row_feedback_history->comments;
				$crm_name = $row_feedback_history->crm_name;
				$log = date('d-M-Y',strtotime($row_feedback_history->log));
				$td19 = $td19."<tr><td style='width:130px;'>".$log."</td><td>".$prev_status."</td><td>".$crm_name."</td></tr>";
			}
			$td19 = $td19."</tbody></table>";
		}
		else
		{
			$td19 = $td19."<p style='margin:50 0 0 0px'>No Records Found</p>";
		}
		$td19 = $td19."</div><div class='div2 col-md-5'>";
		$page = base64_encode('idle');
		$td19 = $td19."<form id='feedback".$booking_id."' class='form' method='post' action='update_feedback.php'>";
		$td19 = $td19."<div class='row'><div class='col-xs-8 col-lg-offset-2 form-group'><br>";
		$td19 = $td19."<select class='form-control' id='status".$booking_id."' name='status' required>";
		switch($fdbck_status){
			case 'RNR':
						$td19 = $td19."<option value='RNR' selected>RNR</option><option value='Purchased'>Purchased</option><option value='Yet to Purchased' >Yet to Purchased</option>";
						break;
			case 'Purchased':
						$td19 = $td19."<option value='RNR'>RNR</option><option value='Purchased' selected>Purchased</option><option value='Yet to Purchased' >Yet to Purchased</option>";
						break;
			case 'Yet to Purchased':
						$td19 = $td19."<option value='RNR'>RNR</option><option value='Purchased'>Purchased</option><option value='Yet to Purchased' selected >Yet to Purchased</option>";
						break;
			default:
						$td19 = $td19."<option value='' selected>Select Status</option><option value='RNR'>RNR</option><option value='Purchased'>Purchased</option><option value='Yet to Purchased' >Yet to Purchased</option>";
						break;
		}
		$td19 = $td19."</select></div></div>";
		$td19 = $td19."<script>";
		$td19 = $td19.'var reason = "'.$fdbck_status.'";';
		$td19 = $td19."switch(reason)
						{
							case 'RNR' : $('#followupDate".$booking_id."').hide();
										 $('#completed-div".$booking_id."').hide();
										 $('#comments".$booking_id."').hide();
										 $('#reason".$booking_id."').hide();
										break;";
		
		$td19 = $td19."case 'Purchased' : $('#followupDate".$booking_id."').hide();
											   $('#completed-div".$booking_id."').show();
											   $('#comments".$booking_id."').show();
											   $('#reason".$booking_id."').hide();
											   break;";
		$td19 = $td19."case 'Yet to Purchased' : $('#followupDate".$booking_id."').hide();
												   $('#completed-div".$booking_id."').hide();
												   $('#comments".$booking_id."').show();
												   $('#reason".$booking_id."').show();
												   break;}";
		$td19 = $td19."$('#reason-select".$booking_id."').change(function(){var reason2 = $('#reason-select".$booking_id."').val();";
		
		$td19 = $td19.'$("#status'.$booking_id.'").change(function(){';
		$td19 = $td19.'var reason = $("#status'.$booking_id.'").val();';
		$td19 = $td19."switch(reason){";
		$td19 = $td19."case 'RNR' : $('#followupDate".$booking_id."').hide();
										 $('#completed-div".$booking_id."').hide();
										 $('#comments".$booking_id."').hide();
										 $('#reason".$booking_id."').hide();
										break;";
		
		$td19 = $td19."case 'Purchased' : $('#followupDate".$booking_id."').hide();
											   $('#completed-div".$booking_id."').show();
											   $('#comments".$booking_id."').show();
											   $('#reason".$booking_id."').hide();
											   break;";
		$td19 = $td19."case 'Yet to Purchased' : $('#followupDate".$booking_id."').hide();
												   $('#completed-div".$booking_id."').hide();
												   $('#comments".$booking_id."').show();
												   $('#reason".$booking_id."').show();
												   break;";
		$td19 = $td19."default : $('#followupDate".$booking_id."').hide();
												   $('#completed-div".$booking_id."').hide();
												   $('#comments".$booking_id."').hide();
												   $('#reason".$booking_id."').hide();break;}";
		$td19 = $td19."$('#reason-select".$booking_id."').change(function(){var reason2 = $('#reason-select".$booking_id."').val();";
	
		$td19 = $td19.'});';
		$td19 = $td19."</script>";
		$td19 = $td19."<div class='row'></div><div class='row' id='reason".$booking_id."' style='display:none;'>";
		$td19 = $td19."<div class='col-lg-2 col-xs-1 col-lg-offset-2 form-group'>";
		$td19 = $td19."<label style='padding-top:5px;'>Reason</label></div>";
		$td19 = $td19."<div class='col-lg-6 col-xs-6' style='font-size:8px;'>";
		$td19 = $td19."<select class='form-control' name='reason' id='reason-select".$booking_id."' >";
		$td19 = $td19."<option value='' selected>-Choose a reason-</option>
						<option value='Purchased with local shop'>Purchased with local shop</option>
						<option value='Improper communication from the garage' >Improper communication from the garage</option>
						<option value='Not in Chennai' >Not in Chennai</option>
						<option value='Pricing was too high' >Pricing was too high</option>
						<option value='Quality was not good' >Quality was not good</option>
						<option value='Others' >Others</option>";
		$td19 = $td19."</select></div></div>";
		$td19 = $td19."<div class='row'><div class='row' id='followupDate".$booking_id."' style='display:none;'><div class='col-xs-4 col-lg-4 col-lg-offset-2 form-group'>";
		$td19 = $td19."<label style='padding-top:5px;' id='followup_date_txt".$booking_id."'>FollowUp Date</label>";
		$td19 = $td19."</div><div class='col-xs-4'><div class='form-group' style='margin-right:10px;'>";
		$td19 = $td19."<input type='text' name='followup_date' class='form-control datepicker' data-date-format='dd-mm-yyyy' type='text' />";
		$td19 = $td19."</div></div></div></div>";
		$td19 = $td19."<div id='completed-div".$booking_id."' style='display:none;'><div class='row'><div class='col-lg-2 col-xs-1 col-lg-offset-2 form-group'>";
		$td19 = $td19."<label style='padding-top:5px;'>Rating</label>";
		$td19 = $td19."</div><div class='col-lg-6 col-xs-6' style='font-size:8px;'>";
		$td19 = $td19."<input id='rating".$booking_id."' name='rating' type='text' class='rating' data-min='0' data-max='5' data-step='0.5' data-stars=5 data-symbol='&#xe005;' data-default-caption='{rating} hearts' data-star-captions='{}' title='' data-show-clear='false' data-show-caption='false' value='".$rtt_rating."'>";
		$td19 = $td19."</div></div><div class='row'><div class='col-lg-2 col-xs-1 col-lg-offset-2 form-group'>";
		$td19 = $td19."<label style='padding-top:5px;'>&nbsp;Amount</label>";
		$td19 = $td19."</div><div class='col-xs-6'><div class='form-group'>";
		$td19 = $td19."<input type='number' name='final_bill_amount' id='final_bill_amount".$booking_id."' class='form-control' style='max-width:280px;' value='".$final_paid_bill."'/>";
		$td19 = $td19."</div></div></div></div>";
		$td19 = $td19."<div class='row' id='comments".$booking_id."' style='display:none;'><div class='col-xs-8 col-lg-offset-2'>";
		$td19 = $td19."<textarea name='comments' id='comments' class='form-control' rows='4' cols='40' placeholder='Comments...' style='width:270px;'>".$comments."</textarea>";
		$td19 = $td19."</div></div><div class='row'><br>";
		$td19 = $td19."<div class='form-group' align='center'><input class='form-control' id='submit".$booking_id."' type='submit' value='Apply' style='background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;width:90px;'/></div></div>";
		
		$td19 = $td19."<script>";
		$td19 = $td19."var reason2 = $('#reason-select".$booking_id."').val();console.log(reason2);";
		$td19 = $td19.'$(function () {$("#rating'.$booking_id.'").rating();});';
		$td19 = $td19."$(document).on('submit','#feedback".$booking_id."',function(e){";
		$td19 = $td19."if($( '#submit".$booking_id."' ).is(':visible')){";
		$td19 = $td19."e.preventDefault();$('#submit".$booking_id."').hide();";
		$td19 = $td19.'setTimeout(function () {$("#feedback'.$booking_id.'").trigger("submit"); }, 200);';
		$td19 = $td19."}});";
		$td19 = $td19."var date = new Date();date.setDate(date.getDate());";
		$td19 = $td19."$('.datepicker').datepicker({autoclose: true,startDate: date});$('input.datepicker').datepicker('setDate', 'today');";
		$td19 = $td19."</script>";
		
		$td19 = $td19."<input type='hidden' name='feedback_status' value='".$feedback_status."' >";
		$td19 = $td19."<input type='hidden' name='shop_name' value='".$shop_name."' >";
		$td19 = $td19."<input type='hidden' name='rtt_updates' value='".$rtt_updates."' >";
		$td19 = $td19."<input type='hidden' name='s_date' value='".date('Y-m-d',strtotime($startdate))."' >";
		$td19 = $td19."<input type='hidden' name='e_date' value='".date('Y-m-d',strtotime($enddate))."' >";
		$td19 = $td19."<input type='hidden' name='b2b_book_id' value='".$b2b_booking_id."' >";
		$td19 = $td19."<input type='hidden' name='user_id' value='".$user_id."' >";
		$td19 = $td19."<input type='hidden' name='book_id' value='".$booking_id."' >";
		$td19 = $td19."<input type='hidden' name='premium' value='".$premium."' >";
		$td19 = $td19."<input type='hidden' name='g_date' value='".date('Y-m-d',strtotime($go_axle_date))."' >";
		$td19 = $td19."</form></div></div></div></div></div></div>";
	}
	else
	{
		$td19 = $td19."Purchased";
	}
	$td19 = $td19."</td>";
	$tr1_l = "</tr>";
	$str = $tr1.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$td9.$td20.$td10.$td11.$td12.$td13.$td14.$td16.$td17.$td18.$td19.$tr1_l;
	$data1[] = array('tr'=>$str);
}
$result['data1'] = $data1;

} // if
else {
	$result['data1'] = "no";
}
echo json_encode($result);
}
else
{
	if(mysqli_num_rows($res_booking) >=1){

while($row_booking = mysqli_fetch_object($res_booking)){
	$go_axle_date = $row_booking->b2b_log;
	$booking_id = $row_booking->gb_booking_id;
	$user_id = $row_booking->user_id;
	$b2b_booking_id = $row_booking->b2b_booking_id;
	$user_name = $row_booking->name;
	$user_mobile = $row_booking->mobile_number;
	$shop_name = $row_booking->shop_name;
	$shop_id = $row_booking->b2b_shop_id;
	$brand = $row_booking->brand;
	$model = $row_booking->model;
	$acpt = $row_booking->b2b_acpt_flg;
	$inspection_stage = $row_booking->b2b_check_in_report;
	$estimate_stage = $row_booking->b2b_vehicle_at_garage;
	$deliver_stage = $row_booking->b2b_vehicle_ready;
	$b2b_tyre_brand = $row_booking->b2b_tyre_brand;
	$b2b_tyre_count = $row_booking->b2b_tyre_count;
	if(in_array($shop_id,$premium_partner_arr))
		{
			$premium = 2;
		}
		else
		{
			$premium = 0;
		}
	$garage_bill = $row_booking->b2b_bid_amt;
	$rtt_rating = $row_booking->b2b_Rating;
	$rtt_feedback = $row_booking->b2b_Feedback;
	$final_paid_bill = $row_booking->final_bill_amt;
	$comments = $row_booking->comments;
	$feedback_status = $row_booking->feedback_status;
	$service_status = $row_booking->service_status;
	$service_status_reason = $row_booking->service_status_reason;

	if($row_booking->feedback_followup == '0000-00-00 00:00:00'){ $feedback_followup_date = 'Not Updated'; } else { $feedback_followup_date = date('d-m-Y', strtotime($row_booking->feedback_followup));};

	if($inspection_stage == 1 || $estimate_stage == 1 || $deliver_stage == 1)
	{
		$rtt_updates = 1;
	}
	else
	{
		$rtt_updates = 0;
	}

	$rnr_status_arr = array(11,12,21,22,31,41,51);
	if(in_array($feedback_status,$rnr_status_arr))
	{
		$fdbck_status = 'RNR';
	}
	if($feedback_status== 1 || $feedback_status == -1)
	{
		$fdbck_status = 'Purchased';
	}
	if($feedback_status== 2 || $feedback_status == -2)
	{
		$fdbck_status = 'Yet to Purchased';
	}
	if($feedback_status== 0)
	{
		$fdbck_status = 'new';
	}

	$date1 = date_create(date('Y-m-d',time()));
	$date2 = date_create(date('Y-m-d', strtotime($go_axle_date)));
	$difference = date_diff($date1,$date2);
	$feedback_cycle = 0;
	if(($rtt_rating != '' || $rtt_rating != 0) && ($service_status == 'Purchased' || $service_status == 'Purchased') && ($final_paid_bill != 0 || $final_paid_bill != ''))
	{
		$feedback_cycle = 1;
	}
	elseif(($difference->format("%a") > 30) && ($inspection_stage == 1 && $deliver_stage != 1))  //(($inspection_stage == 1 && $deliver_stage != 1) && ($difference->format("%a") > 30))
	{
		$feedback_cycle = -1;
	}
	
	if($feedback_cycle == -1)
	{
		$tr1 = "<tr style = 'background-color:#f0e68cc2;'>";
	}
	else
	{
		$tr1 = "<tr>";
	}
	$td1 = "<td style='vertical-align: middle;'>".$no=$no+1 ; 
	if($feedback_cycle ==1) { 
		$td1 = $td1."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Done!'></i>";
	}
	if($feedback_status == '11'){ $td1 = $td1."<i class='fa fa-phone' style='font-size:20px;color:#305D02' aria-hidden='true'></i> ";}
	if($feedback_status == '12'){ $td1 = $td1."<i class='fa fa-phone' style='font-size:20px;color:#5BA829' aria-hidden='true'></i> ";}
	if($feedback_status == '21'){ $td1 = $td1."<i class='fa fa-phone' style='font-size:20px;color:#9ACD32' aria-hidden='true'></i> ";}
	if($feedback_status == '22'){ $td1 = $td1."<i class='fa fa-phone' style='font-size:20px;color:#CDD614' aria-hidden='true'></i> ";}
	if($feedback_status == '31'){ $td1 = $td1."<i class='fa fa-phone' style='font-size:20px;color:#FFBA00' aria-hidden='true'></i> ";}
	if($feedback_status == '41'){ $td1 = $td1."<i class='fa fa-phone' style='font-size:20px;color:#FF7800' aria-hidden='true'></i> ";}
	if($feedback_status == '51'){ $td1 = $td1."<i class='fa fa-phone' style='font-size:20px;color:#C60714' aria-hidden='true'></i> ";}
	$td2 = "<td style='vertical-align: middle;'>".date('d M Y', strtotime($go_axle_date))."</td>";
	$td4 = "<td style='vertical-align: middle;'>".$booking_id."</td>";
	$td5 = "<td style='vertical-align: middle;'>".$user_name."</td>";
	$td6 = "<td style='vertical-align: middle;'>".$user_mobile."</td>";
	$td7 = "<td style='vertical-align: middle;'>".$shop_name;
	if ($premium == 2) {  $td7 = $td7."<img src='images/authorized.png' style='width:22px;' title='Premium Garage'>";}
	$td7 = $td7."</td>";
	$td8 = "<td style='vertical-align: middle;'>".$brand." ".$model."</td>";
	$td9 = "<td style='vertical-align: middle;'>".$b2b_tyre_brand."</td>";
	$td20 = "<td style='vertical-align: middle;'>".$b2b_tyre_count."</td>";
	$td10 = "<td style='vertical-align: middle;'>";
	$garage_bill!='' ? $td10 = $td10.'<i class="fa fa-inr" aria-hidden="true" style="padding-right: 2px;padding-left: 1px;"></i>'.$garage_bill : $td10 = $td10.'Not updated';
	$td10 = $td10."</td>";
	$td11 = "<td style='vertical-align: middle;'>";
	$rtt_rating!='' ? $td11 = $td11.$rtt_rating : $td11 = $td11.'Not updated';
	$td11 = $td11."</td>";
	$td12 = "<td style='vertical-align: middle;'>";
	$rtt_feedback!='' ? $td12 = $td12.$rtt_feedback : $td12 = $td12.'Not updated';
	$td12 = $td12."</td>";
	$td13 = "<td style='vertical-align: middle;'>";
	$final_paid_bill!='' ? $td13 = $td13.'<i class="fa fa-inr" aria-hidden="true" style="padding-right: 2px;padding-left: 1px;"></i>'.$final_paid_bill : $td13 = $td13.'Not updated';
	$td13 = $td13."</td>";
	$td14 = "<td style='vertical-align: middle;'>";
	$comments!='' ? $td14 = $td14.$comments : $td14 = $td14.'Not updated';
	$td14 = $td14."</td>";
	
	$td16 = "<td style='vertical-align: middle;'>";
	$acpt == '1' ? $td16 = $td16."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Done!'></i>" : $td16 = $td16."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Not updated!'></i>";
	$td16 = $td16."</td>";
	$td17 = "<td style='vertical-align: middle;'>";
	$deliver_stage == '1' ? $td17 = $td17."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Done!'></i>" : $td17 = $td17."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Not updated!'></i>";
	$td17 = $td17."</td>";
	$td18 = "<td style='vertical-align: middle;'>";
	if($service_status != 'Purchased') { $service_status_reason!='' ? $td18 = $td18.$service_status_reason : $td18 = $td18.'Not updated'; }
	if (strpos($service_status_reason, 'Rescheduled') !== false) { $td18 = $td18."<br>- ".$feedback_followup_date;}
	$td18 = $td18."</td>";
	$td19 = "<td style='vertical-align: middle;'>";
	if($service_status != 'Purchased') {
		$td19 = $td19.$service_status;
		$td19 = $td19."<button class='btn btn-sm' style='background-color:#9fe88d;' data-toggle='modal' data-target='#myModal".$booking_id."' title='Edit his Service Status!'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>&nbsp;Edit</button>";
	 	$td19 = $td19."<div class='modal fade' id='myModal".$booking_id."' role='dialog' >";
		$td19 = $td19."<div class='modal-dialog' align='center' style='width:90%;'>";
		$td19 = $td19."<div class='modal-content'  style='width:100%;'>";
		$td19 = $td19."<div class='modal-header'>";
		$td19 = $td19."<button type='button' class='close' data-dismiss='modal'>&times;</button>";
		$td19 = $td19."<h3 class='modal-title' align='left'>Booking Id (".$booking_id.")</h3>";
		$td19 = $td19."</div>";
		$td19 = $td19."<div class='modal-body'><div class='container'><div class='div1 col-md-7'>";
		$sql_feedback_history = "SELECT com.category,com.comments,com.Follow_up_date,com.log,crm.name as crm_name FROM tyres_comments_tbl com LEFT JOIN crm_admin crm ON com.crm_log_id = crm.crm_log_id WHERE com.book_id = '$booking_id' AND com.status='FeedBack' ORDER BY com.log DESC";
		$res_feedback_history = mysqli_query($conn1, $sql_feedback_history);
		if(mysqli_num_rows($res_feedback_history)>0)
		{
			$td19 = $td19."<table class='table table-hover table-striped'><thead><tr><th>Date</th><th>Status</th><th>Support Person</th></tr></thead><tbody>";
			while($row_feedback_history = mysqli_fetch_object($res_feedback_history))
			{
				$prev_status = $row_feedback_history->category." - ".$row_feedback_history->comments;
				$crm_name = $row_feedback_history->crm_name;
				$log = date('d-M-Y',strtotime($row_feedback_history->log));
				$td19 = $td19."<tr><td style='width:130px;'>".$log."</td><td>".$prev_status."</td><td>".$crm_name."</td></tr>";
			}
			$td19 = $td19."</tbody></table>";
		}
		else
		{
			$td19 = $td19."<p style='margin:50 0 0 0px'>No Records Found</p>";
		}
		$td19 = $td19."</div><div class='div2 col-md-5'>";
		$page = base64_encode('idle');
		$td19 = $td19."<form id='feedback".$booking_id."' class='form' method='post' action='update_feedback.php'>";
		$td19 = $td19."<div class='row'><div class='col-xs-8 col-lg-offset-2 form-group'><br>";
		$td19 = $td19."<select class='form-control' id='status".$booking_id."' name='status' required>";
		switch($fdbck_status){
			case 'RNR':
						$td19 = $td19."<option value='RNR' selected>RNR</option><option value='Purchased'>Purchased</option><option value='Yet to Purchased' >Yet to Purchased</option>";
						break;
			case 'Purchased':
						$td19 = $td19."<option value='RNR'>RNR</option><option value='Purchased' selected>Purchased</option><option value='Yet to Purchased' >Yet to Purchased</option>";
						break;
			case 'Yet to Purchased':
						$td19 = $td19."<option value='RNR'>RNR</option><option value='Purchased'>Purchased</option><option value='Yet to Purchased' selected >Yet to Purchased</option>";
						break;
			default:
						$td19 = $td19."<option value='' selected>Select Status</option><option value='RNR'>RNR</option><option value='Purchased'>Purchased</option><option value='Yet to Purchased' >Yet to Purchased</option>";
						break;
		}
		$td19 = $td19."</select></div></div>";
		$td19 = $td19."<script>";
		$td19 = $td19.'var reason = "'.$fdbck_status.'";';
		$td19 = $td19."switch(reason)
						{
							case 'RNR' : $('#followupDate".$booking_id."').hide();
										 $('#completed-div".$booking_id."').hide();
										 $('#comments".$booking_id."').hide();
										 $('#reason".$booking_id."').hide();
										break;";
		
		$td19 = $td19."case 'Yet to Purchased' : $('#followupDate".$booking_id."').hide();
											   $('#completed-div".$booking_id."').show();
											   $('#comments".$booking_id."').show();
											   $('#reason".$booking_id."').hide();
											   break;";
		$td19 = $td19."case 'Purchased' : $('#followupDate".$booking_id."').hide();
												   $('#completed-div".$booking_id."').hide();
												   $('#comments".$booking_id."').show();
												   $('#reason".$booking_id."').show();
												   break;}";
		$td19 = $td19."$('#reason-select".$booking_id."').change(function(){var reason2 = $('#reason-select".$booking_id."').val();";
		$td19 = $td19."if(reason2 == 'Rescheduled the service'){
							$('#followupDate".$booking_id."').show();
							$('#followup_date_txt".$booking_id."').text('Rescheduled Date');
						}else{
							$('#followupDate".$booking_id."').hide();
							$('#followup_date_txt".$booking_id."').text('FollowUp Date');
						}});";
		$td19 = $td19.'$("#status'.$booking_id.'").change(function(){';
		$td19 = $td19.'var reason = $("#status'.$booking_id.'").val();';
		$td19 = $td19."switch(reason){";
		$td19 = $td19."case 'RNR' : $('#followupDate".$booking_id."').hide();
										 $('#completed-div".$booking_id."').hide();
										 $('#comments".$booking_id."').hide();
										 $('#reason".$booking_id."').hide();
										break;";
		
		$td19 = $td19."case 'Purchased' : $('#followupDate".$booking_id."').hide();
											   $('#completed-div".$booking_id."').show();
											   $('#comments".$booking_id."').show();
											   $('#reason".$booking_id."').hide();
											   break;";
		$td19 = $td19."case 'Yet to Purchased' : $('#followupDate".$booking_id."').hide();
												   $('#completed-div".$booking_id."').hide();
												   $('#comments".$booking_id."').show();
												   $('#reason".$booking_id."').show();
												   break;";
		$td19 = $td19."default : $('#followupDate".$booking_id."').hide();
												   $('#completed-div".$booking_id."').hide();
												   $('#comments".$booking_id."').hide();
												   $('#reason".$booking_id."').hide();break;}";
		$td19 = $td19."$('#reason-select".$booking_id."').change(function(){var reason2 = $('#reason-select".$booking_id."').val();";
	
		$td19 = $td19.'});';
		$td19 = $td19."</script>";
		$td19 = $td19."<div class='row'></div><div class='row' id='reason".$booking_id."' style='display:none;'>";
		$td19 = $td19."<div class='col-lg-2 col-xs-1 col-lg-offset-2 form-group'>";
		$td19 = $td19."<label style='padding-top:5px;'>Reason</label></div>";
		$td19 = $td19."<div class='col-lg-6 col-xs-6' style='font-size:8px;'>";
		$td19 = $td19."<select class='form-control' name='reason' id='reason-select".$booking_id."' >";
		$td19 = $td19."<option value='' selected>-Choose a reason-</option>
						<option value='Purchased with local shop'>Purchased with local shop</option>
						<option value='Improper communication from the garage' >Improper communication from the garage</option>
						<option value='Not in Chennai' >Not in Chennai</option>
						<option value='Pricing was too high' >Pricing was too high</option>
						<option value='Quality was not good' >Quality center was not good</option>
						<option value='Others' >Others</option>";
		$td19 = $td19."</select></div></div>";
		$td19 = $td19."<div class='row'><div class='row' id='followupDate".$booking_id."' style='display:none;'><div class='col-xs-4 col-lg-4 col-lg-offset-2 form-group'>";
		$td19 = $td19."<label style='padding-top:5px;' id='followup_date_txt".$booking_id."'>FollowUp Date</label>";
		$td19 = $td19."</div><div class='col-xs-4'><div class='form-group' style='margin-right:10px;'>";
		$td19 = $td19."<input type='text' name='followup_date' class='form-control datepicker' data-date-format='dd-mm-yyyy' type='text' />";
		$td19 = $td19."</div></div></div></div>";
		$td19 = $td19."<div id='completed-div".$booking_id."' style='display:none;'><div class='row'><div class='col-lg-2 col-xs-1 col-lg-offset-2 form-group'>";
		$td19 = $td19."<label style='padding-top:5px;'>Rating</label>";
		$td19 = $td19."</div><div class='col-lg-6 col-xs-6' style='font-size:8px;'>";
		$td19 = $td19."<input id='rating".$booking_id."' name='rating' type='text' class='rating' data-min='0' data-max='5' data-step='0.5' data-stars=5 data-symbol='&#xe005;' data-default-caption='{rating} hearts' data-star-captions='{}' title='' data-show-clear='false' data-show-caption='false' value='".$rtt_rating."'>";
		$td19 = $td19."</div></div><div class='row'><div class='col-lg-2 col-xs-1 col-lg-offset-2 form-group'>";
		$td19 = $td19."<label style='padding-top:5px;'>&nbsp;Amount</label>";
		$td19 = $td19."</div><div class='col-xs-6'><div class='form-group'>";
		$td19 = $td19."<input type='number' name='final_bill_amount' id='final_bill_amount".$booking_id."' class='form-control' style='max-width:280px;' value='".$final_paid_bill."'/>";
		$td19 = $td19."</div></div></div></div>";
		$td19 = $td19."<div class='row' id='comments".$booking_id."' style='display:none;'><div class='col-xs-8 col-lg-offset-2'>";
		$td19 = $td19."<textarea name='comments' id='comments' class='form-control' rows='4' cols='40' placeholder='Comments...' style='width:270px;'>".$comments."</textarea>";
		$td19 = $td19."</div></div><div class='row'><br>";
		$td19 = $td19."<div class='form-group' align='center'><input class='form-control' id='submit".$booking_id."' type='submit' value='Apply' style='background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;width:90px;'/></div></div>";
		
		$td19 = $td19."<script>";
		$td19 = $td19."var reason2 = $('#reason-select".$booking_id."').val();console.log(reason2);";
		
		$td19 = $td19.'$(function () {$("#rating'.$booking_id.'").rating();});';
		$td19 = $td19."$(document).on('submit','#feedback".$booking_id."',function(e){";
		$td19 = $td19."if($( '#submit".$booking_id."' ).is(':visible')){";
		$td19 = $td19."e.preventDefault();$('#submit".$booking_id."').hide();";
		$td19 = $td19.'setTimeout(function () {$("#feedback'.$booking_id.'").trigger("submit"); }, 200);';
		$td19 = $td19."}});";
		$td19 = $td19."var date = new Date();date.setDate(date.getDate());";
		$td19 = $td19."$('.datepicker').datepicker({autoclose: true,startDate: date});$('input.datepicker').datepicker('setDate', 'today');";
		$td19 = $td19."</script>";		
		$td19 = $td19."<input type='hidden' name='feedback_status' value='".$feedback_status."' >";
		$td19 = $td19."<input type='hidden' name='rtt_updates' value='".$rtt_updates."' >";
		$td19 = $td19."<input type='hidden' name='s_date' value='".date('Y-m-d',strtotime($startdate))."' >";
		$td19 = $td19."<input type='hidden' name='e_date' value='".date('Y-m-d',strtotime($enddate))."' >";
		$td19 = $td19."<input type='hidden' name='b2b_book_id' value='".$b2b_booking_id."' >";
		$td19 = $td19."<input type='hidden' name='user_id' value='".$user_id."' >";
		$td19 = $td19."<input type='hidden' name='book_id' value='".$booking_id."' >";
		$td19 = $td19."<input type='hidden' name='premium' value='".$premium."' >";
		$td19 = $td19."<input type='hidden' name='g_date' value='".date('Y-m-d',strtotime($go_axle_date))."' >";
		$td19 = $td19."</form></div></div></div></div></div></div>";
	}
	else
	{
		$td19 = $td19."Purchased";
	}
	$td19 = $td19."</td>";
	$tr1_l = "</tr>";
	$str = $tr1.$td1.$td2.$td4.$td5.$td6.$td7.$td8.$td9.$td20.$td10.$td11.$td12.$td13.$td14.$td16.$td17.$td18.$td19.$tr1_l;
	$data2[] = array('tr'=>$str);
}
	$result['data2'] = $data2;

} // if
else {
	$result['data2'] = "no";
}
echo json_encode($result);
}
 ?>

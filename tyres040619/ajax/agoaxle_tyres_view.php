<?php
//error_reporting(E_ALL);
include("../config.php");
@ini_set('max_execution_time',-1);
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$super_flag = $_SESSION['super_flag'];


$src_crm = array('crm016','crm017','crm018','crm064','crm036','crm033');
 $src_column = 1;
if(in_array($crm_log_id,$src_crm))
{
  $src_column = 1;
}

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$source = $_GET['source'];
$city = $_GET['city'];
$status = $_GET['status'];
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;

$cond ='';

$cond = $cond.($source == 'all' ? "" : "AND tb.source='$source'");
$cond = $cond.($city == 'all' ? "" : "AND tb.city='$city'");
// $cond = $cond.($cluster == 'all' ? "" : ($vehicle != 'all' ? "AND ".$col_name."='$cluster'" : "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'"));


$sql_booking = "SELECT DISTINCT tb.booking_id,tb.shop_name,tb.mec_id_leads,tb.log,tb.tyres_req_time,tb.tyre_brand,tb.booking_status,tb.flag,tb.axle_flag,tb.user_pmt_flg,tb.flag_fo,tb.priority,tb.crm_update_time,tb.utm_source,tb.tyre_count,u.name,u.mobile_number,bb.b2b_booking_id,bb.b2b_bid_flg,c.name as crm_name,bb.b2b_acpt_flg,sum(bb.b2b_bid_flg) as bid_sum,sum(bb.b2b_acpt_flg) as bid_acpt,count(bb.b2b_bid_flg) as bid_count,v.brand,v.model,ts.tyre_size FROM tyre_booking_tb as tb LEFT JOIN crm_admin as c ON tb.crm_update_id=c.crm_log_id LEFT JOIN b2b.b2b_booking_tbl_tyres as bb ON tb.booking_id=bb.gb_booking_id LEFT JOIN user_register as u ON u.reg_id=tb.user_id LEFT JOIN user_vehicle_table as v ON  tb.user_veh_id=v.id LEFT JOIN localities as l ON tb.locality = l.localities LEFT JOIN b2b.b2b_status as s ON bb.b2b_booking_id=s.b2b_booking_id LEFT JOIN admin_tyre_sizes as ts ON ts.brand=v.brand AND ts.model=v.model WHERE tb.booking_id!='' {$cond} AND tb.booking_status='2' AND tb.axle_flag='1' AND tb.flag='0' AND DATE(tb.crm_update_time) BETWEEN '$startdate' and '$enddate' group by tb.booking_id ORDER BY tb.crm_update_time DESC";
//echo $sql_booking;
$res_booking = mysqli_query($conn,$sql_booking);
$arr = array();
$count = mysqli_num_rows($res_booking);
if($count >0){
  while($row_booking = mysqli_fetch_object($res_booking)){
    // print_r($row_booking);
    $booking_id = $row_booking->booking_id;
    $b2b_booking_id = $row_booking->b2b_booking_id;
    $shop_name = $row_booking->shop_name;
    $mec_id_leads = $row_booking->mec_id_leads;
    $log = $row_booking->crm_update_time;
    $axle_flag = $row_booking->axle_flag;
    $tyres_req_time = $row_booking->tyres_req_time;
    $tyre_brand = $row_booking->tyre_brand;
    $tyre_count = $row_booking->tyre_count;
    $user_pmt_flg = $row_booking->user_pmt_flg;
    $flag_fo = $row_booking->flag_fo;
    $priority = $row_booking->priority;
    $utm_source = $row_booking->utm_source;
    $alloted_to_id = $row_booking->crm_update_id;
    $name = $row_booking->name;
    $mobile_number = $row_booking->mobile_number;
    $b2b_bid_flg = $row_booking->b2b_bid_flg;
    $b2b_acpt_flg = $row_booking->b2b_acpt_flg;
    $bid_sum = $row_booking->bid_sum;
    $alloted_to = $row_booking->crm_name;
    $bid_count = $row_booking->bid_count;
    $brand = $row_booking->brand;
    $model = $row_booking->model;
    $bid_acpt = $row_booking->bid_acpt;
    $tyresize = $row_booking->tyre_size;
  
  $arr[] = array ("booking_id" => $booking_id,"b2b_booking_id" => $b2b_booking_id,"shop_name"=> $shop_name, "mec_id_leads" => $mec_id_leads,
    "log" => $log,"axle_flag" => $axle_flag,"user_pmt_flg" => $user_pmt_flg,"flag_fo"=>$flag_fo,"priority"=>$priority,"utm_source" => $utm_source,"tyres_req_time" => $tyres_req_time,"name" => $name, "mobile_number" => $mobile_number,"tyre_brand" => $tyre_brand,"brand" => $brand,"model" => $model,"tyre_count" => $tyre_count,"b2b_bid_flg" => $b2b_bid_flg, "b2b_acpt_flg"=>$b2b_acpt_flg,"bid_count"=>$bid_count,"bid_sum"=>$bid_sum,"bid_acpt"=>$bid_acpt,"tyresize" => $tyresize //,"acpt"=>$acpt,"deny"=>$deny
    );
}

    function sort_crm_time($a, $b) {
        if ($a['log'] == $b['log']) {
          return 0;
        }
        return ($a['log'] > $b['log']) ? -1 : 1;
      }
      usort($arr, "sort_crm_time");
      $no =0;
     
      foreach($arr as $a)
      {
         $booking_id = $a['booking_id'];
         $b2b_booking_id = $a['b2b_booking_id'];
         $shop_name = $a['shop_name'];
          $mec_id_leads = $a['mec_id_leads'];
          $log = $a['log'];
          $axle_flag = $a['axle_flag'];
          $tyres_req_time = $a['tyres_req_time'];
          $tyre_count = $a['tyre_count'];
          $tyre_brand = $a['tyre_brand'];
          $brand = $a['brand'];
          $model = $a['model'];
          $user_pmt_flg = $a['user_pmt_flg'];
          $flag_fo = $a['flag_fo'];
          $priority = $a['priority'];
          $utm_source = $a['utm_source'];
          $mobile_number = $a['mobile_number'];
          $b2b_bid_flg = $a['b2b_bid_flg'];
          $b2b_acpt_flg = $a['b2b_acpt_flg'];
           $name = $a['name'];
           $bid_sum = $a['bid_sum'];
           $bid_count = $a['bid_count'];
           $bid_acpt= $a['bid_acpt'];
           $tyresize = $a['tyresize'];

      if($status == 'senttoaxle')
{
  if(($b2b_acpt_flg == 1 && $b2b_bid_flg == 1) || ($b2b_acpt_flg == 0 && $b2b_bid_flg == 1))
  {
    continue;
  }
}
elseif($status == 'acceptedlead')
{
  if(($b2b_bid_flg == 1 && $b2b_acpt_flg == 0) || ($b2b_bid_flg == 0 && $b2b_acpt_flg == 0))
  {
    continue;
  }
}
elseif($status == 'rejectedlead')
{
  if(($b2b_acpt_flg == 1 && $b2b_bid_flg == 1) || ($b2b_acpt_flg == 0 && $b2b_bid_flg == 0))
  {
    continue;
  }
}
elseif($status == 'rejectedleadall')
{
  if(($b2b_acpt_flg == 0 && $b2b_bid_flg == 1) || ($b2b_acpt_flg == 0 && $b2b_bid_flg == 0))
  {
    continue;
  }
}


           $tr = '<tr>';
          
          $td2 = '<td><p style="float:left;padding:10px;">'.$booking_id.'</p>';
          if($flag_fo == '1'){
  $td2 = $td2.'<p class="p" style="font-size:16px;color:#D81B60;float:left;padding:5px;" title="Resheduled Booking!"><i class="fa fa-recycle" aria-hidden="true"></i></p>';
  switch($priority){
    case '1': $td2 = $td2.'<p style="font-size:16px;color:#00ACC1;float:left;padding:5px;" title="Low Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';break;
    case '2': $td2 = $td2.'<p style="font-size:16px;color:#FFB300;float:left;padding:5px;" title="Medium Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';break;
    case '3':$td2 = $td2.'<p style="font-size:16px;color:#E53935;float:left;padding:5px;" title="High Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';break;
  }
}
			if($user_pmt_flg == 1)
			{
				$td2 = $td2.'<p style="font-size:16px;color:#1c57af;float:left;padding:5px;" title="Paid!"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></p>';
			}
      $td2 = $td2.'</td>';

          $td3 = '<td><div class="row">';
              if($b2b_booking_id == ''){
                 $td3 = $td3.'<a href="tyres_goaxle.php?bi='.base64_encode($booking_id).'" style="background-color:#ffa800;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
               }
               else
              {
                if($b2b_acpt_flg==1 && $b2b_bid_flg==1){
                       $td3 = $td3.'<a style="background-color:#6FA3DE;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>'; 
                      }

                else if($b2b_bid_flg==1 && $b2b_acpt_flg==0 ){ 
                      $td3 = $td3.'<a style="background-color:#D25F34;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
                  
                 // else if($bid_count == $bid_sum){
                 //       $td3 = $td3.'<a style="background-color:#8C7272;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>'; 
                 //      }
                  
                  }
                  else
                  {
                   $td3 = $td3.'<a style="background-color:#69ED85;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
                  }
                 }
                
          $td4 = '<td><a href="user_details_tyres.php?bi='.base64_encode($booking_id).'&t='. base64_encode("g").'"><i id="'.$booking_id.'" class="fa fa-eye" aria-hidden="true"></i></td>';
      
      
          $td5 = '<td>'.$name.'</td>';
          $td6 = '<td>'.$mobile_number.'</td>';
          $td7 = '<td>'.$brand.' '.$model.'</td>';
          $td15 = '<td>'.$tyresize.'</td>';
          $td8 = '<td>'.$tyre_brand.'</td>';
          $td9 = '<td>'.$tyre_count.'</td>';
          
           if($tyres_req_time=='immediately')
          {
            $req_time=strtoupper($tyres_req_time);
          $td10 = '<td><span class="label label-success">'.$req_time.'</span></td>';
          }
          else if($tyres_req_time=='in1month')
          {
            $req_time='In 1 Month';
            $td10 = '<td><span class="label label-danger">'.$req_time.'</span></td>';
          }
          else if($tyres_req_time=='in3months')
          {
            $req_time='In 3 Months';
            $td10 = '<td><span class="label label-danger">'.$req_time.'</span></td>';
          }
          else{
            $req_time='Not Given';
            $td10 = '<td><span class="label label-danger">'.$req_time.'</span></td>';
          }
        
          if($shop_name!='')
          {
        
           $td11 = '<td>'.$shop_name.'</td>';

          }
        
        
            else
              {
               $td11 = '<td>'.'Not yet assigned'.'</td>';
               }
         
          $td12 = '<td>'.$alloted_to.'</td>';
          // print_r($utm_source);
          $td13 = '<td>'.date('d M Y h.i A', strtotime($log)).'</td>';
      
          $tr_l = '</tr>';
      
          $str = $tr.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td15.$td8.$td9.$td10.$td11.$td12.$td13.$tr_l;
          // print_r($str);
          $data[] = array('tr'=>$str);
            
        }
// print_r($data);
       
    echo $data1 = json_encode($data);
} // if

else {
  echo "no";
}


?>

<?php
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();

//error_reporting(E_ALL); ini_set('display_errors', 1); 

$tab = $_GET['tab'];

//0-all && 1-particular
class goaxles {
    function cr0_vg0_sup0_vr0_vd0(){
        return "Accepted";
    }
    function cr0_vg0_sup0_vr0_vd1(){
        return "Delivered";
    }
    function cr0_vg0_sup0_vr1_vd0(){
        return "Ready";
    }
    function cr0_vg0_sup0_vr1_vd1(){
        return "Delivered";
    }
    function cr0_vg0_sup1_vr0_vd0(){
        return "In Progress";
    }
    function cr0_vg0_sup1_vr0_vd1(){
        return "Delivered";
    }
    function cr0_vg0_sup1_vr1_vd0(){
        return "Ready";
    }
    function cr0_vg0_sup1_vr1_vd1(){
        return "Delivered";
    }
    function cr0_vg1_sup0_vr0_vd0(){
        return "In Garage";
    }
    function cr0_vg1_sup0_vr0_vd1(){
        return "Delivered";
    }
    function cr0_vg1_sup0_vr1_vd0(){
        return "Ready";
    }
    function cr0_vg1_sup0_vr1_vd1(){
        return "Delivered";
    }
    function cr0_vg1_sup1_vr0_vd0(){
        return "In Progress";
    }
    function cr0_vg1_sup1_vr0_vd1(){
        return "Delivered";
    }
    function cr0_vg1_sup1_vr1_vd0(){
        return "Ready";
    }
    function cr0_vg1_sup1_vr1_vd1(){
        return "Delivered";
    }
    function cr1_vg0_sup0_vr0_vd0(){
        return "Checked In";
    }
    function cr1_vg0_sup0_vr0_vd1(){
        return "Delivered";
    }
    function cr1_vg0_sup0_vr1_vd0(){
        return "Ready";
    }
    function cr1_vg0_sup0_vr1_vd1(){
        return "Delivered";
    }
    function cr1_vg0_sup1_vr0_vd0(){
        return "In Progress";
    }
    function cr1_vg0_sup1_vr0_vd1(){
        return "Delivered";
    }
    function cr1_vg0_sup1_vr1_vd0(){
        return "Ready";
    }
    function cr1_vg0_sup1_vr1_vd1(){
        return "Delivered";
    }
    function cr1_vg1_sup0_vr0_vd0(){
        return "In Garage";
    }
    function cr1_vg1_sup0_vr0_vd1(){
        return "Delivered";
    }
    function cr1_vg1_sup0_vr1_vd0(){
        return "Ready";
    }
    function cr1_vg1_sup0_vr1_vd1(){
        return "Delivered";
    }
    function cr1_vg1_sup1_vr0_vd0(){
        return "In Progress";
    }
    function cr1_vg1_sup1_vr0_vd1(){
        return "Delivered";
    }
    function cr1_vg1_sup1_vr1_vd0(){
        return "Ready";
    }
    function cr1_vg1_sup1_vr1_vd1(){
        return "Delivered";
    }
}
$goaxles_obj = new goaxles(); 


switch($tab){
    case 'person': 
       // print_r($_POST); 
        $crmlogid = $_POST['crmlogid'];
        $startdate = $_POST['startdate'];
        $enddate = $_POST['enddate'];
        $status = $_POST['status'];
        $city = $_POST['city'];
        // $cluster = $city != 'Chennai' ? 'all' : $_POST['cluster'];
		$cluster='all';

        switch($status){
            case 'lead': $cond = " AND b.flag_unwntd!='1'"; break;
            case 'booking': $cond = " AND b.booking_status = '2' AND b.flag!='1' AND b.flag_unwntd!='1'"; $redir = base64_encode("b") ; break;
            case 'followup': $cond = " AND (b.booking_status IN(3,4,5,6) OR (b.booking_status='1' AND b.flag_fo='1')) AND b.flag!='1' AND b.flag_unwntd!='1'"; $redir = base64_encode("f") ;break;
            case 'cancelled': $cond = " AND b.flag = '1' AND b.flag_unwntd!='1'"; $redir = base64_encode("c") ; break;
            case 'others': $cond = " AND b.booking_status = '0' AND b.flag!='1' AND b.flag_unwntd!='1'"; $redir = base64_encode("o") ; break;
            case 'idle': $cond = " AND b.booking_status = '1' AND b.flag_unwntd!='1' AND b.flag_fo!='1' AND b.flag!='1'"; $redir = base64_encode("l") ; break;
            case 'duplicate': $cond = " AND b.activity_status = '26' AND b.flag_unwntd='1' AND b.flag='1'"; $redir = base64_encode("c") ; break;
            default : $cond = " AND b.flag_unwntd!='1'"; $redir = base64_encode("l") ; break; 
        }

        $cond2 = $crmlogid == 'all' ? "" :" AND crm_update_id='$crmlogid' ";

        $cond2 = $cond2.($city == 'all' ? "" : "AND b.city='$city'");
        $cond2 = $cond2.($cluster == 'all' ? "" : "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'");


        $sql_person = $city == "Chennai" ? "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM tyre_booking_tb as b LEFT JOIN localities as l ON b.locality=l.localities WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226) AND b.log BETWEEN '$startdate' AND '$enddate' {$cond}{$cond2} ORDER BY b.log DESC" : "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM tyre_booking_tb as b WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.log BETWEEN '$startdate' AND '$enddate' {$cond}{$cond2} ORDER BY b.log DESC";
		
		//echo $sql_person;
        $res_person = mysqli_query($conn,$sql_person);

        if(mysqli_num_rows($res_person) >= 1 ){
            ?>
            <table class="table table-striped table-bordered tablesorter table-hover">
            <thead style="background-color: #D3D3D3;">
            <!--<th>No</th>-->
            <th>BookingId</th>
            <th>CustomerName</th>
            <th>ServiceCenter</th>
            <th>PickUp</th>
            <?php if($status == 'booking'){ ?> <th>GoAxleStatus</th> <?php } ?>
            </thead>
            <tbody>
            <?php
            while($row_person = mysqli_fetch_object($res_person)){
				//print_r($row_person);
                $booking_id = $row_person->booking_id;
                $user_id = $row_person->user_id;
                $veh_id = $row_person->user_veh_id;
                $res_user = mysqli_query($conn,"SELECT name FROM user_register WHERE reg_id='$user_id'");
                $row_user = mysqli_fetch_object($res_user);
                $user_name = $row_user->name;
                
                if($status == 'lead'){
                    $st = $row_person->booking_status;
                    switch($st){
                        case '1': $redir = base64_encode("l") ; break;
                        case '2': $redir = base64_encode("b") ; break;
                        case '3': 
                        case '4':
                        case '5':
                        case '6': $redir = base64_encode("f") ; break;
                        case '0': $redir = base64_encode("o") ; break;
                        default: $redir = base64_encode("l") ;
                    }
                }
                
                
                if($status == 'booking'){
                 //   error_reporting(E_ALL); ini_set('display_errors', 1);

                    $sql_b2b = "SELECT DISTINCT b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_service_under_progress,b.b2b_vehicle_ready,b.b2b_vehicle_delivered,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl_tyres as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.gb_booking_id='$booking_id' ";
                    //echo $sql_b2b;
                    $res_b2b = mysqli_query($conn2,$sql_b2b) or die(mysqli_error($res_b2b));
                    $row_b2b = mysqli_fetch_object($res_b2b);
                    $acpt = $row_b2b->b2b_acpt_flag;
                    $deny = $row_b2b->b2b_deny_flag;

                    if(mysqli_num_rows($res_b2b) >=1 ){
                        if($deny == '1' && $acpt == '0'){
                            $goaxle = "Rejected";
                        }
                        else if($deny == '0' && $acpt == '0'){
                            $goaxle = "No Action";
                        }
                        else{
                            $check_in_report = $row_b2b->b2b_check_in_report;
                            $vehicle_at_garage = $row_b2b->b2b_vehicle_at_garage;
                            $service_under_progress = $row_b2b->b2b_service_under_progress;
                            $vehicle_ready = $row_b2b->b2b_vehicle_ready;
                            $vehicle_delivered = $row_b2b->b2b_vehicle_delivered;
                            
                            $check_in_report_val = $check_in_report == '0' ? "0" : "1";
                            $vehicle_at_garage_val = $vehicle_at_garage == '0' ? "0" : "1";
                            $service_under_progress_val = $service_under_progress == '0' ? "0" : "1";
                            $vehicle_ready_val = $vehicle_ready=='0' ? "0" : "1";
                            $vehicle_delivered_val = $vehicle_delivered == '0' ? "0" : "1";
                    
                            $goaxle = $goaxles_obj->{"cr{$check_in_report_val}_vg{$vehicle_at_garage_val}_sup{$service_under_progress_val}_vr{$vehicle_ready_val}_vd{$vehicle_delivered_val}"}();
                        }
                    }
                    else{
                        $goaxle = "Record not found!";
                    }
                }
                ?>
                <tr>
                <td><a target="_blank" href="user_details_tyres.php?t=<?php echo $redir; ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>"><?php echo $booking_id; ?></a></td>
                <td><?php echo $user_name; ?></td>
                <td><?php echo $row_person->shop_name; ?></td>
                <td><?php $p = $row_person->pick_up; echo $pc = $p== '1' ? 'Yes': 'No'; ?></td>
            <?php if($status == 'booking'){ ?> <td> <?php echo $goaxle; ?></td> <?php } ?>
                </tr>
                <?php
            }
            ?>
            </tbody>
            </table>
            <?php
        }
        else{
            echo "<h4> No Records Found!</h4>";
        }
        break;
        case 'source': 
        $vehicle = $_POST['vehicle'];
        $source = $_POST['source'];
        $startdate = $_POST['startdate'];
        $enddate = $_POST['enddate'];
        $status = $_POST['status'];
        $city = $_POST['city'];
        // $cluster = $city != 'Chennai' ? 'all' : $_POST['cluster'];
		$cluster='all';
        $cond ='';

        switch($vehicle){
            case '2w': $lc = "AND l.bike_cluster='$cluster'"; break;
            case '4w' :  $lc = "AND l.car_cluster='$cluster'" ; break;
            default :  $lc = "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'" ;
        }

        $cond = $cond.($vehicle == 'all' ? "" : "AND b.vehicle_type='$vehicle'");
        $cond = $cond.($city == 'all' ? "" : "AND b.city='$city'");
        $cond = $cond.($source == 'all' ? "" : "AND b.source='$source'");
        $cond = $cond.($cluster == 'all' ? "" : $lc );

        switch($status){
            case 'lead': $cond2 = " AND b.flag_unwntd!='1'"; break;
            case 'booking': $cond2 = " AND b.booking_status = '2' AND b.flag!='1' AND b.flag_unwntd!='1'"; $redir = base64_encode("b"); break;
            case 'followup': $cond2 = " AND (b.booking_status IN(3,4,5,6) OR (b.booking_status='1' AND b.flag_fo='1')) AND b.flag!='1' AND b.flag_unwntd!='1'"; $redir = base64_encode("f"); break;
            case 'cancelled': $cond2 = " AND b.flag = '1' AND b.flag_unwntd!='1'"; $redir = base64_encode("c"); break;
            case 'others': $cond2 = " AND b.booking_status = '0' AND b.flag!='1' AND b.flag_unwntd!='1'"; $redir = base64_encode("o"); break;
            case 'idle': $cond2 = " AND b.booking_status = '1' AND b.flag_unwntd!='1' AND b.flag_fo!='1' AND b.flag!='1'"; $redir = base64_encode("l"); break;
            case 'duplicate': $cond2 = " AND b.activity_status = '26' AND b.flag_unwntd='1' AND b.flag='1'"; $redir = base64_encode("c"); break;                        
            default : $cond2 = " AND b.flag_unwntd!='1'"; $redir = base64_encode("l"); break; 
        }

        $sql_source = $city == 'Chennai' ? "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM tyre_booking_tb as b LEFT JOIN localities as l ON b.locality=l.localities WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226) AND b.log BETWEEN '$startdate' AND '$enddate' {$cond} {$cond2} " : "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM tyre_booking_tb as b WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.log BETWEEN '$startdate' AND '$enddate' {$cond} {$cond2} " ;
        $res_source = mysqli_query($conn, $sql_source);

        if(mysqli_num_rows($res_source) >= 1){
            ?>
            <table class="table table-striped table-bordered tablesorter table-hover">
            <thead style="background-color: #D3D3D3;">
            <!--<th>No</th>-->
            <th>BookingId</th>
            <th>CustomerName</th>
            <th>ServiceCenter</th>
            <th>PickUp</th>
            <?php if($status == 'booking'){ ?> <th>GoAxleStatus</th> <?php } ?>
            </thead>
            <tbody>
            <?php
            while($row_source = mysqli_fetch_object($res_source)){
                $booking_id = $row_source->booking_id;
                $user_id = $row_source->user_id;
                $veh_id = $row_source->user_veh_id;
                $res_user = mysqli_query($conn,"SELECT name FROM user_register WHERE reg_id='$user_id'");
                $row_user = mysqli_fetch_object($res_user);
                $user_name = $row_user->name;
                
                if($status == 'lead'){
                    $st = $row_source->booking_status;
                    switch($st){
                        case '1': $redir = base64_encode("l") ; break;
                        case '2': $redir = base64_encode("b") ; break;
                        case '3': 
                        case '4':
                        case '5':
                        case '6': $redir = base64_encode("f") ; break;
                        case '0': $redir = base64_encode("o") ; break;
                        default: $redir = base64_encode("l") ;
                    }
                }
                if($status == 'booking'){
                    //   error_reporting(E_ALL); ini_set('display_errors', 1);
   
                       $sql_b2b = "SELECT DISTINCT b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_service_under_progress,b.b2b_vehicle_ready,b.b2b_vehicle_delivered,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl_tyres as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.gb_booking_id='$booking_id'";
                       $res_b2b = mysqli_query($conn2,$sql_b2b) or die(mysqli_error($res_b2b));
                       $row_b2b = mysqli_fetch_object($res_b2b);
                       $acpt = $row_b2b->b2b_acpt_flag;
                       $deny = $row_b2b->b2b_deny_flag;
   
                       if(mysqli_num_rows($res_b2b) >=1 ){
                           if($deny == '1' && $acpt == '0'){
                               $goaxle = "Rejected";
                           }
                           else if($deny == '0' && $acpt == '0'){
                               $goaxle = "No Action";
                           }
                           else{
                               $check_in_report = $row_b2b->b2b_check_in_report;
                               $vehicle_at_garage = $row_b2b->b2b_vehicle_at_garage;
                               $service_under_progress = $row_b2b->b2b_service_under_progress;
                               $vehicle_ready = $row_b2b->b2b_vehicle_ready;
                               $vehicle_delivered = $row_b2b->b2b_vehicle_delivered;

                             
   
                               $check_in_report_val = $check_in_report == '0' ? "0" : "1";
                               $vehicle_at_garage_val = $vehicle_at_garage == '0' ? "0" : "1";
                               $service_under_progress_val = $service_under_progress == '0' ? "0" : "1";
                               $vehicle_ready_val = $vehicle_ready=='0' ? "0" : "1";
                               $vehicle_delivered_val = $vehicle_delivered == '0' ? "0" : "1";
                       
                               $goaxle = $goaxles_obj->{"cr{$check_in_report_val}_vg{$vehicle_at_garage_val}_sup{$service_under_progress_val}_vr{$vehicle_ready_val}_vd{$vehicle_delivered_val}"}();
                           }
                       }
                       else{
                           $goaxle = "Record not found!";
                       }
                   }
               
                ?>
                <tr>
                <td><a target="_blank" href="user_details_tyres.php?t=<?php echo $redir; ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>"><?php echo $booking_id; ?></a></td>
                <td><?php echo $user_name; ?></td>
                <td><?php echo $row_source->shop_name; ?></td>
                <td><?php $p = $row_source->pick_up; echo $pc = $p== '1' ? 'Yes': 'No'; ?></td>
                <?php if($status == 'booking'){ ?> <td><?php echo $goaxle; ?></td> <?php } ?>
                </tr>
                <?php
            }
            ?>
            </tbody>
            </table>
            <?php
        }
        else{
            echo "<h4> No Records Found!</h4>";
        }  
        break;
    case 'nonconv': 
        $activity = $_POST['activity'];
        $vehicle = $_POST['vehicle'];
        $startdate = $_POST['startdate'];
        $enddate = $_POST['enddate'];
        $city = $_POST['city'];
        // $cluster = $city != 'Chennai' ? 'all' : $_POST['cluster'];
		$cluster='all';

        $cond ='';

        switch($vehicle){
            case '2w': $lc = "AND l.bike_cluster='$cluster'"; break;
            case '4w' :  $lc = "AND l.car_cluster='$cluster'" ; break;
            default :  $lc = "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'" ;
        }

        $cond = $cond.($vehicle == 'all' ? "" : "AND b.vehicle_type='$vehicle'");
        $cond = $cond.($city == 'all' ? "" : "AND b.city='$city'");
        $cond = $cond.($cluster == 'all' ? "" : $lc );

        $sql_nonconv = $city == "Chennai" ? "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.flag,b.booking_status,b.user_veh_id FROM tyres_comments_tbl as c INNER JOIN tyre_booking_tb as b ON c.book_id=b.booking_id LEFT JOIN localities as l ON b.locality=l.localities WHERE c.activity_status='$activity' AND c.log BETWEEN '$startdate' AND '$enddate' {$cond}" : "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.flag,b.booking_status,b.user_veh_id FROM tyres_comments_tbl as c INNER JOIN tyre_booking_tb as b ON c.book_id=b.booking_id WHERE c.activity_status='$activity' AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226) AND c.log BETWEEN '$startdate' AND '$enddate' {$cond}" ;
        $res_nonconv = mysqli_query($conn,$sql_nonconv);

        if(mysqli_num_rows($res_nonconv) >= 1 ){
            ?>
            <table class="table table-striped table-bordered tablesorter table-hover">
            <thead style="background-color: #D3D3D3;">
            <!--<th>No</th>-->
            <th>BookingId</th>
            <th>CustomerName</th>
            <th>ServiceCenter</th>
            <th>PickUp</th>
            </thead>
            <tbody>
            <?php
            while($row_nonconv = mysqli_fetch_object($res_nonconv)){
                $booking_id = $row_nonconv->booking_id;
                $user_id = $row_nonconv->user_id;
                $veh_id = $row_nonconv->user_veh_id;
                $flag = $row_nonconv->flag;
                $st = $row_nonconv->booking_status;

                $res_user = mysqli_query($conn,"SELECT name FROM user_register WHERE reg_id='$user_id'");
                $row_user = mysqli_fetch_object($res_user);
                $user_name = $row_user->name;
                
                
                if($flag == '1'){
                    $redir = base64_encode("c") ; 
                }
                else if($st == '0'){
                    $redir = base64_encode("o") ; 
                }
                else{
                    $redir = base64_encode("l") ; 
                }

                
              ?>
                <tr>
                <td><a target="_blank" href="user_details_tyres.php?t=<?php echo $redir; ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>"><?php echo $booking_id; ?></a></td>
                <td><?php echo $user_name; ?></td>
                <td><?php echo $row_nonconv->shop_name; ?></td>
                <td><?php $p = $row_nonconv->pick_up; echo $pc = $p== '1' ? 'Yes': 'No'; ?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
            </table>
            <?php
        }
        else{
            echo "<h4> No Records Found!</h4>";
        }

       // print_r($_POST); 
        break;
    default: echo "<h4>Sorry! No records to Display!!!</h4>"; 
}


?>
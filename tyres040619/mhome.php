<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
//if((empty($_SESSION['crm_log_id'])) || $flag!="1") {
if((empty($_SESSION['crm_log_id'])) || $cluster_admin!='1') {
	header('location:logout.php');
	die();
}
$_SESSION['modal_sess'] = 'open';
$city = $_SESSION['crm_city'];
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>GoBumpr Bridge</title>


<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>

<style>
.borderless td, .borderless th {
    border: none !important;
}
*{box-sizing:border-box;}html{min-height:100%;}body{color:black;margin:0px;min-height:inherit;background:rgb(255, 255, 255) !important;}[data-sidebar-overlay]{display:none;position:fixed;top:0px;bottom:0px;left:0px;opacity:0;width:100%;min-height:inherit;}.overlay{background-color:rgb(222, 214, 196);z-index:999990 !important;}aside{position:relative;height:100%;width:200px;top:0px;left:0px;background-color:rgb(236, 239, 241);box-shadow:rgba(0, 0, 0, 0.8) 0px 6px 6px 0px;z-index:999999 !important;}[data-sidebar]{display:none;position:absolute;height:100%;z-index:100;}.padding{padding:2em;}.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6{font-family:inherit;font-weight:500;line-height:1.1;color:inherit;}.h4, .h5, .h6, h4, h5, h6{margin-top:10px;margin-bottom:10px;}.h4, h4{font-size:18px;}img{border:0px;vertical-align:middle;}a{text-decoration:none;color:black;}aside a{color:rgb(0, 0, 0);font-size:16px;text-decoration:none;}.fa{display:inline-block;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;line-height:1;font-family:FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;}nav, ol{font-size:18px;margin-top:-4px;background:rgb(0, 150, 136) !important;}.navbar-fixed-top{z-index:100 !important;}.container-fluid{padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto;}ol, ul{margin-top:0px;margin-bottom:10px;}.breadcrumb > li{display:inline-block;}ol ol, ol ul, ul ol, ul ul{margin-bottom:0px;}.nav{padding-left:0px;margin-bottom:0px;list-style:none;}.navbar-nav{margin:0px;float:left;}.navbar-right{margin-right:-15px;float:right !important;}.nav > li{position:relative;display:block;}.navbar-nav > li{float:left;}.dropdown{position:relative;display:inline-block;}.form-group{margin-bottom:15px;}button, input, optgroup, select, textarea{margin:0px;font-style:inherit;font-variant:inherit;font-weight:inherit;font-stretch:inherit;font-size:inherit;line-height:inherit;font-family:inherit;color:inherit;}button, select{text-transform:none;}button, input, select, textarea{font-family:inherit;font-size:inherit;line-height:inherit;}.form-control{display:block;width:100%;height:34px;padding:6px 12px;font-size:14px;line-height:1.42857;color:rgb(85, 85, 85);background-color:rgb(255, 255, 255);background-image:none;border:1px solid rgb(204, 204, 204);border-radius:4px;box-shadow:rgba(0, 0, 0, 0.075) 0px 1px 1px inset;transition:border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;}p{margin:0px 0px 10px;}b, strong{font-weight:700;}.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9{position:relative;min-height:1px;padding-right:15px;padding-left:15px;}.col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9{float:left;}.col-sm-2{width:16.6667%;}.col-sm-offset-1{margin-left:8.33333%;}.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9{float:left;}.col-lg-3{width:25%;}.col-lg-offset-1{margin-left:8.33333%;}.col-sm-3{width:25%;}.glyphicon{position:relative;top:1px;display:inline-block;font-family:"Glyphicons Halflings";font-style:normal;font-weight:400;line-height:1;-webkit-font-smoothing:antialiased;}.caret{display:inline-block;width:0px;height:0px;margin-left:2px;vertical-align:middle;border-top:4px dashed;border-right:4px solid transparent;border-left:4px solid transparent;}.floating-box{display:inline-block;margin:22px;padding:22px;width:203px;height:105px;box-shadow:rgba(0, 0, 0, 0.2) 0px 8px 16px 0px;font-size:20px;}.uil-default-css{position:relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(1){animation:uil-default-anim 1s linear -0.5s infinite;}.uil-default-css > div:nth-of-type(2){animation:uil-default-anim 1s linear -0.416667s infinite;}.uil-default-css > div:nth-of-type(3){animation:uil-default-anim 1s linear -0.333333s infinite;}.uil-default-css > div:nth-of-type(4){animation:uil-default-anim 1s linear -0.25s infinite;}.uil-default-css > div:nth-of-type(5){animation:uil-default-anim 1s linear -0.166667s infinite;}.uil-default-css > div:nth-of-type(6){animation:uil-default-anim 1s linear -0.0833333s infinite;}.uil-default-css > div:nth-of-type(7){animation:uil-default-anim 1s linear 0s infinite;}.uil-default-css > div:nth-of-type(8){animation:uil-default-anim 1s linear 0.0833333s infinite;}.uil-default-css > div:nth-of-type(9){animation:uil-default-anim 1s linear 0.166667s infinite;}.uil-default-css > div:nth-of-type(10){animation:uil-default-anim 1s linear 0.25s infinite;}.uil-default-css > div:nth-of-type(11){animation:uil-default-anim 1s linear 0.333333s infinite;}.uil-default-css > div:nth-of-type(12){animation:uil-default-anim 1s linear 0.416667s infinite;}.dropdown-menu{position:absolute;top:100%;left:0px;z-index:1000;display:none;float:left;min-width:160px;padding:5px 0px;margin:2px 0px 0px;font-size:14px;text-align:left;list-style:none;background-color:rgb(255, 255, 255);-webkit-background-clip:padding-box;background-clip:padding-box;border:1px solid rgba(0, 0, 0, 0.15);border-radius:4px;box-shadow:rgba(0, 0, 0, 0.176) 0px 6px 12px;}button, html input[type="button"], input[type="reset"], input[type="submit"]{-webkit-appearance:button;cursor:pointer;}button{overflow:visible;}.btn{display:inline-block;padding:6px 12px;margin-bottom:0px;font-size:14px;font-weight:400;line-height:1.42857;text-align:center;white-space:nowrap;vertical-align:middle;touch-action:manipulation;cursor:pointer;user-select:none;background-image:none;border:1px solid transparent;border-radius:4px;}.btn-success{color:rgb(255, 255, 255);background-color:rgb(92, 184, 92);border-color:rgb(76, 174, 76);}.btn-group-sm > .btn, .btn-sm{padding:5px 10px;font-size:12px;line-height:1.5;border-radius:3px;}button[disabled], html input[disabled]{cursor:default;}.btn.disabled, .btn[disabled], fieldset[disabled] .btn{cursor:not-allowed;box-shadow:none;opacity:0.65;}.btn-default{color:rgb(51, 51, 51);background-color:rgb(255, 255, 255);border-color:rgb(204, 204, 204);}input{line-height:normal;}
</style>
</head>
<body>
<?php include_once("header.php"); ?>

 <div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>
<?php
$sql_quote = "SELECT quote FROM bridge_quotes ORDER BY RAND() LIMIT 1";
$res_quote = mysqli_query($conn,$sql_quote);
$row_quote = mysqli_fetch_object($res_quote);
$quote = $row_quote->quote;
?>
 <div style="margin-top:20px;margin-left:50px;font-size:20px;">
 <p><span style="font: 21px/30px Georgia, serif;color:#263238;">Hi <strong><?php echo $crm_name; ?>!</strong></span> <span style="font: 20px/30px Georgia, serif;color:#263238;"><?php echo $quote; ?></span>
  </p>
 </div>

     <!-- date range picker -->
    <div class="col-lg-3 col-sm-2 col-lg-offset-1 col-sm-offset-1">
    <div id="reportrange" class=" col-sm-3 " style="cursor: pointer; margin-top:8px; margin-left: 40px;">
       <div class=" floating-box1">
    		 <div id="range" class="form-control" style="min-width:312px;">
         <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
        <span id="dateval"></span> <b class="caret"></b>
    	</div>
        </div>
    </div>
  </div>
  
<div id="show" style="float:left;">
  <div id="id1" align="center" class="navbar-fixed-top" style="margin-top:162px;">
		 <a href="mleads_tyres.php?t=<?php echo base64_encode(l); ?>">
	   <div id="leads" class="floating-box" style="background-color: #A7E9B1;">
	   <p ><i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;&nbsp;New Bookings</p>
	   <p id="leadsCount"></p>
	   </div>
		 </a>
     <a href="mgoaxle_tyres.php?t=<?php echo base64_encode(b); ?>">
     <div id="reports" class="floating-box" style="background-color:#FFE4B5;">
     <p><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp;GoAxles</p>
     <p id="goaxleCount"></p>
     </div>
     </a>
     <a href="mfollowups_tyres.php?t=<?php echo base64_encode(f); ?>">
     <div id="reports" class="floating-box" style="background-color:#ffb933;">
     <p><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;&nbsp;Follow Ups</p>
     <p id="followupsCount"></p>
     </div>
     </a>
    </div>
    <div id="id2" align="center" class="navbar-fixed-top" style="margin-top:302px;">

      <a href="mcancelled_tyres.php?t=<?php echo base64_encode(c); ?>">
    <div id="acancelled" class="floating-box" style="background-color: #FF7043;">
    <p><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;&nbsp;Cancelled</p>
    <p id="cancelledCount"></p>
    </div>
    </a>
      <a href="mothers_tyres.php?t=<?php echo base64_encode(o); ?>">
    <div id="aothers" class="floating-box" style="background-color: #85A1E7;">
    <p><i class="fa fa-tasks" aria-hidden="true"></i>&nbsp;&nbsp;Others</p>
    <p id="othersCount"></p> 
    </div>
    </a>
    <!-- <a href="maxle_tyres.php?t=<?php echo base64_encode(a); ?>">
      <div id="axle" class="floating-box" style="background-color:#8e9a7a;">
      <p><i class="fa fa-telegram" aria-hidden="true"></i>&nbsp;&nbsp;Axle</p>
      <p id="ac">&nbsp;</p>
      </div>
      </a> -->
   </div>
 <!--  <div id="id3" align="center" class="navbar-fixed-top" style="margin-top:442px;">

  <a href="daily_reports_tyres.php?t=<?php echo base64_encode(r); ?>">
     <div id="reports" class="floating-box" style="background-color:rgba(126, 57, 181, 0.72);">
     <p><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp;Tyres</p>
     <p id="rep">Reports</p>
     </div>
     </a>
   </div>
 -->	</div>
 </div>
  <!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>

<noscript id="async-styles">

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />

</noscript>
<script> // The CSS mentioned in "async-styles" will be loaded and styles will be applied.
    var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("async-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
        webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function() {
        window.setTimeout(loadDeferredStyles, 0);
    });
    else window.addEventListener('load', loadDeferredStyles);
</script>

<script async src="js/jquery-3.2.1.min.js" onLoad="loadOtherScripts();"></script>

<script>
    function loadScript(src) {
        return new Promise(function (resolve, reject) {
            var s;
            s = document.createElement('script');
            s.src = src;
            s.onload = resolve;
            s.onerror = reject;
            document.head.appendChild(s);
        });
    }

    function loadOtherScripts() {
      loadScript('js/moment.min.js')
      .then(function() {
      Promise.all([ loadScript('js/daterangepicker.js'),loadScript('js/sidebar.js')]).then(function () {
      console.log('scripts are loaded');
      dateRangePicker();
      momentInPicker();
      intialFunction();
      }).catch(function (error) {
      console.log('some error!' + error)
      })
      }).catch(function (error) {
      console.log('Moment call error!' + error)
      })
      }
</script>

<!-- date range picker -->
<script>
function dateRangePicker(){
  $('input[name="daterange"]').daterangepicker({
  locale: {
        format: 'DD-MM-YYYY'
      }
  });
}
</script>
<script>
function momentInPicker(){
    var start = moment().subtract(1, 'days');
    // var start = moment();
      var end = moment();
    var lifetime = moment().year(2016).month(0).date(1).hour(0).minute(0).second(0);

      function cb(start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }

      $('#reportrange').daterangepicker({
          startDate: start,
          endDate: end,
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'Life Time': [lifetime, moment()]
          }
      }, cb);

      cb(start, end);
}
</script>

<script>
function defaultview(){
	var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
	var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
  var veh = $("#veh").val();
  var person = $("#alloted").val();
  var city = $("#city").val();
  var cluster = $("#cluster").val();
  console.log(city);
	//console.log(startDate);
	//console.log(endDate);
	          //Make AJAX request, using the selected value as the POST

			  $.ajax({
	            url : "ajax/mhome_view.php",  // create a new php page to handle ajax request
	            type : "GET",
              json : false,
	            data : {"startdate": startDate , "enddate": endDate,"veh":veh,"person":person, "city":city, "cluster":cluster},
	            success : function(data) {
                  //alert(data);
                 // console.log(data);
                  $('#show').show();
                  $("#loading").hide();
                  // $('#show').html(data);
                  var fin_data=data;
 $("#leadsCount").html(fin_data.leadsCount);
 $("#goaxleCount").html(fin_data.goaxleCount);
 $("#followupsCount").html(fin_data.followupsCount);
 $("#cancelledCount").html(fin_data.cancelledCount);
 $("#othersCount").html(fin_data.othersCount);
                  $.map($.parseJSON(data), displayInfo);
              },
	            error: function(xhr, ajaxOptions, thrownError) {
	                // alert(xhr.status + " "+ thrownError);
	            }
				 });
}
</script>
<script>
  function intialFunction(){
  //show city selection
  $(document).ready(function(){
    $('#city').show();
    $('#cluster').show();
    var mark_flag = "<?php echo $_SESSION['marketing_flag']; ?>" ;
    var su_flag = "<?php echo $_SESSION['flag']; ?>" ;
    //console.log(mark_flag);
    mark_flag != '1' ? $("#extbook").hide() : $("#extbook").show();
   /* if(mark_flag != '1'){
      $("#extbook").hide();
    }
    else{
      $("#extbook").show();
    }
   */
    //console.log(admin_flag);
    if(su_flag != '1'){
      $("#incmpltbook").hide();
    }
    else{
      $("#incmpltbook").show();
    }
  })

  // automatic refresh
  window.setInterval(function(){
    // defaultview();
  }, 240000); // milli seconds

  //check online status
  function when_online() {
    document.title = "GoBumpr Bridge";
    $("#show").hide();
    $("#loading").show();
    // defaultview();
  }
  function when_offline() {
    document.title = "You're Offline!";
    alert("You're Offline! Please check your internet connection");
  }

  // Update the online status icon based on connectivity
  window.addEventListener('online',  when_online);
  window.addEventListener('offline', when_offline);

  //on page load
  $(document).ready( function (){
    $('#show').hide();
    $("#loading").show();
    defaultview();
  
    $('#dateval').on("DOMSubtreeModified", function (){
    $('#show').hide();
    $("#loading").show();
    defaultview();
    });
 
    $('#city').change(function (){
    $('#show').hide();
    $("#loading").show();
    defaultview();
    });
  
    $('#cluster').change(function (){
    $('#show').hide();
    $("#loading").show();
    defaultview();
    });
  });
}
</script>
</body>
</html>

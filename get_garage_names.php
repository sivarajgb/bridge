<?php
include('../config.php');
$conn = db_connect3();

$type = $_POST['selecttype'];

switch($type){
    case "existing":$query = "SELECT DISTINCT m.b2b_shop_id,m.b2b_shop_name,m.b2b_vehicle_type FROM b2b_mec_tbl as m INNER JOIN b2b_credits_tbl as c ON m.b2b_shop_id = c.b2b_shop_id WHERE m.b2b_cred_model=1  ORDER BY m.b2b_shop_name ASC"; break;
    case "new":$query = "SELECT DISTINCT b2b_shop_id,b2b_shop_name,b2b_vehicle_type FROM b2b_mec_tbl WHERE b2b_cred_model!=1 ORDER BY b2b_shop_name ASC"; break;
}
$result = mysqli_query($conn,$query);
?>
<option value="" selected>Select Garage Name</option>
<?php
while($row = mysqli_fetch_object($result)){ 
    $mec_id=$row->b2b_shop_id;
    $shop_name=$row->b2b_shop_name;
    $veh_type=$row->b2b_vehicle_type;
?>
<option value="<?php echo $mec_id;?>"><?php echo $shop_name.' - '.$veh_type; ?></option>
<?php }	
?>

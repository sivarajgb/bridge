<?php
// error_reporting(E_ALL); ini_set('display_errors', 1);
include('config.php');
$conn = db_connect1();
session_start();

$user_id = $_POST['user_id'];
$veh_id = $_POST['veh_id'];
$type = $_POST['type'];
$booking_id= $_POST['book_id'];
$log = $_POST['log'];
// echo $log;die;
$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$user_flag=$_SESSION['flag'];
$cluster_admin = $_SESSION['cluster_admin'];
date_default_timezone_set('Asia/Kolkata');
$today = date('Y-m-d H:i:s');

$category=mysqli_real_escape_string($conn,$_POST['status']);
$comments=mysqli_real_escape_string($conn,$_POST['comments']);
 
$sql_act = mysqli_query($conn,"SELECT id FROM tyres_activity_tbl WHERE activity='$category'");
$row_act = mysqli_fetch_object($sql_act);
$activity_status = $row_act->id;


if($category == "Duplicate Booking"){
	$original = 1;
	$prev = date('Y-m-d H:i:s',strtotime("-1 day",strtotime($log)));
	$next = date('Y-m-d H:i:s',strtotime("+1 day",strtotime($log)));

	$prev_booking_query  = "SELECT booking_id,booking_status,flag,flag_unwntd FROM tyre_booking_tb WHERE user_id = '$user_id' AND booking_id!='$booking_id' and log >= '$prev' and log <= '$next'  ORDER BY booking_id DESC ";
	// echo $prev_booking_query;die;
	$prev_booking_res = mysqli_query($conn,$prev_booking_query) or die(mysqli_error($conn));
	if(mysqli_num_rows($prev_booking_res)>0){

		while($prev_booking_row = mysqli_fetch_object($prev_booking_res))
		{
			// var_dump($prev_booking_row);
			$prev_booking_id = $prev_booking_row->booking_id;
			$booking_status = $prev_booking_row->booking_status;
			$flag = $prev_booking_row->flag;
			$flag_unwntd = $prev_booking_row->flag_unwntd;
		
			$comments_action_sql = "SELECT * FROM tyres_comments_tbl WHERE book_id = '$prev_booking_id' AND (activity_status NOT IN (16) OR activity_status IS NULL)";
			// echo $comments_action_sql;
			$comments_action_res = mysqli_query($conn,$comments_action_sql) or die(mysqli_error($conn));
			$action_count = mysqli_num_rows($comments_action_res);
			if($flag == 1 && $flag_unwntd == 1)
			{
				$original = 1;
			}
			else if($action_count > 0 || $booking_status == 2)
			{
				
				$original = 0;
				break;
			}
		}
	} 
	// die;
	if($original == 0)
	{
		$sql = "UPDATE tyre_booking_tb SET flag='1',flag_unwntd='1',crm_update_time='$today',activity_status='$activity_status' WHERE booking_id='$booking_id' ";
		$res = mysqli_query($conn,$sql) or die(mysqli_error($conn));
		$data['original']=$original;
	}
	else
	{
		$data['original']=$original;
	}
}
else{
    $sql = "UPDATE tyre_booking_tb SET flag='1',crm_update_time='$today',activity_status='$activity_status' WHERE booking_id='".$booking_id."' "; 
   // echo $sql; 
    $res = mysqli_query($conn,$sql) or die(mysqli_error($conn));

    $data['original']=0;  
}

if($data['original']==0){

$cancelled = 'Booking Cancelled - '.$booking_id.' - '.$category;
$query="INSERT INTO tyres_comments_tbl(user_id,crm_log_id,category,comments,Follow_up_date,flag,log,status,activity_status,book_id) VALUES ('$user_id','$crm_log_id','$cancelled','$comments','0000-00-00','0','$today','Cancelled','$activity_status','".$booking_id."')";

$result=mysqli_query($conn,$query) or die(mysqli_error($conn));
}

if($cluster_admin == '1'){
    if($type == 'l' || $type == 'ur'){
        header("Location:mleads_tyres.php");
   }
   else{
       header("Location:mcancelled_tyres.php");
   }
} 
else if($user_flag == '1'){
    if($type == 'l'){
         header("Location:aleads_tyres.php");
    }
    else{
        header("Location:acancelled_tyres.php");
    }
}
else{
    if($type == 'l'){
        header("Location:leads_tyres.php"); 
   }
   else{
       header("Location:cancelled_tyres.php");
   }
}  

    
?>
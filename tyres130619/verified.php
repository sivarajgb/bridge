<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id']))) {
  header('location:logout.php');
  die();
}
$crm_log_id = $_SESSION['crm_log_id'];
date_default_timezone_set('Asia/Kolkata');
$today=date('Y-m-d H:i:s');

$user_id=base64_decode($_GET['u']);


if($user_id == ''){
  header('location:somethingwentwrong.php');
  die();
}
//edit user
if(isset($_POST['edit_user_submit'])){
$log = date('Y-m-d H:i:s');

  $mobile = mysqli_real_escape_string($conn,$_POST['mobile']);
  $mobile2 = mysqli_real_escape_string($conn,$_POST['mobile2']);
  $name = mysqli_real_escape_string($conn,$_POST['user_name']);
  $email_id = mysqli_real_escape_string($conn,$_POST['email']);
  $City = mysqli_real_escape_string($conn,$_POST['city']);
  $Locality_Home = mysqli_real_escape_string($conn,$_POST['location_home']);
  $Locality_Work = mysqli_real_escape_string($conn,$_POST['location_work']);
  $Next_service_date = date('Y-m-d',strtotime(mysqli_real_escape_string($conn,$_POST['next_service_date'])));
  $Last_service_date = date('Y-m-d',strtotime(mysqli_real_escape_string($conn,$_POST['last_service_date'])));
  $Last_called_on = date('Y-m-d',strtotime(mysqli_real_escape_string($conn,$_POST['last_called_on'])));
  $Follow_up_date = date('Y-m-d',strtotime(mysqli_real_escape_string($conn,$_POST['follow_up_date'])));
  $comments = mysqli_real_escape_string($conn,$_POST['comments']);
  $campaign = mysqli_real_escape_string($conn,$_POST['campaign']);
  $source = mysqli_real_escape_string($conn,$_POST['source']);
  $query="UPDATE user_register SET name='$name',mobile_number2='$mobile2',email_id='$email_id',crm_log_id='$crm_log_id',crm_update_time='$log',source='$source',campaign='$campaign', City='$City',Locality_Home='$Locality_Home',Locality_Work='$Locality_Work',Next_service_date='$Next_service_date',Last_service_date='$Last_service_date',Last_called_on='$Last_called_on',Follow_up_date='$Follow_up_date',comments='$comments' WHERE reg_id='$user_id'";

  $res=mysqli_query($conn,$query) or die(mysqli_error($conn));
   $u=base64_encode($user_id);
 header("Location:verified.php?u=$u");
exit(1);

}

// add vehicle
if(isset($_POST['vehicle_submit'])){
     $log= date('Y-m-d H:i:s'); 
   $type='4w';
   $brand_model=mysqli_real_escape_string($conn,$_POST['BrandModelid']);
  $fuel_type=mysqli_real_escape_string($conn,$_POST['fuel']);
   $reg_no=mysqli_real_escape_string($conn,$_POST['regno']);
  $year=mysqli_real_escape_string($conn,$_POST['year']);
   $km_driven=mysqli_real_escape_string($conn,$_POST['km']);
   
   $sql_b_m = "SELECT id,brand,model FROM admin_vehicle_table_new WHERE brand_s_model = '$brand_model' ";
   $quer_b_m = mysqli_query($conn,$sql_b_m);
   $row_b_m = mysqli_fetch_array($quer_b_m);
   $vehicle_id=$row_b_m['id'];
  $brand=$row_b_m['brand'];
  $model=$row_b_m['model'];
  

echo $sqlins="INSERT INTO user_vehicle_table(user_id,vehicle_id,brand,model,type,fuel_type,year,km_driven,reg_no,flag,crm_log_id,crm_update_time,log) VALUES('$user_id','$vehicle_id','$brand','$model','$type','$fuel_type','$year','$km_driven','$reg_no','1','$crm_log_id','$log','$log')";
mysqli_query($conn,$sqlins) or die(mysqli_error($conn));

mysqli_query($conn,"UPDATE user_register SET vehicle='1' WHERE reg_id='$user_id'");

$sql_get_veh = mysqli_query($conn,"SELECT id FROM user_vehicle_table WHERE user_id='$user_id' AND vehicle_id='$vehicle_id' AND flag='1'");
$row_get_veh = mysqli_fetch_object($sql_get_veh);

$veh_id = $row_get_veh->id;
// $category1 = $user_id.' - '.'New Vehicle';
//   $query1="INSERT INTO tyres_comments_tbl(user_id,crm_log_id,category,comments,Follow_up_date,flag,log,update_log,status,activity_status,book_id) VALUES ('$user_id','$crm_log_id','$category1','New vehicle','-','0','$log','$log','NewVehicle','0','-')";
  
//   $r1=mysqli_query($conn,$query1);

/*redirect */
$u= base64_encode($user_id);
$veh = base64_encode($veh_id);

header("Location:verified.php?u=$u");
//exit(1);
}
// add booking
if(isset($_POST['booking_submit'])){
 date_default_timezone_set('Asia/Kolkata');
 $log= date('Y-m-d H-i-s'); 
 $tyre_brand = $_POST['tyre_brand'];
 $tyre_count=$_POST['tyre_count'];
 $tyre_req_time = $_POST['tyre_req_time'];
 $mec_id_lead = $_POST['mec_id_lead'];
 $axle_id_lead = $_POST['axle_id_lead'];
 $shop_name = $_POST['shopname_ref'];
  $mec_id=$_POST['mechanic'];
  $sql_mec = "SELECT shop_name FROM admin_mechanic_table WHERE mec_id='$mec_id' ";
  $res_mec = mysqli_query($conn,$sql_mec);
  $row_mec=mysqli_fetch_object($res_mec);
  $locality=mysqli_real_escape_string($conn,$_POST['location']);
  $vehicle_type=mysqli_real_escape_string($conn,$_POST['veh_b']);
  $user_vech_id=mysqli_real_escape_string($conn,$_POST['veh_no']);
  $service_type=mysqli_real_escape_string($conn,$_POST['service_type']);
  $service_description=mysqli_real_escape_string($conn,$_POST['description']);
  $amt=mysqli_real_escape_string($conn,$_POST['amount']);
  $service_date=date('Y-m-d',strtotime(mysqli_real_escape_string($conn,$_POST['service_date'])));
  $next_service_date=date('Y-m-d',strtotime(mysqli_real_escape_string($conn,$_POST['next_service_date'])));
  // $pick_up=mysqli_real_escape_string($conn,$_POST['pickup']);
  $pick_up = 0;
  $sms = mysqli_real_escape_string($conn,$_POST['sms']);
  $locality=mysqli_real_escape_string($conn,$_POST['location']);
  
  $sql_loc = "SELECT city FROM localities WHERE localities='$locality'";
  $res_loc = mysqli_query($conn,$sql_loc);
  $row_loc = mysqli_fetch_array($res_loc);
  $city = $row_loc['city'];
  
  //get vehicle details
  $sql_veh="SELECT vehicle_id,brand,model,reg_no FROM user_vehicle_table WHERE id='$user_vech_id'";
  $res_veh=mysqli_query($conn,$sql_veh);
  $row_veh=mysqli_fetch_object($res_veh);
  $vehicle_id=$row_veh->vehicle_id;
  $brand = $row_veh->brand;
  $model = $row_veh->model;
  $veh_no = $row_veh->reg_no;  
    
  //insert into booking table
  $query = "INSERT INTO tyre_booking_tb(user_id,mec_id,shop_name,rep_name,rep_number,vech_id,user_veh_id,vehicle_type,user_vech_no,service_description,service_type,initial_service_type,amt,service_date,status,source,pick_up,crm_log_id,crm_allocate_id,pickup_address,crm_update_time,log,flag,booking_status,crm_update_id,followup_date,axle_flag,locality,city,tyre_brand,tyre_count,tyres_req_time,mec_id_leads,axle_id_leads) VALUES ('$user_id','0','$shop_name','','','$vehicle_id','$user_vech_id','4w','$veh_no','$service_description','$service_type','$service_type','$amt','$service_date','admin','Hub Booking','$pick_up','$crm_log_id','$crm_log_id','$locality','$log','$log','0','1','$crm_log_id','$next_service_date','0','$locality','$city','$tyre_brand','$tyre_count','$tyre_req_time','$mec_id_lead','$axle_id_lead')";

  $sqlins1 = mysqli_query($conn,$query)or die(mysqli_error($conn));  
  $last_id_query = "SELECT LAST_INSERT_ID() as id_value";
  $id_query = mysqli_query($conn,$last_id_query) or die(mysqli_error($conn));
  $id_val = mysqli_fetch_array($id_query);
    $id = $id_val['id_value'];
  $booking_id = $id;
  // $comments = "Booking";
  // $category1 = 'Booking - '.$booking_id.'';
  
  // $query1="INSERT INTO tyres_comments_tbl(user_id,crm_log_id,category,comments,Follow_up_date,flag,log,status,activity_status,book_id) VALUES ('$user_id','$crm_log_id','$category1','$comments','0000-00-00','0','$log','NewBooking','-','$booking_id')";
  
  //        $r1=mysqli_query($conn,$query1) or die(mysqli_error($conn));    
if($sms==1)
{
$exclude_sms = array('crm012','crm041','crm055','crm079');
  
if(!in_array($crm_log_id,$exclude_sms))
{
  
//********sms start*****************
$sql=mysqli_query($conn,"SELECT mobile_number,name FROM user_register WHERE reg_id='$user_id'");
while ($row=mysqli_fetch_array($sql)) {
      $to=$row['mobile_number'];
      $user_name=$row['name'];
}
//  $to="9505913666";
//$user_name="Sirisha";
$sql_contact = "SELECT * FROM go_axle_service_price_tbl WHERE service_type = '$service_type' AND type='$vehicle_type'";
$res_contact = mysqli_query($conn,$sql_contact);
  $row_contact = mysqli_fetch_object($res_contact);
  
  $row_contact = mysqli_fetch_array($res_contact);
  
  $column = ucwords($city).'_support';
  
  $contact = $row_contact[$column];
  
  if($contact == ''){
      $contact = '7824000649';
  } 

  $sms = "Dear ".$user_name.", thanks for booking with us. Our representative will get in touch with you shortly. For help, please call us at ".$contact.". Let's GoBumpr!";
  
  $to = str_replace(' ', '%20', $to);
  $user_name = str_replace(' ', '%20', $user_name);
  $sms = str_replace(' ', '%20', $sms);


$ch=curl_init("http://203.212.70.200/smpp/sendsms?username=northautohttp&password=north123&to=%22".$to."%22&from=GOBMPR&udh=&text=".$sms."");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result=curl_exec($ch);
curl_close($ch);
$post = '{"status":"'.$result.'"}';
  
//********sms end**************** 
}

}
if($flag == 1){
        header("Location:aleads_tyres.php");
    }
    else{
        header("Location:leads_tyres.php");
    }
}
?>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
  <title>GoBumpr Bridge</title>
<!-- validate -->
<script sr="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <!-- masking -->
<script type="text/javascript" src="js/inputmask.js"></script>
<script type="text/javascript" src="js/jquery.inputmask.js"></script>
  <script type="text/javascript" src="js/inputmask.extensions.js"></script>
  <!-- auto complete -->
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/jquery-ui.structure.min.css"> 
<link rel="stylesheet" href="css/jquery-ui.theme.min.css">
<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- date time picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.0.1/js/bootstrap-switch.js"></script>

<link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="css/bootstrap-datetimepicker-standalone.css">

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>
  <style>
  <!-- auto complete -->
  @charset "utf-8";.ui-autocomplete{z-index:1000 !important;cursor:default;list-style:none;}
  .ui-widget{}
  .ui-autocomplete{overflow-y:auto;overflow-x:hidden;}
  .ui-menu{width:0px;display:none;}
  .ui-autocomplete > li{padding:10px;padding-left:10px;}
  ul{margin-bottom:0;}
  .ui-autocomplete > li.ui-state-focus{background-color:#DDD;}
  .ui-autocomplete .ui-front .ui-menu .ui-widget .ui-widget-content{border:0;}
  .ui-helper-hidden-accessible{display:none;}
  .gobumpr-icon{font-style:normal;font-weight:normal;speak:none;display:inline-block;text-decoration:inherit;text-align:center;font-variant:normal;text-transform:none;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
  .ui-widget{background-color:white;width:100%;}
  .ui-widget-content{padding-left:1px;display:block;width:20px;position:relative;line-height:12px;max-height:210px;border:.5px solid #DADADA;}
  .ui-widget{}
  .ui-autocomplete { position: absolute; cursor: default;}


<!-- vehicle type -->
.bike,
.car{
  cursor: pointer;
  user-select: none;
  -webkit-user-select: none;
  -webkit-touch-callout: none;
}
.bike > input,
.car > input{ /* HIDE ORG RADIO & CHECKBOX */
  visibility: hidden;
  position: absolute;
}
/* RADIO & CHECKBOX STYLES */
.bike > i,
.car > i{     /* DEFAULT <i> STYLE */
  display: inline-block;
  vertical-align: middle;
  width:  16px;
  height: 16px;
  border-radius: 50%;
  transition: 0.2s;
  box-shadow: inset 0 0 0 8px #fff;
  border: 1px solid gray;
  background: gray;
}

label > input{ /* HIDE RADIO */
  visibility: hidden; /* Makes input not-clickable */
  position: absolute; /* Remove input from document flow */
}
label > input + img{ /* IMAGE STYLES */
  cursor:pointer;
  border:2px solid transparent;
}
label > input:checked + img { /* (RADIO CHECKED) IMAGE STYLES */
   border-radius:12px;
   padding:5px;
   background-color:#ffa800;
  box-shadow: 0 0 3px 0 #394;
}
.borderless td, .borderless th {
    border: none !important;
}

    .datepicker {
    cursor:pointer;
  }
.datepicker:before {
content: '';
display: inline-block;
border-left: 7px solid transparent;
border-right: 7px solid transparent;
border-bottom: 7px solid #ccc;
border-bottom-color: transparent !important;
position: absolute;
top: -7px;
left: 190px;  // I made a change here 
}

.datepicker:after {
content: '';
display: inline-block;
border-left: 6px solid transparent;
border-right: 6px solid transparent;
border-top-color: transparent !important;
border-top: 6px solid #ffffff;
position: absolute;
bottom: -6px;
left: 191px;  // I made a change here 
}
#datepick > span:hover{cursor: pointer;}

.floating-box {
   display: inline-block;
   margin: 22px;
 padding: 22px;
 width:203px;
 height:105px;
 box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  font-size: 17px;
}
select{
    color: red;
}
option{
  height: 25px;
}
option:hover {
  box-shadow: 0 0 10px 100px #ddd inset;
}
</style>

</head>
<body>
<?php include_once("header.php"); ?>
<script>
  $(document).ready(function(){
    $('#city').hide();
  })
</script>
<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>
    <?php
    $sql_get = "SELECT name,mobile_number,mobile_number2,email_id,City,vehicle,Locality_Home FROM user_register WHERE reg_id='$user_id' ";
    $res_get = mysqli_query($conn,$sql_get);
    $row_get = mysqli_fetch_object($res_get);
    $user_name = $row_get->name;
    $mobile = $row_get->mobile_number;
  $alt_mobile = $row_get->mobile_number2;
    $email = $row_get->email_id;
    $city = $row_get->City;
    $veh = $row_get->vehicle;
    $user_location = $row_get->Locality_Home;
  $sql_book = mysqli_query($conn,"SELECT booking_id,user_veh_id,user_vech_no,service_type,service_date,shop_name,axle_flag,booking_status,flag,tyre_brand FROM tyre_booking_tb WHERE user_id='$user_id' AND flag_unwntd!='1' ORDER  BY booking_id DESC LIMIT  1;");
    $row_book = mysqli_fetch_array($sql_book);
    $row_count = mysqli_num_rows($sql_book);
    if($row_count >= 1){
       $book = 1;
       $booking_id=$row_book['booking_id'];
       $vech_id=$row_book['user_veh_id'];
       $veh_no = $row_book['user_vech_no'];
       $s_type = $row_book['tyre_brand'];
       $s_date=$row_book['service_date'];
       $shop_name=$row_book['shop_name'];
       $axle=$row_book['axle_flag'];
       $status = $row_book['booking_status'];
       $booking_flag = $row_book['flag'];
        if($booking_flag == 1){
            $st="c";
        }
        else{
            switch($status){
                case 0: $st="o"; break;
                case 1: $st="l"; break;
                case 2: $st="b"; break;
                case 3: $st="f"; break;
                case 4: $st="f"; break;
                case 5: $st="f"; break;
                case 6: $st="f"; break;
              }
        }
       
       $b=base64_encode($booking_id);
       $v=base64_encode($vech_id);
       $t=base64_encode($st);
    }
    else{
        $book = 0;
    }
    ?>
      <div id="user" style="border:2px solid #708090; border-radius:8px;margin-left:30px; width:380px;height:330px; padding:20px; margin-top:18px; float:left;">
    <table id="table1" class="table borderless" >
       <tr><td><strong>User Id</strong></td><td><?php echo $user_id; ?></td></tr>
       <tr><td><strong>Name</strong></td><td><?php echo $user_name; ?></td></tr>
       <tr><td><strong>Phn No.</strong></td><td><?php echo $mobile; ?></td></tr>
     <tr><td><strong>AltPhnNo.</strong></td><td><?php echo $alt_mobile; ?></td></tr>
       <tr><td><strong>E-mail</strong></td><td><?php echo $email; ?></td></tr>
       <tr><td><strong>Current Location<strong></td><td><?php echo $user_location; ?></td></tr>
  </table>
 
<!-- Edit User -->
<div  style="float:left; margin-left:90px;">
<div id="edting_user" style="display:inline-block; align-items:center;" >
<!-- Trigger the modal with a button -->
<button id="eu" type="button" class="btn btn-sm" data-toggle="modal" data-target="#myModal_user" style="background-color:#E0F2F1;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;Edit User</button>
</div></div>
 </div>
<div id="vehd" align="center" style="margin-left:30px;border:2px solid #708090; border-radius:8px; width:430px;height:330px; padding:20px; margin-top:18px; float:left;">
<?php
if($veh == 1){
 $sql_veh = "SELECT reg_no,type,brand,model,flag FROM user_vehicle_table WHERE user_id='$user_id' ORDER BY brand Asc";
 $res_veh = mysqli_query($conn,$sql_veh);
 $rc = mysqli_num_rows($res_veh);
 if($rc >=1){
   ?>
   <div style="height:295px;overflow-y:auto;">
    <table class="table">
    <thead>
    <th>Vehicle No.</th>
    <th>Type</th>
    <th>Brand</th>
    <th>Model</th>
    </thead>
    <tbody>
      <?php 
      while($row_veh = mysqli_fetch_object($res_veh)){
          $veh_flag = $row_veh->flag;
          if($veh_flag == 0){
              ?>
               <tr style="color:red !important;">
       <td><?php echo $reg_no=$row_veh->reg_no; ?></td>
       <td><?php echo $vtype = $row_veh->type; ?></td>
       <td><?php echo $veh_brand = $row_veh->brand; ?></td>
       <td><?php echo $veh_model = $row_veh->model; ?></td>
       </tr>
              <?php
          }
          else{
?>

       <tr>
       <td><?php echo $reg_no=$row_veh->reg_no; ?></td>
       <td><?php echo $vtype = $row_veh->type; ?></td>
       <td><?php echo $veh_brand = $row_veh->brand; ?></td>
       <td><?php echo $veh_model = $row_veh->model; ?></td>
       </tr>
       <?php
      }// else
      }//while
      ?>
      </tbody>
    </table>
    </div>
<?php
 }
 else{
   echo "<div style='margin-top:25px;'><h4>No Vehicles to show !!!</h4></div>";
 }
}
else{
    echo "<div style='margin-top:25px;'><h4>No Active Vehicles to show !!!</h4></div>";
}
?>
</div>
<div align="center" style="margin-left:30px;border:2px solid #708090; border-radius:8px; width:350px;height:330px; padding:20px; margin-top:18px; float:left;">
   <?php
   if($book == 0){
    echo "<div style='margin-top:25px;'><h4>No Bookings!</h4></div>";
    
 }
   else{
     ?>
     <div  id="division" style="display:none;">
    <table class="table borderless" style=" border-collapse: collapse;">
    <tr><td><strong>BookingId</strong></td><td><?php echo $booking_id; ?></td></tr>
    <tr><td><strong>BrandType</strong></td><td><?php echo $s_type; ?></td></tr>
    <tr><td><strong>Vehicle</strong></td><td><?php echo $veh_no; ?></td></tr>
    <tr><td><strong>ServiceDate</strong></td><td><?php echo $s_date; ?></td></tr>
    <tr><td><strong>Mechanic</strong></td><td><?php echo $shop_name; ?></td></tr>
    <tr><td><strong>GoAxle</strong></td><td><?php if($booking_flag == '1'){ echo "Booking Cancelled"; } else if($axle=='' || $axle=='0'){ echo "Not Sent"; } else{ echo "Sent"; } ?></td></tr>
    </table>
    </div>
    <?php
   }
   ?>

    </div>
    
    <div align="center" style="margin-top:60px;">
    <?php 
    // if no vehicle is there, give add vehicle option
   /* if($veh == 0){
        ?>
        <div style="margin-top:450px;">
        <h3>Please Add a vehicle to continue...</h3>
    <button id="a2" type="button" class="btn btn-lg" data-toggle="modal" data-target="#myModal_vehicle" style="background-color:#4CB192;margin-right:30px;"><i class="fa fa-car" aria-hidden="true"></i>&nbsp;&nbsp;Add Vehicle</button>
       <?php if($book == 1){ ?>
            <a href="user_details.php?t=<?php echo $t; ?>&v58i4=<?php echo $v; ?>&bi=<?php echo $b; ?>"><button id="a3" type="button" class="btn btn-lg" style="background-color:#E19A16;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;User Details</button></a>
       <?php } ?>
    </div>
    <script>
        $(document).ready(function(){
          $("#division").show();
          $("#div2").hide();
        })
        </script>
        <?php
    }*/
    if($book ==0){
      ?>
       <div style="margin-top:450px;">
        <h3>No Booking is made yet...</h3>
     <button id="a2" type="button" class="btn btn-lg" data-toggle="modal" data-target="#myModal_vehicle" style="background-color:#47B862;margin-right:70px;"><i class="fa fa-car" aria-hidden="true"></i>&nbsp;&nbsp;Add Vehicle</button>
     <button id="a3" type="button" class="btn btn-lg" data-toggle="modal" data-target="#myModal_booking" style="background-color:#4CB192;margin-right:30px;"><i class="fa fa-check-square" aria-hidden="true"></i>&nbsp;&nbsp;Add Booking</button>   
    </div>
    <?php
    }
    if($book == 1){
        ?>
        <script>
        $(document).ready(function(){
          $("#division").show();
          $("#div2").show();
        })
        </script>
    
        <?php
    }
    ?>
  </div>
  <div  id="div2" align="center" style="margin-top:460px;display:none;">
    <button  type="button" class="btn btn-lg" data-toggle="modal" data-target="#myModal_vehicle" style="background-color:#47B862;margin-right:30px;"><i class="fa fa-car" aria-hidden="true"></i>&nbsp;&nbsp;Add Vehicle</button>
    <button id="a2" type="button" class="btn btn-lg" data-toggle="modal" data-target="#myModal_booking" style="background-color:#4CB192;margin-right:30px;"><i class="fa fa-car" aria-hidden="true"></i>&nbsp;&nbsp;Add Booking</button>    
    <a href="user_details_tyres.php?t=<?php echo $t; ?>&v58i4=<?php echo $v; ?>&bi=<?php echo $b; ?>"><button id="a3" type="button" class="btn btn-lg" style="background-color:#E19A16;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;User Details</button></a>
  </div>

 <!-- Edit user Modal -->
<div class="modal fade" id="myModal_user" role="dialog" >
 <div class="modal-dialog" style="width:860px;">
 <!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Edit User</h3>
</div>
 <div class="modal-body" style="height:490px;">
                <?php
                $sql_edit_user ="SELECT name,mobile_number,mobile_number2,email_id,City,Locality_Home,Locality_Work,source,campaign,Last_service_date,Next_service_date,Last_called_on,Follow_up_date,comments FROM user_register WHERE reg_id='$user_id'";
                $res_edit_user = mysqli_query($conn, $sql_edit_user);
                $row_edit_user = mysqli_fetch_object($res_edit_user);
                $u_name = $row_edit_user->name;
                $u_mobile = $row_edit_user->mobile_number;
                $u_alt_mobile = $row_edit_user->mobile_number2;
                $u_mail  = $row_edit_user->email_id;
                $u_city  = $row_edit_user->City;
                $u_loc_home  = $row_edit_user->Locality_Home;
                $u_loc_work  = $row_edit_user->Locality_Work;
                $u_source = $row_edit_user->source;
                $u_campaign = $row_edit_user->campaign;
                $u_last_serviced  = $row_edit_user->Last_service_date;
                $u_next_serviced  = $row_edit_user->Next_service_date;
                $u_last_called  = $row_edit_user->Last_called_on;
                $u_followup  = $row_edit_user->Follow_up_date;
                $u_comments  = $row_edit_user->comments;
                ?>
  <form id="edit_user" class="form" method="post" action="">
    <div class="row">
    <br>
     <div class="col-xs-4 col-xs-offset-1 form-group">
     <input class="form-control" type="text" id="mobile" name="mobile" placeholder="Mobile" readonly maxlength="10" value="<?php echo $u_mobile; ?>">
     </div>
  
  <div class="col-xs-4 form-group">
     <input class="form-control" type="text" id="mobile2" name="mobile2" placeholder="Alternate Mobile" maxlength="10" value="<?php echo $u_alt_mobile; ?>">
     </div>
     </div>
   
   <div class="row">
      <div class="col-xs-5 col-xs-offset-1 form-group">
      <input  class="form-control" type="text" id="user_name" name="user_name"  pattern="^[a-zA-Z0-9\s]+$" onchange="try{setCustomValidity('')}catch(e){}" placeholder="User Name" required  value="<?php echo $u_name; ?>">
      </div>

      <div class="col-xs-5 form-group">
      <input type="email" class="form-control" id="email" name="email" placeholder="E-Mail"  value="<?php echo $u_mail; ?>">
      </div>
      </div>
       <div class="row"></div>
       <div class="row">
         <div id="city_u" class="col-xs-4 col-xs-offset-1 form-group">
          <div class="ui-widget">
                  <select class="form-control" id="cityu" name="city" required>
                             <?php

                            $sql_city = "SELECT DISTINCT city FROM go_bumpr.localities ORDER BY city ASC";
                            $res_city = mysqli_query($conn,$sql_city);
                            while($row_city = mysqli_fetch_object($res_city)){
                                ?>
                                <option value="<?php echo $row_city->city; ?>"><?php echo $row_city->city; ?></option>
                                <?php
                            }
                            ?>
                             
                    </select>
                    <script>
                    $(document).ready(function(){
                        var city = '<?php echo $u_city; ?>' ;
                        $('#cityu option[value='+city+']').attr('selected','selected');
                    })
                    </script>
                    </div>
                      </div>
                      <div id="loc_home" class="col-xs-3 form-group">
                        <div class="ui-widget">
                          <input class="form-control autocomplete" id="location_home" type="text" name="location_home" placeholder="Home Locality" value="<?php echo $u_loc_home; ?>">
                       </div>
                      </div>

                      <div id="loc_work" class="col-xs-3 form-group">
                               <div class="ui-widget">
                          <input class="form-control autocomplete" id="location_work" type="text" name="location_work" placeholder="Work Locality" value="<?php echo $u_loc_work; ?>">
                       </div>
                      </div>
                  </div>
                  <div class="row"></div>
                  <div class="row">
                    <div id="source_u" class="col-xs-5 col-xs-offset-1 form-group">
                        <div class="ui-widget">
                           <select class="form-control" id="source" name="source">
                           <option value="<?php echo $u_source; ?>" selected><?php echo $u_source; ?></option>
                           <?php 
      $sql_sources = "SELECT user_source FROM user_source_tbl WHERE flag='0' AND user_source !='$u_source' ORDER BY  user_source ASC";
      $res_sources = mysqli_query($conn,$sql_sources);
      while($row_sources = mysqli_fetch_object($res_sources)){
        $source_name = $row_sources->user_source;
        ?>
        <option value="<?php echo $source_name; ?>"><?php echo $source_name; ?></option>
        <?php
      }
      ?>
                           </select>
                        </div>
                    </div>

                   <div id="cam" class="col-xs-5 form-group">
                      <div class="ui-widget">
                          <input class="form-control" id="campaign" type="text" name="campaign"  placeholder="Campaign"   value="<?php echo $u_campaign; ?>">
                      </div>
                   </div>
                  </div>
                  <div class="row"></div>
                  <div class="row">

                 <div class="col-xs-2 col-xs-offset-1 form-group">
                    <label> Last Serviced</label></div>
                    <div class="col-xs-3  form-group">
                    <?php 
                    if($u_last_serviced == $today){
                      ?>
                      <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="last_service_date" name="last_service_date"  value="<?php echo date('d-m-Y',strtotime($u_last_serviced)); ?>">
                    <?php }
                    else{
                      ?>
                        <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="last_service_date" name="last_service_date"  value="<?php echo date('d-m-Y',strtotime($u_last_serviced)); ?>" readonly>
                    <?php } ?>                  
                     </div>
                   <div class="col-xs-2 form-group">
                       <label> Next Service On</label>
                   </div>
                   <div class="col-xs-3 form-group">
                      <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="next_service_date" name="next_service_date"  value="<?php echo date('d-m-Y',strtotime($u_next_serviced)); ?>">
                   </div>
                  </div>
                  <div class="row"></div>
                  <div class="row">
              <div class="col-xs-2 col-xs-offset-1 form-group">
                <label> Last Called On</label>
              </div>
              <div class="col-xs-3  form-group">
 <?php 
              if($u_last_called == $today){
                ?>
                <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="last_called_on" name="last_called_on"  value="<?php echo date('d-m-Y',strtotime($u_last_called)); ?>">
             <?php  }
              else{
                ?>
                 <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="last_called_on" name="last_called_on"  value="<?php echo date('d-m-Y',strtotime($u_last_called)); ?>" readonly>
              <?php } ?>              </div>
              <div class="col-xs-2 form-group">
                 <label> FollowUp Date</label>
              </div>
              <div class="col-xs-3  form-group">
                  <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="follow_up_date" name="follow_up_date"  value="<?php echo date('d-m-Y',strtotime($u_followup)); ?>">
              </div>
                  </div>
                  <div class="row"></div>
                  <div class="row">
           <div class="col-xs-10 col-xs-offset-1 form-group">
              <textarea class="form-control" maxlength="100" id="comments" name="comments" placeholder="Comments..." ><?php echo $u_comments; ?></textarea>
          </div>
        </div>
                  <div class="row"></div>
          <div class="row">
           <br>
           <div class="form-group" align="center">
            <input class="form-control" type="submit" id="edit_user_submit" name="edit_user_submit" value="Update" style=" width:90px; background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
          </div>
        </div>
    </form>
</div> <!-- modal body -->
</div> <!-- modal content -->
</div>  <!-- modal dailog -->
</div>  <!-- modal -->
<!-- Add vehicle -->
<!-- Modal -->
<div class="modal fade" id="myModal_vehicle" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Add Vehicle (<?php echo $user_id; ?>)</h3>
</div>
<div class="modal-body" style="height:490px;">
<form id="add_vehicle" class="form" method="post" action="" >
  <div class="row">
        <div class="col-xs-6 col-xs-offset-5 form-group">
          
          <label class="car">
            <button id="veh" class="veh" type="radio" name="veh" value="4w" disabled />
            <img id="car" src="images/car.png">
          </label>
        </div>
      </div>
<div class="row"></div>
<div class="row">
<div id="b_m" class="col-xs-6 col-xs-offset-3 form-group">
       <div class="ui-widget">
         <input class="form-control autocomplete" id="BrandModelid" type="text" name="BrandModelid"  required placeholder="Select Model">
       </div>
</div>
</div>

<div class="row"></div>
<div class="row">
<div class="col-xs-6 col-xs-offset-3 form-group">
<select class="form-control" id="fuel" name="fuel">
<option selected>Fuel Type </option>
<option data-imagesrc="images/bike.png" value="Diesel">Diesel</option>
<option data-imagesrc="images/car.png" value="Petrol">Petrol</option>
</select>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-6 col-xs-offset-3 form-group">
<input class="form-control" type="text" id="regno" name="regno"  data-mask-reverse="true" maxlength="13" placeholder="Vehicle No">
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-6 col-xs-offset-3 form-group">
<input class="form-control" type="text" id="year" name="year" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" placeholder="Year">
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-6 col-xs-offset-3 form-group">
<input class="form-control" type="number" id="km" name="km" min="0" placeholder="Km Driven">
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<br>
<input class="form-control" type="submit" id="vehicle_submit" name="vehicle_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
 </form>
</div>
</div>
</div></div>
<!-- add booking -->
   <!-- Modal -->
<div class="modal fade" id="myModal_booking" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Add Booking(<?php echo $user_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="add_booking" class="form" method="post" action="">
<div class="row"></div>

<div class="row">
<div id="b_m" class="col-xs-5 col-xs-offset-1 form-group">
<select class="form-control" id="veh_no" name="veh_no" required>
<option selected value="">Vehicle</option>
 </select>
</div>

<div class="col-xs-5 form-group" id="service">
<div class="ui-widget">
<select class="form-control" name="tyre_brand" required>
<option selected value="">Select tyre brand</option>
<option value="JK Tyres">JK Tyres</option>
<option value="MRF">MRF</option>
<option value="Good Year">Good Year</option>
<option value="CEAT">CEAT</option>
<option value="Bridgestone">Bridgestone</option>
<option value="Michelin">Michelin</option>
<option value="Apollo">Apollo</option>
<option value="Yokohama">Yokohama</option>
<option value="Nexen">Nexen</option>
 </select>

  </div>
</div>
</div>
<div class="row"></div>

<div class="row">
<div id="b_m" class="col-xs-5 col-xs-offset-1 form-group">
<select class="form-control" id="tyre_count" name="tyre_count" required>
<option selected value="">Select tyre count</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
 </select>
</div>

<div class="col-xs-5 form-group" id="service">
<div class="ui-widget">
<select class="form-control" name="tyre_req_time" required>
<option selected value="">Select requested time</option>
<option value="immediately">Immediately</option>
<option value="in1month">In 1 month</option>
<option value="in3months">In 3 month</option>
 </select>

  </div>
</div>
</div>
<div class="row"></div>

<div class="row">
<div class="col-xs-9 col-xs-offset-1 form-group" id="loc">
        <div class="ui-widget">
          <input class="form-control autocomplete" id="location" type="text" name="location" placeholder="Start typing Location..." required>
        </div>
</div>
        <div><p class="btn btn-sm" id="go" style="margin-left:-8px;background-color:#B2EBF2;">Go</p></div>
</div> 

<div class="row"></div>


<div class="row">
<div class="col-xs-6 col-xs-offset-1 form-group" id="mec">

    <select class="form-control" id="mechanic" name="mechanic">
<option selected>Select Mechanic</option>
</select>
</div>
<input type="hidden" name="mec_id_lead" id="mec_ref">
  <input type="hidden" name="axle_id_lead" id="axle_ref">
  <input type="hidden" name="shopname_ref" id="shopname_ref">
<div class="col-xs-4 form-group">
<input class="form-control" type="number" id="amount" min="1" name="amount" placeholder="Amount">
</div>
</div>

<div class="row"></div>
<div class="row">
<div class="col-xs-3 col-xs-offset-1 form-group">
<label> Potential Purchase date</label></div>
<div class="col-xs-4 form-group">
  <div class='input-group'>
<input class="form-control datepicker" type="text" id="service_date" data-date-format='dd-mm-yyyy' name="service_date" required> 
<span class="input-group-addon">    
          <span class="glyphicon glyphicon-calendar"></span>    
          </span> 
</div>
</div>

<!-- <div class="col-xs-2 form-group">
<label> Next Service Date</label></div>
<div class="col-xs-3  form-group">
<input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="next_service_date" name="next_service_date" required>
</div> -->
</div>

<div class="row"></div>
<div class="row">
<div class="col-xs-4 col-xs-offset-4 form-group">
<!--<label>PickUp</label>
  <input class="form-group" type="radio" id="pickup" name="pickup" value="1" >&nbsp;Yes&nbsp;</input>
  <input class="form-group" type="radio" id="pickup" name="pickup" value="0" checked>&nbsp;No</input>
</div>

<div class="col-xs-4  form-group">-->
<label>SMS</label>
<input class="form-group" type="radio" id="sms" name="sms" value="1" checked>&nbsp;Yes&nbsp;</input>
<input class="form-group" type="radio" id="sms" name="sms" value="0" >&nbsp;No</input></div>
</div>

<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<input class="form-control" type="submit" id="booking_submit" name="booking_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
</form>

</div>

</div>
</div>
</div>

<!-- jQuery library -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- validation -->
<script>
var userinput = document.getElementById('user_name');
userinput.oninvalid = function(event) {
    document.getElementById("user_name").style.borderColor="#E42649";
    event.target.setCustomValidity('Only aplhabets and digits are allowed!');
}
</script>
<!-- user location -->
<script>
$("#location_home").click(function(){
var c = $('#cityu').val();
  //console.log(c);
  if(c==''){
    alert("Plese select City to get localityies!");
  }
  else{
$(function() {
 
          $( "#location_home" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_user_locality.php",
                type: "GET",
                data: {
                    term: request.term,
                    city:$('#cityu').val()
           },
                dataType: 'json'
            }).done(function(data) {
        //alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
          response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location_home").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
     open: function(event, ui) {
            $(".ui-autocomplete").css("position", "absolute");
            $(".ui-autocomplete").css("z-index", "99999");
    },
     change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
       $(this).autocomplete("search");
    });
});

  }
});
 
</script> 
<!-- locality work -->
<script>
$("#location_work").click(function(){
var c = $('#cityu').val();
  //console.log(c);
  if(c==''){
    alert("Plese select City to get localityies!");
  }
  else{
$(function() {
          $( "#location_work" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_user_locality.php",
                type : "GET",
                data: {
                    term: request.term,
                    city:$('#cityu').val()
           },
                dataType: 'json'
            }).done(function(data) {
        //alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
          response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location_work").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
     open: function(event, ui) {
            $(".ui-autocomplete").css("position", "absolute");
            $(".ui-autocomplete").css("z-index", "99999");
    },
     change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
       $(this).autocomplete("search");
    });
});
  }
});
</script> 
<!-- empty autocompletes localities on city change -->
<script>
  $(document).ready(function(){
    $('#cityu').change(function(){
      $('#location_work').empty();
      $('#location_work').autocomplete('close').val('');
      $('#location_home').empty();
      $('#location_home').autocomplete('close').val('');
    });
  })
  </script>
<!-- brand and model -->
 <script>
$(function() {
        $( "#BrandModelid" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_brandmodel.php",
                data: {
                    term: request.term,
          extraParams:$('#veh:checked').val()
                },
                dataType: 'json'
            }).done(function(data) {
        console.log(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
          response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#BrandModelid").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
     open: function(event, ui) {
            $(".ui-autocomplete").css("position", "absolute");
            $(".ui-autocomplete").css("z-index", "9999999");
    },
     change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
       $(this).autocomplete("search");
    });
});

</script>
<!-- brand and model  in booking -->
 <script>
$(function() {
        $( "#model" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_brandmodel.php",
                data: {
                    term: request.term,
          extraParams:$('#veh_b:checked').val()
                },
                dataType: 'json'
            }).done(function(data) {
        console.log(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
          response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#model").next(),
        delay: 1,
        minLength: 1,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
     open: function(event, ui) {
            $(".ui-autocomplete").css("position", "absolute");
            $(".ui-autocomplete").css("z-index", "9999999");
    },
     change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
       $(this).autocomplete("search");
    });
});

</script>

<!-- reg number -->
<script>
Inputmask("A{2}/9{2}/A{2}/9{4}").mask($("#regno"));

</script>
<!-- select Vehicle Number -->
<script>
$(document).ready(function($) {
 $(document).on('change', '[name="veh_b"]' , function(){
    var selectvalue = $('[name="veh_b"]:checked').val();
  var selectuser = "<?php echo $user_id; ?>";
 // console.log(selectuser);
  $("#veh_no").empty();
     $("div.veh").show();
          //Make AJAX request, using the selected value as the POST
      $.ajax({
            url : "ajax/get_veh.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selectvalue": selectvalue , "selectuser" : selectuser},
            success : function(data) {
        // alert(data);
                $('#veh_no').append(data);
                $("#location").autocomplete('close').val('');
                 $("#service_type").autocomplete('close').val('');
                  $("#mechanic").empty();
                  $("#mechanic").html('<option value="">Select Mechanic</option>');

            },
          error: function (xhr, ajaxOptions, thrownError) {
          //  alert(xhr.status + " "+ thrownError);
          }

        });

    });
});
</script>
<!-- select vehicle on page loaad -->
<script>
$(document).ready(function($) {
    var selectvalue = $('[name="veh_b"]:checked').val();
  var selectuser = <?php echo $user_id; ?>;
  $("#veh_no").empty();
     $("div.veh").show();
          //Make AJAX request, using the selected value as the POST
      $.ajax({
            url : "ajax/get_veh.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selectvalue": selectvalue , "selectuser" : selectuser},
            success : function(data) {
        // alert(data);
                $('#veh_no').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
          //  alert(xhr.status + " "+ thrownError);
          }

        });

});
</script>
<!-- select Service type -->
<!-- select Service type -->
 <script>
$(function() {
          $( "#service_type" ).autocomplete({
          
source: function(request, response) {
            $.ajax({
                url: "ajax/get_service_type.php",
                data: {
                    term: request.term,
          //type : $('#veh_b:checked').val()
            type : $('#veh_b:checked').val(),
          vtype: $('#veh_no').val(),
           },
                dataType: 'json'
            }).done(function(data) {
        //alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
          response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#service_type").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
     open: function(event, ui) {
            $(".ui-autocomplete").css("position", "absolute");
            $(".ui-autocomplete").css("z-index", "99999");
    },
     change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
       $(this).autocomplete("search");
    });
});

</script>
<!-- select Location of service center -->
 <script>
$(function() {
          $( "#location" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_location.php",
                data: {
                    term: request.term,
          //type : $('#veh_b:checked').val()
          vtype: $('[name="veh_b"]:checked').val(),
          service:$('#service_type').val()
           },
                dataType: 'json'
            }).done(function(data) {
        //alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
          response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
     open: function(event, ui) {
            $(".ui-autocomplete").css("position", "absolute");
            $(".ui-autocomplete").css("z-index", "99999");
    },
     change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
       $(this).autocomplete("search");
    });
});

</script>

<!-- select Mechanic - ->
<script>
$(document).ready(function($) {
 $(document).on('change', '[name="veh_b"]' , function(){
    var selecttype = $('[name="veh_b"]:checked').val();
  //  var selectservice = $("#service_type").val();
//  var selecttype = $("#veh_b").val();
  $("#mechanic").empty();
     $("div.mec").show();
          //Make AJAX request, using the selected value as the POST
      $.ajax({
            url : "ajax/get_mechanic.php",  // create a new php page to handle ajax request
            type : "POST",
         //   data : {"selectservice": selectservice , "selecttype": selecttype},
            data : {"selecttype": selecttype},
            success : function(data) {
      //  console.log(data);
        //alert(data);
                $('#mechanic').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError); 
          }

        });

    });
});
</script> -->
<!-- select Mechanic on page load- - >
<script>
$(document).ready(function($) {
    var selecttype = $('[name="veh_b"]:checked').val();
  //  var selectservice = $("#service_type").val();
//  var selecttype = $("#veh_b").val();
  $("#mechanic").empty();
     $("div.mec").show();
          //Make AJAX request, using the selected value as the POST
      $.ajax({
            url : "ajax/get_mechanic.php",  // create a new php page to handle ajax request
            type : "POST",
         //   data : {"selectservice": selectservice , "selecttype": selecttype},
            data : {"selecttype": selecttype},
            success : function(data) {
      //  console.log(data);
        //alert(data);
                $('#mechanic').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
          //  alert(xhr.status + " "+ thrownError); 
          }

        });

});
</script>-->


<!-- (old)select Mechanic Based On Location - ->
<script>
$(document).ready(function($) {
 $("#location").on("autocompletechange", function(){
   console.log("ad");
    var selectservice = $("#service_type").val();
  var selecttype = $("#veh_b").val();
  var selectloc = $("#location").val();
  $("#mechanic").empty();
     $("div.mec").show();
          //Make AJAX request, using the selected value as the POST
      $.ajax({
            url : "ajax/get_mechanic_loc.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selectservice": selectservice , "selecttype": selecttype , "selectloc" : selectloc },
            success : function(data) {
        //console.log(data);
        //alert(data);
                $('#mechanic').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

    });
});
</script> -->
<!-- select Mechanic Based On Location - ->
<script>
$(document).ready(function($) {
 $("#location").on("autocompletechange", function(){
  //  var selectservice = $("#service_type").val();
  var selecttype = $('[name="veh_b"]:checked').val();
  var selectloc = $("#location").val();
  $("#mechanic").empty();
     $("div.mec").show();
          //Make AJAX request, using the selected value as the POST
      $.ajax({
            url : "ajax/get_mechanic_loc.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc },
            success : function(data) {
        //console.log(data);
        //alert(data);
                $('#mechanic').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

    });
});
</script>-->
<!-- select Mechanic Based On Location -->
<script>
$(document).ready(function($) {
 $("#location").on("autocompletechange", function(){
  var selectloc = $("#location").val();
  var book_id = $("#book_id").val();
  $("#mechanic").empty();
      $("div.mec").show();
          //Make AJAX request, using the selected value as the POST
      $.ajax({
            url : "ajax/get_mec_location.php",  // create a new php page to handle ajax request
            type : "POST",
            data : { "selectloc" : selectloc ,"book_id" : book_id },
            success : function(data) {
        //console.log(data);
        //alert(data);
                $('#mechanic').html(data.shopname);
                //alert(data.shopname);
                $('#mec_ref').val(data.mec_ref);
                $('#axle_ref').val(data.axle_ref);
                $('#shopname_ref').val(data.shopname_ref);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

    });
});
</script>


<script>
 var date = new Date();
date.setDate(date.getDate());

$('.datepicker').datepicker({
    autoclose: true,
    startDate: date
});
if($('input.datepicker').val()){
    $('input.datepicker').datepicker('setDate', 'today');
 }
  </script>

 
 <!-- select veh before fetching models -->
 <script>
 $(document).ready(function(){
   $("#BrandModelid").click(function(){
     var veh = $('input[name=veh]:checked').val();
     //console.log(veh);
     if(veh == null){
       //console.log(veh);
       alert("Please select vehicle type to get vehicle models!");
     }
   });
 })
 </script>
 <script>
 $(document).ready(function(){
$('input[name=veh]').change(function(){
  $("#BrandModelid").val("");
  $("#location").autocomplete('close').val('');
  $("#mechanic").empty();
  $("#mechanic").html('<option value="">Select Mechanic</option>');
});
 });
 
 $(document).ready(function(){
$('#veh_no').change(function(){
  $("#BrandModelid").val("");
  $("#service_type").val("");
  $("#location").val("");
  $("#location").autocomplete('close').val('');
  $("#mechanic").val("");
  //$("#mechanic").append('<option value="">Select Mechanic</option>');
});
 });
 
 $(document).ready(function(){
$('#service_type').change(function(){
  $("#location").val("");
  $("#location").autocomplete('close').val('');
  $("#mechanic").val("");
  //$("#mechanic").append('<option value="">Select Mechanic</option>');
});
 });
 
 </script>
 <!-- check if locality is empty -->
<script>
$(document).ready(function() {
  $("#mechanic").click(function(){
    if($("#location").val()== ''){
      $("#mechanic").empty();
      $("#mechanic").append('<option value="">Select Mechanic</option>');
      alert("Please select a Location to get mechanics!!!");
    }
  });
});
</script>
<script>
$(document).ready(function($) {
  $("#go").click(function(){
    if($("#location").val()== ''){
      alert("Oops no location has been selected!!!");
    }
  });
});
</script>
</body>
</html>

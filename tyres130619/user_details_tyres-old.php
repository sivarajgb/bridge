<?php
 include("sidebar.php");

$conn = db_connect1();
// login or not
 if((empty($_SESSION['crm_log_id']))) {
  if ($_SESSION['crm_log_id']!='crm146' && $_SESSION['crm_log_id']!='crm195' && $_SESSION['crm_log_id']!='crm182' && $_SESSION['crm_log_id']!='crm201' && $_SESSION['crm_log_id']!='crm202') {
 	header('location:logout.php');
 	die();
 }
 }
$today = date('Y-m-d');
$crm_log_id = $_SESSION['crm_log_id'];
$booking_id = base64_decode($_GET['bi']);
// $type = base64_decode($_GET['t']);

$page_booking_id = $booking_id;


if($booking_id == ''){
  header('location:somethingwentwrong.php');
	die();
}
?>

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
  <title>GoBumpr Bridge</title>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.0.1/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" crossorigin="anonymous">


<!-- auto complete -->
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/jquery-ui.structure.min.css">
<link rel="stylesheet" href="css/jquery-ui.theme.min.css">
<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>
<!-- date time picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.0.1/js/bootstrap-switch.js"></script>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="css/bootstrap-datetimepicker-standalone.css">
<style>
span.make-switch.switch-radio {
    float: left;
}
.bootstrap-switch-container
{
	height:30px;
}
</style>
  <style>
  
			
			.socket{
				display:none;
				width: 57%;
				height: 57%;
				position: absolute;
				left: 37%;
				margin-left: -17%;
				top: 33%;
				margin-top: -17%;
			}
			
			.hex-brick{
			  background: #FFA800;
			  width: 30px;
			  height: 17px;
			  position: absolute;
			  top: 5px;
        animation-name: fade;
				animation-duration: 2s;
				animation-iteration-count: infinite;
				-webkit-animation-name: fade;
				-webkit-animation-duration: 2s;
				-webkit-animation-iteration-count: infinite;
			}
			
			.h2{
				transform: rotate(60deg);
				-webkit-transform: rotate(60deg);
			}
			
			.h3{
				transform: rotate(-60deg);
				-webkit-transform: rotate(-60deg);
			}
			
			.gel{
				height: 30px;
				width: 30px;	
				transition: all .3s;
				-webkit-transition: all .3s;
				position: absolute;
        top: 50%;
        left: 50%;
			}
			
			.center-gel{
				margin-left: -15px;
				margin-top: -15px;
				
				animation-name: pulse;
				animation-duration: 2s;
				animation-iteration-count: infinite;
				-webkit-animation-name: pulse;
				-webkit-animation-duration: 2s;
				-webkit-animation-iteration-count: infinite;
			}
			
			.c1{
				margin-left: -47px;
				margin-top: -15px;
			}
			
			.c2{
				margin-left: -31px;
				margin-top: -43px;
			}
			
			.c3{
				margin-left: 1px;
				margin-top: -43px;
			}
			
			.c4{
				margin-left: 17px;
				margin-top: -15px;
			}
			.c5{
				margin-left: -31px;
				margin-top: 13px;
			}
			
			.c6{
				margin-left: 1px;
				margin-top: 13px;
			}
			
			.c7{
				margin-left: -63px;
				margin-top: -43px;
			}
			
			.c8{
				margin-left: 33px;
				margin-top: -43px;
			}
			
			.c9{
				margin-left: -15px;
				margin-top: 41px;
			}
			
			.c10{
				margin-left: -63px;
				margin-top: 13px;
			}
			
			.c11{
				margin-left: 33px;
				margin-top: 13px;
			}
			
			.c12{
				margin-left: -15px;
				margin-top: -71px;
			}
			
			.c13{
				margin-left: -47px;
				margin-top: -71px;
			}
			
			.c14{
				margin-left: 17px;
				margin-top: -71px;
			}
			
			.c15{
				margin-left: -47px;
				margin-top: 41px;
			}
			
			.c16{
				margin-left: 17px;
				margin-top: 41px;
			}
			
			.c17{
				margin-left: -79px;
				margin-top: -15px;
			}
			
			.c18{
				margin-left: 49px;
				margin-top: -15px;
			}
			
			.c19{
				margin-left: -63px;
				margin-top: -99px;
			}
			
			.c20{
				margin-left: 33px;
				margin-top: -99px;
			}
			
			.c21{
				margin-left: 1px;
				margin-top: -99px;
			}
			
			.c22{
				margin-left: -31px;
				margin-top: -99px;
			}
			
			.c23{
				margin-left: -63px;
				margin-top: 69px;
			}
			
			.c24{
				margin-left: 33px;
				margin-top: 69px;
			}
			
			.c25{
				margin-left: 1px;
				margin-top: 69px;
			}
			
			.c26{
				margin-left: -31px;
				margin-top: 69px;
			}
			
			.c27{
				margin-left: -79px;
				margin-top: -15px;
			}
			
			.c28{
				margin-left: -95px;
				margin-top: -43px;
			}
			
			.c29{
				margin-left: -95px;
				margin-top: 13px;
			}
			
			.c30{
				margin-left: 49px;
				margin-top: 41px;
			}
			
			.c31{
				margin-left: -79px;
				margin-top: -71px;
			}
			
			.c32{
				margin-left: -111px;
				margin-top: -15px;
			}
			
			.c33{
				margin-left: 65px;
				margin-top: -43px;
			}
			
			.c34{
				margin-left: 65px;
				margin-top: 13px;
			}
			
			.c35{
				margin-left: -79px;
				margin-top: 41px;
			}
			
			.c36{
				margin-left: 49px;
				margin-top: -71px;
			}
			
			.c37{
				margin-left: 81px;
				margin-top: -15px;
			}
			
			.r1{
				animation-name: pulse;
				animation-duration: 2s;
				animation-iteration-count: infinite;
				animation-delay: .2s;
				-webkit-animation-name: pulse;
				-webkit-animation-duration: 2s;
				-webkit-animation-iteration-count: infinite;
				-webkit-animation-delay: .2s;
			}
			
			.r2{
				animation-name: pulse;
				animation-duration: 2s;
				animation-iteration-count: infinite;
				animation-delay: .4s;
				-webkit-animation-name: pulse;
				-webkit-animation-duration: 2s;
				-webkit-animation-iteration-count: infinite;
				-webkit-animation-delay: .4s;
			}
			
			.r3{
				animation-name: pulse;
				animation-duration: 2s;
				animation-iteration-count: infinite;
				animation-delay: .6s;
				-webkit-animation-name: pulse;
				-webkit-animation-duration: 2s;
				-webkit-animation-iteration-count: infinite;
				-webkit-animation-delay: .6s;
			}
			
			.r1 > .hex-brick{
				animation-name: fade;
				animation-duration: 2s;
				animation-iteration-count: infinite;
				animation-delay: .2s;
				-webkit-animation-name: fade;
				-webkit-animation-duration: 2s;
				-webkit-animation-iteration-count: infinite;
				-webkit-animation-delay: .2s;
			}
			
			.r2 > .hex-brick{
				animation-name: fade;
				animation-duration: 2s;
				animation-iteration-count: infinite;
				animation-delay: .4s;
				-webkit-animation-name: fade;
				-webkit-animation-duration: 2s;
				-webkit-animation-iteration-count: infinite;
				-webkit-animation-delay: .4s;
			}
			
			.r3 > .hex-brick{
				animation-name: fade;
				animation-duration: 2s;
				animation-iteration-count: infinite;
				animation-delay: .6s;
				-webkit-animation-name: fade;
				-webkit-animation-duration: 2s;
				-webkit-animation-iteration-count: infinite;
				-webkit-animation-delay: .6s;
			}
			
			
			@keyframes pulse{
				0%{
					-webkit-transform: scale(1);
					transform: scale(1);
				}
				
				50%{
					-webkit-transform: scale(0.01);
					transform: scale(0.01);
				}
				
				100%{
					-webkit-transform: scale(1);
					transform: scale(1);
				}
			}
			
			@keyframes fade{
				0%{
					background: #FFA800;
				}
				
				50%{
					background: #f9cc70;
				}
				
				100%{
					background: #FFA800;
				}
			}
			
			@-webkit-keyframes pulse{
				0%{
					-webkit-transform: scale(1);
					transform: scale(1);
				}
				
				50%{
					-webkit-transform: scale(0.01);
					transform: scale(0.01);
				}
				
				100%{
					-webkit-transform: scale(1);
					transform: scale(1);
				}
			}
			
			@-webkit-keyframes fade{
				0%{
					background: #FFA800;
				}
				
				50%{
					background: #ffc759;
				}
				
				100%{
					background: #FFA800;
				}
			}
  /*.loader_overlay{
	display:none;
    height: 100%;
    width: 100%;
    margin-top: -113%;
    background: rgba(221, 221, 221,15);
    opacity: 0.5;
  }
  .loader {
	display:none;
	margin-left: 40%;
    position: fixed;
    margin-top: -65%;
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 90px;
    height: 90px;
    animation: spin 0.5s linear infinite;
	z-index:9999;
}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}*/



  nav,ol{
	  background: #009688 !important;
	  font-size:18px;
	   margin-top: -4px;
  }
   .navbar-fixed-top {
    top: 0;
    border-width: 0 0 0px;
  }
  body{
	  background: #fff !important;
	  color:black;
  }
	<!-- auto complete -->
	@charset "utf-8";.ui-autocomplete{z-index:1000 !important;cursor:default;list-style:none;}
	.ui-widget{}
	.ui-autocomplete{overflow-y:auto;overflow-x:hidden;}
	.ui-menu{width:0px;display:none;}
	.ui-autocomplete > li{padding:10px;padding-left:10px;}
	ul{margin-bottom:0;}
	.ui-autocomplete > li.ui-state-focus{background-color:#DDD;}
	.ui-autocomplete .ui-front .ui-menu .ui-widget .ui-widget-content{border:0;}
	.ui-helper-hidden-accessible{display:none;}
	.gobumpr-icon{font-style:normal;font-weight:normal;speak:none;display:inline-block;text-decoration:inherit;text-align:center;font-variant:normal;text-transform:none;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
	.ui-widget{background-color:white;width:100%;}
	.ui-widget-content{padding-left:1px;display:block;width:20px;position:relative;line-height:12px;max-height:210px;border:.5px solid #DADADA;}
	.ui-widget{}
	.ui-autocomplete { position: absolute; cursor: default;}


<!-- vehicle type -->
.bike,
.car{
  cursor: pointer;
  user-select: none;
  -webkit-user-select: none;
  -webkit-touch-callout: none;
}
.bike > input,
.car > input{ /* HIDE ORG RADIO & CHECKBOX */
  visibility: hidden;
  position: absolute;
}
/* RADIO & CHECKBOX STYLES */
.bike > i,
.car > i{     /* DEFAULT <i> STYLE */
  display: inline-block;
  vertical-align: middle;
  width:  16px;
  height: 16px;
  border-radius: 50%;
  transition: 0.2s;
  box-shadow: inset 0 0 0 8px #fff;
  border: 1px solid gray;
  background: gray;
}

label > input{ /* HIDE RADIO */
  visibility: hidden; /* Makes input not-clickable */
  position: absolute; /* Remove input from document flow */
}
label > input + img{ /* IMAGE STYLES */
  cursor:pointer;
  border:2px solid transparent;
}
label > input:checked + img { /* (RADIO CHECKED) IMAGE STYLES */
   border-radius:12px;
   padding:5px;
   background-color:#ffa800;
  box-shadow: 0 0 3px 0 #394;
}
.borderless td, .borderless th {
    border: none !important;
}
	/* anchor tags */
  a{
	  text-decoration:none;
	  color:black;
  }
   a:hover{
	  text-decoration:none;
	  color:#4B436A;
  }
    .datepicker {
	  cursor:pointer;
  }
.datepicker:before {
content: '';
display: inline-block;
border-left: 7px solid transparent;
border-right: 7px solid transparent;
border-bottom: 7px solid #ccc;
border-bottom-color: transparent !important;
position: absolute;
top: -7px;
left: 190px;  // I made a change here 
}

.datepicker:after {
content: '';
display: inline-block;
border-left: 6px solid transparent;
border-right: 6px solid transparent;
border-top-color: transparent !important;
border-top: 6px solid #ffffff;
position: absolute;
bottom: -6px;
left: 191px;  // I made a change here 
}

#datepick > span:hover{cursor: pointer;}

 .floating-box {
	 display: inline-block;
	 margin: 22px;
 padding: 22px;
 width:203px;
 height:105px;
 box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  font-size: 17px;
}

/* vertical menu */
/* Mixin */
@mixin vertical-align($position: relative) {
  position: $position;
  top: 50%;
  -webkit-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}

.ver_menu {
  @include vertical-align();
}

select{
    color: red;
}
option{
  height: 25px;
}
option:hover {
  box-shadow: 0 0 10px 100px #ddd inset;
}
</style>

</head>
<body>
 <?php include_once("header.php"); ?> 
<script>
$(document).ready(function(){
  $('#city').hide();
})
</script>
 <div class="overlay" data-sidebar-overlay></div>
 <div class="navbar-fixed-top" style=" margin-top:4%;border-top:52px;padding-top:5px;background-color:#fff;z-index:1037;">

<p class="col-lg-offset-1 col-sm-offset-1" style="background-color:#ffa800;padding:7px;width:30px;border-radius:18px;float:left;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p><p style="width:150px;padding:6px;float:left;">New Lead</p>
<p style="background-color:#69ED85;padding:7px;width:30px;border-radius:18px;float:left;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p><p style="width:150px;padding:6px;float:left;">Lead sent to garage</p>
<p style="background-color:#D25F34;padding:7px;width:30px;border-radius:18px;float:left;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p><p style="width:150px;padding:6px;float:left;">Bid Placed</p>
<p style="background-color:#8C7272;padding:7px;width:30px;border-radius:18px;float:left;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p><p style="width:150px;padding:6px;float:left;">Bid Placed By All</p>
<p style="background-color:#6FA3DE;padding:7px;width:30px;border-radius:18px;float:left;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p><p style="width:150px;padding:6px;float:left;">Bid Accepted</p>


</div>

<div class="padding"></div>
  <?php
    // get user id  from user_booking_table
       $sql_user_bk = "SELECT tb.user_id,tb.locality,u.name,u.mobile_number,u.mobile_number2,u.email_id,tb.user_vech_no,tb.tyre_brand,v.brand,v.model from tyre_booking_tb as tb  LEFT JOIN user_register as u ON u.reg_id=tb.user_id LEFT JOIN user_vehicle_table as v ON tb.user_veh_id=v.id Where tb.booking_id='$page_booking_id' group by tb.booking_id ";
      $res_user_bk = mysqli_query($conn,$sql_user_bk);
      $row_user_bk = mysqli_fetch_object($res_user_bk);

      $user_id = $row_user_bk->user_id;
      $locality=$row_user_bk->locality;
      $name = $row_user_bk->name;
      $mobile_number = $row_user_bk->mobile_number;
      $mobile_number2 = $row_user_bk->mobile_number2;
	  $email = $row_user_bk->email_id;
	  $vehicle_no = $row_user_bk->user_vehicle_no;
	  $tyre_brand = $row_user_bk->tyre_brand;
	  $brand = $row_user_bk->brand;
	  $model = $row_user_bk->model;
	  

  ?>
  <div  id="user" style=" margin-left:20px;border:2px solid #708090; border-radius:8px; width:25%;height:470px; padding:10px; margin-top:60px; float:left;">
    <table id="table1" class="table borderless">
       <tr><td><strong>User Id</strong></td><td><?php echo $user_id; ?></td></tr>
       <tr><td><strong>Name</strong></td><td><?php echo $name; ?></td></tr>
       <tr><td><strong>Phn No</strong></td><td><?php echo $mobile_number; ?></td></tr>
       <tr><td><strong>Alt Phn No</strong></td><td><?php echo $mobile_number2; ?></td></tr>
       <tr><td><strong>E-mail</strong></td><td><?php echo $email; ?></td></tr>
       <tr><td><strong>Veh No</strong></td> <td><?php echo $vehicle_no; ?></td></tr>
       <tr><td><strong>Tyre Brand</strong></td> <td><?php echo $tyre_brand; ?></td></tr>
        <tr><td><strong>Brand</strong></td> <td><?php echo $brand; ?>&nbsp<?php echo $model; ?></td></tr>
       <tr><td><strong>Current Location<strong></td><td><?php echo $locality; ?></td></tr>
  </table>
<!-- Edit User -->
<div  style="float:left; margin-left:30px;">
   <div id="edting_user" style="display:inline-block; align-items:center;" >
       <!-- Trigger the modal with a button -->
       <button id="eu" type="button" class="btn btn-sm" data-toggle="modal" data-target="#myModal_user" style="background-color:rgb(156, 197, 202);"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;Edit User</button>

       <!-- Modal -->
       <div class="modal fade" id="myModal_user" role="dialog" >
         <div class="modal-dialog" style="width:860px;">

          <!-- Modal content-->
          <div class="modal-content">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Edit User</h3>
             </div>
             <div class="modal-body">
								<?php
								$sql_edit_user ="SELECT name,mobile_number,mobile_number2,email_id,City,Locality_Home,Locality_Work,source,campaign,Last_service_date,Next_service_date,Last_called_on,Follow_up_date,comments FROM user_register WHERE reg_id='$user_id'";
								$res_edit_user = mysqli_query($conn, $sql_edit_user);
								$row_edit_user = mysqli_fetch_object($res_edit_user);
								$u_name = $row_edit_user->name;
								$u_mobile = $row_edit_user->mobile_number;
								$u_alt_mobile = $row_edit_user->mobile_number2;
								$u_mail  = $row_edit_user->email_id;
								$u_city  = $row_edit_user->City;
								$u_loc_home  = $row_edit_user->Locality_Home;
								$u_loc_work  = $row_edit_user->Locality_Work;
								$u_source = $row_edit_user->source;
								$u_campaign = $row_edit_user->campaign;
								$u_last_serviced  = $row_edit_user->Last_service_date;
								$u_next_serviced  = $row_edit_user->Next_service_date;
								$u_last_called  = $row_edit_user->Last_called_on;
								$u_followup  = $row_edit_user->Follow_up_date;
								$u_comments  = $row_edit_user->comments;
								?>
								<form id="edit_user" class="form" method="post" action="edit_user.php">
                  <div class="row">
                       <br>
                       <div class="col-xs-4 col-xs-offset-1 form-group">
                         <input class="form-control" type="text" id="mobile" name="mobile" placeholder="Mobile" readonly maxlength="10" value="<?php echo $u_mobile; ?>">
                       </div>
					   
					   <div class="col-xs-4 form-group">
						 <input class="form-control" type="text" id="mobile2" name="mobile2" placeholder="Alternate Mobile" maxlength="10" value="<?php echo $u_alt_mobile; ?>">
						 </div>
						 </div>
						 
						 <div class="row">

                      <div class="col-xs-5 col-xs-offset-1 form-group">
                          <input  class="form-control" type="text" id="user_name" name="user_name"  pattern="^[a-zA-Z0-9\s]+$" onchange="try{setCustomValidity('')}catch(e){}" placeholder="User Name" required  value="<?php echo $u_name; ?>">
                      </div>

                      <div class="col-xs-5 form-group">
                         <input type="email" class="form-control" id="email" name="email" placeholder="E-Mail"  value="<?php echo $u_mail; ?>">
                      </div>
                  </div>
                  <div class="row"></div>
                  <div class="row">
                      <div id="city_u" class="col-xs-4 col-xs-offset-1 form-group">
			                    <div class="ui-widget">
                             <select class="form-control" id="cityu" name="city" required>
                             <?php

                              $sql_city = "SELECT DISTINCT city FROM go_bumpr.localities ORDER BY city ASC";
                              $res_city = mysqli_query($conn,$sql_city);
                              while($row_city = mysqli_fetch_object($res_city)){
                                  ?>
                                  <option value="<?php echo $row_city->city; ?>"><?php echo $row_city->city; ?></option>
                                  <?php
                              }
                             ?>

                             </select>
							                 <script>
                               $(document).ready(function(){
                                 var city = '<?php echo $u_city; ?>' ;
                                 $('#cityu option[value='+city+']').attr('selected','selected');
                               })
                               </script>
			                    </div>
                      </div>
                      <div id="loc_home" class="col-xs-3 form-group">
                        <div class="ui-widget" id="loc_home">
        	                <input class="form-control autocomplete" id="location_home" type="text" name="location_home" placeholder="Home Locality" value="<?php echo $u_loc_home; ?>">
                       </div>
                      </div>

                      <div id="loc_work" class="col-xs-3 form-group">
                          <div class="ui-widget" id="loc_work">
        	                <input class="form-control autocomplete" id="location_work" type="text" name="location_work" placeholder="Work Locality" value="<?php echo $u_loc_work; ?>">
                       </div>
                      </div>
									</div>
                  <div class="row"></div>
                  <div class="row">
                    <div id="source_u" class="col-xs-5 col-xs-offset-1 form-group">
			                  <div class="ui-widget">
                           <select class="form-control" id="source" name="source">
                           <option value="<?php echo $u_source; ?>" selected><?php echo $u_source; ?></option>
                           <?php 
      $sql_sources = "SELECT user_source FROM user_source_tbl WHERE flag='0' AND user_source !='$u_source' ORDER BY  user_source ASC";
      $res_sources = mysqli_query($conn,$sql_sources);
      while($row_sources = mysqli_fetch_object($res_sources)){
        $source_name = $row_sources->user_source;
        ?>
        <option value="<?php echo $source_name; ?>"><?php echo $source_name; ?></option>
        <?php
      }
      ?>
                           </select>
			                  </div>
                    </div>

                   <div id="cam" class="col-xs-5 form-group">
			                <div class="ui-widget">
				                  <input class="form-control" id="campaign" type="text" name="campaign"  placeholder="Campaign"   value="<?php echo $u_campaign; ?>">
			                </div>
                   </div>
							    </div>
                  <div class="row"></div>
                  <div class="row">

                 <div class="col-xs-2 col-xs-offset-1 form-group">
                    <label> Last Serviced</label></div>
                    <div class="col-xs-3  form-group">
                    <?php
                    if($u_last_serviced == $today){
                      ?>
                      <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="last_service_date" name="last_service_date"  value="<?php echo date('d-m-Y',strtotime($u_last_serviced)); ?>">
                    <?php }
                    else{
                      ?>
                        <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="last_service_date" name="last_service_date"  value="<?php echo date('d-m-Y',strtotime($u_last_serviced)); ?>" readonly>
                    <?php } ?>
                   </div>
                   <div class="col-xs-2 form-group">
                       <label> Next Service On</label>
									 </div>
                   <div class="col-xs-3  form-group">
                      <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="next_service_date" name="next_service_date"  value="<?php echo date('d-m-Y',strtotime($u_next_serviced)); ?>">
                   </div>
                  </div>
                  <div class="row"></div>
                  <div class="row">
              <div class="col-xs-2 col-xs-offset-1 form-group">
                <label> Last Called On</label>
              </div>
              <div class="col-xs-3  form-group">
              <?php
              if($u_last_called == $today){
                ?>
                <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="last_called_on" name="last_called_on"  value="<?php echo date('d-m-Y',strtotime($u_last_called)); ?>">
             <?php  }
              else{
                ?>
                 <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="last_called_on" name="last_called_on"  value="<?php echo date('d-m-Y',strtotime($u_last_called)); ?>" readonly>
              <?php } ?>
              </div>
              <div class="col-xs-2 form-group">
                 <label> FollowUp Date</label>
							</div>
              <div class="col-xs-3  form-group">
                  <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="follow_up_date" name="follow_up_date"  value="<?php echo date('d-m-Y',strtotime($u_followup)); ?>">
              </div>
                  </div>
                  <div class="row"></div>
                  <div class="row">
           <div class="col-xs-10 col-xs-offset-1 form-group">
              <textarea class="form-control" maxlength="100" id="comments" name="comments" placeholder="Comments..." ><?php echo $u_comments; ?></textarea>
          </div>
        </div>
                  <div class="row"></div>
          <div class="row">
           <br>
           <div class="form-group" align="center">
            <input class="form-control" type="submit" id="edit_user_submit" name="edit_user_submit" value="Update" style=" width:90px; background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
          </div>
				</div>
                   <input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
									 <input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
								   <input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
                   <input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
                 </form>
  </div> <!-- modal body -->
  </div> <!-- modal content -->
  </div>  <!-- modal dailog -->
  </div>  <!-- modal -->

  </div>
</div> <!-- edit user -->
<div  style="float:left; margin-left:30px;">
<!-- Add Vehicle -->
<!-- Trigger the modal with a button -->
<div id="edting_vehicle" style="display:inline-block; align-items:center;" >
<button data-bid="<?php echo $booking_id; ?>" type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_vehicle" style="background-color:rgb(156, 197, 202);"><i class="fa fa-car" aria-hidden="true"></i>&nbsp;&nbsp;Add Vehicle</button>
</div>
</div>
</div>

<!-- table 1 -->

<div  id="box1" style="float:left;margin-top:60px; margin-left:15px; border:2px solid #4CB192; border-radius:5px;width:43%;height:470px;">
<!--<h3 style="padding-left: 10px;"> Booking Id:<?php echo $booking_id; ?></h3>-->
<div style="  width:97%; height:450px; overflow-y:auto; margin: 10px;">
  <table id="tab" class="table table-bordered table-responsive " style="border-collapse:collapse; ">
	<thead style="background-color: #D3D3D3;">
	 <th>Service centre</th>
	  <th>Activity</th> 
	 <th>Tyre brand</th>
	 <th>Tyre count</th>
	 <th>Vehicle Brand</th>
	 <th>Quote</th>
	 <th>Timestamp</th>
	 </thead>
	 <tbody>
	 <?php
	 $sql_booking = "SELECT DISTINCT tb.booking_id,tb.log,bb.b2b_booking_id,bb.b2b_shop_id,bb.b2b_tyre_brand,am.shop_name,tb.tyre_brand,tb.tyre_count,v.brand,v.model,bb.b2b_bid_amt,tb.mec_id_leads,bb.b2b_bid_flg,bb.b2b_acpt_flg FROM tyre_booking_tb as tb LEFT JOIN b2b.b2b_booking_tbl_tyres as bb ON tb.booking_id=bb.gb_booking_id LEFT JOIN b2b.b2b_mec_tbl as bm on bb.b2b_shop_id=bm.b2b_shop_id LEFT JOIN admin_mechanic_table am ON am.axle_id = bm.b2b_shop_id LEFT JOIN user_vehicle_table as v ON tb.user_veh_id=v.id WHERE tb.booking_id='$page_booking_id' AND bb.b2b_shop_id NOT IN (1035,1014,1670,'') ORDER BY tb.log DESC";
// echo $sql_booking;
$res_booking = mysqli_query($conn,$sql_booking);
$count = mysqli_num_rows($res_booking);
if($count >0){
  while($row_booking = mysqli_fetch_object($res_booking)){
    $booking_id = $row_booking->booking_id;
    $log = $row_booking->log;
    $shop_id = $row_booking->b2b_shop_id;
    $shop_name = $row_booking->shop_name;
    $tyre_brand = $row_booking->tyre_brand;
    $b2b_tyre_brand = $row_booking->b2b_tyre_brand;
    $tyre_count = $row_booking->tyre_count;
    $brand = $row_booking->brand;
    $model = $row_booking->model;
    $bid_amt = $row_booking->b2b_bid_amt;
    $b2b_booking_id = $row_booking->b2b_booking_id;    
    $mec_id_leads = $row_booking->mec_id_leads;
    $b2b_bid_flg = $row_booking->b2b_bid_flg;
    $b2b_acpt_flg = $row_booking->b2b_acpt_flg;

     $td2 = '<div class="row">';
     if($b2b_booking_id == ''){
                 $td2 = $td2.'<a href="tyres_goaxle.php?bi='.base64_encode($booking_id).'" style="background-color:#ffa800;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
               }
               else
              {
                if($b2b_acpt_flg==1){
                       $td2 = $td2.'<a style="background-color:#6FA3DE;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>'; 
                      }
                   else if($b2b_bid_flg==1){ 
                      $td2 = $td2.'<a  style="background-color:#D25F34;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
                  }
                  
                  else
                  {
                   $td2 = $td2.'<a style="background-color:#69ED85;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
                  }
                 }
    if(empty($tyre_brand))
    {
    	$tyre_brand = $b2b_tyre_brand;
    }             

	 ?>
	 <td><?php echo $shop_name;?></td>
	 <td><?php echo $td2;?> </td>
	 <td><?php echo $tyre_brand; ?></td>
	 <td><?php echo $tyre_count ; ?></td>
	 <td><?php echo $brand.' '.$model;?></td>
	 
	 <td>Rs:<?php echo $bid_amt; if($b2b_bid_flg==0){ ?> <button type="button" class="btn btn-link edit" data-shopid="<?php echo $shop_id?>" data-toggle="modal" href="#myModal_others"><i class="fa fa-edit"></i></button> </td>
	 <?php } ?>
	 <td><?php echo date('d M Y h.i A', strtotime($log)); ?></td>
	 </tr>
	 <?php
	}
		 }
		 else
		 {
		 	?>  
      <h3 style="text-align: center;padding-top: 40px;"><?php echo ("No results found...!"); ?></h3>
      <script>
      	$("#tab").hide();
      </script>
 <?php  
		 }
		 
		 ?>
	 </tbody>
 </table>
</div>
</div>

<!--table 2-->

<div  id="box2" style="float:left;margin-top:60px; margin-left:15px; border:2px solid #4CB192; border-radius:5px;width:28%;height:470px;">
<!--<h3 style="padding-left: 10px;">History</h3>-->
<div style="  width:94%; height:450px; overflow-y:auto; margin: 10px;overflow-x:auto;overflow:auto !important;">
  <table id="table2" class="table table-bordered table-responsive " style="border-collapse:collapse; ">
	<thead style="background-color: #D3D3D3;">
	 <th>Booking Id</th>
	  <th style="width: 200px;">Activity</th>
	
	 <th>Timestamp</th>
	 </thead>
	 <tbody>
	 <?php
	 $sql_booking = "SELECT tb.booking_id,tb.user_id,tb.log,bb.b2b_shop_id,bb.b2b_booking_id,tb.tyre_brand,tb.tyre_count,v.brand,v.model,bb.b2b_bid_amt,tb.mec_id_leads,bb.b2b_bid_flg,bb.b2b_acpt_flg,sum(bb.b2b_acpt_flg) as acpt_sum FROM tyre_booking_tb as tb LEFT JOIN b2b.b2b_booking_tbl_tyres as bb ON tb.booking_id=bb.gb_booking_id LEFT JOIN user_vehicle_table as v ON tb.user_veh_id=v.id WHERE tb.user_id='$user_id' group by tb.booking_id ORDER BY tb.log DESC";
//echo $sql_booking;
$res_booking = mysqli_query($conn,$sql_booking);
$count = mysqli_num_rows($res_booking);
if($count >0){
  while($row_booking = mysqli_fetch_object($res_booking)){
    $booking_id = $row_booking->booking_id;
    $b2b_booking_id = $row_booking->b2b_booking_id;
    $log = $row_booking->log;
    $shop_id = $row_booking->b2b_shop_id;
    $tyre_brand = $row_booking->tyre_brand;
    $tyre_count = $row_booking->tyre_count;
    $brand = $row_booking->brand;
    $model = $row_booking->model;
    $b2b_booking_id = $row_booking->b2b_booking_id;       
    $bid_amt = $row_booking->b2b_bid_amt;
    $mec_id_leads = $row_booking->mec_id_leads;
    $b2b_bid_flg = $row_booking->b2b_bid_flg;
    $b2b_acpt_flg = $row_booking->b2b_acpt_flg;
    $acpt_flg_sum = $row_booking->acpt_sum;
    $b2b_bid_log=$row_booking->b2b_bid_log;

     $td2 = '<div class="row">';
     if($b2b_booking_id == ''){
                 $td2 = $td2.'<a href="tyres_goaxle.php?bi='.base64_encode($booking_id).'" style="background-color:#ffa800;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
               }
               else
              {
                if($b2b_acpt_flg==1 || $acpt_flg_sum > 0){
                       $td2 = $td2.'<a style="background-color:#6FA3DE;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>'; 
                      }
                   else if($b2b_bid_flg==1){ 
                      $td2 = $td2.'<a  style="background-color:#D25F34;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
                  }
                  
                  else
                  {
                   $td2 = $td2.'<a style="background-color:#69ED85;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
                  }
                 }
      if(empty($tyre_brand))
    {
    	$tyre_brand = 'Any brand';
    }            
	 if($booking_id == $page_booking_id){?>
	<td style="background-color:#adccef;text-align: center;"><?php echo $booking_id;?></br> <a href="user_details_tyres.php?bi=<?php echo base64_encode($booking_id) ?>"><i id="'.$booking_id.'" class="fa fa-eye" aria-hidden="true"></i> </td>
	<td style="background-color:#adccef;padding-left:10px;text-align: left;"><?php echo $td2; ?>   <div style="text-align:justify;padding-left: 50px;"><?php echo $tyre_brand.'*'.$tyre_count.'<br>'.$brand.' '.$model;?> </div></td>
	<td style="background-color:#adccef;"><?php echo date('d M Y h.i A', strtotime($log)); ?></td>
	<?php 
	 }
	 else
	 {
	?>
	 <td style="text-align: center;"><?php echo $booking_id;?></br> <a href="user_details_tyres.php?bi=<?php echo base64_encode($booking_id) ?>"><i id="'.$booking_id.'" class="fa fa-eye" aria-hidden="true"></i> </td>
	 <td style="padding-left:10px;text-align: left;"><?php echo $td2; ?>   <div style="text-align:justify;padding-left: 50px;"><?php echo $tyre_brand.' * '.$tyre_count.'<br>'.$brand.' '.$model;?> </div></td>
	 <td><?php echo date('d M Y h.i A', strtotime($log)); ?></td>
	 <?php 
	 }
	 ?>
	 <!-- <td>Rs:<?php //echo $bid_amt; ?> --> <!-- <button type="button" class="btn btn-link edit" data-shopid="<?php //echo $shop_id?>" data-toggle="modal" href="#myModal_others"><i class="fa fa-edit"></i></button> --> <!-- </td> -->
	 <!-- <td><a href="edit-course.php?id=<?php// echo $row->id;?>"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
<a href="manage-courses.php?del=<?php //echo $row->id;?>" onclick="return confirm("Do you want to delete");"><i class="fa fa-close"></i></a></td> -->
	 </tr>
	 <?php
	}
		 }
		 ?>
	 </tbody>
 </table>
</div>
</div>


<!-- move to  others -->
<!-- Modal -->
<div class="modal fade" id="myModal_others" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Move to Others (Id : <?php echo $user_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="add_others" class="form" method="post" action="add_others.php">
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<br>
<select class="form-control" id="others_status" name="status" required>
<option selected value="">Select Reason</option>
<?php
$sql="SELECT activity FROM admin_activity_tbl WHERE flag='2'";
$query=mysqli_query($conn,$sql);

while ($rows = mysqli_fetch_array($query)){ ?>
<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>
<?php } ?>
</select>
</div>
</div>
<div class="row"></div>

<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<textarea class="form-control" maxlength="1000" id="comments" name="comments" style="min-height:100px;" placeholder="Comments..."></textarea>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<br>
<input class="form-control" type="submit" id="others_submit" name="others_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
</form>
</div>
</div>
</div>
</div>

<!-- cancel booking -->
<div id="cb" style="position:fixed; margin-left:34%; bottom:20px;display:none;">
<button class="btn btn-md" data-bid="<?php echo $booking_id; ?>"  id="cancel" data-toggle="modal" data-target="#myModal_cancel_booking" style="background-color:#FF7043;"><i class="fa fa-ban" aria-hidden="true"></i>&nbsp;&nbsp;Cancel Booking</button>
</div>

<!-- followup booking -->
<div style="position:fixed; margin-left:49%; bottom:20px;"><button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_unlimited_RNR"  style="background-color:#6ed0c7;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;FollowUp</button></div>
</div>

<!-- others booking -->
 <div style="position:fixed; margin-left:60%; bottom:20px;"><button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_others" style="background-color:#D0D854;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Others</button></div>


<!-- edit booking booking -->
<!-- <div id="edb" style="position:fixed;margin-left:62.5%;bottom:20px;display:none;">
<div id="ebook" sytyle="position:fixed; margin-left:62.5%; bottom:20px;display:none;"><button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_edit_booking"  style="background-color:#bfbbf1;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Edit Bookings</button></div>
</div>
 --><!-- <div style="position:fixed;margin-left:81.25%;bottom:20px;">
<div id="addbook" sytyle="position:fixed; margin-left:81.25%; bottom:20px;display:none;"><button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_add_booking"  style="background-color:#B5A5C3;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;New Booking</button></div>
</div> -->

<!-- push others to bookings -->
<!-- <div id="pbook" style="position:fixed; margin-left:55%; bottom:20px;display:none;">
  <button data-bid="<?php echo $booking_id; ?>"  type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_unlimited_RNR"  style="position:relative;background-color:#58da80;width:180px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Push To FollowUp</button>
  <button  data-bid="<?php echo $booking_id; ?>"  type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_add_booking"  style="position:relative;background-color:#39B8AC;width:180px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Push To Bookings</button>
</div>
<!-- revert cancelled bookings -->
<!-- <div id="revert" style="position:fixed; margin-left:55%; bottom:20px;display:none;">
  <a href="revert_cancel_booking.php?id=<?php echo base64_encode($booking_id); ?>&t=<?php echo base64_encode($type); ?>"><button type="button" class="btn btn-md" style="position:relative;background-color:#39B8AC;width:130px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i>&nbsp;Revert Back</button></a>
</div>
 -->
 <!-- -->
<!-- jQuery library -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<!-- show 3 buttons -->

<script>
$(document).ready(function(){
		var t = "<?php echo $type; ?>" ;
    switch(t){
      case 'o': 	$("#av").hide();
			$("#pb").hide();
      $("#cb").hide();
      $("#pbook").show();
      $("#ebook").hide();
      $("#addbook").hide();
      break;
      case 'c':	$("#av").hide();
			$("#pb").hide();
      $("#pbook").hide();
      $("#cb").hide();
      $("#revert").show();
      $("#edb").hide();
      $("#ebook").hide();
      $("#addbook").hide();
      break;
      case 'l':$("#av").show();
			$("#pb").show();
      $("#cb").show();
      $("#edb").show();
      $("#ebook").show();
      $("#addbook").hide();
      break;
      default:$("#av").show();
			$("#pb").show();
      $("#cb").show();
      $("#edb").show();
      $("#ebook").show();
      $("#addbook").show();

    }
    
    var axle_flag = "<?php $sql_get_axle = mysqli_query($conn,"SELECT axle_flag FROM user_booking_tb WHERE booking_id='$booking_id'");
    $row_get_axle = mysqli_fetch_object($sql_get_axle);
    $axle_flag = $row_get_axle->axle_flag;
    echo $axle_flag; ?>";
    //console.log(axle_flag);
    if(axle_flag == 1 && t == "f"){
      
      $("#cb").hide();
      $("#edb").hide();
      $("#ebook").hide();
      $("#addbook").show();
    }
    
    if(axle_flag == 1 && t == "b"){
      $("#cb").hide();
      $("#ebook").hide();
      $("#addbook").hide();
    }
    if(t == "eb"){
      $("#eu").hide();
      $("#av").hide();
			$("#pb").hide();
      $("#cb").hide();
      $("#edb").hide();
      $("#ebook").hide();
      $("#addbook").hide();
    }
    if(t == "unwanted"){
      $("#av").show();
			$("#pb").show();
      $("#cb").show();
      $("#eu").show();
      $("#revert").hide();
      $("#pbook").hide();
      $("#edb").show();
      $("#ebook").show();
      $("#addbook").hide();
    }
    if(t == "irp"){
      $("#av").show();
      $("#cb").hide();
      $("#edb").hide();
      $("#ebook").hide();
      $("#addbook").show();
    }
	$('#revertBack').on('click',function(){
		$('#revertBack').prop('disabled',true);
	});
});
</script>

<!-- push button -->
<script>
$(document).ready(function(){
		var t = "<?php echo $type; ?>" ;
		var s = "<?php echo $status; ?>" ;
		if(t == "l" || t == "unwanted"){
      //	$("#var_menu1").slideToggle("slow");
      $("#var_menu6").show();
		}
    if(t == "irp"){
      //	$("#var_menu1").slideToggle("slow");
      $("#var_menu6").hide();
		}
		if(t == "b"){
		  //	$("#var_menu2").slideToggle("slow");
      $("#var_menu6").hide();
		}
		if(t == "f"){
			if(s == "4")
			{
        //	$("#var_menu6").slideToggle("slow");
        $("#var_menu6").show();
			}
			if(s == "5"){
				//$("#var_menu6").slideToggle("slow");
        $("#var_menu6").show();
			}
			if(s == "3"){
				//$("#var_menu6").slideToggle("slow");
        $("#var_menu6").show();
			}
			if(s == "6"){
				//$("#var_menu6").slideToggle("slow");
        $("#var_menu6").show();
			}
		}
		if(t == "ur")
		{
			//$("#var_menu6").slideToggle("slow");
      $("#var_menu6").show();
		}
});
</script>

<!-- add vehicle -->
<!-- Modal -->
<div class="modal fade" id="myModal_vehicle" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Add Vehicle (<?php echo $user_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="add_vehicle" class="form" method="post" action="add_vehicle.php" >
<div class="row">
  <div class="col-xs-6 col-xs-offset-3 form-group">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <label class="bike">
 <input id="veh" type="radio" name="veh" value="2w" />
 <img id="bike" src="images/bike.png">
  </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <label class="car">
  <input id="veh" type="radio" name="veh" value="4w" />
  <img id="car" src="images/car.png">
  </label>
  </div>
</div>
<div class="row"></div>
<div class="row">
<div id="b_m" class="col-xs-6 col-xs-offset-3 form-group">
			 <div class="ui-widget">
				 <input class="form-control autocomplete" id="BrandModelid" type="text" name="BrandModelid"  required placeholder="Select Model">
			 </div>
</div>
</div>

<div class="row"></div>
<div class="row">
<div class="col-xs-6 col-xs-offset-3 form-group">
<select class="form-control" id="fuel" name="fuel">
<option selected >Fuel Type </option>
<option data-imagesrc="images/bike.png" value="Diesel">Diesel</option>
<option data-imagesrc="images/car.png" value="Petrol">Petrol</option>
</select>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-6 col-xs-offset-3 form-group">
<input class="form-control" type="text" id="regno" name="regno"  data-mask-reverse="true" maxlength="13" placeholder="Vehicle No">
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-6 col-xs-offset-3 form-group">
<input class="form-control" type="text" id="year" name="year" placeholder="Year">
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-6 col-xs-offset-3 form-group">
<input class="form-control" type="number" id="km" name="km" placeholder="Km Driven">
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<br>
<input class="form-control" type="submit" id="vehicle_submit" name="vehicle_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
 <input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" />
 <input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" />
 <input type="hidden" id="type" name="type" value="<?php echo $type; ?>" />
 <input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" />
 </form>
</div>
</div>
</div></div>

<!-- add booking -->
<!-- Modal -->
<div class="modal fade" id="myModal_add_booking" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Add Booking(<?php echo $user_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="add_booking" class="form" method="post" action="add_booking.php">
<div class="row" align="center">
<div  id="veh_t" class="col-xs-6 col-xs-offset-3 form-group">

  <label class="bike">
  <input class="veh_b" id="veh_b" type="radio" name="veh_b" value="2w" checked/>
  <img id="veh_b" src="images/bike.png">
</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label class="car">
  <input class="veh_b" id="car" type="radio" name="veh_b" value="4w" />
  <img id="veh_b" src="images/car.png">
</label>
</div></div>

<div class="row"></div>

<div class="row">
<div id="b_m" class="col-xs-5 col-xs-offset-1 form-group">
<select class="form-control brandmodel" id="veh_no" name="veh_no" required>
<option selected value="">Vehicle</option>
 </select>
</div>


<div class="col-xs-5 form-group" id="service">
<div class="ui-widget" id="serv">
        	<input class="form-control autocomplete servicetype" id="service_type" type="text" name="service_type" placeholder="Service Type" required>
  </div>
</div>
</div>

<div class="row"></div>

<div class="row">
<div class="col-xs-8 col-xs-offset-1 form-group" id="loc">
        <div class="ui-widget">
        	<input class="form-control autocomplete" id="location" type="text" name="location" placeholder="Start typing Location..." required>
        </div>
</div>
        <div><p class="btn btn-sm" id="go" style="margin-left:-8px;background-color:#B2EBF2;">Go</p></div>
</div> 

<div class="row"></div>

<div class="row">
<div class="col-xs-6 col-xs-offset-1 form-group" id="mec">

<select class="form-control  mechanic" id="mechanic" name="mechanic" required>
<option selected>Select Mechanic</option>
</select>
</div>

<div class="col-xs-4 form-group">
<input class="form-control" type="number" id="amount" name="amount" placeholder="Amount">
</div>
</div>


<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-1 form-group">
<label> Service Date</label></div>
<div class="col-xs-3  form-group">
<input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="service_date" name="service_date" required style="margin-top:10px;">
</div>

<div class="col-xs-2 form-group">
<label> Next Service Date</label></div>
<div class="col-xs-3  form-group">
<input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="next_service_date" name="next_service_date" required style="margin-top:10px;">
</div>
</div>
<div class="row"></div>

<div class="row">
<div class="col-xs-10 col-xs-offset-1 form-group" >
<textarea class="form-control" maxlength="100" id="description" name="description" placeholder="Description..." ></textarea>
</div>
</div>

<div class="row"></div>
<div class="row">
<div class="col-xs-4 col-xs-offset-4 form-group">
<!--<label>PickUp</label>
  <input class="form-group" type="radio" id="pickup" name="pickup" value="1" >&nbsp;Yes&nbsp;</input>
  <input class="form-group" type="radio" id="pickup" name="pickup" value="0" checked>&nbsp;No</input>
</div>

<div class="col-xs-4  form-group">-->
<label>SMS</label>
<input class="form-group" type="radio" id="sms" name="sms" value="1" checked>&nbsp;Yes&nbsp;</input>
<input class="form-group" type="radio" id="sms" name="sms" value="0" >&nbsp;No</input></div>
</div>

<div class="row"></div>

<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<input class="form-control" type="submit" id="booking_submit" name="booking_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
</form>

</div>

</div>
</div>
</div>

<!-- edit booking -->
<!-- Modal -->
<div class="modal fade" id="myModal_edit_booking" role="dialog">
<div class="modal-dialog" style="width:1050px;">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Edit Booking(<?php echo $booking_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="edit_booking" class="form" method="post" action="edit_booking.php">
  <?php
$sql_book = "SELECT vech_id,user_veh_id,vehicle_type,user_vech_no,service_type,mec_id,shop_name,service_description,pickup_full_address,estimate_amt,service_date,pick_up,pickup_address,amt,followup_date,locality,pickup_date_time FROM user_booking_tb WHERE booking_id = '$booking_id'";
$res_book = mysqli_query($conn,$sql_book);
$row_book = mysqli_fetch_object($res_book);
$b_veh_id=$row_book->vech_id;
$b_user_veh_id = $row_book->user_veh_id;
$b_veh = $row_book->vehicle_type;
$b_veh_no = $row_book->user_vech_no;
$b_service_type = $row_book->service_type;
$b_mec_id = $row_book->mec_id;
$b_shop_name = $row_book->shop_name;
$b_desc = $row_book->service_description;
$b_service_date = $row_book->service_date;
$b_pickup = $row_book->pick_up;
$b_amount = $row_book->amt;
$estimate_amt = $row_book->estimate_amt;
$next_service_date = $row_book->followup_date;
$b_locality = $row_book->locality;
$pickup_location = $row_book->pickup_address;
$pickup_full_address = $row_book->pickup_full_address;
$pickup_date_time = date('d-m-Y H:i',strtotime($row_book->pickup_date_time));

// get brand model
$sql_model = "SELECT id,brand,model,reg_no FROM user_vehicle_table WHERE id='$b_veh_id'";
$res_model = mysqli_query($conn,$sql_model);
$row_model = mysqli_fetch_object($res_model);
$brand_id = $row_model->id;
$brandmodel = $row_model->brand." ".$row_model->model." ".$row_model->reg_no;

if($b_mec_id == "" || $b_mec_id==0){
  $shop_address = "";
}
else{
//get shop address
$sql_shop_loc = mysqli_query($conn,"SELECT address4 FROM admin_mechanic_table WHERE mec_id='$b_mec_id' ");
$row_shop_loc= mysqli_fetch_object($sql_shop_loc);
$shop_address = $row_shop_loc->address4;
}

    switch($b_service_type){
      case 'general_service' :
      if($b_veh =='2w'){
       $b_service_type = "General Service";
      }
      else{
       $b_service_type = "Car service and repair";
      }
      break;
      case 'break_down': $b_service_type = "Breakdown Assistance"; break;
      case 'tyre_puncher':$b_service_type = "Tyre Puncture";  break;
      case 'other_service':$b_service_type = "Repair Job"; break;
      case 'car_wash':
      if($b_veh == '2w'){
       $b_service_type = "water Wash";
      }
      else{
       $b_service_type = "Car wash exterior";
      }break;
      case 'engine_oil':$b_service_type = "Repair Job"; break;
      case 'free_service':$b_service_type = "General Service"; break;
      case 'car_wash_both':
      case 'completecarspa':$b_service_type = "Complete Car Spa"; break;
      case 'car_wash_ext':$b_service_type = "Car wash exterior"; break;
      case 'car_wash_int':$b_service_type = "Interior Detailing"; break;
      case 'Doorstep car spa':$b_service_type = "Doorstep Car Spa"; break;
      case 'Doorstep_car_wash':$b_service_type = "Doorstep Car Wash"; break;
      case 'free_diagnastic':
      case 'free_diagnostic':
      case 'diagnostics':
      if($b_veh == '2w'){
       $b_service_type = "Diagnostics/Check-up";
     }
     else{
      $b_service_type = "Vehicle Diagnostics";
     }
         break;
      case 'water_wash':$b_service_type = "water Wash"; break;
      case 'exteriorfoamwash':$b_service_type = "Car wash exterior"; break;
      case 'interiordetailing':$b_service_type = "Interior Detailing"; break;
      case 'rubbingpolish':$b_service_type = "Car Polish"; break;
      case 'underchassisrustcoating':$b_service_type = "Underchassis Rust Coating"; break;
      case 'headlamprestoration':$b_service_type = "Headlamp Restoration"; break;
      default:$b_service_type = $b_service_type;
      }

if($b_veh == "2w"){
  ?>
  <div class="row" align="center">
  <div class="col-xs-1" style="margin-top: 6%;border-right: solid 1px #ddd;width: 13%;">
<div class="row" align="center">
  <div class="col-xs-1 form-group">
  <label class="bike">
  <input id="veh_be" type="radio" name="veh_be" value="2w" checked/>
  <img id="veh_be" src="images/bike.png">
  </label>
  </div></div>
  <div class="row" align="center">
  <div class="col-xs-1  form-group">
  <label class="car">
  <input id="car" type="radio" name="veh_be" value="4w" />
  <img id="veh_be" src="images/car.png">
  </label>
</div></div>
</div>
<?php
}
else{
  ?>
  <div class="row" align="center">
  <div class="col-xs-1" style="margin-top: 6%;border-right: solid 1px #ddd;width: 13%;">
<div class="row" align="center">
  <div class="col-xs-1 form-group">
  <label class="bike">
  <input id="veh_be" type="radio" name="veh_be" value="2w" />
  <img id="veh_be" src="images/bike.png">
  </label>
  </div></div>
  <div class="row" align="center">
  <div class="col-xs-1  form-group">
  <label class="car">
  <input id="car" type="radio" name="veh_be" value="4w" checked/>
  <img id="veh_be" src="images/car.png">
  </label>
</div></div>
</div>
<?php
}?>
<div class="col-xs-10" style="margin-left: 3%;">
<div class="row" align="center">
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
<label> Brand Model</label> 
</div>
<div id="b_me" class="col-xs-4 form-group">
<select class="form-control" id="veh_noe" name="veh_noe">
<option selected value="<?php echo $brand_id; ?>"><?php echo $brandmodel; ?></option>
 </select>
</div>
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
<label> Service Type</label> 
</div>
<div class="col-xs-3 form-group" id="service">
<div class="ui-widget">
        	<input class="form-control autocomplete" id="service_typee" type="text" name="service_typee" placeholder="Service Type" value="<?php echo $b_service_type; ?>" required>
</div>
</div>
</div>

<div class="row" align="center">
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
<label> Customer Location</label> 
</div>
<div class="col-xs-4 form-group" id="loce">
        <div class="ui-widget">
        	<input class="form-control autocomplete" id="locatione" type="text" name="locatione" placeholder="Start typing Location..." value="<?php echo $b_locality; ?>" readonly>
        </div>
        
</div>
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
<label> PickUp Location</label> 
</div>
<div class="col-xs-3 form-group" id="locpickup">
        <div class="ui-widget">
        	<input class="form-control autocomplete" id="location_pickup" type="text" name="location_pickup" placeholder="PickUp Location..." value="<?php if($pickup_location == "" || $pickup_location == "-"){ echo $b_locality;} else { echo $pickup_location;} ?>" required>
        	<!--<input class="form-control autocomplete" id="location_pickup" type="text" name="location_pickup" placeholder="PickUp Location..." required>-->
        </div>
        
</div>
<div class="col-xs-1 form-group" id="go-div"><p class="btn btn-sm" id="go" style="margin-left:-8px;background-color:#B2EBF2;">Go</p></div>
</div>
<div class="row" align="center">
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
<label> Service Center</label> 
</div>
<div class="col-xs-4 form-group" id="mece">

<select class="form-control" id="mechanice" name="mechanice" required>
<?php if($b_shop_name == '' || $b_shop_name == '0'){ ?>
<option selected value="">Select Mechanic</option>
<?php } 
else{ ?>
<option selected value="<?php echo $b_mec_id; ?>"><?php echo $b_shop_name; ?></option>
<?php } ?>
</select>
</div>
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
<label> Service Date</label>
</div>
<div class="col-xs-3  form-group">
<input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="service_datee" name="service_datee" value="<?php echo date("d-m-Y",strtotime($b_service_date)); ?>" required>
</div>
</div>
<div class="row" align="center">
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
<label> Service Description</label> 
</div>
<div class="col-xs-4 form-group" >
<textarea class="form-control" maxlength="100" id="descriptione" name="descriptione" placeholder="Description..." ><?php echo $b_desc; ?></textarea>
</div>
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
<label> Estimate Quoted</label> 
</div>
<div class="col-xs-3 form-group">
<input class="form-control" type="number" id="amounte" name="amounte" placeholder="Amount" value="<?php echo $estimate_amt; ?>">
</div>
</div>
<div class="row" align="center">
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
<label>PickUp</label>
</div>
<div class="col-xs-4 form-group" >
<span class="make-switch switch-radio">
  <input type="radio" <?php  if($b_pickup=="yes"||$b_pickup=="1"){ echo checked;} ?> data-on-text="Yes" data-off-text="No" data-on-color="success" data-off-color="danger" class="alert-status" id="pickup_switch">
</span>
<?php
// if($b_pickup == "yes"){
  ?>
<!--  <input class="form-group" type="radio" id="pickupe" name="pickupe" value="1" checked>&nbsp;Yes&nbsp;</input>
  <input class="form-group" type="radio" id="pickupe" name="pickupe" value="0">&nbsp;No</input>-->
  <?php
// }
// else{
  ?>
  <!--<input class="form-group" type="radio" id="pickupe" name="pickupe" value="1">&nbsp;Yes&nbsp;</input>
  <input class="form-group" type="radio" id="pickupe" name="pickupe" value="0" checked>&nbsp;No</input>-->
  <?php
// } ?>
</div>
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
<label>PickUp Date</label>
</div>
<div class="col-xs-3 form-group">		
				<div class='input-group date' id='datetimepicker7'>		
					<input type='text' class="form-control" id="pickup_date" name="pickup_date" value="<?php if($pickup_date_time != '01-01-1970 05:30')echo $pickup_date_time;?>"/>		
					<span class="input-group-addon">		
					<span class="glyphicon glyphicon-calendar"></span>		
					</span>		
				</div>		
			</div>		

</div>
<div class="row" align="center">
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
<label> Pickup Address</label> 
</div>
<div class="col-xs-4 form-group" >
<textarea class="form-control" maxlength="100" id="pickup_full_addresse" name="pickup_full_addresse" placeholder="Address..." ><?php echo $pickup_full_address;?></textarea>
</div>
</div>

</div>
</div>
<?php
//}?>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<input class="form-control" type="submit" id="edit_booking_submit" name="edit_booking_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
<input type="hidden" id="pickupe" name="pickupe" <?php  if($b_pickup=="yes"){ echo "value='1'";}else{ echo "value='0'";} ?>>
</form>
<!--<div class="loader_overlay">
</div>
<div class="loader">
</div>-->
		<div class="socket loader">
			<div class="gel center-gel">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c1 r1">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c2 r1">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c3 r1">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c4 r1">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c5 r1">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c6 r1">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			
			<div class="gel c7 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			
			<div class="gel c8 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c9 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c10 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c11 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c12 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c13 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c14 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c15 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c16 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c17 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c18 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c19 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c20 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c21 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c22 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c23 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c24 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c25 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c26 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c28 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c29 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c30 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c31 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c32 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c33 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c34 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c35 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c36 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c37 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			
		</div>
</div>

</div>
</div>
</div>

<!-- add unlimited RNR -->		
<!-- Modal -->		
<div class="modal fade" id="myModal_unlimited_RNR" role="dialog">		
<div class="modal-dialog">		
<!-- Modal content-->		
<div class="modal-content">		
 <div class="modal-header">		
    <button type="button" class="close" data-dismiss="modal">&times;</button>		
    <h3 class="modal-title">Add Follow Up (Id : <?php echo $user_id; ?>)</h3>		
 </div>		
 <div class="modal-body">		
 <?php 
 if($type == "l"){
   ?> 
  <form id="add_rnr" role="form" method="post" action="add_followup.php">
  <?php
 }
 else{
   ?>
    <form id="add_rnr" role="form" method="post" action="add_rnr.php">	
   <?php
 } ?>
  	
	<div class="row">		
		<div class="col-xs-8 col-xs-offset-2 form-group">		
			<br>		
			<select class="form-control" id="followup_status" name="status" required>		
				<option selected value="">Select Reason</option>		
				<?php		
				$sql="SELECT activity FROM admin_activity_tbl WHERE flag='0'";		
				$query=mysqli_query($conn,$sql);		
				while ($rows = mysqli_fetch_array($query)){ ?>		
				<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>		
				<?php } ?>		
			</select>		
		</div>		
	</div>		
	<div class="row"></div>		
	<div class="row">		
		<div class="col-xs-8 col-xs-offset-2 form-group">		
			<textarea class="form-control" maxlength="1000" id="comments" name="comments" style="min-height:100px;" placeholder="Comments..."></textarea>		
		</div>		
    </div>		
	<div class="row"></div>		
    <div class="row">		
		<div class="col-xs-3 col-xs-offset-2 form-group">		
			<label>&nbsp;FollowUp Date</label>		
		</div>		
		<div class="col-xs-5">		
			<div class="form-group">		
				<div class='input-group date' id='datetimepicker6'>		
					<input type='text' class="form-control" name="follow_date"/>		
					<span class="input-group-addon">		
					<span class="glyphicon glyphicon-calendar"></span>		
					</span>		
				</div>		
			</div>		
		</div>		
	</div>		
	<div class="row">		
		<div class="col-xs-3 col-xs-offset-2 form-group">		
			<label>&nbsp;Priority</label>		
		</div>		
		<div class="col-xs-5">		
			<div class="form-group">		
				<input type="radio" name="priority" value="1" style="width:30px;">Low</input>		
				<input type="radio" name="priority" value="2" style="width:30px;">Medium</input>		
				<input type="radio" name="priority" value="3" style="width:30px;" checked>High</input>		
			</div>		
		</div>		
	</div>					
	<div class="row"></div>		
	<div class="row">		
		<div class="col-xs-2 col-xs-offset-5 form-group">		
			<br>		
			<input class="form-control" type="submit" id="followup_submit" name="followup_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>		
		</div>		
	</div>		
	<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >		
	<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >		
	<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >		
	<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >		
   </form>		
 </div>
 
</div>
</div>
</div>

<!-- move to  others -->
<!-- Modal -->

<!-- <div class="modal fade" id="myModal_others" role="dialog">
<div class="modal-dialog"> -->

<!-- Modal content-->
<!-- <div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Move to Others (Id : <?php echo $user_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="add_others" class="form" method="post" action="add_others.php">
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<br>
<select class="form-control" id="others_status" name="status" required>
<option selected value="">Select Reason</option>
<?php
$sql="SELECT activity FROM admin_activity_tbl WHERE flag='2'";
$query=mysqli_query($conn,$sql);

while ($rows = mysqli_fetch_array($query)){ ?>
<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>
<?php } ?>
</select>
</div>
</div>
<div class="row"></div>

<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<textarea class="form-control" maxlength="1000" id="comments" name="comments" style="min-height:100px;" placeholder="Comments..."></textarea>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<br>
<input class="form-control" type="submit" id="others_submit" name="others_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
</form>
</div>
</div>
</div>
</div> -->
<!-- add RNR 1 -->
<!-- Modal -->
<div class="modal fade" id="myModal_reschedule" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Reschedule (Id : <?php echo $booking_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="reschedule_booking" class="form" method="post" action="reschedule_booking.php">
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<br>
<select class="form-control" id="reschedule_status" name="status" required>
<option selected value="">Select Reason</option>
<?php
$sql="SELECT activity FROM admin_activity_tbl WHERE flag='4'";
$query=mysqli_query($conn,$sql);

while ($rows = mysqli_fetch_array($query)){ ?>
<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>
<?php } ?>
</select>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<textarea class="form-control" maxlength="1000" id="comments" name="comments" style="min-height:100px;" placeholder="Comments..."></textarea>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-3 col-xs-offset-2 form-group">
<label>&nbsp;Rechedule To</label>
</div>
<div class="col-xs-5  form-group">
<div class="form-group">
                <div class='input-group date' id='datetimepicker4'>
                    <input type='text' class="form-control" name="reschedule_to"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
</div></div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<br>
<input class="form-control" type="submit" id="reschedule_submit" name="reschedule_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >

</form>
</div>
</div>
</div>
</div>

<!-- Cancel Bookings -->
<!-- Modal -->
<div class="modal fade" id="myModal_cancel_booking" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Cancel Booking (Id : <?php echo $booking_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="cancel_booking" class="form" method="post" action="cancel_booking.php">
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<br>
<select class="form-control" id="cancel_status" name="status" required>
<option selected value="">Select Reason</option>
<?php
$sql="SELECT activity FROM admin_activity_tbl WHERE flag='1'";
$query=mysqli_query($conn,$sql);

while ($rows = mysqli_fetch_array($query)){ ?>
<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>
<?php } ?>
</select>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<textarea class="form-control" maxlength="1000" id="cancel_comments" name="comments" style="min-height:100px;" placeholder="Comments..." required></textarea>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<br>
<input class="form-control" type="submit" id="cancel_submit" name="cancel_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
</form>
</div>
</div>
</div>
</div>
<!-- brand and model -->
 <script>
$(function() {
		    $( "#BrandModelid" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_brandmodel.php",
                data: {
                    term: request.term,
					extraParams:$('#veh:checked').val()
                },
                dataType: 'json'
            }).done(function(data) {
				//console.log(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#BrandModelid").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "9999999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script>
<!-- user location -->
<script>
$("#location_home").click(function(){
var c = $('#cityu').val();
  //console.log(c);
  if(c==''){
    alert("Plese select City to get localityies!");
  }
  else{
$(function() {

			    $( "#location_home" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_user_locality.php",
				type : "GET",
                data: {
                    term: request.term,
          					city:$('#cityu').val()
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location_home").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

  }
});

</script>
<!-- locality work -->
<script>
$("#location_work").click(function(){
var c = $('#cityu').val();
  //console.log(c);
  if(c==''){
    alert("Plese select City to get localityies!");
  }
  else{
$(function() {
			    $( "#location_work" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_work_locality.php",
				type : "GET",
                data: {
                    term: request.term,
          					city:$('#cityu').val()
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location_work").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});
  }
});
</script>
<!-- empty autocompletes localities on city change -->
<script>
  $(document).ready(function(){
	  	  $('.alert-status').bootstrapSwitch();
$('#pickup_switch').on('switchChange.bootstrapSwitch', function (event, state) {
//console.log(state);
var s;
if(state==true)
s=1;
else
s = 0;
$("#pickupe").val(s);
});
    $('#cityu').change(function(){
      $('#location_work').empty();
      $('#location_work').autocomplete('close').val('');
      $('#location_home').empty();
      $('#location_home').autocomplete('close').val('');
    });
  })
  </script>

<!-- reg number -->
<!-- masking -->
<script type="text/javascript" src="js/inputmask.js"></script>
<script type="text/javascript" src="js/jquery.inputmask.js"></script>
  <script type="text/javascript" src="js/inputmask.extensions.js"></script>

<script>
Inputmask("A{2}/9{2}/A{2}/9{4}").mask($("#regno"));
</script>

<!-- select Vehicle Number  -->
<script>
$(document).ready(function($) {
 $(document).on('change', '[name="veh_b"]' , function(){
  	var selectvalue = $('[name="veh_b"]:checked').val();
	var selectuser = <?php echo $user_id; ?>;
	$("#veh_no").empty();
		 $("div.veh").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_veh.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selectvalue": selectvalue , "selectuser" : selectuser},
            success : function(data) {
				// alert(data);
                $('#veh_no').append(data);
                 $("#location").autocomplete('close').val('');
                 $("#service_type").autocomplete('close').val('');
                  $("#mechanic").empty();
                  $("#mechanic").append('<option value="">Select Mechanic</option>');

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

    });
});
</script>
<!-- select vehicle on page load -->
<script>
$(document).ready(function($) {
  	var selectvalue = $('[name="veh_b"]:checked').val();
	var selectuser = <?php echo $user_id; ?>;
  var veh_no = $('#veh_no').val();
	$("#veh_no").empty();
		 $("div.veh").show();
      // $("#location").autocomplete('close').val('');
      // $("#mechanic").empty();
      // $("#mechanic").append('<option value="">Select Mechanic</option>');
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_veh.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selectvalue": selectvalue , "selectuser" : selectuser, "veh_no": veh_no},
            success : function(data) {
				// alert(data);
                $('#veh_no').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

});
</script>

<!-- select Service type -->
<!-- select Service type -->
 <script>
$(function() {
			    $( "#service_type" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_service_type.php",
                data: {
                    term: request.term,
					//type : $('#veh_b:checked').val()
						type : $('#veh_b:checked').val(),
					vtype: $('#veh_no').val(),
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#service_type").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script>

<!-- select Mechanic Based On Location -->
<script>
$(document).ready(function($) {
 $("#location").on("autocompletechange", function(){
  //	var selectservice = $("#service_type").val();
	var selecttype = $('[name="veh_b"]:checked').val();
	var selectloc = $("#location").val();
  var veh_no = $("#veh_no").val();
  var service = $("#service_type").val();
  var book_id = $("#book_id").val();
  var city = $("#city").val();
  var crm = '<?php echo $crm_log_id; ?>';
  var type = '<?php echo $type; ?>';
  console.log(crm);
	$("#mechanic").empty();
		 $("div.mec").show();
          //Make AJAX request, using the selected value as the POST
		  





		  // if(crm != 'crm036' && crm != 'crm012' && crm != 'crm041' && crm != 'crm018')
		  			  if(crm != 'crm036' && crm != 'crm012' && crm != 'crm041' && crm != 'crm046')
		  // if((selecttype === '2w' || service === 'Express Car Service 999' || service === 'Car dent / scratch removal' || service === 'Dent Removal 2499' || service === 'Dent 2999 1-2 Panels' || service === 'Dent 2999 3-4 Panels' || service === 'Dent 2999 5-6 Panels' || service === 'Dent 2999 7-8 Panels' || service === 'Dent Removal 2999') && crm != 'crm036' && city == 'Chennai' && crm != 'crm012' && crm != 'crm041')
		  // if(selecttype === '2w' && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
		  {
			  $('.loader').show();
			  $('.loader_overlay').show();
			  $.ajax({
            url : "ajax/project_crown.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc ,"veh_no": veh_no ,"service":service,"book_id":book_id,"crm_log_id":crm},
            success : function(data) {
				$('.loader').hide();
				$('.loader_overlay').hide();
				// console.log(data);
				//alert(data);
                $('#mechanic').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });
			
		  }
		  else
		  {
		  $.ajax({
            url : "ajax/get_mechanic_loc.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc,"veh_no": veh_no , "service":service},
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanic').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });
		  }

    });
});
</script>


<!-- --------------------------------------- edit booking ------------------------------------------------------ -->
<!-- select Vehicle Number  -->
<script>
$(document).ready(function($) {
 $(document).on('change', '[name="veh_be"]' , function(){
  	var selectvalue = $('[name="veh_be"]:checked').val();
	var selectuser = <?php echo $user_id; ?>;
	$("#veh_noe").empty();
  
		 $("div.veh").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_veh.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selectvalue": selectvalue , "selectuser" : selectuser},
            success : function(data) {
				// alert(data);
                $('#veh_noe').append(data);
                // $("#locatione").autocomplete('close').val('');
				$("#location_pickup").autocomplete('close').val('');
                $("#service_typee").autocomplete('close').val('');
                $("#mechanice").empty();
                $("#mechanice").append('<option value="">Select Mechanic</option>');

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

    });
});
</script>
<!-- select vehicle on page loaad -->
<script>
$(document).ready(function($) {
  	var selectvalue = $('[name="veh_be"]:checked').val();
	var selectuser = <?php echo $user_id; ?>;
	$("#veh_noe").empty();
		 $("div.veh").show();
      //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_veh.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selectvalue": selectvalue , "selectuser" : selectuser},
            success : function(data) {
				// alert(data);
                $('#veh_noe').append(data);
               // $("#locatione").autocomplete('close').val('');
               // $("#mechanice").empty();
               // $("#mechanice").append('<option valu="">Select Mechanic</option>');

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

});
</script>

<!-- select Service type -->
 <script>
$(function() {
			    $( "#service_typee" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_service_type.php",
                data: {
                    term: request.term,
					//type : $('#veh_b:checked').val()
						type : $('#veh_be:checked').val(),
					vtype: $('#veh_noe').val(),
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#service_typee").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script>
<!-- select Location of service center -->
 <script>
$(function() {
			    $( "#location" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_location.php",
                data: {
                    term: request.term,
					//type : $('#veh_b:checked').val()
					vtype: $('[name="veh_b"]:checked').val(),
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script> 

<!-- edit booking select Location of service center -->
 <script>
/*$(function() {
			    $( "#locatione" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_location.php",
                data: {
                    term: request.term,
					//type : $('#veh_b:checked').val()
					vtype: $('[name="veh_be"]:checked').val(),
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#locatione").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});*/

<!-- Pickup location -->
$(function() {
			    $( "#location_pickup" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_location.php",
                data: {
                    term: request.term,
					//type : $('#veh_b:checked').val()
					vtype: $('[name="veh_be"]:checked').val(),
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location_pickup").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script> 

<!-- select Mechanic Based On Location -->
<script>
$(document).ready(function($) {
 $("#service_typee").on("autocompletechange", function(){
	 console.log('he');
	 $('#location_pickup').attr('value', '');
	 $("#mechanice").empty();
 });
	
 $("#location_pickup").on("autocompletechange", function(){
  	//var selectservice = $("#service_typee").val();
	var selecttype = $('[name="veh_be"]:checked').val();
	var selectloc = $("#location_pickup").val();
  var veh_no = $("#veh_noe").val();
  var service = $("#service_typee").val();
  var book_id = $("#book_id").val();
  var city = $("#city").val();
  var crm = '<?php echo $crm_log_id; ?>'; 
  var type = '<?php echo $type; ?>';
  console.log(crm);
	$("#mechanice").empty();

		 $("div.mece").show();
          //Make AJAX request, using the selected value as the POST
		  // if(crm != 'crm036' && crm != 'crm012' && crm != 'crm041' && crm != 'crm018')

		  			  if(crm != 'crm036' && crm != 'crm012' && crm != 'crm041' && crm != 'crm046')
		  // if((selecttype === '2w' || service === 'Express Car Service 999' || service === 'Car dent / scratch removal' || service === 'Dent Removal 2499' || service === 'Dent 2999 1-2 Panels' || service === 'Dent 2999 3-4 Panels' || service === 'Dent 2999 5-6 Panels' || service === 'Dent 2999 7-8 Panels' || service === 'Dent Removal 2999') && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
		  // if(selecttype === '2w' && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
		  {
			  $('.loader').show();
			  $('.loader_overlay').show();
			  $.ajax({
            url : "ajax/project_crown.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc ,"veh_no": veh_no ,"service":service,"book_id":book_id,"crm_log_id":crm},
            success : function(data) {
				// console.log(data);
				$('.loader').hide();
			  $('.loader_overlay').hide();
				//alert(data);
                $('#mechanice').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrown	Error);
          }

        });
			
		  }
		  else
		  {
		  $.ajax({
            url : "ajax/get_mechanic_loc.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc ,"veh_no": veh_no ,"service":service},
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanice').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });
		}
    });
});
</script>
<!-- select Mechanic Based On Location  on page load-->
<!--<script>
$(document).ready(function($) {
 //$("#location_pickup").on("autocompletechange", function(){
  	//var selectservice = $("#service_typee").val();
	var selecttype = $('[name="veh_be"]:checked').val();
	var selectloc = $("#location_pickup").val();
  var veh_no = $("#veh_noe").val();
  var service = $("#service_typee").val();
  var book_id = $("#book_id").val();
  var city = $("#city").val();
  var crm = '<?php echo $crm_log_id; ?>'; 
  var type = '<?php echo $type; ?>';
  console.log(crm);
  if(selectloc != ""){
    $("#mechanice").empty();

		 $("div.mece").show();
          //Make AJAX request, using the selected value as the POST
		  if((selecttype === '2w' || service === 'Express Car Service 999' || service === 'Car dent / scratch removal' || service === 'Dent Removal 2499' || service === 'Dent 2999 1-2 Panels' || service === 'Dent 2999 3-4 Panels' || service === 'Dent 2999 5-6 Panels' || service === 'Dent 2999 7-8 Panels' || service === 'Dent Removal 2999') && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
		  // if(selecttype === '2w' && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
		  {
			  $('.loader').show();
			  $('.loader_overlay').show();
			  $.ajax({
            url : "ajax/project_crown.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc ,"veh_no": veh_no ,"service":service,"book_id":book_id},
            success : function(data) {
				// console.log(data);
				$('.loader').hide();
				$('.loader_overlay').hide();
				//alert(data);
                $('#mechanice').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });
			
		  }
		  else
		  {
		  $.ajax({
            url : "ajax/get_mechanic_loc.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc,"veh_no": veh_no , "service":service},
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanice').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });
		  }
  }
});
</script>-->


<!-- --------------------------------------- edit booking ----------------------------------------------- ->
<!-- validation -->
<script>
var userinput = document.getElementById('user_name');
userinput.oninvalid = function(event) {
	  document.getElementById("user_name").style.borderColor="#E42649";
    event.target.setCustomValidity('Only aplhabets and digits are allowed!');
}
</script>
<script>
 var date = new Date();
date.setDate(date.getDate());

$('.datepicker').datepicker({
   autoclose: true,
    startDate: date
});
if($('input.datepicker').val()){
    $('input.datepicker').datepicker('setDate', 'today');
 }
 </script>
<!-- date time picker -->
<script type="text/javascript">
    $(function () {
        var dateNow = new Date();
		        $('#datetimepicker7').datetimepicker({
            format: 'DD-MM-YYYY HH:mm',
            defaultDate:dateNow
        });
        $('#datetimepicker6').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            defaultDate:dateNow
        });
         $('#datetimepicker1').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            defaultDate:dateNow
        });
         $('#datetimepicker2').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            defaultDate:dateNow
        });
         $('#datetimepicker3').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            defaultDate:dateNow
        });
        $('#datetimepicker4').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            defaultDate:dateNow
        });
    });

</script>
 <!-- select veh before fetching models -->
 <script>
 $(document).ready(function(){
   $("#BrandModelid").click(function(){
     var veh = $('input[name=veh]:checked').val();
     //console.log(veh);
     if(veh == null){
       //console.log(veh);
       alert("Please select vehicle type to get vehicle models!");
     }
   });
 })
 </script>
 <script>
 $(document).ready(function(){
$('input[name=veh]').change(function(){
  $("#BrandModelid").val("");
});
 });
 </script>
 <!-- disable buttons on submit -->
 <script>
 $(document).ready(function(){
  $("#add_followup").submit(function(e){
    $('#followup_submit').attr('disabled','disabled');
    var option = document.getElementById('followup_status');
    option.oninvalid = function(event) {
        document.getElementById("followup_status").style.borderColor="#E42649";
        event.target.setCustomValidity('Please select a reason!');
        $('#followup_submit').removeAttr('disabled'); 
    }
  });
 });
 </script>
 <script>
 $(document).ready(function(){
  $("#add_rnr1").submit(function(e){
    $('#rnr1_submit').attr('disabled','disabled');
    var option = document.getElementById('rnr1_status');
    option.oninvalid = function(event) {
        document.getElementById("rnr1_status").style.borderColor="#E42649";
        event.target.setCustomValidity('Please select a reason!');
        $('#rnr1_submit').removeAttr('disabled'); 
    }
  });
 });
 </script>
  <script>
 $(document).ready(function(){
  $("#add_rnr2").submit(function(e){
    $('#rnr2_submit').attr('disabled','disabled');
    var option = document.getElementById('rnr2_status');
    option.oninvalid = function(event) {
        document.getElementById("rnr2_status").style.borderColor="#E42649";
        event.target.setCustomValidity('Please select a reason!');
        $('#rnr2_submit').removeAttr('disabled'); 
    }
  });
 });
 </script>
  <script>
 $(document).ready(function(){
  $("#add_others").submit(function(e){
    $('#others_submit').attr('disabled','disabled');
    var option = document.getElementById('others_status');
    userinput.oninvalid = function(event) {
        document.getElementById("others_status").style.borderColor="#E42649";
        event.target.setCustomValidity('Please select a reason!');
        $('#others_submit').removeAttr('disabled'); 
    }
  });
 });
 </script>
  <script>
 $(document).ready(function(){
  $("#cancel_booking").submit(function(e){
    $('#cancel_submit').attr('disabled','disabled');
    var option = document.getElementById('cancel_status');
    option.oninvalid = function(event) {
        document.getElementById("cancel_status").style.borderColor="#E42649";
        event.target.setCustomValidity('Please select a reason!');
        $('#cancel_submit').removeAttr('disabled'); 
    }
    var comt = document.getElementById('cancel_comments');
    comt.oninvalid = function(event) {
        document.getElementById("cancel_comments").style.borderColor="#E42649";
        event.target.setCustomValidity('Please explain the reason to cancel!');
        $('#cancel_submit').removeAttr('disabled'); 
    }
  });
 });
 </script>
 <!-- check if locality is empty -->
<script>
$(document).ready(function($) {
  $("#mechanic").click(function(){
    if($("#location").val()== ''){
      $("#mechanic").empty();
      $("#mechanic").append('<option value="">Select Mechanic</option>');
      alert("Please select a Location to get mechanics!!!");
    }
  });
});
</script>
<script>
$(document).ready(function($) {
  $("#mechanice").click(function(){
    if($("#location_pickup").val()== ''){
      $("#mechanice").empty();
      $("#mechanice").append('<option value="">Select Mechanic</option>');
      alert("Please select a Location to get mechanics!!!");
    }
  });
});
</script>
<script>
$(document).ready(function($) {
  
  $("#go-div").click(function(){
    if($("#location_pickup").val()== ''){
      alert("Oops no location has been selected!!!");
    }
	else
	{
		var selecttype = $('[name="veh_be"]:checked').val();
	var selectloc = $("#location_pickup").val();
  var veh_no = $("#veh_noe").val();
  var service = $("#service_typee").val();
  var book_id = $("#book_id").val();
  var city = $("#city").val();
  var crm = '<?php echo $crm_log_id; ?>'; 
  var type = '<?php echo $type; ?>';
  console.log(crm);
	$("#mechanice").empty();

		 $("div.mece").show();
          //Make AJAX request, using the selected value as the POST
		  // if(crm != 'crm036' && crm != 'crm012' && crm != 'crm041' && crm != 'crm018')

		  			  if(crm != 'crm036' && crm != 'crm012' && crm != 'crm041' && crm != 'crm046')
		  // if((selecttype === '2w' || service === 'Express Car Service 999' || service === 'Car dent / scratch removal' || service === 'Dent Removal 2499' || service === 'Dent 2999 1-2 Panels' || service === 'Dent 2999 3-4 Panels' || service === 'Dent 2999 5-6 Panels' || service === 'Dent 2999 7-8 Panels' || service === 'Dent Removal 2999') && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
		  // if(selecttype === '2w' && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
		  {
			  $('.loader').show();
			  $('.loader_overlay').show();
			  $.ajax({
            url : "ajax/project_crown.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc ,"veh_no": veh_no ,"service":service,"book_id":book_id,"crm_log_id":crm},
            success : function(data) {
				// console.log(data);
				$('.loader').hide();
			  $('.loader_overlay').hide();
				//alert(data);
                $('#mechanice').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrown	Error);
          }

        });
			
		  }
		  else
		  {
		  $.ajax({
            url : "ajax/get_mechanic_loc.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc ,"veh_no": veh_no ,"service":service},
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanice').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });
		}
	}
  });
});
$('.btn').on("click",function(){
	var id = $(this).data("bid");
	if(id!=null)
	{
		//alert(id);
		$.ajax({
			type: 'GET',
			url: 'ajax/firstresponse1.php',
			data: {
				bi: id
			}
		});
	}
});
</script>


</body>
</html>

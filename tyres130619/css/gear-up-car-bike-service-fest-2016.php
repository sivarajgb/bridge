<!DOCTYPE html>
<!--[if IE 8]><html class="ie ie8"> <![endif]-->
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Gear Up Car and Bike Service Fest 2016</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="GoBumpr-Team">
    <meta name="robots" content="noindex" />
    <style type="text/css">
	.table.options_booking td,img{vertical-align:middle}a,table{background-color:transparent}.table,label{max-width:100%}hr,img{border:0}body{margin:0}.col-xs-3,.col-xs-9{float:left}html{font-family:sans-serif;font-size:10px}aside,header,nav,section{display:block}a{color:#ffa800;text-decoration:none;outline:#000}strong{font-weight:700}h1{margin:.67em 0}h1,h3,h4,h5,ul{margin-bottom:10px}hr{height:0;box-sizing:content-box}button,input,select{margin:0;font-style:inherit;font-variant:inherit;font-weight:inherit;color:inherit;font-family:inherit;font-size:inherit;line-height:inherit}button{overflow:visible;-webkit-appearance:button}button,select{text-transform:none}input[type=checkbox]{box-sizing:border-box;padding:0}input[type=search]::-webkit-search-cancel-button,input[type=search]::-webkit-search-decoration{-webkit-appearance:none}table{border-spacing:0;border-collapse:collapse}td,th{padding:0}*,::after,::before{box-sizing:border-box}h1,h3,h4,h5{font-family:inherit;font-weight:500;line-height:1.1}h1,h3{margin-top:20px}h4,h5{margin-top:10px}h1{font-size:36px}h4{font-size:18px}h5{font-size:14px}ul{margin-top:0}@media (min-width:768px){.container{width:750px}}th{text-align:left}#social_footer,#social_footer ul,#social_footer ul li a,.box_style_1 h3.inner,.btn_full{text-align:center}.container{margin-right:auto;margin-left:auto;padding-right:15px;padding-left:15px}@media (min-width:992px){.container{width:970px}}@media (min-width:1200px){.container{width:1170px}}.row{margin-right:-15px;margin-left:-15px}.col-md-10,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-8,.col-md-9,.col-sm-10,.col-sm-12,.col-sm-3,.col-sm-9,.col-xs-3,.col-xs-9{position:relative;min-height:1px;padding-right:15px;padding-left:15px}.col-xs-9{width:75%}.col-xs-3{width:25%}@media (min-width:768px){.col-sm-10,.col-sm-12,.col-sm-3,.col-sm-9{float:left}.col-sm-12{width:100%}.col-sm-10{width:83.33333333%}.col-sm-9{width:75%}.col-sm-3{width:25%}}@media (min-width:992px){.col-md-10,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-8,.col-md-9{float:left}.col-md-12{width:100%}.col-md-10{width:83.33333333%}.col-md-9{width:75%}.col-md-8{width:66.66666667%}.col-md-4{width:33.33333333%}.col-md-3{width:25%}.col-md-2{width:16.66666667%}}.table{width:100%;margin-bottom:20px}.table>tbody>tr>td,.table>thead>tr>th{padding:8px;line-height:1.42857143;vertical-align:top;border-top-width:1px;border-top-style:solid;border-top-color:#ddd}.table>thead>tr>th{vertical-align:bottom;border-bottom-width:2px;border-bottom-style:solid;border-bottom-color:#ddd}.table>thead:first-child>tr:first-child>th{border-top-width:0}.table-striped>tbody>tr:nth-of-type(odd){background-color:#f9f9f9}label{font-weight:700;display:inline-block;margin-bottom:5px}input[type=search]{box-sizing:border-box;-webkit-appearance:none}input[type=checkbox]{margin:4px 0 0;line-height:normal}.form-control{background-image:none;line-height:1.42857143;display:block;width:100%;padding:6px 12px;background-color:#fff;border:1px solid #ccc;-webkit-box-shadow:rgba(0,0,0,.0745098) 0 1px 1px inset;box-shadow:rgba(0,0,0,.0745098) 0 1px 1px inset}.form-control::-webkit-input-placeholder{color:#999}.form-group{margin-bottom:15px}.navbar-form{padding:10px 15px;border-top-width:1px;border-top-style:solid;border-top-color:transparent;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:transparent;-webkit-box-shadow:rgba(255,255,255,.0980392) 0 1px 0 inset,rgba(255,255,255,.0980392) 0 1px 0;box-shadow:rgba(255,255,255,.0980392) 0 1px 0 inset,rgba(255,255,255,.0980392) 0 1px 0;margin:8px -15px}@media (min-width:768px){.form-inline .form-control{display:inline-block;vertical-align:middle}.navbar-form .form-control{display:inline-block;width:auto;vertical-align:middle}.navbar-form{width:auto;padding-top:0;padding-bottom:0;margin-right:0;margin-left:0;border:0;-webkit-box-shadow:none;box-shadow:none}}.container::after,.container::before,.row::after,.row::before{display:table;content:' '}.container::after,.row::after{clear:both}.pull-right{float:right!important}@media (min-width:1200px){.hidden-lg{display:none!important}}@media (max-width:767px){.hidden-xs{display:none!important}}@media (max-width:991px) and (min-width:768px){.hidden-sm{display:none!important}}@media (max-width:1199px) and (min-width:992px){.hidden-md{display:none!important}}#header_menu,.layer{display:none}.main-menu{position:relative;z-index:9;width:auto}.main-menu ul,.main-menu ul li{position:relative;margin:0;padding:0}.header-form{box-shadow:none!important}.layer{position:fixed;top:0;left:0;width:100%;min-width:100%;min-height:100%;background-color:#000;opacity:0;z-index:9999}@media only screen and (min-width:992px){.main-menu{width:auto}.main-menu ul li{display:inline-block}}@media only screen and (max-width:991px){#header_menu,.main-menu li{position:relative}#header_menu{text-align:center;padding:25px 15px 10px;display:block}.main-menu ul li{border-top-style:none;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#ededed;color:#fff}.main-menu li{display:block;color:#333!important}.main-menu ul>li{padding-bottom:0}.main-menu ul>li i{float:right}.main-menu{overflow:auto;left:-100%;bottom:0;width:55%;height:100%;opacity:0;position:fixed;background-color:#fff;z-index:999999;-webkit-box-shadow:rgba(50,50,50,.54902) 1px 0 5px 0;box-shadow:rgba(50,50,50,.54902) 1px 0 5px 0}.search_mobile{box-shadow:none;margin-left:20%;border-bottom-style:none!important}}@media only screen and (max-width:480px){.main-menu{width:100%;margin-bottom:20px!important}#menu_div{margin-bottom:.1px!important}}body,html{overflow-x:hidden}body,h1,h3,h4,h5{-webkit-font-smoothing:antialiased}body{background-color:#fff;font-size:12px;line-height:20px;font-family:Montserrat,Arial,sans-serif;color:#565a5c;background-position:initial initial;background-repeat:initial initial}h1,h3,h4,h5{color:#333}h3{font-size:22px}.box_style_1 h3.inner{margin:-30px -30px 20px;background-color:#565a5c;padding:10px 20px 10px 18px;color:#fff;border:1px solid #fff;border-top-left-radius:3px;border-top-right-radius:3px}#logo,.btn_full{margin-bottom:10px}hr,nav{margin-top:20px}hr{margin-bottom:20px;border-width:1px 0 0;border-top-style:solid;border-top-color:#ddd}.btn_1,.btn_full{font-family:inherit;outline:#000;border:none;background-color:#ffa800;background-position:initial initial;background-repeat:initial initial;color:#fff;text-transform:uppercase;font-weight:700;font-size:12px}.btn_1{padding:7px 20px;display:inline-block;border-radius:3px}.btn_full{padding:12px 20px;display:block;width:100%;border-radius:3px}#overlay,header #logo .logo_sticky{display:none}#logo{margin-top:10px}header{position:fixed;left:0;top:0;width:100%;z-index:99999;padding:10px 0}footer a{color:#fff}footer ul{list-style:none;margin:0;padding:0 0 20px}#social_footer ul{margin:0;padding:0 0 10px}#social_footer ul li{display:inline-block;margin:0 5px 10px}#social_footer ul li a{color:#fff;line-height:34px;display:block;font-size:16px;width:35px;height:35px;border:1px solid rgba(255,255,255,.298039);border-radius:50%}.parallax-content-2{position:absolute;left:0;bottom:0;z-index:999;padding:18px 0 20px;color:#333;font-size:13px;background-image:url(../img/shadow_single.png);width:100%;background-position:0 100%;background-repeat:repeat no-repeat}#overlay,.box_style_1{background-position:initial initial}#overlay,.box_style_1,.parallax-window{background-repeat:initial initial}.parallax-content-2 div h1{text-transform:uppercase;font-size:36px;font-weight:700;color:#333;margin:0}.table.options_booking td i{font-size:26px}.table{border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#ddd}.box_style_1{background-color:#fff;border:1px solid #ddd;margin-bottom:25px;padding:30px;position:relative;color:#666;border-radius:3px}.form-control{font-size:12px;color:#333;height:40px;border-radius:3px}#overlay,#preloader{top:0;left:0;right:0;width:100%;height:100%;bottom:0}#overlay{background-color:rgba(0,0,0,.4);position:fixed;z-index:1}.parallax-window{min-height:180px;position:relative;background-position:0 0}#preloader{position:fixed;background-color:#fff;z-index:999999}.sk-spinner-wave.sk-spinner{margin:-15px 0 0 -25px;position:absolute;left:50%;top:50%;width:50px;height:30px;text-align:center;font-size:10px}.sk-spinner-wave div{background-color:#ccc;height:100%;width:6px;display:inline-block;-webkit-animation:sk-waveStretchDelay 1.2s ease-in-out infinite}.sk-spinner-wave .sk-rect2{-webkit-animation:-1.1s}.sk-spinner-wave .sk-rect3{-webkit-animation:-1s}.sk-spinner-wave .sk-rect4{-webkit-animation:-.9s}.sk-spinner-wave .sk-rect5{-webkit-animation:-.8s}.dd-pointer{width:0;height:0;position:absolute;right:10px;top:50%;margin-top:-3px}.dd-pointer-down{border-right-width:5px;border-bottom-width:5px;border-left-width:5px;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-right-color:transparent;border-bottom-color:transparent;border-left-color:transparent;border-top-style:solid!important;border-top-width:5px!important;border-top-color:#999!important}input{text-transform:capitalize!important}.switch-light span span{display:none}@media only screen{.switch-light{display:block;height:30px;width:80px;position:relative;overflow:visible;padding:0;margin:auto}.switch-light *{box-sizing:border-box}.switch-light>span{line-height:30px;vertical-align:middle;height:30px;position:absolute;left:-100px;width:100%;margin:0;padding-right:100px;text-align:left}.switch-light a,.switch-light>span span{width:50%;display:block;position:absolute;top:0}.switch-light input{position:absolute;opacity:0;z-index:5}.switch-light>span span{left:0;z-index:5;margin-left:100px;text-align:center}.switch-light>span span:last-child{left:50%}.switch-light a{outline:0;right:50%;z-index:4;height:100%;padding:0}.switch-ios.switch-light{color:#868686}.switch-ios.switch-light a{left:0;width:30px;background-color:#fff;border:1px solid #d3d3d3;box-shadow:rgba(0,0,0,.0235294) 0 -3px 3px inset,rgba(0,0,0,.14902) 0 1px 4px,rgba(0,0,0,.0980392) 0 4px 4px;border-radius:100%}.switch-ios.switch-light>span span{width:100%;left:0;opacity:0}.switch-ios.switch-light>span span:first-of-type{opacity:1;padding-left:6px}.switch-ios.switch-light>span span:last-of-type{padding-right:50px}.switch-ios.switch-light>span::before{content:'';display:block;width:70%;height:100%;position:absolute;left:100px;top:0;background-color:#fafafa;border:1px solid #d3d3d3;box-shadow:rgba(0,0,0,.0980392) 0 1px 0 inset;border-radius:30px}}@media only screen and (-webkit-max-device-pixel-ratio:2) and (max-device-width:1280px){.switch-light{-webkit-animation:webkitSiblingBugfix 1s infinite}}
	</style>

    <!-- Favicons-->
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="/img/gobumpr/home_page/GoBumpr_57.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="/img/gobumpr/home_page/GoBumpr_72.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="/img/gobumpr/home_page/GoBumpr_114.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="/img/gobumpr/home_page/GoBumpr_144.png">
   
    <meta property="og:title" content="Gear up Car and Bike Service Fest 2016">
    <meta property="og:description" content="">
	<meta property="og:url" content="https://gobumpr.com/offers/gear-up-car-bike-service-fest-2016">
    <meta property="og:site_name" content="GoBumpr">
	<meta property="og:site" content="https://gobumpr.com">
	<meta property="og:type" content="website">
    <meta name="robots" content="noindex, nofollow">

<style type="text/css">
.icons_first {
	width:50px;
	height:50px;
}
.sp-arrows {
	display:none;
}
.main-menu ul li {
	margin-left:-3px;
	margin-right: -1px;
}
li#search_mobile .form-control {
	border-left:0;
	outline:0;
}
.dd-option-image {
	width:25px !important;
	height:25px !important;
}
.dd-selected-image {
	width:25px !important;
	height:25px !important;
}
.dd-options ul {
	width:100px;
	margin-left: -3px;
}
.main-menu ul ul li {
	width:100%;
}
.main-menu ul ul, .main-menu ul .menu-wrapper {
	margin-left:-3px;
}
.main-menu ul ul, .main-menu ul .menu-wrapper  {
	min-width:101px;
	overflow:hidden;
}
.dd-option-text {
    line-height: 25px !important;
}
.heading_tooltip {
	text-transform:uppercase;
}
.parallax-window {
    min-height: 150px !important;
}
.parallax-content-2 {
	padding-bottom: 0px !important;
}
.labour_free {
	font-size:0.8em !important;
}
@media only screen and (max-width: 480px) {
	.parallax-window {
		max-height: 30%;
		min-height: 30%;
	}
.parallax-window {
    min-height: 120px !important;
    height: 120px !important;
}
}
</style>        

    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  
<!-- Google Analytics Code -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '582926561860139');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
</head>
<body style="background:#fff;">

<!--[if lte IE 8]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="//browsehappy.com/">upgrade your browser</a>.</p>
<![endif]-->


<?php include "website-php-files/include/header.php" ?>

    <section class="parallax-window" data-parallax="scroll" data-natural-width="700" data-natural-height="180" style="background-color:#fff">
    <div class="parallax-content-2" style="background-image:none;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-10">
                <h1>Gear up Car and Bike Service Fest</h1>
                <h5> OCT 2 - OCT 9, 2016</h5>
                </div>
            </div>
    </div>
    </div>
    </section><!-- End section -->
 
    <div id="img" style="background-image:url(../img/gobumpr/repair-jobs/gear-up-2nd-oct.jpg); background-repeat:no-repeat; width:100%; height:100%; background-size:cover">    
<hr>

    <div class="container">
    <div class="row">
        <div class="col-md-8" style="padding-top:10px">
        <div class="textual">
        	<!--<p style="color:#fff; font-size:25px;"> 
            	Doorstep Car Care Services at your Convenience!
            </p>
        	<p> 
            	<ul style="color:#fff; font-size:22px;">
                <li>Get a Profesional Wash for your Car now</li><br>
                <li>Premium 3M and Wurth Products are used</li><br>
                <li>Subscription Packages are also available</li>
                </ul>
            </p>-->
                <div class="hidden-xs hidden-sm" style="margin-top:100%"></div>

        </div>
        </div>
        <aside class="col-md-4" style="z-index:999" id="fix_booking" >
        <div class="theiaStickySidebar">
        <div class="box_style_1">
            <h3 class="inner">- Avail Offer -</h3>
<form role="form" method="post" id="1st_book" name="1st_book" action="">            
            <div class="row" id="1st_booking_form">
               <div class="col-md-12 col-sm-12" style="top:10px;">
	<div class="form-group">
    <label> Offer </label>
	<select class="ddslick" name="ServiceType" id="ddslick_id">
		<option value="Car Wash" data-imagesrc="/img/gobumpr/services_offered/car_Wash_90.png">Car Wash @ Rs. 199/-</option>
	</select>
	</div>
	</div>
               <div class="col-md-12 col-sm-12">                  
                 <div class="form-group">
                      <label>Locality</label>
                      <input class="form-control ui-autocomplete" id="locality_form" type="text" name="locality" required placeholder="Select Locality"><span id="message"></span>
  <span id="dropdown_icon" class="dd-pointer dd-pointer-down" style=" margin-right:15px; margin-top:1%"></span>
                </div>
                </div>
               <div class="col-md-12 col-sm-12">
                    <div class="form-group">
                        <label style="position: static"><i class="icon-calendar-7 "></i>Date of Service</label>
                        <input id="dos_book" class="date-pick form-control" data-error="Select a date" data-date-format="M d, D" type="text" name="dos_book">
                    </div>                                              
                </div>

               <div class="col-md-12 col-sm-12">
                 <div class="form-group"> 
                      <label>Payment Option</label>
                      <select class="form-control" id="payment_opt" name="payment_opt">
                      <option selected value="1">Online Payment</option>
                      <option value="0">Cash on Service Fulfillment</option>
                      </select>
                </div>
               </div>

               <div class="col-md-12 col-sm-12">
                    <div class="form-group">
                    <table class="table table-striped options_booking">
                    <thead>
                    <tr>
                        <th colspan="3">
                            Add on Services
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td width="6%">
                            <i class="icon_set_1_icon-51"></i>
                        </td>
                        <td width="59%">
                            Under Chassis Wash | <strong>Rs. 100</strong>
                        </td>
                        <td width="35%">
                            <label class="switch-light switch-ios pull-right">
                            <input type="checkbox" name="uc_wash" id="uc_wash" value="Yes">
                            <span>
                            <span>No</span>
                            <span>Yes</span>
                            </span>
                            <a></a>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="icon_set_1_icon-51"></i>
                        </td>
                        <td>
                            Interior Vacuuming & Polish | <strong>Rs. 100</strong>
                        </td>
                        <td>
                            <label class="switch-light switch-ios pull-right">
                            <input type="checkbox" name="vac_pol" id="vac_pol" value="Yes">
                            <span>
                            <span>No</span>
                            <span>Yes</span>
                            </span>
                            <a></a>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="icon_set_1_icon-51"></i>
                        </td>
                        <td>
                            Exterior Wax Polish | <strong>Rs. 150</strong>
                        </td>
                        <td>
                            <label class="switch-light switch-ios pull-right">
                            <input type="checkbox" name="wax_pol" id="wax_pol" value="Yes">
                            <span>
                            <span>No</span>
                            <span>Yes</span>
                            </span>
                            <a></a>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="icon_set_1_icon-51"></i>
                        </td>
                        <td>
                            Pick-up (3 KM) | <strong>Rs. 100</strong>
                        </td>
                        <td>
                            <label class="switch-light switch-ios pull-right">
                            <input type="checkbox" name="pick_up_y_n" id="pick_up_y_n" value="Yes">
                            <span>
                            <span>No</span>
                            <span>Yes</span>
                            </span>
                            <a></a>
                            </label>
                        </td>
                    </tr>
                    </tbody>
                    </table>
                    </div>                                              
                </div>

               <div class="col-md-12 col-sm-12">
                 <div class="form-group">  
                      <label>Mobile Number</label>
                      <input class="form-control" type="phone" name="mobile_num_book" id="mobile_num_book" maxlength="10" required placeholder="Enter Mobile Number">
                </div>
               </div>
               

<?php 
require_once 'detector/detect.php';

$device_type = Detect::deviceType();
$ip_address = Detect::ip();
$host_ip = Detect::ipHostname();
$ip_org = Detect::ipOrg();
$ip_country = Detect::ipCountry();
$os = Detect::os();
$browser = Detect::browser();
$user_brand = Detect::brand();


$user_agent = "type: ".$device_type.", IP: ".$ip_address.", IP Host: ".$host_ip.", IP Org: ".$ip_org.", IP Country: ".$ip_country.", OS: ".$os.", Browser: ".$browser.", User Brand: ".$user_brand;

?>               
<input type="hidden" type="text" value="<?php echo $user_agent?>" name="user_agent" id="user_agent">               
            <br>
            <center><button type="submit" class="btn_full" id="1st_submit"> Book Now </button></center>
            </div>
</form>
<div class="row" id="thankyou" style="display:none">
<div  class="col-md-12" style="margin-top:25%; margin-bottom:25%;">
<h4> Thank you for sharing your details with us, We will get into touch with you soon! </h4>
</div>
</div>

</div>
</div>
        </aside>
    </div><!--End row -->
    </div><!--End container -->
 </div><!--img-->  
<input type="hidden" id="VehicleType_id" value="4w">


<?php include "website-php-files/include/footer.php"; ?>

<div id="toTop"></div><!-- Back to top button --> 
<div id="overlay"></div><!-- Mask on input focus -->   
	<script src="https://code.jquery.com/jquery-1.10.2.min.js" integrity="sha256-C6CB9UYIS9UJeqinPHWTHVqh/E1uhG5Twh+Y5qFQmYg=" crossorigin="anonymous"></script>
	<script src="/js/jquery-1.11.2.min.js"></script>
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" integrity="sha256-xNjb53/rY+WmG+4L6tTl9m6PpqknWZvRt0rO1SRnJzw=" crossorigin="anonymous"></script>
<script type="text/javascript" src="/offers/offers_js.js"></script>

<noscript id="deferred-styles">
    <!-- CSS -->
	<link rel="stylesheet" href="/css/base.min.css">
	<link rel="stylesheet" href="/css/bootstrap.css">
	<link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="/css/custom.min.css">
	<link rel="stylesheet" href="/css/icons.css?ver=3">
	<link rel="stylesheet" href="/css/autocomplete.css">
    <link rel="stylesheet" href="/css/date_time_picker.css">
	<link rel="stylesheet" href="/css/tabs_home.css">

     <!-- Google web fonts -->
   <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
   <link href='//fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
   <link href='//fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'>

</noscript>
<script>
      var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("deferred-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
      };
      var raf = requestAnimationFrame || mozRequestAnimationFrame ||
          webkitRequestAnimationFrame || msRequestAnimationFrame;
      if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
      else window.addEventListener('load', loadDeferredStyles);
</script>

    <script src="https://gobumpr.com/js/tabs.js"></script>
	<script>new CBPFWTabs( document.getElementById( 'tabs2' ) );</script>


<!-- Fresh Chat -->
<script type='text/javascript'>var fc_JS=document.createElement('script');fc_JS.type='text/javascript';fc_JS.src='https://assets1.freshchat.io/production/assets/widget.js?t='+Date.now();(document.body?document.body:document.getElementsByTagName('head')[0]).appendChild(fc_JS); window._fcWidgetCode='GjbNEMoe';window._fcURL='https://gobumpr.freshchat.io';</script>

 <!-- Common scripts -->
<script src="/js/common_scripts_min.js"></script>
<script src="/js/functions.js"></script>
	<link rel="stylesheet" href="/css/autocomplete.css">


<script type="text/javascript">
 function showDiv(elem){ 
    if(elem.value == 1){ 
        document.getElementById('click').style.display = "block";
}
else{
    document.getElementById('click').style.display = "none";
}
     }

 function showDivDesc(elem){ 
    if(elem.value == 1){ 
        document.getElementById('description_id').style.display = "block";
}
else{
    document.getElementById('description_id').style.display = "none";
}
     }

</script>    

 <!-- Date and time pickers -->
<script src="/js/jquery.sliderPro.min.js"></script>
<script type="text/javascript">
	$( document ).ready(function( $ ) {
		$( '#Img_carousel' ).sliderPro({
			width: 890,
			height: 500,
			fade: true,
			arrows: true,
			buttons: false,
			fullScreen: false,
			smallSize: 500,
			startSlide: 0,
			mediumSize: 1000,
			largeSize: 3000,
			thumbnailArrows: true,
			autoplay: false
		});
	});
</script>


 <!-- Date and time pickers -->
 <script src="/js/bootstrap-datepicker.js"></script>
 <script>
    var date = new Date();
    date.setDate(date.getDate());

    $('.date-pick').datepicker({
    startDate: date
    });
    $('input.date-pick').datepicker('setDate', 'today');
   </script>
 <!-- Carousel -->

<!--Review modal validation -->
<script src="/js/validate.js"></script>

<!-- Fixed sidebar -->
<script src="/js/theia-sticky-sidebar.js"></script>
<script>
    jQuery('#fix_booking').theiaStickySidebar({
      additionalMarginTop: 10
    });
</script>
<script>
jQuery("#SearchForm").submit(function(e) {
     var self = this;
     e.preventDefault();
	 localStorage.removeItem('sort_id');
	 localStorage.removeItem('option');
     self.submit();
     return false; //is superfluous, but I put it here as a fallback
});
</script>
<script>
$(function() {
	$( "#locality" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "/locality.php",
                data: {
                    term: request.term
                },
                dataType: 'json'
            }).done(function(data) {
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
        delay: 2,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
		
        }).focus(function() {
		$(this).autocomplete("search");
	});
});
$(function() {
	$( "#locality_form" ).autocomplete({
	source: function(request, response) {
            $.ajax({
                url: "/locality.php",
                data: {
                    term: request.term
                },
                dataType: 'json'
            }).done(function(data) {
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
        delay: 2,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
	});
});
</script>
<script>
$(function() {
	$( "#BrandModel" ).autocomplete({
	source: function(request, response) {
            $.ajax({
                url: "/getBrands.php",
                data: {
                    term: request.term,
					extraParams:$('input[name=VehicleType]').val()
                },
                dataType: 'json'
            }).done(function(data) {
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
        delay: 2,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
	});
});

$(function() {
		    $( "#BrandModel_form" ).autocomplete({

source: function(request, response) {
            $.ajax({
                url: "/getBrands.php",
                data: {
                    term: request.term,
					extraParams:$('#VehicleType_id').val()
                },
                dataType: 'json'
            }).done(function(data) {
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
        delay: 2,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script>

<script src="/js/jquery.ddslick.js"></script>
<script>
$("select.ddslick").each(function(){
            $(this).ddslick({
                showSelectedHTML: true,
                onSelected: function(data){
						//$('.dd-selected-value').attr('name', 'option[' + data.selectedData.val + ']');
						//console.log(data.selectedData);
					if($('input[name=VehicleType]').val()!=$('#vehicle_type_hidden').val()) {
						$("#BrandModel").val("");
						if($('input[name=VehicleType]').val()=='4w') {
							$('#bike_options_ddslick').hide();
							$('#car_options_ddslick').show();
							//$('input[name=ServiceType]').val(data.selectedData.value);
							//console.log($('input[name=ServiceType]').val());
						}
						else {
							$('#car_options_ddslick').hide();
							$('#bike_options_ddslick').show();
							$('input[name=ServiceType]').val(data.selectedData.value);
							//console.log($('input[name=ServiceType]').val());
						}
					}
					else {
						//$("#BrandModel").val("");
						if($('input[name=VehicleType]').val()=='4w') {
							$('#bike_options_ddslick').hide();
							$('#car_options_ddslick').show();
							//$('input[name=ServiceType]').val(data.selectedData.value);
							//console.log($('input[name=ServiceType]').val());
						}
						else {
							$('#car_options_ddslick').hide();
							$('#bike_options_ddslick').show();
							$('input[name=ServiceType]').val(data.selectedData.value);
							//console.log($('input[name=ServiceType]').val());
						}
					}
				}
            });
        });
</script>

<script>
$(function() {
    $("#entry_submit").on("click", function(event) {
		$("#entry_text").hide();
		$("#1st_booking_form").show("slide", { direction: "left" }, 300);	
	});
});
</script>
  </body>
</html>
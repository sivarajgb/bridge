<?php
include 'config.php';
$conn = db_connect2();
session_start();
error_reporting(0);
$city = $_GET['city'];
 $cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

$_SESSION['crm_city'] = $city;
 $_SESSION['crm_cluster'] = $cluster;
 
$go_date = $_POST['go_date'];
$html.="<table class='user_history table'>
			<thead>
				<tr>
					<th>ShopName</th>
					<th>BookingId</th>
					<th>CreditAmount</th>
					<th>LogDate</th>
				</tr>
			</thead>
			<tbody>";
$sql_b2b = "SELECT ut.booking_id,bt.b2b_credit_amt ,cast(bt.b2b_log as date) as date,ut.mec_id,amt.b2b_shop_name 
from go_bumpr.tyre_booking_tb  ut 
join b2b.b2b_booking_tbl_tyres bt on ut.booking_id=bt.gb_booking_id
left join b2b.b2b_mec_tbl amt on bt.b2b_shop_id=amt.b2b_shop_id
WHERE cast(bt.b2b_log as date) ='$go_date' AND ut.booking_status='2' AND ut.axle_flag='1' AND bt.b2b_shop_id NOT IN (1670,1014,1035)"; 
//echo $sql_b2b;
$res_b2b = mysqli_query($conn,$sql_b2b);
while($row=mysqli_fetch_assoc($res_b2b)){
	$html.="<tr>
				<td>".$row[b2b_shop_name]."</td>
				<td>".$row[booking_id]."</td>
				<td>".$row[b2b_credit_amt]."</td>
				<td>".$row[date]."</td>
	
	</tr>";
}

$html.="</tbody>
</table>";
header('Content-Type: application/json');

echo json_encode($html);
exit;

?>
<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id']))) {
	header('location:logout.php');
	die();
}

$user_id = base64_decode($_GET['u']);
$veh_id = base64_decode($_GET['v']);

$cluster_admin = $_SESSION['cluster_admin'];

if(isset($_POST['booking_submit'])){
  $log= date('Y-m-d H:i:s'); 
  $mec_id=mysqli_real_escape_string($conn,$_POST['mechanic']);

  $sql_mec = "SELECT shop_name FROM admin_mechanic_table WHERE mec_id='$mec_id' ";
  $res_mec = mysqli_query($conn,$sql_mec);
  $row_mec=mysqli_fetch_object($res_mec);
  $shop_name = $row_mec->shop_name;
  $locality=mysqli_real_escape_string($conn,$_POST['location']);

  $sql_loc = "SELECT city FROM localities WHERE localities='$locality'";
  $res_loc = mysqli_query($conn,$sql_loc);
  $row_loc = mysqli_fetch_array($res_loc);
  $city = $row_loc['city'];

  $vehicle_type=mysqli_real_escape_string($conn,$_POST['veh_b']);
  $user_veh_id=mysqli_real_escape_string($conn,$_POST['veh_no']);

  $sql_veh_no = mysqli_query($conn,"SELECT vehicle_id,reg_no,type FROM user_vehicle_table WHERE id='$user_veh_id' ");
  $row_veh_no = mysqli_fetch_object($sql_veh_no);
  $vehicle_id = $row_veh->vehicle_id;
  $veh_no = $row_veh_no->reg_no;
  $vehicle_type = $row_veh_no->type;

  $service_type=mysqli_real_escape_string($conn,$_POST['service_type']);
  $amt=mysqli_real_escape_string($conn,$_POST['amount']);
  $service_date=date('Y-m-d',strtotime(mysqli_real_escape_string($conn,$_POST['service_date'])));
  $next_service_date=date('Y-m-d',strtotime(mysqli_real_escape_string($conn,$_POST['next_service_date'])));
  $service_description=mysqli_real_escape_string($conn,$_POST['description']);
  $pick_up=mysqli_real_escape_string($conn,$_POST['pickup']);
  //insert into booking table
  $query = "INSERT INTO user_booking_tb(user_id,mec_id,shop_name,rep_name,rep_number,vech_id,user_veh_id,vehicle_type,user_vech_no,service_type,initial_service_type,amt,service_description,service_date,status,source,pick_up,crm_log_id,crm_allocate_id,crm_update_time,log,flag,booking_status,crm_update_id,followup_date,axle_flag,locality,city) VALUES ('$user_id','$mec_id','$shop_name','karthik','+919941413598','$vehicle_id','$user_veh_id','$vehicle_type','$veh_no','$service_type','$service_type','$amt','$service_description','$service_date','admin','Hub Booking','$pick_up','$crm_log_id','$crm_log_id','$log','$log','0','1','$crm_log_id','$next_service_date','0','$locality','$city')";
  $sqlins1 = mysqli_query($conn,$query)or die(mysqli_error($conn));

  $mec_details =mysqli_query($conn,"SELECT shop_name FROM admin_mechanic_table WHERE mec_id='$mec_id'");
  $row_mec=mysqli_fetch_object($mec_details);
  $shop_name=$row_mec->shop_name;        
        
//********sms start*****************
$sql=mysqli_query($conn,"SELECT mobile_number,name FROM user_register WHERE reg_id='$user_id'");
while ($row=mysqli_fetch_array($sql)) {
      $to=$row['mobile_number'];
      $user_name=$row['name'];
}
//  $to="9505913666";
//$user_name="Sirisha";
$sql_contact = "SELECT * FROM go_axle_service_price_tbl WHERE service_type = '$service_type' AND type='$vehicle_type'";
$res_contact = mysqli_query($conn,$sql_contact);
  $row_contact = mysqli_fetch_array($res_contact);
  
  $column = ucwords($city).'_support';
  
  $contact = $row_contact[$column];
  
  if($contact == ''){
      $contact = '9003251754';
  } 

  $sms = "Dear ".$user_name.", thanks for booking with us. Our representative will get in touch with you shortly. For help, please call us at ".$contact.". Let's GoBumpr!";
  
  $to = str_replace(' ', '%20', $to);
  $user_name = str_replace(' ', '%20', $user_name);
  $sms = str_replace(' ', '%20', $sms);


$ch=curl_init("http://203.212.70.200/smpp/sendsms?username=northautohttp&password=north123&to=%22".$to."%22&from=GOBMPR&udh=&text=".$sms."");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result=curl_exec($ch);
curl_close($ch);
$post = '{"status":"'.$result.'"}';
  
//********sms end**************** 
if($cluster_admin == '1'){
    header("Location:mleads.php");
  } 
  else if($flag == 1){
    header("Location:aleads.php");
  }
  else{
    header("Location:leads.php");
  }
  

}
?>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
  <title>GoBumpr Bridge</title>

<!-- validate -->
<script sr="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />

<!-- masking -->
<script type="text/javascript" src="js/inputmask.js"></script>
<script type="text/javascript" src="js/jquery.inputmask.js"></script>
  <script type="text/javascript" src="js/inputmask.extensions.js"></script>
  <!-- auto complete -->
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/jquery-ui.structure.min.css">
<link rel="stylesheet" href="css/jquery-ui.theme.min.css">
<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>

  <style>
	<!-- auto complete -->
	@charset "utf-8";.ui-autocomplete{z-index:1000 !important;cursor:default;list-style:none;}
	.ui-widget{}
	.ui-autocomplete{overflow-y:auto;overflow-x:hidden;}
	.ui-menu{width:0px;display:none;}
	.ui-autocomplete > li{padding:10px;padding-left:10px;}
	ul{margin-bottom:0;}
	.ui-autocomplete > li.ui-state-focus{background-color:#DDD;}
	.ui-autocomplete .ui-front .ui-menu .ui-widget .ui-widget-content{border:0;}
	.ui-helper-hidden-accessible{display:none;}
	.gobumpr-icon{font-style:normal;font-weight:normal;speak:none;display:inline-block;text-decoration:inherit;text-align:center;font-variant:normal;text-transform:none;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
	.ui-widget{background-color:white;width:100%;}
	.ui-widget-content{padding-left:1px;display:block;width:20px;position:relative;line-height:12px;max-height:210px;border:.5px solid #DADADA;}
	.ui-widget{}
	.ui-autocomplete { position: absolute; cursor: default;}


<!-- vehicle type -->
.bike,
.car{
  cursor: pointer;
  user-select: none;
  -webkit-user-select: none;
  -webkit-touch-callout: none;
}
.bike > input,
.car > input{ /* HIDE ORG RADIO & CHECKBOX */
  visibility: hidden;
  position: absolute;
}
/* RADIO & CHECKBOX STYLES */
.bike > i,
.car > i{     /* DEFAULT <i> STYLE */
  display: inline-block;
  vertical-align: middle;
  width:  16px;
  height: 16px;
  border-radius: 50%;
  transition: 0.2s;
  box-shadow: inset 0 0 0 8px #fff;
  border: 1px solid gray;
  background: gray;
}

label > input{ /* HIDE RADIO */
  visibility: hidden; /* Makes input not-clickable */
  position: absolute; /* Remove input from document flow */
}
label > input + img{ /* IMAGE STYLES */
  cursor:pointer;
  border:2px solid transparent;
}
label > input:checked + img { /* (RADIO CHECKED) IMAGE STYLES */
   border-radius:12px;
   padding:5px;
   background-color:#ffa800;
  box-shadow: 0 0 3px 0 #394;
}
.borderless td, .borderless th {
    border: none !important;
}
#datepick > span:hover{cursor: pointer;}
.datepicker:before {
content: '';
display: inline-block;
border-left: 7px solid transparent;
border-right: 7px solid transparent;
border-bottom: 7px solid #ccc;
border-bottom-color: transparent !important;
position: absolute;
top: -7px;
left: 190px;  // I made a change here 
}

.datepicker:after {
content: '';
display: inline-block;
border-left: 6px solid transparent;
border-right: 6px solid transparent;
border-top-color: transparent !important;
border-top: 6px solid #ffffff;
position: absolute;
bottom: -6px;
left: 191px;  // I made a change here 
}
.floating-box {
	 display: inline-block;
	 margin: 22px;
 padding: 22px;
 width:203px;
 height:105px;
 box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  font-size: 17px;
}
select{
    color: red;
}
option{
  height: 25px;
}
option:hover {
  box-shadow: 0 0 10px 100px #ddd inset;
}
</style>

</head>
<body>
<?php include_once("header.php"); ?>
<script>
	$(document).ready(function(){
		$('#city').hide();
	})
</script>
<div class="overlay" data-sidebar-overlay></div>

<div class="padding"></div>

<div align="center" style="margin-top:142px;">
   <h3> Please add a Booking to continue!</h3>
   <button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_booking"  style="position:relative;background-color:#39B8AC;width:140px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Add Booking</button>
</div>
<!-- add booking -->
   <!-- Modal -->
<div class="modal fade" id="myModal_booking" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Add Booking(<?php echo $user_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="add_booking" class="form" method="post" action="">
<div class="row" align="center">
<div  id="veh_t" class="col-xs-6 col-xs-offset-3 form-group">

  <label class="bike">
  <input id="veh_b" type="radio" name="veh_b" value="2w" checked/>
  <img id="veh_b" src="images/bike.png">
</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label class="car">
  <input id="car" type="radio" name="veh_b" value="4w" />
  <img id="veh_b" src="images/car.png">
</label>
</div></div>

<div class="row"></div>

<div class="row">
<div id="b_m" class="col-xs-5 col-xs-offset-1 form-group">
<select class="form-control" id="veh_no" name="veh_no">
<option selected value="">Vehicle</option>
 </select>
</div>

<div class="col-xs-5 form-group" id="service">
<div class="ui-widget">
    <input class="form-control autocomplete" id="service_type" type="text" name="service_type" placeholder="Service Type" required>
</div>
</div>
</div>
<div class="row"></div>

<div class="row">
<div class="col-xs-9 col-xs-offset-1 form-group" id="loc">
        <div class="ui-widget">
        	<input class="form-control autocomplete" id="location" type="text" name="location" placeholder="Start typing Location..." required>
        </div>
</div>
        <div><p class="btn btn-sm" id="go" style="margin-left:-8px;background-color:#B2EBF2;">Go</p></div>
</div> 

<div class="row"></div>


<div class="row">
<div class="col-xs-6 col-xs-offset-1 form-group" id="mec">

<select class="form-control" id="mechanic" name="mechanic" required>
<option selected>Select Mechanic</option>
</select>
</div>

<div class="col-xs-4 form-group">
<input class="form-control" type="number" id="amount" name="amount" placeholder="Amount">
</div>
</div>

<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-1 form-group">
<label> Service Date</label></div>
<div class="col-xs-3  form-group">
<input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="service_date" name="service_date" required>
</div>

<div class="col-xs-2 form-group">
<label> Next Service Date</label></div>
<div class="col-xs-3  form-group">
<input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="next_service_date" name="next_service_date" required>
</div>
</div>
<div class="row"></div>

<div class="row">
<div class="col-xs-10 col-xs-offset-1 form-group" >
<textarea class="form-control" maxlength="100" id="description" name="description" placeholder="Description..." ></textarea>
</div>
</div>

<div class="row"></div>
<div class="row">
<div class="col-xs-4 col-xs-offset-2 form-group">
<label>PickUp</label>
  <input class="form-group" type="radio" id="pickup" name="pickup" value="1" >&nbsp;Yes&nbsp;</input>
  <input class="form-group" type="radio" id="pickup" name="pickup" value="0" checked>&nbsp;No</input>
</div>

<div class="col-xs-4  form-group">
<label>SMS</label>
<input class="form-group" type="radio" id="sms" name="sms" value="1" checked>&nbsp;Yes&nbsp;</input>
<input class="form-group" type="radio" id="sms" name="sms" value="0" >&nbsp;No</input></div>
</div>

<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<input class="form-control" type="submit" id="booking_submit" name="booking_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>

</form>

</div>
</div>
</div>
</div>
<!-- jQuery library -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- brand and model -->
 <script>
$(function() {
		    $( "#BrandModelid" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_brandmodel.php",
                data: {
                    term: request.term,
					extraParams:$('#veh:checked').val()
                },
                dataType: 'json'
            }).done(function(data) {
				console.log(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#BrandModelid").next(),
        delay: 1,
        minLength: 1,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "9999999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script>
<!-- brand and model  in booking -->
 <script>
$(function() {
		    $( "#model" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_brandmodel.php",
                data: {
                    term: request.term,
					extraParams:$('#veh_b:checked').val()
                },
                dataType: 'json'
            }).done(function(data) {
				console.log(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#model").next(),
        delay: 1,
        minLength: 1,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "9999999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script>

<!-- select Vehicle Number -->
<script>
$(document).ready(function($) {
 $(document).on('change', '[name="veh_b"]' , function(){
  	var selectvalue = $('[name="veh_b"]:checked').val();
	var selectuser = "<?php echo $user_id; ?>";
 // console.log(selectuser);
	$("#veh_no").empty();
		 $("div.veh").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_veh.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selectvalue": selectvalue , "selectuser" : selectuser},
            success : function(data) {
				// alert(data);
                $('#veh_no').append(data);
                $("#location").autocomplete('close').val('');
                 $("#service_type").autocomplete('close').val('');
                  $("#mechanic").empty();
                  $("#mechanic").append('<option value="">Select Mechanic</option>');
            },
          error: function (xhr, ajaxOptions, thrownError) {
         //   alert(xhr.status + " "+ thrownError);
          }

        });

    });
});
</script>
<!-- select vehicle on page loaad -->
<script>
$(document).ready(function($) {
  	var selectvalue = $('[name="veh_b"]:checked').val();
	var selectuser = <?php echo $user_id; ?>;
	$("#veh_no").empty();
		 $("div.veh").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_veh.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selectvalue": selectvalue , "selectuser" : selectuser},
            success : function(data) {
				// alert(data);
                $('#veh_no').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

});
</script>
<!-- select Service type -->
 <script>
$(function() {
			    $( "#service_type" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_service_type.php",
                data: {
                    term: request.term,
					//type : $('#veh_b:checked').val()
						type : $('#veh_b:checked').val(),
					vtype: <?php echo $veh_id; ?>,
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#service_type").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script>

<!-- select Location of service center -->
 <script>
$(function() {
			    $( "#location" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_location.php",
                data: {
                    term: request.term,
					//type : $('#veh_b:checked').val()
					vtype: $('[name="veh_b"]:checked').val(),
					service:$('#service_type').val()
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script>

<!-- select Mechanic - ->
<script>
$(document).ready(function($) {
 $(document).on('change', '[name="veh_b"]' , function(){
  	var selecttype = $('[name="veh_b"]:checked').val();
  //	var selectservice = $("#service_type").val();
//	var selecttype = $("#veh_b").val();
	$("#mechanic").empty();
		 $("div.mec").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_mechanic.php",  // create a new php page to handle ajax request
            type : "POST",
         //   data : {"selectservice": selectservice , "selecttype": selecttype},
            data : {"selecttype": selecttype},
            success : function(data) {
			//	console.log(data);
				//alert(data);
                $('#mechanic').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
         //   alert(xhr.status + " "+ thrownError); 
          }

        });

    });
});
</script>-->
<!-- select Mechanic on page load- ->
<script>
$(document).ready(function($) {
  	var selecttype = $('[name="veh_b"]:checked').val();
  //	var selectservice = $("#service_type").val();
//	var selecttype = $("#veh_b").val();
	$("#mechanic").empty();
		 $("div.mec").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_mechanic.php",  // create a new php page to handle ajax request
            type : "POST",
         //   data : {"selectservice": selectservice , "selecttype": selecttype},
            data : {"selecttype": selecttype},
            success : function(data) {
			//	console.log(data);
				//alert(data);
                $('#mechanic').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
          //  alert(xhr.status + " "+ thrownError); 
          }

        });

});
</script>-->


<!-- (old)select Mechanic Based On Location - ->
<script>
$(document).ready(function($) {
 $("#location").on("autocompletechange", function(){
	 console.log("ad");
  	var selectservice = $("#service_type").val();
	var selecttype = $("#veh_b").val();
	var selectloc = $("#location").val();
	$("#mechanic").empty();
		 $("div.mec").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_mechanic_loc.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selectservice": selectservice , "selecttype": selecttype , "selectloc" : selectloc },
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanic').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
          //  alert(xhr.status + " "+ thrownError);
          }

        });

    });
});
</script> -->
<!-- select Mechanic Based On Location -->
<script>
$(document).ready(function($) {
 $("#location").on("autocompletechange", function(){
  //	var selectservice = $("#service_type").val();
	var selecttype = $('[name="veh_b"]:checked').val();
	var selectloc = $("#location").val();
    var veh_no = $("#veh_no").val();
    var service = $("#service_type").val();
	$("#mechanic").empty();
		 $("div.mec").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_mechanic_loc.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc ,"veh_no":veh_no,"service":service},
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanic').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

    });
});
</script>

<script>
 var date = new Date();
date.setDate(date.getDate());

$('.datepicker').datepicker({
    autoclose: true,
    startDate: date
});
if($('input.datepicker').val()){
    $('input.datepicker').datepicker('setDate', 'today');
 } 
 </script>
 <!-- select veh before fetching models -->
 <script>
 $(document).ready(function(){
   $("#BrandModelid").click(function(){
     var veh = $('input[name=veh]:checked').val();
     //console.log(veh);
     if(veh == null){
       //console.log(veh);
       alert("Please select vehicle type to get vehicle models!");
     }
   });
 })
 </script>
<script>
 $(document).ready(function(){
$('input[name=veh]').change(function(){
  $("#BrandModelid").val("");
  $("#location").autocomplete('close').val('');
  $("#mechanic").empty();
  $("#mechanic").append('<option value="">Select Mechanic</option>');
});
 });
 </script>
 <!-- check if locality is empty -->
<script>
$(document).ready(function($) {
  $("#mechanic").click(function(){
    if($("#location").val()== ''){
      $("#mechanic").empty();
      $("#mechanic").append('<option value="">Select Mechanic</option>');
      alert("Please select a Location to get mechanics!!!");
    }
  });
});
</script>
<script>
$(document).ready(function($) {
  $("#go").click(function(){
    if($("#location").val()== ''){
      alert("Oops no location has been selected!!!");
    }
  });
});
</script>
</body>
</html>



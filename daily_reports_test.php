<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id']))) {
	
	header('location:logout.php');
	die();
}
?>

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>GoBumpr Bridge</title>


  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


  <!-- Include Date Range Picker -->
  <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

  <!-- table sorter -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js"></script>

<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>

<style>
/*home page blocks */
.floating-box1 {
 display: inline-block;
}
.navbar-fixed-top{
  z-index:100 !important;
}
.upper-div{
  z-index:999999 !important;
}
#range > span:hover{cursor: pointer;}
 /* table */
#tbody{
   font-size:15px !important;
  border:1.5px solid #c4b8b8 !important;
  
}
thead:hover{
	cursor:pointer;
}

.results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px;
  color:#9E9E9E;
}
#tbody, tbody tr {
    -webkit-animation: opacity 5s ease-in-out;
    animation: opacity 5s ease-in-out;
}

.switch {
  position: relative;
  display: inline-block;
  width: 46px;
  height: 25px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 18px;
  width: 18px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #6ed4cb;
}

input:focus + .slider {
  box-shadow: 0 0 1px #6ed4cb;
}

input:checked + .slider:before {
  -webkit-transform: translateX(20px);
  -ms-transform: translateX(20px);
  transform: translateX(20px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 38px;
}

.slider.round:before {
  border-radius: 50%;
}

</style>
</head>
<body id="body">
<?php include_once("header.php"); ?>
<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>

<!-- date range picker -->
<div id="reportrange" class=" col-sm-3 " style="cursor: pointer; margin-top:28px; margin-left:10px;max-width:332px;">
    <div class=" floating-box1">
        <div id="range" class="form-control" style="max-width:332px;">
        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
        <span id="dateval"></span> <b class="caret"></b>
        </div>
    </div>
</div>

<!-- master service type filter -->
<div class=" col-sm-2 col-lg-2" style="cursor: pointer; margin-top:28px;max-width:170px;margin-left:-40px;">
    <div class=" floating-box1">
      <select id="master_service" name="master_service" class="form-control" style="max-width:170px;">
      <option value="all" selected>MasterService</option>
      <?php 
      $sql_master_service = "SELECT DISTINCT(master_service) FROM go_axle_service_price_tbl WHERE bridge_flag='0' ORDER BY  master_service ASC";
      $res_master_service = mysqli_query($conn,$sql_master_service);
      while($row_master_service = mysqli_fetch_object($res_master_service)){
        $master_service = $row_master_service->master_service;
        ?>
        <option value="<?php echo $master_service; ?>"><?php echo $master_service; ?></option>
        <?php
      }
      ?>
      </select>
    </div>
</div>
<!-- service type filter -->
<div class=" col-sm-2 col-lg-2" style="cursor: pointer; margin-top:28px;max-width:160px;margin-left:-20px;">
    <div class=" floating-box1">
      <select id="service" name="service" class="form-control" style="max-width:160px;">
        <option selected value="all">All Services</option>
        <?php
        $sql = "SELECT DISTINCT service_type FROM go_axle_service_price_tbl WHERE bridge_flag='0'";
        $res = mysqli_query($conn,$sql);
        while($row = mysqli_fetch_object($res)){
          ?>
          <option value="<?php echo $row->service_type; ?>"><?php echo $row->service_type; ?></option>
          <?php
        }
        ?>
      </select>
    </div>
</div>
<!-- Alloted to filter -->
<div class=" col-sm-2 col-lg-2 " style="cursor: pointer; margin-top:28px;max-width:170px;margin-left:-20px;">
    <div class=" floating-box1">
      <select id="person" name="person" class="form-control"style="max-width:170px;">
        <option selected value="all">Alloted to All</option>
        <?php
        $sql_person = "SELECT crm_log_id,name FROM crm_admin WHERE (crm_flag='1' OR (su_flag!='1' AND admin_crm='1'))";
        $res_person = mysqli_query($conn,$sql_person);
        while($row_person = mysqli_fetch_object($res_person)){
            ?>
            <option value="<?php echo $row_person->crm_log_id; ?>"><?php echo $row_person->name; ?></option>
            <?php
        }
        ?>
      </select>
    </div>
</div>
<!-- city filter -->
<div class=" col-sm-2 col-lg-2 " style="cursor: pointer; margin-top:28px;max-width:150px;margin-left:-20px;">
    <div class=" floating-box1">
      <select id="city" name="city" class="form-control"style="max-width:150px;">
        <option selected value="Chennai">Chennai</option>
        <?php
        $sql_city = "SELECT DISTINCT city FROM localities WHERE city!='chennai' ORDER BY city ASC";
        $res_city = mysqli_query($conn,$sql_city);
        while($row_city = mysqli_fetch_object($res_city)){
            ?>
            <option value="<?php echo $row_city->city; ?>"><?php echo $row_city->city; ?></option>
            <?php
        }
        ?>
      </select>
    </div>
</div>
<!-- Programatic toggle -->
<div class=" col-sm-2 col-lg-2" style="cursor: pointer; margin-top:28px;max-width:220px;">
    <table>
    <tbody>
    <tr>
    <td><span class="floating-box1">Programatic</span>&nbsp;&nbsp;&nbsp;</td>
    <td><label class="switch">
    <input type="checkbox" id="programatic" name="programatic">
    <span class="slider round"></span>
  </label> 
  </td>  
  </tr> 
  </tbody>
  </table>
</div>
<!-- Online Payments -->
<div class=" col-sm-2 col-lg-2 " style="cursor: pointer; margin-top:28px;max-width:170px; display:none;" id="onlinepayments">
    <div class=" floating-box1">
    <i class="fa fa-credit-card-alt" aria-hidden="true" title="Total Online Payments" style="color:rgba(0, 114, 255, 0.66);"></i>&nbsp;&nbsp;<span id="payments" title="Total Online Payments">0</span>&nbsp;&nbsp;&nbsp;<i class="fa fa-inr" aria-hidden="true" title="Amount Paid Online" style="color:#ffa800;"></i>&nbsp;&nbsp;<span id="paid" title="Amount Paid Online">0</span>
    </div>
</div>
<div id="show" style="margin-top:82px;width:99%;">
</div>  
<!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>


<!-- jQuery library -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- date range picker -->
<script>
$('input[name="daterange"]').daterangepicker({
 locale: {
      format: 'DD-MM-YYYY'
    }
});
</script>
<script type="text/javascript">
$(function() {

  //var start = moment().subtract(1, 'days');
  var start = moment().startOf('month');
	 // var start = moment();
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});
</script>
<script>
function defaultview(val){
	var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
	var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
  var master_service = $('#master_service').val();
  var service = $('#service').val();
  var person = $('#person').val();
  var programatic = $('#programatic').val();
  var city = $('#city').val();
  var prog;
  if(programatic === "on"){
    prog = "no";
  }
  else{
    prog = "yes";
  }
	//Make AJAX request, using the selected value as the POST
//console.log(programatic);
	$.ajax({
	    url : "ajax/daily_reports_view_test.php",  // create a new php page to handle ajax request
	    type : "POST",
	    data : {"startdate": startDate , "enddate": endDate, "master_service":master_service, "service":service, "person":person ,"programatic":prog ,"city":city},
	    success : function(data) {
			//alert(data);
		    //console.log(data);
            $('#show').show();
			$("#loading").hide();
			$('#show').html(data);
      onlinepayments();
       },
	   error: function(xhr, ajaxOptions, thrownError) {
	        // alert(xhr.status + " "+ thrownError);
	    }
	});
}
</script>

<script>
function onlinepayments(val){
	var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
	var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
  var city = $('#city').val();
	//Make AJAX request, using the selected value as the POST
//console.log(programatic);
	$.ajax({
	    url : "ajax/daily_reports_onlinepay_view.php",  // create a new php page to handle ajax request
	    type : "POST",
	    data : {"startdate": startDate , "enddate": endDate, "city":city},
	    success : function(data) {
			//alert(data);
		   // console.log(data);
        $('#onlinepayments').show();
        var pays = data.split('.');
        var payments = pays[0];
        var paid = pays[1];
        $('#payments').html(payments);
        $('#paid').html(paid);
       },
	   error: function(xhr, ajaxOptions, thrownError) {
	        // alert(xhr.status + " "+ thrownError);
	    }
	});
}
</script>


<script>
$(document).ready( function (){
   $('#editor').hide();
   $('#show').hide();
   $('#onlinepayments').hide();
  $("#loading").show();
  defaultview();
});
</script>
<!-- on change of date -->
<script>
$(document).ready( function (){
	$('#dateval').on("DOMSubtreeModified", function (){
   $('#editor').hide();
   $('#show').hide();
   $('#onlinepayments').hide();
	$("#loading").show();
  defaultview();
	 });
});
</script>
<!-- master seervice type -->
<script>
function get_services(){
  var master = $("#master_service").val();
  console.log(master);
  $("#service").empty();
  $.ajax({
	    url : "ajax/get_services_master.php",  // create a new php page to handle ajax request
	    type : "POST",
	    data : {"master": master},
	    success : function(data) {
      	$('#service').append(data);	
        $("#table").hide();
    $("#loading").show();
    defaultview();	
  },
	    error: function(xhr, ajaxOptions, thrownError) {
	          //  alert(xhr.status + " "+ thrownError);
	    }
	});
}
</script>
<script>
$(document).ready( function (){
  get_services();
});
</script>
<script>
$(document).ready( function (){
  $("#master_service").change(function(){
    get_services();
    
  });
});
</script>
<!-- on change of service -->
<script>
$(document).ready( function (){
	$('#service').change(function(){
   $('#editor').hide();
   $('#show').hide();
   $('#onlinepayments').hide();
	$("#loading").show();
  defaultview();
	 });
});
</script>

<!-- on change of alloted to filter -->
<script>
$(document).ready( function (){
	$('#person').change(function (){
   $('#editor').hide();
   $('#show').hide();
   $('#onlinepayments').hide();
	$("#loading").show();
  defaultview();
	 });
});
</script>


<!-- on change of city -->
<script>
$(document).ready( function (){
	$('#city').change(function (){
   $('#editor').hide();
   $('#show').hide();
   $('#onlinepayments').hide();
	$("#loading").show();
  defaultview();
	 });
});
</script>

<!-- on change of programatic toggle -->
<script>
$(document).ready( function (){
	$('#programatic').change(function (){
    var prog = $('#programatic').val();

    if(prog === 'on'){
			$('#programatic').val('off');
		}
		if(prog === 'off'){
			$('#programatic').val('on');
		}
   $('#editor').hide();
   $('#show').hide();
	$("#loading").show();
  defaultview();
	 });
});
</script>

</body>
</html>

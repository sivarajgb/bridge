<?php
ini_set('max_execution_time', '0');
include 'config.php';

$conn = db_connect1();
// error_reporting(1);
session_start();
$flag = $_SESSION['flag'];

if ((empty($_SESSION['crm_log_id'])) || $flag != "1") {
	header('location:index.php');
	die();
}

$day_interval = 60;

//call SP
// $query = "call go_bumpr.un_actioned_new_booking('$day_interval')";
// $result = mysqli_query($conn, $query);

$un_actioned_new_booking = "UPDATE go_bumpr.user_booking_tb SET crm_update_time = NOW()
WHERE booking_id IN(
SELECT ub.bid
FROM (
SELECT booking_id AS bid
FROM go_bumpr.user_booking_tb
WHERE flag = 0 AND booking_status = 1 AND axle_flag = 0 AND flag_fo = 0 AND flag_unwntd = 0 AND flag_duplicate = 0 AND rnr_flag = 0 AND rnr_count = 0 AND override_flag = 0 AND ire_flag = 0 AND DATE(crm_update_time) BETWEEN CURDATE() - INTERVAL $day_interval DAY AND CURDATE() - INTERVAL 1 DAY) AS ub)";

$update_res = mysqli_query($conn, $un_actioned_new_booking) or die(mysqli_error($conn));

if($update_res){
	echo "Past 60 of Un actioned New Bookings are Moved to Current Date";
}

flush();
ob_flush();
mysqli_free_result($conn);
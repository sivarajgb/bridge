<?php
include("sidebar.php");
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
$conn1 = db_connect1();
$conn2 = db_connect3();
// login or not
if((empty($_SESSION['crm_log_id'])) || $flag!="1" || $super_flag != "1") {
	if($_SESSION['crm_log_id']!='crm024' && $_SESSION['crm_log_id']!='crm035' && $_SESSION['crm_log_id']!='crm135' && $_SESSION['crm_log_id']!='crm161' && $_SESSION['crm_log_id']!='crm187' && $_SESSION['crm_log_id']!='crm189' && $_SESSION['crm_log_id']!='crm202')
	{
		header('location:logout.php');
		die();
	}
}

$today = date('Y-m-d');
$shop_id = base64_decode($_GET['si']);
?>

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>GoBumpr Bridge</title>


  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<!-- date time picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<!-- table sorter -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js"></script>
<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>

<style>
/*home page blocks */
.navbar-fixed-top{
  z-index:100 !important;
}
.upper-div{
  z-index:999999 !important;
}
.borderless td, .borderless th {
    border: none !important;
}
</style>

</head>
<body>
<?php include_once("header.php"); ?>
<script>
	$(document).ready(function(){
		$('#city').hide();
	})
</script>
<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>
<?php
$sql_garage = "SELECT b2b_cover_image,b2b_shop_name,b2b_owner_name,b2b_vehicle_type,b2b_authorized,b2b_address1,b2b_address1,b2b_address2,b2b_address3,b2b_address4,b2b_address5,b2b_pincode,b2b_mobile_number_1,b2b_mobile_number_2,b2b_mobile_number_3,b2b_mobile_number_4,b2b_mobile_number_5,b2b_email_id_1,b2b_shop_size,b2b_lead_usr,b2b_renw_date FROM b2b_mec_tbl WHERE b2b_shop_id = '$shop_id' ";
$res_garage = mysqli_query($conn2,$sql_garage);

while($row_garage = mysqli_fetch_assoc($res_garage)){
    $image = $row_garage['b2b_cover_image'];
    $shop_name = $row_garage['b2b_shop_name'];
    $owner_name = $row_garage['b2b_owner_name'];
    $vehicle_type = $row_garage['b2b_vehicle_type'];
    $address1 = $row_garage['b2b_address1'];
    $address2 = $row_garage['b2b_address2'];
    $address3 = $row_garage['b2b_address3'];
    $address4 = $row_garage['b2b_address4'];
    $address5 = $row_garage['b2b_address5'];
    $pincode = $row_garage['b2b_pincode'];
    $mobile1 = $row_garage['b2b_mobile_number_1'];
    $mobile2 = $row_garage['b2b_mobile_number_2'];
    $mobile3 = $row_garage['b2b_mobile_number_3'];
    $mobile4 = $row_garage['b2b_mobile_number_4'];
    $mobile5 = $row_garage['b2b_mobile_number_5'];
    $email = $row_garage['b2b_email_id_1'];
    $shop_size = $row_garage['b2b_shop_size'];
    $lead_user = $row_garage['b2b_lead_usr'];
    $renewal_date = $row_garage['b2b_renw_date'];
    $authorized = $row_garage['b2b_authorized'];
}
$addressline1 ='';
$addressline2 ='';
$addressline3 ='';

if($address1 != ''){
    $addressline1 = $addressline1.$address1;
}
if($address2 != ''){
    $addressline1 = $addressline1.','.$address2;
}
if($address3 != ''){
    $addressline2 = $addressline2.$address3;
}
if($address4 != ''){
    $addressline2 = $addressline2.','.$address4;
}
if($address5 != ''){
    $addressline3 = $addressline3.$address5;
}
if($pincode != ''){
    $addressline3 = $addressline3.','.$pincode.'.';
}
$mobile_number ='';

if($mobile1 != ''){
    $mobile_number = $mobile_number.$mobile1;
}
if($mobile2 != ''){
    $mobile_number = $mobile_number.','.$mobile2;
}
if($mobile3 != ''){
    $mobile_number = $mobile_number.$mobile3;
}
if($mobile4 != ''){
    $mobile_number = $mobile_number.','.$mobile4;
}
if($mobile5 != ''){
    $mobile_number = $mobile_number.$mobile5;
}

$today_date = strtotime($today);
$renew_date = strtotime($renewal_date);

if($lead_user == '0'){
  if($today_date > $renew_date){
    $membership = "Premium"; 
  }
  else{
    $membership = "Basic";
  }
}
else{
  $membership = "Basic";
}
$sql_credits = "SELECT b2b_credits FROM b2b_credits_tbl WHERE b2b_shop_id='$shop_id'";
$res_credits = mysqli_query($conn2,$sql_credits);

$row_credits = mysqli_fetch_array($res_credits);
$credits = $row_credits['b2b_credits'];
?> 
<div id="show" style="float:left; width:30%;" align="center">
<div style="margin-top:30px;margin-left:30px;border:0.5px solid #ffa800;padding:12px;border-radius:12px;">
    <?php if($image !=''){ ?><img src="<?php echo $image; ?>" width="250" height="150" class="image" caption="<?php echo $shop_name; ?>" /><?php }
    else{ ?><img src="images/image_not_found.png" width="250" height="150" class="image" caption="<?php echo $shop_name; ?>" /><?php } ?>
    <h4><?php echo $shop_name." (".$vehicle_type.") "; ?> <?php if($authorized == '1'){ ?><img src="images/authorized.png" alt="authorized" width="25" height="25" /><?php } ?></h4>
    <table id="shopdetails" class="table borderless">
       <tr><td><strong>Shop Id</strong></td><td><?php echo $shop_id; ?></td></tr>
       <tr><td><strong>Owner</strong></td><td><?php echo $owner_name; ?></td></tr>
       <tr><td><strong>Address</strong></td><?php if($addressline1!= '' ){ ?><td><?php echo $addressline1; ?></td> <?php } ?></tr>
       <?php if($addressline2!= '' ){ ?><tr><td></td><td><?php echo $addressline2; ?></td></tr> <?php } ?>
        <?php if($addressline2!= '' ){ ?><tr><td></td><td><?php echo $addressline3; ?></td></tr> <?php } ?>
       <?php if($mobile_number != '' ){ ?><tr><td><strong>Mobile</strong></td><td><?php echo $mobile_number; ?></td></tr><?php } ?>
       <?php 
       if($email !=''){
         ?>
         <tr><td><strong>E-Mail</strong></td><td><?php echo $email; ?></td></tr>
         <?php
       }
       ?>
       <?php if($shop_size !=''){ ?><tr><td><strong>ShopSize</strong></td><td><?php echo $shop_size; ?></td></tr><?php } ?>
       <tr><td><strong>MemberShip</strong></td><td><?php echo $membership; ?></td></tr>
       <tr><td><strong>Renewal Date</strong></td><td><?php echo date('d M Y h.i A', strtotime($renewal_date)); ?></td></tr>
      <tr><td><strong>Credit Balance</strong></td><td><?php echo $credits; ?></td></tr>
  </table>
</div>
</div>
<div style="float:left;margin-top:30px;margin-left:30px;padding:12px;width:67%;" align="center">
<div style="cursor: pointer;float:right;clear:both;">
    <button id="export_excel" class="btn btn-md" style="margin-right:20px;background-color:#59a95b;color:#fafafa;"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;&nbsp;Export</button>
    <button class="btn btn-md" data-toggle="modal" data-target="#myModal_credits" style="background-color:rgba(128, 203, 196, 0.97);"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Recharge</button>
</div>
<div style="float-left;clear:both;margin-top:50px;">
<table class="table tablesorter" id="example">
<thead style="background-color:#eee;">
<th>No.</th>
<th>Credits</th>
<th>AmtPaid</th>
<th>Amt</th>
<th>Tax</th>
<th>PymntType</th>
<th>ReferenceId</th>
<th>Date</th>
<th>Month</th>
<th>Year</th>
<th>UpdatedBy</th>
<th>CollectedBy</th>
<th>Comments</th>
</thead>
<tbody>
<?php 
$sql_payment = "SELECT b2b_credits,b2b_amt,b2b_credit_amt,b2b_payment_mode,txnid,instmj_pmt_id,b2b_receipt_no,b2b_cheque_no,b2b_neft_trn_id,b2b_other_ref,b2b_comments,b2b_log,b2b_crm_update_id,b2b_crm_log_id,b2b_pmt_scs_flg FROM b2b_credits_payment WHERE b2b_shop_id='$shop_id' ORDER BY b2b_log DESC";
$res_payment = mysqli_query($conn2,$sql_payment);
$no = 0;

$num_rows = mysqli_num_rows($res_payment);

if($num_rows >=1){
while($row_payment = mysqli_fetch_object($res_payment)){
  $credits = $row_payment->b2b_credits;
  $amount_paid = $row_payment->b2b_amt;
  $credit_amount = $row_payment->b2b_credit_amt;
  $payment_mode = $row_payment->b2b_payment_mode;
  $payu_id = $row_payment->txnid;
  $instamojo_id = $row_payment->instmj_pmt_id;
  $receipt_no = $row_payment->b2b_receipt_no;
  $cheque_no = $row_payment->b2b_cheque_no;
  $neft_trn_id = $row_payment->b2b_neft_trn_id;
  $other_ref = $row_payment->b2b_other_ref;
  $comments = $row_payment->b2b_comments;
  $up_by_id = $row_payment->b2b_crm_log_id;
  $up_by_id = $row_payment->b2b_crm_update_id;
  $pymt_scs_flag = $row_payment->b2b_pmt_scs_flg;

  $sql_crm_name = "SELECT name FROM crm_admin WHERE crm_log_id='$up_by_id'";
  $res_crm_name = mysqli_query($conn1,$sql_crm_name);
  $row_crm_name = mysqli_fetch_array($res_crm_name);
  $updated_by = $row_crm_name['name'];
  $sql_crm_name2 = "SELECT name FROM crm_admin WHERE crm_log_id='$up_by_id'";
  $res_crm_name2 = mysqli_query($conn1,$sql_crm_name2);
  $row_crm_name2 = mysqli_fetch_array($res_crm_name2);
  $collected_by = $row_crm_name2['name'];
  $pay_log = $row_payment->b2b_log;  
  $pay_date = date('d',strtotime($pay_log));
  $pay_month = date('m',strtotime($pay_log));
  $pay_year = date('Y',strtotime($pay_log));

  

  switch($payment_mode){
    case 'payu':$reference_id = $payu_id; break;
    case 'Instamojo':$reference_id = $instamojo_id; break;
    case 'Cash':$reference_id = $receipt_no; break;
    case 'Cheque':$reference_id = $payu_id; break;
    case 'NEFT':$reference_id = $neft_trn_id; break;
    case 'Others':$reference_id = $other_ref; break;
    default:$reference_id = "NA";
  }
if($pymt_scs_flag == '1'){ 
?>
<tr bgcolor="#e88c87">
<td><?php echo $no=$no+1; ?></td>
<td><?php echo $credits; ?></td>
<td><?php echo $amount_paid; ?></td>
<td><?php echo $credit_amount; ?></td>
<td><?php echo $tax = $amount_paid-$credit_amount ; ?></td>
<td><?php echo $payment_mode; ?></td>
<td><?php echo $reference_id; ?></td>
<td><?php echo $pay_date; ?></td>
<td><?php echo $pay_month; ?></td>
<td><?php echo $pay_year; ?></td>
<td><?php echo $updated_by; ?></td>
<td><?php echo $collected_by; ?></td>
<td><?php echo $comments; ?></td>
</tr>

<?php
}
else{ 
?>
<tr>
<td><?php echo $no=$no+1; ?></td>
<td><?php echo $credits; ?></td>
<td><?php echo $amount_paid; ?></td>
<td><?php echo $credit_amount; ?></td>
<td><?php echo $tax = $amount_paid-$credit_amount ; ?></td>
<td><?php echo $payment_mode; ?></td>
<td><?php echo $reference_id; ?></td>
<td><?php echo $pay_date; ?></td>
<td><?php echo $pay_month; ?></td>
<td><?php echo $pay_year; ?></td>
<td><?php echo $updated_by; ?></td>
<td><?php echo $collected_by; ?></td>
<td><?php echo $comments; ?></td>
</tr>

<?php
}
}
} // if
else{
  echo "<h3>Sorry there is no Payment History!!</h3> ";
}
?>
</tbody>
</table>
</div>
</div>
  <!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>


<!-- jQuery library -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
       
<!-- Modal -->
<div class="modal fade" id="myModal_credits" role="dialog" >
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Recharge</h3>
      </div>
      <div class="modal-body">
        <form class="form" action="adding_credits.php" method="post">
          <div class="row">
              <div class="col-lg-3 col-sm-2 col-md-3 col-lg-offset-2">
              <label style="padding:5px;">Amount Paid</label>
              </div>
              <div class="col-lg-4 col-sm-3 col-md-3">
              <input class="form-control" type="number" name="amount_paid" id="amount_paid" placeholder="Eg. 1150" required/>
              </div>
              <p style="color:#4DB6AC;font-size:22px;padding:5px;"><i class="fa fa-calculator" aria-hidden="true"  id="calculate"></i></p>
          </div>
          <div class="row col-lg-offset-2" id="calculations">
              <div class="col-lg-3 col-sm-3 col-md-3" style="float:left;">
                  <label style="padding:5px;" for="amount">Amount</label>
                  <input class="form-control" type="number" name="amount" id="amount" placeholder="Eg.1000" style="width:96px;" required/>
              </div>
              <div class="col-lg-3 col-sm-3 col-md-3" style="float:left;margin-left:-10px;">
                  <label style="padding:5px;" for="tax">Tax</label>
                  <input class="form-control" type="number" name="tax" id="tax" placeholder="Eg.150" style="width:96px;" required/>
              </div>
              <div class="col-lg-3 col-sm-3 col-md-3" style="float:left;margin-left:-10px;">
                  <label style="padding:5px;" for="tax">Credits</label>
                  <input class="form-control" type="number" name="no_of_credits" id="no_of_credits" placeholder="Eg.150" style="width:96px;" required/>
              </div>
          </div>
          <br>
          <div class="row">
              <div class="col-lg-3 col-sm-2 col-md-3 col-lg-offset-2">
              <label style="padding:5px;">Payment Mode</label>
              </div>
              <div class="col-lg-5 col-sm-3 col-md-4">
              <select class="form-control" id="payment_mode" name="payment_mode" required>
              <option value="" selected> Select mode of Payment</option>
              <option value="Cash">Cash</option>
              <option value="Cheque">Cheque</option>
              <option value="NEFT">NEFT</option>
              <option value="Instamojo">Instamojo</option>
              <option value="Others">Others</option>
              </select>
              </div>
          </div>
          <div id="cheque_div" style="display:none;">
          <br>
            <div class="row" >
                <div class="col-lg-3 col-sm-2 col-md-3 col-lg-offset-2">
                <label style="padding:5px;">Cheque No.</label>
                </div>
                <div class="col-lg-5 col-sm-3 col-md-4">
                <input class="form-control" type="text" name="cheque_no" id="cheque_no" placeholder="Eg. Chq1223332332"/>
                </div>
            </div>
            <div class="row" >
                <div class="col-lg-4 col-sm-3 col-md-4 col-lg-offset-2">
                <label style="padding:5px;" for="cheque_date">Cheque Date</label>
                <input class="form-control datepicker"  data-date-format='dd-mm-yyyy' type="text" name="cheque_date" id="cheque_date"/>
                </div>
                <div class="col-lg-4 col-sm-3 col-md-4 ">
                <label style="padding:5px;" for="bank_name">Bank Name</label>
                <input class="form-control" type="text" name="bank_name" id="bank_name" placeholder="Eg. Vijaya Bank"/>
                </div>
            </div>
          </div>
          <div id="cash_div" style="display:none;">
          <br>
            <div class="row">
                <div class="col-lg-3 col-sm-2 col-md-3 col-lg-offset-2">
                <label style="padding:5px;">Receipt No.</label>
                </div>
                <div class="col-lg-5 col-sm-3 col-md-4">
                <input class="form-control" type="text" name="receipt_no" id="receipt_no" placeholder="Eg. Rec112"/>
                </div>
            </div>
          </div>
          <div id="neft_div" style="display:none;">
          <br>
              <div class="row">
                <div class="col-lg-3 col-sm-2 col-md-3 col-lg-offset-2">
                <label style="padding:5px;">Transaction Id</label>
                </div>
                <div class="col-lg-5 col-sm-3 col-md-4">
                <input class="form-control" type="text" name="transaction_id" id="transaction_id" placeholder="Eg. Trn0202020202"/>
                </div>
            </div>
          </div>
          <div id="instamojo_div" style="display:none;">
          <br>
              <div class="row">
                <div class="col-lg-3 col-sm-2 col-md-3 col-lg-offset-2">
                <label style="padding:5px;">Instamojo Id</label>
                </div>
                <div class="col-lg-5 col-sm-3 col-md-4">
                <input class="form-control" type="text" name="instamojo_id" id="instamojo_id" placeholder="Eg. MOJO12121212112"/>
                </div>
            </div>
          </div>
          <div id="others_div" style="display:none;">
          <br>
              <div class="row">
                <div class="col-lg-3 col-sm-2 col-md-3 col-lg-offset-2">
                <label style="padding:5px;">Reference</label>
                </div>
                <div class="col-lg-5 col-sm-3 col-md-4">
                <input class="form-control" type="text" name="reference" id="reference" placeholder="Eg. Customer's bill"/>
                </div>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-lg-3 col-sm-2 col-md-3 col-lg-offset-2">
              <label style="padding:5px;">Collected By</label>
            </div>
            <div class="col-lg-5 col-sm-3 col-md-4">
              <select class="form-control" type="text" name="updated_by" id="updated_by" required>
              <option selected value="">Select a person</option>
              <?php 
                $sql_crm = "SELECT crm_log_id,name FROM crm_admin ORDER BY name ASC";
                $res_crm = mysqli_query($conn1,$sql_crm);
                while($row_crm = mysqli_fetch_object($res_crm)){
              ?>
                <option value="<?php echo $row_crm->crm_log_id; ?>"><?php echo $row_crm->name; ?></option>
              <?php
            }
            ?>
            </select>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-lg-3 col-sm-2 col-md-3 col-lg-offset-2">
            <label style="padding:5px;">Comments</label>
            </div>
            <div class="col-lg-5 col-sm-3 col-md-4">
            <input class="form-control" type="text" name="comments" id="comments" placeholder="Eg. Some text to remind" required/>
            </div>
          </div>
          <br>
          <input type="hidden" class="form-control" name="shop_id" id="shop_id" value="<?php echo $shop_id; ?>"/>
          <input class="from-control" type="hidden" name="customer" id="existing" value="existing" />
          <div class="row">
              <div align="center">
              <input type="submit" class="btn btn-md" value="Submit" style="background-color:#00796B;color:#fff;"/>
              </div>
          </div>

          </form>

      </div> <!-- modal body -->
    </div> <!-- modal content -->
  </div>  <!-- modal dailog -->
</div>  <!-- modal -->
<!-- calculate amount and tax -->
<script>
$(document).ready(function($) {
  $("#calculate").click(function(){
    var amountpaid = $("#amount_paid").val();
    var amount = 0;
    var tax = 0;
    var credits = 0;
    var total = amountpaid;
    if(amountpaid === ''){
      alert("Enter the AMOUNT PAID to get actual amount and tax");
    }
    else{
      amount =Math.floor(total / 1.18);
      tax = Math.floor(amountpaid-amount);
      credits = Math.floor(amount/100);
      $("#amount").val(amount);
      $("#tax").val(tax);
      $("#no_of_credits").val(credits);
    }

  });
});
</script>
<!-- on change of payment mode -->
<script>
$(document).ready(function($) {
  $("#payment_mode").change(function(){
    var mode = $("#payment_mode").val();
    switch(mode){
      case "Cash": $("#cash_div").show();$("#receipt_no").prop('required',true);
                   $("#cheque_div").hide();$("#cheque_no").prop('required',false);$("#cheque_date").prop('required',false);$("#bank_name").prop('required',false);
                   $("#neft_div").hide();$("#transaction_id").prop('required',false);
                   $("#instamojo_div").hide();$("#instamojo_id").prop('required',false);
                   $("#others_div").hide();$("#reference").prop('required',false); 
                   break;
      case "Cheque": $("#cash_div").hide();$("#receipt_no").prop('required',false);
                     $("#cheque_div").show();$("#cheque_no").prop('required',true);$("#cheque_date").prop('required',true);$("#bank_name").prop('required',true);
                     $("#neft_div").hide();$("#transaction_id").prop('required',false);
                     $("#instamojo_div").hide();$("#instamojo_id").prop('required',false);
                     $("#others_div").hide();$("#reference").prop('required',false);
                     break;
      case "NEFT": $("#cash_div").hide();$("#receipt_no").prop('required',false);
                   $("#cheque_div").hide();$("#cheque_no").prop('required',false);$("#cheque_date").prop('required',false);$("#bank_name").prop('required',false); 
                   $("#neft_div").show();$("#transaction_id").prop('required',true); 
                   $("#instamojo_div").hide();$("#instamojo_id").prop('required',false);
                   $("#others_div").hide();$("#reference").prop('required',false); 
                   break;
      case "Instamojo": $("#cash_div").hide();$("#receipt_no").prop('required',false);
                        $("#cheque_div").hide();$("#cheque_no").prop('required',false);$("#cheque_date").prop('required',false);$("#bank_name").prop('required',false);
                        $("#neft_div").hide();$("#transaction_id").prop('required',false); 
                        $("#instamojo_div").show();$("#instamojo_id").prop('required',true);
                        $("#others_div").hide();$("#reference").prop('required',false); 
                        break;
      case "Others": $("#cash_div").hide();$("#receipt_no").prop('required',false);
                     $("#cheque_div").hide();$("#cheque_no").prop('required',false);$("#cheque_date").prop('required',false);$("#bank_name").prop('required',false); 
                     $("#neft_div").hide();$("#transaction_id").prop('required',false); 
                     $("#instamojo_div").hide();$("#instamojo_id").prop('required',false);
                     $("#others_div").show();$("#reference").prop('required',true); 
                     break;
      default:$("#cash_div").hide();$("#receipt_no").prop('required',false);
              $("#cheque_div").hide();$("#cheque_no").prop('required',false);$("#cheque_date").prop('required',false);$("#bank_name").prop('required',false); 
              $("#neft_div").hide();$("#transaction_id").prop('required',false); 
              $("#instamojo_div").hide();$("#instamojo_id").prop('required',false);
              $("#others_div").hide();$("#reference").prop('required',false);
    }
  });
});
</script>
<!-- date picker -->
<script>
var date = new Date();
date.setDate(date.getDate());

$('.datepicker').datepicker({
   autoclose: true,
    startDate: date
});
$('input.datepicker').datepicker('setDate', 'today');
</script>
 <!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example").tablesorter( {sortList: [[0,0], [1,0]]} );
    }
);
</script>
<!-- export data -->
<script type="text/javascript" src="js/jquery.table2excel.js"></script>

<script>
 $(function() {
    var date = "<?php echo date('d-m-Y',strtotime($today)); ?>";
    var shopname = "<?php echo $shop_name; ?>";
    var shopid = "<?php echo $shop_id; ?>";
    $("#export_excel").click(function(){
        $("#example").table2excel({
           // exclude: ".noExl",
            //name: "Excel Document Name",
            filename:  "Credits Payment History of " + shopname +"("+shopid+")"+ " Dated " + date
        }); 
    });
    //$("#example").dataTable();
  });
</script>

</body>
</html>

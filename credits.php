<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id']))) {
	header('location:logout.php');
	die();
}
$today=date('Y-m-d');
?>

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
  <title>GoBumpr Bridge</title>

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>

  <style>
#datepick > span:hover{cursor: pointer;}

.floating-box {
	 display: inline-block;
	 margin: 22px;
 padding: 22px;
 width:203px;
 height:105px;
 box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  font-size: 17px;
}
  </style>

</head>
<body>
  <nav class="navbar navbar-default navbar-fixed-top" >
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->

   <ol class="breadcrumb" style="max-height:40px;">
   <a href="#" data-sidebar-button style="text-decoration:none;"><i class="fa fa-bars" aria-hidden="true"></i></a> &nbsp;&nbsp;&nbsp;&nbsp;

<?php if($flag == 1){
    ?>
    <li style="margin-top:10px;"><a href="aleads.php"><i class="fa fa-home"></i> Home</a></li>
    <?php
}
else{
    ?>
    <li style="margin-top:10px;"><a href="leads.php"><i class="fa fa-home"></i> Home</a></li>
    <?php
}
?>

	<ul class="nav navbar-nav navbar-right" >
	<li style=" margin-top:11px; font-size:20px; ">
	<div class="dropdown">
	<a href="" style="color:#000; padding-right: 15px;"><i class="fa fa-user-circle" aria-hidden="true"></i> <?php echo $crm_name; ?> </a>
	</li>
	</ul>
</ol>
  </div><!-- /.container-fluid -->
</nav>
 <div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>
<div align="center" style="margin-top:25px;">
<?php 
if(isset($_GET['wk']) && $_GET['wk']){
  ?>
  <div id="loading" style="margin-top:140px;" align="center">
  <img src="images/oops.png" title"opps!"="" height"200"="" width="200">
<h1> Lead Cap Exhausted! Please come back tomorrow !!! </h1></div>
  <?php
}
else if(isset($_GET['st']) && $_GET['st'])
{?>
  <div id="loading" style="margin-top:140px;" align="center">
  <img src="images/oops.png" title"opps!"="" height"200"="" width="200">
<h1> Service Partner is not available !!! Try someone else ! </h1></div>
  <?php
}
else if(isset($_GET['na']) && $_GET['na'])
{?>
  <div id="loading" style="margin-top:140px;" align="center">
  <img src="images/oops.png" title"opps!"="" height"200"="" width="200">
<h1> Service Partner is not available to accept leads !!! Try someone else ! </h1></div>
  <?php
}
else{
  ?>
  <img src="images/credits.jpg" alt="Smiley face" height="500" width="80%">

  <?php
}
?>
</div>


<!-- jQuery library -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
  <script>
	$(document).ready(function(){
		$('#city').show();
	})
</script>
</body>
</html>

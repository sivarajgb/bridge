<?php
include("sidebar.php");
$conn = db_connect1();
// login or not || $flag!="1"
if((empty($_SESSION['crm_log_id']))) {
	
	header('location:logout.php');
	die();
}
?>

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
  <title>GoBumpr Bridge</title>
<!-- tags input -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    
    <script src="js/bootstrap-tagsinput.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="css/app.css">


  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


  <!-- Include Date Range Picker -->
  <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>

<style>
/* Style the tab */
div.tab {
    align:center;
    width:80%;
    overflow: hidden;
    border: 1px solid #E0F2F1;
    background-color: #E0F2F1;
}

/* Style the buttons inside the tab */
div.tab button {

    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
}

/* Change background color of buttons on hover */
div.tab button:hover {
    background-color: #4DB6AC;
}

/* Create an active/current tablink class */
div.tab button.active {
    background-color: #26A69A;
    color:#fff;
}

/* Style the tab content */
.tabcontent {
    align:center;
    width:80%;
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}

.tabcontent {
    -webkit-animation: fadeEffect 1s;
    animation: fadeEffect 1s; /* Fading effect takes 1 second */
}

@-webkit-keyframes fadeEffect {
    from {opacity: 0;}
    to {opacity: 1;}
}

@keyframes fadeEffect {
    from {opacity: 0;}
    to {opacity: 1;}
}
.label {
    line-height: 2 !important;
}
/*.bootstrap-tagsinput{
  display: inline-flex !important;
}*/
.label-info{
  background-color: #4DB6AC !important;
}
</style>

<!-- on document load hide all option and assign city value to a php variable -->
<script>
function viewtable(){
    $('select[name*="city"] option[value="all"]').remove();
    var city = $('#city').val();
	          //Make AJAX request, using the selected value as the POST

			  $.ajax({
	            url : "ajax/allocation_interface_view.php",  // create a new php page to handle ajax request
	            type : "POST",
	            data : {"city":city},
	            success : function(data) {
					//console.log(data);
					$("#loading").hide();
          $("#show").show();
      			$('#show').html(data);		
	            },
	          error: function(xhr, ajaxOptions, thrownError) {
	          //  alert(xhr.status + " "+ thrownError);
	         }
				 });
}
</script>
<script>
  $(document).ready(function(){
    viewtable();
  })
</script>
<script>
  $(document).ready(function(){
    $('#city').change(function(){
      viewtable();
    });
  })
</script>
</head>
<body>
<?php include_once("header.php"); ?>
<script>
	$(document).ready(function(){
		$('#city').show();
	})
</script>
<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>

<div id="show" style="display:none;">
</div>
  <!-- loading -->
  <div id="loading" style="display:none; margin-top:140px;" align="center">
    <div class='uil-default-css' style='transform:scale(0.58);'>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
  </div>
  </div>
  
  <?php error_reporting(E_ALL); ini_set('display_errors', 1);
  include("ajax/persons.php");//to Update the crm persons in json file 
?>
<script>
$(document).ready(function(){
  var persons = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('text'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  prefetch: 'ajax/persons.json'
  cache: false
});
persons.clearPrefetchCache();
persons.initialize();

 <?php 
  $sql = "SELECT id,crm_id FROM go_axle_service_price_tbl";
  $res = mysqli_query($conn,$sql);
  while($row = mysqli_fetch_array($res)){
    $s_id = $row['id'];
    $s_crm_id = $row['crm_id'];
    ?>
    var elt = $("#<?php echo $s_id; ?>");
  elt.tagsinput({
  itemValue: 'value',
  itemText: 'text',
  typeaheadjs: {
    name: 'persons',
    displayKey: 'text',
    source: persons.ttAdapter()
  }
});
    <?php
    if($s_crm_id != ''){
    $people = explode(",", $s_crm_id);
       foreach($people as $person)
      {
        $sql_crm = "SELECT name FROM crm_admin WHERE crm_log_id='$person' ";
        $res_crm = mysqli_query($conn,$sql_crm);
        $row_crm = mysqli_fetch_object($res_crm);
        $crm_name = $row_crm->name;
        ?> 
       // console.log('<?php echo $crm_name; ?>');    
         elt.tagsinput('add', { "value": "<?php echo $person; ?>" , "text": "<?php echo $crm_name; ?>" });
        <?php
      }
    }
  }
  ?>
})
</script>
<!-- jQuery library -->

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script>
$(".bikes_submit").click(function( event ){
  event.preventDefault();
  var input = $("input").val();
  alert("bikes");
  if(input==''){
    alert("please enter assign atleast one person");
  }
});
</script>

</body>
</html>
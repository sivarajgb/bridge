<?php
include("../config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();
$today=date('Y-m-d');

	$crm_log_id = $_SESSION['crm_log_id'] ;
	$crm_name = $_SESSION['crm_name'];
	$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$shop_id = $_POST['shop_id'];
$master_service = $_POST['master_service'];
$service = $_POST['service'];
$city = $_POST['city'];
$vehicle = $_POST['vehicle'];
$source_type =$_POST['source_type'];

$_SESSION['crm_city'] = $city;

$cond =''; 
$cond = $cond.($city == 'all' ? "" : " AND f.city='$city'");
$cond = $cond.($vehicle == 'all' ? "" : " AND f.veh_type='$vehicle'");
$cond = $cond.($shop_id == 'all' ? "" : " AND f.shop_id='$shop_id'");

if($master_service != "all" && $service == "all"){
    $conn1 = db_connect1();
    $sql_serv = "SELECT service_type FROM go_axle_service_price_tbl WHERE bridge_flag='0' AND master_service='$master_service'";
    $res_serv = mysqli_query($conn1,$sql_serv);
    $services = '';
    while($row_serv = mysqli_fetch_array($res_serv)){
        if($services == ""){
            $services = $row_serv['service_type']."'";
        }
        else{
            $services = $services.",'".$row_serv['service_type']."'";
        }
    }
    $cond = $cond." AND f.service_type IN ('$services)";
}
$cond = $cond.($service != 'all' ?  " AND f.service_type='$service'" : '' ) ;

if($source_type=='all'){
		$conds.="";
	}
	else if($source_type=='coreops'){
		$conds.=" and ub.source!='Re-Engagement Bookings'";
	}
	else if($source_type=='reng'){
		$conds.=" and ub.source='Re-Engagement Bookings'";
	}

$sql_booking = "SELECT b2b_b.gb_booking_id,f.booking_id,f.feedback_status,f.service_status,b2b_b.b2b_check_in_report,b2b_b.b2b_vehicle_at_garage,
b2b_b.b2b_vehicle_ready,f.flag,b2b_b.b2b_log,f.log FROM b2b.b2b_booking_tbl AS b2b_b left join go_bumpr.feedback_track as f on b2b_b.b2b_booking_id = f.b2b_booking_id INNER JOIN b2b.b2b_status AS s ON b2b_b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb AS ub ON b2b_b.gb_booking_id = ub.booking_id  WHERE s.b2b_acpt_flag = '1' AND b2b_b.b2b_swap_flag = 0 AND b2b_b.gb_booking_id != '0' {$cond} {$conds} AND f.shop_id NOT IN (1014,1035,1670) AND DATE(b2b_b.b2b_log) BETWEEN '$startdate' AND '$enddate' group by b2b_b.gb_booking_id ";
//echo $sql_booking;
$res_booking = mysqli_query($conn1,$sql_booking);
$converted = 0;
$yet_to = 0;
$completed = 0;
$in_progress = 0;
$rnr = 0;
$not_contacted = 0;

//echo $sql_booking;
if(mysqli_num_rows($res_booking) >=1){
	$total=mysqli_num_rows($res_booking);
	while($row_booking = mysqli_fetch_object($res_booking)){
		$booking_id = $row_booking->booking_id;
		$feedback_status = $row_booking->feedback_status;
		$service_status = $row_booking->service_status;
		$check_in = $row_booking->b2b_check_in_report;
		$at_garage = $row_booking->b2b_vehicle_at_garage;
		$vehicle_ready = $row_booking->b2b_vehicle_ready;
	if($service_status == 'Completed' || $service_status == 'In Progress' || $check_in==1 || $at_garage == 1 || $vehicle_ready == 1)
	{
		$converted = $converted+1;
	}
	switch($service_status)
	{
		case 'Yet to Service his Vehicle': $yet_to = $yet_to + 1;
											break;
		case 'Completed': $completed = $completed + 1;
							break;
		case 'In Progress': $in_progress = $in_progress + 1;
							break;
		default: if($feedback_status>10 && $feedback_status<52)
					{
						$rnr = $rnr + 1;
					}
					elseif($feedback_status == 0)
					{
						$not_contacted = $not_contacted + 1;
					}
					break;
	}
	}
	$rate = ($converted/$total)*100;
} // if
else {
  //echo $sql_booking;
  $total = 0;
  $converted = 0;
  $rate = 0;
}
$conv_arr['total'] = $total;
$conv_arr['converted'] = $converted;
$conv_arr['rate'] = floor($rate)."%";
$conv_arr['yet_to'] = $yet_to;
$conv_arr['in_progress'] = $in_progress;
$conv_arr['completed'] = $completed;
$conv_arr['rnr'] = $rnr;
$conv_arr['not_contacted'] = $not_contacted;

echo json_encode($conv_arr);

?>
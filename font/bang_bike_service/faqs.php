<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
          <title>FAQ's - Express Car Service at Rs. 999</title>

		<link rel="icon" type="image/png" sizes="16x16" href="https://static.gobumpr.com/web-app/fav-icons/16.png">
		<link rel="icon" type="image/png" sizes="32x32" href="https://static.gobumpr.com/web-app/fav-icons/32.png">
		<link rel="icon" type="image/png" sizes="57x57" href="https://static.gobumpr.com/web-app/fav-icons/57.png">
		<link rel="icon" type="image/png" sizes="76x76" href="https://static.gobumpr.com/web-app/fav-icons/76.png">
		<link rel="icon" type="image/png" sizes="96x96" href="https://static.gobumpr.com/web-app/fav-icons/96.png">
		<link rel="icon" type="image/png" sizes="114x114" href="https://static.gobumpr.com/web-app/fav-icons/114.png">
		<link rel="icon" type="image/png" sizes="144x144" href="https://static.gobumpr.com/web-app/fav-icons/144.png">
		<link rel="icon" type="image/png" sizes="152x152" href="https://static.gobumpr.com/web-app/fav-icons/152x152.png">
        <!-- Latest compiled and minified JavaScript -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600" >
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="stylesheet" href="./css/faqPageCss.css">
        <style>
            .heading{font-size:30px}.help-button{display:none}.faq_answer{opacity:0;height:0}.faq_image{width:80px}
        </style>
    </head>
    <body>
       <div class="wrapper">
       <section id="header-section" >
       <nav class="navbar navbar-default">
                <div class="container-fluid">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                      <a href="#" class="help-button"><img src="https://static.gobumpr.com/tyres/help.svg" class="img-responsive" alt="GoBumpr FAQs"></a>         
                      <button type="button" class="navbar-toggle collapsed navbar-togglecollapsed" data-toggle="collapse" data-target="#bs-navbar-collapse-1" aria-expanded="false">
                        <img src="https://static.gobumpr.com/tyres/menu.png" class="menu-icon" alt="GoBumpr quick links"> 
                      </button>
                      <a class="logo" href="./index.php"><img src="https://static.gobumpr.com/img/logo-new-gb.svg"" class=" img-responsive gobumpr_logo" alt="GoBumpr Tyre Shop Online"/></a>
                       </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right" >
                      <li><a href="./index.php">HOME</a></li>
                      <li><a href="./about.php">ABOUT</a></li>
                      <li><a href="./about.php#contact_us">CONTACT</a></li>
                      <li><a href="#">FAQ</a></li>
                      <li><a href="./about.php#testimonials">TESTIMONIALS</a></li>
                    </ul>
                  </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
       <section class="faqs_section">
        <h1 class="heading">We're here to help</h1>
        <div class="container">
            <div class="questions">
                <div class="faq_item">
                    <p class="faq_question">What's included? <a href="#"><i class="fas fa-angle-down"></i></a></p>
                    <p class="faq_answer remove_transition">General Service includes air filter cleaning, oil filter cleaning, carburettor cleaning, clutch play adjustment, chain adjustment and lubrication, brake shoes cleaning and exterior foam wash.
                    </p>
                </div>
                <div class="faq_item">
                    <p class="faq_question"> Pricing <a href="#"><i class="fas fa-angle-down"></i></a></p>
                    <p class="faq_answer  remove_transition">Pricing includes all the services mentioned above. In addition to this, oil (manufacturer recommended / equivalent grade) change will be done at the same cost. Spares and consumables are chargeable. All prices mentioned in the website are exclusive of taxes.
                    </p>
                </div>
                <div class="faq_item">
                    <p class="faq_question">Time for Delivery <a href="#"><i class="fas fa-angle-down"></i></a></p>                    
                    <p class="faq_answer remove_transition">General service usually takes between 5 - 6 hours for the entire process.If any additional repairs / replacements are involved, the time taken may vary.
                    </p>
                </div>
                <div class="faq_item">
                    <p class="faq_question">Reliability<a href="#"><i class="fas fa-angle-down"></i></a></p>                    
                    <p class="faq_answer remove_transition">Our service partners use only OEM spare parts (or equivalent grade). There will be no compromise in the quality of workmanship. In case of any queries, you can reach out to us at 9003251754. Alternatively, you can send an e-mail to support@gobumpr.com
                    </p>
                </div>
                <!-- <div class="faq_item">
                    <p class="faq_question">What is Service and Check-up? <a href="#"><i class="fas fa-angle-down"></i></a></p>                    
                    <p class="faq_answer remove_transition">Express Car Service comprises of complete car health check-up followed by submission of an inspection report on the status of the vehicle. Post evaluation of the vehicle's condition, the service center will advise a course of action for the same. General service on the other hand comprises of oil filter replacement, oil change, suspension check-up, AC filter replacement & air filter replacement. The cost incurred for the same would be significantly higher and would vary depending upon the make & model of the vehicle. 
                    </p>
                </div> -->
                <!-- <div class="faq_item">
                    <p class="faq_question">What is the assurance for Service Quality? <a href="#"><i class="fas fa-angle-down"></i></a></p>                    
                    <p class="faq_answer remove_transition">Our service partners use only OEM spare parts (or equivalent grade). There will be no compromise in the quality of workmanship. In case of any queries, you can reach out to us at 9003251754. Alternatively, you can send an e-mail to support@gobumpr.com
                    </p>
                </div> --> 
                <div class='faq_item'>
                    <div class="row">
                        <div class="col-xs-5 text-right">
                            <img src="https://static.gobumpr.com/tyres/FAQ.svg" class="faq_image" alt="GoBumpr FAQs">
                        </div>
                        <div class="col-xs-7 text-left">
                            <p>Can’t find answer to your question?</p>
                            <p>Feel free to <a href="./about#contact_us" target="_blank" class="contact_us_link">contact us</a>, we are always ready to assist you.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
       </section>
  <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <script src="./js/faqScript.js"></script>          
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <noscript id="deferred-styles">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600" >
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
            <link rel="stylesheet" href="./css/aboutPageCss.css">
            <link rel="stylesheet" href="./css/faqPageCss.css">
        </noscript>
        <script>
          var loadDeferredStyles = function() {
            var addStylesNode = document.getElementById("deferred-styles");
            var replacement = document.createElement("div");
            replacement.innerHTML = addStylesNode.textContent;
            document.body.appendChild(replacement)
            addStylesNode.parentElement.removeChild(addStylesNode);
          };
          var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
              window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
          if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
          else window.addEventListener('load', loadDeferredStyles);
        </script>
    </body>
</html>

$(function() {
    $("#vehicle_tags").autocomplete({
        source: function(e, t) {
            var n = e.term;
            "" === n && (n = "A"), $.ajax({
                url: "./get_vehicles.php",
                data: {
                    term: n
                },
                dataType: "json"
            }).done(function(e) {
                t(null !== e ? $.map(e, function(e) {
                    return e
                }) : $.map(e, function(e) {
                    return "No Results"
                }))
            })
        },
        delay: 2,
        minLength: 0,
        response: function(e, t) {
            t.content.length ? $("#message").empty() : t.content.push({
                value: "",
                label: "No results found"
            })
        },
        change: function(e, t) {
            t.item || (e.target.value = ""), $("#required").hide()
        },
        open: function(e, t) {
            navigator.userAgent.match(/(iPod|iPhone|iPad)/) && $(".ui-autocomplete").off("menufocus hover mouseover mouseenter"), $(".enter-vehicle-input").on("keypress", function(e) {
                13 == e.which && e.preventDefault()
            })
        },
        select: function(e, t) {
            $(".enter-vehicle-input").unbind("keypress")
        }
    })
    .focus(function() {
        $(this).autocomplete("search")
  });
$("#location_tags").autocomplete({
        source: function(e, t) {
            var n = e.term;
            "" === n && (n = "A"), $.ajax({
                url: "./get_localities.php",
                data: {
                    term: n
                },
                dataType: "json"
            }).done(function(e) {
                t(null !== e ? $.map(e, function(e) {
                    return e
                }) : $.map(e, function(e) {
                    return "No Results"
                }))
            })
        },
        delay: 2,
        minLength: 0,
        response: function(e, t) {
            t.content.length ? $("#message").empty() : t.content.push({
                value: "",
                label: "No results found"
            })
        },
        change: function(e, t) {
//            console.log("changed");
//            t.item || (e.target.value = ""), t.item ? $("#location-entry").attr("disabled", !1) : $("#location-entry").attr("disabled", !0)
        },
        focus: function(e, t) {
            $("#location-entry").attr("disabled", true);
        },
        open: function(e, t) {
            $(".location-input").keypress(function(t){
                if(t.which==13){
                    t.preventDefault();
                }
            });
            $("#location-entry").on("click",function(e,t){
                $("#locaton-entry").attr("disabled",true);
            });
            //navigator.userAgent.match(/(iPod|iPhone|iPad)/) && $(".ui-autocomplete").off("menufocus hover mouseover mouseenter")
        },
        select: function(e, t) {
            $(".location-input").unbind("keypress");
            if(e.which==13){
                $("#location-entry").trigger("click");
            }
            $("#location-entry").attr("disabled",false);
        }
    }).focus(function() {
        $(this).autocomplete("search");
        $("#location-entry").attr("disabled", true);
    });
});
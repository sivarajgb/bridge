<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id']))) {
	
	header('location:logout.php');
	die();
}
if($crm_flag==1 && $flag!=1){
	header('location:logout.php');
	die();
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>GoBumpr Bridge</title>

  <!-- Facebook Pixel Code -->
  <script async>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '582926561860139');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
    /></noscript>
  <!-- DO NOT MODIFY -->
  <!-- End Facebook Pixel Code -->
  <!-- Google Analytics Code -->
  <script async>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-67994843-2', 'auto');
    ga('send', 'pageview');

  </script>

  <style>
    html{min-height:100%;}*{box-sizing:border-box;}body{color:black;background:rgb(255, 255, 255) !important;margin:0px;min-height:inherit;}[data-sidebar-overlay]{display:none;position:fixed;top:0px;bottom:0px;left:0px;opacity:0;width:100%;min-height:inherit;}.overlay{background-color:rgb(222, 214, 196);z-index:999990 !important;}aside{position:relative;height:100%;width:200px;top:0px;left:0px;background-color:rgb(236, 239, 241);box-shadow:rgba(0, 0, 0, 0.8) 0px 6px 6px 0px;z-index:999999 !important;}[data-sidebar]{display:none;position:absolute;height:100%;z-index:100;}.padding{padding:2em;}.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6{font-family:inherit;font-weight:500;line-height:1.1;color:inherit;}.h4, .h5, .h6, h4, h5, h6{margin-top:10px;margin-bottom:10px;}.h4, h4{font-size:18px;}img{border:0px;vertical-align:middle;}a{text-decoration:none;color:black;}aside a{color:rgb(0, 0, 0);font-size:16px;text-decoration:none;}.fa{display:inline-block;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;line-height:1;font-family:FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;}nav, ol{font-size:18px;margin-top:-4px;background:rgb(0, 150, 136) !important;}.navbar-fixed-top{z-index:100 !important;}.container-fluid{padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto;}.breadcrumb > li{display:inline-block;}ol, ul{margin-top:0px;margin-bottom:10px;}ol ol, ol ul, ul ol, ul ul{margin-bottom:0px;}.nav{padding-left:0px;margin-bottom:0px;list-style:none;}.navbar-nav{margin:0px;float:left;}.navbar-right{margin-right:-15px;float:right !important;}.nav > li{position:relative;display:block;}.navbar-nav > li{float:left;}.dropdown{position:relative;display:inline-block;}.form-group{margin-bottom:15px;}button, input, optgroup, select, textarea{margin:0px;font-style:inherit;font-variant:inherit;font-weight:inherit;font-stretch:inherit;font-size:inherit;line-height:inherit;font-family:inherit;color:inherit;}button, select{text-transform:none;}button, input, select, textarea{font-family:inherit;font-size:inherit;line-height:inherit;}.form-control{display:block;width:100%;height:34px;padding:6px 12px;font-size:14px;line-height:1.42857;color:rgb(85, 85, 85);background-color:rgb(255, 255, 255);background-image:none;border:1px solid rgb(204, 204, 204);border-radius:4px;box-shadow:rgba(0, 0, 0, 0.075) 0px 1px 1px inset;transition:border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;}.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9{position:relative;min-height:1px;padding-right:15px;padding-left:15px;}.col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9{float:left;}.col-sm-5{width:41.6667%;}.floating-box1{display:inline-block;}.glyphicon{position:relative;top:1px;display:inline-block;font-family:"Glyphicons Halflings";font-style:normal;font-weight:400;line-height:1;-webkit-font-smoothing:antialiased;}b, strong{font-weight:700;}.caret{display:inline-block;width:0px;height:0px;margin-left:2px;vertical-align:middle;border-top:4px dashed;border-right:4px solid transparent;border-left:4px solid transparent;}.col-sm-3{width:25%;}.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9{float:left;}.col-lg-2{width:16.6667%;}.col-sm-2{width:16.6667%;}table{border-spacing:0px;border-collapse:collapse;background-color:transparent;}#tbody, tbody tr{animation:opacity 5s ease-in-out;}td, th{padding:0px;}.switch{position:relative;display:inline-block;width:46px;height:25px;}.switch input{display:none;}.slider{position:absolute;cursor:pointer;top:0px;left:0px;right:0px;bottom:0px;background-color:rgb(204, 204, 204);transition:0.4s;}.slider.round{border-radius:38px;}.table{width:100%;max-width:100%;margin-bottom:20px;}.table-bordered{border:1px solid rgb(221, 221, 221);}th{text-align:left;}.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th{padding:8px;line-height:1.42857;vertical-align:top;border-top:1px solid rgb(221, 221, 221);}.table > thead > tr > th{vertical-align:bottom;border-bottom:2px solid rgb(221, 221, 221);}.table > caption + thead > tr:first-child > td, .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table > thead:first-child > tr:first-child > th{border-top:0px;}.table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th{border:1px solid rgb(221, 221, 221);}.table-bordered > thead > tr > td, .table-bordered > thead > tr > th{border-bottom-width:2px;}#tbody{font-size:15px !important;border:1.5px solid rgb(196, 184, 184) !important;}.uil-default-css{position:relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(1){animation:uil-default-anim 1s linear -0.5s infinite;}.uil-default-css > div:nth-of-type(2){animation:uil-default-anim 1s linear -0.416667s infinite;}.uil-default-css > div:nth-of-type(3){animation:uil-default-anim 1s linear -0.333333s infinite;}.uil-default-css > div:nth-of-type(4){animation:uil-default-anim 1s linear -0.25s infinite;}.uil-default-css > div:nth-of-type(5){animation:uil-default-anim 1s linear -0.166667s infinite;}.uil-default-css > div:nth-of-type(6){animation:uil-default-anim 1s linear -0.0833333s infinite;}.uil-default-css > div:nth-of-type(7){animation:uil-default-anim 1s linear 0s infinite;}.uil-default-css > div:nth-of-type(8){animation:uil-default-anim 1s linear 0.0833333s infinite;}.uil-default-css > div:nth-of-type(9){animation:uil-default-anim 1s linear 0.166667s infinite;}.uil-default-css > div:nth-of-type(10){animation:uil-default-anim 1s linear 0.25s infinite;}.uil-default-css > div:nth-of-type(11){animation:uil-default-anim 1s linear 0.333333s infinite;}.uil-default-css > div:nth-of-type(12){animation:uil-default-anim 1s linear 0.416667s infinite;}.dropdown-menu{position:absolute;top:100%;left:0px;z-index:1000;display:none;float:left;min-width:160px;padding:5px 0px;margin:2px 0px 0px;font-size:14px;text-align:left;list-style:none;background-color:rgb(255, 255, 255);-webkit-background-clip:padding-box;background-clip:padding-box;border:1px solid rgba(0, 0, 0, 0.15);border-radius:4px;box-shadow:rgba(0, 0, 0, 0.176) 0px 6px 12px;}button, html input[type="button"], input[type="reset"], input[type="submit"]{-webkit-appearance:button;cursor:pointer;}button[disabled], html input[disabled]{cursor:default;}button{overflow:visible;}.btn{display:inline-block;padding:6px 12px;margin-bottom:0px;font-size:14px;font-weight:400;line-height:1.42857;text-align:center;white-space:nowrap;vertical-align:middle;touch-action:manipulation;cursor:pointer;user-select:none;background-image:none;border:1px solid transparent;border-radius:4px;}.btn.disabled, .btn[disabled], fieldset[disabled] .btn{cursor:not-allowed;box-shadow:none;opacity:0.65;}.btn-success{color:rgb(255, 255, 255);background-color:rgb(92, 184, 92);border-color:rgb(76, 174, 76);}.btn-group-sm > .btn, .btn-sm{padding:5px 10px;font-size:12px;line-height:1.5;border-radius:3px;}.btn-default{color:rgb(51, 51, 51);background-color:rgb(255, 255, 255);border-color:rgb(204, 204, 204);}input{line-height:normal;}
    /*home page blocks */
    .floating-box1 {
    display: inline-block;
    }
    .navbar-fixed-top{
      z-index:100 !important;
    }
    .upper-div{
      z-index:999999 !important;
    }
    #range > span:hover{cursor: pointer;}
    /* table */
    #tbody{
      font-size:15px !important;
      border:1.5px solid #c4b8b8 !important;
    }
    thead:hover{
      cursor:pointer;
    }

    .results tr[visible='false'],
    .no-result{
      display:none;
    }

    .results tr[visible='true']{
      display:table-row;
    }

    .counter{
      padding:8px;
      color:#9E9E9E;
    }
    #tbody, tbody tr {
        -webkit-animation: opacity 5s ease-in-out;
        animation: opacity 5s ease-in-out;
    }

    .switch {
      position: relative;
      display: inline-block;
      width: 46px;
      height: 25px;
    }

    .switch input {display:none;}

    .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
    }

    .slider:before {
      position: absolute;
      content: "";
      height: 18px;
      width: 18px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
    }

    input:checked + .slider {
      background-color: #6ed4cb;
    }

    input:focus + .slider {
      box-shadow: 0 0 1px #6ed4cb;
    }

    input:checked + .slider:before {
      -webkit-transform: translateX(20px);
      -ms-transform: translateX(20px);
      transform: translateX(20px);
    }

    /* Rounded sliders */
    .slider.round {
      border-radius: 38px;
    }

    .slider.round:before {
      border-radius: 50%;
    }

  </style>

</head>
<body id="body">
<?php include_once("header.php"); ?>

<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>

<!-- date range picker -->
<div id="reportrange" class=" col-sm-5 " style="cursor: pointer; margin-top:28px; margin-left:10px;max-width:332px;">
    <div class=" floating-box1">
        <div id="range" class="form-control" style="max-width:332px;">
        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
        <span id="dateval"></span> <b class="caret"></b>
        </div>
    </div>
</div>

<!-- master service type filter -->
<div class=" col-sm-3 col-lg-2" style="cursor: pointer; margin-top:28px;max-width:170px;margin-left:-20px;">
    <div class=" floating-box1">
      <select id="master_service" name="master_service" class="form-control" style="max-width:170px;">
      <option value="all" selected>MasterService</option>
      <?php 
      $sql_master_service = "SELECT DISTINCT(master_service) FROM go_axle_service_price_tbl WHERE bridge_flag='0' ORDER BY  master_service ASC";
      $res_master_service = mysqli_query($conn,$sql_master_service);
      while($row_master_service = mysqli_fetch_object($res_master_service)){
        $master_service = $row_master_service->master_service;
        ?>
        <option value="<?php echo $master_service; ?>"><?php echo $master_service; ?></option>
        <?php
      }
      ?>
      </select>
    </div>
</div>
<!-- service type filter -->
<div class=" col-sm-2 col-lg-2" style="cursor: pointer; margin-top:28px;max-width:160px;margin-left:-20px;">
    <div class=" floating-box1">
      <select id="service" name="service" class="form-control" style="max-width:160px;">
        <option selected value="all">All Services</option>
        <?php
        $sql = "SELECT DISTINCT service_type FROM go_axle_service_price_tbl WHERE bridge_flag='0'";
        $res = mysqli_query($conn,$sql);
        while($row = mysqli_fetch_object($res)){
          ?>
          <option value="<?php echo $row->service_type; ?>"><?php echo $row->service_type; ?></option>
          <?php
        }
        ?>
      </select>
    </div>
</div>
<!-- Alloted to filter -->
<div class=" col-sm-2 col-lg-2 " style="cursor: pointer; margin-top:28px;max-width:170px;margin-left:-20px;">
  <div class=" floating-box1">
    <select id="person" name="person" class="form-control"style="max-width:170px;">
    </select>
  </div>
</div>

<!-- Programatic toggle -->
<div class=" col-sm-3 col-lg-2" style="cursor: pointer; margin-top:28px;max-width:220px;">
    <table>
    <tbody>
    <tr>
    <td><span class="floating-box1">Programmatic</span>&nbsp;&nbsp;&nbsp;</td>
    <td><label class="switch">
    <input type="checkbox" id="programatic" name="programatic" value="off" checked>
    <span class="slider round"></span>
  </label> 
  </td>  
  </tr> 
  </tbody>
  </table>
</div>
<!-- Online Payments -->
<div class=" col-sm-2 col-lg-2 " style="cursor: pointer; margin-top:28px;max-width:170px; display:none;" id="onlinepayments">
    <div class=" floating-box1">
    <i class="fa fa-credit-card-alt" aria-hidden="true" title="Total Online Payments" style="color:rgba(0, 114, 255, 0.66);"></i>&nbsp;&nbsp;<span id="payments" title="Total Online Payments">0</span>&nbsp;&nbsp;&nbsp;<i class="fa fa-inr" aria-hidden="true" title="Amount Paid Online" style="color:#ffa800;"></i>&nbsp;&nbsp;<span id="paid" title="Amount Paid Online">0</span>
    </div>
</div>
<div id="show" style="margin-top:82px;width:99%;display:none;">
<div id="division" style="float:left; margin-top:10px;margin-left:35px;width:65%;clear:both;">
  <div id="div1" style="width:89%;float:left;">
    <table class="table table-bordered table-stripped tablesorter" id="table" >
    <thead style="background-color:#B2DFDB;align:center;display: table-header-group;">
    <th style="text-align:center;">Date <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th style="text-align:center;">Day </th>
    <th style="text-align:center;">Car Credits <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th style="text-align:center;">Bike Credits <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th style="text-align:center;">Total <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    </thead>
    <tbody id="tbody">

    </tbody>
    </table>
  </div>
  <div id="rightpane" style="margin-top:-30px;position:fixed;right:30px;width:30%;">
    <div style="float:left;width:90%;">
    <table class="table table-bordered" style="border:1px solid #00BCD4;margin:15px;padding:15px;overflow:auto;" >
    <thead>
    <th colspan="2" style="border:1px solid #B2EBF2;text-align:center;background-color:#B2EBF2;" >Target</th>
    </thead>
    <tbody>
    <tr>
    <td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;">Car</td>
    <td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;" id="car_cpd">Credits/Day</td>
    </tr>
    <tr>
    <td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;">Bike</td>
    <td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;" id="bike_cpd" >Credits/Day</td>
    </tr>
    </tbody>
    </table>
    </div>
    <div style="float:left;width:38%;">
    <table class="table table-bordered" style="border:1px solid #00BCD4;margin:15px;padding:15px;" >
    <thead>
    <th colspan="2" style="border:1px solid #95ea85;text-align:center;background-color:#95ea85;" >Daily Average</th>
    </thead>
    <tbody>
    <tr>
    <td style="border:1px solid #95ea85;text-align:center;font-size:13px;">Car</td>
    <td style="border:1px solid #95ea85;text-align:center;font-size:13px;" id="caravg" ></td>
    </tr>
    <tr>
    <td style="border:1px solid #95ea85;text-align:center;font-size:13px;">Bike</td>
    <td style="border:1px solid #95ea85;text-align:center;font-size:13px;" id="bikeavg" ></td>
    </tr>
    <tr>
    <td style="border:1px solid #95ea85;text-align:center;font-size:13px;">Total</td>
    <td style="border:1px solid #95ea85;text-align:center;font-size:13px;" id="totalavg" ></td>
    </tr>
    </tbody>
    </table>
    </div>
    <div style="float:left;width:49%;margin-left:10px;">
    <table class="table table-bordered" style="border:1px solid #00BCD4;margin:15px;padding:15px;" >
    <thead>
    <th colspan="2" style="border:1px solid #d4c166;text-align:center;background-color:#d4c166;font-size:14px;" >Projected Consumption</th>
    </thead>
    <tbody>
    <tr>
    <td style="border:1px solid #d4c166;text-align:center;font-size:13px;">Car</td>
    <td style="border:1px solid #d4c166;text-align:center;font-size:13px;" id="carprocon" ></td>
    </tr>
    <tr>
    <td style="border:1px solid #d4c166;text-align:center;font-size:13px;">Bike</td>
    <td style="border:1px solid #d4c166;text-align:center;font-size:13px;" id="bikeprocon" ></td>
    </tr>
    <tr>
    <td style="border:1px solid #d4c166;text-align:center;font-size:13px;">Total</td>
    <td style="border:1px solid #d4c166;text-align:center;font-size:13px;" id="totalprocon" ></td>
    </tr>
    </tbody>
    </table>
    </div>
    <div style="float:left;width:90%;">
    <table class="table table-bordered" style="border:1px solid #00BCD4;margin:15px;padding:15px;" >
    <thead>
    <th style="border:1px solid #a3e08f;text-align:center;font-size:13px;background-color:#B2DFDB;"><i class="fa fa-crop" aria-hidden="true"></i></th>
    <th style="border:1px solid #B2DFDB;text-align:center;font-size:13px;background-color:#B2DFDB;">Car</th>
    <th style="border:1px solid #B2DFDB;text-align:center;font-size:13px;background-color:#B2DFDB;">Bike</th>
    </thead>
    <tbody>
    <tr>
    <td style="border:1px solid #B2DFDB;text-align:center;font-size:13px;background-color:#a3e08f;">Target</td>
    <td style="border:1px solid #B2DFDB;text-align:center;font-size:13px;" id="cartarget" ></td>
    <td style="border:1px solid #B2DFDB;text-align:center;font-size:13px;" id="biketarget" ></td>
    </tr>
    <tr>
    <td style="border:1px solid #B2DFDB;text-align:center;font-size:13px;background-color:#a3e08f;">Achieved</td>
    <td style="border:1px solid #B2DFDB;text-align:center;font-size:13px;" id="carachieved" ></td>
    <td style="border:1px solid #B2DFDB;text-align:center;font-size:13px;" id="bikeachieved" ></td>
    </tr>
    </tbody>
    </table>
    </div>
    <div style="width:90%;margin-left:8px;">
    <table class="table table-bordered" style="border:1px solid #00BCD4;margin:15px;" >
    <tbody>
    <tr>
    <td style="border:2px solid #fff;text-align:left;font-size:11px;background-color:#7aa8ec;vertical-align:middle;">&nbsp;</td>
    <td style="border:2px solid #fff;text-align:left;font-size:11px;">>100% Achieved</td>
	<td style="border:2px solid #fff;text-align:left;font-size:11px;background-color:#7bde95;vertical-align:middle;">&nbsp;</td>
    <td style="border:2px solid #fff;text-align:left;font-size:11px;">Target Achieved</td>
    <td style="border:2px solid #fff;text-align:left;font-size:11px;background-color:#f3ae2b;vertical-align:middle;">&nbsp;</td>
    <td style="border:2px solid #fff;text-align:left;font-size:11px;">90% Achieved</td>
    <td style="border:2px solid #fff;text-align:left;font-size:11px;background-color:#e88c87;vertical-align:middle;">&nbsp;</td>
    <td style="border:2px solid #fff;text-align:left;font-size:11px;">Target not Achieved</td>
    </tr>

    </tbody>
    </table>
    </div>
  </div>
</div>  
</div>
<!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>

<noscript id="async-styles">

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
</noscript>
<script> // The CSS mentioned in "async-styles" will be loaded and styles will be applied.
    var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("async-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
        webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function() {
        window.setTimeout(loadDeferredStyles, 0);
    });
    else window.addEventListener('load', loadDeferredStyles);
</script>



<script>
  function loadScript(src) {
      return new Promise(function (resolve, reject) {
          var s;
          s = document.createElement('script');
          s.src = src;
          s.onload = resolve;
          s.onerror = reject;
          document.head.appendChild(s);
      });
  }    

  function loadOtherScripts() {
    loadScript('//cdn.jsdelivr.net/momentjs/latest/moment.min.js').then(function() {
      Promise.all([ loadScript('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'),loadScript('//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js'),loadScript('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'),loadScript('js/sidebar.js'),loadScript('https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js')]).then(function () {
        //console.log('scripts are loaded');
        dateRangePicker();
        datePicker();
        intialFunction();
        tableSorter()
      }).catch(function (error) {
        //console.log('some error!' + error)
      })
      }).catch(function (error) {
        //console.log('Moment call error!' + error)
      })
  }
</script>
<script async src="https://code.jquery.com/jquery-3.2.1.js" onLoad="loadOtherScripts();"></script>
<!-- date range picker -->
<script>
function dateRangePicker(){
  $('input[name="daterange"]').daterangepicker({
  locale: {
        format: 'DD-MM-YYYY'
      }
  });
}
</script>
<script type="text/javascript">
function datePicker(){
  $(function() {

    //var start = moment().subtract(1, 'days');
    var start = moment().startOf('month');
    // var start = moment();
      var end = moment();

      function cb(start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }

      $('#reportrange').daterangepicker({
          startDate: start,
          endDate: end,
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, cb);

      cb(start, end);

  });
}
</script>
<script>
function defaultview(val){
	var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
	var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
  var master_service = $('#master_service').val();
  var service = $('#service').val();
  var person = $('#person').val();
  var programatic = $('#programatic').val();
  var city = $('#city').val();
  var cluster = $('#cluster').val();
  var prog;
  if(programatic === "on"){
    prog = "no";
  }
  else{
    prog = "yes";
  }
  
  console.log(prog);
	//Make AJAX request, using the selected value as the POST
  //console.log(programatic);
	$.ajax({
	    url : "ajax/daily_reports_view_clean.php",  // create a new php page to handle ajax request
	    type : "GET",
		// json : false,
		dataType: 'json',
	    data : {"startdate": startDate , "enddate": endDate, "master_service":master_service, "service":service, "person":person ,"programatic":prog ,"city":city, "cluster":cluster},
	    success : function(data) {
        //alert(data);
        console.log(data.data2);

        $('#show').show();
        $("#table").show();
        $("#loading").hide();
        $("#tbody").empty();
        if(data === "no"){
              $("#tbody").append("<h2 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h2>");
        }
        else{
          //$('#show').html(data);
          function displayInfo(item){
            $('#tbody').append(item['tr']);
            //console.log(item);
          }
		  function displayInfo2(item, index){
			  if(index == 'car_cpd' || index == 'bike_cpd'){
				$('#'+index).html(item+" Credits/Day");
			  }
			  else{
				$('#'+index).html(item);
			  }
			}
          $.map(data.data1, displayInfo);
          $.map(data.data2, displayInfo2);
		  $("#table").trigger("update");
		  //onlinepayments();
        }
       },
	   error: function(xhr, ajaxOptions, thrownError) {
	        // alert(xhr.status + " "+ thrownError);
	    }
	});
	$.ajax({
	    url : "ajax/daily_reports_onlinepay_view.php",  // create a new php page to handle ajax request
	    type : "GET",
	    data : {"startdate": startDate , "enddate": endDate, "city":city, "cluster":cluster},
	    success : function(data) {
			//alert(data);
		   // console.log(data);
       //console.log("online payments");
        $('#onlinepayments').show();
        var pays = data.split('.');
        var payments = pays[0];
        var paid = pays[1];
        $('#payments').html(payments);
        $('#paid').html(paid);
       },
	   error: function(xhr, ajaxOptions, thrownError) {
	        // alert(xhr.status + " "+ thrownError);
	    }
	});
  /* $.ajax({
	    url : "ajax/daily_reports_rightpane_view.php",  // create a new php page to handle ajax request
	    type : "GET",
      json : false,
	    data : {"startdate": startDate , "enddate": endDate, "master_service":master_service, "service":service, "person":person ,"programatic":prog ,"city":city, "cluster":cluster},
	    success : function(data) {
        //alert(data);

        console.log(data);
      
        function displayInfo(item, index){
          if(index == 'car_cpd' || index == 'bike_cpd'){
            $('#'+index).html(item+" Credits/Day");
          }
          else{
            $('#'+index).html(item);
          }
        }
        $.map($.parseJSON(data), displayInfo);
        onlinepayments();
       },
	   error: function(xhr, ajaxOptions, thrownError) {
	         alert(xhr.status + " "+ thrownError);
	    }
	}); */
}
</script>

<script>
/* function onlinepayments(val){
	var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
	var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
  var city = $('#city').val();
  var cluster = $('#cluster').val();
	//Make AJAX request, using the selected value as the POST
  //console.log(programatic);
	$.ajax({
	    url : "ajax/daily_reports_onlinepay_view.php",  // create a new php page to handle ajax request
	    type : "GET",
	    data : {"startdate": startDate , "enddate": endDate, "city":city, "cluster":cluster},
	    success : function(data) {
			//alert(data);
		   // console.log(data);
       //console.log("online payments");
        $('#onlinepayments').show();
        var pays = data.split('.');
        var payments = pays[0];
        var paid = pays[1];
        $('#payments').html(payments);
        $('#paid').html(paid);
       },
	   error: function(xhr, ajaxOptions, thrownError) {
	        // alert(xhr.status + " "+ thrownError);
	    }
	});
}
 */</script>

<script>
function getpeople(){
  //console.log("Get people");
  var city = $('#city').val();
  var cluster = $('#cluster').val();
  //console.log(city);
         $('#person').empty();
         $.ajax({
              url : "ajax/get_people_filter.php",  // create a new php page to handle ajax request
              type : "GET",
              data : {"city":city , "cluster" : cluster},
              success : function(data) {
            $('#person').append(data);  
            //console.log(data); 
            get_services();
            //console.log("get people");
          },
            error: function(xhr, ajaxOptions, thrownError) {
            //  alert(xhr.status + " "+ thrownError);
           }
         });
}
</script>
<!-- master seervice type -->
<script>
function get_services(){
  var master = $("#master_service").val();
  //console.log(master);
  $("#service").empty();
  $.ajax({
	    url : "ajax/get_services_master.php",  // create a new php page to handle ajax request
	    type : "POST",
	    data : {"master": master},
	    success : function(data) {
      	$('#service').append(data);	
        $("#table").hide();
      $("#loading").show();
      //console.log("get services");
      defaultview();	
    },
	    error: function(xhr, ajaxOptions, thrownError) {
	          //  alert(xhr.status + " "+ thrownError);
	    }
	});
}
</script>
<script>
  function tablesort(){
    $(document).ready(function()
      {
          $("#example").tablesorter( {sortList: [[0,0], [1,0]]} );
      }
    );
  }
  </script>
<script>
function intialFunction(){
  $(document).ready(function (){
    $('#editor').hide();
    $('#show').hide();
    $('#onlinepayments').hide();
    $("#loading").show();
    $("#city").show();
    $("#cluster").show();
    getpeople();

    $('#dateval').on("DOMSubtreeModified", function (){
      $('#editor').hide();
      $('#show').hide();
      $('#onlinepayments').hide();
      $("#loading").show();
      console.log("on date change");
      defaultview();
    });

    $("#master_service").change(function(){
      get_services();
      console.log("on master change");
    });

    $('#service').change(function(){
      console.log("on service change");
      $('#editor').hide();
      $('#show').hide();
      $('#onlinepayments').hide();
      $("#loading").show();
      defaultview();
    });

    $('#person').change(function (){
      console.log("on person change");
      $('#editor').hide();
      $('#show').hide();
      $('#onlinepayments').hide();
      $("#loading").show();
      defaultview();
    });
    $('#city').change(function (){
      console.log("on city change");
      $('#editor').hide();
      $('#show').hide();
      $('#onlinepayments').hide();
      $("#loading").show();
      getpeople();
    });
    $('#cluster').change(function (){
      console.log("on cluster change");
      $('#editor').hide();
      $('#show').hide();
      $('#onlinepayments').hide();
      $("#loading").show();
      getpeople();
    });

    $('#programatic').change(function (){
      var prog = $('#programatic').val();

      if(prog === 'on'){
        $('#programatic').val('off');
      }
      if(prog === 'off'){
        $('#programatic').val('on');
      }
      console.log("on prog change");
      $('#editor').hide();
      $('#show').hide();
      $("#loading").show();
      defaultview();
    });
  });
}
</script>
<script>
function tableSorter(){
$(document).ready(function()
    {
        $("#table").tablesorter( {sortList: [[0,0], [1,0]]} );
    }
);
}
</script>
</body>
</html>

<?php
ini_set('max_execution_time', '0');
include '../config.php';

$conn = db_connect1();
error_reporting(1);

$default_limit_per_user = 4;

//get re engage users count by city wise
$query_get_re_engage_users_count_in_city = 'SELECT city as city_name, count(*) as user_count, group_concat(crm_log_id) as crm_users FROM go_bumpr.crm_admin where crm_flag =1 and re_eng_flag=1 and city != "all" group by city';

$result = mysqli_query($conn, $query_get_re_engage_users_count_in_city);

$city_wise_re_engage_users = []; //city wise re_engage users
while ($row = mysqli_fetch_assoc($result)) {
    $city_wise_re_engage_users[] = $row;
}

echo '//==========city_wise_re_engage_users===========';
echo '<pre>';
print_r($city_wise_re_engage_users);
//exit;
//====================================================

//Process city wise
foreach ($city_wise_re_engage_users as $city) {

    //actual limit for city by number of users * max limit for user
    $default_limit_per_city = $city['user_count'] * $default_limit_per_user;
    echo '<br/>';
    echo '//==========' . $city['city_name'] . ' default total Slots required ===========';
    echo '<pre>';
    echo $default_limit_per_city;
//    continue;

    $crm_users = explode(',', $city['crm_users']);
    $city = $city['city_name'];

    //query for get currently available slot for each reengage crm admin
    $query_get_re_engage_user_existing_slots = 'SELECT crm_log_id, crm_update_id, crm_update_id, status, count(crm_update_id) AS available_slot FROM go_bumpr.ire_user_tbl where crm_update_id in("' . implode('","', $crm_users) . '") and status in(1,2) group by crm_update_id';

    $result = mysqli_query($conn, $query_get_re_engage_user_existing_slots);

    //currently existing slot for each reengage crm admin
    $re_engage_user_existing_slots = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $re_engage_user_existing_slots[] = $row;
    }

    // echo '<br/>';
    // echo '//==========' . $city . ' existing re_engage_user_slots===========';
    // echo $query_get_re_engage_user_existing_slots;
    // echo '<pre>';
    // print_r(array_sum(array_column($re_engage_user_existing_slots, 'available_slot')));
//    continue;

    $reengage_users = array_column($re_engage_user_existing_slots, 'crm_update_id');

    $extra_reengage_users = array_diff($crm_users, $reengage_users);

    foreach ($extra_reengage_users as $extra_reengage_user) {
        array_push($re_engage_user_existing_slots, array(
            'crm_update_id' => $extra_reengage_user,
            'available_slot' => 0
        ));
    }

    echo '<br/>';
    echo '//==========' . $city . ' re_engage_user existing Slots ===========';
    echo '<pre>';
    print_r($re_engage_user_existing_slots);
//    continue;

    //total available slot for city
    $total_available = array_sum(array_column($re_engage_user_existing_slots, 'available_slot'));

    echo '//==========' . $city . ' total existing Slots ===========';
    echo '<pre>';
    echo $total_available;
//    continue;

    //Required slot for city
    $required_limit = $default_limit_per_city - $total_available;

    echo '<br/>';
    echo '//==========' . $city . ' total Slots to be allot ===========';
    echo '<pre>';
    echo $required_limit;
//    exit;
    if ($required_limit < 1) {
        continue;
    }
//    continue;

    //call SP
    $query = "call go_bumpr.Final_IRE_Panel_New('$city','$required_limit')";

    $result = mysqli_query($conn, $query);
    $data_sp = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $data_sp[] = $row;
    }

    echo '<br/>';
    echo '//==========' . $city . ' records from SP ===========';
    echo '<pre>';
    echo count($data_sp);
    print_r($data_sp);

    mysqli_close($row);
    mysqli_next_result($conn);
//    exit;


    $today = date("Y-m-d H:i:s");

//    foreach ($re_engage_user_existing_slots as $re_engage_user) {
//        echo '<br/>';
//        echo 're engage users ' . $re_engage_user['crm_update_id'];
//        $slot_to_be_insert = 4 - $re_engage_user['available_slot'];
//
//        foreach ($data_sp as $key => $row) {
//            echo '<br/>';
//            echo '$slot_to_be_insert' . $slot_to_be_insert;
//            if ($slot_to_be_insert == 0) {
//                break;
//            }
//
//
//            $slot_to_be_insert--;
//            echo $row['booking_id'];
//
//
//            $ire_insert = "INSERT INTO go_bumpr.ire_user_tbl(`user_id`,`mobile_number`,`b2b_shop_id`,`mec_id`,`booking_id`,`locality`,`city`,`service_type`,`vehicle_type`,`service_date`,`shop_name`,`rating`,`feedback_status`,`log`,`update_log`,`crm_update_time`,`status`,`crm_log_id`,`crm_update_id`,`crm_allocate_id`,`crm_allocated_by`)VALUES('$row[user_id]','$row[mobile_number]','$row[axle_id]','$row[mec_id]','$row[booking_id]','$row[locality]','$row[city]','$row[service_type]','$row[vehicle_type]','$row[service_date]','$row[shop_name]','$row[rating]','$row[feedback_status]','$today','$today','$today','1','crm018','$re_engage_user[crm_update_id]','$re_engage_user[crm_update_id]','')";
//
//            $ire_insert = mysqli_query($conn, $ire_insert) or die(mysqli_error($conn));
//
//
//            echo $ire_insert . "<br>";
//
//            // $ire_res=$ire_insert) ;
//
//            flush();
//            ob_flush();
//            //mysqli_next_result($conn,$ire_res);
//            //mysqli_free_result($conn);
//
//            //if($ire_res){
//            $ire_user_update = "UPDATE go_bumpr.user_register SET ire_flag='1' where reg_id='$row[user_id]'";
//           //echo $ire_user_update;
//            $update_res = mysqli_query($conn, $ire_user_update) or die(mysqli_error($conn));
//            //echo "Success";
//
//            //}else{
//            //echo "fail";
//            // }
//            //sleep(1);
//        }
//    }
//    foreach ($re_engage_user_existing_slots as $re_engage_user) {
//
//        echo '<br/>';
//        echo 're engage users ' . $re_engage_user['crm_update_id'];
//        $slot_to_be_insert = 120 - $re_engage_user['available_slot'];
//        exit;

    $current_crm_user = 0;
    $slot_inserted = 1;
    $resArr = [];
    echo '<pre>';
    print_r($re_engage_user_existing_slots);
    for ($i = 0; $i < count($data_sp); $i++) {
        $crm_user = $re_engage_user_existing_slots[$current_crm_user];
        $slot_to_be_insert = $default_limit_per_user - $crm_user['available_slot'];
        $crm_id = $crm_user['crm_update_id'];

        array_push($resArr, array(
            'booking_id' => $data_sp[$i]['booking_id'],
            'crm_user' => $crm_user['crm_update_id'],
            'slot_to_be_insert' => $slot_to_be_insert,
            'slot_inserted' => $slot_inserted,
        ));
        echo '<pre/>';

        $row = $data_sp[$i];
        $ire_insert = "INSERT INTO go_bumpr.ire_user_tbl(`user_id`,`mobile_number`,`b2b_shop_id`,`mec_id`,`booking_id`,`locality`,`city`,`service_type`,`vehicle_type`,`service_date`,`shop_name`,`rating`,`feedback_status`,`log`,`update_log`,`crm_update_time`,`status`,`crm_log_id`,`crm_update_id`,`crm_allocate_id`,`crm_allocated_by`)VALUES('$row[user_id]','$row[mobile_number]','$row[axle_id]','$row[mec_id]','$row[booking_id]','$row[locality]','$row[city]','$row[service_type]','$row[vehicle_type]','$row[service_date]','$row[shop_name]','$row[rating]','$row[feedback_status]','$today','$today','$today','1','crm018','$crm_user[crm_update_id]','$crm_user[crm_update_id]','')";

        $ire_insert = mysqli_query($conn, $ire_insert) or die(mysqli_error($conn));

        $ire_user_update = "UPDATE go_bumpr.user_register SET ire_flag='1' where reg_id='$row[user_id]'";

        $update_res = mysqli_query($conn, $ire_user_update) or die(mysqli_error($conn));
        if ($slot_to_be_insert == $slot_inserted) {
            $current_crm_user++;
            $slot_inserted = 1;
        } else {
            $slot_inserted++;
        }

        flush();
        ob_flush();
        mysqli_free_result($conn);
    }
    // print_r($resArr);
    //
    // exit;
}

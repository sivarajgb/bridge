<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id']))) {
	
	header('location:logout.php');
	die();
}
if($_SESSION['crm_log_id']!='crm003'&&$_SESSION['crm_log_id']!='crm004'&&$_SESSION['crm_log_id']!='crm005'&&$_SESSION['crm_log_id']!='crm018'&&$_SESSION['crm_log_id']!='crm225'&&$_SESSION['crm_log_id']!='crm296'){
	header('location:logout.php');
	die();
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>GoBumpr Bridge</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">


  <!-- Facebook Pixel Code -->
  <script async>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '582926561860139');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
    /></noscript>
  <!-- DO NOT MODIFY -->
  <!-- End Facebook Pixel Code -->
  <!-- Google Analytics Code -->
  <script async>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-67994843-2', 'auto');
    ga('send', 'pageview');

  </script>

  <style>
    html{min-height:100%;}*{box-sizing:border-box;}body{color:black;background:rgb(255, 255, 255) !important;margin:0px;min-height:inherit;}[data-sidebar-overlay]{display:none;position:fixed;top:0px;bottom:0px;left:0px;opacity:0;width:100%;min-height:inherit;}.overlay{background-color:rgb(222, 214, 196);z-index:999990 !important;}aside{position:relative;height:100%;width:200px;top:0px;left:0px;background-color:rgb(236, 239, 241);box-shadow:rgba(0, 0, 0, 0.8) 0px 6px 6px 0px;z-index:999999 !important;}[data-sidebar]{display:none;position:absolute;height:100%;z-index:100;}.padding{padding:2em;}.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6{font-family:inherit;font-weight:500;line-height:1.1;color:inherit;}.h4, .h5, .h6, h4, h5, h6{margin-top:10px;margin-bottom:10px;}.h4, h4{font-size:18px;}img{border:0px;vertical-align:middle;}a{text-decoration:none;color:black;}aside a{color:rgb(0, 0, 0);font-size:16px;text-decoration:none;}.fa{display:inline-block;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;line-height:1;font-family:FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;}nav, ol{font-size:18px;margin-top:-4px;background:rgb(0, 150, 136) !important;}.navbar-fixed-top{z-index:100 !important;}.container-fluid{padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto;}.breadcrumb > li{display:inline-block;}ol, ul{margin-top:0px;margin-bottom:10px;}ol ol, ol ul, ul ol, ul ul{margin-bottom:0px;}.nav{padding-left:0px;margin-bottom:0px;list-style:none;}.navbar-nav{margin:0px;float:left;}.navbar-right{margin-right:-15px;float:right !important;}.nav > li{position:relative;display:block;}.navbar-nav > li{float:left;}.dropdown{position:relative;display:inline-block;}.form-group{margin-bottom:15px;}button, input, optgroup, select, textarea{margin:0px;font-style:inherit;font-variant:inherit;font-weight:inherit;font-stretch:inherit;font-size:inherit;line-height:inherit;font-family:inherit;color:inherit;}button, select{text-transform:none;}button, input, select, textarea{font-family:inherit;font-size:inherit;line-height:inherit;}.form-control{display:block;width:100%;height:34px;padding:6px 12px;font-size:14px;line-height:1.42857;color:rgb(85, 85, 85);background-color:rgb(255, 255, 255);background-image:none;border:1px solid rgb(204, 204, 204);border-radius:4px;box-shadow:rgba(0, 0, 0, 0.075) 0px 1px 1px inset;transition:border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;}.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9{position:relative;min-height:1px;padding-right:15px;padding-left:15px;}.col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9{float:left;}.col-sm-5{width:41.6667%;}.floating-box1{display:inline-block;}.glyphicon{position:relative;top:1px;display:inline-block;font-family:"Glyphicons Halflings";font-style:normal;font-weight:400;line-height:1;-webkit-font-smoothing:antialiased;}b, strong{font-weight:700;}.caret{display:inline-block;width:0px;height:0px;margin-left:2px;vertical-align:middle;border-top:4px dashed;border-right:4px solid transparent;border-left:4px solid transparent;}.col-sm-3{width:25%;}.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9{float:left;}.col-lg-2{width:16.6667%;}.col-sm-2{width:16.6667%;}table{border-spacing:0px;border-collapse:collapse;background-color:transparent;}#tbody, tbody tr{animation:opacity 5s ease-in-out;}td, th{padding:0px;}.switch{position:relative;display:inline-block;width:46px;height:25px;}.switch input{display:none;}.slider{position:absolute;cursor:pointer;top:0px;left:0px;right:0px;bottom:0px;background-color:rgb(204, 204, 204);transition:0.4s;}.slider.round{border-radius:38px;}.table{width:100%;max-width:100%;margin-bottom:20px;}.table-bordered{border:1px solid rgb(221, 221, 221);}th{text-align:left;}.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th{padding:8px;line-height:1.42857;vertical-align:top;border-top:1px solid rgb(221, 221, 221);}.table > thead > tr > th{vertical-align:bottom;border-bottom:2px solid rgb(221, 221, 221);}.table > caption + thead > tr:first-child > td, .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table > thead:first-child > tr:first-child > th{border-top:0px;}.table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th{border:1px solid rgb(221, 221, 221);}.table-bordered > thead > tr > td, .table-bordered > thead > tr > th{border-bottom-width:2px;}#tbody{font-size:15px !important;border:1.5px solid rgb(196, 184, 184) !important;}.uil-default-css{position:relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(1){animation:uil-default-anim 1s linear -0.5s infinite;}.uil-default-css > div:nth-of-type(2){animation:uil-default-anim 1s linear -0.416667s infinite;}.uil-default-css > div:nth-of-type(3){animation:uil-default-anim 1s linear -0.333333s infinite;}.uil-default-css > div:nth-of-type(4){animation:uil-default-anim 1s linear -0.25s infinite;}.uil-default-css > div:nth-of-type(5){animation:uil-default-anim 1s linear -0.166667s infinite;}.uil-default-css > div:nth-of-type(6){animation:uil-default-anim 1s linear -0.0833333s infinite;}.uil-default-css > div:nth-of-type(7){animation:uil-default-anim 1s linear 0s infinite;}.uil-default-css > div:nth-of-type(8){animation:uil-default-anim 1s linear 0.0833333s infinite;}.uil-default-css > div:nth-of-type(9){animation:uil-default-anim 1s linear 0.166667s infinite;}.uil-default-css > div:nth-of-type(10){animation:uil-default-anim 1s linear 0.25s infinite;}.uil-default-css > div:nth-of-type(11){animation:uil-default-anim 1s linear 0.333333s infinite;}.uil-default-css > div:nth-of-type(12){animation:uil-default-anim 1s linear 0.416667s infinite;}.dropdown-menu{position:absolute;top:100%;left:0px;z-index:1000;display:none;float:left;min-width:160px;padding:5px 0px;margin:2px 0px 0px;font-size:14px;text-align:left;list-style:none;background-color:rgb(255, 255, 255);-webkit-background-clip:padding-box;background-clip:padding-box;border:1px solid rgba(0, 0, 0, 0.15);border-radius:4px;box-shadow:rgba(0, 0, 0, 0.176) 0px 6px 12px;}button, html input[type="button"], input[type="reset"], input[type="submit"]{-webkit-appearance:button;cursor:pointer;}button[disabled], html input[disabled]{cursor:default;}button{overflow:visible;}.btn{display:inline-block;padding:6px 12px;margin-bottom:0px;font-size:14px;font-weight:400;line-height:1.42857;text-align:center;white-space:nowrap;vertical-align:middle;touch-action:manipulation;cursor:pointer;user-select:none;background-image:none;border:1px solid transparent;border-radius:4px;}.btn.disabled, .btn[disabled], fieldset[disabled] .btn{cursor:not-allowed;box-shadow:none;opacity:0.65;}.btn-success{color:rgb(255, 255, 255);background-color:rgb(92, 184, 92);border-color:rgb(76, 174, 76);}.btn-group-sm > .btn, .btn-sm{padding:5px 10px;font-size:12px;line-height:1.5;border-radius:3px;}.btn-default{color:rgb(51, 51, 51);background-color:rgb(255, 255, 255);border-color:rgb(204, 204, 204);}input{line-height:normal;}
    /*home page blocks */
    .floating-box1 {
    display: inline-block;
    }
    .navbar-fixed-top{
      z-index:100 !important;
    }
    .upper-div{
      z-index:999999 !important;
    }
    #range > span:hover{cursor: pointer;}
    /* table */
    #tbody{
      font-size:15px !important;
      border:1.5px solid #c4b8b8 !important;
    }
    thead:hover{
      cursor:pointer;
    }

    .results tr[visible='false'],
    .no-result{
      display:none;
    }

    .results tr[visible='true']{
      display:table-row;
    }

    .counter{
      padding:8px;
      color:#9E9E9E;
    }
    #tbody, tbody tr {
        -webkit-animation: opacity 5s ease-in-out;
        animation: opacity 5s ease-in-out;
    }

    .switch {
      position: relative;
      display: inline-block;
      width: 46px;
      height: 25px;
    }

    .switch input {display:none;}

    .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
    }

    .slider:before {
      position: absolute;
      content: "";
      height: 18px;
      width: 18px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
    }

    input:checked + .slider {
      background-color: #6ed4cb;
    }

    input:focus + .slider {
      box-shadow: 0 0 1px #6ed4cb;
    }

    input:checked + .slider:before {
      -webkit-transform: translateX(20px);
      -ms-transform: translateX(20px);
      transform: translateX(20px);
    }

    /* Rounded sliders */
    .slider.round {
      border-radius: 38px;
    }

    .slider.round:before {
      border-radius: 50%;
    }
	
	.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{ vertical-align:inherit!important;}
    @media screen and (min-width: 769px){
    #mymodal {
      text-align: center;
      padding: 0!important;
    }

    #mymodal:before {
      content: '';
      display: inline-block;
      height: 100%;
      vertical-align: middle;
      margin-right: -4px;
    }

    #mymodal .modal-dialog {
      display: inline-block;
      text-align: left;
      vertical-align: middle;
      width: auto;
    }
    }

  </style>

</head>
<body id="body">
<?php include_once("header.php"); ?>

<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>

<!-- date range picker -->
<div id="reportrange" class=" col-sm-5 " style="cursor: pointer; margin-top:28px; margin-left:10px;max-width:342px;display: inline">
    <div class=" floating-box1">
        <div id="range" class="form-control" style="max-width:332px;">
        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
        <span id="dateval"></span> <b class="caret"></b>
        </div>
    </div>
</div>
<select id="vehicle-type">
  <option value="all">All</option>
  <option value="2w">2W</option>
  <option value="4w">4W</option>
</select>
<a href="ebm_revenue.php" target="_blank" id="ebm"><button style="margin-top:28px; margin-left:10px;outline: none;border: none;background-color: #009688;padding:0.5em;color: white;border-radius:5px;box-shadow: 2px 2px 2px grey">EBM Revenue</button></a>
<a href="swap_revenue.php" target="_blank" id="swap"><button style="margin-top:28px; margin-left:10px;outline: none;border: none;background-color: #009688;padding:0.5em;color:white;border-radius:5px;box-shadow: 2px 2px 2px grey">Swap Revenue</button></a>

<div id="show" style="margin-top:50px;display:none;">
<div id="division" style="margin-top:10px;margin-left:2%;margin-right:2%;clear:both;">
  <div id="div1">
    <table class="table table-bordered table-stripped" id="table" >
    <thead style="background-color:#B2DFDB;align:center;display: table-header-group;">
    <th style="text-align:center;">Date <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th style="text-align:center;">Day </th>
	<th style="text-align:center;">GoAxles</th>
	<th style="text-align:center;">GoAxles GMV</th>
	<th style="text-align:center;">Total Revenue</th>
    </thead>
    <tbody id="tbody">

    </tbody>
    <tbody id="tbody1" class="avoid-sort">

    </tbody>
    <tbody id="tbody2" class="avoid-sort">

    </tbody>
    <tbody id="tbody3" class="avoid-sort">

    </tbody>
    </table>
  </div>
</div>  
</div>
<div class="modal fade" id="mymodal" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h3 id="modal-city" style="color:#ffa800"></h3>        
      </div>
      <div class="modal-body">
        <table class="table table-hover table-striped table-responsive" style="table-layout: auto;width:500px;">
          <tr>
            <th>Model</th>
            <th style="text-align: center;">No of Bookings</th>
            <th style="text-align: center;">Revenue</th>
          </tr>
          <tr>
            <td>Pre-credit 4W</td>
            <td id="precredit-4-goaxles" style="text-align: center;">0</td>
            <td id="precredit-4" style="text-align: center;">0</td>
          </tr>
          <tr>
            <td>Pre-credit 2W</td>
            <td id="precredit-2-goaxles" style="text-align: center;">0</td>
            <td id="precredit-2" style="text-align: center;">0</td>
          </tr>
          <tr>
            <td>Premium 4W</td>
            <td id="premium-4-goaxles" style="text-align: center;">0</td>
            <td id="premium-4" style="text-align: center;">0</td>
          </tr>
          <tr>
            <td>Premium 2W</td>
            <td id="premium-2-goaxles" style="text-align: center;">0</td>
            <td id="premium-2" style="text-align: center;">0</td>
          </tr>
          <tr>
            <td>Leads 3.0 4W</td>
            <td id="leads-4-goaxles" style="text-align: center;">0</td>
            <td id="leads-4" style="text-align: center;">0</td>
          </tr>
          <tr>
            <td>Leads 3.0 2W</td>
            <td id="leads-2-goaxles" style="text-align: center;">0</td>
            <td id="leads-2" style="text-align: center;">0</td>
          </tr>
          <tr>
            <td>EBM 2.0 Pre-credit</td>
            <td id="ebm-precredit-goaxles" style="text-align: center;">0</td>
            <td id="ebm-precredit" style="text-align: center;">0</td>
          </tr>
          <tr>
            <td>EBM 2.0 Premium</td>
            <td id="ebm-premium-goaxles" style="text-align: center;">0</td>
            <td id="ebm-premium" style="text-align: center;">0</td>
          </tr>
          <tr>
            <td>EBM 2.0 Leads 3.0</td>
            <td id="ebm-leads-goaxles" style="text-align: center;">0</td>
            <td id="ebm-leads" style="text-align: center;">0</td>
          </tr>
          <tr class="info">
            <td>Total </td>
            <td id="total-modal-goaxles" style="text-align: center;">0</td>
            <td id="total-credits" style="text-align: center;">0</td>
          </tr>
        </table>
      </div>
      <div class="modal-footer" style="text-align: center">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
    
  </div>
</div>

<!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>

<noscript id="async-styles">

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
</noscript>
<script> // The CSS mentioned in "async-styles" will be loaded and styles will be applied.
    var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("async-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
        webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function() {
        window.setTimeout(loadDeferredStyles, 0);
    });
    else window.addEventListener('load', loadDeferredStyles);
</script>



<script>
  function loadScript(src) {
      return new Promise(function (resolve, reject) {
          var s;
          s = document.createElement('script');
          s.src = src;
          s.onload = resolve;
          s.onerror = reject;
          document.head.appendChild(s);
      });
  }    

  function loadOtherScripts() {
    loadScript('//cdn.jsdelivr.net/momentjs/latest/moment.min.js').then(function() {
      Promise.all([ loadScript('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'),loadScript('//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js'),loadScript('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'),loadScript('js/sidebar.js'),loadScript('https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js')]).then(function () {
        //console.log('scripts are loaded');
        dateRangePicker();
        datePicker();
        intialFunction();
        tableSorter();
        modalFunction();
      }).catch(function (error) {
        //console.log('some error!' + error)
      })
      }).catch(function (error) {
        //console.log('Moment call error!' + error)
      })
  }
</script>
<script async src="https://code.jquery.com/jquery-3.2.1.js" onLoad="loadOtherScripts();"></script>
<!-- date range picker -->
<script>
function dateRangePicker(){
  $('input[name="daterange"]').daterangepicker({
  locale: {
        format: 'DD-MM-YYYY'
      }
  });
}
</script>
<script type="text/javascript">
function datePicker(){
  $(function() {

    //var start = moment().subtract(1, 'days');
    var start = moment().startOf('month');
    // var start = moment();
      var end = moment();

      function cb(start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }

      $('#reportrange').daterangepicker({
          startDate: start,
          endDate: end,
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, cb);

      cb(start, end);

  });
}
</script>
<script>
function defaultview(val){
  var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
  var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
  var city = $('#city').val();
  var type=$('#vehicle-type').val();
  total_goaxle=0;
  total_gmv=0;
  total_revenue=0;
  
  //Make AJAX request, using the selected value as the POST
  //console.log(programatic);
  $.ajax({
      url : "ajax/revenue_report_v4.php",  // create a new php page to handle ajax request
      type : "GET",
      dataType: 'json',
      data : {"startdate": startDate , "enddate": endDate,"city":city,"type":type},
      success : function(data) {
        //alert(data);
        // console.log(data);
        sd=moment(startDate).format('MMM DD');
        ed=moment(endDate).format('MMM DD');
        total_goaxle=0;
        total_gmv=0;
        total_revenue=0;
        total_swap_gmv=0;
        total_swap_goaxle=0;
        total_swap_revenue=0;
        dateData=data[0];
        swapData=data[1];
        $('#show').show();

        $("#tbody").empty();
        $("#tbody1").empty();
        $("#tbody2").empty();
        $("#tbody3").empty();
        if(data === "no"){
              $("#tbody").append("<h2 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h2>");
        }
        else{
          $.each(dateData,function(key,value){
            $("#tbody").append('<tr><td style="text-align:center;">'+key+'</td><td style="text-align:center;">'+value['day']+'</td><td style="text-align:center;"><a href="revenue_report_details.php?dt='+btoa(key)+'&vt='+btoa("all")+'" target="_blank" style="text-decoration:underline;">'+value['goaxle']+'</a></td><td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+value['GMV']+'</td><td style="text-align:center;"><a id="revenue-col" href="#" data-date='+key+' style="text-decoration:underline;"><i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+Math.round(value['revenue'])+'</a></td></tr>');
            total_gmv+=value['GMV'];
            total_goaxle+=value['goaxle'];
            total_revenue+=value['revenue'];
            // console.log(key);
          });     
          
          $("#tbody1").append('<tr><td style="text-align:center;" colspan=2><strong>Total Goaxles ('+sd+' - '+ed+')</strong></td><td style="text-align:center;"><a href="revenue_report_details.php?dt='+btoa(startDate)+'&vt='+btoa("all")+'&et='+btoa(endDate)+'" target="_blank" style="text-decoration:underline;">'+total_goaxle+'</a></td><td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+total_gmv+'</td><td style="text-align:center;"><a id="revenue-col" href="#" data-date="all" style="text-decoration:underline;"><i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+Math.round(total_revenue)+'</a></td></tr>');
          $.each(swapData,function(key,value){
            total_swap_gmv+=value['GMV'];
            total_swap_goaxle+=value['goaxle'];
            total_swap_revenue+=value['revenue'];
          });
          $("#tbody2").append('<tr><td style="text-align:center;" colspan=2><strong>Swapped Goaxles ('+sd+' - '+ed+')</strong></td><td style="text-align:center;"><a href="swap_revenue.php?st='+btoa(startDate)+'&et='+btoa(endDate)+'" target="_blank" style="text-decoration:underline;">'+total_swap_goaxle+'</a></td><td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+total_swap_gmv+'</td><td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+Math.round(total_swap_revenue)+'</td></tr>');
          $("#tbody3").append('<tr class="info"><td style="text-align:center;" colspan=2><strong>Total</strong></td><td style="text-align:center;">'+(total_swap_goaxle+total_goaxle)+'</td><td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+(total_swap_gmv+total_gmv)+'</td><td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+(Math.round(total_swap_revenue)+Math.round(total_revenue))+'</td></tr>');
        }
        $("#table").trigger("update");
        $("#table").show();
        $("#loading").hide();
       },
     error: function(xhr, ajaxOptions, thrownError) {
          // alert(xhr.status + " "+ thrownError);
      }
  });


}
</script>

<script>
  function tablesort(){
    $(document).ready(function()
      {
          $("#table").tablesorter( {sortList: [[0,0], [1,0]]} );
      }
    );
  }
  </script>
<script>
function intialFunction(){
  $(document).ready(function (){
    $('#editor').hide();
    $('#show').hide();
    $("#loading").show();
    $("#city").show();
    $("#cluster").hide();
  defaultview();
    

    $('#dateval').on("DOMSubtreeModified", function (){
      $('#editor').hide();
      $('#show').hide();
      $("#loading").show();
      defaultview();
    });

    $('#vehicle-type').change(function (){
      $('#show').hide();
      $("#loading").show();
      defaultview();
    });
    $('#city').change(function (){
      console.log("on city change");
      $('#editor').hide();
      $('#show').hide();
      $("#loading").show();
      defaultview();
    });
    $("#ebm").click(function(event){
      event.preventDefault();
      var url=$(this).attr('href');
      var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
      var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
      window.open(url+"?st="+btoa(startDate)+"&et="+btoa(endDate),"_blank");
    });
    $("#swap").click(function(event){
      event.preventDefault();
      var url=$(this).attr('href');
      var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
      var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
      window.open(url+"?st="+btoa(startDate)+"&et="+btoa(endDate),"_blank");
    });

  });
}
</script>
<script>
function tableSorter(){
$(document).ready(function()
    {
        $("#table").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[0,0], [1,0]]} );
    }
);
}
</script>
<script type="text/javascript">
  function modalFunction(){
    
     $(document).on('click','#revenue-col',function(event){
        event.preventDefault();
        date_selected=$(this).attr('data-date');
        if(date_selected=="all"){
          var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
          var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
          $("#modal-city").html($("#city").val()+" <span style='float:right;color:black;font-size:0.8em;'>" +startDate+" - "+endDate+"</span>");
          var total_precredit_2w=0;
          var total_precredit_4w=0;
          var total_premium_2w=0;
          var total_premium_4w=0;
          var total_leads_2w=0;
          var total_leads_4w=0;
          var total_ebm_leads=0;
          var total_ebm_precredit=0;
          var total_ebm_premium=0;
          var modal_total=0;
          var total_goaxle_precredit_2w=0;
          var total_goaxle_precredit_4w=0;
          var total_goaxle_premium_2w=0;
          var total_goaxle_premium_4w=0;
          var total_goaxle_leads_2w=0;
          var total_goaxle_leads_4w=0;
          var total_goaxle_ebm_leads=0;
          var total_goaxle_ebm_precredit=0;
          var total_goaxle_ebm_premium=0;
          var modal_total_goaxle=0;
          $.each(dateData,function(key,value){
            total_precredit_2w+=value['precredit']['2w']['revenue'];
            total_precredit_4w+=value['precredit']['4w']['revenue'];
            total_premium_2w+=value['premium']['2w']['revenue'];
            total_premium_4w+=value['premium']['4w']['revenue'];
            total_leads_2w+=value['leads']['2w']['revenue'];
            total_leads_4w+=value['leads']['4w']['revenue'];
            total_ebm_leads+=value['ebm']['leads']['revenue'];
            total_ebm_premium+=value['ebm']['premium']['revenue'];
            total_ebm_precredit+=value['ebm']['precredit']['revenue'];
            total_goaxle_precredit_2w+=value['precredit']['2w']['goaxles'];
            total_goaxle_precredit_4w+=value['precredit']['4w']['goaxles'];
            total_goaxle_premium_2w+=value['premium']['2w']['goaxles'];
            total_goaxle_premium_4w+=value['premium']['4w']['goaxles'];
            total_goaxle_leads_2w+=value['leads']['2w']['goaxles'];
            total_goaxle_leads_4w+=value['leads']['4w']['goaxles'];
            total_goaxle_ebm_leads+=value['ebm']['leads']['goaxles'];
            total_goaxle_ebm_premium+=value['ebm']['premium']['goaxles'];
            total_goaxle_ebm_precredit+=value['ebm']['precredit']['goaxles'];
          });
          modal_total=total_precredit_2w+total_precredit_4w+total_premium_2w+total_premium_4w+total_leads_2w+total_leads_4w+total_ebm_leads+total_ebm_premium+total_ebm_precredit;
          modal_total_goaxle=total_goaxle_precredit_2w+total_goaxle_precredit_4w+total_goaxle_premium_2w+total_goaxle_premium_4w+total_goaxle_leads_2w+total_goaxle_leads_4w+total_goaxle_ebm_leads+total_goaxle_ebm_premium+total_goaxle_ebm_precredit;
          $("#precredit-2").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+total_precredit_2w);
          $("#precredit-4").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+total_precredit_4w);
          $("#premium-2").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+total_premium_2w);
          $("#premium-4").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+total_premium_4w);
          $("#leads-2").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+total_leads_2w);
          $("#leads-4").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+total_leads_4w);
          $("#ebm-leads").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+total_ebm_leads);
          $("#ebm-premium").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+total_ebm_premium);
          $("#ebm-precredit").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+total_ebm_precredit);
          $("#total-credits").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+modal_total);
          $("#precredit-2-goaxles").html(total_goaxle_precredit_2w);
          $("#precredit-4-goaxles").html(total_goaxle_precredit_4w);
          $("#premium-2-goaxles").html(total_goaxle_premium_2w);
          $("#premium-4-goaxles").html(total_goaxle_premium_4w);
          $("#leads-2-goaxles").html(total_goaxle_leads_2w);
          $("#leads-4-goaxles").html(total_goaxle_leads_4w);
          $("#ebm-leads-goaxles").html(total_goaxle_ebm_leads);
          $("#ebm-premium-goaxles").html(total_goaxle_ebm_premium);
          $("#ebm-precredit-goaxles").html(total_goaxle_ebm_precredit);
          $("#total-modal-goaxles").html(modal_total_goaxle);
        }else{
          $("#modal-city").html($("#city").val()+" <span style='float:right;color:black;font-size:0.8em;'>" +date_selected+"</span>");
          $("#precredit-2").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+dateData[date_selected]['precredit']['2w']['revenue']);
          $("#precredit-4").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+dateData[date_selected]['precredit']['4w']['revenue']);
          $("#premium-2").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+dateData[date_selected]['premium']['2w']['revenue']);
          $("#premium-4").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+dateData[date_selected]['premium']['4w']['revenue']);
          $("#leads-2").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+dateData[date_selected]['leads']['2w']['revenue']);
          $("#leads-4").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+dateData[date_selected]['leads']['4w']['revenue']);
          $("#ebm-leads").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+dateData[date_selected]['ebm']['leads']['revenue']);
          $("#ebm-premium").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+dateData[date_selected]['ebm']['premium']['revenue']);
          $("#ebm-precredit").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+dateData[date_selected]['ebm']['precredit']['revenue']);
          $("#total-credits").html('<i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '+(dateData[date_selected]['precredit']['2w']['revenue']+dateData[date_selected]['precredit']['4w']['revenue']+dateData[date_selected]['premium']['2w']['revenue']+dateData[date_selected]['premium']['4w']['revenue']+dateData[date_selected]['leads']['2w']['revenue']+dateData[date_selected]['leads']['4w']['revenue']+dateData[date_selected]['ebm']['premium']['revenue']+dateData[date_selected]['ebm']['precredit']['revenue']+dateData[date_selected]['ebm']['leads']['revenue']));
          $("#precredit-2-goaxles").html(dateData[date_selected]['precredit']['2w']['goaxles']);
          $("#precredit-4-goaxles").html(dateData[date_selected]['precredit']['4w']['goaxles']);
          $("#premium-2-goaxles").html(dateData[date_selected]['premium']['2w']['goaxles']);
          $("#premium-4-goaxles").html(dateData[date_selected]['premium']['4w']['goaxles']);
          $("#leads-2-goaxles").html(dateData[date_selected]['leads']['2w']['goaxles']);
          $("#leads-4-goaxles").html(dateData[date_selected]['leads']['4w']['goaxles']);
          $("#ebm-leads-goaxles").html(dateData[date_selected]['ebm']['leads']['goaxles']);
          $("#ebm-premium-goaxles").html(dateData[date_selected]['ebm']['premium']['goaxles']);
          $("#ebm-precredit-goaxles").html(dateData[date_selected]['ebm']['precredit']['goaxles']);
          $("#total-modal-goaxles").html(dateData[date_selected]['precredit']['2w']['goaxles']+dateData[date_selected]['precredit']['4w']['goaxles']+dateData[date_selected]['premium']['2w']['goaxles']+dateData[date_selected]['premium']['4w']['goaxles']+dateData[date_selected]['leads']['2w']['goaxles']+dateData[date_selected]['leads']['4w']['goaxles']+dateData[date_selected]['ebm']['premium']['goaxles']+dateData[date_selected]['ebm']['precredit']['goaxles']+dateData[date_selected]['ebm']['leads']['goaxles']);
        }
        $("#mymodal").modal('show');
      });

  }
</script>
</body>
</html>

<?php
include("sidebar.php");
error_reporting(0);
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id'])) || ($crm_feedback!="1" && $feedback_admin == "1")) {
  header('location:index.php');
  die();
}

$crm_log_id = $_SESSION['crm_log_id'];
$setDate = 0;
if(isset($_GET['st'])&&isset($_GET['ed']))
{
	$setDate = 1;
	$st = base64_decode($_GET['st']);
	$ed = base64_decode($_GET['ed']);
}
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<title>GoBumpr Bridge</title>


<style>
  html{min-height:100%;}*{box-sizing:border-box;}body{color:black;background:rgb(255, 255, 255) !important;margin:0px;min-height:inherit;}[data-sidebar-overlay]{display:none;position:fixed;top:0px;bottom:0px;left:0px;opacity:0;width:100%;min-height:inherit;}.overlay{background-color:rgb(222, 214, 196);z-index:999990 !important;}aside{position:relative;height:100%;width:200px;top:0px;left:0px;background-color:rgb(236, 239, 241);box-shadow:rgba(0, 0, 0, 0.8) 0px 6px 6px 0px;z-index:999999 !important;}[data-sidebar]{display:none;position:absolute;height:100%;z-index:100;}.padding{padding:2em;}.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6{font-family:inherit;font-weight:500;line-height:1.1;color:inherit;}.h4, .h5, .h6, h4, h5, h6{margin-top:10px;margin-bottom:10px;}.h4, h4{font-size:18px;}img{border:0px;vertical-align:middle;}a{text-decoration:none;color:black;}aside a{color:rgb(0, 0, 0);font-size:16px;text-decoration:none;}.fa{display:inline-block;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;line-height:1;font-family:FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;}nav, ol{font-size:18px;margin-top:-4px;background:rgb(0, 150, 136) !important;}.container-fluid{padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto;}.breadcrumb > li{display:inline-block;}ol, ul{margin-top:0px;margin-bottom:10px;}ol ol, ol ul, ul ol, ul ul{margin-bottom:0px;}.nav{padding-left:0px;margin-bottom:0px;list-style:none;}.navbar-nav{margin:0px;float:left;}.navbar-right{margin-right:-15px;float:right !important;}.nav > li{position:relative;display:block;}.navbar-nav > li{float:left;}.dropdown{position:relative;display:inline-block;}.form-group{margin-bottom:15px;}button, input, optgroup, select, textarea{margin:0px;font-style:inherit;font-variant:inherit;font-weight:inherit;font-stretch:inherit;font-size:inherit;line-height:inherit;font-family:inherit;color:inherit;}button, select{text-transform:none;}button, input, select, textarea{font-family:inherit;font-size:inherit;line-height:inherit;}.form-control{display:block;width:100%;height:34px;padding:6px 12px;font-size:14px;line-height:1.42857;color:rgb(85, 85, 85);background-color:rgb(255, 255, 255);background-image:none;border:1px solid rgb(204, 204, 204);border-radius:4px;box-shadow:rgba(0, 0, 0, 0.075) 0px 1px 1px inset;transition:border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;}.navbar-fixed-bottom, .navbar-fixed-top{position:fixed;right:0px;left:0px;z-index:1030;border-radius:0px;}.navbar-fixed-top{top:0px;border-width:0px 0px 1px;}.btn-group, .btn-group-vertical{position:relative;display:inline-block;vertical-align:middle;}label{cursor:pointer;}b, strong{font-weight:700;}input{line-height:normal;}input[type="checkbox"], input[type="radio"]{box-sizing:border-box;padding:0px;margin:4px 0px 0px;line-height:normal;}[data-toggle="buttons"] > .btn input[type="checkbox"], [data-toggle="buttons"] > .btn input[type="radio"], [data-toggle="buttons"] > .btn-group > .btn input[type="checkbox"], [data-toggle="buttons"] > .btn-group > .btn input[type="radio"]{position:absolute;clip:rect(0px 0px 0px 0px);pointer-events:none;}p{margin:0px 0px 10px;}.col-sm-offset-1{margin-left:8.33333%;}.col-lg-offset-1{margin-left:8.33333%;}.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9{position:relative;min-height:1px;padding-right:15px;padding-left:15px;}.col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9{float:left;}.col-xs-2{width:16.6667%;}.col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9{float:left;}.col-sm-2{width:16.6667%;}.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9{float:left;}.col-lg-1{width:8.33333%;}.floating-box1{display:inline-block;margin-top:10px;float:left;clear:both;}.col-sm-3{width:25%;}.col-lg-3{width:25%;}.glyphicon{position:relative;top:1px;display:inline-block;font-family:"Glyphicons Halflings";font-style:normal;font-weight:400;line-height:1;-webkit-font-smoothing:antialiased;}.caret{display:inline-block;width:0px;height:0px;margin-left:2px;vertical-align:middle;border-top:4px dashed;border-right:4px solid transparent;border-left:4px solid transparent;}.col-sm-1{width:8.33333%;}.col-sm-4{width:33.3333%;}button{overflow:visible;}button, html input[type="button"], input[type="reset"], input[type="submit"]{-webkit-appearance:button;cursor:pointer;}.btn{display:inline-block;padding:6px 12px;margin-bottom:0px;font-size:14px;font-weight:400;line-height:1.42857;text-align:center;white-space:nowrap;vertical-align:middle;touch-action:manipulation;cursor:pointer;user-select:none;background-image:none;border:1px solid transparent;border-radius:4px;}.pull-right{float:right;}.form-control:focus{border-color:rgb(102, 175, 233);outline:0px;box-shadow:rgba(0, 0, 0, 0.075) 0px 1px 1px inset, rgba(102, 175, 233, 0.6) 0px 0px 8px;}.counter{padding:8px;color:rgb(204, 204, 204);}.fade{opacity:0;transition:opacity 0.15s linear;}.modal{position:fixed;top:0px;right:0px;bottom:0px;left:0px;z-index:1050;display:none;overflow:hidden;outline:0px;}.modal.fade .modal-dialog{transition:transform 0.3s ease-out;transform:translate(0px, -25%);}.modal-dialog{position:relative;width:600px;margin:30px auto;}.modal-content{position:relative;background-color:rgb(255, 255, 255);-webkit-background-clip:padding-box;background-clip:padding-box;border:1px solid rgba(0, 0, 0, 0.2);border-radius:6px;outline:0px;box-shadow:rgba(0, 0, 0, 0.5) 0px 5px 15px;}.modal-header{padding:15px;border-bottom:1px solid rgb(229, 229, 229);}.close{float:right;font-size:21px;font-weight:700;line-height:1;color:rgb(0, 0, 0);text-shadow:rgb(255, 255, 255) 0px 1px 0px;opacity:0.2;}button.close{-webkit-appearance:none;padding:0px;cursor:pointer;background:0px 0px;border:0px;}.modal-header .close{margin-top:-2px;}.h1, .h2, .h3, h1, h2, h3{margin-top:20px;margin-bottom:10px;}.h3, h3{font-size:24px;}.modal-title{margin:0px;line-height:1.42857;}.modal-body{position:relative;padding:15px;}.row{margin-right:-15px;margin-left:-15px;}.col-sm-offset-3{margin-left:25%;}.col-lg-6{width:50%;}.col-lg-offset-3{margin-left:25%;}.uil-default-css{position:relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(1){animation:uil-default-anim 1s linear -0.5s infinite;}.uil-default-css > div:nth-of-type(2){animation:uil-default-anim 1s linear -0.416667s infinite;}.uil-default-css > div:nth-of-type(3){animation:uil-default-anim 1s linear -0.333333s infinite;}.uil-default-css > div:nth-of-type(4){animation:uil-default-anim 1s linear -0.25s infinite;}.uil-default-css > div:nth-of-type(5){animation:uil-default-anim 1s linear -0.166667s infinite;}.uil-default-css > div:nth-of-type(6){animation:uil-default-anim 1s linear -0.0833333s infinite;}.uil-default-css > div:nth-of-type(7){animation:uil-default-anim 1s linear 0s infinite;}.uil-default-css > div:nth-of-type(8){animation:uil-default-anim 1s linear 0.0833333s infinite;}.uil-default-css > div:nth-of-type(9){animation:uil-default-anim 1s linear 0.166667s infinite;}.uil-default-css > div:nth-of-type(10){animation:uil-default-anim 1s linear 0.25s infinite;}.uil-default-css > div:nth-of-type(11){animation:uil-default-anim 1s linear 0.333333s infinite;}.uil-default-css > div:nth-of-type(12){animation:uil-default-anim 1s linear 0.416667s infinite;}table{border-spacing:0px;border-collapse:collapse;background-color:transparent;}.table{width:100%;max-width:100%;margin-bottom:20px;}.table-bordered{border:1px solid rgb(221, 221, 221);}td, th{padding:0px;}th{text-align:left;}.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th{padding:8px;line-height:1.42857;vertical-align:top;border-top:1px solid rgb(221, 221, 221);}.table > thead > tr > th{vertical-align:bottom;border-bottom:2px solid rgb(221, 221, 221);}.table > caption + thead > tr:first-child > td, .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table > thead:first-child > tr:first-child > th{border-top:0px;}.table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th{border:1px solid rgb(221, 221, 221);}.table-bordered > thead > tr > td, .table-bordered > thead > tr > th{border-bottom-width:2px;}#tbody{font-size:15px !important;border:1.5px solid rgb(196, 184, 184) !important;}#tbody, tbody tr{animation:opacity 5s ease-in-out;}.btn-info{color:rgb(255, 255, 255);background-color:rgb(91, 192, 222);border-color:rgb(70, 184, 218);}.col-xs-8{width:66.6667%;}.col-xs-offset-2{margin-left:16.6667%;}.modal-footer{padding:15px;text-align:right;border-top:1px solid rgb(229, 229, 229);}.dropdown-menu{position:absolute;top:100%;left:0px;z-index:1000;display:none;float:left;min-width:160px;padding:5px 0px;margin:2px 0px 0px;font-size:14px;text-align:left;list-style:none;background-color:rgb(255, 255, 255);-webkit-background-clip:padding-box;background-clip:padding-box;border:1px solid rgba(0, 0, 0, 0.15);border-radius:4px;box-shadow:rgba(0, 0, 0, 0.176) 0px 6px 12px;}button[disabled], html input[disabled]{cursor:default;}.btn.disabled, .btn[disabled], fieldset[disabled] .btn{cursor:not-allowed;box-shadow:none;opacity:0.65;}.btn-success{color:rgb(255, 255, 255);background-color:rgb(92, 184, 92);border-color:rgb(76, 174, 76);}.btn-group-sm > .btn, .btn-sm{padding:5px 10px;font-size:12px;line-height:1.5;border-radius:3px;}.btn-default{color:rgb(51, 51, 51);background-color:rgb(255, 255, 255);border-color:rgb(204, 204, 204);}
  .body{
      overflow-x:scroll!important;
  }
  #datepick > span:hover{cursor: pointer;}
  .top-box {
      display: inline-block;
      z-index: 1;
      height:30px;
      width:90px;
      background-color: #B6A3BF;
      -webkit-display:button;
      box-shadow: 0px 8px 8px 0px rgba(0,0,0,0.2);
  }

 .floating-box {
    display: inline-block;
    margin: 10px;
    padding: 12px;
    width:183px;
    height:75px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
 }
  .floating-box1 {
      display: inline-block;
      margin-top:10px;
      float:left;
      clear:both;
  }

  input[] {
      visibility:hidden;
  }
  label {
      cursor: pointer;
  }
  input:checked + label {
      background: red;
  }
  /* table */
  #tbody{
    font-size:15px !important;
    border:1.5px solid #c4b8b8 !important;
    
  }
  thead:hover{
    cursor:pointer;
  }

  .results tr[visible='false'],
  .no-result{
    display:none;
  }

  .results tr[visible='true']{
    display:table-row;
  }

  .counter{
    padding:8px;
    color:#ccc;
  }
  #tbody, tbody tr {
      -webkit-animation: opacity 5s ease-in-out;
      animation: opacity 5s ease-in-out;
  }
  p[data-title]:hover:after {
      content: attr(data-title);
      padding: 4px 8px;
      color: #333;
      position: absolute;
      left: 0;
      top: 100%;
      white-space: nowrap;
      z-index: 20px;
      -moz-border-radius: 5px;
      -webkit-border-radius: 5px;
      border-radius: 5px;
      -moz-box-shadow: 0px 0px 4px red;
      -webkit-box-shadow: 0px 0px 4px red;
      box-shadow: 0px 0px 4px #222;
      background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
      background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #eeeeee),color-stop(1, #cccccc));
      background-image: -webkit-linear-gradient(top, #eeeeee, #cccccc);
      background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
      background-image: -ms-linear-gradient(top, #eeeeee, #cccccc);
      background-image: -o-linear-gradient(top, #eeeeee, #cccccc);
    }
</style>

</head>
<body>
<?php include_once("header.php"); ?>

<div class="overlay" data-sidebar-overlay></div>


<div class="padding">
</div>


<div class="navbar-fixed-top" style=" margin-top:92px;border-top:52px;padding-top:5px;background-color:#fff;z-index:1037;">
 <!-- service type filter -->
<div class=" col-sm-2 col-lg-2" style="float:left;margin-left:25%;" >
<select id="service" name="service" class="form-control">
   <option value="all" selected>All Services</option>
   <?php
   $sql_service = "SELECT DISTINCT service_type FROM go_axle_service_price_tbl ORDER BY service_type";
   $res_service = mysqli_query($conn,$sql_service);
   while($row_service = mysqli_fetch_object($res_service)){
     $c_service = $row_service->service_type;
     ?>
       <option value="<?php echo $c_service; ?>" > <?php echo $c_service; ?> </option>
       <?php
   }
   ?>
   </select>
</div>
<!-- Shop filter -->
<div class=" col-sm-2 col-lg-2" style="float:left;" >
<select id="shop_list" name="shop_list" class="form-control">
</select>
</div>
<!-- vehicle filter -->
<div class=" col-sm-2 col-lg-2" style="float:left;">
    <select id="vehicle" name="vehicle" class="form-control">
      <option value="all" selected>All Vehicles</option>
      <option value="2w">2 Wheeler</option>
      <option value="4w">4 Wheeler</option>
    </select>
</div>
</div>

<div class="navbar-fixed-top" style=" margin-top:130px; padding-bottom:8px; background-color:#fff;">


<!-- date range picker -->
<div id="reportrange" class=" col-sm-3 col-lg-3" style="cursor: pointer; margin-left:24%;">
   <div class=" floating-box1">
     <div id="range" class="form-control" style="min-width:312px;">
     <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
    <span id="dateval"></span> <b class="caret"></b>
  </div>
    </div>
</div>

  <!-- search bar -->
  <div class=" col-sm-4 ">

  <div  class="floating-box1" >
    <form id="myForm" method="post" action="check_user.php" onsubmit="return ValidationEvent(event)">
    <button class="form-group pull-right btn btn-small" type="submit" id="search_submit" name="search_submit" style=" width:50px;height:34px; margin-left:6px;background-color:#B2DFDB;font-size:15px;"><i class="fa fa-search" aria-hidden="true"></i></button>

  <div class="form-group pull-right">
        <input type="text" class="search form-control" id="su" name="searchuser" placeholder="Search">
        <input type="hidden" id="search_type" name="search_type" value="">
  </div>
  
  <span class="counter pull-right" style="margin-left:28px;"></span>
  </div>
  
</form>
  </div>
  </div>



 <div id="adding_user" style="display:inline-block; float:left; align-items:center;" >

<!-- Modal -->
<div class="modal fade" id="myModal_user" role="dialog" >
<div class="modal-dialog" style="width:400px;height:150px;margin-top:150px;">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Add User</h3>
</div>
<div class="modal-body" style="height:220px;">
<form id="check_user" class="form" method="post" action="check_user.php">
<div align="center">
<div class="row"><h4>Please enter Customer's Mobile number.</h4></div>
<div class="row"></br><div class="col-lg-6 col-sm-3 col-lg-offset-3 col-sm-offset-3" ><input type="text" maxlength="10" minlength="9" onchange="try{setCustomValidity('')}catch(e){}" pattern="^[0-9]{6}|[0-9]{8}|[0-9]{10}$" title="Mobile number must have 10 digits!" id="mobile" name="mobile" placeholder="Eg.9876543210" class="form-control" required></div></div>
<div class="row"> </br><input class="btn btn-md" style="background-color:#B6ED6F;font-size:16px;" type="submit" id="submit" name="submit" value="Verify"/></div>
</div>
</form>
</div>

</div>
</div></div>
</div>

<!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>

<div align="center" id = "table" style="margin-top:140px;margin-left:10px;margin-right:10px; display: none;">
<table id="example" class="table table-striped table-bordered tablesorter table-hover results" style="text-align:center;">
  <thead style="background-color: #D3D3D3;">
  <tr>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">No.</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">BookingId</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">GoAxle Date</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Lead Status</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">View</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">CustomerName</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Mobile</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">ShopName</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Vehicle</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">ServiceType</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">ServiceStatus</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Reason</th>
  <th style="text-align:center;vertical-align: middle;" colspan="3">Comments</th>
  </thead>
<tbody id="tbody">
</tbody>
</table>
</div>


<noscript id="async-styles">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="css/style.css" />

</noscript>
<script> // The CSS mentioned in "async-styles" will be loaded and styles will be applied.
    var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("async-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
        webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function() {
        window.setTimeout(loadDeferredStyles, 0);
    });
    else window.addEventListener('load', loadDeferredStyles);
</script>

<script async src="js/jquery-3.2.1.min.js" onLoad="loadOtherScripts();"></script>

<script>
    function loadScript(src) {
        return new Promise(function (resolve, reject) {
            var s;
            s = document.createElement('script');
            s.src = src;
            s.onload = resolve;
            s.onerror = reject;
            document.head.appendChild(s);
        });
    }

    function loadOtherScripts() {
      loadScript('js/moment.min.js').then(function() {
      Promise.all([ loadScript('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'),loadScript('js/daterangepicker.js'),loadScript('js/sidebar.js'),loadScript('https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js')]).then(function () {
   // console.log('scripts are loaded');
      dateRangePicker();
      momentInPicker();

     
      intialFunction();
     
      tablesort();
      searchbar();
      //modal();
      }).catch(function (error) {
    //  console.log('some error!' + error)
      })
      }).catch(function (error) {
    //  console.log('Moment call error!' + error)
      })
      }
</script>

 <!-- table sorter -->
<script>
function tablesort(){
  $(document).ready(function()
    {
        $("#example").tablesorter( {sortList: [11,0]} );
    }
  );
}
</script>
<!-- search bar  -->
<script>
function searchbar(){
  $(document).ready(function() {
    $(".search").keyup(function() {
      var searchTerm = $(".search").val();
      var listItem = $('.results tbody').children('tr');
      var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

    $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
          return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
      }
    });

    $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
      $(this).attr('visible','false');
    });

    $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
      $(this).attr('visible','true');
    });

    var jobCount = $('.results tbody tr[visible="true"]').length;
      $('.counter').text(jobCount + ' item');

    if(jobCount == '0') {$('.no-result').show();}
      else {$('.no-result').hide();
    }
        });
  });
}
</script>

<!-- date range picker -->
<script>
function dateRangePicker(){
  $('input[name="daterange"]').daterangepicker({
    locale: {
          format: 'DD-MM-YYYY'
      }
  });
}
</script>
<script type="text/javascript">
function momentInPicker(){
  $(function() {
 var s_date = '<?php echo $st; ?>';
  var e_date = '<?php echo $ed; ?>';
  if(s_date == '' || e_date == '')
  {
	e_date = moment();
	s_date = moment().subtract(1, 'days');
  }
  var start = moment(s_date);
   var end = moment(e_date);
	console.log(moment());
    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

  });
}
</script>
<!-- view table -->
<script>
function viewtable(){
  var setdate = '<?php echo $setDate; ?>';
  if(setdate=='1')
  {
	var startDate = '<?php echo $st; ?>';
	var endDate = '<?php echo $ed; ?>';
	setdate = 0;
  }
  else
  {
	var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
	var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
  }
  var vehicle = $('#vehicle').val();
  var service = $('#service').val();
  var city = $('#city').val();
  var shop_id = $('#shop_list').val();
  console.log(shop_id);
            //Make AJAX request, using the selected value as the POST

        $.ajax({
              url : "ajax/fdbkcancelled_view.php",  // create a new php page to handle ajax request
              type : "GET", json : false,
              data : {"startdate": startDate , "enddate": endDate,"vehicle": vehicle ,"service": service,"city":city,'shop_id':shop_id},
              success : function(data) {
              // console.log(data);
          $("#loading").hide();
          $("#table").show();
          $("#tbody").empty();
          if(data === "no"){
            $("#tbody").html("<h2 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h2>");
          }
          else{
            //console.log(data);
            if($.trim(data) && $.trim(data)!== "no" && $.trim(data)!==""){
            function displayInfo(item,index){
                    $('#tbody').append(item['tr']);
                    //console.log(item);
                  }
                  $.map($.parseJSON(data), displayInfo);
          //  $('#tbody').html(data);   
            $("#example").trigger("update"); 
          }}
		  modal();
	
          counterval();         
              },
            error: function(xhr, ajaxOptions, thrownError) {
            //  alert(xhr.status + " "+ thrownError);
           }
         });
}
</script>
<script>
function counterval(){
  var jobCount = $("#tbody tr").length;
   $('.counter').text(jobCount + ' item');
}
</script>

<script>
function get_shops(){
  var city = $("#city").val();
  $.ajax({
	    url : "ajax/get_shops_fb_panel.php",  // create a new php page to handle ajax request
	    type : "POST",
	    data : {"city": city},
	    success : function(data) {
		$("#shop_list").empty();
      	$('#shop_list').append(data);
        $('#show').hide();
		$("#loading").show();
		viewtable();
		},
	    error: function(xhr, ajaxOptions, thrownError) {
	          //  alert(xhr.status + " "+ thrownError);
	    }
	});
}

</script>
<!-- automatic refresh -->
<script>
function intialFunction(){
  $(document).ready(function(){
		$('#city').show();
    $("#table").hide();
    $("#loading").show();
    get_shops();

    $('#dateval').on("DOMSubtreeModified", function (){
      $("#table").hide();
      $("#loading").show();
      viewtable();
    });

    /*$("#bsource").change(function (){
      $("#table").hide();
      $("#loading").show();
      viewtable();
    });*/

    $("#service").change(function (){
      $("#table").hide();
      $("#loading").show();
      viewtable();
    });

    $("#vehicle").change(function (){
      $("#table").hide();
      $("#loading").show();
      viewtable();
    });

    $('#city').change(function (){
      $("#table").hide();
      $("#loading").show();
      viewtable();
	  });
    
	  $("#shop_list").change(function (){
		$("#table").hide();
		$("#loading").show();
		viewtable();
	});
	  
	
	
	});
  window.setInterval(function(){
    viewtable();
  }, 240000);

  //check online status
  function when_online() {
    document.title = "GoBumpr Bridge";
    $("#table").hide();
    $("#loading").show();
    viewtable();
  }
  function when_offline() {
    document.title = "You're Offline!";
    alert("You're Offline! Please check your internet connection");
  }

  // Update the online status icon based on connectivity
  window.addEventListener('online',  when_online);
  window.addEventListener('offline', when_offline);
}

</script>

<!-- search/add user -->
<script type="text/javascript">
function ValidationEvent(event) {
  event.preventDefault();
  var mobile = document.getElementById("su").value;
  var phoneno = /^[1-9][0-9]*$/;  
 // var emailid = /\S+@\S+\.\S+/;
  var emailid =/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
  var name =/^[a-zA-Z\s]+$/;

  //console.log(phoneno.test(mobile))
  if(phoneno.test(mobile)){  
    if((mobile.length >= 7 && mobile.length <10) || mobile.length > 10 || mobile.length<=4){
      document.getElementById("search_type").value="";
      alert("Please enter a valid BookingId / Mobile number !");
    }
    else if(mobile.length<=7){
    document.getElementById("search_type").value="booking";
     document.getElementById("myForm").submit();
    }
    else{
      document.getElementById("search_type").value="mobile";
      document.getElementById("myForm").submit();  
    }
  }
  else if(emailid.test(mobile)){
    document.getElementById("search_type").value="email";
    document.getElementById("myForm").submit();
  }
  else if(name.test(mobile)){
    document.getElementById("search_type").value="username";
    document.getElementById("myForm").submit();
  }
  else{  
    document.getElementById("search_type").value="";
    alert("Please enter a valid Booking Id / Mobile Number / Email / User Name !");
  }  
}
</script>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Mechanic Select</h4>
      </div>
      <div id="loading1" style="display:none; margin-top:80px;" align="center">
        <div class='uil-default-css' style='transform:scale(0.58);'>
          <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
          </div>
          <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
          </div>
          <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
          </div>
          <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
          </div>
          <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
          </div>
          <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
          </div>
          <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
          </div>
          <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
          </div>
          <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
          </div>
          <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
          </div>
          <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
          </div>
          <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
          </div>
        </div>
      </div>
      <div class="col-xs-8 col-xs-offset-2 form-group" id="insert">
      </div>
      <div class="modal-footer">
      </div>
    </div>
      
  </div>
</div>
<script type="text/javascript">
  function loadModal(button){
  var locality=button.data('locality');
  var vehicletype=button.data('vehicletype');
  var servicetype=button.data('servicetype');
  var bookingid=button.data('id');
  var brand=button.data('brand');

  $.ajax({
          url : "ajax/axpress_goaxle.php",  // create a new php page to handle ajax request
          type : "POST", json : false,
          data : {"brand":brand,"selecttype":vehicletype, "selectloc": locality ,"servicetype":servicetype,"bookingId":bookingid},
          success : function(data) {
            //console.log(data);
          //var modal = $(this)
        
        $("#loading1").hide();
        $("#insert").show();
              $('#insert').empty().append(data);
          },
        error: function (xhr, ajaxOptions, thrownError) {
        alert(xhr.status + " "+ thrownError);
      }
    });

  }
  
  function modal(){
	  //console.log('called');
  var bookingid;
  $("#myModal").on('hide.bs.modal', function(event){
      $("#table").addClass('body');
  });
  $("#myModal").on('show.bs.modal', function(event){
  $("#loading1").show();
  $("#insert").hide();
          
  var button=$(event.relatedTarget);
  loadModal(button);
  });
}

  function sendEmail() {
      $.ajax({
          async: true,
          type: 'GET',
          url: '/go_axle.php',
          data: {
              bi: bookingid,
              t:'b'
          },
          success: function(data_res) {},
      });
  }
</script>

</body>
</html>

<?php
//include("config.php");
include("sidebar.php");
$conn = db_connect1();
//session_start();
// login or not
if((empty($_SESSION['crm_log_id'])) || ($crm_se!="1" && $flag!="1")) {
	if($_SESSION['crm_log_id']!='crm087')
	{
		header('location:logout.php');
		die();
	}
}
$crm_log_id = $_SESSION['crm_log_id'];
date_default_timezone_set('Asia/Kolkata');
?>

<!DOCTYPE html>
<html>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
  <title>GoBumpr Bridge</title>

 <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />  -->
 <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<!-- validate -->
<script sr="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>

   <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
   <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

   <!-- bread crumbs -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

   <!-- Include Date Range Picker -->
 <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
 <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>

  <style>
	#datepick > span:hover{cursor: pointer;}

	.floating-box {
	 display: inline-block;
	 margin: 22px;
 padding: 22px;
 width:203px;
 height:105px;
 box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  font-size: 17px;
}
.floating-box1 {
    display: inline-block;
	margin-top:10px;
	float:left;
	clear:both;
 }
#tbody{
   font-size:15px !important;
}

/* table */
thead:hover{
	cursor:pointer;
}

.results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px;
  color:#ccc;
}
</style>

</head>
<body>
<?php include_once("header.php"); ?>
<script>
	$(document).ready(function(){
		$('#city').show();
	})
</script>
<div class="overlay" data-sidebar-overlay></div>

<div class="padding">
	</div>

<!--<div class="navbar-fixed-top" style=" margin-top:52px;border-top:52px;padding-top:5px;background-color:#fff;z-index:1037;">
<p class="col-lg-offset-3 col-sm-offset-2" style="background-color:#ffa800;padding:7px;width:30px;border-radius:18px;float:left;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p><p style="width:150px;padding:6px;float:left;">New Lead.</p>
<p style="background-color:#69ED85;padding:7px;width:30px;border-radius:18px;float:left;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p><p style="width:150px;padding:6px;float:left;">Sent to axle.</p>
<p style="background-color:#6FA3DE;padding:7px;width:30px;border-radius:18px;float:left;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p><p style="width:150px;padding:6px;float:left;">Lead Accepted.</p>
<p style="background-color:#D25F34;padding:7px;width:30px;border-radius:18px;float:left;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p><p style="width:150px;padding:6px;float:left;">Lead Rejected.</p>
</div>-->
  <div class="navbar-fixed-top" style=" margin-top:58px; padding-top: 30px; padding-bottom:10px; background-color:#fff;">

    <!-- date range picker -->
    <div class="col-lg-3 col-sm-3 col-xs-1 col-lg-offset-1">
    <div id="reportrange" class=" col-sm-3 " style="cursor: pointer;margin-right:30px; margin-left: 40px;">
       <div class=" floating-box1">
    		 <div id="range" class="form-control" style="min-width:315px;margin-left:-125px;">
        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
        <span id="dateval"></span> <b class="caret"></b>
    	</div>
        </div>
    </div>
  </div>

  <!-- search bar -->
  <div class=" col-sm-8 ">

  <div  class="floating-box1" >
    <form method="post" action="check_user.php" onsubmit="return ValidationEvent()">
    <button class="form-group pull-right btn btn-small" type="submit" id="search_submit" name="search_submit" style=" width:50px;height:34px; margin-left:12px;background-color:#B2DFDB;font-size:15px;"><i class="fa fa-search" aria-hidden="true"></i></button>
  <div class="form-group pull-right">
      	<input type="text" class="search form-control" id="su" name="searchuser" placeholder="Search">
        <input type="hidden" id="search_type" name="search_type" value="">
  </div>  
  <span class="counter pull-right"></span>
  </div>
  </form>
  <div class="floating-box" id="toggler" align="center" style="cursor:pointer;margin-top: 9px;margin-right: 45px;padding-top: 6px;border-radius:5px;/* padding-right: 8px; */height:35px;width: 15%;float: right;background-color: #00031A5E;">
			<p id="toggle-text">Advanced</p>
		</div>

  </div>
  </div>



  <!-- loading -->
  <div id="loading" style="display:none; margin-top:140px;" align="center">
    <div class='uil-default-css' style='transform:scale(0.58);'>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
    <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
    </div>
  </div>


  </div>
<!-- table -->
<div align="center" id = "table" style=" margin-top:110px;margin-left:10px;margin-right:10px; display: none;">
<table id="example" class="table table-striped table-bordered tablesorter table-hover results">
  <thead style="background-color: #D3D3D3;">
  <!--<th>No</th>-->
  <th style="border-color: #888;text-align: center;">Shop Name</th>
  <th style="border-color: #888;text-align: center;">Avg. Action Time (mins)</th>
  <th style="border-color: #888;text-align: center;">Avg. Contact Time (mins)</th>
  <th style="border-color: #888;text-align: center;">Total Vehicles</th>
  <th style="border-color: #888;text-align: center;">Accepted Vehicles</th>
  <th style="border-color: #888;text-align: center;">Idle Vehicles</th>
  <th style="border-color: #888;text-align: center;">Checked-In Vehicles</th>
  <th style="border-color: #888;text-align: center;">Inspected Vehicles</th>
  <th style="border-color: #888;text-align: center;">Completed Vehicles</th>
  </thead>
  <tbody id="tbody">
  </tbody>
  </table>
  </div>
<!-- table -->
<div align="center" id = "table1" style="margin-top:110px;margin-left:10px;margin-right:10px; display: none;">
<table id="example1" class="table table-striped table-bordered tablesorter table-hover results">
  <thead style="background-color: #D3D3D3;">
  <!--<th>No</th>-->
  <tr>
  <th rowspan="2" style="text-align: center;vertical-align: inherit;border-color: #888;">Shop Name</th>
  <th colspan="4" style="text-align: center;vertical-align: inherit;border-color: #888;"> Action Time (mins)</th>
  <th colspan="4" style="text-align: center;vertical-align: inherit;border-color: #888;"> Contact Time (mins)</th>
  <th colspan="7" style="text-align: center;vertical-align: inherit;border-color: #888;">Vehicles</th>
  </tr>
  <tr>
  <th style="border-color: #888;text-align: center;">Avg.</th>
  <th style="border-color: #888;text-align: center;">Min.</th>
  <th style="border-color: #888;text-align: center;">Max.</th>
  <th style="border-color: #888;text-align: center;">Median</th>
  <th style="border-color: #888;text-align: center;">Avg.</th>
  <th style="border-color: #888;text-align: center;">Min.</th>
  <th style="border-color: #888;text-align: center;">Max.</th>
  <th style="border-color: #888;text-align: center;">Median</th>
  <th style="border-color: #888;text-align: center;">Total</th>
  <th style="border-color: #888;text-align: center;">Accepted</th>
  <th style="border-color: #888;text-align: center;">Rejected</th>
  <th style="border-color: #888;text-align: center;">Idle</th>
  <th style="border-color: #888;text-align: center;">Checked-In</th>
  <th style="border-color: #888;text-align: center;">Inspected</th>
  <th style="border-color: #888;text-align: center;">Completed</th>
  </tr>
  </thead>
  <tbody id="tbody1">
  </tbody>
  </table>
  </div>

  <!-- table sorter -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js"></script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<!-- search bar  -->
<script>
$(document).ready(function() {
  $(".search").keyup(function () {
    var searchTerm = $(".search").val();
    var listItem = $('.results tbody').children('tr');
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

  $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });

  $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','false');
  });

  $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','true');
  });

  var jobCount = $('.results tbody tr[visible="true"]').length;
    $('.counter').text(jobCount + ' item');

  if(jobCount == '0') {$('.no-result').show();}
    else {$('.no-result').hide();
	}
		  });
});
</script>
<!-- date range picker -->
<script>
$('input[name="daterange"]').daterangepicker({
 locale: {
      format: 'DD-MM-YYYY'
    }
});
</script>
<script type="text/javascript">
$(function() {

  var start = moment().subtract(1, 'days');
	 // var start = moment();
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});
</script>

<!-- default view -->
<script>
function viewtable(){
	var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
	var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
  var city = $('#city').val();
	          //Make AJAX request, using the selected value as the POST

			  $.ajax({
	            url : "ajax/partner_usage_view.php",  // create a new php page to handle ajax request
	            type : "POST",
	            data : {"startdate": startDate , "enddate": endDate,"city":city },
	            success : function(data) {
					    //alert(data);
					//console.log(data);
					$("#loading").hide();
          $("#table").show();
          if(data === "no"){
            $("#tbody").html("<h2 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h2>");
          }
          else{
      			$('#tbody').html(data);		
            $("#example").trigger("update"); 
          }	  
          counterval();         
          },
	          error: function(xhr, ajaxOptions, thrownError) {
	           // alert(xhr.status + " "+ thrownError);
	         }
				 });
			$.ajax({
	            url : "ajax/advanced_usage_view.php",  // create a new php page to handle ajax request
	            type : "POST",
	            data : {"startdate": startDate , "enddate": endDate,"city":city },
	            success : function(data) {
					    //alert(data);
					//console.log(data);
					$("#loading").hide();
					//$("#table1").show();
          if(data === "no"){
            $("#tbody1").html("<h2 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h2>");
          }
          else{
      			$('#tbody1').html(data);		
            $("#example1").trigger("update"); 
          }	  
          counterval();         
          },
	          error: function(xhr, ajaxOptions, thrownError) {
	           // alert(xhr.status + " "+ thrownError);
	         }
				 });
}
</script>
<script>
function counterval(){
	var jobCount = $("#tbody1 tr").length;;
	 $('.counter').text(jobCount + ' item');
}
</script>
 <!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example").tablesorter( {sortList: [[0,0], [1,0]]} );
        $("#example1").tablesorter( {sortList: [[0,0], [1,0]]} );
    }
);
</script>

<script>
$(document).ready( function (){
	$("#table").hide();
	$("#table1").hide();
	$("#loading").show();
	viewtable();
});
</script>
<!-- on change of date -->
<script>
$(document).ready( function (){
	$('#dateval').on("DOMSubtreeModified", function (){
		//console.log("date changed");
	   $("#table").hide();
	   $("#table1").hide();
	   $("#loading").show();
	   viewtable();
	 });
});
</script>

<!-- Modal -->
<div class="modal fade" id="myModal_user" role="dialog" >
<div class="modal-dialog" style="width:400px;height:150px;margin-top:150px;">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Add User</h3>
</div>
<div class="modal-body" style="height:220px;">
<form id="check_user" class="form" method="post" action="check_user.php">
<div align="center">
<div class="row"><h4>Please enter Customer's Mobile number.</h4></div>
<div class="row"></br><div class="col-lg-6 col-sm-3 col-lg-offset-3 col-sm-offset-3" ><input type="text" maxlength="10" minlength="9" onchange="try{setCustomValidity('')}catch(e){}" pattern="^[0-9]{6}|[0-9]{8}|[0-9]{10}$" title="Mobile number must have 10 digits!" id="mobile" name="mobile" placeholder="Eg.9876543210" class="form-control" required></div></div>
<div class="row"> </br><input class="btn btn-md" style="background-color:#B6ED6F;font-size:16px;" type="submit" id="submit" name="submit" value="Verify"/></div>
</div>
</form>
</div>

</div>
</div></div>
</div>
<!-- on change of city -->
<script>
$(document).ready( function (){
	$('#city').change(function (){
	   $("#table").hide();
	   $("#table1").hide();
	   $("#loading").show();
	   viewtable();
	 });
	 $("#toggler").on("click",function(){
		$("#table").slideToggle('fast','linear');
		if($("#toggle-text").text()=="Advanced")
		$("#toggle-text").text("Basic");
		else
		$("#toggle-text").text("Advanced");
		$("#table1").slideToggle('fast','linear');
	 });
});
</script>

<!-- search/add user -->
<script type="text/javascript">
function ValidationEvent() {
  var mobile = document.getElementById("su").value;
  var phoneno = /^[1-9][0-9]*$/;  
 // var emailid = /\S+@\S+\.\S+/;
  var emailid = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
  var name = /^[a-zA-Z\s]+$/;

  if(mobile.match(phoneno)){  
    if((mobile.length >= 7 && mobile.length <10) || mobile.length > 10 || mobile.length<=4){
      document.getElementById("search_type").value="";
      alert("Please enter a valid BookingId / Mobile number !");
      //alert(document.getElementById("search_type").value);
      return false;
    }
    else if(mobile.length<=7){
      document.getElementById("search_type").value="booking";
      //alert("booking id !");
      //alert(document.getElementById("search_type").value);
      return true;
    }
    else{
       document.getElementById("search_type").value="mobile";
      //alert("mobile number !");
      //alert(document.getElementById("search_type").value);
      return true;   
    }
  }
  else if(mobile.match(emailid)){
    document.getElementById("search_type").value="email";
    //alert("Email !");
    //alert(document.getElementById("search_type").value);
    return true;
  }
  else if(mobile.match(name)){
    document.getElementById("search_type").value="username";
    //alert("Name !");
    //alert(document.getElementById("search_type").value);
    return true;
  }
  else{  
    document.getElementById("search_type").value="";
    alert("Please enter a valid Booking Id / Mobile Number / Email / User Name !");
    //alert(document.getElementById("search_type").value);
    return false;  
  }  
}
</script>

</body>
</html>

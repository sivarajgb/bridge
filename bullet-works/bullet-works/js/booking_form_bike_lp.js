function sendTempData(t, e) {
    let utm_campaign = $("input[name=utm_campaign]").val(),
    utm_medium = $("input[name=utm_medium]").val(),
    utm_source = $("input[name=utm_source]").val();
    if(utm_campaign==""){
        utm_campaign ="bike-service-landing-page";
    }
    if ("get-started" === t) {
        var n = $("#vehicle_tags").val(),
            a = $(".wrapper").attr("data-log");
        $.ajax({
            url: "./temp_booking.php",
            type: "POST",
            async: !1,
            data: {
                step: 1,
                vehicle: n,
                log: a,
                utm_campaign: utm_campaign,
                utm_medium: utm_medium,
                utm_source: utm_source
            },
            success: function(t) {
                temp_id = t;
            }
        })
    }
    if ( "locality" === t && (n = $("#location_tags").val(), $.ajax({
            url: "./temp_booking.php",
            type: "POST",
            data: {
                step: 2,
                locality: n,
                id: temp_id
            },
            success: function(t) {
                temp_id = t
                
            }
        })), "mobile-number" === t) {
        var i = $("#mobile_number").val();
        $.ajax({
            url: "./temp_booking.php",
            type: "POST",
            data: {
                step: 3,
                mobile_number: i,
                id: temp_id
            },
            success: function(t) {
                temp_id = t;
               
                     $.ajax({
                     url: "./getOTP.php",
                     type: "GET",
                     data: {
                         mobile: i,
                         id: temp_id
                     },
                     success: function(t) {
                         $("#myModal").modal("show"), $("#OTP").val(""), $("#otp_submit").click(function() {
                             $("#OTP").val() === t.trim() ? ($("#get_deals_text").hide(), $("#lds-ring").show(), $("#myModal").modal("hide"), $.ajax({
                                 url: "./book-now.php",
                                 type: "POST",
                                 data: {
                                     temp_id: temp_id
                                 },
                                 success: function(t) {
                                     t = t.split("=");
                                     var e = t[0];
                                     $.ajax({
                                         url: "./sendSMS.php",
                                         type: "GET",
                                         data: {
                                             mobile: i,
                                             booking_id: e
                                         },
                                         success: function(t) {
                                         }  
                                     }),
                                    window.location.href = "./checkout/" + t[0]
                                 }
                             })) : $("#otp_mismatch").show()
                         })
                     }
                 }) 
                
            }
        }) 
    }
    return temp_id
}
$(function() {
    let form_height = $("#details-form").height();
    let formPos = $("#details-form").offset();
    formPos = formPos.top + form_height;
    let headerImagePos  = $(".header-image").offset().top;
    if($(window).width()<500){
        if(headerImagePos < formPos){
            $("header").height($("header").height()+60);
//         console.log("overlapping");
        }
        else{
 //       console.log(headerImagePos+" "+formPos);
        }
    }
    $(window).resize(function() {
        350 >= $(window).height() ? $("header").height($(".header-section").height() + $(".header-image").height() + 10) : $("header").height($("header").height())
    }), $("#lds-ring").hide();
    $("header").height($("header").height()+30);
    
    var n = Array.from($(".input")),
        a = $(".description-flex-container").find("section");
        a = Array.from(a);
        console.log(n.length+" "+a.length);
    var d = $("#location-entry"),
        l = $("#contact-entry");
    var u, p, h, m, f, v = $(".contact-input"),
        g = 0,
        y = 0,
        b = "Bridgestone",
        _ = "",
        w = [],timeValue;
    $("#get-started-button").click(function(t) {
        t.preventDefault(),
        $(window).width() < 414 && $(".flex-parent").height("95vh"), 
        "" === $("#vehicle_tags").val() ? $("#required").show() : (sendTempData("get-started"), $("#required").hide(), $("#header-section").attr("style", "display:none"), $("#booking-form-section").attr("style", "display:block;")), -1 !== navigator.userAgent.indexOf("Safari") && -1 == navigator.userAgent.indexOf("Chrome") && 414 >= $(window).width() && (console.log("safari browser"), $(".flex-parent").height("95vh")), document.body.scrollTop = document.documentElement.scrollTop = 0
        y=1;
        (m = h.index()) <= g && (a.forEach(function(t) {
            $(t).removeClass("active")
        }), $(a[m]).addClass("active")), y = m
    })
    ,$(d).click(function(t) {
        if($(".location-input").val()==""){
            t.preventDefault();
            $("#location-required").show();
        }
        else{
        t.preventDefault();
        console.log("in here");
        sendTempData("locality"), 
        $(".location-section").removeClass("active"), 
        $(".contact-section").addClass("active"),
        1 > g && ($(n[1]).removeClass("active"), 
        $(n[2]).addClass("active")), y = g = 1, 
        $(".location-input").val(), $("#lds-ring").hide(), $("#get_deals_text").show()
        }
    }), $(l).click(function(t) {
        g = 2;
        sendTempData("mobile-number"), t.preventDefault(), $(".contact-input").val(), $("#lds-ring").show(), $("#get_deals_text").hide()
    }), $(n).click(function() {
        let boolean="";
        if($(this).index()==2 ){
             boolean = $("#location-entry").attr("disabled");
        }
//        if(boolean != "disabled"){
            h = $(this);
            if(h.index()==0){
                y=0;
                $(".link").trigger("click");
            }
            else{
            (m = h.index()) <= g && (a.forEach(function(t) {
            $(t).removeClass("active")
        }), $(a[m-1]).addClass("active")), y = m
            }
        
//        }
    }),
     $(v).on("input", function(t) {
        $(".contact-input").val().match(/[a-z]/i) && $(".contact-input").val(previouslyEnteredNumber), 10 === $(".contact-input").val().length ? ($(l).attr("disabled", !1), previouslyEnteredNumber = $(".contact-input").val()) : 10 < $(".contact-input").val().length ? $(".contact-input").val(previouslyEnteredNumber) : ($(l).attr("disabled", !0), previouslyEnteredNumber = $(".contact-input").val()), 13 === t.which && 10 !== $(".contact-input").val().length && t.preventDefault()
    }), $(".link").click(function() {
        0 === y ? ($("#booking-form-section").attr("style", "display:none;"), $("#header-section").attr("style", "display:block;")) : ((m = y - 1) <= g && (a.forEach(function(t) {
            $(t).removeClass("active")
        }), $(a[m]).addClass("active")), y--)
    })
}), $('.inline a[href="#"]').click(function(t) {
    t.preventDefault()
}), $(".enter-vehicle-input").on("keypress", function(t) {
    13 == t.which && t.preventDefault();
}),$(".location-input").on("keypress",function(t){
    $("#location-entry").attr("disabled",true);
    13 == t.which && t.preventDefault();
});

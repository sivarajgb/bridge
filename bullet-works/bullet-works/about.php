<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>About Us | GoBumpr Bull-at-Works</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" sizes="16x16" href="https://static.gobumpr.com/web-app/fav-icons/16.png">
        <link rel="icon" type="image/png" sizes="32x32" href="https://static.gobumpr.com/web-app/fav-icons/32.png">
        <link rel="icon" type="image/png" sizes="57x57" href="https://static.gobumpr.com/web-app/fav-icons/57.png">
        <link rel="icon" type="image/png" sizes="76x76" href="https://static.gobumpr.com/web-app/fav-icons/76.png">
        <link rel="icon" type="image/png" sizes="96x96" href="https://static.gobumpr.com/web-app/fav-icons/96.png">
        <link rel="icon" type="image/png" sizes="114x114" href="https://static.gobumpr.com/web-app/fav-icons/114.png">
        <link rel="icon" type="image/png" sizes="144x144" href="https://static.gobumpr.com/web-app/fav-icons/144.png">
        <link rel="icon" type="image/png" sizes="152x152" href="https://static.gobumpr.com/web-app/fav-icons/152x152.png">
        <style>
            .link,.user_data{float:none}.gobumpr_logo{width:150px}.header_h1{font-size:24px}.header_p{font-size:18px}.help-button{display:none}.link{color:#FFA800}.get-in-touch div{width:100%}
        </style>
        <!-- Latest compiled and minified JavaScript -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600" >
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<!--        <link rel="stylesheet" href="/tyrest/css/aboutPageCss.css">
        <link rel="stylesheet" href="/tyrest/css/faqPageCss.css">-->
    </head>
    <body>
        <nav class="navbar navbar-default">
                <div class="container-fluid">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                      <a href="./faqs" class="help-button"><img src="https://static.gobumpr.com/tyres/help.svg" class="img-responsive" alt="GoBumpr Tyres Help"></a>                 
                      <button type="button" class="navbar-toggle collapsed navbar-togglecollapsed" data-toggle="collapse" data-target="#bs-navbar-collapse-1" aria-expanded="false">
                        <img src="https://static.gobumpr.com/tyres/menu.svg" class="menu-icon" alt="GoBumpr menu"> 
                      </button>
                      <!--https://static.gobumpr.com/tyres/-->
                      <a class="logo" href="./"><img src="https://static.gobumpr.com/img/logo-new-gb.svg"" class="img-responsive gobumpr_logo" alt="GoBumpr Bike Service Online"/></a>
                  </div>
                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right" >
                      <li><a href="index.php">HOME</a></li>
                      <li><a href="#">ABOUT</a></li>
                      <li><a href="#" id="contact_link">CONTACT</a></li>
                      <li><a target="_self" href="faqs" >FAQ</a></li>
                      <li><a href="#" id="testimonials_link">TESTIMONIALS</a></li>
                    </ul>
                  </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
        </nav>
        <div>
           <header>
            <div class="header">
                <h1 class="header_h1">Reimagining automotive maintenance,</h1>
                <p class="header_p">One aspect at a time.</p>
            </div>
            </header> 
        </div>
        <div class="aboutUs_content">
            <p>Hola! GoBumpr - Chennai's most loved automobile service facilitator brings to you an all-new product - <a href="https://marketing.gobumpr.com/bullet-works"  class="link">GoBumpr Bull-at-Works</a></p>
            <p>GoBumpr! is your one stop shop to service, maintain and repair all of your cars & bikes at a single tap of your mobile screen - starting from Car Service, Bike Service, 24x7 Breakdown Assistance, Car Repair, Bike Repair, Road Assistance on the go. No need to panic when you are stuck in the middle of a road owing to a break-down. Book your service at just tap of your mobile screen! Today GoBumpr has become the common choice for people who value their vehicles and want only the best when it comes to service. </p>
            
        </div>
        <style type="text/css">
                        .glyphicon-star {
                            font-size: 20px;
                            color: #ffa800;
                          }
                          .glyphicon-star.half {
                            position: relative;
                          }
                          .glyphicon-star.half:before {
                            position: relative;
                            z-index: 9;
                            width: 47%;
                            display: block;
                            overflow: hidden;
                          }
                          .glyphicon-star.half:after {
                            content: '\e006';
                            position: absolute;
                            z-index: 8;
                            color: #bdc3c7;
                            top: 0;
                            left: 0;
                          }
                      </style>
        <div class="testimonials" id="testimonials">
            <h3 class="text-center">Testimonials</h3>
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
              <div class="item active container">
                  <div class="row">
                      <div class="user_image">
                          <img src="https://static.gobumpr.com/offers-imgs/testimonials/vishal.jpg" alt="GoBumpr user image">
                      </div>
                      <div class="user_data">
                          <p class="user_name">Vishal</p>
                          <p class="user_car">Royal Enfield Thunderbird 500</p>
                          <div class="rating">
                            <i class="glyphicon glyphicon-star"></i>
                            <i class="glyphicon glyphicon-star"></i>
                            <i class="glyphicon glyphicon-star"></i>
                            <i class="glyphicon glyphicon-star"></i>
                            <i class="glyphicon glyphicon-star"></i>
                          </div>
                          <p class="user_views">"Very good response and service. All my requirements were duly noted and the issues were rectified promptly. The mileage of my Thunderbird has increased post-service. Excellent work at the garage. "</p>
                      </div> 
                  </div>
              </div>
              <div class="item container">
                <div>
                      <div class="user_image">
                          <img src="https://static.gobumpr.com/offers-imgs/testimonials/vigneshwaran.jpg" alt="GoBumpr user image">
                      </div>
                      <div class="user_data">
                          <p class="user_name">Vigneshwaran</p>
                          <p class="user_car">Royal Enfield Classic Desert Storm</p>
                          <div class="rating">
                            <i class="glyphicon glyphicon-star"></i>
                            <i class="glyphicon glyphicon-star"></i>
                            <i class="glyphicon glyphicon-star"></i>
                            <i class="glyphicon glyphicon-star"></i>
                            <i class="glyphicon glyphicon-star half"></i>
                          </div>
                          <p class="user_views">"Can feel the difference in smoothness while driving my vehicle. The vehicle was attended to carefully and the service was done to perfection. Extremely happy! "</p>
                      </div>
                  </div>
              </div>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
        <div class="container" id="contact_us">
            <div class="text-center">
                <img src="https://static.gobumpr.com/tyres/contact.svg" class="contact_img" alt="Contact us">
            </div>
            <form method="POST" name="contact_form" action="contact-form-handler.php">
                <input type="text" name="name"placeholder="Name" class="form-control" spellcheck="false" required>
                <input type="email" name="email"placeholder="E-mail Address" class="form-control" spellcheck="false" required>
                <input type="text" pattern="[0-9]{10}"name="mobile" placeholder="Contact No." class="form-control contact-input" spellcheck="false" required>
                <textarea cols="2" name="message" placeholder="How can we help you?" class="form-control" spellcheck="false" required></textarea>
                <div class="text-center"><button class="btn text-center" id="message_button" spellcheck="false">MESSAGE</button></div>
            </form>
        </div>
        <footer class="text-center">
            <div class="container get-in-touch">
                <div class="row">
                    <p>Get in touch with us on </p>
                    <ul id="social">
                        <li><a title="facebook" target="_blank" href="https://www.facebook.com/gobumprapp"><i class="fab fa-facebook-f"></i></a></li>               
                        <li><a title="twitter" target="_blank" href="https://twitter.com/gobumpr"><i class="fab fa-twitter"></i></a></li>             
                        <li><a title="instagram" target="_blank" href="https://www.instagram.com/letsgobumpr/"><i class="fab fa-instagram"></i></a></li>
                        <li><a title="youtube" target="_blank" href="https://www.youtube.com/watch?v=rQqFgE191SA"><i class="fab fa-youtube"></i></a></li> 
                        <li><a title="linkedIn" href="https://www.linkedin.com/company/gobumpr-" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                    </ul>
                </div>
            </div>
            <p class="footer-line">&#169; <?php echo date('Y');?> - NORTHERLY AUTOMATIVE SOLUTIONS PRIVATE LIMITED. ALL RIGHT RESERVED.<a id="terms-of-services" href="https://gobumpr.com/terms" target="_blank">TERMS OF SERVICE.</a>
        </footer>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script>
            $(function()
            {
                let previouslyEnteredNumber = 0;
                $(".contact-input").on('input',function(){
                if(($('.contact-input').val()).match(/[a-z]/i)){
                    $('.contact-input').val(previouslyEnteredNumber);
                }
                if(($('.contact-input').val()).length === 10){
                    previouslyEnteredNumber = $('.contact-input').val();
                }
                else if(($('.contact-input').val()).length >10){
                    $('.contact-input').val(previouslyEnteredNumber);
                }
                 previouslyEnteredNumber = $('.contact-input').val();
            });
            $("#contact_link").click(function(){
                 $('html, body').animate({
                    scrollTop: $("#contact_us").offset().top
                }, 1000);
            });
            $("#testimonials_link").click(function(){
                 $('html, body').animate({
                    scrollTop: $("#testimonials").offset().top
                }, 1000);
            });
            });          
        </script>
        <noscript id="deferred-styles">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600" >
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
            <!-- <link rel="stylesheet" href="/full-body-painting/css/aboutPageCss.css">
            <link rel="stylesheet" href="/full-body-painting/css/faqPageCss.css"> -->
            <link rel="stylesheet" href="/bike-service/css/faqPageCss.css">
            <link rel="stylesheet" href="/bike-service/css/aboutPageCss.css">
        </noscript>
        <script>
          var loadDeferredStyles = function() {
            var addStylesNode = document.getElementById("deferred-styles");
            var replacement = document.createElement("div");
            replacement.innerHTML = addStylesNode.textContent;
            document.body.appendChild(replacement)
            addStylesNode.parentElement.removeChild(addStylesNode);
          };
          var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
              window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
          if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
          else window.addEventListener('load', loadDeferredStyles);
        </script>
    </body>
</html>
<?php
include('config.php');
$conn = dbconnect();
$conn2 = dbconnect2(); 
$booking_id = base64_decode($_GET["id"]);
$num_of_deals= "SELECT * FROM b2b_booking_tbl_tyres WHERE gb_booking_id=".$booking_id;
$count_result = mysqli_query($conn2,$num_of_deals);
$num_deals = mysqli_num_rows($count_result);
$get_deals_query = "SELECT * FROM b2b_booking_tbl_tyres WHERE gb_booking_id=".$booking_id." AND b2b_bid_flg = 1 ORDER BY b2b_acpt_flg DESC"; 
$query_result = mysqli_query($conn2,$get_deals_query);
$count =  mysqli_num_rows($query_result);
$data = array();
$fetch =  $num_deals - $count;
$cd_query = "SELECT user_id,locality,tyre_brand FROM go_bumpr.tyre_booking_tb WHERE booking_id=".$booking_id;
$cd_result = mysqli_query($conn,$cd_query);
$cd_values = mysqli_fetch_object($cd_result);
$user_id= $cd_values->user_id;
$user_locality = $cd_values->locality;
$actual_tyre_brand = $cd_values->tyre_brand;
$left_it_to_expert = false;
if($actual_tyre_brand == ""){
    $left_it_to_expert = true;
}
$ll_query = "SELECT lat,lng FROM localities WHERE localities='".$user_locality."'";
$ll_result = mysqli_query($conn,$ll_query);
$ll_values = mysqli_fetch_array($ll_result);
$booking_lat = $ll_values[0];
$booking_lng = $ll_values[1];
while($row = mysqli_fetch_object($query_result)){
    $array_data=array();
    $bid_flag = $row->b2b_bid_flg;
    $shop_id = $row->b2b_shop_id;
    $bid_amount = $row->b2b_bid_amt;
    $b2b_booking_id = $row->b2b_booking_id;
    $tyre_brand = $row->b2b_tyre_brand;
    $shop_details_query = "SELECT address4,mobile_number1,shop_name,address1,address2,address3,address4,pincode,(6371*ACOS(COS(RADIANS('$booking_lat'))*COS(RADIANS(lat))*COS(RADIANS(lng)-RADIANS('$booking_lng'))+SIN(RADIANS('$booking_lat'))*SIN(RADIANS(lat)))) as dist,axle_id FROM admin_mechanic_table where axle_id=".$shop_id;
    $shop_query_result = mysqli_query($conn,$shop_details_query);
    $shop_query_values = mysqli_fetch_object($shop_query_result);
    $locality = $shop_query_values->address4;
    $mobile_number = $shop_query_values->mobile_number1;
    $shop_name = $shop_query_values->shop_name;
    $address = '';
    $axle_id= $shop_query_values->axle_id;
    if($shop_query_values->address1!=""){
        $address = $shop_query_values->address1."<br/>";
    }
    if($shop_query_values->address2!=""){
        $address = $address.$shop_query_values->address2."<br/>";
    }
    if($shop_query_values->address3!=""){
        $address = $address.$shop_query_values->address3."<br/>";
    }if($shop_query_values->address4!=""){
        $address = $address.$shop_query_values->address4;
    }
//    $address = $shop_query_values->address1."<br>".$shop_query_values->address2."<br>".$shop_query_values->address3."<br>".$shop_query_values->address4."<br>".$shop_query_values->pincode;
    $distance = round($shop_query_values->dist,2);
    $selected_flag = $row->b2b_acpt_flg;
    $invalid_shop_id = [];
    if(!in_array($shop_id,$invalid_shop_id)){
    array_push($array_data,$bid_amount,$locality,$shop_name,$mobile_number,$address,$distance,$b2b_booking_id,$selected_flag,$bid_flag);
    if($left_it_to_expert===true){
        array_push($array_data,$tyre_brand);
    }
    else{
       array_push($array_data,"undefined");
    }
    array_push($array_data,$axle_id);
    array_push($array_data,$fetch);
    $data[]=$array_data;
    }
}
$data_encoded = json_encode($data);
echo $data_encoded;
?>
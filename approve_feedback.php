<?php
include("config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();

$today = date('Y-m-d');
$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$action = $_POST['action'];
if($action == "approve")
{
	$feedback=mysqli_real_escape_string($conn1,$_POST['feedback']);
	$booking_id=mysqli_real_escape_string($conn1,$_POST['bi']);
	$rating_id=mysqli_real_escape_string($conn1,$_POST['ri']);
	$mec_id=mysqli_real_escape_string($conn1,$_POST['mi']);
	$b2b_booking_id=mysqli_real_escape_string($conn1,$_POST['b2b_booking_id']);

	$sql_rat_gb = "UPDATE user_rating_tbl SET feedback='$feedback',log='$today',status='0' WHERE rating_id='$rating_id'";
	$res_rat_gb = mysqli_query($conn1,$sql_rat_gb) or die(mysqli_error($conn1));

	//update overall rating in gobumpr

	$sql_o_rat_gb = "SELECT avg(rating) as rating FROM user_rating_tbl WHERE mec_id='$mec_id' AND status!='1' AND rating!='0'";
	$res_o_rat_gb = mysqli_query($conn1,$sql_o_rat_gb) or die(mysqli_error($conn1));

	$row_o_rat = mysqli_fetch_object($res_o_rat_gb);
	$overall_rating_gb = $row_o_rat->rating;
	$overall_rating_gb = round($overall_rating_gb,2);
	$sql_mec_overall = "UPDATE admin_mechanic_table SET ratting='$overall_rating_gb' WHERE mec_id='$mec_id' ";
	$res_mec_overall = mysqli_query($conn1,$sql_mec_overall) or die(mysqli_error($conn1));

	$sql_book_b2b = "SELECT b2b_shop_id FROM b2b_booking_tbl WHERE b2b_booking_id='$b2b_booking_id'";
	$res_book_b2b = mysqli_query($conn2,$sql_book_b2b) or die(mysqli_error($conn2));
	$row_book_b2b = mysqli_fetch_object($res_book_b2b);

	$b2b_shop_id = $row_book_b2b->b2b_shop_id;

	//update overall rating in b2b

	$sql_o_rat_b2b = "SELECT avg(b2b_Rating) as b2b_rating FROM b2b_booking_tbl WHERE b2b_shop_id='$b2b_shop_id' AND b2b_flag!='1' AND b2b_Rating!='0'";
	$res_o_rat_b2b = mysqli_query($conn2,$sql_o_rat_b2b) or die(mysqli_error($conn2));

	$row_o_rat_b2b = mysqli_fetch_object($res_o_rat_b2b);
	$overall_rating_b2b = $row_o_rat_b2b->b2b_rating;
	$overall_rating_b2b = round($overall_rating_b2b,2);
	$sql_mec_overall = "UPDATE b2b_mec_tbl SET b2b_overall_rating='$overall_rating_b2b' WHERE b2b_shop_id='$b2b_shop_id' ";
	$res_mec_overall = mysqli_query($conn2,$sql_mec_overall) or die(mysqli_error($conn2));
}
else
{
	$rating_id=mysqli_real_escape_string($conn1,$_POST['ri']);
	$sql_rat_gb = "UPDATE user_rating_tbl SET log='$today',status='1',reject_flag='1' WHERE rating_id='$rating_id'";
	$res_rat_gb = mysqli_query($conn1,$sql_rat_gb) or die(mysqli_error($conn1));
}
?>
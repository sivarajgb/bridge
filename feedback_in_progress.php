<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id']))) {

	header('location:logout.php');
	die();
}

$date = base64_decode($_GET['dt']);

?>

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>GoBumpr Bridge</title>


  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


  <!-- Include Date Range Picker -->
  <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<!-- date time picker -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

  <!-- table sorter -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js"></script>

<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>
<!-- star rating -->
<link rel="stylesheet" href="css/star-rating.min.css">
<script src="js/star-rating.min.js"></script>
<style>
/*home page blocks */
.floating-box {
 display: inline-block;
 margin: 2px;
 padding-top: 12px;
 padding-left: 12px;
 width:300px;
}
.navbar-fixed-top{
  z-index:100 !important;
}
.upper-div{
  z-index:999999 !important;
}
#range > span:hover{cursor: pointer;}
 /* table */
#tbody{
   font-size:15px !important;
  border:1.5px solid #c4b8b8 !important;

}
thead:hover{
	cursor:pointer;
}

.results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px;
  color:#9E9E9E;
}
#tbody, tbody tr {
    -webkit-animation: opacity 5s ease-in-out;
    animation: opacity 5s ease-in-out;
}
.datepicker {
	  cursor:pointer;
  }
.datepicker:before {
content: '';
display: inline-block;
border-left: 7px solid transparent;
border-right: 7px solid transparent;
border-bottom: 7px solid #ccc;
border-bottom-color: transparent !important;
position: absolute;
top: -7px;
left: 190px;  // I made a change here 
}

.datepicker:after {
content: '';
display: inline-block;
border-left: 6px solid transparent;
border-right: 6px solid transparent;
border-top-color: transparent !important;
border-top: 6px solid #ffffff;
position: absolute;
bottom: -6px;
left: 191px;  // I made a change here 
}

</style>

</head>
<body id="body">
<?php include_once("header.php"); ?>
<script>
	$(document).ready(function(){
		$('#city').show();
	})
</script>
<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>

<!-- date range picker -->
<div id="reportrange" class=" col-sm-3 " style="cursor: pointer; margin-top:28px; margin-left: 40px;">
    <div class=" floating-box1">
        <div id="range" class="form-control" style="min-width:312px;">
        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
        <span id="dateval"></span> <b class="caret"></b>
        </div>
    </div>
</div>


<!-- vehicle tye filter -->
<div id="vehicle" class=" col-sm-2 " style="cursor: pointer; margin-top:28px; margin-left: 40px;">
    <div class=" floating-box1">
        <select id="vehicle_type" name="vehicle_type" class="form-control">
        <?php 
        if(isset($_GET['vt'])){
         $veh = base64_decode($_GET['vt']);
          if($veh == '2w'){
          ?>
        <option value="2w" selected>2 Wheeler</option>
        <option value="4w">4 Wheeler</option>
        <?php } 
        else if($veh == '4w'){
          ?>
        <option value="2w">2 Wheeler</option>
        <option value="4w" selected>4 Wheeler</option>

          <?php
        }
        else{
          ?>
        <option value="all" selected>All Vehicles</option>
        <option value="2w">2 Wheeler</option>
        <option value="4w">4 Wheeler</option>

          <?php
        }
        }
        else{
          ?>
        <option value="all" selected>All Vehicles</option>
        <option value="2w">2 Wheeler</option>
        <option value="4w">4 Wheeler</option>

          <?php
        }
        ?>
        </select>
    </div>
</div>
<!-- search bar -->
<div class=" col-sm-4" style="margin-top:28px;">
  <div  class="floating-box1" >
  <div class="form-group pull-right" >
      	<input type="text" class="search form-control" id="su" name="mobile" placeholder="Search">   
  </div>
  <span class="counter pull-right"></span>
  </div>
  </div>
</div>
<div id="show" style="margin-top:82px;width:99%;display:none;">
</div>
<!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>


<!-- jQuery library -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- date range picker -->
<script>
$('input[name="daterange"]').daterangepicker({
 locale: {
      format: 'DD-MM-YYYY'
    }
});
</script>
<script type="text/javascript">
$(function() {

  //var start = moment().subtract(1, 'days');
  //var start = moment().startOf('month');
  var start = moment('<?php echo $date; ?>');
   var end = moment('<?php echo $date; ?>');
	 // var start = moment();
    //var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});
</script>
<!-- default view -->
<script>
function viewtable(){
	var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
	var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
	var vehicle_type = $('#vehicle_type').val();
  var city = $('#city').val();
	//console.log(startDate);
	//console.log(endDate);
	//Make AJAX request, using the selected value as the POST
  $.ajax({
	    url : "ajax/feedback_in_progress_view.php",  // create a new php page to handle ajax request
	    type : "POST",
	    data : {"startdate": startDate , "enddate": endDate,"vehicle_type": vehicle_type, "city":city},
	    success : function(data) {
        //alert(data);
        //console.log(data);
        $("#loading").hide();
        $('#show').show();
        $('#show').html(data);
        counterval();
      },
	    error: function(xhr, ajaxOptions, thrownError) {
	      //  alert(xhr.status + " "+ thrownError);
	    }
  });
}
</script>
<script>
$(document).ready(function(){
  $('#show').hide();
  $("#loading").show();
  viewtable();
});
</script>
<!-- on change of date -->
<script>
$(document).ready( function (){
	$('#dateval').on("DOMSubtreeModified", function (){
		//console.log("date changed");
     $('#show').hide();
	   $("#loading").show();
	   viewtable();
	 });
});
</script>
<!--  on Selecting vehicles -->
<script>
$(document).ready(function() {
$("#vehicle_type").change(function (){
  $('#show').hide();
  $("#loading").show();
  viewtable();
} );
});
</script>
<!--  on changing city -->
<script>
$(document).ready(function() {
$("#city").change(function (){
  $('#show').hide();
  $("#loading").show();
  viewtable();
} );
});
</script>
<!-- search bar  -->
<script>
$(document).ready(function() {
  $(".search").keyup(function () {
    var searchTerm = $(".search").val();
    var listItem = $('.results tbody').children('tr');
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

  $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });

  $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','false');
  });

  $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','true');
  });

  var jobCount = $('.results tbody tr[visible="true"]').length;
    $('.counter').text(jobCount + ' item');

  if(jobCount == '0') {$('.no-result').show();}
    else {$('.no-result').hide();
	}
		  });
});
</script>
<script>
function counterval(){
	var jobCount = $("#tbody tr").length;;
	 $('.counter').text(jobCount + ' item');
}
</script>
</body>
</html>

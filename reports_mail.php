<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
include('config.php');
$conn = db_connect1();

require_once('New/email/class.phpmailer.php');
require_once('New/email/class.smtp.php');

echo "<br>today:".$today = date('Y-m-d');
echo "<br>yesterday:".$yesterday = date("Y-m-d", strtotime('-1 days'));
echo "<br>last week:".$last_week = date("Y-m-d", strtotime('-1 week'));
echo "<br>prevs week:".$previous_week = date("Y-m-d", strtotime('-2 week'));
echo "<br>this month:".$this_month = date("Y-m-01");
echo "<br>this month end:".$this_month_last = date("Y-m-t");
echo "<br>last month:".$last_month = date("Y-m-01", strtotime('-1 month'));
echo "<br>last 3 months start:".$three_m_ago = date("Y-m-01", strtotime('-3 month'));

$tm_date1=date_create($this_month);
$tm_date2=date_create($today);
$no_of_days_this_month_c =date_diff($tm_date1,$tm_date2);
echo "<br>no.of days till today this mnth:".$no_of_days_this_month = $no_of_days_this_month_c->format("%a"); 
echo "<br>no.of days in last month:".$no_of_days_last_month = date(' t ', strtotime($last_month) );

$l3m_date1=date_create($three_m_ago);
$l3m_date2=date_create($this_month);
$no_of_days_last_3_month_c =date_diff($l3m_date1,$l3m_date2);

echo "<br>no.of days in last 3 month:".$no_of_days_last_3_months = $no_of_days_last_3_month_c ->format("%a"); 



$sql = "SELECT b.gb_booking_id,b.b2b_credit_amt,g.vehicle_type,b.b2b_log,g.shop_name,(SELECT p.name FROM go_bumpr.crm_admin AS p WHERE p.crm_log_id = g.crm_update_id) as crm_name,(SELECT p.city FROM go_bumpr.crm_admin AS p WHERE p.crm_log_id = g.crm_update_id) as crm_city,(SELECT p.flag FROM go_bumpr.crm_admin AS p WHERE p.crm_log_id = g.crm_update_id) as person_flag,g.city,(SELECT sr.master_service FROM go_bumpr.go_axle_service_price_tbl AS sr WHERE sr.service_type = g.service_type AND sr.type=g.vehicle_type) as master_service,s.b2b_acpt_flag,s.b2b_deny_flag,(SELECT cr.b2b_partner_flag FROM b2b.b2b_credits_tbl as cr WHERE cr.b2b_shop_id = b.b2b_shop_id) as premium FROM b2b.b2b_booking_tbl AS b INNER JOIN go_bumpr.user_booking_tb AS g ON b.gb_booking_id = g.booking_id LEFT JOIN b2b.b2b_status AS s ON b.b2b_booking_id = s.b2b_booking_id WHERE b.b2b_log BETWEEN '$three_m_ago' AND '$today' AND b.b2b_swap_flag!='1' AND g.crm_update_id NOT IN ('crm003','crm036') AND g.mec_id NOT IN (200360,200018,400001,400974,200629)";
$res = mysqli_query($conn,$sql) or die(mysqli_error($conn));

// fetch the metric values into array
if(mysqli_num_rows($res) >= 1){ 
    // fetch data
    {     
        while($row = mysqli_fetch_object($res)){
            $acpt = $row->b2b_acpt_flag;
            $deny = $row->b2b_deny_flag;
            $premium = $row->premium;
            $person_flag = $row->person_flag;

            if($acpt == 1 && $deny == 0 && $person_flag!='1'){
                //accepted 
                $city_arr_accepted[] = ucwords(strtolower($row->city));
                $master_service_arr_accepted[] = $row->master_service;
                $vehicle_arr_accepted[] =$row->vehicle_type;
               // $shop_name_arr_accepted[] = ucwords(strtolower($row->shop_name));
                $person_arr_accepted[] = array("crm_name" =>$row->crm_name,"city" =>$row->crm_city) ;
                $data_accepted[] = array("booking_id" => $row->gb_booking_id,"credits" => $row->b2b_credit_amt,"vehicle" => $row->vehicle_type,"log"=>$row->b2b_log,"shop_name"=>ucwords(strtolower($row->shop_name)),"person"=>$row->crm_name,"city"=>ucwords(strtolower($row->city)),"master_service"=>$row->master_service); 
                
                if($premium == '2'){
                    $city_arr_accepted_pre[] = ucwords(strtolower($row->city));
                    $master_service_arr_accepted_pre[] = $row->master_service;
                    $vehicle_arr_accepted_pre[] =$row->vehicle_type;
                    $shop_name_arr_accepted_pre[] = ucwords(strtolower($row->shop_name));
                    $data_accepted_pre[] = array("booking_id" => $row->gb_booking_id,"credits" => $row->b2b_credit_amt,"vehicle" => $row->vehicle_type,"log"=>$row->b2b_log,"shop_name"=>ucwords(strtolower($row->shop_name)),"person"=>$row->crm_name,"city"=>ucwords(strtolower($row->city)),"master_service"=>$row->master_service); 
                }
            }
           /* else if($acpt == 0 && $deny == 1 && $person_flag!='1'){
                //rejected
                $city_arr_rejected[] = ucwords(strtolower($row->city));
                $master_service_arr_rejected[] = $row->master_service;
                $vehicle_arr_rejected[] =$row->vehicle_type;
                $shop_name_arr_rejected[] = ucwords(strtolower($row->shop_name));
               // $person_arr_rejected[] = $row->crm_name;
                $data_rejected[] = array("booking_id" => $row->gb_booking_id,"credits" => $row->b2b_credit_amt,"vehicle" => $row->vehicle_type,"log"=>$row->b2b_log,"shop_name"=>ucwords(strtolower($row->shop_name)),"person"=>$row->crm_name,"city"=>ucwords(strtolower($row->city)),"master_service"=>$row->master_service);
            }
            else if($acpt == 0 && $deny == 0 && $person_flag!='1'){
                //idle
                $city_arr_idle[] = ucwords(strtolower($row->city));
                $master_service_arr_idle[] = $row->master_service;
                $vehicle_arr_idle[] =$row->vehicle_type;
                $shop_name_arr_idle[] = ucwords(strtolower($row->shop_name));
               // $person_arr_idle[] = $row->crm_name;
                $data_idle[] = array("booking_id" => $row->gb_booking_id,"credits" => $row->b2b_credit_amt,"vehicle" => $row->vehicle_type,"log"=>$row->b2b_log,"shop_name"=>ucwords(strtolower($row->shop_name)),"person"=>$row->crm_name,"city"=>ucwords(strtolower($row->city)),"master_service"=>$row->master_service);    
            }*/
        }

        // accepted set
        //echo "<h3>Accepted set</h3>";
        $city_arr_accepted = array_unique($city_arr_accepted);
        $master_service_arr_accepted = array_unique($master_service_arr_accepted);
        $vehicle_arr_accepted = array_unique($vehicle_arr_accepted);
        //$shop_name_arr_accepted = array_unique($shop_name_arr_accepted);
        //$person_arr_accepted = array_unique($person_arr_accepted);
        $person_arr_accepted = array_map('unserialize', array_unique(array_map('serialize', $person_arr_accepted)));
        // Obtain a list of columns
        foreach ($person_arr_accepted as $key => $row) {
            $pcity[$key]  = $row['city'];
        }
        // Sort the data with volume descending, edition ascending
        // Add $data as the last parameter, to sort by the common key
        array_multisort($pcity, SORT_ASC, $person_arr_accepted);
       // var_dump($person_arr_accepted1);
        
        //city accepted set
        {

            $city_table_accepted_g = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">City</th>
              <th colspan="5" class="list1">Avg no.of Goaxles</th>
            </tr>
            <tr class="subheader">
              <td class="list1">LastWeek</td>
              <td class="list1">PreviousWeek</td>
              <td class="list1">MonthToDate</td>
              <td class="list1">LastMonth</td>
              <td class="list1">LastThreeMonths</td>
            </tr>';
            $city_table_accepted_c = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">City</th>
              <th colspan="5" class="list2">Avg Credits Acquired</th>
            </tr>
            <tr class="subheader">
              <td class="list2">LastWeek</td>
              <td class="list2">PreviousWeek</td>
              <td class="list2">MonthToDate</td>
              <td class="list2">LastMonth</td>
              <td class="list2">LastThreeMonths</td>
            </tr>';

                foreach($city_arr_accepted as $value_accepted){
                    $city_b_c_lw_accepted = 0 ;$city_c_c_lw_accepted = 0 ;
                    $city_b_c_pw_accepted = 0 ;$city_c_c_pw_accepted = 0 ;
                    $city_b_c_tm_accepted = 0 ;$city_c_c_tm_accepted = 0 ;
                    $city_b_c_lm_accepted = 0 ;$city_c_c_lm_accepted = 0 ; 
                    $city_b_c_l3m_accepted = 0 ;$city_c_c_l3m_accepted = 0 ;
                    foreach ($data_accepted as $key_accepted){
                        $credits_accepted = $key_accepted['credits'];
                        $city_accepted = $key_accepted['city'];
                        $log_accepted = $key_accepted['log'];
                        
                        if($log_accepted<$today && $log_accepted>= $last_week){
                            if($city_accepted == $value_accepted){
                                $city_b_c_lw_accepted = $city_b_c_lw_accepted+1;
                                $city_c_c_lw_accepted = $city_c_c_lw_accepted+$credits_accepted;
                            }
                        }
                        if($log_accepted<$last_week && $log_accepted>= $previous_week){
                            if($city_accepted == $value_accepted){
                                $city_b_c_pw_accepted = $city_b_c_pw_accepted+1;
                                $city_c_c_pw_accepted = $city_c_c_pw_accepted+$credits_accepted;
                            }
                        }
                        if($log_accepted<$today && $log_accepted>= $this_month){
                            if($city_accepted == $value_accepted){
                                $city_b_c_tm_accepted = $city_b_c_tm_accepted+1;
                                $city_c_c_tm_accepted = $city_c_c_tm_accepted+$credits_accepted;
                            }
                        }
                        if($log_accepted<$this_month && $log_accepted>= $last_month){
                            if($city_accepted == $value_accepted){
                                $city_b_c_lm_accepted = $city_b_c_lm_accepted+1;
                                $city_c_c_lm_accepted = $city_c_c_lm_accepted+$credits_accepted;
                            }
                        }
                        if($log_accepted<$this_month && $log_accepted>= $three_m_ago){
                            if($city_accepted == $value_accepted){
                                $city_b_c_l3m_accepted = $city_b_c_l3m_accepted+1;
                                $city_c_c_l3m_accepted = $city_c_c_l3m_accepted+$credits_accepted;
                            }
                        }
                    }//foreach data

                    $city_b_c_lw_accepted = $city_b_c_lw_accepted == 0 ? 0 : round($city_b_c_lw_accepted/7,1);
                    $city_b_c_pw_accepted = $city_b_c_pw_accepted == 0 ? 0 : round($city_b_c_pw_accepted/7,1);
                    $city_b_c_tm_accepted = $city_b_c_tm_accepted == 0 ? 0 : round($city_b_c_tm_accepted/$no_of_days_this_month,1);
                    $city_b_c_lm_accepted = $city_b_c_lm_accepted == 0 ? 0 : round($city_b_c_lm_accepted/$no_of_days_last_month,1);
                    $city_b_c_l3m_accepted = $city_b_c_l3m_accepted == 0 ? 0 : round($city_b_c_l3m_accepted/$no_of_days_last_3_months,1);

                    $city_c_c_lw_accepted = $city_c_c_lw_accepted == 0 ? 0 : round($city_c_c_lw_accepted/(7*100),1);
                    $city_c_c_pw_accepted = $city_c_c_pw_accepted == 0 ? 0 : round($city_c_c_pw_accepted/(7*100),1);
                    $city_c_c_tm_accepted = $city_c_c_tm_accepted == 0 ? 0 : round($city_c_c_tm_accepted/($no_of_days_this_month*100),1);
                    $city_c_c_lm_accepted = $city_c_c_lm_accepted == 0 ? 0 : round($city_c_c_lm_accepted/($no_of_days_last_month*100),1);
                    $city_c_c_l3m_accepted = $city_c_c_l3m_accepted == 0 ? 0 : round($city_c_c_l3m_accepted/($no_of_days_last_3_months*100),1);
                    
                    if($city_b_c_lw_accepted == 0 && $city_b_c_pw_accepted == 0 && $city_b_c_tm_accepted == 0 && $city_b_c_lm_accepted == 0 && $city_b_c_l3m_accepted == 0 && $city_c_c_lw_accepted == 0 && $city_c_c_pw_accepted == 0 && $city_c_c_tm_accepted == 0 && $city_c_c_lm_accepted == 0 && $city_c_c_l3m_accepted){
                        continue;
                    }

                    $city_table_accepted_g = $city_table_accepted_g.'<tr><th>'.$value_accepted.'</th><td>'.$city_b_c_lw_accepted.'</td><td>'.$city_b_c_pw_accepted.'</td><td>'.$city_b_c_tm_accepted.'</td><td>'.$city_b_c_lm_accepted.'</td><td>'.$city_b_c_l3m_accepted.'</td></tr>';
                    $city_table_accepted_c = $city_table_accepted_c.'<tr><th>'.$value_accepted.'</th><td>'.$city_c_c_lw_accepted.'</td><td>'.$city_c_c_pw_accepted.'</td><td>'.$city_c_c_tm_accepted.'</td><td>'.$city_c_c_lm_accepted.'</td><td>'.$city_c_c_l3m_accepted.'</td></tr>';

                }// for each


            $city_table_accepted_g = $city_table_accepted_g.'</tbody></table>';
            $city_table_accepted_c = $city_table_accepted_c.'</tbody></table>';


            $city_table_accepted = $city_table_accepted_g."<br>".$city_table_accepted_c;
        }
        //echo "<br>";
        //master service accepted set
        {

            $master_service_table_accepted_g = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Segment</th>
              <th colspan="5" class="list1">Avg no.of Goaxles</th>
            </tr>
            <tr class="subheader">
              <td class="list1">LastWeek</td>
              <td class="list1">PreviousWeek</td>
              <td class="list1">MonthToDate</td>
              <td class="list1">LastMonth</td>
              <td class="list1">LastThreeMonths</td>
            </tr>';
            $master_service_table_accepted_c = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Segment</th>
              <th colspan="5" class="list2">Avg Credits Acquired</th>
            </tr>
            <tr class="subheader">
              <td class="list2">LastWeek</td>
              <td class="list2">PreviousWeek</td>
              <td class="list2">MonthToDate</td>
              <td class="list2">LastMonth</td>
              <td class="list2">LastThreeMonths</td>
            </tr>';
                foreach($master_service_arr_accepted as $value_accepted){
                    $master_service_b_c_lw_accepted = 0 ;$master_service_c_c_lw_accepted = 0 ;
                    $master_service_b_c_pw_accepted = 0 ;$master_service_c_c_pw_accepted = 0 ;
                    $master_service_b_c_tm_accepted = 0 ;$master_service_c_c_tm_accepted = 0 ;
                    $master_service_b_c_lm_accepted = 0 ;$master_service_c_c_lm_accepted = 0 ; 
                    $master_service_b_c_l3m_accepted = 0 ;$master_service_c_c_l3m_accepted = 0 ;
                    foreach ($data_accepted as $key_accepted){
                        $credits_accepted = $key_accepted['credits'];
                        $master_service_accepted = $key_accepted['master_service'];
                        $log_accepted = $key_accepted['log'];
                        
                        if($log_accepted<$today && $log_accepted>= $last_week){
                            if($master_service_accepted == $value_accepted){
                                $master_service_b_c_lw_accepted = $master_service_b_c_lw_accepted+1;
                                $master_service_c_c_lw_accepted = $master_service_c_c_lw_accepted+$credits_accepted;
                            }
                        }
                        if($log_accepted<$last_week && $log_accepted>= $previous_week){
                            if($master_service_accepted == $value_accepted){
                                $master_service_b_c_pw_accepted = $master_service_b_c_pw_accepted+1;
                                $master_service_c_c_pw_accepted = $master_service_c_c_pw_accepted+$credits_accepted;
                            }
                        }
                        if($log_accepted<$today && $log_accepted>= $this_month){
                            if($master_service_accepted == $value_accepted){
                                $master_service_b_c_tm_accepted = $master_service_b_c_tm_accepted+1;
                                $master_service_c_c_tm_accepted = $master_service_c_c_tm_accepted+$credits_accepted;
                            }
                        }
                        if($log_accepted<$this_month && $log_accepted>= $last_month){
                            if($master_service_accepted == $value_accepted){
                                $master_service_b_c_lm_accepted = $master_service_b_c_lm_accepted+1;
                                $master_service_c_c_lm_accepted = $master_service_c_c_lm_accepted+$credits_accepted;
                            }
                        }
                        if($log_accepted<$this_month && $log_accepted>= $three_m_ago){
                            if($master_service_accepted == $value_accepted){
                                $master_service_b_c_l3m_accepted = $master_service_b_c_l3m_accepted+1;
                                $master_service_c_c_l3m_accepted = $master_service_c_c_l3m_accepted+$credits_accepted;
                            }
                        }
                    }//foreach data

                    $master_service_b_c_lw_accepted = $master_service_b_c_lw_accepted == 0 ? 0 : round($master_service_b_c_lw_accepted/7,1);
                    $master_service_b_c_pw_accepted = $master_service_b_c_pw_accepted == 0 ? 0 : round($master_service_b_c_pw_accepted/7,1);
                    $master_service_b_c_tm_accepted = $master_service_b_c_tm_accepted == 0 ? 0 : round($master_service_b_c_tm_accepted/$no_of_days_this_month,1);
                    $master_service_b_c_lm_accepted = $master_service_b_c_lm_accepted == 0 ? 0 : round($master_service_b_c_lm_accepted/$no_of_days_last_month,1);
                    $master_service_b_c_l3m_accepted = $master_service_b_c_l3m_accepted == 0 ? 0 : round($master_service_b_c_l3m_accepted/$no_of_days_last_3_months,1);

                    $master_service_c_c_lw_accepted = $master_service_c_c_lw_accepted == 0 ? 0 : round($master_service_c_c_lw_accepted/(7*100),1);
                    $master_service_c_c_pw_accepted = $master_service_c_c_pw_accepted == 0 ? 0 : round($master_service_c_c_pw_accepted/(7*100),1);
                    $master_service_c_c_tm_accepted = $master_service_c_c_tm_accepted == 0 ? 0 : round($master_service_c_c_tm_accepted/($no_of_days_this_month*100),1);
                    $master_service_c_c_lm_accepted = $master_service_c_c_lm_accepted == 0 ? 0 : round($master_service_c_c_lm_accepted/($no_of_days_last_month*100),1);
                    $master_service_c_c_l3m_accepted = $master_service_c_c_l3m_accepted == 0 ? 0 : round($master_service_c_c_l3m_accepted/($no_of_days_last_3_months*100),1);

                    if($master_service_b_c_lw_accepted == 0 && $master_service_b_c_pw_accepted == 0 && $master_service_b_c_tm_accepted == 0 && $master_service_b_c_lm_accepted == 0 && $master_service_b_c_l3m_accepted == 0 && $master_service_c_c_lw_accepted == 0 && $master_service_c_c_pw_accepted == 0 && $master_service_c_c_tm_accepted == 0 && $master_service_c_c_lm_accepted == 0 && $master_service_c_c_l3m_accepted == 0){
                        continue;
                    }
                    $master_service_table_accepted_g = $master_service_table_accepted_g.'<tr><th>'.$value_accepted.'</th><td>'.$master_service_b_c_lw_accepted.'</td><td>'.$master_service_b_c_pw_accepted.'</td><td>'.$master_service_b_c_tm_accepted.'</td><td>'.$master_service_b_c_lm_accepted.'</td><td>'.$master_service_b_c_l3m_accepted.'</td></tr>';
                    $master_service_table_accepted_c = $master_service_table_accepted_c.'<tr><th>'.$value_accepted.'</th><td>'.$master_service_c_c_lw_accepted.'</td><td>'.$master_service_c_c_pw_accepted.'</td><td>'.$master_service_c_c_tm_accepted.'</td><td>'.$master_service_c_c_lm_accepted.'</td><td>'.$master_service_c_c_l3m_accepted.'</td></tr>';
                }// for each


            $master_service_table_accepted_g = $master_service_table_accepted_g.'</tbody></table>';
            $master_service_table_accepted_c = $master_service_table_accepted_c.'</tbody></table>';

           $master_service_table_accepted = $master_service_table_accepted_g."<br>".$master_service_table_accepted_c;

           // echo $master_service_table_accepted;
        }
        //echo "<br>";
        //vehicle accepted set
        {
            
            $vehicle_table_accepted_g =  '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Vehicle</th>
              <th colspan="5" class="list1">Avg no.of Goaxles</th>
            </tr>
            <tr class="subheader">
              <td class="list1">LastWeek</td>
              <td class="list1">PreviousWeek</td>
              <td class="list1">MonthToDate</td>
              <td class="list1">LastMonth</td>
              <td class="list1">LastThreeMonths</td>
            </tr>';

            $vehicle_table_accepted_c =  '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Vehicle</th>
              <th colspan="5" class="list2">Avg Credits Acquired</th>
            </tr>
            <tr class="subheader">
              <td class="list2">LastWeek</td>
              <td class="list2">PreviousWeek</td>
              <td class="list2">MonthToDate</td>
              <td class="list2">LastMonth</td>
              <td class="list2">LastThreeMonths</td>
            </tr>';
                foreach($vehicle_arr_accepted as $value_accepted){
                    $vehicle_b_c_lw_accepted = 0 ;$vehicle_c_c_lw_accepted = 0 ;
                    $vehicle_b_c_pw_accepted = 0 ;$vehicle_c_c_pw_accepted = 0 ;
                    $vehicle_b_c_tm_accepted = 0 ;$vehicle_c_c_tm_accepted = 0 ;
                    $vehicle_b_c_lm_accepted = 0 ;$vehicle_c_c_lm_accepted = 0 ; 
                    $vehicle_b_c_l3m_accepted = 0 ;$vehicle_c_c_l3m_accepted = 0 ;
                    foreach ($data_accepted as $key_accepted){
                        $credits_accepted = $key_accepted['credits'];
                        $vehicle_accepted = $key_accepted['vehicle'];
                        $log_accepted = $key_accepted['log'];
                        
                        if($log_accepted<$today && $log_accepted>= $last_week){
                            if($vehicle_accepted == $value_accepted){
                                $vehicle_b_c_lw_accepted = $vehicle_b_c_lw_accepted+1;
                                $vehicle_c_c_lw_accepted = $vehicle_c_c_lw_accepted+$credits_accepted;
                            }
                        }
                        if($log_accepted<$last_week && $log_accepted>= $previous_week){
                            if($vehicle_accepted == $value_accepted){
                                $vehicle_b_c_pw_accepted = $vehicle_b_c_pw_accepted+1;
                                $vehicle_c_c_pw_accepted = $vehicle_c_c_pw_accepted+$credits_accepted;
                            }
                        }
                        if($log_accepted<$today && $log_accepted>= $this_month){
                            if($vehicle_accepted == $value_accepted){
                                $vehicle_b_c_tm_accepted = $vehicle_b_c_tm_accepted+1;
                                $vehicle_c_c_tm_accepted = $vehicle_c_c_tm_accepted+$credits_accepted;
                            }
                        }
                        if($log_accepted<$this_month && $log_accepted>= $last_month){
                            if($vehicle_accepted == $value_accepted){
                                $vehicle_b_c_lm_accepted = $vehicle_b_c_lm_accepted+1;
                                $vehicle_c_c_lm_accepted = $vehicle_c_c_lm_accepted+$credits_accepted;
                            }
                        }
                        if($log_accepted<$this_month && $log_accepted>= $three_m_ago){
                            if($vehicle_accepted == $value_accepted){
                                $vehicle_b_c_l3m_accepted = $vehicle_b_c_l3m_accepted+1;
                                $vehicle_c_c_l3m_accepted = $vehicle_c_c_l3m_accepted+$credits_accepted;
                            }
                        }
                    }//foreach data

                    $vehicle_b_c_lw_accepted = $vehicle_b_c_lw_accepted == 0 ? 0 : round($vehicle_b_c_lw_accepted/7,1);
                    $vehicle_b_c_pw_accepted = $vehicle_b_c_pw_accepted == 0 ? 0 : round($vehicle_b_c_pw_accepted/7,1);
                    $vehicle_b_c_tm_accepted = $vehicle_b_c_tm_accepted == 0 ? 0 : round($vehicle_b_c_tm_accepted/$no_of_days_this_month,1);
                    $vehicle_b_c_lm_accepted = $vehicle_b_c_lm_accepted == 0 ? 0 : round($vehicle_b_c_lm_accepted/$no_of_days_last_month,1);
                    $vehicle_b_c_l3m_accepted = $vehicle_b_c_l3m_accepted == 0 ? 0 : round($vehicle_b_c_l3m_accepted/$no_of_days_last_3_months,1);

                    $vehicle_c_c_lw_accepted = $vehicle_c_c_lw_accepted == 0 ? 0 : round($vehicle_c_c_lw_accepted/(7*100),1);
                    $vehicle_c_c_pw_accepted = $vehicle_c_c_pw_accepted == 0 ? 0 : round($vehicle_c_c_pw_accepted/(7*100),1);
                    $vehicle_c_c_tm_accepted = $vehicle_c_c_tm_accepted == 0 ? 0 : round($vehicle_c_c_tm_accepted/($no_of_days_this_month*100),1);
                    $vehicle_c_c_lm_accepted = $vehicle_c_c_lm_accepted == 0 ? 0 : round($vehicle_c_c_lm_accepted/($no_of_days_last_month*100),1);
                    $vehicle_c_c_l3m_accepted = $vehicle_c_c_l3m_accepted == 0 ? 0 : round($vehicle_c_c_l3m_accepted/($no_of_days_last_3_months*100),1);


                    if($vehicle_b_c_lw_accepted == 0 && $vehicle_b_c_pw_accepted == 0 && $vehicle_b_c_tm_accepted == 0 && $vehicle_b_c_lm_accepted == 0 && $vehicle_b_c_l3m_accepted == 0 && $vehicle_c_c_lw_accepted == 0 && $vehicle_c_c_pw_accepted == 0 && $vehicle_c_c_tm_accepted == 0 && $vehicle_c_c_lm_accepted == 0 && $vehicle_c_c_l3m_accepted == 0){
                        continue;
                    }
                    $vehicle_table_accepted_g = $vehicle_table_accepted_g.'<tr><th>'.$value_accepted.'</th><td>'.$vehicle_b_c_lw_accepted.'</td><td>'.$vehicle_b_c_pw_accepted.'</td><td>'.$vehicle_b_c_tm_accepted.'</td><td>'.$vehicle_b_c_lm_accepted.'</td><td>'.$vehicle_b_c_l3m_accepted.'</td></tr>';
                    $vehicle_table_accepted_c = $vehicle_table_accepted_c.'<tr><th>'.$value_accepted.'</th><td>'.$vehicle_c_c_lw_accepted.'</td><td>'.$vehicle_c_c_pw_accepted.'</td><td>'.$vehicle_c_c_tm_accepted.'</td><td>'.$vehicle_c_c_lm_accepted.'</td><td>'.$vehicle_c_c_l3m_accepted.'</td></tr>';

                }// for each
            $vehicle_table_accepted_g = $vehicle_table_accepted_g.'</tbody></table>';
            $vehicle_table_accepted_c = $vehicle_table_accepted_c.'</tbody></table>';

            $vehicle_table_accepted = $vehicle_table_accepted_g."<br>".$vehicle_table_accepted_c;
           //echo $vehicle_table_accepted;
        }
        //echo "<br>";
        //person accepted set
        {
        
            $person_table_accepted_g = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">CRM Person</th>
              <th rowspan="2" align="center" class="list1">City</th>
              <th colspan="5" class="list1">Avg no.of Goaxles</th>
            </tr>
            <tr class="subheader">
              <td class="list1">LastWeek</td>
              <td class="list1">PreviousWeek</td>
              <td class="list1">MonthToDate</td>
              <td class="list1">LastMonth</td>
              <td class="list1">LastThreeMonths</td>
            </tr>';

            $person_table_accepted_c = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">CRM Person</th>
              <th rowspan="2" align="center" class="list1">City</th>
              <th colspan="5" class="list2">Avg Credits Acquired</th>
            </tr>
            <tr class="subheader">
              <td class="list2">LastWeek</td>
              <td class="list2">PreviousWeek</td>
              <td class="list2">MonthToDate</td>
              <td class="list2">LastMonth</td>
              <td class="list2">LastThreeMonths</td>
            </tr>';
                foreach($person_arr_accepted as $value_accepted){
                    $person = $value_accepted['crm_name'];
                    
                    $person_b_c_lw_accepted = 0 ;$person_c_c_lw_accepted = 0 ;
                    $person_b_c_pw_accepted = 0 ;$person_c_c_pw_accepted = 0 ;
                    $person_b_c_tm_accepted = 0 ;$person_c_c_tm_accepted = 0 ;
                    $person_b_c_lm_accepted = 0 ;$person_c_c_lm_accepted = 0 ; 
                    $person_b_c_l3m_accepted = 0 ;$person_c_c_l3m_accepted = 0 ;
                    foreach ($data_accepted as $key_accepted){
                        $credits_accepted = $key_accepted['credits'];
                        $person_accepted = $key_accepted['person'];
                        $log_accepted = $key_accepted['log'];
                        if($person_accepted == $person){
                            $city_p = $value_accepted['city'] == "all" ? "India" : $value_accepted['city'];
                        }
                        
                        if($log_accepted<$today && $log_accepted>= $last_week){
                            if($person_accepted == $person){
                                $person_b_c_lw_accepted = $person_b_c_lw_accepted+1;
                                $person_c_c_lw_accepted = $person_c_c_lw_accepted+$credits_accepted;
                            }
                        }
                        if($log_accepted<$last_week && $log_accepted>= $previous_week){
                            if($person_accepted == $person){
                                $person_b_c_pw_accepted = $person_b_c_pw_accepted+1;
                                $person_c_c_pw_accepted = $person_c_c_pw_accepted+$credits_accepted;
                            }
                        }
                        if($log_accepted<$today && $log_accepted>= $this_month){
                            if($person_accepted == $person){
                                $person_b_c_tm_accepted = $person_b_c_tm_accepted+1;
                                $person_c_c_tm_accepted = $person_c_c_tm_accepted+$credits_accepted;
                            }
                        }
                        if($log_accepted<$this_month && $log_accepted>= $last_month){
                            if($person_accepted == $person){
                                $person_b_c_lm_accepted = $person_b_c_lm_accepted+1;
                                $person_c_c_lm_accepted = $person_c_c_lm_accepted+$credits_accepted;
                            }
                        }
                        if($log_accepted<$this_month && $log_accepted>= $three_m_ago){
                            if($person_accepted == $person){
                                $person_b_c_l3m_accepted = $person_b_c_l3m_accepted+1;
                                $person_c_c_l3m_accepted = $person_c_c_l3m_accepted+$credits_accepted;
                            }
                        }
                    }//foreach data

                    $person_b_c_lw_accepted = $person_b_c_lw_accepted == 0 ? 0 : round($person_b_c_lw_accepted/7,1);
                    $person_b_c_pw_accepted = $person_b_c_pw_accepted == 0 ? 0 : round($person_b_c_pw_accepted/7,1);
                    $person_b_c_tm_accepted = $person_b_c_tm_accepted == 0 ? 0 : round($person_b_c_tm_accepted/$no_of_days_this_month,1);
                    $person_b_c_lm_accepted = $person_b_c_lm_accepted == 0 ? 0 : round($person_b_c_lm_accepted/$no_of_days_last_month,1);
                    $person_b_c_l3m_accepted = $person_b_c_l3m_accepted == 0 ? 0 : round($person_b_c_l3m_accepted/$no_of_days_last_3_months,1);

                    $person_c_c_lw_accepted = $person_c_c_lw_accepted == 0 ? 0 : round($person_c_c_lw_accepted/(7*100),1);
                    $person_c_c_pw_accepted = $person_c_c_pw_accepted == 0 ? 0 : round($person_c_c_pw_accepted/(7*100),1);
                    $person_c_c_tm_accepted = $person_c_c_tm_accepted == 0 ? 0 : round($person_c_c_tm_accepted/($no_of_days_this_month*100),1);
                    $person_c_c_lm_accepted = $person_c_c_lm_accepted == 0 ? 0 : round($person_c_c_lm_accepted/($no_of_days_last_month*100),1);
                    $person_c_c_l3m_accepted = $person_c_c_l3m_accepted == 0 ? 0 : round($person_c_c_l3m_accepted/($no_of_days_last_3_months*100),1);
        
                    if($person_b_c_lw_accepted == 0 && $person_b_c_pw_accepted == 0 && $person_b_c_tm_accepted == 0 && $person_b_c_lm_accepted == 0 && $person_b_c_l3m_accepted == 0 && $person_c_c_lw_accepted == 0 && $person_c_c_pw_accepted == 0 && $person_c_c_tm_accepted == 0 && $person_c_c_lm_accepted == 0 && $person_c_c_l3m_accepted == 0){
                        continue;
                    }
                    $person_table_accepted_g = $person_table_accepted_g.'<tr><th>'.$person.'</th><td>'.$city_p.'</td><td>'.$person_b_c_lw_accepted.'</td><td>'.$person_b_c_pw_accepted.'</td><td>'.$person_b_c_tm_accepted.'</td><td>'.$person_b_c_lm_accepted.'</td><td>'.$person_b_c_l3m_accepted.'</td></tr>';
                    $person_table_accepted_c = $person_table_accepted_c.'<tr><th>'.$person.'</th><td>'.$city_p.'</td><td>'.$person_c_c_lw_accepted.'</td><td>'.$person_c_c_pw_accepted.'</td><td>'.$person_c_c_tm_accepted.'</td><td>'.$person_c_c_lm_accepted.'</td><td>'.$person_c_c_l3m_accepted.'</td></tr>';

                }// for each
        
        
            $person_table_accepted_g = $person_table_accepted_g.'</tbody></table>';
            $person_table_accepted_c = $person_table_accepted_c.'</tbody></table>';
            $person_table_accepted = $person_table_accepted_g."<br>".$person_table_accepted_c;
        }
       // echo "<h3>premium</h3><br>";
        // accepted set premium
        $city_arr_accepted_pre = array_unique($city_arr_accepted_pre);
        $master_service_arr_accepted_pre = array_unique($master_service_arr_accepted_pre);
        $vehicle_arr_accepted_pre = array_unique($vehicle_arr_accepted_pre);
        $shop_name_arr_accepted_pre = array_unique($shop_name_arr_accepted_pre);
        
        //city accepted premium set
        {
    
            $city_table_accepted_pre_g = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">City</th>
              <th colspan="5" class="list1">Avg no.of Goaxles</th>
            </tr>
            <tr class="subheader">
              <td class="list1">LastWeek</td>
              <td class="list1">PreviousWeek</td>
              <td class="list1">MonthToDate</td>
              <td class="list1">LastMonth</td>
              <td class="list1">LastThreeMonths</td>
            </tr>';
            $city_table_accepted_pre_c = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">City</th>
              <th colspan="5" class="list2">Avg Credits Acquired</th>
            </tr>
            <tr class="subheader">
              <td class="list2">LastWeek</td>
              <td class="list2">PreviousWeek</td>
              <td class="list2">MonthToDate</td>
              <td class="list2">LastMonth</td>
              <td class="list2">LastThreeMonths</td>
            </tr>';
                foreach($city_arr_accepted_pre as $value_accepted_pre){
                    $city_b_c_lw_accepted_pre = 0 ;$city_c_c_lw_accepted_pre = 0 ;
                    $city_b_c_pw_accepted_pre = 0 ;$city_c_c_pw_accepted_pre = 0 ;
                    $city_b_c_tm_accepted_pre = 0 ;$city_c_c_tm_accepted_pre = 0 ;
                    $city_b_c_lm_accepted_pre = 0 ;$city_c_c_lm_accepted_pre = 0 ; 
                    $city_b_c_l3m_accepted_pre = 0 ;$city_c_c_l3m_accepted_pre = 0 ;
                    foreach ($data_accepted_pre as $key_accepted_pre){
                        $credits_accepted_pre = $key_accepted_pre['credits'];
                        $city_accepted_pre = $key_accepted_pre['city'];
                        $log_accepted_pre = $key_accepted_pre['log'];
                        
                        if($log_accepted_pre<$today && $log_accepted_pre>= $last_week){
                            if($city_accepted_pre == $value_accepted_pre){
                                $city_b_c_lw_accepted_pre = $city_b_c_lw_accepted_pre+1;
                                $city_c_c_lw_accepted_pre = $city_c_c_lw_accepted_pre+$credits_accepted_pre;
                            }
                        }
                        if($log_accepted_pre<$last_week && $log_accepted_pre>= $previous_week){
                            if($city_accepted_pre == $value_accepted_pre){
                                $city_b_c_pw_accepted_pre = $city_b_c_pw_accepted_pre+1;
                                $city_c_c_pw_accepted_pre = $city_c_c_pw_accepted_pre+$credits_accepted_pre;
                            }
                        }
                        if($log_accepted_pre<$today && $log_accepted_pre>= $this_month){
                            if($city_accepted_pre == $value_accepted_pre){
                                $city_b_c_tm_accepted_pre = $city_b_c_tm_accepted_pre+1;
                                $city_c_c_tm_accepted_pre = $city_c_c_tm_accepted_pre+$credits_accepted_pre;
                            }
                        }
                        if($log_accepted_pre<$this_month && $log_accepted_pre>= $last_month){
                            if($city_accepted_pre == $value_accepted_pre){
                                $city_b_c_lm_accepted_pre = $city_b_c_lm_accepted_pre+1;
                                $city_c_c_lm_accepted_pre = $city_c_c_lm_accepted_pre+$credits_accepted_pre;
                            }
                        }
                        if($log_accepted_pre<$this_month && $log_accepted_pre>= $three_m_ago){
                            if($city_accepted_pre == $value_accepted_pre){
                                $city_b_c_l3m_accepted_pre = $city_b_c_l3m_accepted_pre+1;
                                $city_c_c_l3m_accepted_pre = $city_c_c_l3m_accepted_pre+$credits_accepted_pre;
                            }
                        }
                    }//foreach data

                    $city_b_c_lw_accepted_pre = $city_b_c_lw_accepted_pre == 0 ? 0 : round($city_b_c_lw_accepted_pre/7,1);
                    $city_b_c_pw_accepted_pre = $city_b_c_pw_accepted_pre == 0 ? 0 : round($city_b_c_pw_accepted_pre/7,1);
                    $city_b_c_tm_accepted_pre = $city_b_c_tm_accepted_pre == 0 ? 0 : round($city_b_c_tm_accepted_pre/$no_of_days_this_month,1);
                    $city_b_c_lm_accepted_pre = $city_b_c_lm_accepted_pre == 0 ? 0 : round($city_b_c_lm_accepted_pre/$no_of_days_last_month,1);
                    $city_b_c_l3m_accepted_pre = $city_b_c_l3m_accepted_pre == 0 ? 0 : round($city_b_c_l3m_accepted_pre/$no_of_days_last_3_months,1);

                    $city_c_c_lw_accepted_pre = $city_c_c_lw_accepted_pre == 0 ? 0 : round($city_c_c_lw_accepted_pre/(7*100),1);
                    $city_c_c_pw_accepted_pre = $city_c_c_pw_accepted_pre == 0 ? 0 : round($city_c_c_pw_accepted_pre/(7*100),1);
                    $city_c_c_tm_accepted_pre = $city_c_c_tm_accepted_pre == 0 ? 0 : round($city_c_c_tm_accepted_pre/($no_of_days_this_month*100),1);
                    $city_c_c_lm_accepted_pre = $city_c_c_lm_accepted_pre == 0 ? 0 : round($city_c_c_lm_accepted_pre/($no_of_days_last_month*100),1);
                    $city_c_c_l3m_accepted_pre = $city_c_c_l3m_accepted_pre == 0 ? 0 : round($city_c_c_l3m_accepted_pre/($no_of_days_last_3_months*100),1);

                    if($city_b_c_lw_accepted_pre == 0 && $city_b_c_pw_accepted_pre == 0 && $city_b_c_tm_accepted_pre == 0 && $city_b_c_lm_accepted_pre == 0 && $city_b_c_l3m_accepted_pre == 0 && $city_c_c_lw_accepted_pre == 0 && $city_c_c_pw_accepted_pre == 0 && $city_c_c_tm_accepted_pre == 0 && $city_c_c_lm_accepted_pre == 0 && $city_c_c_l3m_accepted_pre == 0){
                        continue;
                    }
                    $city_table_accepted_pre_g = $city_table_accepted_pre_g.'<tr><th>'.$value_accepted_pre.'</th><td>'.$city_b_c_lw_accepted_pre.'</td><td>'.$city_b_c_pw_accepted_pre.'</td><td>'.$city_b_c_tm_accepted_pre.'</td><td>'.$city_b_c_lm_accepted_pre.'</td><td>'.$city_b_c_l3m_accepted_pre.'</td></tr>';
                    $city_table_accepted_pre_c = $city_table_accepted_pre_c.'<tr><th>'.$value_accepted_pre.'</th><td>'.$city_c_c_lw_accepted_pre.'</td><td>'.$city_c_c_pw_accepted_pre.'</td><td>'.$city_c_c_tm_accepted_pre.'</td><td>'.$city_c_c_lm_accepted_pre.'</td><td>'.$city_c_c_l3m_accepted_pre.'</td></tr>';

                }// for each


            $city_table_accepted_pre_g = $city_table_accepted_pre_g.'</tbody></table>';
            $city_table_accepted_pre_c = $city_table_accepted_pre_c.'</tbody></table>';
            
            $city_table_accepted_pre =  $city_table_accepted_pre_g."<br>". $city_table_accepted_pre_c;

           // echo $city_table_accepted_pre;
        }
        //echo "<br>";
        //master service accepted pre set
        {

            $master_service_table_accepted_pre_g = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Segment</th>
              <th colspan="5" class="list1">Avg no.of Goaxles</th>
            </tr>
            <tr class="subheader">
              <td class="list1">LastWeek</td>
              <td class="list1">PreviousWeek</td>
              <td class="list1">MonthToDate</td>
              <td class="list1">LastMonth</td>
              <td class="list1">LastThreeMonths</td>
            </tr>';
            $master_service_table_accepted_pre_c = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Segment</th>
              <th colspan="5" class="list2">Avg Credits Acquired</th>
            </tr>
            <tr class="subheader">
              <td class="list2">LastWeek</td>
              <td class="list2">PreviousWeek</td>
              <td class="list2">MonthToDate</td>
              <td class="list2">LastMonth</td>
              <td class="list2">LastThreeMonths</td>
            </tr>';
                foreach($master_service_arr_accepted_pre as $value_accepted_pre){
                    $master_service_b_c_lw_accepted_pre = 0 ;$master_service_c_c_lw_accepted_pre = 0 ;
                    $master_service_b_c_pw_accepted_pre = 0 ;$master_service_c_c_pw_accepted_pre = 0 ;
                    $master_service_b_c_tm_accepted_pre = 0 ;$master_service_c_c_tm_accepted_pre = 0 ;
                    $master_service_b_c_lm_accepted_pre = 0 ;$master_service_c_c_lm_accepted_pre = 0 ; 
                    $master_service_b_c_l3m_accepted_pre = 0 ;$master_service_c_c_l3m_accepted_pre = 0 ;
                    foreach ($data_accepted_pre as $key_accepted_pre){
                        $credits_accepted_pre = $key_accepted_pre['credits'];
                        $master_service_accepted_pre = $key_accepted_pre['master_service'];
                        $log_accepted_pre = $key_accepted_pre['log'];
                        
                        if($log_accepted_pre<$today && $log_accepted_pre>= $last_week){
                            if($master_service_accepted_pre == $value_accepted_pre){
                                $master_service_b_c_lw_accepted_pre = $master_service_b_c_lw_accepted_pre+1;
                                $master_service_c_c_lw_accepted_pre = $master_service_c_c_lw_accepted_pre+$credits_accepted_pre;
                            }
                        }
                        if($log_accepted_pre<$last_week && $log_accepted_pre>= $previous_week){
                            if($master_service_accepted_pre == $value_accepted_pre){
                                $master_service_b_c_pw_accepted_pre = $master_service_b_c_pw_accepted_pre+1;
                                $master_service_c_c_pw_accepted_pre = $master_service_c_c_pw_accepted_pre+$credits_accepted_pre;
                            }
                        }
                        if($log_accepted_pre<$today && $log_accepted_pre>= $this_month){
                            if($master_service_accepted_pre == $value_accepted_pre){
                                $master_service_b_c_tm_accepted_pre = $master_service_b_c_tm_accepted_pre+1;
                                $master_service_c_c_tm_accepted_pre = $master_service_c_c_tm_accepted_pre+$credits_accepted_pre;
                            }
                        }
                        if($log_accepted_pre<$this_month && $log_accepted_pre>= $last_month){
                            if($master_service_accepted_pre == $value_accepted_pre){
                                $master_service_b_c_lm_accepted_pre = $master_service_b_c_lm_accepted_pre+1;
                                $master_service_c_c_lm_accepted_pre = $master_service_c_c_lm_accepted_pre+$credits_accepted_pre;
                            }
                        }
                        if($log_accepted_pre<$this_month && $log_accepted_pre>= $three_m_ago){
                            if($master_service_accepted_pre == $value_accepted_pre){
                                $master_service_b_c_l3m_accepted_pre = $master_service_b_c_l3m_accepted_pre+1;
                                $master_service_c_c_l3m_accepted_pre = $master_service_c_c_l3m_accepted_pre+$credits_accepted_pre;
                            }
                        }
                    }//foreach data

                    $master_service_b_c_lw_accepted_pre = $master_service_b_c_lw_accepted_pre == 0 ? 0 : round($master_service_b_c_lw_accepted_pre/7,1);
                    $master_service_b_c_pw_accepted_pre = $master_service_b_c_pw_accepted_pre == 0 ? 0 : round($master_service_b_c_pw_accepted_pre/7,1);
                    $master_service_b_c_tm_accepted_pre = $master_service_b_c_tm_accepted_pre == 0 ? 0 : round($master_service_b_c_tm_accepted_pre/$no_of_days_this_month,1);
                    $master_service_b_c_lm_accepted_pre = $master_service_b_c_lm_accepted_pre == 0 ? 0 : round($master_service_b_c_lm_accepted_pre/$no_of_days_last_month,1);
                    $master_service_b_c_l3m_accepted_pre = $master_service_b_c_l3m_accepted_pre == 0 ? 0 : round($master_service_b_c_l3m_accepted_pre/$no_of_days_last_3_months,1);

                    $master_service_c_c_lw_accepted_pre = $master_service_c_c_lw_accepted_pre == 0 ? 0 : round($master_service_c_c_lw_accepted_pre/(7*100),1);
                    $master_service_c_c_pw_accepted_pre = $master_service_c_c_pw_accepted_pre == 0 ? 0 : round($master_service_c_c_pw_accepted_pre/(7*100),1);
                    $master_service_c_c_tm_accepted_pre = $master_service_c_c_tm_accepted_pre == 0 ? 0 : round($master_service_c_c_tm_accepted_pre/($no_of_days_this_month*100),1);
                    $master_service_c_c_lm_accepted_pre = $master_service_c_c_lm_accepted_pre == 0 ? 0 : round($master_service_c_c_lm_accepted_pre/($no_of_days_last_month*100),1);
                    $master_service_c_c_l3m_accepted_pre = $master_service_c_c_l3m_accepted_pre == 0 ? 0 : round($master_service_c_c_l3m_accepted_pre/($no_of_days_last_3_months*100),1);
                   
                    if($master_service_b_c_lw_accepted_pre == 0 && $master_service_b_c_pw_accepted_pre == 0 && $master_service_b_c_tm_accepted_pre == 0 && $master_service_b_c_lm_accepted_pre == 0 && $master_service_b_c_l3m_accepted_pre == 0 && $master_service_c_c_lw_accepted_pre == 0 && $master_service_c_c_pw_accepted_pre == 0 && $master_service_c_c_tm_accepted_pre == 0 && $master_service_c_c_lm_accepted_pre == 0 && $master_service_c_c_l3m_accepted_pre == 0){
                        continue;
                    }
                    $master_service_table_accepted_pre_g = $master_service_table_accepted_pre_g.'<tr><th>'.$value_accepted_pre.'</th><td>'.$master_service_b_c_lw_accepted_pre.'</td><td>'.$master_service_b_c_pw_accepted_pre.'</td><td>'.$master_service_b_c_tm_accepted_pre.'</td><td>'.$master_service_b_c_lm_accepted_pre.'</td><td>'.$master_service_b_c_l3m_accepted_pre.'</td></tr>';
                    $master_service_table_accepted_pre_c = $master_service_table_accepted_pre_c.'<tr><th>'.$value_accepted_pre.'</th><td>'.$master_service_c_c_lw_accepted_pre.'</td><td>'.$master_service_c_c_pw_accepted_pre.'</td><td>'.$master_service_c_c_tm_accepted_pre.'</td><td>'.$master_service_c_c_lm_accepted_pre.'</td><td>'.$master_service_c_c_l3m_accepted_pre.'</td></tr>';

                }// for each


            $master_service_table_accepted_pre_g = $master_service_table_accepted_pre_g.'</tbody></table>';
            $master_service_table_accepted_pre_c = $master_service_table_accepted_pre_c.'</tbody></table>';
            $master_service_table_accepted_pre = $master_service_table_accepted_pre_g."<br>".$master_service_table_accepted_pre_c;
        }
       // echo "<br>";
        //vehicle accepted_pre set
        {
            
            $vehicle_table_accepted_pre_g = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Vehicle</th>
              <th colspan="5" class="list1">Avg no.of Goaxles</th>
            </tr>
            <tr class="subheader">
              <td class="list1">LastWeek</td>
              <td class="list1">PreviousWeek</td>
              <td class="list1">MonthToDate</td>
              <td class="list1">LastMonth</td>
              <td class="list1">LastThreeMonths</td>
            </tr>';
            $vehicle_table_accepted_pre_c = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Vehicle</th>
              <th colspan="5" class="list2">Avg Credits Acquired</th>
            </tr>
            <tr class="subheader">
              <td class="list2">LastWeek</td>
              <td class="list2">PreviousWeek</td>
              <td class="list2">MonthToDate</td>
              <td class="list2">LastMonth</td>
              <td class="list2">LastThreeMonths</td>
            </tr>';
                foreach($vehicle_arr_accepted_pre as $value_accepted_pre){
                    $vehicle_b_c_lw_accepted_pre = 0 ;$vehicle_c_c_lw_accepted_pre = 0 ;
                    $vehicle_b_c_pw_accepted_pre = 0 ;$vehicle_c_c_pw_accepted_pre = 0 ;
                    $vehicle_b_c_tm_accepted_pre = 0 ;$vehicle_c_c_tm_accepted_pre = 0 ;
                    $vehicle_b_c_lm_accepted_pre = 0 ;$vehicle_c_c_lm_accepted_pre = 0 ; 
                    $vehicle_b_c_l3m_accepted_pre = 0 ;$vehicle_c_c_l3m_accepted_pre = 0 ;
                    foreach ($data_accepted_pre as $key_accepted_pre){
                        $credits_accepted_pre = $key_accepted_pre['credits'];
                        $vehicle_accepted_pre = $key_accepted_pre['vehicle'];
                        $log_accepted_pre = $key_accepted_pre['log'];
                        
                        if($log_accepted_pre<$today && $log_accepted_pre>= $last_week){
                            if($vehicle_accepted_pre == $value_accepted_pre){
                                $vehicle_b_c_lw_accepted_pre = $vehicle_b_c_lw_accepted_pre+1;
                                $vehicle_c_c_lw_accepted_pre = $vehicle_c_c_lw_accepted_pre+$credits_accepted_pre;
                            }
                        }
                        if($log_accepted_pre<$last_week && $log_accepted_pre>= $previous_week){
                            if($vehicle_accepted_pre == $value_accepted_pre){
                                $vehicle_b_c_pw_accepted_pre = $vehicle_b_c_pw_accepted_pre+1;
                                $vehicle_c_c_pw_accepted_pre = $vehicle_c_c_pw_accepted_pre+$credits_accepted_pre;
                            }
                        }
                        if($log_accepted_pre<$today && $log_accepted_pre>= $this_month){
                            if($vehicle_accepted_pre == $value_accepted_pre){
                                $vehicle_b_c_tm_accepted_pre = $vehicle_b_c_tm_accepted_pre+1;
                                $vehicle_c_c_tm_accepted_pre = $vehicle_c_c_tm_accepted_pre+$credits_accepted_pre;
                            }
                        }
                        if($log_accepted_pre<$this_month && $log_accepted_pre>= $last_month){
                            if($vehicle_accepted_pre == $value_accepted_pre){
                                $vehicle_b_c_lm_accepted_pre = $vehicle_b_c_lm_accepted_pre+1;
                                $vehicle_c_c_lm_accepted_pre = $vehicle_c_c_lm_accepted_pre+$credits_accepted_pre;
                            }
                        }
                        if($log_accepted_pre<$this_month && $log_accepted_pre>= $three_m_ago){
                            if($vehicle_accepted_pre == $value_accepted_pre){
                                $vehicle_b_c_l3m_accepted_pre = $vehicle_b_c_l3m_accepted_pre+1;
                                $vehicle_c_c_l3m_accepted_pre = $vehicle_c_c_l3m_accepted_pre+$credits_accepted_pre;
                            }
                        }
                    }//foreach data

                    $vehicle_b_c_lw_accepted_pre = $vehicle_b_c_lw_accepted_pre == 0 ? 0 : round($vehicle_b_c_lw_accepted_pre/7,1);
                    $vehicle_b_c_pw_accepted_pre = $vehicle_b_c_pw_accepted_pre == 0 ? 0 : round($vehicle_b_c_pw_accepted_pre/7,1);
                    $vehicle_b_c_tm_accepted_pre = $vehicle_b_c_tm_accepted_pre == 0 ? 0 : round($vehicle_b_c_tm_accepted_pre/$no_of_days_this_month,1);
                    $vehicle_b_c_lm_accepted_pre = $vehicle_b_c_lm_accepted_pre == 0 ? 0 : round($vehicle_b_c_lm_accepted_pre/$no_of_days_last_month,1);
                    $vehicle_b_c_l3m_accepted_pre = $vehicle_b_c_l3m_accepted_pre == 0 ? 0 : round($vehicle_b_c_l3m_accepted_pre/$no_of_days_last_3_months,1);

                    $vehicle_c_c_lw_accepted_pre = $vehicle_c_c_lw_accepted_pre == 0 ? 0 : round($vehicle_c_c_lw_accepted_pre/(7*100),1);
                    $vehicle_c_c_pw_accepted_pre = $vehicle_c_c_pw_accepted_pre == 0 ? 0 : round($vehicle_c_c_pw_accepted_pre/(7*100),1);
                    $vehicle_c_c_tm_accepted_pre = $vehicle_c_c_tm_accepted_pre == 0 ? 0 : round($vehicle_c_c_tm_accepted_pre/($no_of_days_this_month*100),1);
                    $vehicle_c_c_lm_accepted_pre = $vehicle_c_c_lm_accepted_pre == 0 ? 0 : round($vehicle_c_c_lm_accepted_pre/($no_of_days_last_month*100),1);
                    $vehicle_c_c_l3m_accepted_pre = $vehicle_c_c_l3m_accepted_pre == 0 ? 0 : round($vehicle_c_c_l3m_accepted_pre/($no_of_days_last_3_months*100),1);

                    if($vehicle_b_c_lw_accepted_pre == 0 && $vehicle_b_c_pw_accepted_pre == 0 && $vehicle_b_c_tm_accepted_pre == 0 && $vehicle_b_c_lm_accepted_pre == 0 && $vehicle_b_c_l3m_accepted_pre == 0 && $vehicle_c_c_lw_accepted_pre == 0 && $vehicle_c_c_pw_accepted_pre == 0 && $vehicle_c_c_tm_accepted_pre == 0 && $vehicle_c_c_lm_accepted_pre == 0 && $vehicle_c_c_l3m_accepted_pre == 0){
                        continue;
                    }
                    $vehicle_table_accepted_pre_g = $vehicle_table_accepted_pre_g.'<tr><th>'.$value_accepted_pre.'</th><td>'.$vehicle_b_c_lw_accepted_pre.'</td><td>'.$vehicle_b_c_pw_accepted_pre.'</td><td>'.$vehicle_b_c_tm_accepted_pre.'</td><td>'.$vehicle_b_c_lm_accepted_pre.'</td><td>'.$vehicle_b_c_l3m_accepted_pre.'</td></tr>';
                    $vehicle_table_accepted_pre_c = $vehicle_table_accepted_pre_c.'<tr><th>'.$value_accepted_pre.'</th><td>'.$vehicle_c_c_lw_accepted_pre.'</td><td>'.$vehicle_c_c_pw_accepted_pre.'</td><td>'.$vehicle_c_c_tm_accepted_pre.'</td><td>'.$vehicle_c_c_lm_accepted_pre.'</td><td>'.$vehicle_c_c_l3m_accepted_pre.'</td></tr>';

                }// for each
            $vehicle_table_accepted_pre_g = $vehicle_table_accepted_pre_g.'</tbody></table>';
            $vehicle_table_accepted_pre_c = $vehicle_table_accepted_pre_c.'</tbody></table>';
            $vehicle_table_accepted_pre = $vehicle_table_accepted_pre_g."<br>".$vehicle_table_accepted_pre_c;
        }
       // echo "<br>";
        //shop_name accepted set
        {
        
            $shop_name_table_accepted_pre_g = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Shop Name</th>
              <th rowspan="2" align="center" class="list1">Type</th>
              <th rowspan="2" align="center" class="list1">City</th>
              <th colspan="5" class="list1">Avg no.of Goaxles</th>
            </tr>
            <tr class="subheader">
              <td class="list1">LastWeek</td>
              <td class="list1">PreviousWeek</td>
              <td class="list1">MonthToDate</td>
              <td class="list1">LastMonth</td>
              <td class="list1">LastThreeMonths</td>
            </tr>';
            $shop_name_table_accepted_pre_c = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Shop Name</th>
              <th rowspan="2" align="center" class="list1">Type</th>
              <th rowspan="2" align="center" class="list1">City</th>
              <th colspan="5" class="list2">Avg Credits Acquired</th>
            </tr>
            <tr class="subheader">
              <td class="list2">LastWeek</td>
              <td class="list2">PreviousWeek</td>
              <td class="list2">MonthToDate</td>
              <td class="list2">LastMonth</td>
              <td class="list2">LastThreeMonths</td>
            </tr>';
                foreach($shop_name_arr_accepted_pre as $value_accepted_pre){
                    $shop_name_b_c_lw_accepted_pre = 0 ;$shop_name_c_c_lw_accepted_pre = 0 ;
                    $shop_name_b_c_pw_accepted_pre = 0 ;$shop_name_c_c_pw_accepted_pre = 0 ;
                    $shop_name_b_c_tm_accepted_pre = 0 ;$shop_name_c_c_tm_accepted_pre = 0 ;
                    $shop_name_b_c_lm_accepted_pre = 0 ;$shop_name_c_c_lm_accepted_pre = 0 ; 
                    $shop_name_b_c_l3m_accepted_pre = 0 ;$shop_name_c_c_l3m_accepted_pre = 0 ;
                    foreach ($data_accepted_pre as $key_accepted_pre){
                        $credits_accepted_pre = $key_accepted_pre['credits'];
                        $shop_name_accepted_pre1 = $key_accepted_pre['shop_name'];
                        $log_accepted_pre = $key_accepted_pre['log'];

                        if($shop_name_accepted_pre1 == $value_accepted_pre){
                            $vehicle_accepted_pre = $key_accepted_pre['vehicle'];
                            $city_accepted_pre = $key_accepted_pre['city'];
                        }
                        
                        if($log_accepted_pre<$today && $log_accepted_pre>= $last_week){
                            if($shop_name_accepted_pre1 == $value_accepted_pre){
                                $shop_name_b_c_lw_accepted_pre = $shop_name_b_c_lw_accepted_pre+1;
                                $shop_name_c_c_lw_accepted_pre = $shop_name_c_c_lw_accepted_pre+$credits_accepted_pre;
                            }
                        }
                        if($log_accepted_pre<$last_week && $log_accepted_pre>= $previous_week){
                            if($shop_name_accepted_pre1 == $value_accepted_pre){
                                $shop_name_b_c_pw_accepted_pre = $shop_name_b_c_pw_accepted_pre+1;
                                $shop_name_c_c_pw_accepted_pre = $shop_name_c_c_pw_accepted_pre+$credits_accepted_pre;
                            }
                        }
                        if($log_accepted_pre<$today && $log_accepted_pre>= $this_month){
                            if($shop_name_accepted_pre1 == $value_accepted_pre){
                                $shop_name_b_c_tm_accepted_pre = $shop_name_b_c_tm_accepted_pre+1;
                                $shop_name_c_c_tm_accepted_pre = $shop_name_c_c_tm_accepted_pre+$credits_accepted_pre;
                            }
                        }
                        if($log_accepted_pre<$this_month && $log_accepted_pre>= $last_month){
                            if($shop_name_accepted_pre1 == $value_accepted_pre){
                                $shop_name_b_c_lm_accepted_pre = $shop_name_b_c_lm_accepted_pre+1;
                                $shop_name_c_c_lm_accepted_pre = $shop_name_c_c_lm_accepted_pre+$credits_accepted_pre;
                            }
                        }
                        if($log_accepted_pre<$this_month && $log_accepted_pre>= $three_m_ago){
                            if($shop_name_accepted_pre1 == $value_accepted_pre){
                                $shop_name_b_c_l3m_accepted_pre = $shop_name_b_c_l3m_accepted_pre+1;
                                $shop_name_c_c_l3m_accepted_pre = $shop_name_c_c_l3m_accepted_pre+$credits_accepted_pre;
                            }
                        }
                    }//foreach data

                    $shop_name_b_c_lw_accepted_pre = $shop_name_b_c_lw_accepted_pre == 0 ? 0 : round($shop_name_b_c_lw_accepted_pre/7,1);
                    $shop_name_b_c_pw_accepted_pre = $shop_name_b_c_pw_accepted_pre == 0 ? 0 : round($shop_name_b_c_pw_accepted_pre/7,1);
                    $shop_name_b_c_tm_accepted_pre = $shop_name_b_c_tm_accepted_pre == 0 ? 0 : round($shop_name_b_c_tm_accepted_pre/$no_of_days_this_month,1);
                    $shop_name_b_c_lm_accepted_pre = $shop_name_b_c_lm_accepted_pre == 0 ? 0 : round($shop_name_b_c_lm_accepted_pre/$no_of_days_last_month,1);
                    $shop_name_b_c_l3m_accepted_pre = $shop_name_b_c_l3m_accepted_pre == 0 ? 0 : round($shop_name_b_c_l3m_accepted_pre/$no_of_days_last_3_months,1);

                    $shop_name_c_c_lw_accepted_pre = $shop_name_c_c_lw_accepted_pre == 0 ? 0 : round($shop_name_c_c_lw_accepted_pre/(7*100),1);
                    $shop_name_c_c_pw_accepted_pre = $shop_name_c_c_pw_accepted_pre == 0 ? 0 : round($shop_name_c_c_pw_accepted_pre/(7*100),1);
                    $shop_name_c_c_tm_accepted_pre = $shop_name_c_c_tm_accepted_pre == 0 ? 0 : round($shop_name_c_c_tm_accepted_pre/($no_of_days_this_month*100),1);
                    $shop_name_c_c_lm_accepted_pre = $shop_name_c_c_lm_accepted_pre == 0 ? 0 : round($shop_name_c_c_lm_accepted_pre/($no_of_days_last_month*100),1);
                    $shop_name_c_c_l3m_accepted_pre = $shop_name_c_c_l3m_accepted_pre == 0 ? 0 : round($shop_name_c_c_l3m_accepted_pre/($no_of_days_last_3_months*100),1);
        
                    if($shop_name_b_c_lw_accepted_pre == 0 && $shop_name_b_c_pw_accepted_pre == 0 && $shop_name_b_c_tm_accepted_pre == 0 && $shop_name_b_c_lm_accepted_pre == 0 && $shop_name_b_c_l3m_accepted_pre == 0 && $shop_name_c_c_lw_accepted_pre == 0 && $shop_name_c_c_pw_accepted_pre == 0 && $shop_name_c_c_tm_accepted_pre == 0 && $shop_name_c_c_lm_accepted_pre == 0 && $shop_name_c_c_l3m_accepted_pre == 0){
                        continue;
                    }

                    $shop_name_table_accepted_pre_g = $shop_name_table_accepted_pre_g.'<tr><th>'.$value_accepted_pre.'</th><td>'.$vehicle_accepted_pre.'</td><td>'.$city_accepted_pre.'</td><td>'.$shop_name_b_c_lw_accepted_pre.'</td><td>'.$shop_name_b_c_pw_accepted_pre.'</td><td>'.$shop_name_b_c_tm_accepted_pre.'</td><td>'.$shop_name_b_c_lm_accepted_pre.'</td><td>'.$shop_name_b_c_l3m_accepted_pre.'</td></tr>';
                    $shop_name_table_accepted_pre_c = $shop_name_table_accepted_pre_c.'<tr><th>'.$value_accepted_pre.'</th><td>'.$vehicle_accepted_pre.'</td><td>'.$city_accepted_pre.'</td><td>'.$shop_name_c_c_lw_accepted_pre.'</td><td>'.$shop_name_c_c_pw_accepted_pre.'</td><td>'.$shop_name_c_c_tm_accepted_pre.'</td><td>'.$shop_name_c_c_lm_accepted_pre.'</td><td>'.$shop_name_c_c_l3m_accepted_pre.'</td></tr>';
       
                }// for each
        
        
            $shop_name_table_accepted_pre_g = $shop_name_table_accepted_pre_g.'</tbody></table>';
            $shop_name_table_accepted_pre_c = $shop_name_table_accepted_pre_c.'</tbody></table>';
            $shop_name_table_accepted_pre = $shop_name_table_accepted_pre_g."<br>".$shop_name_table_accepted_pre_c;
        }
        
        /*
        // rejected set
       // echo "<h3>Rejected set</h3>";
        $city_arr_rejected = array_unique($city_arr_rejected);
        $master_service_arr_rejected = array_unique($master_service_arr_rejected);
        $vehicle_arr_rejected = array_unique($vehicle_arr_rejected);
        $shop_name_arr_rejected = array_unique($shop_name_arr_rejected);

        //city rejected set
        {

            $city_table_rejected_g = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">City</th>
              <th colspan="5" class="list1">Avg no.of Goaxles</th>
            </tr>
            <tr class="subheader">
              <td class="list1">LastWeek</td>
              <td class="list1">PreviousWeek</td>
              <td class="list1">MonthToDate</td>
              <td class="list1">LastMonth</td>
              <td class="list1">LastThreeMonths</td>
            </tr>';
            $city_table_rejected_c = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">City</th>
              <th colspan="5" class="list2">Avg Credits Acquired</th>
            </tr>
            <tr class="subheader">
              <td class="list2">LastWeek</td>
              <td class="list2">PreviousWeek</td>
              <td class="list2">MonthToDate</td>
              <td class="list2">LastMonth</td>
              <td class="list2">LastThreeMonths</td>
            </tr>';
                foreach($city_arr_rejected as $value_rejected){
                    $city_b_c_lw_rejected = 0 ;$city_c_c_lw_rejected = 0 ;
                    $city_b_c_pw_rejected = 0 ;$city_c_c_pw_rejected = 0 ;
                    $city_b_c_tm_rejected = 0 ;$city_c_c_tm_rejected = 0 ;
                    $city_b_c_lm_rejected = 0 ;$city_c_c_lm_rejected = 0 ;
                    $city_b_c_l3m_rejected = 0 ;$city_c_c_l3m_rejected = 0 ;
                    foreach ($data_rejected as $key_rejected){
                        $credits_rejected = $key_rejected['credits'];
                        $city_rejected = $key_rejected['city'];
                        $log_rejected = $key_rejected['log'];
                        
                        if($log_rejected<$today && $log_rejected>= $last_week){
                            if($city_rejected == $value_rejected){
                                $city_b_c_lw_rejected = $city_b_c_lw_rejected+1;
                                $city_c_c_lw_rejected = $city_c_c_lw_rejected+$credits_rejected;
                            }
                        }
                        if($log_rejected<$last_week && $log_rejected>= $previous_week){
                            if($city_rejected == $value_rejected){
                                $city_b_c_pw_rejected = $city_b_c_pw_rejected+1;
                                $city_c_c_pw_rejected = $city_c_c_pw_rejected+$credits_rejected;
                            }
                        }
                        if($log_rejected<$today && $log_rejected>= $this_month){
                            if($city_rejected == $value_rejected){
                                $city_b_c_tm_rejected = $city_b_c_tm_rejected+1;
                                $city_c_c_tm_rejected = $city_c_c_tm_rejected+$credits_rejected;
                            }
                        }
                        if($log_rejected<$this_month && $log_rejected>= $last_month){
                            if($city_rejected == $value_rejected){
                                $city_b_c_lm_rejected = $city_b_c_lm_rejected+1;
                                $city_c_c_lm_rejected = $city_c_c_lm_rejected+$credits_rejected;
                            }
                        }
                        if($log_rejected<$this_month && $log_rejected>= $three_m_ago){
                            if($city_rejected == $value_rejected){
                                $city_b_c_l3m_rejected = $city_b_c_l3m_rejected+1;
                                $city_c_c_l3m_rejected = $city_c_c_l3m_rejected+$credits_rejected;
                            }
                        }
                    }//foreach data

                    
                    //$city_b_c_lw_rejected = $city_b_c_lw_rejected == 0 ? 0 : round($city_b_c_lw_rejected/7,1);
                    //$city_b_c_pw_rejected = $city_b_c_pw_rejected == 0 ? 0 : round($city_b_c_pw_rejected/7,1);
                    //$city_b_c_tm_rejected = $city_b_c_tm_rejected == 0 ? 0 : round($city_b_c_tm_rejected/$no_of_days_this_month,1);
                    //$city_b_c_lm_rejected = $city_b_c_lm_rejected == 0 ? 0 : round($city_b_c_lm_rejected/$no_of_days_last_month,1);
                    //$city_b_c_l3m_rejected = $city_b_c_l3m_rejected == 0 ? 0 : round($city_b_c_l3m_rejected/$no_of_days_last_3_months,1);

                    // $city_c_c_lw_rejected = $city_c_c_lw_rejected == 0 ? 0 : round($city_c_c_lw_rejected/(7*100),1);
                    // $city_c_c_pw_rejected = $city_c_c_pw_rejected == 0 ? 0 : round($city_c_c_pw_rejected/(7*100),1);
                    // $city_c_c_tm_rejected = $city_c_c_tm_rejected == 0 ? 0 : round($city_c_c_tm_rejected/($no_of_days_this_month*100),1);
                    // $city_c_c_lm_rejected = $city_c_c_lm_rejected == 0 ? 0 : round($city_c_c_lm_rejected/($no_of_days_last_month*100),1);
                    // $city_c_c_l3m_rejected = $city_c_c_l3m_rejected == 0 ? 0 : round($city_c_c_l3m_rejected/($no_of_days_last_3_months*100),1);
                    

                    $city_c_c_lw_rejected = $city_c_c_lw_rejected /100;
                    $city_c_c_pw_rejected = $city_c_c_pw_rejected /100;
                    $city_c_c_tm_rejected = $city_c_c_tm_rejected /100;
                    $city_c_c_lm_rejected = $city_c_c_lm_rejected /100;
                    $city_c_c_l3m_rejected = $city_c_c_l3m_rejected /100;

                    if($city_b_c_lw_rejected == 0 && $city_b_c_pw_rejected == 0 && $city_b_c_tm_rejected == 0 && $city_b_c_lm_rejected == 0 && $city_b_c_l3m_rejected == 0 && $city_c_c_lw_rejected == 0 && $city_c_c_pw_rejected == 0 && $city_c_c_tm_rejected == 0 && $city_c_c_lm_rejected == 0 && $city_c_c_l3m_rejected == 0){
                        continue;
                    }
                    $city_table_rejected_g = $city_table_rejected_g.'<tr><th>'.$value_rejected.'</th><td>'.$city_b_c_lw_rejected.'</td><td>'.$city_b_c_pw_rejected.'</td><td>'.$city_b_c_tm_rejected.'</td><td>'.$city_b_c_lm_rejected.'</td><td>'.$city_b_c_l3m_rejected.'</td></tr>';
                    $city_table_rejected_c = $city_table_rejected_c.'<tr><th>'.$value_rejected.'</th><td>'.$city_c_c_lw_rejected.'</td><td>'.$city_c_c_pw_rejected.'</td><td>'.$city_c_c_tm_rejected.'</td><td>'.$city_c_c_lm_rejected.'</td><td>'.$city_c_c_l3m_rejected.'</td></tr>';

                }// for each


            $city_table_rejected_g = $city_table_rejected_g.'</tbody></table>';
            $city_table_rejected_c = $city_table_rejected_c.'</tbody></table>';
            $city_table_rejected = $city_table_rejected_g."<br>".$city_table_rejected_c;
        }
        //master service rejected set
        {

            $master_service_table_rejected_g = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Segment</th>
              <th colspan="5" class="list1">Avg no.of Goaxles</th>
            </tr>
            <tr class="subheader">
              <td class="list1">LastWeek</td>
              <td class="list1">PreviousWeek</td>
              <td class="list1">MonthToDate</td>
              <td class="list1">LastMonth</td>
              <td class="list1">LastThreeMonths</td>
            </tr>';
            $master_service_table_rejected_c = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Segment</th>
              <th colspan="5" class="list2">Avg Credits Acquired</th>
            </tr>
            <tr class="subheader">
              <td class="list2">LastWeek</td>
              <td class="list2">PreviousWeek</td>
              <td class="list2">MonthToDate</td>
              <td class="list2">LastMonth</td>
              <td class="list2">LastThreeMonths</td>
            </tr>';
                foreach($master_service_arr_rejected as $value_rejected){
                    $master_service_b_c_lw_rejected = 0 ;$master_service_c_c_lw_rejected = 0 ;
                    $master_service_b_c_pw_rejected = 0 ;$master_service_c_c_pw_rejected = 0 ;
                    $master_service_b_c_tm_rejected = 0 ;$master_service_c_c_tm_rejected = 0 ;
                    $master_service_b_c_lm_rejected = 0 ;$master_service_c_c_lm_rejected = 0 ; 
                    $master_service_b_c_l3m_rejected = 0 ;$master_service_c_c_l3m_rejected = 0 ;
                    foreach ($data_rejected as $key_rejected){
                        $credits_rejected = $key_rejected['credits'];
                        $master_service_rejected = $key_rejected['master_service'];
                        $log_rejected = $key_rejected['log'];
                        
                        if($log_rejected<$today && $log_rejected>= $last_week){
                            if($master_service_rejected == $value_rejected){
                                $master_service_b_c_lw_rejected = $master_service_b_c_lw_rejected+1;
                                $master_service_c_c_lw_rejected = $master_service_c_c_lw_rejected+$credits_rejected;
                            }
                        }
                        if($log_rejected<$last_week && $log_rejected>= $previous_week){
                            if($master_service_rejected == $value_rejected){
                                $master_service_b_c_pw_rejected = $master_service_b_c_pw_rejected+1;
                                $master_service_c_c_pw_rejected = $master_service_c_c_pw_rejected+$credits_rejected;
                            }
                        }
                        if($log_rejected<$today && $log_rejected>= $this_month){
                            if($master_service_rejected == $value_rejected){
                                $master_service_b_c_tm_rejected = $master_service_b_c_tm_rejected+1;
                                $master_service_c_c_tm_rejected = $master_service_c_c_tm_rejected+$credits_rejected;
                            }
                        }
                        if($log_rejected<$this_month && $log_rejected>= $last_month){
                            if($master_service_rejected == $value_rejected){
                                $master_service_b_c_lm_rejected = $master_service_b_c_lm_rejected+1;
                                $master_service_c_c_lm_rejected = $master_service_c_c_lm_rejected+$credits_rejected;
                            }
                        }
                        if($log_rejected<$this_month && $log_rejected>= $three_m_ago){
                            if($master_service_rejected == $value_rejected){
                                $master_service_b_c_l3m_rejected = $master_service_b_c_l3m_rejected+1;
                                $master_service_c_c_l3m_rejected = $master_service_c_c_l3m_rejected+$credits_rejected;
                            }
                        }
                    }//foreach data

                    
                    // $master_service_b_c_lw_rejected = $master_service_b_c_lw_rejected == 0 ? 0 : round($master_service_b_c_lw_rejected/7,1);
                    // $master_service_b_c_pw_rejected = $master_service_b_c_pw_rejected == 0 ? 0 : round($master_service_b_c_pw_rejected/7,1);
                    // $master_service_b_c_tm_rejected = $master_service_b_c_tm_rejected == 0 ? 0 : round($master_service_b_c_tm_rejected/$no_of_days_this_month,1);
                    // $master_service_b_c_lm_rejected = $master_service_b_c_lm_rejected == 0 ? 0 : round($master_service_b_c_lm_rejected/$no_of_days_last_month,1);
                    // $master_service_b_c_l3m_rejected = $master_service_b_c_l3m_rejected == 0 ? 0 : round($master_service_b_c_l3m_rejected/$no_of_days_last_3_months,1);

                    // $master_service_c_c_lw_rejected = $master_service_c_c_lw_rejected == 0 ? 0 : round($master_service_c_c_lw_rejected/(7*100),1);
                    // $master_service_c_c_pw_rejected = $master_service_c_c_pw_rejected == 0 ? 0 : round($master_service_c_c_pw_rejected/(7*100),1);
                    // $master_service_c_c_tm_rejected = $master_service_c_c_tm_rejected == 0 ? 0 : round($master_service_c_c_tm_rejected/($no_of_days_this_month*100),1);
                    // $master_service_c_c_lm_rejected = $master_service_c_c_lm_rejected == 0 ? 0 : round($master_service_c_c_lm_rejected/($no_of_days_last_month*100),1);
                    // $master_service_c_c_l3m_rejected = $master_service_c_c_l3m_rejected == 0 ? 0 : round($master_service_c_c_l3m_rejected/($no_of_days_last_3_months*100),1);
                    

                    $master_service_c_c_lw_rejected = $master_service_c_c_lw_rejected /100;
                    $master_service_c_c_pw_rejected = $master_service_c_c_pw_rejected /100;
                    $master_service_c_c_tm_rejected = $master_service_c_c_tm_rejected /100;
                    $master_service_c_c_lm_rejected = $master_service_c_c_lm_rejected /100;
                    $master_service_c_c_l3m_rejected = $master_service_c_c_l3m_rejected /100;

                    if($master_service_b_c_lw_rejected == 0 && $master_service_b_c_pw_rejected == 0 && $master_service_b_c_tm_rejected == 0 && $master_service_b_c_lm_rejected == 0 && $master_service_b_c_l3m_rejected == 0 && $master_service_c_c_lw_rejected == 0 && $master_service_c_c_pw_rejected == 0 && $master_service_c_c_tm_rejected == 0 && $master_service_c_c_lm_rejected == 0 && $master_service_c_c_l3m_rejected == 0){
                        continue;
                    } 
                    $master_service_table_rejected_g = $master_service_table_rejected_g.'<tr><th>'.$value_rejected.'</th><td>'.$master_service_b_c_lw_rejected.'</td><td>'.$master_service_b_c_pw_rejected.'</td><td>'.$master_service_b_c_tm_rejected.'</td><td>'.$master_service_b_c_lm_rejected.'</td><td>'.$master_service_b_c_l3m_rejected.'</td></tr>';
                    $master_service_table_rejected_c = $master_service_table_rejected_c.'<tr><th>'.$value_rejected.'</th><td>'.$master_service_c_c_lw_rejected.'</td><td>'.$master_service_c_c_pw_rejected.'</td><td>'.$master_service_c_c_tm_rejected.'</td><td>'.$master_service_c_c_lm_rejected.'</td><td>'.$master_service_c_c_l3m_rejected.'</td></tr>';

                }// for each


            $master_service_table_rejected_g = $master_service_table_rejected_g.'</tbody></table>';
            $master_service_table_rejected_c = $master_service_table_rejected_c.'</tbody></table>';
            $master_service_table_rejected = $master_service_table_rejected_g."<br>".$master_service_table_rejected_c;
        }
        //vehicle rejected set
        {
        
            $vehicle_table_rejected_g = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Vehicle</th>
              <th colspan="5" class="list1">Avg no.of Goaxles</th>
            </tr>
            <tr class="subheader">
              <td class="list1">LastWeek</td>
              <td class="list1">PreviousWeek</td>
              <td class="list1">MonthToDate</td>
              <td class="list1">LastMonth</td>
              <td class="list1">LastThreeMonths</td>
            </tr>';
            $vehicle_table_rejected_c = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Vehicle</th>
              <th colspan="5" class="list2">Avg Credits Acquired</th>
            </tr>
            <tr class="subheader">
              <td class="list2">LastWeek</td>
              <td class="list2">PreviousWeek</td>
              <td class="list2">MonthToDate</td>
              <td class="list2">LastMonth</td>
              <td class="list2">LastThreeMonths</td>
            </tr>';
                foreach($vehicle_arr_rejected as $value_rejected){
                    $vehicle_b_c_lw_rejected = 0 ;$vehicle_c_c_lw_rejected = 0 ;
                    $vehicle_b_c_pw_rejected = 0 ;$vehicle_c_c_pw_rejected = 0 ;
                    $vehicle_b_c_tm_rejected = 0 ;$vehicle_c_c_tm_rejected = 0 ;
                    $vehicle_b_c_lm_rejected = 0 ;$vehicle_c_c_lm_rejected = 0 ; 
                    $vehicle_b_c_l3m_rejected = 0 ;$vehicle_c_c_l3m_rejected = 0 ;
                    foreach ($data_rejected as $key_rejected){
                        $credits_rejected = $key_rejected['credits'];
                        $vehicle_rejected = $key_rejected['vehicle'];
                        $log_rejected = $key_rejected['log'];
                        
                        if($log_rejected<$today && $log_rejected>= $last_week){
                            if($vehicle_rejected == $value_rejected){
                                $vehicle_b_c_lw_rejected = $vehicle_b_c_lw_rejected+1;
                                $vehicle_c_c_lw_rejected = $vehicle_c_c_lw_rejected+$credits_rejected;
                            }
                        }
                        if($log_rejected<$last_week && $log_rejected>= $previous_week){
                            if($vehicle_rejected == $value_rejected){
                                $vehicle_b_c_pw_rejected = $vehicle_b_c_pw_rejected+1;
                                $vehicle_c_c_pw_rejected = $vehicle_c_c_pw_rejected+$credits_rejected;
                            }
                        }
                        if($log_rejected<$today && $log_rejected>= $this_month){
                            if($vehicle_rejected == $value_rejected){
                                $vehicle_b_c_tm_rejected = $vehicle_b_c_tm_rejected+1;
                                $vehicle_c_c_tm_rejected = $vehicle_c_c_tm_rejected+$credits_rejected;
                            }
                        }
                        if($log_rejected<$this_month && $log_rejected>= $last_month){
                            if($vehicle_rejected == $value_rejected){
                                $vehicle_b_c_lm_rejected = $vehicle_b_c_lm_rejected+1;
                                $vehicle_c_c_lm_rejected = $vehicle_c_c_lm_rejected+$credits_rejected;
                            }
                        }
                        if($log_rejected<$this_month && $log_rejected>= $three_m_ago){
                            if($vehicle_rejected == $value_rejected){
                                $vehicle_b_c_l3m_rejected = $vehicle_b_c_l3m_rejected+1;
                                $vehicle_c_c_l3m_rejected = $vehicle_c_c_l3m_rejected+$credits_rejected;
                            }
                        }
                    }//foreach data

                    
                    // $vehicle_b_c_lw_rejected = $vehicle_b_c_lw_rejected == 0 ? 0 : round($vehicle_b_c_lw_rejected/7,1);
                    // $vehicle_b_c_pw_rejected = $vehicle_b_c_pw_rejected == 0 ? 0 : round($vehicle_b_c_pw_rejected/7,1);
                    // $vehicle_b_c_tm_rejected = $vehicle_b_c_tm_rejected == 0 ? 0 : round($vehicle_b_c_tm_rejected/$no_of_days_this_month,1);
                    // $vehicle_b_c_lm_rejected = $vehicle_b_c_lm_rejected == 0 ? 0 : round($vehicle_b_c_lm_rejected/$no_of_days_last_month,1);
                    // $vehicle_b_c_l3m_rejected = $vehicle_b_c_l3m_rejected == 0 ? 0 : round($vehicle_b_c_l3m_rejected/$no_of_days_last_3_months,1);

                    // $vehicle_c_c_lw_rejected = $vehicle_c_c_lw_rejected == 0 ? 0 : round($vehicle_c_c_lw_rejected/(7*100),1);
                    // $vehicle_c_c_pw_rejected = $vehicle_c_c_pw_rejected == 0 ? 0 : round($vehicle_c_c_pw_rejected/(7*100),1);
                    // $vehicle_c_c_tm_rejected = $vehicle_c_c_tm_rejected == 0 ? 0 : round($vehicle_c_c_tm_rejected/($no_of_days_this_month*100),1);
                    // $vehicle_c_c_lm_rejected = $vehicle_c_c_lm_rejected == 0 ? 0 : round($vehicle_c_c_lm_rejected/($no_of_days_last_month*100),1);
                    // $vehicle_c_c_l3m_rejected = $vehicle_c_c_l3m_rejected == 0 ? 0 : round($vehicle_c_c_l3m_rejected/($no_of_days_last_3_months*100),1);
                    

                    $vehicle_c_c_lw_rejected = $vehicle_c_c_lw_rejected /100;
                    $vehicle_c_c_pw_rejected = $vehicle_c_c_pw_rejected /100;
                    $vehicle_c_c_tm_rejected = $vehicle_c_c_tm_rejected /100;
                    $vehicle_c_c_lm_rejected = $vehicle_c_c_lm_rejected /100;
                    $vehicle_c_c_l3m_rejected = $vehicle_c_c_l3m_rejected /100;
        
                    if($vehicle_b_c_lw_rejected  == 0 && $vehicle_b_c_pw_rejected  == 0 && $vehicle_b_c_tm_rejected  == 0 && $vehicle_b_c_lm_rejected  == 0 && $vehicle_b_c_l3m_rejected  == 0 && $vehicle_c_c_lw_rejected  == 0 && $vehicle_c_c_pw_rejected  == 0 && $vehicle_c_c_tm_rejected  == 0 && $vehicle_c_c_lm_rejected  == 0 && $vehicle_c_c_l3m_rejected  == 0){
                        continue;
                    }
                    $vehicle_table_rejected_g = $vehicle_table_rejected_g.'<tr><th>'.$value_rejected.'</th><td>'.$vehicle_b_c_lw_rejected.'</td><td>'.$vehicle_b_c_pw_rejected.'</td><td>'.$vehicle_b_c_tm_rejected.'</td><td>'.$vehicle_b_c_lm_rejected.'</td><td>'.$vehicle_b_c_l3m_rejected.'</td></tr>';
                    $vehicle_table_rejected_c = $vehicle_table_rejected_c.'<tr><th>'.$value_rejected.'</th><td>'.$vehicle_c_c_lw_rejected.'</td><td>'.$vehicle_c_c_pw_rejected.'</td><td>'.$vehicle_c_c_tm_rejected.'</td><td>'.$vehicle_c_c_lm_rejected.'</td><td>'.$vehicle_c_c_l3m_rejected.'</td></tr>';

                }// for each
        
        
            $vehicle_table_rejected_g = $vehicle_table_rejected_g.'</tbody></table>';
            $vehicle_table_rejected_c = $vehicle_table_rejected_c.'</tbody></table>';

            $vehicle_table_rejected = $vehicle_table_rejected_g."<br>".$vehicle_table_rejected_c;
        }
        //shop_name rejected set
        {
        
            $shop_name_table_rejected_g = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Shop Name</th>
              <th rowspan="2" align="center" class="list1">Type</th>
              <th rowspan="2" align="center" class="list1">City</th>
              <th colspan="5" class="list1">Avg no.of Goaxles</th>
            </tr>
            <tr class="subheader">
              <td class="list1">LastWeek</td>
              <td class="list1">PreviousWeek</td>
              <td class="list1">MonthToDate</td>
              <td class="list1">LastMonth</td>
              <td class="list1">LastThreeMonths</td>
            </tr>';
            $shop_name_table_rejected_c = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Shop Name</th>
              <th rowspan="2" align="center" class="list1">Type</th>
              <th rowspan="2" align="center" class="list1">City</th>
              <th colspan="5" class="list2">Avg Credits Acquired</th>
            </tr>
            <tr class="subheader">
              <td class="list2">LastWeek</td>
              <td class="list2">PreviousWeek</td>
              <td class="list2">MonthToDate</td>
              <td class="list2">LastMonth</td>
              <td class="list2">LastThreeMonths</td>
            </tr>';
                foreach($shop_name_arr_rejected as $value_rejected){
                    $shop_name_b_c_lw_rejected = 0 ;$shop_name_c_c_lw_rejected = 0 ;
                    $shop_name_b_c_pw_rejected = 0 ;$shop_name_c_c_pw_rejected = 0 ;
                    $shop_name_b_c_tm_rejected = 0 ;$shop_name_c_c_tm_rejected = 0 ;
                    $shop_name_b_c_lm_rejected = 0 ;$shop_name_c_c_lm_rejected = 0 ; 
                    $shop_name_b_c_l3m_rejected = 0 ;$shop_name_c_c_l3m_rejected = 0 ;
                    foreach ($data_rejected as $key_rejected){
                        $credits_rejected = $key_rejected['credits'];
                        $shop_name_rejected = $key_rejected['shop_name'];
                        $log_rejected = $key_rejected['log'];
                        if($shop_name_rejected == $value_rejected){
                            $vehicle_rejected = $key_rejected['vehicle'];
                            $city_rejected = $key_rejected['city'];
                        }
                        
                        if($log_rejected<$today && $log_rejected>= $last_week){
                            if($shop_name_rejected == $value_rejected){
                                $shop_name_b_c_lw_rejected = $shop_name_b_c_lw_rejected+1;
                                $shop_name_c_c_lw_rejected = $shop_name_c_c_lw_rejected+$credits_rejected;
                            }
                        }
                        if($log_rejected<$last_week && $log_rejected>= $previous_week){
                            if($shop_name_rejected == $value_rejected){
                                $shop_name_b_c_pw_rejected = $shop_name_b_c_pw_rejected+1;
                                $shop_name_c_c_pw_rejected = $shop_name_c_c_pw_rejected+$credits_rejected;
                            }
                        }
                        if($log_rejected<$today && $log_rejected>= $this_month){
                            if($shop_name_rejected == $value_rejected){
                                $shop_name_b_c_tm_rejected = $shop_name_b_c_tm_rejected+1;
                                $shop_name_c_c_tm_rejected = $shop_name_c_c_tm_rejected+$credits_rejected;
                            }
                        }
                        if($log_rejected<$this_month && $log_rejected>= $last_month){
                            if($shop_name_rejected == $value_rejected){
                                $shop_name_b_c_lm_rejected = $shop_name_b_c_lm_rejected+1;
                                $shop_name_c_c_lm_rejected = $shop_name_c_c_lm_rejected+$credits_rejected;
                            }
                        }
                        if($log_rejected<$this_month && $log_rejected>= $three_m_ago){
                            if($shop_name_rejected == $value_rejected){
                                $shop_name_b_c_l3m_rejected = $shop_name_b_c_l3m_rejected+1;
                                $shop_name_c_c_l3m_rejected = $shop_name_c_c_l3m_rejected+$credits_rejected;
                            }
                        }
                    }//foreach data

                    // $shop_name_b_c_lw_rejected = $shop_name_b_c_lw_rejected == 0 ? 0 : round($shop_name_b_c_lw_rejected/7,1);
                    // $shop_name_b_c_pw_rejected = $shop_name_b_c_pw_rejected == 0 ? 0 : round($shop_name_b_c_pw_rejected/7,1);
                    // $shop_name_b_c_tm_rejected = $shop_name_b_c_tm_rejected == 0 ? 0 : round($shop_name_b_c_tm_rejected/$no_of_days_this_month,1);
                    // $shop_name_b_c_lm_rejected = $shop_name_b_c_lm_rejected == 0 ? 0 : round($shop_name_b_c_lm_rejected/$no_of_days_last_month,1);
                    // $shop_name_b_c_l3m_rejected = $shop_name_b_c_l3m_rejected == 0 ? 0 : round($shop_name_b_c_l3m_rejected/$no_of_days_last_3_months,1);

                    // $shop_name_c_c_lw_rejected = $shop_name_c_c_lw_rejected == 0 ? 0 : round($shop_name_c_c_lw_rejected/(7*100),1);
                    // $shop_name_c_c_pw_rejected = $shop_name_c_c_pw_rejected == 0 ? 0 : round($shop_name_c_c_pw_rejected/(7*100),1);
                    // $shop_name_c_c_tm_rejected = $shop_name_c_c_tm_rejected == 0 ? 0 : round($shop_name_c_c_tm_rejected/($no_of_days_this_month*100),1);
                    // $shop_name_c_c_lm_rejected = $shop_name_c_c_lm_rejected == 0 ? 0 : round($shop_name_c_c_lm_rejected/($no_of_days_last_month*100),1);
                    // $shop_name_c_c_l3m_rejected = $shop_name_c_c_l3m_rejected == 0 ? 0 : round($shop_name_c_c_l3m_rejected/($no_of_days_last_3_months*100),1);
                    
                    $shop_name_c_c_lw_rejected = $shop_name_c_c_lw_rejected /100;
                    $shop_name_c_c_pw_rejected = $shop_name_c_c_pw_rejected /100;
                    $shop_name_c_c_tm_rejected = $shop_name_c_c_tm_rejected /100;
                    $shop_name_c_c_lm_rejected = $shop_name_c_c_lm_rejected /100;
                    $shop_name_c_c_l3m_rejected = $shop_name_c_c_l3m_rejected /100;

                    if($shop_name_b_c_lw_rejected == 0 && $shop_name_b_c_pw_rejected == 0 && $shop_name_b_c_tm_rejected == 0 && $shop_name_b_c_lm_rejected == 0 && $shop_name_b_c_l3m_rejected == 0 && $shop_name_c_c_lw_rejected == 0 && $shop_name_c_c_pw_rejected == 0 && $shop_name_c_c_tm_rejected == 0 && $shop_name_c_c_lm_rejected == 0 && $shop_name_c_c_l3m_rejected == 0){
                        continue;
                    }
                    $shop_name_table_rejected_g = $shop_name_table_rejected_g.'<tr><th>'.$value_rejected.'</th><td>'.$vehicle_rejected.'</td><td>'.$city_rejected.'</td><td>'.$shop_name_b_c_lw_rejected.'</td><td>'.$shop_name_b_c_pw_rejected.'</td><td>'.$shop_name_b_c_tm_rejected.'</td><td>'.$shop_name_b_c_lm_rejected.'</td><td>'.$shop_name_b_c_l3m_rejected.'</td></tr>';
                    $shop_name_table_rejected_c = $shop_name_table_rejected_c.'<tr><th>'.$value_rejected.'</th><td>'.$vehicle_rejected.'</td><td>'.$city_rejected.'</td><td>'.$shop_name_c_c_lw_rejected.'</td><td>'.$shop_name_c_c_pw_rejected.'</td><td>'.$shop_name_c_c_tm_rejected.'</td><td>'.$shop_name_c_c_lm_rejected.'</td><td>'.$shop_name_c_c_l3m_rejected.'</td></tr>';

                }// for each
        
        
            $shop_name_table_rejected_g = $shop_name_table_rejected_g.'</tbody></table>';
            $shop_name_table_rejected_c = $shop_name_table_rejected_c.'</tbody></table>';
            $shop_name_table_rejected = $shop_name_table_rejected_g."<br>".$shop_name_table_rejected_c;
        }

        //idle set
      //  echo "<h3>Idle set</h3>";
        $city_arr_idle = array_unique($city_arr_idle);
        $master_service_arr_idle = array_unique($master_service_arr_idle);
        $vehicle_arr_idle = array_unique($vehicle_arr_idle);
        $shop_name_arr_idle = array_unique($shop_name_arr_idle);
        
        //city idle set
        {

            $city_table_idle_g = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">City</th>
              <th colspan="5" class="list1">Avg no.of Goaxles</th>
            </tr>
            <tr class="subheader">
              <td class="list1">LastWeek</td>
              <td class="list1">PreviousWeek</td>
              <td class="list1">MonthToDate</td>
              <td class="list1">LastMonth</td>
              <td class="list1">LastThreeMonths</td>
            </tr>';
            $city_table_idle_c = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">City</th>
              <th colspan="5" class="list2">Avg Credits Acquired</th>
            </tr>
            <tr class="subheader">
              <td class="list2">LastWeek</td>
              <td class="list2">PreviousWeek</td>
              <td class="list2">MonthToDate</td>
              <td class="list2">LastMonth</td>
              <td class="list2">LastThreeMonths</td>
            </tr>';
                foreach($city_arr_idle as $value_idle){
                    $city_b_c_lw_idle = 0 ;$city_c_c_lw_idle = 0 ;
                    $city_b_c_pw_idle = 0 ;$city_c_c_pw_idle = 0 ;
                    $city_b_c_tm_idle = 0 ;$city_c_c_tm_idle = 0 ;
                    $city_b_c_lm_idle = 0 ;$city_c_c_lm_idle = 0 ; 
                    $city_b_c_l3m_idle = 0 ;$city_c_c_l3m_idle = 0 ;
                    foreach ($data_idle as $key_idle){
                        $credits_idle = $key_idle['credits'];
                        $city_idle = $key_idle['city'];
                        $log_idle = $key_idle['log'];
                        
                        if($log_idle<$today && $log_idle>= $last_week){
                            if($city_idle == $value_idle){
                                $city_b_c_lw_idle = $city_b_c_lw_idle+1;
                                $city_c_c_lw_idle = $city_c_c_lw_idle+$credits_idle;
                            }
                        }
                        if($log_idle<$last_week && $log_idle>= $previous_week){
                            if($city_idle == $value_idle){
                                $city_b_c_pw_idle = $city_b_c_pw_idle+1;
                                $city_c_c_pw_idle = $city_c_c_pw_idle+$credits_idle;
                            }
                        }
                        if($log_idle<$today && $log_idle>= $this_month){
                            if($city_idle == $value_idle){
                                $city_b_c_tm_idle = $city_b_c_tm_idle+1;
                                $city_c_c_tm_idle = $city_c_c_tm_idle+$credits_idle;
                            }
                        }
                        if($log_idle<$this_month && $log_idle>= $last_month){
                            if($city_idle == $value_idle){
                                $city_b_c_lm_idle = $city_b_c_lm_idle+1;
                                $city_c_c_lm_idle = $city_c_c_lm_idle+$credits_idle;
                            }
                        }
                        if($log_idle<$this_month && $log_idle>= $three_m_ago){
                            if($city_idle == $value_idle){
                                $city_b_c_l3m_idle = $city_b_c_l3m_idle+1;
                                $city_c_c_l3m_idle = $city_c_c_l3m_idle+$credits_idle;
                            }
                        }
                    }//foreach data

                    // $city_b_c_lw_idle = $city_b_c_lw_idle == 0 ? 0 : round($city_b_c_lw_idle/7,1);
                    // $city_b_c_pw_idle = $city_b_c_pw_idle == 0 ? 0 : round($city_b_c_pw_idle/7,1);
                    // $city_b_c_tm_idle = $city_b_c_tm_idle == 0 ? 0 : round($city_b_c_tm_idle/$no_of_days_this_month,1);
                    // $city_b_c_lm_idle = $city_b_c_lm_idle == 0 ? 0 : round($city_b_c_lm_idle/$no_of_days_last_month,1);
                    // $city_b_c_l3m_idle = $city_b_c_l3m_idle == 0 ? 0 : round($city_b_c_l3m_idle/$no_of_days_last_3_months,1);

                    // $city_c_c_lw_idle = $city_c_c_lw_idle == 0 ? 0 : round($city_c_c_lw_idle/(7*100),1);
                    // $city_c_c_pw_idle = $city_c_c_pw_idle == 0 ? 0 : round($city_c_c_pw_idle/(7*100),1);
                    // $city_c_c_tm_idle = $city_c_c_tm_idle == 0 ? 0 : round($city_c_c_tm_idle/($no_of_days_this_month*100),1);
                    // $city_c_c_lm_idle = $city_c_c_lm_idle == 0 ? 0 : round($city_c_c_lm_idle/($no_of_days_last_month*100),1);
                    // $city_c_c_l3m_idle = $city_c_c_l3m_idle == 0 ? 0 : round($city_c_c_l3m_idle/($no_of_days_last_3_months*100),1);
                    

                    $city_c_c_lw_idle = $city_c_c_lw_idle /100;
                    $city_c_c_pw_idle = $city_c_c_pw_idle /100;
                    $city_c_c_tm_idle = $city_c_c_tm_idle /100;
                    $city_c_c_lm_idle = $city_c_c_lm_idle /100;
                    $city_c_c_l3m_idle = $city_c_c_l3m_idle /100;

                    if($city_b_c_lw_idle == 0 && $city_b_c_pw_idle == 0 && $city_b_c_tm_idle == 0 && $city_b_c_lm_idle == 0 && $city_b_c_l3m_idle == 0 && $city_c_c_lw_idle == 0 && $city_c_c_pw_idle == 0 && $city_c_c_tm_idle == 0 && $city_c_c_lm_idle == 0 && $city_c_c_l3m_idle == 0){
                        continue;
                    }
                    $city_table_idle_g = $city_table_idle_g.'<tr><th>'.$value_idle.'</th><td>'.$city_b_c_lw_idle.'</td><td>'.$city_b_c_pw_idle.'</td><td>'.$city_b_c_tm_idle.'</td><td>'.$city_b_c_lm_idle.'</td><td>'.$city_b_c_l3m_idle.'</td></tr>';
                    $city_table_idle_c = $city_table_idle_c.'<tr><th>'.$value_idle.'</th><td>'.$city_c_c_lw_idle.'</td><td>'.$city_c_c_pw_idle.'</td><td>'.$city_c_c_tm_idle.'</td><td>'.$city_c_c_lm_idle.'</td><td>'.$city_c_c_l3m_idle.'</td></tr>';

                }// for each


            $city_table_idle_g = $city_table_idle_g.'</tbody></table>';
            $city_table_idle_c = $city_table_idle_c.'</tbody></table>';
            $city_table_idle = $city_table_idle_g."<br>".$city_table_idle_c;
        }
        //master service idle set
        {

            $master_service_table_idle_g = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Segment</th>
              <th colspan="5" class="list1">Avg no.of Goaxles</th>
            </tr>
            <tr class="subheader">
              <td class="list1">LastWeek</td>
              <td class="list1">PreviousWeek</td>
              <td class="list1">MonthToDate</td>
              <td class="list1">LastMonth</td>
              <td class="list1">LastThreeMonths</td>
            </tr>';
            $master_service_table_idle_c = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Segment</th>
              <th colspan="5" class="list2">Avg Credits Acquired</th>
            </tr>
            <tr class="subheader">
              <td class="list2">LastWeek</td>
              <td class="list2">PreviousWeek</td>
              <td class="list2">MonthToDate</td>
              <td class="list2">LastMonth</td>
              <td class="list2">LastThreeMonths</td>
            </tr>';
                foreach($master_service_arr_idle as $value_idle){
                    $master_service_b_c_lw_idle = 0 ;$master_service_c_c_lw_idle = 0 ;
                    $master_service_b_c_pw_idle = 0 ;$master_service_c_c_pw_idle = 0 ;
                    $master_service_b_c_tm_idle = 0 ;$master_service_c_c_tm_idle = 0 ;
                    $master_service_b_c_lm_idle = 0 ;$master_service_c_c_lm_idle = 0 ; 
                    $master_service_b_c_l3m_idle = 0 ;$master_service_c_c_l3m_idle = 0 ;
                    foreach ($data_idle as $key_idle){
                        $credits_idle = $key_idle['credits'];
                        $master_service_idle = $key_idle['master_service'];
                        $log_idle = $key_idle['log'];
                        
                        if($log_idle<$today && $log_idle>= $last_week){
                            if($master_service_idle == $value_idle){
                                $master_service_b_c_lw_idle = $master_service_b_c_lw_idle+1;
                                $master_service_c_c_lw_idle = $master_service_c_c_lw_idle+$credits_idle;
                            }
                        }
                        if($log_idle<$last_week && $log_idle>= $previous_week){
                            if($master_service_idle == $value_idle){
                                $master_service_b_c_pw_idle = $master_service_b_c_pw_idle+1;
                                $master_service_c_c_pw_idle = $master_service_c_c_pw_idle+$credits_idle;
                            }
                        }
                        if($log_idle<$today && $log_idle>= $this_month){
                            if($master_service_idle == $value_idle){
                                $master_service_b_c_tm_idle = $master_service_b_c_tm_idle+1;
                                $master_service_c_c_tm_idle = $master_service_c_c_tm_idle+$credits_idle;
                            }
                        }
                        if($log_idle<$this_month && $log_idle>= $last_month){
                            if($master_service_idle == $value_idle){
                                $master_service_b_c_lm_idle = $master_service_b_c_lm_idle+1;
                                $master_service_c_c_lm_idle = $master_service_c_c_lm_idle+$credits_idle;
                            }
                        }
                        if($log_idle<$this_month && $log_idle>= $three_m_ago){
                            if($master_service_idle == $value_idle){
                                $master_service_b_c_l3m_idle = $master_service_b_c_l3m_idle+1;
                                $master_service_c_c_l3m_idle = $master_service_c_c_l3m_idle+$credits_idle;
                            }
                        }
                    }//foreach data

                    // $master_service_b_c_lw_idle = $master_service_b_c_lw_idle == 0 ? 0 : round($master_service_b_c_lw_idle/7,1);
                    // $master_service_b_c_pw_idle = $master_service_b_c_pw_idle == 0 ? 0 : round($master_service_b_c_pw_idle/7,1);
                    // $master_service_b_c_tm_idle = $master_service_b_c_tm_idle == 0 ? 0 : round($master_service_b_c_tm_idle/$no_of_days_this_month,1);
                    // $master_service_b_c_lm_idle = $master_service_b_c_lm_idle == 0 ? 0 : round($master_service_b_c_lm_idle/$no_of_days_last_month,1);
                    // $master_service_b_c_l3m_idle = $master_service_b_c_l3m_idle == 0 ? 0 : round($master_service_b_c_l3m_idle/$no_of_days_last_3_months,1);

                    // $master_service_c_c_lw_idle = $master_service_c_c_lw_idle == 0 ? 0 : round($master_service_c_c_lw_idle/(7*100),1);
                    // $master_service_c_c_pw_idle = $master_service_c_c_pw_idle == 0 ? 0 : round($master_service_c_c_pw_idle/(7*100),1);
                    // $master_service_c_c_tm_idle = $master_service_c_c_tm_idle == 0 ? 0 : round($master_service_c_c_tm_idle/($no_of_days_this_month*100),1);
                    // $master_service_c_c_lm_idle = $master_service_c_c_lm_idle == 0 ? 0 : round($master_service_c_c_lm_idle/($no_of_days_last_month*100),1);
                    // $master_service_c_c_l3m_idle = $master_service_c_c_l3m_idle == 0 ? 0 : round($master_service_c_c_l3m_idle/($no_of_days_last_3_months*100),1);
                    

                    $master_service_c_c_lw_idle = $master_service_c_c_lw_idle /100;
                    $master_service_c_c_pw_idle = $master_service_c_c_pw_idle /100;
                    $master_service_c_c_tm_idle = $master_service_c_c_tm_idle /100;
                    $master_service_c_c_lm_idle = $master_service_c_c_lm_idle /100;
                    $master_service_c_c_l3m_idle = $master_service_c_c_l3m_idle /100;

                    if($master_service_b_c_lw_idle == 0 && $master_service_b_c_pw_idle == 0 && $master_service_b_c_tm_idle == 0 && $master_service_b_c_lm_idle == 0 && $master_service_b_c_l3m_idle == 0 && $master_service_c_c_lw_idle == 0 && $master_service_c_c_pw_idle == 0 && $master_service_c_c_tm_idle == 0 && $master_service_c_c_lm_idle == 0 && $master_service_c_c_l3m_idle == 0){
                        continue;
                    }
                    $master_service_table_idle_g = $master_service_table_idle_g.'<tr><th>'.$value_idle.'</th><td>'.$master_service_b_c_lw_idle.'</td><td>'.$master_service_b_c_pw_idle.'</td><td>'.$master_service_b_c_tm_idle.'</td><td>'.$master_service_b_c_lm_idle.'</td><td>'.$master_service_b_c_l3m_idle.'</td></tr>';
                    $master_service_table_idle_c = $master_service_table_idle_c.'<tr><th>'.$value_idle.'</th><td>'.$master_service_c_c_lw_idle.'</td><td>'.$master_service_c_c_pw_idle.'</td><td>'.$master_service_c_c_tm_idle.'</td><td>'.$master_service_c_c_lm_idle.'</td><td>'.$master_service_c_c_l3m_idle.'</td></tr>';

                }// for each


            $master_service_table_idle_g = $master_service_table_idle_g.'</tbody></table>';
            $master_service_table_idle_c = $master_service_table_idle_c.'</tbody></table>';
            $master_service_table_idle = $master_service_table_idle_g."<br>".$master_service_table_idle_c;
        }
       // echo "<br>";
        //vehicle idle set
        {
        
            $vehicle_table_idle_g = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Vehicle</th>
              <th colspan="5" class="list1">Avg no.of Goaxles</th>
            </tr>
            <tr class="subheader">
              <td class="list1">LastWeek</td>
              <td class="list1">PreviousWeek</td>
              <td class="list1">MonthToDate</td>
              <td class="list1">LastMonth</td>
              <td class="list1">LastThreeMonths</td>
            </tr>';
            $vehicle_table_idle_c = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Vehicle</th>
              <th colspan="5" class="list2">Avg Credits Acquired</th>
            </tr>
            <tr class="subheader">
              <td class="list2">LastWeek</td>
              <td class="list2">PreviousWeek</td>
              <td class="list2">MonthToDate</td>
              <td class="list2">LastMonth</td>
              <td class="list2">LastThreeMonths</td>
            </tr>';
                foreach($vehicle_arr_idle as $value_idle){
                    $vehicle_b_c_lw_idle = 0 ;$vehicle_c_c_lw_idle = 0 ;
                    $vehicle_b_c_pw_idle = 0 ;$vehicle_c_c_pw_idle = 0 ;
                    $vehicle_b_c_tm_idle = 0 ;$vehicle_c_c_tm_idle = 0 ;
                    $vehicle_b_c_lm_idle = 0 ;$vehicle_c_c_lm_idle = 0 ; 
                    $vehicle_b_c_l3m_idle = 0 ;$vehicle_c_c_l3m_idle = 0 ;
                    foreach ($data_idle as $key_idle){
                        $credits_idle = $key_idle['credits'];
                        $vehicle_idle = $key_idle['vehicle'];
                        $log_idle = $key_idle['log'];
                        
                        if($log_idle<$today && $log_idle>= $last_week){
                            if($vehicle_idle == $value_idle){
                                $vehicle_b_c_lw_idle = $vehicle_b_c_lw_idle+1;
                                $vehicle_c_c_lw_idle = $vehicle_c_c_lw_idle+$credits_idle;
                            }
                        }
                        if($log_idle<$last_week && $log_idle>= $previous_week){
                            if($vehicle_idle == $value_idle){
                                $vehicle_b_c_pw_idle = $vehicle_b_c_pw_idle+1;
                                $vehicle_c_c_pw_idle = $vehicle_c_c_pw_idle+$credits_idle;
                            }
                        }
                        if($log_idle<$today && $log_idle>= $this_month){
                            if($vehicle_idle == $value_idle){
                                $vehicle_b_c_tm_idle = $vehicle_b_c_tm_idle+1;
                                $vehicle_c_c_tm_idle = $vehicle_c_c_tm_idle+$credits_idle;
                            }
                        }
                        if($log_idle<$this_month && $log_idle>= $last_month){
                            if($vehicle_idle == $value_idle){
                                $vehicle_b_c_lm_idle = $vehicle_b_c_lm_idle+1;
                                $vehicle_c_c_lm_idle = $vehicle_c_c_lm_idle+$credits_idle;
                            }
                        }
                        if($log_idle<$this_month && $log_idle>= $three_m_ago){
                            if($vehicle_idle == $value_idle){
                                $vehicle_b_c_l3m_idle = $vehicle_b_c_l3m_idle+1;
                                $vehicle_c_c_l3m_idle = $vehicle_c_c_l3m_idle+$credits_idle;
                            }
                        }
                    }//foreach data

                    // $vehicle_b_c_lw_idle = $vehicle_b_c_lw_idle == 0 ? 0 : round($vehicle_b_c_lw_idle/7,1);
                    // $vehicle_b_c_pw_idle = $vehicle_b_c_pw_idle == 0 ? 0 : round($vehicle_b_c_pw_idle/7,1);
                    // $vehicle_b_c_tm_idle = $vehicle_b_c_tm_idle == 0 ? 0 : round($vehicle_b_c_tm_idle/$no_of_days_this_month,1);
                    // $vehicle_b_c_lm_idle = $vehicle_b_c_lm_idle == 0 ? 0 : round($vehicle_b_c_lm_idle/$no_of_days_last_month,1);
                    // $vehicle_b_c_l3m_idle = $vehicle_b_c_l3m_idle == 0 ? 0 : round($vehicle_b_c_l3m_idle/$no_of_days_last_3_months,1);

                    // $vehicle_c_c_lw_idle = $vehicle_c_c_lw_idle == 0 ? 0 : round($vehicle_c_c_lw_idle/(7*100),1);
                    // $vehicle_c_c_pw_idle = $vehicle_c_c_pw_idle == 0 ? 0 : round($vehicle_c_c_pw_idle/(7*100),1);
                    // $vehicle_c_c_tm_idle = $vehicle_c_c_tm_idle == 0 ? 0 : round($vehicle_c_c_tm_idle/($no_of_days_this_month*100),1);
                    // $vehicle_c_c_lm_idle = $vehicle_c_c_lm_idle == 0 ? 0 : round($vehicle_c_c_lm_idle/($no_of_days_last_month*100),1);
                    // $vehicle_c_c_l3m_idle = $vehicle_c_c_l3m_idle == 0 ? 0 : round($vehicle_c_c_l3m_idle/($no_of_days_last_3_months*100),1);
                    
                    
                    $vehicle_c_c_lw_idle = $vehicle_c_c_lw_idle /100;
                    $vehicle_c_c_pw_idle = $vehicle_c_c_pw_idle /100;
                    $vehicle_c_c_tm_idle = $vehicle_c_c_tm_idle /100;
                    $vehicle_c_c_lm_idle = $vehicle_c_c_lm_idle /100;
                    $vehicle_c_c_l3m_idle = $vehicle_c_c_l3m_idle /100;
                    
                    if($vehicle_b_c_lw_idle == 0 && $vehicle_b_c_pw_idle == 0 && $vehicle_b_c_tm_idle == 0 && $vehicle_b_c_lm_idle == 0 && $vehicle_b_c_l3m_idle == 0 && $vehicle_c_c_lw_idle == 0 && $vehicle_c_c_pw_idle == 0 && $vehicle_c_c_tm_idle == 0 && $vehicle_c_c_lm_idle == 0 && $vehicle_c_c_l3m_idle == 0){
                        continue;
                    }
                    $vehicle_table_idle_g = $vehicle_table_idle_g.'<tr><th>'.$value_idle.'</th><td>'.$vehicle_b_c_lw_idle.'</td><td>'.$vehicle_b_c_pw_idle.'</td><td>'.$vehicle_b_c_tm_idle.'</td><td>'.$vehicle_b_c_lm_idle.'</td><td>'.$vehicle_b_c_l3m_idle.'</td></tr>';
                    $vehicle_table_idle_c = $vehicle_table_idle_c.'<tr><th>'.$value_idle.'</th><td>'.$vehicle_c_c_lw_idle.'</td><td>'.$vehicle_c_c_pw_idle.'</td><td>'.$vehicle_c_c_tm_idle.'</td><td>'.$vehicle_c_c_lm_idle.'</td><td>'.$vehicle_c_c_l3m_idle.'</td></tr>';

                }// for each
        
        
            $vehicle_table_idle_g = $vehicle_table_idle_g.'</tbody></table>';
            $vehicle_table_idle_c = $vehicle_table_idle_c.'</tbody></table>';
            $vehicle_table_idle = $vehicle_table_idle_g."<br>".$vehicle_table_idle_c;
        }
        //shop_name idle set
        {
        
            $shop_name_table_idle_g = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Shop Name</th>
              <th rowspan="2" align="center" class="list1">Type</th>
              <th rowspan="2" align="center" class="list1">City</th>
              <th colspan="5" class="list1">Avg no.of Goaxles</th>
            </tr>
            <tr class="subheader">
              <td class="list1">LastWeek</td>
              <td class="list1">PreviousWeek</td>
              <td class="list1">MonthToDate</td>
              <td class="list1">LastMonth</td>
              <td class="list1">LastThreeMonths</td>
            </tr>';
            $shop_name_table_idle_c = '<table align="center" class="float-center">
            <tr class="header">
              <th rowspan="2" align="center" class="list1">Shop Name</th>
              <th rowspan="2" align="center" class="list1">Type</th>
              <th rowspan="2" align="center" class="list1">City</th>
              <th colspan="5" class="list2">Avg Credits Acquired</th>
            </tr>
            <tr class="subheader">
              <td class="list2">LastWeek</td>
              <td class="list2">PreviousWeek</td>
              <td class="list2">MonthToDate</td>
              <td class="list2">LastMonth</td>
              <td class="list2">LastThreeMonths</td>
            </tr>';
                foreach($shop_name_arr_idle as $value_idle){
                    $shop_name_b_c_lw_idle = 0 ;$shop_name_c_c_lw_idle = 0 ;
                    $shop_name_b_c_pw_idle = 0 ;$shop_name_c_c_pw_idle = 0 ;
                    $shop_name_b_c_tm_idle = 0 ;$shop_name_c_c_tm_idle = 0 ;
                    $shop_name_b_c_lm_idle = 0 ;$shop_name_c_c_lm_idle = 0 ; 
                    $shop_name_b_c_l3m_idle = 0 ;$shop_name_c_c_l3m_idle = 0 ;
                    foreach ($data_idle as $key_idle){
                        $credits_idle = $key_idle['credits'];
                        $shop_name_idle = $key_idle['shop_name'];
                        $log_idle = $key_idle['log'];
                        if($shop_name_idle == $value_idle){
                            $vehicle_idle = $key_idle['vehicle'];
                            $city_idle = $key_idle['city'];
                        }
                        
                        if($log_idle<$today && $log_idle>= $last_week){
                            if($shop_name_idle == $value_idle){
                                $shop_name_b_c_lw_idle = $shop_name_b_c_lw_idle+1;
                                $shop_name_c_c_lw_idle = $shop_name_c_c_lw_idle+$credits_idle;
                            }
                        }
                        if($log_idle<$last_week && $log_idle>= $previous_week){
                            if($shop_name_idle == $value_idle){
                                $shop_name_b_c_pw_idle = $shop_name_b_c_pw_idle+1;
                                $shop_name_c_c_pw_idle = $shop_name_c_c_pw_idle+$credits_idle;
                            }
                        }
                        if($log_idle<$today && $log_idle>= $this_month){
                            if($shop_name_idle == $value_idle){
                                $shop_name_b_c_tm_idle = $shop_name_b_c_tm_idle+1;
                                $shop_name_c_c_tm_idle = $shop_name_c_c_tm_idle+$credits_idle;
                            }
                        }
                        if($log_idle<$this_month && $log_idle>= $last_month){
                            if($shop_name_idle == $value_idle){
                                $shop_name_b_c_lm_idle = $shop_name_b_c_lm_idle+1;
                                $shop_name_c_c_lm_idle = $shop_name_c_c_lm_idle+$credits_idle;
                            }
                        }
                        if($log_idle<$this_month && $log_idle>= $three_m_ago){
                            if($shop_name_idle == $value_idle){
                                $shop_name_b_c_l3m_idle = $shop_name_b_c_l3m_idle+1;
                                $shop_name_c_c_l3m_idle = $shop_name_c_c_l3m_idle+$credits_idle;
                            }
                        }
                    }//foreach data

                    
                    // $shop_name_b_c_lw_idle = $shop_name_b_c_lw_idle == 0 ? 0 : round($shop_name_b_c_lw_idle/7,1);
                    // $shop_name_b_c_pw_idle = $shop_name_b_c_pw_idle == 0 ? 0 : round($shop_name_b_c_pw_idle/7,1);
                    // $shop_name_b_c_tm_idle = $shop_name_b_c_tm_idle == 0 ? 0 : round($shop_name_b_c_tm_idle/$no_of_days_this_month,1);
                    // $shop_name_b_c_lm_idle = $shop_name_b_c_lm_idle == 0 ? 0 : round($shop_name_b_c_lm_idle/$no_of_days_last_month,1);
                    // $shop_name_b_c_l3m_idle = $shop_name_b_c_l3m_idle == 0 ? 0 : round($shop_name_b_c_l3m_idle/$no_of_days_last_3_months,1);

                    // $shop_name_c_c_lw_idle = $shop_name_c_c_lw_idle == 0 ? 0 : round($shop_name_c_c_lw_idle/(7*100),1);
                    // $shop_name_c_c_pw_idle = $shop_name_c_c_pw_idle == 0 ? 0 : round($shop_name_c_c_pw_idle/(7*100),1);
                    // $shop_name_c_c_tm_idle = $shop_name_c_c_tm_idle == 0 ? 0 : round($shop_name_c_c_tm_idle/($no_of_days_this_month*100),1);
                    // $shop_name_c_c_lm_idle = $shop_name_c_c_lm_idle == 0 ? 0 : round($shop_name_c_c_lm_idle/($no_of_days_last_month*100),1);
                    // $shop_name_c_c_l3m_idle = $shop_name_c_c_l3m_idle == 0 ? 0 : round($shop_name_c_c_l3m_idle/($no_of_days_last_3_months*100),1);
                    
        
                    $shop_name_c_c_lw_idle = $shop_name_c_c_lw_idle /100;
                    $shop_name_c_c_pw_idle = $shop_name_c_c_pw_idle /100;
                    $shop_name_c_c_tm_idle = $shop_name_c_c_tm_idle /100;
                    $shop_name_c_c_lm_idle = $shop_name_c_c_lm_idle /100;
                    $shop_name_c_c_l3m_idle = $shop_name_c_c_l3m_idle /100;
        
                    if($shop_name_b_c_lw_idle == 0 && $shop_name_b_c_pw_idle == 0 && $shop_name_b_c_tm_idle == 0 && $shop_name_b_c_lm_idle == 0 && $shop_name_b_c_l3m_idle == 0 && $shop_name_c_c_lw_idle == 0 && $shop_name_c_c_pw_idle == 0 && $shop_name_c_c_tm_idle == 0 && $shop_name_c_c_lm_idle == 0 && $shop_name_c_c_l3m_idle == 0){
                        continue;
                    }
                    $shop_name_table_idle_g = $shop_name_table_idle_g.'<tr><th>'.$value_idle.'</th><td>'.$vehicle_idle.'</td><td>'.$city_idle.'</td><td>'.$shop_name_b_c_lw_idle.'</td><td>'.$shop_name_b_c_pw_idle.'</td><td>'.$shop_name_b_c_tm_idle.'</td><td>'.$shop_name_b_c_lm_idle.'</td><td>'.$shop_name_b_c_l3m_idle.'</td></tr>';
                    $shop_name_table_idle_c = $shop_name_table_idle_c.'<tr><th>'.$value_idle.'</th><td>'.$vehicle_idle.'</td><td>'.$city_idle.'</td><td>'.$shop_name_c_c_lw_idle.'</td><td>'.$shop_name_c_c_pw_idle.'</td><td>'.$shop_name_c_c_tm_idle.'</td><td>'.$shop_name_c_c_lm_idle.'</td><td>'.$shop_name_c_c_l3m_idle.'</td></tr>';
       
                }// for each
        
        
            $shop_name_table_idle_g = $shop_name_table_idle_g.'</tbody></table>';
            $shop_name_table_idle_c = $shop_name_table_idle_c.'</tbody></table>';
            $shop_name_table_idle = $shop_name_table_idle_g."<br>".$shop_name_table_idle_c;
        }
        */
               
    }      
    // ******************************* send emails ******************************************* //
    echo "<br>";
    
    {

        $to1='metrics@gobumpr.com';
        $to2='metrics@gobumpr.com';
        //$to3='metrics@gobumpr.com';
        //$to4='metrics@gobumpr.com';
        $to5='metrics@gobumpr.com';

        
       /* $to1='sirisha@gobumpr.com';
        $to2='sirisha@gobumpr.com';
        //$to3='sirisha@gobumpr.com';
        //$to4='sirisha@gobumpr.com';
        $to5='sirisha@gobumpr.com';*/
        
        if($to1!=''||$to1!=NULL){
            //SMTP Settings
            $mail1 = new PHPMailer();
            $mail1->IsSMTP();
            $mail1->SMTPAuth   = true;
            $mail1->SMTPSecure = "SSL";
            $mail1->Host       = "smtp.pepipost.com";
            $mail1->Username   = "letsgobumpr";
            $mail1->Password   = "GoBumpr25000!";
            $mail1->isHTML(true);

            $mail1->SetFrom('admin@gobumpr.com', 'Admin'); //from (verified email address)
            $mail1->Subject = "Credit Consumption Report - ".date('d M Y',strtotime($today));
            $body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            
            <head>
              <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
              <meta name="viewport" content="width=device-width">
              
              <style>
                .wrapper {
                  width: 100%;
                }
            
                body {
                  width: 100% !important;
                  min-width: 100%;
                  -webkit-text-size-adjust: 100%;
                  -ms-text-size-adjust: 100%;
                  margin: 0;
                  Margin: 0;
                  padding: 0;
                  -moz-box-sizing: border-box;
                  -webkit-box-sizing: border-box;
                  box-sizing: border-box;
                }
            
                center {
                  width: 100%;
                  min-width: 580px;
                }
            
                table {
                  border-spacing: 0;
                  border-collapse: collapse;
                }
            
                td {
                  word-wrap: break-word;
                  -webkit-hyphens: auto;
                  -moz-hyphens: auto;
                  hyphens: auto;
                  border-collapse: collapse !important;
                }
            
                table,
                tr,
                td {
                  padding: 0;
                  vertical-align: top;
                  text-align: left;
                }
            
                @media only screen {
                  html {
                    min-height: 100%;
                    background: white;
                  }
                }
            
                table.body {
                  background: white;
                  height: 100%;
                  width: 100%;
                  border: none;
                }
            
                table.row {
                  padding: 0;
                  width: 100%;
                  position: relative;
                }
            
                h2.text-center {
                  text-align: center;
                }
            
                table.float-center,
                td.float-center {
                  margin: 0 auto;
                  Margin: 0 auto;
                  float: none;
                  text-align: center;
                }
            
                body,
                table.body,
                h2,
                td,
                th {
                  color: #0a0a0a;
                  font-family: Helvetica, Arial, sans-serif;
                  font-weight: normal;
                  padding: 0;
                  margin: 0;
                  Margin: 0;
                  text-align: left;
                  line-height: 1.3;
                }
            
                h2 {
                  color: inherit;
                  word-wrap: normal;
                  font-family: Helvetica, Arial, sans-serif;
                  font-weight: normal;
                  margin-bottom: 10px;
                  Margin-bottom: 10px;
                }
            
                h2 {
                  font-size: 30px;
                }
            
                body,
                table.body,
                td,
                th {
                  font-size: 16px;
                  line-height: 1.3;
                }
            
                @media only screen and (max-width: 596px) {
                  table.body center {
                    min-width: 0 !important;
                  }
                }
                table {
                  font-family: arial, sans-serif;
                  border-collapse: collapse;
                  width: 100%;
                }
                table {
                  font-family: arial, sans-serif;
                  border-collapse: collapse;
                  width: 100%;
                }
              
                td,
                th {
                  border: 1px solid #afafaf;
                  text-align: left;
                  padding: 8px;
                }
              
                tr:nth-child(even) {
                  background-color: #f9f9f9;
                }
            
                .header > .list1{
                  background: #80DEEA;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                  display: table-cell;
                  vertical-align: middle;
                }
                .header > .list2{
                  background: #81D4FA;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                  display: table-cell;
                  vertical-align: middle;
                }
                .subheader > .list1{
                  background: #4DD0E1;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                }
                .subheader > .list2{
                  background: #4FC3F7;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                }
                .noborder{
                  border: none;
                }
            
                
              </style>
            </head>
            
            <body>
              <table class="body" data-made-with-foundation="">
                <tr>
                  <td class="float-center noborder" align="center" valign="top">
                    <center data-parsed="">
                      <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                        <h2 class="text-center">Based on Cities</h2></td>
                      </tr></tbody></table>

                      '.$city_table_accepted.'
                       <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                            <h2 class="text-center">Based on Vehicles</h2></td>
                       </tr></tbody></table>
                     
                     '.$vehicle_table_accepted.'
                     <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                     <h2 class="text-center">Based on Segment</h2></td>
                   </tr></tbody></table>
                      
                      '.$master_service_table_accepted.'
            
                      </center>
                      </td>
                    </tr>
                  </table>
                </body>
                
                </html>';
            $body = preg_replace("[\\\]",'',$body);
            $mail1->MsgHTML($body);

            //recipient
            $mail1->AddAddress($to1,'Credits Consumption Report');
            //$mail1->AddBCC("metrics@gobumpr.com","Sirisha");
            //$mail1->AddBCC("metrics@gobumpr.com","Mukund");
            //$mail1->AddBCC("sales@gobumpr.com",$shop_name);

            if ($mail1->Send()) {
                echo "Credit Consumption Report mail sent!<br>";
            }
        }
        if($to2!=''||$to2!=NULL){
            //SMTP Settings
            $mail2 = new PHPMailer();
            $mail2->IsSMTP();
            $mail2->SMTPAuth   = true;
            $mail2->SMTPSecure = "SSL";
            $mail2->Host       = "smtp.pepipost.com";
            $mail2->Username   = "letsgobumpr";
            $mail2->Password   = "GoBumpr25000!";
            $mail2->isHTML(true);

            $mail2->SetFrom('admin@gobumpr.com', 'Admin'); //from (verified email address)
            $mail2->Subject = "Premium Performance Report - ".date('d M Y',strtotime($today));
            $body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            
            <head>
              <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
              <meta name="viewport" content="width=device-width">
              
              <style>
                .wrapper {
                  width: 100%;
                }
            
                body {
                  width: 100% !important;
                  min-width: 100%;
                  -webkit-text-size-adjust: 100%;
                  -ms-text-size-adjust: 100%;
                  margin: 0;
                  Margin: 0;
                  padding: 0;
                  -moz-box-sizing: border-box;
                  -webkit-box-sizing: border-box;
                  box-sizing: border-box;
                }
            
                center {
                  width: 100%;
                  min-width: 580px;
                }
            
                table {
                  border-spacing: 0;
                  border-collapse: collapse;
                }
            
                td {
                  word-wrap: break-word;
                  -webkit-hyphens: auto;
                  -moz-hyphens: auto;
                  hyphens: auto;
                  border-collapse: collapse !important;
                }
            
                table,
                tr,
                td {
                  padding: 0;
                  vertical-align: top;
                  text-align: left;
                }
            
                @media only screen {
                  html {
                    min-height: 100%;
                    background: white;
                  }
                }
            
                table.body {
                  background: white;
                  height: 100%;
                  width: 100%;
                  border: none;
                }
            
                table.row {
                  padding: 0;
                  width: 100%;
                  position: relative;
                }
            
                h2.text-center {
                  text-align: center;
                }
            
                table.float-center,
                td.float-center {
                  margin: 0 auto;
                  Margin: 0 auto;
                  float: none;
                  text-align: center;
                }
            
                body,
                table.body,
                h2,
                td,
                th {
                  color: #0a0a0a;
                  font-family: Helvetica, Arial, sans-serif;
                  font-weight: normal;
                  padding: 0;
                  margin: 0;
                  Margin: 0;
                  text-align: left;
                  line-height: 1.3;
                }
            
                h2 {
                  color: inherit;
                  word-wrap: normal;
                  font-family: Helvetica, Arial, sans-serif;
                  font-weight: normal;
                  margin-bottom: 10px;
                  Margin-bottom: 10px;
                }
            
                h2 {
                  font-size: 30px;
                }
            
                body,
                table.body,
                td,
                th {
                  font-size: 16px;
                  line-height: 1.3;
                }
            
                @media only screen and (max-width: 596px) {
                  table.body center {
                    min-width: 0 !important;
                  }
                }
                table {
                  font-family: arial, sans-serif;
                  border-collapse: collapse;
                  width: 100%;
                }
                table {
                  font-family: arial, sans-serif;
                  border-collapse: collapse;
                  width: 100%;
                }
              
                td,
                th {
                  border: 1px solid #afafaf;
                  text-align: left;
                  padding: 8px;
                }
              
                tr:nth-child(even) {
                  background-color: #f9f9f9;
                }
            
                .header > .list1{
                  background: #80DEEA;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                  display: table-cell;
                  vertical-align: middle;
                }
                .header > .list2{
                  background: #81D4FA;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                  display: table-cell;
                  vertical-align: middle;
                }
                .subheader > .list1{
                  background: #4DD0E1;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                }
                .subheader > .list2{
                  background: #4FC3F7;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                }
                .noborder{
                  border: none;
                }
            
                
              </style>
            </head>
            
            <body>
              <table class="body" data-made-with-foundation="">
                <tr>
                  <td class="float-center noborder" align="center" valign="top">
                    <center data-parsed="">
                      <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                        <h2 class="text-center">Based on Cities</h2></td>
                      </tr></tbody></table>
                      
                      '.$city_table_accepted_pre.'
                      <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                      <h2 class="text-center">Based on Vehicles</h2></td>
                    </tr></tbody></table>
                     
                     '.$vehicle_table_accepted_pre.'
                     <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                     <h2 class="text-center">Based on Service Centers</h2></td>
                   </tr></tbody></table>
                      
                      '.$shop_name_table_accepted_pre.'
                       <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                        <h2 class="text-center">Based on Segments</h2></td>
                      </tr></tbody></table>
                      
                      '.$master_service_table_accepted_pre.'
            
                    </center>
                  </td>
                </tr>
              </table>
            </body>
            </html>';
            $body = preg_replace("[\\\]",'',$body);
            $mail2->MsgHTML($body);

            //recipient
            $mail2->AddAddress($to2,'Premium Performance Report');
            //$mail2->AddBCC("metrics@gobumpr.com","Sirisha");
            //$mail2->AddBCC("metrics@gobumpr.com","Mukund");
            //$mail2->AddBCC("sales@gobumpr.com",$shop_name);

            if ($mail2->Send()) {
                echo "Premium Performance Report mail sent!<br>";
            }
        }
        /*
        if($to3!=''||$to3!=NULL){
            //SMTP Settings
            $mail3 = new PHPMailer();
            $mail3->IsSMTP();
            $mail3->SMTPAuth   = true;
            $mail3->SMTPSecure = "SSL";
            $mail3->Host       = "smtp.pepipost.com";
            $mail3->Username   = "letsgobumpr";
            $mail3->Password   = "GoBumpr25000!";
            $mail3->isHTML(true);

            $mail3->SetFrom('admin@gobumpr.com', 'Admin'); //from (verified email address)
            $mail3->Subject = "GoAxle Rejection Report - ".date('d M Y',strtotime($today));
            $body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            
            <head>
              <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
              <meta name="viewport" content="width=device-width">
              
              <style>
                .wrapper {
                  width: 100%;
                }
            
                body {
                  width: 100% !important;
                  min-width: 100%;
                  -webkit-text-size-adjust: 100%;
                  -ms-text-size-adjust: 100%;
                  margin: 0;
                  Margin: 0;
                  padding: 0;
                  -moz-box-sizing: border-box;
                  -webkit-box-sizing: border-box;
                  box-sizing: border-box;
                }
            
                center {
                  width: 100%;
                  min-width: 580px;
                }
            
                table {
                  border-spacing: 0;
                  border-collapse: collapse;
                }
            
                td {
                  word-wrap: break-word;
                  -webkit-hyphens: auto;
                  -moz-hyphens: auto;
                  hyphens: auto;
                  border-collapse: collapse !important;
                }
            
                table,
                tr,
                td {
                  padding: 0;
                  vertical-align: top;
                  text-align: left;
                }
            
                @media only screen {
                  html {
                    min-height: 100%;
                    background: white;
                  }
                }
            
                table.body {
                  background: white;
                  height: 100%;
                  width: 100%;
                  border: none;
                }
            
                table.row {
                  padding: 0;
                  width: 100%;
                  position: relative;
                }
            
                h2.text-center {
                  text-align: center;
                }
            
                table.float-center,
                td.float-center {
                  margin: 0 auto;
                  Margin: 0 auto;
                  float: none;
                  text-align: center;
                }
            
                body,
                table.body,
                h2,
                td,
                th {
                  color: #0a0a0a;
                  font-family: Helvetica, Arial, sans-serif;
                  font-weight: normal;
                  padding: 0;
                  margin: 0;
                  Margin: 0;
                  text-align: left;
                  line-height: 1.3;
                }
            
                h2 {
                  color: inherit;
                  word-wrap: normal;
                  font-family: Helvetica, Arial, sans-serif;
                  font-weight: normal;
                  margin-bottom: 10px;
                  Margin-bottom: 10px;
                }
            
                h2 {
                  font-size: 30px;
                }
            
                body,
                table.body,
                td,
                th {
                  font-size: 16px;
                  line-height: 1.3;
                }
            
                @media only screen and (max-width: 596px) {
                  table.body center {
                    min-width: 0 !important;
                  }
                }
                table {
                  font-family: arial, sans-serif;
                  border-collapse: collapse;
                  width: 100%;
                }
                table {
                  font-family: arial, sans-serif;
                  border-collapse: collapse;
                  width: 100%;
                }
              
                td,
                th {
                  border: 1px solid #afafaf;
                  text-align: left;
                  padding: 8px;
                }
              
                tr:nth-child(even) {
                  background-color: #f9f9f9;
                }
            
                .header > .list1{
                  background: #80DEEA;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                  display: table-cell;
                  vertical-align: middle;
                }
                .header > .list2{
                  background: #81D4FA;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                  display: table-cell;
                  vertical-align: middle;
                }
                .subheader > .list1{
                  background: #4DD0E1;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                }
                .subheader > .list2{
                  background: #4FC3F7;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                }
                .noborder{
                  border: none;
                }
            
                
              </style>
            </head>
            
            <body>
              <table class="body" data-made-with-foundation="">
                <tr>
                  <td class="float-center noborder" align="center" valign="top">
                    <center data-parsed="">
                      <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                     <h2 class="text-center">Based on Service Centers</h2></td>
                   </tr></tbody></table>
                      
                      '.$shop_name_table_rejected.'
                      <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                      <h2 class="text-center">Based on Vehicles</h2></td>
                    </tr></tbody></table>
                     
                     '.$vehicle_table_rejected.'
                      <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                      <h2 class="text-center">Based on Segments</h2></td>
                    </tr></tbody></table>
                      
                      '.$master_service_table_rejected.'
                      <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                        <h2 class="text-center">Based on Cities</h2></td>
                      </tr></tbody></table>
                      
                      '.$city_table_rejected.'
            
                    </center>
                  </td>
                </tr>
              </table>
            </body>
            </html>';
            $body = preg_replace("[\\\]",'',$body);
            $mail3->MsgHTML($body);

            //recipient
            $mail3->AddAddress($to3,'GoAxle Rejection Report');
            //$mail3->AddBCC("metrics@gobumpr.com","Sirisha");
            //$mail3->AddBCC("metrics@gobumpr.com","Mukund");
            //$mail3->AddBCC("sales@gobumpr.com",$shop_name);

            if ($mail3->Send()) {
                echo "GoAxle Rejection Report mail sent!<br>";
            }
        }
        
        if($to4!=''||$to4!=NULL){
            //SMTP Settings
            $mail4 = new PHPMailer();
            $mail4->IsSMTP();
            $mail4->SMTPAuth   = true;
            $mail4->SMTPSecure = "SSL";
            $mail4->Host       = "smtp.pepipost.com";
            $mail4->Username   = "letsgobumpr";
            $mail4->Password   = "GoBumpr25000!";
            $mail4->isHTML(true);

            $mail4->SetFrom('admin@gobumpr.com', 'Admin'); //from (verified email address)
            $mail4->Subject = "GoAxle Idle Report - ".date('d M Y',strtotime($today));
            $body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            
            <head>
              <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
              <meta name="viewport" content="width=device-width">
              
              <style>
                .wrapper {
                  width: 100%;
                }
            
                body {
                  width: 100% !important;
                  min-width: 100%;
                  -webkit-text-size-adjust: 100%;
                  -ms-text-size-adjust: 100%;
                  margin: 0;
                  Margin: 0;
                  padding: 0;
                  -moz-box-sizing: border-box;
                  -webkit-box-sizing: border-box;
                  box-sizing: border-box;
                }
            
                center {
                  width: 100%;
                  min-width: 580px;
                }
            
                table {
                  border-spacing: 0;
                  border-collapse: collapse;
                }
            
                td {
                  word-wrap: break-word;
                  -webkit-hyphens: auto;
                  -moz-hyphens: auto;
                  hyphens: auto;
                  border-collapse: collapse !important;
                }
            
                table,
                tr,
                td {
                  padding: 0;
                  vertical-align: top;
                  text-align: left;
                }
            
                @media only screen {
                  html {
                    min-height: 100%;
                    background: white;
                  }
                }
            
                table.body {
                  background: white;
                  height: 100%;
                  width: 100%;
                  border: none;
                }
            
                table.row {
                  padding: 0;
                  width: 100%;
                  position: relative;
                }
            
                h2.text-center {
                  text-align: center;
                }
            
                table.float-center,
                td.float-center {
                  margin: 0 auto;
                  Margin: 0 auto;
                  float: none;
                  text-align: center;
                }
            
                body,
                table.body,
                h2,
                td,
                th {
                  color: #0a0a0a;
                  font-family: Helvetica, Arial, sans-serif;
                  font-weight: normal;
                  padding: 0;
                  margin: 0;
                  Margin: 0;
                  text-align: left;
                  line-height: 1.3;
                }
            
                h2 {
                  color: inherit;
                  word-wrap: normal;
                  font-family: Helvetica, Arial, sans-serif;
                  font-weight: normal;
                  margin-bottom: 10px;
                  Margin-bottom: 10px;
                }
            
                h2 {
                  font-size: 30px;
                }
            
                body,
                table.body,
                td,
                th {
                  font-size: 16px;
                  line-height: 1.3;
                }
            
                @media only screen and (max-width: 596px) {
                  table.body center {
                    min-width: 0 !important;
                  }
                }
                table {
                  font-family: arial, sans-serif;
                  border-collapse: collapse;
                  width: 100%;
                }
                table {
                  font-family: arial, sans-serif;
                  border-collapse: collapse;
                  width: 100%;
                }
              
                td,
                th {
                  border: 1px solid #afafaf;
                  text-align: left;
                  padding: 8px;
                }
              
                tr:nth-child(even) {
                  background-color: #f9f9f9;
                }
            
                .header > .list1{
                  background: #80DEEA;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                  display: table-cell;
                  vertical-align: middle;
                }
                .header > .list2{
                  background: #81D4FA;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                  display: table-cell;
                  vertical-align: middle;
                }
                .subheader > .list1{
                  background: #4DD0E1;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                }
                .subheader > .list2{
                  background: #4FC3F7;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                }
                .noborder{
                  border: none;
                }
            
                
              </style>
            </head>
            
            <body>
              <table class="body" data-made-with-foundation="">
                <tr>
                  <td class="float-center noborder" align="center" valign="top">
                    <center data-parsed="">
                     
                    
                     <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                     <h2 class="text-center">Based on Service Centers</h2></td>
                   </tr></tbody></table>
                      
                      '.$shop_name_table_idle.'
                      <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                      <h2 class="text-center">Based on Vehicles</h2></td>
                    </tr></tbody></table>

                     '.$vehicle_table_idle.'
                      <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                      <h2 class="text-center">Based on Segment</h2></td>
                    </tr></tbody></table>
                      
                      '.$master_service_table_idle.'
                      <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                      <h2 class="text-center">Based on Cities</h2></td>
                    </tr></tbody></table>
                    
                    '.$city_table_idle.'
            
                    </center>
                  </td>
                </tr>
              </table>
            </body>
            </html>';
            $body = preg_replace("[\\\]",'',$body);
            $mail4->MsgHTML($body);

            //recipient
            $mail4->AddAddress($to4,'GoAxle Idle Report');
            //$mail4->AddBCC("metrics@gobumpr.com","Sirisha");
            //$mail4->AddBCC("metrics@gobumpr.com","Mukund");
            //$mail4->AddBCC("sales@gobumpr.com",$shop_name);

            if ($mail4->Send()) {
                echo "GoAxle Idle Report mail sent!<br>";
            }
        }
        */
        if($to5!=''||$to5!=NULL){
            //SMTP Settings
            $mail5 = new PHPMailer();
            $mail5->IsSMTP();
            $mail5->SMTPAuth   = true;
            $mail5->SMTPSecure = "SSL";
            $mail5->Host       = "smtp.pepipost.com";
            $mail5->Username   = "letsgobumpr";
            $mail5->Password   = "GoBumpr25000!";
            $mail5->isHTML(true);

            $mail5->SetFrom('admin@gobumpr.com', 'Admin'); //from (verified email address)
            $mail5->Subject = "Customer Experience Personal Performance Report - ".date('d M Y',strtotime($today));
            $body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            
            <head>
              <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
              <meta name="viewport" content="width=device-width">
              
              <style>
                .wrapper {
                  width: 100%;
                }
            
                body {
                  width: 100% !important;
                  min-width: 100%;
                  -webkit-text-size-adjust: 100%;
                  -ms-text-size-adjust: 100%;
                  margin: 0;
                  Margin: 0;
                  padding: 0;
                  -moz-box-sizing: border-box;
                  -webkit-box-sizing: border-box;
                  box-sizing: border-box;
                }
            
                center {
                  width: 100%;
                  min-width: 580px;
                }
            
                table {
                  border-spacing: 0;
                  border-collapse: collapse;
                }
            
                td {
                  word-wrap: break-word;
                  -webkit-hyphens: auto;
                  -moz-hyphens: auto;
                  hyphens: auto;
                  border-collapse: collapse !important;
                }
            
                table,
                tr,
                td {
                  padding: 0;
                  vertical-align: top;
                  text-align: left;
                }
            
                @media only screen {
                  html {
                    min-height: 100%;
                    background: white;
                  }
                }
            
                table.body {
                  background: white;
                  height: 100%;
                  width: 100%;
                  border: none;
                }
            
                table.row {
                  padding: 0;
                  width: 100%;
                  position: relative;
                }
            
                h2.text-center {
                  text-align: center;
                }
            
                table.float-center,
                td.float-center {
                  margin: 0 auto;
                  Margin: 0 auto;
                  float: none;
                  text-align: center;
                }
            
                body,
                table.body,
                h2,
                td,
                th {
                  color: #0a0a0a;
                  font-family: Helvetica, Arial, sans-serif;
                  font-weight: normal;
                  padding: 0;
                  margin: 0;
                  Margin: 0;
                  text-align: left;
                  line-height: 1.3;
                }
            
                h2 {
                  color: inherit;
                  word-wrap: normal;
                  font-family: Helvetica, Arial, sans-serif;
                  font-weight: normal;
                  margin-bottom: 10px;
                  Margin-bottom: 10px;
                }
            
                h2 {
                  font-size: 30px;
                }
            
                body,
                table.body,
                td,
                th {
                  font-size: 16px;
                  line-height: 1.3;
                }
            
                @media only screen and (max-width: 596px) {
                  table.body center {
                    min-width: 0 !important;
                  }
                }
                table {
                  font-family: arial, sans-serif;
                  border-collapse: collapse;
                  width: 100%;
                }
                table {
                  font-family: arial, sans-serif;
                  border-collapse: collapse;
                  width: 100%;
                }
              
                td,
                th {
                  border: 1px solid #afafaf;
                  text-align: left;
                  padding: 8px;
                }
              
                tr:nth-child(even) {
                  background-color: #f9f9f9;
                }
            
                .header > .list1{
                  background: #80DEEA;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                  display: table-cell;
                  vertical-align: middle;
                }
                .header > .list2{
                  background: #81D4FA;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                  display: table-cell;
                  vertical-align: middle;
                }
                .subheader > .list1{
                  background: #4DD0E1;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                }
                .subheader > .list2{
                  background: #4FC3F7;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                }
                .noborder{
                  border: none;
                }
            
                
              </style>
            </head>
            
            <body>
              <table class="body" data-made-with-foundation="">
                <tr>
                  <td class="float-center noborder" align="center" valign="top">
                    <center data-parsed="">
                      <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                        <h2 class="text-center">Based on Cities</h2></td>
                      </tr></tbody></table>
                      
                      '.$city_table_accepted.'
                      <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                        <h2 class="text-center">Based on Vehicles</h2></td>
                      </tr></tbody></table>
                     
                     '.$vehicle_table_accepted.'
                     <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                     <h2 class="text-center">Based on Support Folks</h2></td>
                   </tr></tbody></table>
                      
                      '.$person_table_accepted.'
                      <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                      <h2 class="text-center">Based on Segments</h2></td>
                    </tr></tbody></table>
                      
                      '.$master_service_table_accepted.'
            
                    </center>
                  </td>
                </tr>
              </table>
            </body>
            </html>';
            $body = preg_replace("[\\\]",'',$body);
            $mail5->MsgHTML($body);

            //recipient
            $mail5->AddAddress($to5,'Customer Experience Personal Performance Report');
            //$mail5->AddBCC("metrics@gobumpr.com","Sirisha");
            //$mail5->AddBCC("metrics@gobumpr.com","Mukund");
            //$mail5->AddBCC("sales@gobumpr.com",$shop_name);

            if ($mail5->Send()) {
                echo "Customer Experience Personal Performance Report mail sent!<br>";
            }
        }
       
    }
    

}// if results exists
else{
    echo '<h3 align="center" style="margin-top:140px;">Sorry! No results found to take furthur operations.</h3>';
}

// ********************************** axle reports ********************************************
$sql2 = "select b.b2b_log,b.b2b_booking_id,b.gb_booking_id,m.b2b_shop_name,m.b2b_renw_date,m.b2b_address5,m.b2b_vehicle_type from b2b.b2b_booking_tbl as b inner join b2b.b2b_mec_tbl as m on b.b2b_shop_id=m.b2b_shop_id where b.b2b_log BETWEEN '$three_m_ago' AND '$today' AND b.b2b_swap_flag!='1' AND b.b2b_shop_id NOT IN (1014,1670,1035,1712)";
$res2 = mysqli_query($conn,$sql2);

if(mysqli_num_rows($res2) >= 1){ 
    while($row2 = mysqli_fetch_object($res2)){
        $gb_booking_id = $row2->gb_booking_id;

        $garage_arr[] = array("garage_name" =>ucwords(strtolower($row2->b2b_shop_name)), "garage_city" =>ucwords(strtolower($row2->b2b_address5)), "renew"=>strtotime($row2->b2b_renw_date),"vehicle"=> $row2->b2b_vehicle_type);

        if($gb_booking_id == 0){
            //boookings
            $data_unique_garages_bookings[]=array("shop"=>ucwords(strtolower($row2->b2b_shop_name)),"booking"=>$row2->b2b_booking_id,"log"=>$row2->b2b_log,"vehicle"=> $row2->b2b_vehicle_type);
        }
        else{
            //leads
            $data_unique_garages_leads[]=array("shop"=>ucwords(strtolower($row2->b2b_shop_name)),"booking"=>$row2->b2b_booking_id,"log"=>$row2->b2b_log,"vehicle"=> $row2->b2b_vehicle_type);
        }
    }

    $unique_garages = array_map('unserialize', array_unique(array_map('serialize', $garage_arr)));

    //top 10 garages
    {
        
        $top_garages_table = '<table align="center" class="float-center">
        <tr class="header">
        <th rowspan="2" align="center">Garage</th>
        <th rowspan="2" align="center">Type</th>
        <th rowspan="2" align="center">City</th>
        <th colspan="3">Last Week</th>
        <th colspan="3">Previous Week</th>
        <th colspan="3">This Month</th>
        <th colspan="3">Last Month</th>
        <th colspan="3">Last 3 Months</th>
      </tr>
      <tr class="subheader">
        <td>B</td>
        <td>L</td>
        <td>T</td>
        <td>B</td>
        <td>L</td>
        <td>T</td>
        <td>B</td>
        <td>L</td>
        <td>T</td>
        <td>B</td>
        <td>L</td>
        <td>T</td>
        <td>B</td>
        <td>L</td>
        <td>T</td>
      </tr>';
            $top5_garages_table = ' <table align="center" class="float-center">
            <tr class="header">
            <th align="center">Garage</th>
            <th>City</th>
            <th>Renewal Date</th>
            </tr>';
        foreach($unique_garages as $value_unique_garages){

            $garage_name = $value_unique_garages['garage_name'];
            $city = $value_unique_garages['garage_city'];
            $vehicle = $value_unique_garages['vehicle'];
            
            $bookings_lw_unique_garages = 0 ;$leads_lw_unique_garages = 0 ;
            $bookings_pw_unique_garages = 0 ;$leads_pw_unique_garages = 0 ;
            $bookings_tm_unique_garages = 0 ;$leads_tm_unique_garages = 0 ;
            $bookings_lm_unique_garages = 0 ;$leads_lm_unique_garages = 0 ; 
            $bookings_l3m_unique_garages = 0 ;$leads_l3m_unique_garages = 0 ;

            //bookings
            foreach ($data_unique_garages_bookings as $key_unique_garages_bookings){
                $shop_unique_garages_bookings = $key_unique_garages_bookings['shop'];
                $booking_unique_garages_bookings = $key_unique_garages_bookings['booking'];
                $log_unique_garages_bookings = $key_unique_garages_bookings['log'];
                
                if($log_unique_garages_bookings<$today && $log_unique_garages_bookings>= $last_week){
                    if($shop_unique_garages_bookings == $garage_name){
                        $bookings_lw_unique_garages = $bookings_lw_unique_garages+1;
                    }
                }
                if($log_unique_garages_bookings<$last_week && $log_unique_garages_bookings>= $previous_week){
                    if($shop_unique_garages_bookings == $garage_name){
                        $bookings_pw_unique_garages = $bookings_pw_unique_garages+1;
                    }
                }
                if($log_unique_garages_bookings<$today && $log_unique_garages_bookings>= $this_month){
                    if($shop_unique_garages_bookings == $garage_name){
                        $bookings_tm_unique_garages = $bookings_tm_unique_garages+1;
                    }
                }
                if($log_unique_garages_bookings<$this_month && $log_unique_garages_bookings>= $last_month){
                    if($shop_unique_garages_bookings == $garage_name){
                        $bookings_lm_unique_garages = $bookings_lm_unique_garages+1;
                    }
                }
                if($log_unique_garages_bookings<$this_month && $log_unique_garages_bookings>= $three_m_ago){
                    if($shop_unique_garages_bookings == $garage_name){
                        $bookings_l3m_unique_garages = $bookings_l3m_unique_garages+1;
                    }
                }
            }//foreach data

            //leads
            foreach ($data_unique_garages_leads as $key_unique_garages_leads){
                $shop_unique_garages_leads = $key_unique_garages_leads['shop'];
                $booking_unique_garages_leads = $key_unique_garages_leads['booking'];
                $log_unique_garages_leads = $key_unique_garages_leads['log'];
                
                if($log_unique_garages_leads<$today && $log_unique_garages_leads>= $last_week){
                    if($shop_unique_garages_leads == $garage_name){
                        $leads_lw_unique_garages = $leads_lw_unique_garages+1;
                    }
                }
                if($log_unique_garages_leads<$last_week && $log_unique_garages_leads>= $previous_week){
                    if($shop_unique_garages_leads == $garage_name){
                        $leads_pw_unique_garages = $leads_pw_unique_garages+1;
                    }
                }
                if($log_unique_garages_leads<$today && $log_unique_garages_leads>= $this_month){
                    if($shop_unique_garages_leads == $garage_name){
                        $leads_tm_unique_garages = $leads_tm_unique_garages+1;
                    }
                }
                if($log_unique_garages_leads<$this_month && $log_unique_garages_leads>= $last_month){
                    if($shop_unique_garages_leads == $garage_name){
                        $leads_lm_unique_garages = $leads_lm_unique_garages+1;
                    }
                }
                if($log_unique_garages_leads<$this_month && $log_unique_garages_leads>= $three_m_ago){
                    if($shop_unique_garages_leads == $garage_name){
                        $leads_l3m_unique_garages = $leads_l3m_unique_garages+1;
                    }
                }
            }//foreach data

            $total_lw_unique_garages = $bookings_lw_unique_garages+$leads_lw_unique_garages;
            $total_pw_unique_garages = $bookings_pw_unique_garages+$leads_pw_unique_garages;
            $total_tm_unique_garages = $bookings_tm_unique_garages+$leads_tm_unique_garages;
            $total_lm_unique_garages = $bookings_lm_unique_garages+$leads_lm_unique_garages; 
            $total_l3m_unique_garages = $bookings_l3m_unique_garages+$leads_l3m_unique_garages;

            $total = $total_lw_unique_garages + $total_pw_unique_garages + $total_tm_unique_garages + $total_lm_unique_garages + $total_l3m_unique_garages;

            $top_garage_arr[] = array("g"=>$garage_name,"city"=>$city,"vehicle"=>$vehicle,"blw"=>$bookings_lw_unique_garages,"llw"=>$leads_lw_unique_garages,"tlw"=>$total_lw_unique_garages,"bpw"=>$bookings_pw_unique_garages,"lpw"=>$leads_pw_unique_garages,"tpw"=>$total_pw_unique_garages,"btm"=>$bookings_tm_unique_garages,"ltm"=>$leads_tm_unique_garages,"ttm"=>$total_tm_unique_garages,"blm"=>$bookings_lm_unique_garages,"llm"=>$leads_lm_unique_garages,"tlm"=>$total_lm_unique_garages,"bl3m"=>$bookings_l3m_unique_garages,"ll3m"=>$leads_l3m_unique_garages,"tl3m"=>$total_l3m_unique_garages,"total"=>$total);

        }// for each

       // Obtain a list of columns for top 10 garages
        foreach ($top_garage_arr as $key => $row) {
            $garage[$key]  = $row['total'];
        }

        // Sort the data with volume descending, edition ascending
        // Add $data as the last parameter, to sort by the common key
        array_multisort($garage, SORT_DESC, $top_garage_arr);

        $count =10;

        foreach($top_garage_arr as $val){
            if($count>0){
                $top_garages_table = $top_garages_table.'<tr><th>'.$val['g'].'</th><td>'.$val['vehicle'].'</td><td>'.$val['city'].'</td><td>'.$val['blw'].'</td><td>'.$val['llw'].'</td><td>'.$val['tlw'].'</td><td>'.$val['bpw'].'</td><td>'.$val['lpw'].'</td><td>'.$val['tpw'].'</td><td>'.$val['btm'].'</td><td>'.$val['ltm'].'</td><td>'.$val['ttm'].'</td><td>'.$val['blm'].'</td><td>'.$val['llm'].'</td><td>'.$val['tlm'].'</td><td>'.$val['bl3m'].'</td><td>'.$val['ll3m'].'</td><td>'.$val['tl3m'].'</td></tr>';                
            }
            else{
                break;
            }
            $count--;            
        }

        $top_garages_table = $top_garages_table.'</tbody></table>';
        //echo $top_garages_table;

        // Obtain a list of columns for top 5 garages
        $top5_garages = $unique_garages;
        // Obtain a list of columns for top 10 garages
        foreach ( $top5_garages as $key => $row) {
                $garage_r[$key]  = $row['renew'];                
        }

        // Sort the data with volume descending, edition ascending
        // Add $data as the last parameter, to sort by the common key
        array_multisort($garage_r, SORT_ASC,  $top5_garages);
        
        foreach($top5_garages as $val1){
            if($val1['renew']<= strtotime($today) || $val1['renew']> strtotime($this_month_last)){
                continue;
            }
            else{
                    $top5_garages_table = $top5_garages_table.'<tr><th>'.$val1['garage_name'].'</th><td>'.$val1['garage_city'].'</td><td>'.date('d M Y',$val1['renew']).'</td></tr>';                
            }
        }
        $top5_garages_table = $top5_garages_table.'</tbody></table>';
       // echo $top5_garages_table;

    }
    // ************************************** send mails *****************************************
    $to6='metrics@gobumpr.com';
    $to7='metrics@gobumpr.com';


    if($to6!=''||$to6!=NULL){
        //SMTP Settings
        $mail6 = new PHPMailer();
        $mail6->IsSMTP();
        $mail6->SMTPAuth   = true;
        $mail6->SMTPSecure = "SSL";
        $mail6->Host       = "smtp.pepipost.com";
        $mail6->Username   = "letsgobumpr";
        $mail6->Password   = "GoBumpr25000!";
        $mail6->isHTML(true);

        $mail6->SetFrom('admin@gobumpr.com', 'Admin'); //from (verified email address)
        $mail6->Subject = "Axle Usage Metrics Report - ".date('d M Y',strtotime($today));
        $body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
          <meta name="viewport" content="width=device-width">
          <style>
            .wrapper {
              width: 100%;
            }
        
            body {
              width: 100% !important;
              min-width: 100%;
              -webkit-text-size-adjust: 100%;
              -ms-text-size-adjust: 100%;
              margin: 0;
              Margin: 0;
              padding: 0;
              -moz-box-sizing: border-box;
              -webkit-box-sizing: border-box;
              box-sizing: border-box;
            }
        
            center {
              width: 100%;
              min-width: 580px;
            }
        
            table {
              border-spacing: 0;
              border-collapse: collapse;
            }
        
            td {
              word-wrap: break-word;
              -webkit-hyphens: auto;
              -moz-hyphens: auto;
              hyphens: auto;
              border-collapse: collapse !important;
            }
        
            table,
            tr,
            td {
              padding: 0;
              vertical-align: top;
              text-align: left;
            }
        
            @media only screen {
              html {
                min-height: 100%;
                background: white;
              }
            }
        
            table.body {
              background: white;
              height: 100%;
              width: 100%;
              border:none;
            }
        
            table.row {
              padding: 0;
              width: 100%;
              position: relative;
            }
        
            h2.text-center {
              text-align: center;
            }
        
            table.float-center,
            td.float-center {
              margin: 0 auto;
              Margin: 0 auto;
              float: none;
              text-align: center;
            }
        
            body,
            table.body,
            h2,
            td,
            th {
              color: #0a0a0a;
              font-family: Helvetica, Arial, sans-serif;
              font-weight: normal;
              padding: 0;
              margin: 0;
              Margin: 0;
              text-align: left;
              line-height: 1.3;
            }
        
            h2 {
              color: inherit;
              word-wrap: normal;
              font-family: Helvetica, Arial, sans-serif;
              font-weight: normal;
              margin-bottom: 10px;
              Margin-bottom: 10px;
            }
        
            h2 {
              font-size: 30px;
            }
        
            body,
            table.body,
            td,
            th {
              font-size: 16px;
              line-height: 1.3;
            }
        
            @media only screen and (max-width: 596px) {
              table.body center {
                min-width: 0 !important;
              }
            }
            table {
              font-family: arial, sans-serif;
              border-collapse: collapse;
              width: 100%;
            }
            table {
              font-family: arial, sans-serif;
              border-collapse: collapse;
              width: 100%;
            }
          
            td,
            th {
              border: 1px solid #afafaf;
              text-align: left;
              padding: 8px;
            }
          
            tr:nth-child(even) {
              background-color: #f9f9f9;
            }
        
            .header > th{
              background: #B2DFDB;
              color: #0a0a0a;
              font-weight: bold;
              text-align: center;
              border: 1px solid #afafaf;
              display: table-cell;
              vertical-align: middle;
              
            }
            .subheader > td{
              background: rgb(164, 198, 195);
              color: #0a0a0a;
              font-weight: bold;
              text-align: center;
              border: 1px solid #afafaf;
            }
            .noborder{
              border: none;
            }
          </style>
        </head>
        <body>
          <table class="body" data-made-with-foundation="">
            <tr>
              <td class="float-center noborder" align="center" valign="top">
                <center data-parsed="">
                  <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                    <h2 class="text-center">Top 10 Garages Based on Bookings</h2></td>
                  </tr><tr><td style="border-color:white !important;">
                  <h4 class="text-center">B - Axle Bookings | L - Leads | T - Total Bookings (Axle Bookings+Leads)</h4></td>
                </tr></tbody></table>
                  
                  '.$top_garages_table.'
                </center>
              </td>
            </tr>
          </table>
        </body>
        </html>';
        $body = preg_replace("[\\\]",'',$body);
        $mail6->MsgHTML($body);

        //recipient
        $mail6->AddAddress($to6,'Axle Usage Metrics Report');
        //$mail6->AddBCC("metrics@gobumpr.com","Sirisha");
        //$mail6->AddBCC("metrics@gobumpr.com","Mukund");
        //$mail6->AddBCC("sales@gobumpr.com",$shop_name);

        if ($mail6->Send()) {
            echo "Axle Usage Metrics Report mail sent!<br>";
        }
    }
    if($to7!=''||$to7!=NULL){
        //SMTP Settings
        $mail7 = new PHPMailer();
        $mail7->IsSMTP();
        $mail7->SMTPAuth   = true;
        $mail7->SMTPSecure = "SSL";
        $mail7->Host       = "smtp.pepipost.com";
        $mail7->Username   = "letsgobumpr";
        $mail7->Password   = "GoBumpr25000!";
        $mail7->isHTML(true);

        $mail7->SetFrom('admin@gobumpr.com', 'Admin'); //from (verified email address)
        $mail7->Subject = "Axle Renewal Report - ".date('d M Y',strtotime($today));
        $body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
          <meta name="viewport" content="width=device-width">
          <style>
            .wrapper {
              width: 100%;
            }
        
            body {
              width: 100% !important;
              min-width: 100%;
              -webkit-text-size-adjust: 100%;
              -ms-text-size-adjust: 100%;
              margin: 0;
              Margin: 0;
              padding: 0;
              -moz-box-sizing: border-box;
              -webkit-box-sizing: border-box;
              box-sizing: border-box;
            }
        
            center {
              width: 100%;
              min-width: 580px;
            }
        
            table {
              border-spacing: 0;
              border-collapse: collapse;
            }
        
            td {
              word-wrap: break-word;
              -webkit-hyphens: auto;
              -moz-hyphens: auto;
              hyphens: auto;
              border-collapse: collapse !important;
            }
        
            table,
            tr,
            td {
              padding: 0;
              vertical-align: top;
              text-align: left;
            }
        
            @media only screen {
              html {
                min-height: 100%;
                background: white;
              }
            }
        
            table.body {
              background: white;
              height: 100%;
              width: 100%;
              border:none;
            }
        
            table.row {
              padding: 0;
              width: 100%;
              position: relative;
            }
        
            h2.text-center {
              text-align: center;
            }
        
            table.float-center,
            td.float-center {
              margin: 0 auto;
              Margin: 0 auto;
              float: none;
              text-align: center;
            }
        
            body,
            table.body,
            h2,
            td,
            th {
              color: #0a0a0a;
              font-family: Helvetica, Arial, sans-serif;
              font-weight: normal;
              padding: 0;
              margin: 0;
              Margin: 0;
              text-align: left;
              line-height: 1.3;
            }
        
            h2 {
              color: inherit;
              word-wrap: normal;
              font-family: Helvetica, Arial, sans-serif;
              font-weight: normal;
              margin-bottom: 10px;
              Margin-bottom: 10px;
            }
        
            h2 {
              font-size: 30px;
            }
        
            body,
            table.body,
            td,
            th {
              font-size: 16px;
              line-height: 1.3;
            }
        
            @media only screen and (max-width: 596px) {
              table.body center {
                min-width: 0 !important;
              }
            }
            table {
              font-family: arial, sans-serif;
              border-collapse: collapse;
              width: 100%;
            }
            table {
              font-family: arial, sans-serif;
              border-collapse: collapse;
              width: 100%;
            }
          
            td,
            th {
              border: 1px solid #afafaf;
              text-align: left;
              padding: 8px;
            }
          
            tr:nth-child(even) {
              background-color: #f9f9f9;
            }
        
            .header > th{
              background: #B2DFDB;
              color: #0a0a0a;
              font-weight: bold;
              text-align: center;
              border: 1px solid #afafaf;
              display: table-cell;
              vertical-align: middle;
              
            }
            .subheader > td{
              background: rgb(164, 198, 195);
              color: #0a0a0a;
              font-weight: bold;
              text-align: center;
              border: 1px solid #afafaf;
            }
            .noborder{
              border: none;
            }
          </style>
        </head>
        <body>
          <table class="body" data-made-with-foundation="">
            <tr>
              <td class="float-center noborder" align="center" valign="top">
                <center data-parsed="">
                  <table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                    <h2 class="text-center">Axle Users who'."'".'s renewals are in the pipeline this month</h2></td>
                  </tr></tbody></table>
                  
                  '.$top5_garages_table.'
                </center>
              </td>
            </tr>
          </table>
        </body>
        </html>';
        $body = preg_replace("[\\\]",'',$body);
        $mail7->MsgHTML($body);

        //recipient
        $mail7->AddAddress($to7,'Credits Consumption');
        //$mail7->AddBCC("metrics@gobumpr.com","Sirisha");
        //$mail7->AddBCC("metrics@gobumpr.com","Mukund");
        //$mail7->AddBCC("sales@gobumpr.com",$shop_name);

        if ($mail7->Send()) {
            echo "Axle Renewal Report mail sent!";
        }
    }
    
}
else{
    echo '<h3 align="center" style="margin-top:140px;">Sorry! No results found to take furthur operations.</h3>';
}

?>

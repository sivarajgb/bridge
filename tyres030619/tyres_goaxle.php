<?php
include("config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();
$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag = $_SESSION['flag'];
echo $booking_id = base64_decode($_GET['bi']);
date_default_timezone_set('Asia/Kolkata');
$log= date('Y-m-d H-i-s');
$this_month = date('n');
$service_date= date('Y-m-d');
$sql_address = "SELECT locality,pickup_address,mec_id,mec_id_leads,axle_id_leads FROM tyre_booking_tb WHERE booking_id = '$booking_id'";
$res_address = mysqli_query($conn,$sql_address);
while($row_address = mysqli_fetch_object($res_address))
{
	$locality = $row_address->locality;
	$pick_address = $row_address->pickup_address;
	$mec_id_leads = $row_address->mec_id_leads;
	$axle_id_leads= $row_address->axle_id_leads;
}
if($mec_id_leads == '' AND $axle_id_leads ='')
{
$sql_booking = "SELECT l.lat as booking_lat,l.lng as booking_lng,b.*,v.brand,v.model FROM tyre_booking_tb b LEFT JOIN localities l ON l.localities = b.locality LEFT JOIN user_vehicle_table v ON v.id=b.user_veh_id WHERE booking_id = '$booking_id'";
// echo $sql_booking;
// die;

$res_booking = mysqli_query($conn,$sql_booking);
while($row_booking = mysqli_fetch_object($res_booking))
{
	$locality = $row_booking->locality;
	$booking_lat = $row_booking->booking_lat;
	$booking_lng = $row_booking->booking_lng;
	$vehicle_type = $row_booking->vehicle_type;
	$tyre_brand = $row_booking->tyre_brand;
	$tyre_count = $row_booking->tyre_count;
	$city = $row_booking->city;
	echo $axle_flag = $row_booking->axle_flag;
}

if($axle_flag == 1)
{
	header("Location:sent_to_axle.php");
	exit();
}
else
{

function fetch($res_mecs,$brand)
{
	while($row_mec = mysqli_fetch_object($res_mecs))
	{
		$brand_serviced = $row_mec->brand;
		$brandsArr = '';
		if($brand!= "")
		{
			if($brand_serviced != 'all' && $brand_serviced != '')
			{
				$brandsArr = explode(',',$brand_serviced);
				if(!in_array($brand,$brandsArr))
				{
					continue;
				}
			}
			$brand_not_serviced = $row_mec->brand_ns;
			$brandsNoArr = '';
			if($brand_not_serviced != 'all' && $brand_not_serviced != '')
			{
				$brandsNoArr = explode(',',$brand_not_serviced);
				if(in_array($brand,$brandsNoArr))
				{
					continue;
				}
			}
		}
		$mec_id = $row_mec->mec_id;
		$axle_id = $row_mec->axle_id;
		$shop_name = $row_mec->shop_name;
		$mec_lat = $row_mec->lat;
		$mec_lng = $row_mec->lng;
		$pick_range = $row_mec->pick_range;
		$distance = $row_mec->dist;
		$address4 = $row_mec->address4;
		$premium = $row_mec->premium;
		$arr['mec_id'][] = $mec_id;
		$arr['axle_id'][] = $axle_id;
		$arr['shop_name'][] = $shop_name;
	}
	return $arr;
}
//change the vehicle type later - 4w to tyres
$sql_mecs = "SELECT (CASE WHEN b2b.monthly_count IS NOT NULL THEN b2b.monthly_count ELSE 0 END) as monthly_count,m.*,(6371*ACOS(COS(RADIANS('$booking_lat'))*COS(RADIANS(m.lat))*COS(RADIANS(m.lng)-RADIANS('$booking_lng'))+SIN(RADIANS('$booking_lat'))*SIN(RADIANS(m.lat)))) as dist FROM go_bumpr.admin_mechanic_table m LEFT JOIN (SELECT b.b2b_shop_id,count(CASE WHEN b.b2b_booking_id is not null THEN b.b2b_booking_id ELSE 0 END) as monthly_count FROM b2b.b2b_booking_tbl_tyres b WHERE MONTH(b.b2b_log) = '$this_month' GROUP BY b.b2b_shop_id) b2b ON b2b.b2b_shop_id = m.axle_id WHERE m.status=0 AND m.type='tyres' AND m.address5='$city' AND m.axle_id NOT IN (1014,1035,1670,2029) AND m.wkly_counter >0 HAVING monthly_count <250 ORDER BY dist LIMIT 2";
//echo $sql_mecs;
$res_mecs = mysqli_query($conn,$sql_mecs);
$rtn_arr = fetch($res_mecs,$tyre_brand);
if(empty($rtn_arr))
{
	$mec_id_leads = '';
	$axle_id_leads = '';
	$shop_name = '';
}
else
{
	$mec_id_leads = implode(";",$rtn_arr['mec_id']);
	$axle_id_leads = implode(";",$rtn_arr['axle_id']);
	$shop_name = implode(";",$rtn_arr['shop_name']);
}
// $axle_id_leads = '1670;1014;1035';


// foreach($mec_ids_arr as $mec_id)
// {
	// $get_axle_ids = "SELECT axle_id FROM admin_mechanic_table WHERE mec_id = '$mec_id'";
	// $res_axle_ids = mysqli_query($conn,$get_axle_ids);
	// $row_axle_ids = mysqli_fetch_object($res_axle_ids);
	// $axle_id_arr[] = $row_axle_ids->axle_id;
// }

$axle_id_arr = explode(";",$axle_id_leads);
// $axle_id_arr = array('1670','1014','1035');

$sql = "SELECT mec_id,user_id,vech_id,shop_name,user_veh_id,vehicle_type,user_vech_no,service_type,pick_up,service_date,service_description,pickup_address,pickup_full_address,estimate_amt,pickup_date_time,axle_flag,b2b_referral_id,b2b_referral_shop_id FROM tyre_booking_tb WHERE booking_id='$booking_id'";
$query = mysqli_query($conn,$sql);
$row_book = mysqli_fetch_object($query);
$user_id = $row_book->user_id;
$user_veh_id = $row_book->user_veh_id;
$veh_no = $row_book->user_vech_no;


$res_veh = mysqli_query($conn,"SELECT uv.brand,uv.model,uv.vehicle_id,uv.fuel_type,av.tyre_size FROM user_vehicle_table uv LEFT JOIN admin_vehicle_table_new av ON av.id = uv.vehicle_id WHERE uv.id='$user_veh_id'");
$row_veh = mysqli_fetch_object($res_veh);
$veh_brand = $row_veh->brand;
$veh_model = $row_veh->model;
$vehicle_id = $row_veh->vehicle_id;
$fuel_type = $row_veh->fuel_type;
$tyre_size = $row_veh->tyre_size;

$res_user = mysqli_query($conn,"SELECT name,mobile_number,mobile_number2,email_id FROM user_register WHERE reg_id='$user_id'");
$row_user = mysqli_fetch_object($res_user);
$user_name = $row_user->name;
$user_phn = $row_user->mobile_number;
$cust_alt_phn = "+91".$row_user->mobile_number2;
$user_mail = $row_user->email_id;
if($tyre_brand=="Nil"){
		$tyre_brand="";
	}
	
// $now = date('H');
// if($now < 19 && $now >9)
// {
	
	foreach($axle_id_arr as $axle_id)
	{
		$sql_mec_shop="SELECT * FROM b2b.b2b_credits_tbl WHERE b2b_shop_id=$axle_id"; 
	$res_mec_id = mysqli_query($conn2,$sql_mec_shop);
	$row_b2b_mec_id = mysqli_fetch_object($res_mec_id);
	

		if($row_b2b_mec_id->b2b_credits>0){

		$sql_b2b_booking_id = "SELECT max(b2b_booking_id) AS b2b_booking_id FROM b2b_booking_tbl_tyres" ;
		$res_b2b_booking_id = mysqli_query($conn2,$sql_b2b_booking_id);
		$row_b2b_booking_id = mysqli_fetch_object($res_b2b_booking_id);

		$b2b_booking_id=$row_b2b_booking_id->b2b_booking_id;
		$b2b_booking_id=$b2b_booking_id+1;
		
		$sql_s_booking_id = "SELECT max(b2b_shop_booking_id) AS b2b_shop_booking_id FROM b2b_booking_tbl_tyres WHERE b2b_shop_id='$axle_id' and b2b_flag=0" ;
		$res_s_booking_id = mysqli_query($conn2,$sql_s_booking_id);
		$row_s_booking_id = mysqli_fetch_object($res_s_booking_id);

		$b2b_shop_booking_id=$row_s_booking_id->b2b_shop_booking_id;
		$b2b_shop_booking_id=$b2b_shop_booking_id+1;
		
		$b2b_credit_amt = 50;
		

		$sql_b2b_booking = "INSERT INTO b2b_booking_tbl_tyres 
			(b2b_shop_id,b2b_booking_id,b2b_shop_booking_id,b2b_customer_name,brand,model,fuel_type,
			b2b_vehicle_no,b2b_cust_phone,b2b_service_type,b2b_service_date,b2b_pick_up,
			b2b_source,b2b_log,gb_booking_id,b2b_credit_amt,b2b_referral_id,b2b_referral_shop_id,
			b2b_vehicle_type,b2b_customer_remarks,b2b_pick_date,b2b_pick_time,b2b_pick_address,
			b2b_bridge_estimate,b2b_alternate_phone,b2b_tyre_brand,b2b_tyre_count,b2b_tyre_size) 
			VALUES 
			('$axle_id','$b2b_booking_id','$b2b_shop_booking_id','$user_name','$veh_brand','$veh_model','$fuel_type',
			'$veh_no','$user_phn','$b2b_service_type','$service_date','$pickup',
			'web','$log','$booking_id','$b2b_credit_amt','$referral_id','$referral_shop_id',
			'$veh_type','$service_description','$pickup_date','$pickup_time','$pickup_full_address,$pickup_location',
			'$estimate_amt','$cust_alt_phn','$tyre_brand','$tyre_count','$tyre_size')";
			
		$res_b2b_booking = mysqli_query($conn2,$sql_b2b_booking) or die(mysqli_error($conn2));

		$sql_b2b_get = "SELECT max(b2b_booking_id) as b2b_booking_id FROM b2b_booking_tbl_tyres WHERE gb_booking_id = '$booking_id'";
		$res_b2b_get = mysqli_query($conn2,$sql_b2b_get);
		$row_b2b_get = mysqli_fetch_object($res_b2b_get);

		$b2b_booking_id = $row_b2b_get->b2b_booking_id;
		

		$weekly_counter=mysqli_query($conn,"UPDATE admin_mechanic_table SET wkly_counter= wkly_counter-1 WHERE axle_id='$axle_id'") or die(mysqli_error($conn));
		
		$update_mec_ids = "UPDATE tyre_booking_tb SET shop_name = '$shop_name', axle_flag = '1',booking_status = '2' ,mec_id_leads = '$mec_id_leads', axle_id_leads = '$axle_id_leads' WHERE booking_id = '$booking_id'";
//echo $update_mec_ids;
mysqli_query($conn,$update_mec_ids);
		// echo "http://axle.gobumpr.com/b2b/tyrespartnerfcm.php?b2b_shop_id=".$axle_id."&booking_id=".$b2b_booking_id."&flag=1";
		$ch=curl_init("http://axle.gobumpr.com/b2b/tyrespartnerfcm.php?b2b_shop_id=".$axle_id."&booking_id=".$b2b_booking_id."&flag=1");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result=curl_exec($ch);
		curl_close($ch);
		
		
	}
	echo "done";
}
					if($flag=='1')
					{
					header('Location:aleads_tyres.php');
					}
					else
					{
						header('Location:leads_tyres.php');
					}
// }
}
}

else
{
	
	$sql = "SELECT mec_id,user_id,vech_id,shop_name,user_veh_id,vehicle_type,user_vech_no,service_type,pick_up,service_date,service_description,pickup_address,pickup_full_address,estimate_amt,pickup_date_time,axle_flag,b2b_referral_id,b2b_referral_shop_id,mec_id_leads,axle_id_leads FROM tyre_booking_tb WHERE booking_id='$booking_id'";
$query = mysqli_query($conn,$sql);
$row_book = mysqli_fetch_object($query);
$user_id = $row_book->user_id;
$user_veh_id = $row_book->user_veh_id;
$veh_no = $row_book->user_vech_no;
$mec_id_leads=$row_book->mec_id_leads;
$axle_id_leads=$row_book->axle_id_leads;

$axle_id_ar = explode(";",$axle_id_leads);


$res_veh = mysqli_query($conn,"SELECT uv.brand,uv.model,uv.vehicle_id,uv.fuel_type,av.tyre_size FROM user_vehicle_table uv LEFT JOIN admin_vehicle_table_new av ON av.id = uv.vehicle_id WHERE uv.id='$user_veh_id'");
$row_veh = mysqli_fetch_object($res_veh);
$veh_brand = $row_veh->brand;
$veh_model = $row_veh->model;
$vehicle_id = $row_veh->vehicle_id;
$fuel_type = $row_veh->fuel_type;
$tyre_size = $row_veh->tyre_size;

$res_user = mysqli_query($conn,"SELECT name,mobile_number,mobile_number2,email_id FROM user_register WHERE reg_id='$user_id'");
$row_user = mysqli_fetch_object($res_user);
$user_name = $row_user->name;
$user_phn = $row_user->mobile_number;
$cust_alt_phn = "+91".$row_user->mobile_number2;
$user_mail = $row_user->email_id;
if($tyre_brand=="Nil"){
		$tyre_brand="";
	}
	
// $now = date('H');
// if($now < 19 && $now >9)
// {
	
	foreach($axle_id_ar as $axle_id)
	{

	$sql_mec_shop="SELECT * FROM b2b.b2b_credits_tbl WHERE b2b_shop_id=$axle_id"; 
	$res_mec_id = mysqli_query($conn2,$sql_mec_shop);
	$row_b2b_mec_id = mysqli_fetch_object($res_mec_id);
	

		if($row_b2b_mec_id->b2b_credits>0){
		$sql_b2b_booking_id = "SELECT max(b2b_booking_id) AS b2b_booking_id FROM b2b_booking_tbl_tyres" ;
		$res_b2b_booking_id = mysqli_query($conn2,$sql_b2b_booking_id);
		$row_b2b_booking_id = mysqli_fetch_object($res_b2b_booking_id);

		$b2b_booking_id=$row_b2b_booking_id->b2b_booking_id;
		$b2b_booking_id=$b2b_booking_id+1;
		
		$sql_s_booking_id = "SELECT max(b2b_shop_booking_id) AS b2b_shop_booking_id FROM b2b_booking_tbl_tyres WHERE b2b_shop_id='$axle_id' and b2b_flag=0" ;
		$res_s_booking_id = mysqli_query($conn2,$sql_s_booking_id);
		$row_s_booking_id = mysqli_fetch_object($res_s_booking_id);

		$b2b_shop_booking_id=$row_s_booking_id->b2b_shop_booking_id;
		$b2b_shop_booking_id=$b2b_shop_booking_id+1;
		
		$b2b_credit_amt = 50;
		

		$sql_b2b_booking = "INSERT INTO b2b_booking_tbl_tyres 
			(b2b_shop_id,b2b_booking_id,b2b_shop_booking_id,b2b_customer_name,brand,model,fuel_type,
			b2b_vehicle_no,b2b_cust_phone,b2b_service_type,b2b_service_date,b2b_pick_up,
			b2b_source,b2b_log,gb_booking_id,b2b_credit_amt,b2b_referral_id,b2b_referral_shop_id,
			b2b_vehicle_type,b2b_customer_remarks,b2b_pick_date,b2b_pick_time,b2b_pick_address,
			b2b_bridge_estimate,b2b_alternate_phone,b2b_tyre_brand,b2b_tyre_count,b2b_tyre_size) 
			VALUES 
			('$axle_id','$b2b_booking_id','$b2b_shop_booking_id','$user_name','$veh_brand','$veh_model','$fuel_type',
			'$veh_no','$user_phn','$b2b_service_type','$service_date','$pickup',
			'web','$log','$booking_id','$b2b_credit_amt','$referral_id','$referral_shop_id',
			'$veh_type','$service_description','$pickup_date','$pickup_time','$pickup_full_address,$pickup_location',
			'$estimate_amt','$cust_alt_phn','$tyre_brand','$tyre_count','$tyre_size')";
			//echo $sql_b2b_booking;
			
		$res_b2b_booking = mysqli_query($conn2,$sql_b2b_booking) or die(mysqli_error($conn2));
		
		$sql_b2b_get = "SELECT max(b2b_booking_id) as b2b_booking_id FROM b2b_booking_tbl_tyres WHERE gb_booking_id = '$booking_id'";
		$res_b2b_get = mysqli_query($conn2,$sql_b2b_get);
		$row_b2b_get = mysqli_fetch_object($res_b2b_get);

		$b2b_booking_id = $row_b2b_get->b2b_booking_id;
		

		$weekly_counter=mysqli_query($conn,"UPDATE admin_mechanic_table SET wkly_counter= wkly_counter-1 WHERE axle_id='$axle_id'") or die(mysqli_error($conn));

		$update_mec_ids = "UPDATE tyre_booking_tb SET axle_flag = '1',booking_status = '2' WHERE booking_id = '$booking_id'";
//echo $update_mec_ids;
         mysqli_query($conn,$update_mec_ids);
		
		
		// echo "http://axle.gobumpr.com/b2b/tyrespartnerfcm.php?b2b_shop_id=".$axle_id."&booking_id=".$b2b_booking_id."&flag=1";
		$ch=curl_init("http://axle.gobumpr.com/b2b/tyrespartnerfcm.php?b2b_shop_id=".$axle_id."&booking_id=".$b2b_booking_id."&flag=1");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result=curl_exec($ch);
		curl_close($ch);
		
		
	}

else
{
	echo "credits not enough to goaxle";
	continue;
}
}
	echo "done";
					if($flag=='1')
					{
					header('Location:aleads_tyres.php');
					}
					else
					{
						header('Location:leads_tyres.php');
					}
					exit();

}

?>
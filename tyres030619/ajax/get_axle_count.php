<?php
ini_set('max_execution_time', -1);
// error_reporting(E_ALL ^ E_NOTICE);
include("../config.php");
$conn = db_connect3();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$shop_id = $_POST['shop_id'];
$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate = date('Y-m-d',strtotime($_POST['enddate']));
$veh_type='tyres';
//print_r($_POST);
$sql_mobile = "SELECT b2b_mobile_number_1 FROM b2b_mec_tbl WHERE b2b_shop_id='$shop_id'";
$res_mobile = mysqli_query($conn,$sql_mobile);
$row_mobile = mysqli_fetch_array($res_mobile);
$mobile = $row_mobile['b2b_mobile_number_1'];

//leads_sent

$sql_leads_sent = "SELECT count(b.b2b_booking_id) as count,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl_tyres as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND b.b2b_source !='' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
//echo $sql_leads_sent;die;
$res_leads_sent = mysqli_query($conn,$sql_leads_sent);
$row_leads_sent = mysqli_fetch_object($res_leads_sent);
$count_leads_sent=$row_leads_sent->count;

//leads_sent RE

// $sql_leads_sent_re = "SELECT count(b.b2b_booking_id) as count,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND brand ='Royal Enfield' and b.b2b_source !='' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
// //echo $sql_leads_sent;die;
// $res_leads_sent_re = mysqli_query($conn,$sql_leads_sent_re);
// $row_leads_sent_re = mysqli_fetch_object($res_leads_sent_re);
// $count_leads_sent_re=$row_leads_sent_re->count;

//leads_sent NRE

// $sql_leads_sent_nre = "SELECT count(b.b2b_booking_id) as count,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND brand !='Royal Enfield' and b.b2b_source !='' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
// //echo $sql_leads_sent;die;
// $res_leads_sent_nre = mysqli_query($conn,$sql_leads_sent_nre);
// $row_leads_sent_nre = mysqli_fetch_object($res_leads_sent_nre);
// $count_leads_sent_nre=$row_leads_sent_nre->count;


//leads_accepted
$sql_leads_accepted = "SELECT count(b.b2b_booking_id) as count,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl_tyres as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND b.b2b_swap_flag!='1' AND s.b2b_acpt_flag='1' AND s.b2b_deny_flag='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate'";
$res_leads_accepted = mysqli_query($conn,$sql_leads_accepted);
$row_leads_accepted = mysqli_fetch_object($res_leads_accepted);
$count_leads_accepted=$row_leads_accepted->count;

//leads_accepted RE
// $sql_leads_accepted = "SELECT count(b.b2b_booking_id) as count,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl_tyres as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' and b.b2b_swap_flag!='1' AND s.b2b_acpt_flag='1' AND s.b2b_deny_flag='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate'";
// $res_leads_accepted = mysqli_query($conn,$sql_leads_accepted);
// $row_leads_accepted = mysqli_fetch_object($res_leads_accepted);
// $count_leads_accepted_re=$row_leads_accepted->count;

//leads_accepted NRE
// $sql_leads_accepted = "SELECT count(b.b2b_booking_id) as count,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND brand !='Royal Enfield' and b.b2b_swap_flag!='1' AND s.b2b_acpt_flag='1' AND s.b2b_deny_flag='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate'";
// $res_leads_accepted = mysqli_query($conn,$sql_leads_accepted);
// $row_leads_accepted = mysqli_fetch_object($res_leads_accepted);
// $count_leads_accepted_nre=$row_leads_accepted->count;

//leads_swapped
$sql_leads_swapped = "SELECT count(b.b2b_booking_id) as count,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl_tyres as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND b.b2b_swap_flag='1' AND s.b2b_acpt_flag='1' AND s.b2b_deny_flag='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate'";
//echo $sql_leads_swapped;
$res_leads_swapped = mysqli_query($conn,$sql_leads_swapped);
$row_leads_swapped = mysqli_fetch_object($res_leads_swapped);
$count_leads_swapped=$row_leads_swapped->count;

//checkins_reported
$sql_checkins_reported = "SELECT count(b.b2b_booking_id) as count,s.b2b_acpt_flag FROM b2b_booking_tbl_tyres as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1'  AND b.b2b_check_in_report='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
$res_checkins_reported = mysqli_query($conn,$sql_checkins_reported);
$row_checkins_reported = mysqli_fetch_object($res_checkins_reported);
$count_checkins_reported=$row_checkins_reported->count;

//inspections_completed
$sql_inspections_completed = "SELECT count(b.b2b_booking_id) as count,s.b2b_acpt_flag FROM b2b_booking_tbl_tyres as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1'  AND b.b2b_vehicle_at_garage='1' AND b.b2b_service_under_progress='0' AND b.b2b_vehicle_ready='0' AND b.b2b_vehicle_delivered='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
$res_inspections_completed = mysqli_query($conn,$sql_inspections_completed);
$row_inspections_completed = mysqli_fetch_object($res_inspections_completed);
$count_inspections_completed=$row_inspections_completed->count;

//services_under_progress 
$sql_services_under_progress = "SELECT count(b.b2b_booking_id) as count,s.b2b_acpt_flag FROM b2b_booking_tbl_tyres as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1'  AND b.b2b_service_under_progress='1' AND b.b2b_vehicle_ready='0' AND b.b2b_vehicle_delivered='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
$res_services_under_progress = mysqli_query($conn,$sql_services_under_progress);
$row_services_under_progress = mysqli_fetch_object($res_services_under_progress);
$count_services_under_progress=$row_services_under_progress->count;

// vehicles ready
$sql_vehicles_ready = "SELECT count(b.b2b_booking_id) as count,s.b2b_acpt_flag FROM b2b_booking_tbl_tyres as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1'  AND b.b2b_vehicle_ready='1' AND b.b2b_vehicle_delivered='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
$res_vehicles_ready = mysqli_query($conn,$sql_vehicles_ready);
$row_vehicles_ready = mysqli_fetch_object($res_vehicles_ready);
$count_vehicles_ready=$row_vehicles_ready->count;

//vehicles delivered
$sql_vehicles_delivered = "SELECT count(b.b2b_booking_id) as count,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl_tyres as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1'  AND b.b2b_vehicle_delivered='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
$res_vehicles_delivered = mysqli_query($conn,$sql_vehicles_delivered);
$row_vehicles_delivered = mysqli_fetch_object($res_vehicles_delivered);
$count_vehicles_delivered=$row_vehicles_delivered->count;
// //RE Vehicles
// $sql_services_under_progress = "SELECT count(b.b2b_booking_id) as count,s.b2b_acpt_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id'  AND s.b2b_acpt_flag='1'  AND b.b2b_service_under_progress='1' AND b.b2b_vehicle_ready='0' AND b.b2b_vehicle_delivered='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
// $res_services_under_progress = mysqli_query($conn,$sql_services_under_progress);
// $row_services_under_progress = mysqli_fetch_object($res_services_under_progress);
// $count_services_under_progress=$row_services_under_progress->count;

// //NRE Vehicles
// $sql_services_under_progress = "SELECT count(b.b2b_booking_id) as count,s.b2b_acpt_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1'  AND b.b2b_service_under_progress='1' AND b.b2b_vehicle_ready='0' AND b.b2b_vehicle_delivered='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
// $res_services_under_progress = mysqli_query($conn,$sql_services_under_progress);
// $row_services_under_progress = mysqli_fetch_object($res_services_under_progress);
// $count_services_under_progress=$row_services_under_progress->count;


//total bill
$sql_total_bill = "SELECT SUM(b.b2b_bill_amount) AS total_bill FROM b2b_booking_tbl_tyres as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1'  AND b.b2b_vehicle_ready='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
$res_total_bill = mysqli_query($conn,$sql_total_bill);
$row_total_bill = mysqli_fetch_object($res_total_bill);
$count_total_bill = $row_total_bill->total_bill;
if($count_total_bill == ""){
   $count_t_bill = 0;
}
$count_t_bill = $count_total_bill;

// average bill
$sql_avg_bill = "SELECT SUM(b.b2b_bill_amount) AS total_bill FROM b2b_booking_tbl_tyres as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1'  AND b.b2b_vehicle_ready='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
$res_avg_bill = mysqli_query($conn,$sql_avg_bill);
$row_avg_bill = mysqli_fetch_object($res_avg_bill);
$bill_avg_bill = $row_avg_bill->total_bill;
$sql1_avg_bill = "SELECT count(b.b2b_booking_id) as count,s.b2b_acpt_flag FROM b2b_booking_tbl_tyres as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1'  AND b.b2b_vehicle_ready='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
$res1_avg_bill = mysqli_query($conn,$sql1_avg_bill);
$row1_avg_bill = mysqli_fetch_object($res1_avg_bill);
$count_avg_bill = $row1_avg_bill->count;
if($count_avg_bill == 0){
    $count_average_bill = "0";
}
else{
    $avg_avg_bill = $bill_avg_bill / $count_avg_bill ;
    $count_average_bill =  number_format((float)$avg_avg_bill, 2, '.', '');
}

//ratings               
$sql_ratings = "SELECT avg(b.b2b_Rating) AS rating FROM b2b_booking_tbl_tyres  as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1'  AND b.b2b_Rating !='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
$res_ratings = mysqli_query($conn,$sql_ratings);
$row_ratings = mysqli_fetch_object($res_ratings);
$rat_ratings = $row_ratings->rating;
$count_rating = number_format((float)$rat_ratings, 2, '.', '');

//reviews
$sql_reviews = "SELECT count(b.b2b_booking_id) as count,s.b2b_acpt_flag FROM b2b_booking_tbl_tyres as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1' AND b.b2b_Feedback !='' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate'";
$res_reviews = mysqli_query($conn,$sql_reviews);
$row_reviews = mysqli_fetch_object($res_reviews);
$count_reviews = $row_reviews->count;
?>

<div id="div2_1" style="margin-top:50px; margin-left:-210px; float:left; border-radius:5px;width:15%;">
	<div id="ls" class="floating-box2" align="center" style="background-color:#f6ecd0;" data-toggle="modal" data-target="#myModal_leads_sent">
		 <p style="float:left;margin-left:20px;"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp;Leads Sent : &nbsp;&nbsp;</p>
		 <p id="leads_sent" style="float:left;"><?php echo $count_leads_sent; ?></p>
	 </div>
 </div>

 <div id="div2_2" style="margin-top:110px; margin-left:-210px; float:left; border-radius:5px;width:15%;">
 	 <div id="ls" class="floating-box2" align="center" style="background-color:#f6ecd0;" data-toggle="modal" data-target="#myModal_leads_accepted">
 		 <p style="float:left;margin-left:20px;"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>&nbsp;&nbsp;Leads Accepted : &nbsp;&nbsp;</p>
 		 <p id="leads_accepted" style="float:left;"><?php echo $count_leads_accepted; ?></p>
 	 </div>
  </div>

  <div id="div2_8" style="margin-top:170px; margin-left:-210px; float:left; border-radius:5px;width:15%;">
 	 <div id="ls" class="floating-box2" align="center" style="background-color:#f6ecd0;" data-toggle="modal" data-target="#myModal_swapped_leads">
 		 <p style="float:left;margin-left:20px;"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>&nbsp;&nbsp;Swapped Leads : &nbsp;&nbsp;</p>
 		 <p id="swapped_leads" style="float:left;"><?php echo $count_leads_swapped; ?></p>
 	 </div>
  </div>
 
		<div id="div2_3" style="margin-top:230px; margin-left:-210px; float:left; border-radius:5px;width:15%;">
		 <div id="ls" class="floating-box2" align="center" style="background-color:	#e6ecd0;" data-toggle="modal" data-target="#myModal_checkins_reported">
			 <p style="float:left;margin-left:20px;"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>&nbsp;&nbsp;Check-Ins Reported : &nbsp;&nbsp;</p>
			 <p id="checkins_reported" style="float:left;"><?php echo $count_checkins_reported; ?></p>
		 </div>
	 </div>
		 <div id="div2_4" style="margin-top:290px; margin-left:-210px; float:left; border-radius:5px;width:15%;">
	 	 <div id="ls" class="floating-box2" align="center" style="background-color:#d6ecd0;" data-toggle="modal" data-target="#myModal_inspections_completed">
	 		 <p style="float:left;margin-left:20px;"><i class="fa fa-calculator" aria-hidden="true"></i>&nbsp;&nbsp;Inspection Completed : &nbsp;&nbsp;</p>
	 		 <p id="inspection_completed" style="float:left;"><?php echo $count_inspections_completed; ?></p>
	 	 </div>
	  </div>
		<div id="div2_5" style="margin-top:350px; margin-left:-210px; float:left; border-radius:5px;width:15%;">
 	 	 <div id="ls" class="floating-box2" align="center" style="background-color:#c6ecd0;" data-toggle="modal" data-target="#myModal_services_under_progress">
 	 		 <p style="float:left;margin-left:20px;"><i class="fa fa-wrench" aria-hidden="true"></i>&nbsp;&nbsp;Services under progress : &nbsp;&nbsp;</p>
 	 		 <p id="service_under_progress" style="float:left;"><?php echo $count_services_under_progress; ?></p>
 	 	 </div>
 	  </div>
		 <div id="div2_6" style="margin-top:410px; margin-left:-210px; float:left; border-radius:5px;width:15%;">
 	 	 <div id="ls" class="floating-box2" align="center" style="background-color:	#b6ecd0;" data-toggle="modal" data-target="#myModal_vehicles_ready">
 	 		 <p style="float:left;margin-left:20px;"><i class="fa fa-car" aria-hidden="true"></i>&nbsp;&nbsp;Vehicles Ready : &nbsp;&nbsp;</p>
 	 		 <p id="vehicle_ready" style="float:left;"><?php echo $count_vehicles_ready; ?></p>
 	 	 </div>
 	  </div>
		<div id="div2_7" style="margin-top:470px; margin-left:-210px; float:left; border-radius:5px;width:15%;">
 	 	 <div id="ls" class="floating-box2" align="center" style="background-color:	#a6ecd0;" data-toggle="modal" data-target="#myModal_vehicles_delivered">
 	 		 <p style="float:left;margin-left:20px;"><i class="fa fa-thumbs-up" aria-hidden="true"></i>&nbsp;&nbsp;Vehicles Delivered : &nbsp;&nbsp;</p>
 	 		 <p id="vehicle_delivered" style="float:left;"><?php echo $count_vehicles_delivered; ?></p>
 	 	 </div>
	 </div>


    <div id="div3" style="margin-top:43px; margin-right:97px; float:right; border-radius:5px;width:15%;">
	<div align="center" style="float:left;font-size:16px;">
			<p id="mobile">&nbsp;&nbsp;<i class="fa fa-mobile" aria-hidden="true" style="font-size:20px;"></i>&nbsp;<?php echo $mobile; ?></p>
	</div>
    <div class="floating-box" id="totalbill" align="center" style="float:left;background-color:#99BBC5; " data-toggle="modal" data-target="#myModal_total_bill">
			<p>Total Bill</p>
			<p id="tbill"><?php echo $count_t_bill; ?></p>
		</div>
		<div class="floating-box" id="avgbill" align="center" style="float:left;background-color: #FFE4B5;" data-toggle="modal" data-target="#myModal_avg_bill">
			<p>Avg. Bill</p>
			<p id="abill"><?php echo $count_average_bill; ?></p>
		</div>
		<br>
		<div class="floating-box" id="rating" align="center" style="float:left;background-color:#A7E9B1; " data-toggle="modal" data-target="#myModal_rating">
			<p>Rating</p>
			<p id="rat"><?php echo $count_rating; ?></p>
		</div>
		<div class="floating-box" id="reviews" align="center" style="float:left;background-color: #DF9B88;" data-toggle="modal" data-target="#myModal_reviews">
			<p>Reviews</p>
			<p id="rev"><?php echo $count_reviews; ?></p>
		</div>
		<div class="floating-box" id="toggler" align="center" style="padding-top: 8px;padding-right: 8px;height:35px;float:left;background-color: #00031A5E;">
			<p>Partner App<span><i class="fa fa-line-chart" aria-hidden="true" style="display:inline;padding-left:5px;"></i></span></p>
		</div>
 </div> 

<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id']))) {
	header('location:logout.php');
	die();
}

$mobile=base64_decode($_GET['m']);


if(isset($_POST['submit'])){
  $mobile=$_POST['mobile'];
  $mobileNumber='+91'.$mobile;
  $username = mysqli_real_escape_string($conn,$_POST['user_name']);
  $email = mysqli_real_escape_string($conn,$_POST['email']);
  $City = mysqli_real_escape_string($conn,$_POST['city']);
  $Locality_Home = mysqli_real_escape_string($conn,$_POST['location_home']);
  $Locality_Work = mysqli_real_escape_string($conn,$_POST['location_work']);
  $comments = mysqli_real_escape_string($conn,$_POST['comments']);
  $campaign = mysqli_real_escape_string($conn,$_POST['campaign']);
  $source = mysqli_real_escape_string($conn,$_POST['source']);
  $address = mysqli_real_escape_string($conn,$_POST['address']);

  $log= date('Y-m-d H:i:s');
  $sqlins = mysqli_query($conn,"INSERT INTO user_register(name,email_id,mobile_number,vehicle,flag,log,City,source,Locality_Home,Locality_Work,comments,campaign,lat_lng,crm_log_id) VALUES('$username','$email','$mobileNumber','0','-1','$log','$City','$source','$Locality_Home','$Locality_Work','$comments','$campaign','$address','$crm_log_id')");
  $sql_fetch = mysqli_query($conn,"SELECT reg_id FROM user_register WHERE mobile_number='$mobileNumber' ");
  $row=mysqli_fetch_object($sql_fetch);
  $user_id = $row->reg_id;

  //redirect 
  $u_id =base64_encode($user_id);
  header("Location:new_vehicle.php?u=$u_id");
}
?>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
  <title>GoBumpr Bridge</title>

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>

  <style>
  .borderless td, .borderless th {
    border: none !important;
}
#datepick > span:hover{cursor: pointer;}
.floating-box {
	 display: inline-block;
	 margin: 22px;
 padding: 22px;
 width:203px;
 height:105px;
 box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  font-size: 17px;
}
</style>
</head>
<body>
  <nav class="navbar navbar-default navbar-fixed-top" >
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->

   <ol class="breadcrumb" style="max-height:40px;">
   <a href="#" data-sidebar-button style="text-decoration:none;"><i class="fa fa-bars" aria-hidden="true"></i></a> &nbsp;&nbsp;&nbsp;&nbsp;
<?php
if($flag ==1){
  ?>
  <li style="margin-top:10px;"><a href="ahome.php"><i class="fa fa-home"></i> Home</a></li>
  <?php
}
else{
  ?>
  <li style="margin-top:10px;"><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
  <?php
}
?>

        <ul class="nav navbar-nav navbar-right" >
        <li style=" margin-top:11px; font-size:20px; ">
        <div class="dropdown">
        <a href="" style="color:#000; padding-right: 15px;"><i class="fa fa-user-circle" aria-hidden="true"></i> <?php echo $crm_name; ?> </a>
        <li class="nav navbar-nav navbar-right" ><a href="logout.php" style="color:#000; font-size:20px;margin-right:20px;"><i class="fa fa-sign-out"></i> LogOut</a></li>
        </li>
        </ul>
</ol>
  </div><!-- /.container-fluid -->
</nav>
 <div class="overlay" data-sidebar-overlay></div>

<div class="padding"></div>

<div align="center" style="margin-top:142px;">
<!-- Add User -->
<!-- Trigger the modal with a button -->
<h3> Please provide the user details to proceed...!</h3>
<button id="a1" type="button" class="btn btn-lg" data-toggle="modal" data-target="#myModal_user" style="background-color:#B6ED6F;"><i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;&nbsp;Add User</button>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal_user" role="dialog" >
<div class="modal-dialog" style="width:860px;">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Add User</h3>
</div>
<div class="modal-body" style="height:398px;">
<form id="add_user" class="form" method="post" action="add_user.php">

<div class="row">
<br>
<div class="col-xs-3 col-xs-offset-1 form-group">
<input class="form-control" type="text" id="mobile" name="mobile" pattern="^[0-9]{6}|[0-9]{8}|[0-9]{10}$" maxlength="10" minlength="9" onchange="try{setCustomValidity('')}catch(e){}" title="Mobile number must have 10 digits!" placeholder="Mobile"  value="<?php echo $mobile; ?>" readonly>
</div>

<div class="col-xs-3 form-group">
<input  class="form-control" type="text" id="user_name" name="user_name" pattern="^[a-zA-Z0-9\s]+$" onchange="try{setCustomValidity('')}catch(e){}" placeholder="User Name" required>
</div>

<div class="col-xs-4 form-group">
<input type="email" class="form-control" id="email" name="email"  onchange="try{setCustomValidity('')}catch(e){}" placeholder="E-Mail">
</div>

</div>

<div class="row"></div>


<div class="row">
<div id="city_u" class="col-xs-4 col-xs-offset-1 form-group">
        <div class="ui-widget">
			<select class="form-control" id="city" type="text" name="city">
				<option value="Chennai" selected>Chennai</option>
				<option value="Coimbatore">Coimbatore</option>
				<option value="Bangalore">Bangalore</option> 
				<option value="Hyderabad">Hyderabad</option> 
			</select>
        </div>
</div>
<div id="loc_home" class="col-xs-3 form-group">
        <div class="ui-widget">
        	<input class="form-control autocomplete" id="location_home" type="text" name="location_home" pattern="^[a-zA-Z0-9\s]+$" onchange="try{setCustomValidity('')}catch(e){}" placeholder="Home Locality">
        </div>
</div>

<div id="loc_work" class="col-xs-3 form-group">
        <div class="ui-widget">
        	<input class="form-control autocomplete" id="location_work" type="text" name="location_work" pattern="^[a-zA-Z0-9\s]+$" onchange="try{setCustomValidity('')}catch(e){}" placeholder="Work Locality">
        </div>
</div></div>

<div class="row"></div>
<div class="row">
<div id="source_u" class="col-xs-5 col-xs-offset-1 form-group">
        <div class="ui-widget">
        	<select class="form-control" id="source" name="source">
              <option value="Hub Booking" selected>Hub Booking</option>
<?php 
      $sql_sources = "SELECT user_source FROM user_source_tbl WHERE flag='0' AND user_source != 'Hub Booking' ORDER BY  user_source ASC";
      $res_sources = mysqli_query($conn,$sql_sources);
      while($row_sources = mysqli_fetch_object($res_sources)){
        $source_name = $row_sources->user_source;
        ?>
        <option value="<?php echo $source_name; ?>"><?php echo $source_name; ?></option>
        <?php
      }
      ?>			  </select>
        </div>
</div>

<div id="cam" class="col-xs-5 form-group">
			 <div class="ui-widget">
				 <input class="form-control autocomplete" id="campaign" type="text" name="campaign"  placeholder="Campaign">
			 </div>
</div></div>

<div class="row"></div>

<div class="row">
<div class="col-xs-10 col-xs-offset-1 form-group">
<textarea class="form-control" maxlength="100" id="comments" name="comments" placeholder="Comments..."></textarea>
</div>
</div>

<div class="row"></div>

<div class="row">
<div class="col-xs-10 col-xs-offset-1 form-group">
<textarea class="form-control" maxlength="100" id="address" name="address" placeholder="Address"></textarea>
</div>
</div>


<div class="row"></div>
<div class="row">
<br>
<div class="form-group" align="center">
<input class="form-control" type="submit" id="submit" name="submit" value="Submit" style=" width:90px; background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
</form>
</div>

</div>
</div></div>

<!-- jQuery library -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script>
// Get the modal
$("#myModal_user").modal("show");
</script>
<!-- validation -->
<script>
var userinput = document.getElementById('user_name');
userinput.oninvalid = function(event) {
	  document.getElementById("user_name").style.borderColor="#E42649";
    event.target.setCustomValidity('Only aplhabets and digits are allowed!');
}
</script>
<script>
var locationhomeinput = document.getElementById('location_home');
locationhomeinput.oninvalid = function(event) {
	  document.getElementById("location_home").style.borderColor="#E42649";
    event.target.setCustomValidity('Only aplhabets and digits are allowed!');
}
</script>
<script>
$(document).ready(function(){
  $("#location_home").change(function(){
		var working_from = $("#location_home").val();
		if(working_from!= ""){
			document.getElementById("location_home").style.borderColor="#35CE23";
		}
	});
});
</script>
<script>
var locationworkinput = document.getElementById('location_work');
locationworkinput.oninvalid = function(event) {
	  document.getElementById("location_work").style.borderColor="#E42649";
    event.target.setCustomValidity('Only aplhabets and digits are allowed!');
}
</script>
<script>
$(document).ready(function(){
  $("#location_work").change(function(){
		var working_from = $("#location_work").val();
		if(working_from!= ""){
			document.getElementById("location_work").style.borderColor="#35CE23";
		}
	});
});
</script>

</body>
</html>

<?php
error_reporting(E_ALL); ini_set('display_errors', 1);
include("../config.php");
$conn = db_connect1(); 
session_start();
header('Content-Type: application/json');


$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$city = $_GET['city'];
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;


function run_query($conn, $query){
  //echo $query;
	$query_result = mysqli_query($conn,$query);
	$query_rows = mysqli_fetch_object($query_result);
	return $query_object = $query_rows->count;
}
function run_query1($conn, $query){
	$count_no_leads = 0;
	$count_no_bookings = 0;
	$count_no_followups = 0;
	$count_no_cancelled = 0;
	$count_no_others = 0;
	$count_no_overall = 0;
	$count_no_unwanted = 0;
	$query_result = mysqli_query($conn,$query);
	// $followup_arr = array(3,4,5,6);
	while($query_rows = mysqli_fetch_object($query_result))
	{
		$booking_id = $query_rows->booking_id;
		$booking_status = $query_rows->booking_status;
		$axle_flag = $query_rows->axle_flag;
		$flag = $query_rows->flag;
		$flag_unwntd = $query_rows->flag_unwntd;
		$count_no_overall = $count_no_overall + 1;


		// $sql = "SELECT s.b2b_acpt_flag FROM b2b.b2b_booking_tbl_tyres AS bb LEFT JOIN b2b.b2b_status AS s ON bb.b2b_booking_id = s.b2b_booking_id WHERE bb.gb_booking_id = '$booking_id' AND bb.b2b_flag != '1'";
			
		// 	$res = mysqli_query($conn,$sql);
		// 	$row = mysqli_fetch_object($res);
			
		
		
		if($booking_status == 2 && $axle_flag = 1 && $flag_unwntd == 0)
		{
			$count_no_bookings = $count_no_bookings + 1;
		}
		else if($booking_status == 1 && $flag != 1 && $axle_flag != 1 && $flag_unwntd == 0)
		{
			$count_no_leads = $count_no_leads + 1;
		}
		// else if(in_array($booking_status,$followup_arr) && $flag != 1)
		// {
			// $count_no_followups = $count_no_followups + 1;
		// }
		else if($flag == 1)
		{
			$count_no_cancelled = $count_no_cancelled + 1;
		}
		else if($booking_status == 0 && $flag != 1)
		{
			$count_no_others = $count_no_others + 1;
		}
		// booking_status='1' AND b.flag!='1' AND b.flag_unwntd = '1'
		else if($booking_status == 1 && $flag != 1 && $flag_unwntd == 1)
		{
			$count_no_unwanted = $count_no_unwanted + 1;
		}
	}
	$rtn['count_no_bookings'] = $count_no_bookings;
	$rtn['count_no_leads'] = $count_no_leads;
	$rtn['count_no_followups'] = $count_no_followups;
	$rtn['count_no_cancelled'] = $count_no_cancelled;
	$rtn['count_no_others'] = $count_no_others;
	$rtn['count_no_overall'] = $count_no_overall;
	$rtn['count_no_unwanted'] = $count_no_unwanted;
	return $rtn;
}

$cond ='';
 
$cond = $cond.($city == 'all' ? "" : "AND b.city='$city'");
$cond = $cond.($cluster == 'all' ? "" : "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'");

$select_sql =  "SELECT count(DISTINCT b.booking_id) as count FROM tyre_booking_tb as b LEFT JOIN localities as l ON b.locality = l.localities WHERE b.booking_id!='' {$cond}";
$select_sql1 =  "SELECT DISTINCT b.booking_id,b.booking_status,b.flag,b.axle_flag,b.flag_unwntd FROM tyre_booking_tb as b LEFT JOIN localities as l ON b.locality = l.localities WHERE b.booking_id!='' {$cond}";

// leads count
$no_leads_arr = run_query1($conn, "{$select_sql1} AND b.flag!='1' AND b.booking_status='1' AND b.axle_flag!='1' AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
//echo $no_leads_arr;
if($startdate=='2016-01-01')
{
	$no_leads_arr = run_query1($conn, "{$select_sql1} AND b.flag!='1' AND b.booking_status='1' AND b.axle_flag!='1' AND b.flag_unwntd = '0'");
	//echo $no_leads_arr;
}
$no_leads = $no_leads_arr['count_no_leads'];
//echo $no_leads;

// bookings count

$no_bookings_arr = run_query1($conn, "{$select_sql1} AND b.booking_status='2' AND b.axle_flag='1' AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
if($startdate=='2016-01-01')
{
	$no_bookings_arr = run_query1($conn, "{$select_sql1} AND bb.gb_booking_id!='0' AND bb.b2b_flag!='1' AND s.b2b_acpt_flag='1' AND b.axle_flag='1' ");
}

$no_bookings = $no_bookings_arr['count_no_bookings'];

// followups count
$no_followups_arr = run_query($conn, "{$select_sql} AND b.booking_status IN(3,4,5,6) AND b.flag!='1' AND DATE(b.followup_date) BETWEEN '$startdate' AND '$enddate'");
if($startdate=='2016-01-01')
{
	$no_followups_arr = run_query($conn, "{$select_sql} AND b.booking_status IN(3,4,5,6) AND b.flag!='1' ");
}
 $no_followups = $no_followups_arr['count_no_followups'];

//cancelled count
$no_cancelled_arr = run_query($conn, "{$select_sql} AND b.flag='1' AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
if($startdate=='2016-01-01')
{
	$no_cancelled_arr = run_query($conn, "{$select_sql} ANDb.flag='1' ");
}
$no_cancelled = $no_cancelled_arr['count_no_cancelled'];

//others count
$no_others_arr = run_query($conn, "{$select_sql} AND b.booking_status='0' AND b.flag!='1' AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
if($startdate=='2016-01-01')
{
	$no_others_arr = run_query($conn, "{$select_sql} AND b.booking_status='0' AND b.flag!='1' ");
}
$no_others = $no_others_arr['count_no_others'];

// overall count
// $no_overall = run_query($conn, "{$select_sql} AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
// if($startdate=='2016-01-01')
// {
// $select_sql = "SELECT count(b.booking_id) as count FROM user_booking_tb as b WHERE {$cond}";
// $no_overall = run_query($conn, "{$select_sql} ");
// }
//$no_overall = $no_leads_arr['count_no_overall'];

// unwanted leads count
// $no_unwanted = run_query($conn, "{$select_sql} AND b.booking_status='1' AND b.flag!='1' AND b.flag_unwntd = '1' AND DATE(b.log) BETWEEN '$startdate' AND '$enddate' ");
// if($startdate=='2016-01-01')
// {
	// $no_unwanted = run_query($conn, "{$select_sql} AND b.booking_status='1' AND b.flag!='1' AND b.flag_unwntd = '1' ");
// }
// if($no_unwanted == '' || $no_unwanted == '0'){
	// $no_unwanted = 0;
// }
$no_unwanted = $no_leads_arr['count_no_unwanted'];

//incomplete count
$no_incomplete = run_query($conn, "{$select_sql}  AND b.booking_status IN(3,4,5,6) AND b.flag!='1' AND DATE_ADD(b.followup_date,INTERVAL 2 DAY) BETWEEN '$startdate' AND '$enddate'");
if($startdate=='2016-01-01')
{
	$no_incomplete = run_query($conn, "{$select_sql}  AND b.booking_status IN(3,4,5,6) AND b.flag!='1' AND DATE_ADD(b.followup_date,INTERVAL 2 DAY)");
}
echo $data = json_encode(array('leadsCount'=>$no_leads, 'goaxleCount'=>$no_bookings, 'followupsCount'=>$no_followups, 'cancelledCount'=>$no_cancelled, 'othersCount'=>$no_others));

//  $data = array('leadsCount'=>$no_leads,'goaxleCount' =>$no_bookings, 'followupsCount'=>$no_followups, 'cancelledCount'=>$no_cancelled, 'othersCount'=>$no_others);
// //echo $select_sql;
 // echo json_encode($data);
?>


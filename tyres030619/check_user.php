<?php
//error_reporting(E_ALL); ini_set('display_errors', 1);
include("config.php");
$conn = db_connect1();
session_start();
date_default_timezone_set('Asia/Calcutta');

$searchuser=mysqli_real_escape_string($conn,$_POST['searchuser']);
$search_type = mysqli_real_escape_string($conn,$_POST['search_type']);

switch($search_type){
  case 'mobile':
        $mobileNumber='+91'.$searchuser;
        $mobileNumber2=$searchuser;
        $usersel =mysqli_query($conn,"SELECT reg_id,name FROM user_register WHERE (mobile_number='$mobileNumber' OR mobile_number2='$mobileNumber2')");
        $no_rows=mysqli_num_rows($usersel);

        if($no_rows>=1){
          $rows=mysqli_fetch_array($usersel);
          $user_id=$rows['reg_id'];
          $u=base64_encode($user_id);
          header("Location:verified.php?u=$u");
          exit(1);
        }
        else {
          $m=base64_encode($searchuser);
          header("Location:add_user.php?m=$m");
          exit(1);
        }
        break;
  case 'booking':
        $sql_search = "SELECT booking_id,user_id,user_veh_id,booking_status,flag FROM tyre_booking_tb WHERE booking_id='$searchuser'";
        $res_search = mysqli_query($conn,$sql_search);
        $num_rows = mysqli_num_rows($res_search);
        if($num_rows >=1){
            $row_search = mysqli_fetch_array($res_search);
            $booking_id = $row_search['booking_id'];
            $user_id = $row_search['user_id'];
            $user_veh_id = $row_search['user_veh_id'];
            $booking_status = $row_search['booking_status'];
            $cancel_flag = $row_search['flag'];

            if($cancel_flag == 1){
              $status="c";
            }
            else{
              switch($booking_status){
                case 0: $status="o"; break;
                case 1: $status="l"; break;
                case 2: $status="b"; break;
                case 3: $status="f"; break;
                case 4: $status="f"; break;
                case 5: $status="f"; break;
                case 6: $status="f"; break;
              }
            }
            //echo $status;
            $s = base64_encode($status);
            $v = base64_encode($user_veh_id);
            $b = base64_encode($booking_id);
            header("Location:user_details_tyres.php?t=$s&v58i4=$v&bi=$b");
        }
        else{
          //redirech to no results found page
          header("Location:no_results_found.php");
          exit(1);
        }
        break;
  case 'email':
        $searchterm = explode('@',$searchuser);
        $email_address = '%'.$searchterm[0].'%';
        $doamin = '%'.$searchterm[1].'%';
        $sql_search = "SELECT reg_id FROM user_register WHERE email_id like '$email_address'";
        $res_search = mysqli_query($conn,$sql_search) or die(mysqli_error($conn));
        $num_rows = mysqli_num_rows($res_search);
        if($num_rows == 1){
          $row_search = mysqli_fetch_array($res_search);
          $user_id = $row_search['reg_id'];
          $u=base64_encode($user_id);
          header("Location:verified.php?u=$u");
          exit(1); 
        }
        else if($num_rows >1){
          $u = base64_encode($searchuser);
          $t = base64_encode($search_type);  
          header("Location:list_of_users.php?u=$u&t=$t");        
          exit(1);
        }
        else{
          header("Location:no_results_found.php");
          exit(1);
        }        
        break; 
  case 'username':
        $username = '%'.$searchuser.'%';
        $sql_search = "SELECT reg_id FROM user_register WHERE name like '$username'";
        $res_search = mysqli_query($conn,$sql_search) or die(mysqli_error($conn));
        $num_rows = mysqli_num_rows($res_search);
        if($num_rows >= 1){
          $u = base64_encode($searchuser);
          $t = base64_encode($search_type);
          //echo "pompom";
          header("Location:list_of_users.php?u=$u&t=$t");
          exit(1); 
        }
        else{
          header("Location:no_results_found.php");
          exit(1);
        }        
        break;
  default:
        header("Location:no_results_found.php");
        exit(1);
}

?>

<?php
//error_reporting(E_ALL);
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$super_flag = $_SESSION['super_flag'];
$feedback_admin = $_SESSION['feedback_admin'];


$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$vehicle = $_GET['vehicle'];
$service = $_GET['service'];
$shop_id = $_GET['shop_id'];
$city = $_GET['city'];

$_SESSION['crm_city'] = $city;

$cond ='';

$cond = $cond.($shop_id == 'all' ? "" : "AND f.shop_id='$shop_id'");
$cond = $cond.($city == 'all' ? "" : "AND f.city='$city'");

$sql_goaxles = "SELECT f.booking_id,b.b2b_log,b.b2b_acpt_flg,b.b2b_bid_flg,b.b2b_customer_name,b.b2b_cust_phone,f.shop_name,f.crm_log_id,b.brand,b.model,f.service_type,b.b2b_bid_amt,b.b2b_Rating,b.b2b_Feedback,b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_vehicle_ready,b.b2b_tyre_brand,b.b2b_tyre_count,f.b2b_booking_id,f.feedback_status FROM feedback_track_tyres f LEFT JOIN b2b.b2b_booking_tbl_tyres b ON b.gb_booking_id = f.booking_id WHERE f.feedback_status = 0 AND f.service_status = '' AND b.b2b_bid_flg =1 AND f.flag = 0 {$cond} AND f.shop_id NOT IN (1014,1035,1670) AND DATE(f.crm_update_time) BETWEEN '$startdate' AND '$enddate' AND f.crm_log_id = '$crm_log_id' GROUP BY f.booking_id DESC";
//echo $sql_goaxles;
$res_goaxles = mysqli_query($conn,$sql_goaxles);
$no = 0;
$arr = array();
$count = mysqli_num_rows($res_goaxles);
if($count >0){
  while($row_goaxles = mysqli_fetch_object($res_goaxles)){
    $booking_id = $row_goaxles->booking_id;
    $goaxle_date = $row_goaxles->b2b_log;
    $acpt = $row_goaxles->b2b_acpt_flg;
    $b2b_bid_flg = $row_goaxles->b2b_bid_flg; 
    $name = $row_goaxles->b2b_customer_name;
    $mobile_number = $row_goaxles->b2b_cust_phone;
    $service_center = $row_goaxles->shop_name;
    $vehicle = $row_goaxles->brand." ".$row_goaxles->model;
	$garage_bill = $row_goaxles->b2b_bid_amt;
	$cust_rating = $row_goaxles->b2b_Rating;
	$cust_feedback = $row_goaxles->b2b_Feedback;
	$inspection_stage = $row_goaxles->b2b_check_in_report;
	$estimate_stage = $row_goaxles->b2b_vehicle_at_garage;
	$deliver_stage = $row_goaxles->b2b_vehicle_ready;
	$premium = $row_goaxles->b2b_partner_flag;
	$b2b_booking_id = $row_goaxles->b2b_booking_id;
	$b2b_tyre_brand = $row_goaxles->b2b_tyre_brand;
	$b2b_tyre_count = $row_goaxles->b2b_tyre_count;
	$feedback_status = $row_goaxles->feedback_status;

	$no = $no+1;
	
	$tr = '<tr>';
      
          $td1 = '<td style="vertical-align: middle;">'.$no.'</td>';
          $td2 = '<td style="vertical-align: middle;">'.$booking_id.'</td>';
          $td3 = '<td style="vertical-align: middle;">'.date('d M Y', strtotime($goaxle_date)).'</td>';
   //        $td4 = '<td style="vertical-align: middle;">';
   //        if($acpt == '1')
			// {
			// 	$td4 = $td4."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Accepted!'></i>";
			// }
			// else if($acpt =='0')
			// {
			// 	$td4 = $td4."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Denied!'></i>";
			// }
			// else
			// {
			// 	$td4 = $td4."<i class='fa fa-exclamation-circle' aria-hidden='true' style='font-size:24px;color:#ffa800!important;' title='Not Accepted!'></i>";
			// }
   //        $td4 = $td4.'</td>';

		  $st_enc = base64_encode($startdate);
		  $ed_enc = base64_encode($enddate);
		  $dt_enc = $st_enc.':'.$ed_enc;
          $td5 = '<td style="vertical-align: middle;"><a href="feedback_details.php?bi='.base64_encode($b2b_booking_id).'&bookid='.base64_encode($booking_id).'&pg='.base64_encode('g').'&dt='.base64_encode($dt_enc).'"><i id="'.$b2b_booking_id.'" class="fa fa-eye" aria-hidden="true"></i></td>';
     		
          $td6 = '<td style="vertical-align: middle;">'.$name.'</td>';
          $td7 = '<td style="vertical-align: middle;">'.$mobile_number.'</td>';
          $td8 = '<td style="vertical-align: middle;">'.$service_center;
		  if ($premium == 2) {  $td8 = $td8."<img src='images/authorized.png' style='width:22px;' title='Premium Garage'>";}
		  $td8 = $td8.'</td>';
		  $td9 = '<td style="vertical-align: middle;">'.$vehicle.'</td>';	
		  $td10 = '<td style="vertical-align: middle;">'.$b2b_tyre_brand.'</td>';
		  $td11 = '<td style="vertical-align: middle;">'.$b2b_tyre_count.'</td>';	
		  $td12 = '<td style="vertical-align: middle;">';
		  $garage_bill!='' ? $td12 = $td12.'<i class="fa fa-inr" aria-hidden="true" style="padding-right: 2px;padding-left: 1px;"></i>'.$garage_bill : $td12 = $td12.'Not updated';
		  $td12 = $td12.'</td>';
		  // $td13 = '<td style="vertical-align: middle;">';
		  // $cust_rating!='' ? $td13 = $td13.$cust_rating : $td13 = $td13.'Not updated';
		  // $td13 = $td13.'</td>';
		  // $td14 = '<td style="vertical-align: middle;">';
		  // $cust_feedback!='' ? $td14 = $td14.$cust_feedback : $td14 = $td14.'Not updated';
		  // $td14 = $td14.'</td>';
		  
		  // $td16 = "<td style='vertical-align: middle;'>";
		  // $b2b_bid_flg == '1' ? $td16 = $td16."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Done!'></i>" : $td16 = $td16."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Not updated!'></i>";
		  // $td16 = $td16."</td>";
		  


		  // $td17 = "<td style='vertical-align: middle;'>";
		  // $feedback_status == '1' ? $td17 = $td17."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Done!'></i>" : $td17 = $td17."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Not updated!'></i>";
		  // $td17 = $td17."</td>";
          
		  
		  $tr_l = '</tr>';
      
          $str = $tr.$td1.$td2.$td3.$td5.$td6.$td7.$td8.$td9.$td10.$td11.$td12.$tr_l;
          $data[] = array('tr'=> $str);
            
        
    }
       echo $data1 = json_encode($data);
} // if

else {
  echo "no";
}


?>


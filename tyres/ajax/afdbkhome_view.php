<?php
//error_reporting(E_ALL); ini_set('display_errors', 1);
include("../config.php");
$conn = db_connect1(); 
session_start();


$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$city = $_GET['city'];
$shop_id = $_GET['shop_id'];
//$vehicle = $_GET['vehicle'];

$_SESSION['crm_city'] = $city;

function run_query($conn, $query){
	//echo $query;
	$count_no_goaxles = 0;
	$count_no_others = 0;
	$count_no_completed = 0;
	$count_no_cancelled = 0;
	$query_result = mysqli_query($conn,$query);
	while($query_rows = mysqli_fetch_object($query_result))
	{
		$booking_id = $query_rows->booking_id;
		$service_status = $query_rows->service_status;
		$feedback_status = $query_rows->feedback_status;
		$b2b_acpt_flg = $query_rows->b2b_acpt_flg;
		$b2b_bid_flg = $query_rows->b2b_bid_flg;
		


		if($feedback_status == 0 && $service_status == '' && $b2b_bid_flg==1 )
		{	
			$count_no_goaxles = $count_no_goaxles + 1;
		}
		else if($feedback_status == 0 && $service_status != '')
		{
			$count_no_others = $count_no_others + 1;
		}
		else if($feedback_status == 1 && $service_status == 'Purchased')
		{
			$count_no_completed = $count_no_completed + 1;
		}
		else if($feedback_status == 5 && $service_status == 'Cancelled')
		{
			$count_no_cancelled = $count_no_cancelled + 1;
		}
	}
	$rtn['count_no_goaxles'] = $count_no_goaxles;
	$rtn['count_no_others'] = $count_no_others;
	$rtn['count_no_completed'] = $count_no_completed;
	$rtn['count_no_cancelled'] = $count_no_cancelled;
	return $rtn;
}


function run_query2($conn, $query){
	$count_no_followups = 0;
	$count_no_reservics = 0;
	$query_result = mysqli_query($conn,$query);
	while($query_rows = mysqli_fetch_object($query_result))
	{
		$service_status = $query_rows->service_status;
		$feedback_status = $query_rows->feedback_status;
		//$reservice_flag = $query_rows->reservice_flag;

		if($feedback_status == 2 || $feedback_status== 3 || $feedback_status > 10)
		{	
			$count_no_followups = $count_no_followups + 1;
		}
		// else if($feedback_status == 1 && $service_status == 'Completed' && $reservice_flag == 1)
		// {
		// 	$count_no_reservics = $count_no_reservics + 1;
		// }
	}
	$rtn['count_no_followups'] = $count_no_followups;
	//$rtn['count_no_reservics'] = $count_no_reservics;
	return $rtn;
}


$cond ='';
 
$cond = $cond.($city == 'all' ? "" : "AND f.city='$city'");
//$cond = $cond.($vehicle == 'all' ? "" : "AND f.veh_type='$vehicle'");
$cond = $cond.($shop_id == 'all' ? "" : "AND f.shop_id='$shop_id'");


$select_sql = "SELECT f.booking_id,f.service_status,f.feedback_status,s.b2b_acpt_flg,s.b2b_bid_flg,f.reservice_flag  FROM feedback_track_tyres f left join b2b.b2b_booking_tbl_tyres s on f.booking_id=s.gb_booking_id WHERE f.flag!=1 {$cond} AND f.shop_id NOT IN (1014,1035,1670) AND s.b2b_bid_flg= 1 ";
//echo $select_sql;
$count_arr = run_query($conn, "{$select_sql} AND DATE(f.crm_update_time) BETWEEN '$startdate' AND '$enddate' GROUP BY s.gb_booking_id");

//echo $count_arr;
$selct_sql2="SELECT f.booking_id,f.feedback_status,f.service_status,f.reservice_flag from feedback_track_tyres f where f.flag!=1 {$cond} AND f.shop_id NOT IN (1014,1035,1670) ";
$count_arr2=run_query2($conn, "{$selct_sql2} AND DATE(f.feedback_followup) BETWEEN '$startdate' AND '$enddate' GROUP BY f.booking_id");

//echo $select_sql2;
//echo $count_arr2;

echo $data = json_encode(array('goaxlesCount'=>$count_arr['count_no_goaxles'], 'completedCount'=>$count_arr['count_no_completed'], 'othersCount'=>$count_arr['count_no_others'],'cancelledCount'=>$count_arr['count_no_cancelled'],'followupsCount'=>$count_arr2['count_no_followups']));
//echo $select_sql;
?>


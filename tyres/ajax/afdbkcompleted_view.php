<?php
//error_reporting(E_ALL);
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$super_flag = $_SESSION['super_flag'];
$se_team = array('crm117','crm087','crm015','crm123');
$se_access = 0;
if(in_array($crm_log_id,$se_team))
{
	$se_access = 1;
}

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$person = $_GET['person'];
$shop_id = $_GET['shop_id'];
$city = $_GET['city'];

$_SESSION['crm_city'] = $city;

$cond ='';


$cond = $cond.($shop_id == 'all' ? "" : "AND f.shop_id='$shop_id'");
$cond = $cond.($person == 'all' ? "" : "AND f.crm_log_id='$person'");
$cond = $cond.($city == 'all' ? "" : "AND f.city='$city'");

$sql_goaxles ="SELECT f.booking_id,b.b2b_log,b.b2b_acpt_flg,b.b2b_bid_flg,b.b2b_customer_name,b.b2b_cust_phone,f.shop_name,f.crm_log_id,b.brand,b.model,f.service_type,b.b2b_bid_amt,f.rating,b.b2b_Feedback,b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_vehicle_ready,f.comments,b.b2b_tyre_brand,b.b2b_tyre_count,f.final_bill_amt,crm.name as crm_name,f.b2b_booking_id,f.feedback_status,f.tyre_count,f.tyre_brand FROM feedback_track_tyres f LEFT JOIN b2b.b2b_booking_tbl_tyres b ON b.gb_booking_id = f.booking_id and f.shop_id=b.b2b_shop_id LEFT JOIN crm_admin crm ON f.crm_log_id = crm.crm_log_id LEFT JOIN tyre_booking_tb as ub ON ub.booking_id=b.gb_booking_id LEFT JOIN user_vehicle_table uv ON uv.id=ub.user_veh_id WHERE f.feedback_status = 1 AND f.flag = 0 {$cond} AND f.shop_id NOT IN (1014,1035,1670) AND DATE(f.crm_update_time) BETWEEN '$startdate' AND '$enddate' GROUP BY f.booking_id DESC";
// echo $sql_goaxles;
$res_goaxles = mysqli_query($conn,$sql_goaxles);
$no = 0;
$arr = array();
$count = mysqli_num_rows($res_goaxles);
if($count >0){
  while($row_goaxles = mysqli_fetch_object($res_goaxles)){
    $booking_id = $row_goaxles->booking_id;
    $b2b_booking_id = $row_goaxles->b2b_booking_id;
    $goaxle_date = $row_goaxles->b2b_log;
    $acpt = $row_goaxles->b2b_acpt_flg;
    $b2b_bid_flg = $row_goaxles->b2b_bid_flg;
    $name = $row_goaxles->b2b_customer_name;
    $mobile_number = $row_goaxles->b2b_cust_phone;
    $service_center = $row_goaxles->shop_name;
    $vehicle = $row_goaxles->brand." ".$row_goaxles->model;
	$service_type = $row_goaxles->service_type;
	$garage_bill = $row_goaxles->b2b_bid_amt;
	$cust_rating = $row_goaxles->rating;
	$cust_feedback = $row_goaxles->b2b_Feedback;
	$inspection_stage = $row_goaxles->b2b_check_in_report;
	$estimate_stage = $row_goaxles->b2b_vehicle_at_garage;
	$deliver_stage = $row_goaxles->b2b_vehicle_ready;
	$final_bill_paid = $row_goaxles->final_bill_amt;
	$comments = $row_goaxles->comments;
	$alloted_to = $row_goaxles->crm_name;
	$alloted_to_id = $row_goaxles->crm_log_id;
	$premium = $row_goaxles->b2b_partner_flag;
	$b2b_tyre_brand = $row_goaxles->b2b_tyre_brand;
	$b2b_tyre_count = $row_goaxles->b2b_tyre_count;
	$feedback_status = $row_goaxles->feedback_status;
	$actual_tyre_count = $row_goaxles->tyre_count;
	$actual_tyre_brand= $row_goaxles->tyre_brand;
	
	$no = $no+1;
    if($alloted_to_id =='' || $alloted_to_id==' ' || $alloted_to_id=='0'){
		$tr = '<tr style="background-color:#EF5350 !important;">';
	}
	else{
		$tr = '<tr>';
	}
      
          $td1 = '<td style="vertical-align: middle;">'.$no.'</td>';
          $td1 = $td1.'<td style="vertical-align: middle;">'.$booking_id.'</td>';
          $td2 = '<td style="vertical-align: middle;">'.date('d M Y', strtotime($goaxle_date)).'</td>';
          if($se_access==1)
		  {
			$td3 = '';
		  }
		  else
		  {
			$td3 = '<td style="vertical-align: middle;"><a href="feedback_details.php?bi='.base64_encode($b2b_booking_id).'&bookid='.base64_encode($booking_id).'&pg='.base64_encode('c').'"><i id="'.$b2b_booking_id.'" class="fa fa-eye" aria-hidden="true"></i></td>';
		  }
		  $td4 = '<td style="vertical-align: middle;">'.$name.'</td>';
          if($se_access==1)
		  {
			$td5 = '';
		  }
		  else
		  {
			$td5 = '<td style="vertical-align: middle;">'.$mobile_number.'</td>';
		  }
          $td6 = '<td style="vertical-align: middle;">'.$service_center;
		  if ($premium == 2) {  $td6 = $td6."<img src='images/authorized.png' style='width:22px;' title='Premium Garage'>";}
		  $td6 = $td6.'</td>';
		  $td7 = '<td style="vertical-align: middle;">'.$vehicle.'</td>';
		  $td8 = '<td style="vertical-align: middle;">'.$b2b_tyre_brand.'</td>';
		  $td20 = '<td style="vertical-align: middle;">'.$actual_tyre_brand.'</td>';
		  $td18 = '<td style="vertical-align: middle;">'.$b2b_tyre_count.'</td>';
		  $td19 = '<td style="vertical-align: middle;">'.$actual_tyre_count.'</td>';
		  $td9 = '<td style="vertical-align: middle;">'.$alloted_to.'</td>';	
		  $td10 = '<td style="vertical-align: middle;">';
		  $garage_bill!='' ? $td10 = $td10.'<i class="fa fa-inr" aria-hidden="true" style="padding-right: 2px;padding-left: 1px;"></i>'.$garage_bill : $td10 = $td10.'Not updated';
		  $td10 = $td10.'</td>';
		  $td11 = '<td style="vertical-align: middle;">';
		  $cust_rating!='' ? $td11 = $td11.$cust_rating : $td11 = $td11.'Not updated';
		  $td11 = $td11.'</td>';
		  $td12 = '<td style="vertical-align: middle;">';
		  $cust_feedback!='' ? $td12 = $td12.$cust_feedback : $td12 = $td12.'Not updated';
		  $td12 = $td12.'</td>';
		  $td13 = '<td style="vertical-align: middle;"><i class="fa fa-inr" aria-hidden="true" style="padding-right: 2px;padding-left: 1px;"></i>'.$final_bill_paid.'</td>';
		  $td14 = '<td style="vertical-align: middle;">'.$comments.'</td>';
		  $td15 = "<td style='vertical-align: middle;'>";
		  $b2b_bid_flg == '1' ? $td15 = $td15."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Done!'></i>" : $td15 = $td15."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Not updated!'></i>";
		  $td15 = $td15."</td>";
		 //   $td16 = '<td style="vertical-align: middle;">';
   //        if($acpt == '1')
			// {
			// 	$td16 = $td16."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Accepted!'></i>";
			// }
			// else if($acpt =='0')
			// {
			// 	$td16 = $td16."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Denied!'></i>";
			// }
			// else
			// {
			// 	$td16 = $td16."<i class='fa fa-exclamation-circle' aria-hidden='true' style='font-size:24px;color:#ffa800!important;' title='Not Accepted!'></i>";
			// }
   //        $td16 = $td16.'</td>';

		  $td17 = "<td style='vertical-align: middle;'>";
		  $feedback_status == '1' ? $td17 = $td17."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Done!'></i>" : $td17 = $td17."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Not updated!'></i>";
		  $td17 = $td17."</td>";
		  $tr_l = '</tr>';
      
          $str = $tr.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$td20.$td18.$td19.$td9.$td10.$td11.$td12.$td13.$td14.$td15.$td17.$tr_l;
          $data[] = array('tr'=>$str);
            
        }
       
        echo $data1 = json_encode($data);
} // if
else {
  echo "no";
}


?>

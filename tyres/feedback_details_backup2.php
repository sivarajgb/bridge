<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
//error_reporting( error_reporting() & ~E_NOTICE );
include("sidebar.php");
$conn = db_connect1();
$conn2 = db_connect2();
// login or not
  if((empty($_SESSION['crm_log_id']))) {
	header('location:logout.php');
	die();
}  
$crm_log_id = $_SESSION['crm_log_id'];
$feedback_admin = $_SESSION['feedback_admin'];
$today=date('Y-m-d H:i:s');

$booking_id=base64_decode($_GET['bi']);
$book_id=base64_decode($_GET['bookid']);
$se_team = array('crm117','crm015','crm135','crm144','crm145','crm232','crm237','crm239','crm240');
$se_access = 0;
if(in_array($crm_log_id,$se_team))
{
	$se_access = 1;
}

if($booking_id == ''){
  header('location:somethingwentwrong.php');
	die();
}
?>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>GoBumpr Bridge</title>

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>

<style>
.borderless td, .borderless th {
    border: none !important;
}
.datepicker {
	cursor:pointer;
}
.datepicker:before {
	content: '';
	display: inline-block;
	border-left: 7px solid transparent;
	border-right: 7px solid transparent;
	border-bottom: 7px solid #ccc;
	border-bottom-color: transparent !important;
	position: absolute;
	top: -7px;
	left: 190px;  // I made a change here 
}

.datepicker:after {
	content: '';
	display: inline-block;
	border-left: 6px solid transparent;
	border-right: 6px solid transparent;
	border-top-color: transparent !important;
	border-top: 6px solid #ffffff;
	position: absolute;
	bottom: -6px;
	left: 191px;  // I made a change here 
}
span.make-switch.switch-radio {
    float: left;
}
.bootstrap-switch-container
{
	height:30px;
}
.slider {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 17px;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* slider Content */
.slider-content {
   /* background-color: #fefefe;*/
    margin: auto;
    margin-top: -90px;
    padding: 20px;
   /* border: 1px solid #888;*/
    width: 80%;
}
.rating-container .rating-input
{
	width: 0px !important;
}
</style>

</head>

<body>
<?php include_once("header.php"); ?>

<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>
<?php
	$sql_get = "SELECT r.source,r.name,r.mobile_number,r.mobile_number2,DATE(f.log) as log,f.user_id,f.b2b_booking_id,f.service_status,f.service_type,b2b.b2b_check_in_report,b2b.b2b_vehicle_at_garage,b2b.b2b_vehicle_ready,b2b.brand,b2b.model,f.shop_id,f.shop_name,m.b2b_address1,m.b2b_address2,m.b2b_address3,m.b2b_address4,m.b2b_mobile_number_1,m.b2b_mobile_number_2,f.feedback_status,ub.locality,ub.pickup_date_time,b2b.b2b_bid_amt,b2b.b2b_contacted_log,b2b.b2b_contacted,ub.service_description,ub.pickup_full_address,rt.b2b_total_amt,f.reservice_flag,m.b2b_avail,b2b.b2b_acpt_flg FROM feedback_track_tyres f LEFT JOIN user_register r ON f.user_id = r.reg_id LEFT JOIN b2b.b2b_booking_tbl_tyres b2b ON b2b.b2b_booking_id = f.b2b_booking_id LEFT JOIN b2b.b2b_mec_tbl m ON m.b2b_shop_id = f.shop_id LEFT JOIN tyre_booking_tb ub ON f.booking_id=ub.booking_id left join b2b.b2b_rate_description_tbl as rt on b2b.b2b_booking_id=rt.b2b_booking_id WHERE f.b2b_booking_id = '$booking_id' AND b2b.b2b_flag = 0;";
	// echo $sql_get;
	// die;
	$res_get = mysqli_query($conn,$sql_get);
	$row_get = mysqli_fetch_object($res_get);
	$user_name = $row_get->name;
	$user_id = $row_get->user_id;
	$b2b_booking_id = $row_get->b2b_booking_id;
	$mobile = $row_get->mobile_number;
	$alt_mobile = $row_get->mobile_number2;
	$goaxle_date = $row_get->log;
	$service_type = $row_get->service_type;
	$service_sts = $row_get->service_status;
	$vehicle_checkin = $row_get->b2b_check_in_report;
	$vehicle_at_garage = $row_get->b2b_vehicle_at_garage;
	$vehicle_ready = $row_get->b2b_vehicle_ready;
	$vehicle = $row_get->brand.' '.$row_get->model;
	$shop_id = $row_get->shop_id;
	$service_partner = $row_get->shop_name;
	$address = $row_get->b2b_address1.', '.$row_get->b2b_address2.', '.$row_get->b2b_address3.',<br>'.$row_get->b2b_address4;
	$b2b_mobile = $row_get->b2b_mobile_number_1;
	$b2b_alt_mobile = $row_get->b2b_mobile_number_2;
	$feedback_status = $row_get->feedback_status;
	$fdbck_status = $service_status;
	$locality = $row_get->locality;
	$reservice_flag = $row_get->reservice_flag;
	$source = $row_get->source;
	$service_description = $row_get->service_description;
	$pickup_address = $row_get->pickup_full_address;
	$pickup_date_time = $row_get->pickup_date_time;
	$b2b_total_amt = $row_get->b2b_total_amt; 
	$b2b_bill_amount = $row_get->b2b_bid_amt;
	$b2b_contacted = $row_get->b2b_contacted;
	$b2b_avail = $row_get->b2b_avail;
	$inspection_stage = $row_get->b2b_acpt_flg;

	$language_of_contact = $row_get->language_of_contact;
	if($b2b_contacted=='1'){
	$b2b_contacted_log = $row_get->b2b_contacted_log;
	}
	if(empty($language_of_contact) || $language_of_contact=='null'){
		$language_of_contact = "-";
	}
	if(empty($service_description) || $service_description=='null'){
		$service_description = "-";
	}
	if(empty($pickup_address) || $pickup_address=='null'){
		$pickup_address = "-";
	}
	if(empty($pickup_date_time) || $pickup_date_time=='null' || $pickup_date_time=="0000-00-00 00:00:00"){
		$pickup_date_time = "-";
	}
	if(empty($b2b_contacted_log) || $b2b_contacted_log=='null' || $b2b_contacted_log=="0000-00-00 00:00:00"){
		$b2b_contacted_log = "-";
	}
	if(empty($b2b_bill_amount)){
		$b2b_bill_amount = 0;
	}
	if(empty($b2b_total_amt)){
		$b2b_total_amt = 0;
	}
			if($b2b_avail=='1'){
				$b2b_avail='Open';
			}else{
				$b2b_avail='Closed';
			}

	$sql = "SELECT b2b_partner_flag FROM b2b_credits_tbl WHERE b2b_shop_id='$shop_id'";
	$res = mysqli_query($conn2,$sql);
	$row = mysqli_fetch_object($res);
	$premium = $row->b2b_partner_flag;
	
	if($vehicle_checkin == 1 || $vehicle_at_garage == 1 || $vehicle_ready == 1)
	{
		$rtt_updates = 1;
	}
	
	$sql_userbooking_userid = "SELECT * FROM tyre_booking_tb WHERE source='GoBumpr App' AND user_id = '$user_id'";
	$res_userbooking_userid = mysqli_query($conn,$sql_userbooking_userid);
	$row_userbooking_userid = mysqli_fetch_object($res_userbooking_userid);
	
	if(($source == 'GoBumpr App') || (!empty($row_userbooking_userid))){
		$app_status = "<i style='color: green;font-size: 24px;margin-left: 8px;' class='fa fa-android' aria-hidden='true'></i>";
	}
	else{
		$app_status = "";
	}
?>
<div id="user" style="border:2px solid #708090; border-radius:8px;margin-left:1%; width:27%;height:480px; padding:20px; margin-top:18px;margin-bottom:18px; float:left;overflow-y:auto">
	<table id="table1" class="table borderless">
		<tr><td><strong>BookingId</strong></td><td><?php echo $book_id;?></td></tr>
		<tr><td><strong>Name</strong></td><td><?php echo $user_name;?></td></tr>
		<tr><td><strong>Phn No.</strong></td><td><?php echo $mobile.$app_status?></td></tr>
		<tr><td><strong>Alt PhnNo.</strong></td><td><?php echo $alt_mobile;?></td></tr>
		<tr><td><strong>Vehicle</strong></td><td><?php echo $vehicle;?></td></tr>
		<tr><td><strong>GoAxle Date</strong></td><td><?php echo date('d-m-Y',strtotime($goaxle_date));?></td></tr>
		<tr><td><strong>ServiceStatus</strong></td><td><?php echo $service_sts;?></td></tr>
		<tr><td><strong>Locality</strong></td><td><?php echo $locality;?></td></tr>
		<tr><td><strong>Service Description</strong></td><td><?php echo $service_description;?></td></tr>
		<tr><td><strong>Pick Up Address</strong></td><td><?php echo $pickup_address;?></td></tr>
		<tr><td><strong>Pick Up Time</strong></td><td><?php echo $pickup_date_time!='-' ?   date('d M Y h.i A',strtotime($pickup_date_time)): '-';?></td></tr>
		<tr><td><strong>Garage Contacted Time</strong></td><td><?php echo $b2b_contacted_log!='-' ?   date('d M Y h.i A',strtotime($b2b_contacted_log)): '-';?></td></tr>
		<tr><td><strong>Language of contact</strong></td><td><?php echo $language_of_contact!='-' ? $language_of_contact: '-';?></td></tr>
	</table>
</div>

<div id="history" align="center" style="overflow-y:auto;margin-left:1%;border:2px solid #708090; border-radius:8px; width:45%;height:480px; padding:20px; margin-top:18px; margin-bottom:18px; float:left;">
<?php
	$sql_b2b_images = "SELECT bb.b2b_check_in_report,bb.b2b_acpt_flg,bb.b2b_bid_amt,bb.b2b_vehicle_at_garage,bb.b2b_vehicle_ready,f.feedback_status,f.final_bill_amt FROM b2b.b2b_booking_tbl_tyres bb LEFT JOIN feedback_track_tyres f ON bb.b2b_booking_id = f.b2b_booking_id WHERE bb.b2b_acpt_flg = 1 AND 	bb.b2b_booking_id = '$b2b_booking_id'";
	 //  echo $sql_b2b_images;
	 // die;
	$res_b2b_images= mysqli_query($conn,$sql_b2b_images);
	$row_b2b_images = mysqli_fetch_object($res_b2b_images);

	$b2b_amt = $row_b2b_images->b2b_bid_amt;
	$acpt = $row_b2b_images->b2b_acpt_flg;
	$deliver_stage = $row_b2b_images->b2b_vehicle_ready;
	$feedbacks_status = $row_b2b_images->feedback_status;
	$final_bill = $row_b2b_images->final_bill_amt;
	
	
	$td16 = "<td style='text-align: center;'>";
	$acpt == '1' ? $td16 = $td16."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Done!'></i>" : $td16 = $td16."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Not updated!'></i>";
	$td16 = $td16."</td>";
	$td17 = "<td style='text-align: center;'>";
	$feedbacks_status == '1' ? $td17 = $td17."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Done!'></i>" : $td17 = $td17."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Not updated!'></i>";
	$td17 = $td17."</td>";
	$td17=$td17."</tr>";
 


		$td19 = "<td style='text-align: center;'><i class='fa fa-inr' aria-hidden='true'>&nbsp".$b2b_amt."</i></td>";

	
		$td20 = "<td style='text-align: center;'><i class='fa fa-inr' aria-hidden='true'>&nbsp".$final_bill."</i></td></tr>";

	$td17 = $td17.$td19.$td20;

	echo '<table class="table table-hover table-striped"><thead><tr>
	<th style="text-align:center;text-align:center;" colspan="10">RTT</th>
	</tr>
	<tr>
	<th style="text-align:center;text-align: center;">BidAccepted</th>
	<th style="text-align:center;text-align: center;">Delivered</th>
	<tbody>'.$td16.$td17.'</tbody></table>';
?>

<table class='table table-hover table-striped'><thead><tr><th>No</th><th>Activity</th><th>Date</th><th>Support</th></tr></thead><tbody>
	<?php
	 $reg_array=array();


		$sql1=mysqli_query($conn,"SELECT * FROM tyre_booking_tb WHERE booking_id='$book_id' ORDER by log DESC");
		while($row1=mysqli_fetch_object($sql1)){
			$bking_id = $row1->booking_id;
			$user_id  = $row1->user_id;
			if(($row1->flag==1)&&($row1->flag_unwntd==1)&&($row1->activity_status==''))
			{
				continue;
			}
			$reg_array[]=$row1;
			if($row1->axle_flag==1)
			{
        $sql6=mysqli_query($conn,"SELECT b.gb_booking_id,b.b2b_log as log,m.b2b_shop_name,b.b2b_acpt_flg,b.b2b_bid_flg,b.b2b_bid_amt FROM b2b.b2b_booking_tbl_tyres b LEFT JOIN tyre_booking_tb u ON b.gb_booking_id = u.booking_id LEFT JOIN b2b.b2b_mec_tbl as m ON b.b2b_shop_id = m.b2b_shop_id WHERE b.gb_booking_id ='$bking_id'");
				//$sql6=mysqli_query($conn,"SELECT g.go_booking_id,g.b2b_shop_id,g.sent_log as log,b.b2b_shop_name,u.service_type FROM goaxle_track g,b2b.b2b_mec_tbl b,user_booking_tb u WHERE g.b2b_shop_id = b.b2b_shop_id AND g.go_booking_id = '$bking_id' AND u.booking_id = '$bking_id'");
				while($row6=mysqli_fetch_object($sql6))
				{
					$reg_array[]=$row6;
				}
			}
		}
		
		
		$sql5=mysqli_query($conn,"SELECT c.*,c.Follow_up_date AS cmnt_followup_date,b.rating as rating,b.feedback as feedback FROM tyres_comments_tbl as c LEFT JOIN tyre_booking_tb as b ON c.book_id=b.booking_id WHERE c.book_id='$bking_id' ORDER by c.log DESC");
		$status_comments=mysqli_num_rows($sql5);
		while($row5=mysqli_fetch_object($sql5)){
			$reg_array[]=$row5;
		}
		function do_compare($item1, $item2) {
	    $ts1 = strtotime($item1->log);
	    $ts2 = strtotime($item2->log);
	    return $ts2 - $ts1;
	     }
	usort($reg_array, 'do_compare');

		 $arr_count = count($reg_array);

		  $x=0;
		  $axle = false;
		 for($i=0;$i<$arr_count;$i++) {
					  $x=$x+1;
	  if(isset($reg_array[$i]->gb_booking_id))
		{
      $activity = '';
      //$accept = $reg_array[$i]->b2b_acpt_flg;
      $bidplaced = $reg_array[$i]->b2b_bid_flg;
      $bidamt = $reg_array[$i]->b2b_bid_amt;
      //$deny = $reg_array[$i]->b2b_deny_flag;
      if($bidplaced == 1 && $bidamt!=0){
                $activity = $activity.'Bid Accepted by <p style="background-color:#6FA3DE;padding:7px;width:30px;border-radius:18px;float:left;margin-right:3px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
    }
    else if($bidplaced == 0 && $bidamt==0){
      $activity = $activity.'GoAxled to <p style="background-color:#69ED85;padding:7px;width:30px;border-radius:18px;float:left;margin-right:3px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
    } 
     else if($bidplaced == 1 && $bidamt == 0){
      $activity = $activity.'Bid Placed to <p style="background-color:#D25F34;padding:7px;width:30px;border-radius:18px;float:left;margin-right:3px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';

    }
    	if($bidamt == 0){
      $activity = $activity.$reg_array[$i]->b2b_shop_name." - ".$reg_array[$i]->gb_booking_id;
    }
    	else
    	{
    		$activity = $activity.$reg_array[$i]->b2b_shop_name." - ".$reg_array[$i]->gb_booking_id."<br><strong>Amt: </strong>Rs.".$bidamt;
    	}
      $support = '';
      if($reg_array[$i]->log == '0000-00-00 00:00:00')
      $log_date=date("d-m-Y h:i a");
      else
      $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
		 }
	 elseif(isset($reg_array[$i]->booking_id)){
					      	 $activity =  'BOOKING -'.$reg_array[$i]->booking_id.' -'.$reg_array[$i]->shop_name ;
							 $service_type = $reg_array[$i]->service_type;
               $support= $reg_array[$i]->crm_update_id;
			   $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
			  
					      	  }
	 
	 elseif(isset($reg_array[$i]->com_id)){
     $cmt = $reg_array[$i]->comments;
     $support= $reg_array[$i]->crm_log_id;
     $service_status = $reg_array[$i]->service_status;
	 if($reg_array[$i]->cmnt_followup_date!='0000-00-00')
	 {
	 
		$followup_on='<br><strong>Next Follow Up On</strong>- '.date("d-M-Y",strtotime($reg_array[$i]->cmnt_followup_date));
	 }
	 else
	 {
		$followup_on="";
	 }
     if($cmt == ''){
       $cmt="";
     }
	      $act_status = $reg_array[$i]->status;
	      $cat = $reg_array[$i]->category;
	      $newstring = substr($cat, -9);
	      //echo $newstring;
        if($act_status == ""){
          $act_status = "FollowUp";
        }
        if($act_status == "Cancelled"){
            $activity = $reg_array[$i]->category;
            if($cmt !=''){
              $activity = $activity.'-'.$cmt;
            }
            $activity = $activity;
			      $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
        }
        else if($act_status == "Reverted"){
          	$activity = $reg_array[$i]->category.$followup_on;
			      $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
        }
        else if($act_status == "FeedBack"){
          $activity = '';
          $rating = $reg_array[$i]->rating;
          $feedback = $reg_array[$i]->feedback;
          $activity = $activity.$reg_array[$i]->category;

          if($cmt !=''){
            $activity = $activity.'<br><strong>Comments</strong>-'.$cmt;
          }
        
          if($newstring == 'Purchased')
          {
          $activity = $activity;
          if($rating>0){
            $activity = $activity.'<br><strong>Rating</strong>-';
            switch($rating){
              case '1':$activity = $activity.'<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i>'; break;
              case '2':$activity = $activity.'<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i>'; break;
              case '3':$activity = $activity.'<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i>'; break;
              case '4':$activity = $activity.'<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i>'; break;
              case '5':$activity = $activity.'<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i>'; break;
              case '0.5':$activity = $activity.'<i class="fa fa-star-half" aria-hidden="true" style="color:#ffa800;"></i>'; break;
              case '1.5':$activity = $activity.'<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star-half" aria-hidden="true" style="color:#ffa800;"></i>'; break;
              case '2.5':$activity = $activity.'<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star-half" aria-hidden="true" style="color:#ffa800;"></i>'; break;
              case '3.5':$activity = $activity.'<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star-half" aria-hidden="true" style="color:#ffa800;"></i>'; break;
              case '4.5':$activity = $activity.'<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star-half" aria-hidden="true" style="color:#ffa800;"></i>'; break;
            }
            
          }

           }
           else{
           	$activity = $activity.$followup_on;
           }
          //echo $activity;
          
          $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
        }
        else{
            $activity = $act_status.'-'.$reg_array[$i]->category;
            if($cmt !=''){
              $activity = $activity.'-'.$cmt;
            }
            $activity = $activity.$followup_on;
			      $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
        }
		$service_type = '';

		}
	  /*if(isset($reg_array[$i]->booking_id)){
			$bkid = $reg_array[$i]->booking_id;
      if($booking_id == $bkid){
      ?>
      <tr style="background-color:#B2DFDB;">
      <?php
      }
      else{
      ?>
      <tr>
      <?php
      }
    }*/
	//$veh_no = $activity;
	if (strlen(stristr($activity, 'BOOKING -'))>0) {
		
		$bkid=explode("-",$activity);
		$v = explode(" ",$bkid[1]);
		$bookid = $v[0];
		
		if($booking_id===$bookid)
		{
		?>
			<tr style="background-color:#B2DFDB;">
			<?php
		}
		else{
		?>
		  <tr>
		  <?php
		}
		}
		else{
		?>
		  <tr>
		  <?php
		}
	 ?>
	 <td><?php echo $x;?></td>
	 <td><?php echo $activity ; ?></td>
	 <td><?php echo $log_date;?></td>
	 <td><?php
	//echo "SELECT crm_lod_id FROM admin_comments_tbl WHERE log='$x' ";
	  $sql_crm_name = mysqli_query($conn,"SELECT name FROM crm_admin WHERE crm_log_id = '$support' ");
	  $row_sup_name= mysqli_fetch_array($sql_crm_name);
	  echo $sup_name=$row_sup_name['name']; ?></td>
	 </tr>
	 <?php
	 //sleep(1);
	flush();
    ob_flush();
		 }
		 ?>

	</tbody></table>
	
</div>
<div id="garage" style="margin-left:1%;margin-right:1%;border:2px solid #708090; border-radius:8px; width:24%;height:480px; padding:20px; margin-top:18px; margin-bottom:18px;float:left;overflow-y:auto">
   <div  id="division">
    <table class="table borderless" style=" border-collapse: collapse;">
		<tr><td><strong>ServicePartner</strong></td><td><?php echo $service_partner;?></td></tr>
		<tr><td><strong>Address</strong></td><td><?php echo $address;?></td></tr>
		<tr><td><strong>Mobile</strong></td><td><?php echo $b2b_mobile;?></td></tr>
		<tr><td><strong>Alt Mobile</strong></td><td><?php echo $b2b_alt_mobile;?></td></tr>
		<tr><td><strong>Availability</strong></td><td><?php echo $b2b_avail;?></td></tr>
    </table>
    </div>
	
	<div class="row" style="margin-top: 2%;">
	<button class ="btn" id="garage_sms" name="garage_sms" style="background-color:springgreen;border-radius:10px;"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;&nbsp;Send Garage Details</button>
	<button class ="btn" id="RTTUpadate" name="RTTUpadate" style="background-color:springgreen;border-radius:10px;margin-right:-5%;display: inline-block;width: 40%;"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;&nbsp;Update RTT</button>
	</div>
	<div class="row" style="margin-top: 2%;">
	<button class ="btn" id="playstore_link" name="playstore_link" style="background-color:springgreen;border-radius:10px;display:  inline-block;width: 52%;"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;&nbsp;Send Play Store link</button>
	<button class ="btn" id="google_link" name="google_link" style="background-color:springgreen;border-radius:10px;margin-right:-5%;display: inline-block;width: 47%;"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;&nbsp;Send Google link</button>
	</div>

    </div>
<?php
 if($service_sts == 'Cancelled' && base64_decode($_GET['pg']) =='g')
{
?>
<div id="div2" align="center" style="display: none;">
	<button  type="button" class="btn btn-lg" data-toggle="modal" data-target="#feedbackModal" style="background-color:#47B862;margin-right:30px;"><i class="fa fa-comment" aria-hidden="true"></i>&nbsp;&nbsp;Update Status</button>
</div>
<?php
} 
else if($service_sts != 'Purchased' && $se_access == 0)
{
?>
<div id="div2" align="center" >
	<button  type="button" class="btn btn-lg" data-toggle="modal" data-target="#feedbackModal" style="background-color:#47B862;margin-right:30px;"><i class="fa fa-comment" aria-hidden="true"></i>&nbsp;&nbsp;Update Status</button>
</div>
<?php
}

else if($service_sts == 'Purchased' && base64_decode($_GET['pg'])!='c' && $se_access == 0){
	?>
	<div id="div2" align="center" >
		<button  type="button" class="btn btn-lg" data-toggle="modal" data-target="#feedbackModal" style="background-color:#47B862;margin-right:30px;"><i class="fa fa-comment" aria-hidden="true"></i>&nbsp;&nbsp;Update Status</button>
	</div>
	<?php
}
?>  

<!-- Modal -->
<div class="modal fade" id="feedbackModal" role="dialog" >
  <div class="modal-dialog" align="center" >
	<!-- Modal content-->
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h3 class="modal-title" align="left">Booking Id (<?php echo $book_id; ?>)</h3>
	  </div>
	  <div class="modal-body">
	  <?php $page = $_GET['pg']; ?>
	  <?php $dt = $_GET['dt']; ?>
		<form id="feedback<?php echo $booking_id; ?>" class="form" method="post" action="update_feedback.php">
		  <div class="row">
			<div class="col-xs-6 col-lg-offset-3 form-group">
			<br>
			<select class="form-control" id="status<?php echo $booking_id; ?>" name="status" required>
			  <?php
				
			  switch($fdbck_status){
				case 'RNR':?>
			  <option value="RNR" selected>RNR</option>
			  <option value="Purchased">Purchased</option>
			  <option value="Yet to Purchase" >Yet to Purchase</option>
			  <option value="Cancelled">Cancelled</option><?php break;
				case 'Completed':?>
			  <option value="RNR" >RNR</option>
			  <option value="Purchased" selected>Purchased</option>
			  <option value="Yet to Purchase" >Yet to Purchase</option>
			  <option value="Cancelled">Cancelled</option><?php break;
				case 'Yet to Purchase':?>
			  <option value="RNR" >RNR</option>
			  <option value="Purchased">Purchased</option>
			  <option value="Yet to Purchase" selected>Yet to Purchase</option>
			  <option value="Cancelled">Cancelled</option><?php break;
				case 'Cancelled':?>
			  <option value="RNR" >RNR</option>
			  <option value="Purchased">Purchased</option>
			  <option value="Yet to Purchase">Yet to Purchase</option>
			  <option value="Cancelled" selected>Cancelled</option><?php break;
				default: if($feedback_status > 10){
							?><option value="">Select Status</option>
							  <option value="RNR" selected>RNR</option>
							  <option value="Purchased" >Purchased</option>
							  <option value="Yet to Purchase" >Yet to Purchase</option>
							  <option value="Cancelled">Cancelled</option>
							  <?php
						}
						else{?><option value="" selected>Select Status</option>
			  <option value="RNR" >RNR</option>
			  <option value="Purchased" >Purchased</option>
			  <option value="Yet to Purchase" >Yet to Purchase</option>
			  <option value="Cancelled">Cancelled</option>
			  <?php
			  }
			  }
			  ?>
			</select>
			</div>
		  </div>
		  <div class="row"></div>
		  <div class="row" id="reason<?php echo $booking_id; ?>" style="display:none;">
			<div class="col-lg-2 col-xs-1 col-lg-offset-3 form-group">
			  <label style="padding-top:5px;">Reason</label>
			</div>
			<div class="col-lg-4 col-xs-4" style="font-size:8px;">
				<select class="form-control" name="reason" id="reason-select<?php echo $booking_id;?>" required>
					<option value="" selected>-Choose a reason-</option>
					<option value="Improper response from the garage" >Improper response from the garage</option>
					<option value="Customer will visit dealer" >Customer will visit dealer</option>
					<option value="Price mismatch" >Price mismatch</option>
				</select>
			</div>
		  </div>
			  
		<div class="row" id="subreason<?php echo $booking_id;?>" style="display:none;">
			<div class="col-lg-3 col-xs-1 col-lg-offset-2 form-group">
			  <label style="padding-top:5px;">Why Exactly</label>
			</div>
			<div class="col-lg-4 col-xs-4" style="font-size:8px;">
				<select class="form-control" name="subreason" id="subreasons<?php echo $booking_id;?>"  >
					
				</select>
			</div>
		</div>

		<div class="row" id="busyheldupreason<?php echo $booking_id;?>" style="display:none;">
			<div class="col-lg-3 col-xs-1 col-lg-offset-3 form-group">
			  <label style="padding-top:5px;">Pick Up</label>
			</div>
				<div class="col-lg-4 col-xs-4"  style="font-size:8px;">
					<span class="make-switch switch-radio">
						<input type="radio" data-on-text="Yes" data-off-text="No" data-on-color="success" data-off-color="danger" class="alert-status" id="pickup_switch">
					</span>
				</div>
		</div>
		  <div class="row" id="RNRreason<?php echo $booking_id; ?>" style="display:none;">
			<div class="col-lg-2 col-xs-1 col-lg-offset-3 form-group">
			  <label style="padding-top:5px;">Reason</label>
			</div>
			<div class="col-lg-4 col-xs-4" style="font-size:8px;">
				<select class="form-control" name="RNRreason" id="RNRreason-select<?php echo $booking_id;?>" required>
					<option value="" selected>-Choose a reason-</option>
					<option value="Ringing Not Responding" >Ringing Not Responding</option>
					<option value="Phone switched Off" >Phone switched Off</option>
					<option value="Phone Not Reachable" >Phone Not Reachable</option>
					<option value="Busy - Cut the call" >Busy - Cut the call</option>
					<option value="Cutting call Hearing GoBumpr">Cutting call Hearing GoBumpr</option>
				</select>
			</div>
		  </div>
		  <div class="row" id="cancelledReason<?php echo $booking_id; ?>" style="display:none;">
			<div class="col-lg-2 col-xs-1 col-lg-offset-3 form-group">
			  <label style="padding-top:5px;">Reason</label>
			</div>
			<div class="col-lg-4 col-xs-4" style="font-size:8px;">
				<select class="form-control" name="cancelledReason" id="cancelledReason-select<?php echo $booking_id;?>" required="">
					<option value="" selected>-Choose a reason-</option>
					<option value="Improper response from Garage" >Improper response from Garage</option>
					<option value="Location too far">Location too far</option>
					<option value="Poor quality" >Poor qualily</option>
					<option value="Price too high" >Price too high</option>
					<option value="Enquiry" >Enquiry</option>
					<option value="Others" >Others</option>
				</select>
			</div>
		  </div>
			<div class="row" id="servicePriority<?php echo $booking_id; ?>" style="display:none;">
				<div class="col-lg-3 col-xs-2 col-lg-offset-2 form-group">
					<label style="padding-top:5px;">Service Priority</label>
				</div>
				<div class="col-xs-5">
						<div class="form-group">
							<label class="radio-inline">
							<input type="radio" id="cool" name="service_priority" value="1" >Low
							<label>&nbsp
							<label class="radio-inline">
							<input type="radio" id="warm" name="service_priority" value="2" checked>Medium
							<label>&nbsp
							<label class="radio-inline">
							<input type="radio" id="hot" name="service_priority" value="3" >High
							<label>
						</div>
				</div>
		  	</div>
		  <div class="row" id="followupDate<?php echo $booking_id; ?>" style="display:none;">
				<div class="col-xs-3 col-lg-3 col-lg-offset-3 form-group" style="text-align:left;">
				  <label style="padding-top:5px;" id="followup_date_txt<?php echo $booking_id;?>">FollowUp Date</label>
				</div>
				<div class="col-xs-3">
				  <div class="form-group">
					<input type="text" name="followup_date" class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" />
				  </div>
				</div>
		  </div>
		  <div class="row" id="deliverydate<?php echo $booking_id; ?>" style="display:none;">
				<div class="col-xs-3 col-lg-3 col-lg-offset-3 form-group" style="text-align:left;">
				  <label style="padding-top:5px;" id="delivery_date_txt<?php echo $booking_id;?>">Delivery Date</label>
				</div>
				<div class="col-xs-3">
				  <div class="form-group">
					<input type="text" name="delivery_date" class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" />
				  </div>
				</div>
		  </div>
		  <div id="completed-div<?php echo $booking_id; ?>" style="display:none;">
		  	<div class="row">
			<div class="col-lg-2 col-xs-1 col-lg-offset-3 form-group">
			  <label style="padding-top:5px;">&nbsp;TyreBrand</label>
			</div>
			<div class="col-xs-4">
			  <div class="form-group">
				<select class="form-control" name="tyre_brand" id="tyre_brand">
				<option selected value="">Select tyre brand</option>
				<option value="JK Tyres">JK Tyres</option>
				<option value="MRF">MRF</option>
				<option value="Good Year">Good Year</option>
				<option value="CEAT">CEAT</option>
				<option value="Bridgestone">Bridgestone</option>
				<option value="Michelin">Michelin</option>
				<option value="Apollo">Apollo</option>
				<option value="Yokohama">Yokohama</option>
				<option value="Nexen">Nexen</option>
				 </select>
			  </div>
			</div>
		  </div>
		  <div class="row">
			<div class="col-lg-2 col-xs-1 col-lg-offset-3 form-group">
			  <label style="padding-top:5px;">&nbsp;TyreCount</label>
			</div>
			<div class="col-xs-4">
			  <div class="form-group">
				<select class="form-control" id="tyre_count" name="tyre_count">
			<option selected value="">Select tyre count</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			 </select>
			  </div>
			</div>
		  </div>
		  <div class="row">
			<div class="col-lg-2 col-xs-1 col-lg-offset-3 form-group">
			  <label style="padding-top:5px;">&nbsp;Garage</label>
			</div>
			<div class="col-xs-4">
			  <div class="form-group">
			 <select class="form-control" id="shopname" name="shopname">
			<option selected value="">Select garage</option>
			  	<?php 
	   $sql_shop="SELECT shop_name,shop_id FROM feedback_track_tyres WHERE booking_id = '".$book_id."' "; 
	 	$res_shop = mysqli_query($conn,$sql_shop);
              while($row_shop = mysqli_fetch_object($res_shop)){

               $shops = $row_shop->shop_name;   
               $axle = $row_shop->shop_id;  
			  	?>
			<option value="<?php echo $axle; ?>"><?php echo $shops; ?></option>

				<?php }
				?>
			 </select>
			  </div>
			</div>
		  </div>
		  <div class="row">
			<div class="col-lg-2 col-xs-1 col-lg-offset-3 form-group">
			  <label style="padding-top:5px;">Rating</label>
			</div>
			<div class="col-lg-4" style="font-size:8px;">
			  <input id="rating<?php echo $booking_id; ?>" name="rating" type="text" class="rating" data-min="0" data-max="5" data-step="0.5" data-stars=5 data-symbol="&#xe005;" data-default-caption="{rating} hearts" data-star-captions="{}" title="" data-show-clear="false" data-show-caption="false" required="">
			  </div>
		  </div>
		  <div class="row">
			<div class="col-lg-2 col-xs-1 col-lg-offset-3 form-group">
			  <label style="padding-top:5px;">&nbsp;Amount</label>
			</div>
			<div class="col-xs-4">
			  <div class="form-group">
				<input type="number" name="final_bill_amount" min="1" id="final_bill_amount<?php echo $booking_id; ?>" class="form-control" style="max-width:280px;" required="">
			  </div>
			</div>
		  </div>
		  </div>

			<div class="row" id="priceDifference<?php echo $booking_id; ?>" style="display:none;">
 		  <div class="row">
			<div class="col-lg-4 col-xs-1 col-lg-offset-1 form-group">
			  <label style="padding-top:5px;">&nbsp;GoBumpr Quoted Price</label>
			</div>
			<div class="col-xs-4">
			  <div class="form-group">
				<input type="number" name="gobumpr_quoted_amt" min="1" id="gobumpr_quoted_amt<?php echo $booking_id; ?>" class="form-control" style="max-width:280px;" >
			  </div>
			</div>
		  </div>
		  <div class="row">
			<div class="col-lg-4 col-xs-1 col-lg-offset-1 form-group">
			  <label style="padding-top:5px;">&nbsp;Garage Quoted Price</label>
			</div>
			<div class="col-xs-4">
			  <div class="form-group">
				<input type="number" name="garage_quoted_amt" min="1" id="garage_quoted_amt<?php echo $booking_id; ?>" class="form-control" style="max-width:280px;" >
			  </div>
			</div>
		  </div>
		  <div class="row">
			<div class="col-lg-4 col-xs-1 col-lg-offset-1 form-group">
			  <label style="padding-top:5px;">&nbsp;Local Tyre Dealer Quote</label>
			</div>
			<div class="col-xs-4">
			  <div class="form-group">
				<input type="number" name="local_garage_quoted_amt" min="1" id="local_garage_quoted_amt<?php echo $booking_id; ?>" class="form-control" style="max-width:280px;" >
			  </div>
			</div>
		  </div>
		  <div class="row">
			<div class="col-lg-4 col-xs-1 col-lg-offset-1 form-group">
			  <label style="padding-top:5px;">&nbsp;Local Tyre Dealer Name</label>
			</div>
			<div class="col-xs-4">
			  <div class="form-group">
				<input type="text" name="local_dealer_name" id="local_dealer_name<?php echo $booking_id; ?>" class="form-control" style="max-width:280px;" >
			  </div>
			</div>
		  </div>
		</div>
		  <div class="row" id="quoteamount<?php echo $booking_id; ?>" style="display:none;">
			<div class="col-xs-3 col-lg-3 col-lg-offset-3 form-group">
			  <label style="padding-top:5px;">Quote Given</label>
			</div>
			<div class="col-xs-3">
			  <div class="form-group">
				<input type="number" name="quote_amount" min="1" id="quote_amount<?php echo $booking_id; ?>" class="form-control" style="max-width:280px;" >
			  </div>
			</div>
		</div>
		  <div class="row" id="comments<?php echo $booking_id; ?>" style="display:none;">
			<div class="col-xs-8 col-lg-offset-2">
				<textarea name="comments" id="comments" class="form-control" rows="4" cols="40" placeholder="Comments..." style="width:270px;resize: vertical;" ></textarea>
			</div>
		  </div>
		  <div class="row">
			<br>
			<div class="form-group" align="center">
			  <input class="form-control" id="submit<?php echo $booking_id; ?>" type="submit" value="Apply" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;width:90px;"/>
			</div>
		  </div>
		  <input type="hidden" name="feedback_status" value="<?php echo $feedback_status; ?>" >
		  <input type="hidden" name="rtt_updates" value="<?php echo $rtt_updates; ?>" >
		  <input type="hidden" name="s_date" value="<?php echo date('Y-m-d',strtotime($startdate)); ?>" >
		  <input type="hidden" name="e_date" value="<?php echo date('Y-m-d',strtotime($enddate)); ?>" >
		  <input type="hidden" name="b2b_book_id" value="<?php echo $b2b_booking_id; ?>" >
		  <input type="hidden" name="shop_id" value="<?php echo $shop_id; ?>" >
		  <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" >
		  <input type="hidden" name="book_id" value="<?php echo $book_id; ?>" >
		  <input type="hidden" name="premium" value="<?php echo $premium; ?>" >
		  <input type="hidden" name="g_date" value="<?php echo date('Y-m-d',strtotime($goaxle_date)); ?>" >
		  <input type="hidden" name="page" value="<?php echo $page; ?>" >
		  <input type="hidden" name="dt" value="<?php echo $dt; ?>" >
		  <input type="hidden" name="service_type" value="<?php echo $service_type; ?>" >
		  <input type="hidden" name="shop_name" value="<?php echo $service_partner; ?>" >
		  <input type="hidden" name="user_name" value="<?php echo $user_name; ?>" >
		  <input type="hidden" id="pickup" name="pickup" value="Will visit the garage">
		</form>
	  </div> <!-- modal body -->
	</div> <!-- modal content -->
  </div>  <!-- modal dailog -->
</div>  <!-- modal -->


<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<noscript id="async-styles">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" href="css/star-rating.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.0.1/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" crossorigin="anonymous">

</noscript>

<script> // The CSS mentioned in "async-styles" will be loaded and styles will be applied.
    var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("async-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
        webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function() {
        window.setTimeout(loadDeferredStyles, 0);
    });
    else window.addEventListener('load', loadDeferredStyles);
</script>

<script async src="js/jquery-3.2.1.min.js" onLoad="loadOtherScripts();"></script>
<script>
    function loadScript(src) {
		return new Promise(function (resolve, reject) {
            var s;
            s = document.createElement('script');
            s.src = src;
            s.onload = resolve;
            s.onerror = reject;
            document.head.appendChild(s);
        });
    }

    function loadOtherScripts() {
		loadScript('js/moment.min.js')
      .then(function() {
      Promise.all([ loadScript('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'),loadScript('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'),loadScript('js/daterangepicker.js'),loadScript('js/star-rating.min.js'),loadScript('js/sidebar.js'),loadScript('https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js'),loadScript('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.0.1/js/bootstrap-switch.js')]).then(function () {
		intialFunction();
      }).catch(function (error) {
    //  console.log('some error!' + error)
      })
      }).catch(function (error) {
    //  console.log('Moment call error!' + error)
      })
      }
</script>

<script>
function intialFunction()
{
	$(document).ready(function(){
		$('#city').hide();
		$("#status<?php echo $booking_id; ?>").change(function(){
			var reason = $("#status<?php echo $booking_id; ?>").val();
			var feedback_status = $("input[name=feedback_status]").val();
			switch(reason)
			{   
				case 'RNR' :	$('#deliverydate<?php echo $booking_id; ?>').hide();
								$('#followupDate<?php echo $booking_id; ?>').hide();
								$('#completed-div<?php echo $booking_id; ?>').hide();
								$('#comments<?php echo $booking_id; ?>').hide();
								$('#reason<?php echo $booking_id; ?>').hide();
								$('#RNRreason<?php echo $booking_id; ?>').show();
								$('#cancelledReason<?php echo $booking_id; ?>').hide();
								$('#subreason<?php echo $booking_id;?>').hide();
								$("#busyheldupreason<?php echo $booking_id;?>").hide();
								$("#reservice<?php echo $booking_id; ?>").hide();
								$("#quoteamount<?php echo $booking_id; ?>").hide();
								$("#servicePriority<?php echo $booking_id; ?>").hide();
								$("#priceDifference<?php echo $booking_id; ?>").hide();
								$(document).ready(function(){
								$('#rating<?php echo $booking_id; ?>').prop('required',false);
								$('#final_bill_amount<?php echo $booking_id; ?>').prop('required',false);
								$('#cancelledReason-select<?php echo $booking_id;?>').prop('required',false);
								$('#reason-select<?php echo $booking_id;?>').prop('required',false);
								});

								
								break;
				
				case 'Purchased' :	$('#deliverydate<?php echo $booking_id; ?>').hide();
									$('#followupDate<?php echo $booking_id; ?>').hide();
									$('#completed-div<?php echo $booking_id; ?>').show();
									$('#comments<?php echo $booking_id; ?>').show();
									$('#reason<?php echo $booking_id; ?>').hide();
									$('#RNRreason<?php echo $booking_id; ?>').hide();
									$('#cancelledReason<?php echo $booking_id; ?>').hide();
									$('#subreason<?php echo $booking_id;?>').hide();
									$("#busyheldupreason<?php echo $booking_id;?>").hide();
									$("#reservice<?php echo $booking_id; ?>").show();
									$("#quoteamount<?php echo $booking_id; ?>").hide();
									$("#servicePriority<?php echo $booking_id; ?>").hide();
									$("#priceDifference<?php echo $booking_id; ?>").hide();
									$(document).ready(function(){
													
									$('#cancelledReason-select<?php echo $booking_id;?>').prop('required',false);
									$('#reason-select<?php echo $booking_id;?>').prop('required',false);
									$('#RNRreason-select<?php echo $booking_id;?>').prop('required',false);
									
									});
									break;
				case 'Yet to Purchase' :	$('#deliverydate<?php echo $booking_id; ?>').hide();
													$('#followupDate<?php echo $booking_id; ?>').hide();
													$('#completed-div<?php echo $booking_id; ?>').hide();
													$('#swappedReason<?php echo $booking_id; ?>').hide();
													$('#cancelledReason<?php echo $booking_id; ?>').hide();
													$('#RNRreason<?php echo $booking_id; ?>').hide();
													$('#comments<?php echo $booking_id; ?>').hide();
													$('#reason<?php echo $booking_id; ?>').show();
													$('#subreason<?php echo $booking_id;?>').hide();
													$("#busyheldupreason<?php echo $booking_id;?>").hide();
													$("#reservice<?php echo $booking_id; ?>").hide();
													$("#quoteamount<?php echo $booking_id; ?>").hide();
													$("#servicePriority<?php echo $booking_id; ?>").hide();
													$("#priceDifference<?php echo $booking_id; ?>").hide();
													$(document).ready(function(){
														$('#rating<?php echo $booking_id; ?>').prop('required',false);
														$('#final_bill_amount<?php echo $booking_id; ?>').prop('required',false);
														$('#cancelledReason-select<?php echo $booking_id;?>').prop('required',false);
														$('#RNRreason-select<?php echo $booking_id;?>').prop('required',false);
														});

													break;
				
				case 'Cancelled' :	$('#deliverydate<?php echo $booking_id; ?>').hide();
									$('#followupDate<?php echo $booking_id; ?>').hide();
									$('#completed-div<?php echo $booking_id; ?>').hide();
									$('#reason<?php echo $booking_id; ?>').hide();
									$('#RNRreason<?php echo $booking_id; ?>').hide();
									$('#comments<?php echo $booking_id; ?>').hide();
									$('#cancelledReason<?php echo $booking_id; ?>').show();
									$('#subreason<?php echo $booking_id;?>').hide();
									$("#busyheldupreason<?php echo $booking_id;?>").hide();
									$("#reservice<?php echo $booking_id; ?>").hide();
									$("#quoteamount<?php echo $booking_id; ?>").hide();
									$("#servicePriority<?php echo $booking_id; ?>").hide();
									$("#priceDifference<?php echo $booking_id; ?>").hide();
									$(document).ready(function(){
									$('#rating<?php echo $booking_id; ?>').prop('required',false);
									$('#final_bill_amount<?php echo $booking_id; ?>').prop('required',false);
									$('#reason-select<?php echo $booking_id;?>').prop('required',false);
									$('#RNRreason-select<?php echo $booking_id;?>').prop('required',false);
									});
									break;

			

				default :	if(feedback_status>10)
							{
								$('#deliverydate<?php echo $booking_id; ?>').hide();
								$('#followupDate<?php echo $booking_id; ?>').hide();
								$('#completed-div<?php echo $booking_id; ?>').hide();
								$('#comments<?php echo $booking_id; ?>').hide();
								$('#reason<?php echo $booking_id; ?>').hide();
								$('#RNRreason<?php echo $booking_id; ?>').show();
								$('#cancelledReason<?php echo $booking_id; ?>').hide();
								$('#subreason<?php echo $booking_id;?>').hide();
								$("#busyheldupreason<?php echo $booking_id;?>").hide();
								$("#reservice<?php echo $booking_id; ?>").hide();
								$("#quoteamount<?php echo $booking_id; ?>").hide();
								$("#servicePriority<?php echo $booking_id; ?>").hide();
								$("#priceDifference<?php echo $booking_id; ?>").hide();
								$(document).ready(function(){
								$('#rating<?php echo $booking_id; ?>').prop('required',false);
								$('#final_bill_amount<?php echo $booking_id; ?>').prop('required',false);
								$('#cancelledReason-select<?php echo $booking_id;?>').prop('required',false);
								});
								$('#reason-select<?php echo $booking_id;?>').prop('required',false);
							}
							else
							{
								$('#deliverydate<?php echo $booking_id; ?>').hide();
								$('#followupDate<?php echo $booking_id; ?>').hide();
								$('#completed-div<?php echo $booking_id; ?>').hide();
								$('#comments<?php echo $booking_id; ?>').hide();
								$('#reason<?php echo $booking_id; ?>').hide();
								$('#RNRreason<?php echo $booking_id; ?>').hide();
								$('#cancelledReason<?php echo $booking_id; ?>').hide();
								$('#subreason<?php echo $booking_id;?>').hide();
								$("#busyheldupreason<?php echo $booking_id;?>").hide();
								$("#reservice<?php echo $booking_id; ?>").hide();
								$("#quoteamount<?php echo $booking_id; ?>").hide();
								$("#servicePriority<?php echo $booking_id; ?>").hide();
								$("#priceDifference<?php echo $booking_id; ?>").hide();
								$(document).ready(function(){
								$('#rating<?php echo $booking_id; ?>').prop('required',false);
								$('#final_bill_amount<?php echo $booking_id; ?>').prop('required',false);
								$('#cancelledReason-select<?php echo $booking_id;?>').prop('required',false);
								$('#reason-select<?php echo $booking_id;?>').prop('required',false);
								});
							}
							break;
			}
		});

		
		// On change Yet to service 
		$("#reason-select<?php echo $booking_id; ?>").change(function(){
			var reason2 =$(this).val();
		 if(reason2 === "Improper response from the garage")
			{	
				$('#subreasons<?php echo $booking_id;?>').empty();
				$('#subreasons<?php echo $booking_id;?>').append("<option value='' selected>-Choose a reason-</option><option value='No Call received'>No Call received</option><option value='Delay in Pickup'>Delay in Pickup</option>");
				$('#subreason<?php echo $booking_id;?>').show();
				$('#followupDate<?php echo $booking_id; ?>').hide();
				$('#comments<?php echo $booking_id; ?>').hide();
				$('#busyheldupreason<?php echo $booking_id;?>').hide();
				$("#servicePriority<?php echo $booking_id; ?>").hide();
			}
		else if(reason2 === "Customer will visit dealer")
		{
				$('#subreason<?php echo $booking_id;?>').hide();
				$('#followupDate<?php echo $booking_id; ?>').show();
				$('#comments<?php echo $booking_id; ?>').hide();
				$("#servicePriority<?php echo $booking_id; ?>").show();
		}
		else if(reason2 === "Price mismatch")
		{
				$('#subreason<?php echo $booking_id;?>').hide();
				$('#followupDate<?php echo $booking_id; ?>').show();
				$('#comments<?php echo $booking_id; ?>').hide();
				$("#servicePriority<?php echo $booking_id; ?>").show();
		}
		});
		// cancelledReason
		$("#cancelledReason-select<?php echo $booking_id; ?>").change(function(){
			var reason =$(this).val();
			if(reason === "Improper response from Garage")
			{
				$('#subreasons<?php echo $booking_id;?>').empty();
				$('#subreasons<?php echo $booking_id;?>').append("<option value='' selected>-Choose a reason-</option><option value='Delay in Call'>Delay in Call</option><option value='Delay in Pickup'>Delay in Pickup</option><option value='Misunderstanding'>Misunderstanding</option>");
				$("#cancelledReason<?php echo $booking_id; ?>").after($('#subreason<?php echo $booking_id;?>'));
				$('#subreason<?php echo $booking_id;?>').show();
				$('#comments<?php echo $booking_id; ?>').hide();
				$("#priceDifference<?php echo $booking_id; ?>").hide(); 
				$("#servicePriority<?php echo $booking_id; ?>").hide();
				$('#followupDate<?php echo $booking_id; ?>').hide();
			}
			
			else if(reason === "Location too far")
			{	
				$('#subreason<?php echo $booking_id;?>').hide();
				$('#comments<?php echo $booking_id; ?>').show();
				$("#priceDifference<?php echo $booking_id; ?>").hide();
				$("#servicePriority<?php echo $booking_id; ?>").hide();
				$('#followupDate<?php echo $booking_id; ?>').hide();
			}
			else if(reason === "Poor quality"){
				$('#comments<?php echo $booking_id; ?>').show();
				$('#subreason<?php echo $booking_id;?>').hide();
				$("#priceDifference<?php echo $booking_id; ?>").hide();
				$("#servicePriority<?php echo $booking_id; ?>").hide();
				$('#followupDate<?php echo $booking_id; ?>').hide();
			}
			else if(reason === "Price too high"){
				$("#priceDifference<?php echo $booking_id; ?>").show();
				$('#subreason<?php echo $booking_id;?>').hide();
				$('#comments<?php echo $booking_id; ?>').show();
				$("#servicePriority<?php echo $booking_id; ?>").hide();
				$('#followupDate<?php echo $booking_id; ?>').hide();
			}
			else if(reason === "Enquiry"){
				$('#subreasons<?php echo $booking_id;?>').empty();
				$('#subreasons<?php echo $booking_id;?>').append("<option value='' selected>-Choose a reason-</option><option value='Just Enquiry'>Just Enquiry</option><option value='No Intent'>No Intent</option>");
				$('#subreason<?php echo $booking_id;?>').show();
				$("#priceDifference<?php echo $booking_id; ?>").hide();
				$('#comments<?php echo $booking_id; ?>').hide();
				$("#cancelledReason<?php echo $booking_id; ?>").after($('#subreason<?php echo $booking_id;?>'));
				$("#servicePriority<?php echo $booking_id; ?>").hide();
				$('#followupDate<?php echo $booking_id; ?>').hide();
			}
			else if(reason === "Others"){
				$('#comments<?php echo $booking_id; ?>').show();
				$('#subreason<?php echo $booking_id;?>').hide();
				$("#priceDifference<?php echo $booking_id; ?>").hide();
				$("#servicePriority<?php echo $booking_id; ?>").hide();
				$('#followupDate<?php echo $booking_id; ?>').hide();
			}
		});
		// Why exactly or Subreasons
		$("#subreasons<?php echo $booking_id;?>").change(function(){
			var cancelReason=$("#cancelledReason-select<?php echo $booking_id; ?>").val();
			
			var reason2 = $("#reason-select<?php echo $booking_id; ?>").val();
			var subreason =$(this).val();
			 if( reason2 === "Improper response from the garage"){
				$('#followupDate<?php echo $booking_id; ?>').show();
				$('#followup_date_txt<?php echo $booking_id; ?>').text('FollowUp Date');
				$("#busyheldupreason<?php echo $booking_id;?>").hide();
				$("#servicePriority<?php echo $booking_id; ?>").show();
			}else if(cancelReason==='Improper response from Garage' || cancelReason==='Enquiry'){
					$('#comments<?php echo $booking_id; ?>').show();
			}
		});
		
	});
	
	
	$('#feedbackModal').on('shown.bs.modal', function (e) {
		var reason = "<?php echo $fdbck_status;?>";
		var reserviceflag = "<?php echo $reservice_flag;?>";
		var feedback_status = $("input[name=feedback_status]").val();
		switch(reason)
		{
			case 'RNR' :	$('#deliverydate<?php echo $booking_id; ?>').hide();
								$('#followupDate<?php echo $booking_id; ?>').hide();
								$('#completed-div<?php echo $booking_id; ?>').hide();
								$('#comments<?php echo $booking_id; ?>').hide();
								$('#reason<?php echo $booking_id; ?>').hide();
								$('#RNRreason<?php echo $booking_id; ?>').show();
								$('#cancelledReason<?php echo $booking_id; ?>').hide();
								$('#subreason<?php echo $booking_id;?>').hide();
								$("#busyheldupreason<?php echo $booking_id;?>").hide();
								$("#quoteamount<?php echo $booking_id; ?>").hide();
								$("#servicePriority<?php echo $booking_id; ?>").hide();
								$("#priceDifference<?php echo $booking_id; ?>").hide();
								break;
				
				case 'Purchased' :	if(reserviceflag=='0'){
										$('#deliverydate<?php echo $booking_id; ?>').hide();
										$('#followupDate<?php echo $booking_id; ?>').hide();
										$('#completed-div<?php echo $booking_id; ?>').show();
										$('#comments<?php echo $booking_id; ?>').show();
										$('#reason<?php echo $booking_id; ?>').hide();
										$('#RNRreason<?php echo $booking_id; ?>').hide();
										$('#cancelledReason<?php echo $booking_id; ?>').hide();
										$('#subreason<?php echo $booking_id;?>').hide();
										$("#busyheldupreason<?php echo $booking_id;?>").hide();
										$("#quoteamount<?php echo $booking_id; ?>").hide();
										$("#servicePriority<?php echo $booking_id; ?>").hide();
										$("#priceDifference<?php echo $booking_id; ?>").hide();
										
									}else{
										$('#deliverydate<?php echo $booking_id; ?>').hide();
										$('#followupDate<?php echo $booking_id; ?>').hide();
										$('#completed-div<?php echo $booking_id; ?>').hide();
										$('#comments<?php echo $booking_id; ?>').hide();
										$('#reason<?php echo $booking_id; ?>').hide();
										$('#RNRreason<?php echo $booking_id; ?>').hide();
										$('#cancelledReason<?php echo $booking_id; ?>').hide();
										$('#subreason<?php echo $booking_id;?>').hide();
										$("#busyheldupreason<?php echo $booking_id;?>").hide();
										$("#quoteamount<?php echo $booking_id; ?>").hide();
										$("#servicePriority<?php echo $booking_id; ?>").hide();
										$("#priceDifference<?php echo $booking_id; ?>").hide();
									}
									break;
				case 'Yet to Purchase' :	$('#deliverydate<?php echo $booking_id; ?>').hide();
													$('#followupDate<?php echo $booking_id; ?>').hide();
													$('#completed-div<?php echo $booking_id; ?>').hide();
													$('#cancelledReason<?php echo $booking_id; ?>').hide();
													$('#RNRreason<?php echo $booking_id; ?>').hide();
													$('#comments<?php echo $booking_id; ?>').hide();
													$('#reason<?php echo $booking_id; ?>').show();
													$('#subreason<?php echo $booking_id;?>').hide();
													$("#busyheldupreason<?php echo $booking_id;?>").hide();
													$("#reservice<?php echo $booking_id; ?>").hide();
													$("#quoteamount<?php echo $booking_id; ?>").hide();
													$("#servicePriority<?php echo $booking_id; ?>").hide();
													$("#priceDifference<?php echo $booking_id; ?>").hide();
													break;
				
				case 'Cancelled' :	$('#deliverydate<?php echo $booking_id; ?>').hide();
									$('#followupDate<?php echo $booking_id; ?>').hide();
									$('#completed-div<?php echo $booking_id; ?>').hide();
									$('#reason<?php echo $booking_id; ?>').hide();
									$('#RNRreason<?php echo $booking_id; ?>').hide();
									$('#comments<?php echo $booking_id; ?>').show();
									$('#cancelledReason<?php echo $booking_id; ?>').show();
									$('#subreason<?php echo $booking_id;?>').hide();
									$("#busyheldupreason<?php echo $booking_id;?>").hide();
									$("#quoteamount<?php echo $booking_id; ?>").hide();
									$("#servicePriority<?php echo $booking_id; ?>").hide();
									$("#priceDifference<?php echo $booking_id; ?>").hide();
									break;						
				default :	if(feedback_status > 10)
							{
								$('#deliverydate<?php echo $booking_id; ?>').hide();
								$('#followupDate<?php echo $booking_id; ?>').hide();
								$('#completed-div<?php echo $booking_id; ?>').hide();
								$('#comments<?php echo $booking_id; ?>').hide();
								$('#reason<?php echo $booking_id; ?>').hide();
								$('#RNRreason<?php echo $booking_id; ?>').show();
								$('#cancelledReason<?php echo $booking_id; ?>').hide();
								$('#subreason<?php echo $booking_id;?>').hide();
								$("#busyheldupreason<?php echo $booking_id;?>").hide();
								$("#reservice<?php echo $booking_id; ?>").hide();
								$("#quoteamount<?php echo $booking_id; ?>").hide();
								$("#priceDifference<?php echo $booking_id; ?>").hide();
							}
							else
							{
								$('#deliverydate<?php echo $booking_id; ?>').hide();
								$('#followupDate<?php echo $booking_id; ?>').hide();
								$('#completed-div<?php echo $booking_id; ?>').hide();
								$('#comments<?php echo $booking_id; ?>').hide();
								$('#reason<?php echo $booking_id; ?>').hide();
								$('#RNRreason<?php echo $booking_id; ?>').hide();
								$('#cancelledReason<?php echo $booking_id; ?>').hide();
								$('#subreason<?php echo $booking_id;?>').hide();
								$("#busyheldupreason<?php echo $booking_id;?>").hide();
								$("#quoteamount<?php echo $booking_id; ?>").hide();
								$("#servicePriority<?php echo $booking_id; ?>").hide();
								$("#priceDifference<?php echo $booking_id; ?>").hide();
							}
							break;		
		}
	});

	<?php if(($source == 'GoBumpr App') || (!empty($row_userbooking_userid))){?>		
			$('#google_link').css({"display": "none"});
			$('#playstore_link').css({"width": "auto"});
			$('.row').css({"text-align":"-webkit-center"});
	<?php } ?>
	
	$(function () {
		$("#rating<?php echo $booking_id; ?>").rating();
	});
	
	var date = new Date();
	date.setDate(date.getDate());

	$('.datepicker').datepicker({
		autoclose: true,
		startDate: date
	});
	$('input.datepicker').datepicker('setDate', 'today');
	
	$(document).on('submit',"#feedback<?php echo $booking_id; ?>",function(e){
	if($( '#submit<?php echo $booking_id; ?>' ).is(":visible")){
		e.preventDefault();
		$("#submit<?php echo $booking_id; ?>").hide();
		setTimeout(function () {$("#feedback<?php echo $booking_id; ?>").trigger("submit"); }, 100);
	}
	});

	$('#RTTUpadate').on('click',function(){
		var shopid = '<?php echo $shop_id; ?>';
		var b2b_booking_id = '<?php echo $b2b_booking_id; ?>';
		var inspection_stage='<?php echo $inspection_stage; ?>';
		var estimate_stage='<?php echo $feedback_status; ?>';
		$.ajax({
			url : "ajax/send_Rttupdate.php",  // create a new php page to handle ajax request
			type : "POST",
			data : {'shopid':shopid,'b2b_booking_id':b2b_booking_id,'inspection_stage':inspection_stage,'estimate_stage':estimate_stage},
			success : function(data) {
				$('#RTTUpadate').attr('disabled',true);			
			},
			error: function(xhr, ajaxOptions, thrownError) {
				  //  alert(xhr.status + " "+ thrownError);
			}
		});
	});
	$('#garage_sms').on('click',function(){
		var cust_phone = '<?php echo $mobile; ?>';
		var booking_id = '<?php echo $booking_id; ?>';
		var shop_name = '<?php echo $service_partner;?>';
		$.ajax({
			url : "ajax/send_garage_sms.php",  // create a new php page to handle ajax request
			type : "POST",
			data : {'cust_phone':cust_phone,'booking_id':booking_id,'shop_name':shop_name},
			success : function(data) {
				$('#garage_sms').attr('disabled',true);			
			},
			error: function(xhr, ajaxOptions, thrownError) {
				  //  alert(xhr.status + " "+ thrownError);
			}
		});
	});
	$('#playstore_link').on('click',function(){
		var cust_phone = '<?php echo $mobile; ?>';
		var user_name = '<?php echo $user_name; ?>';
		$.ajax({
			url : "ajax/playstore_link_sms.php",  // create a new php page to handle ajax request
			type : "POST",
			data : {'cust_phone':cust_phone,'user_name':user_name,'type':'playstore_link'},
			success : function(data) {
				$('#playstore_link').attr('disabled',true);			
			},
			error: function(xhr, ajaxOptions, thrownError) {
				  //  alert(xhr.status + " "+ thrownError);
			}
		});
	});
	$('#google_link').on('click',function(){
		var cust_phone = '<?php echo $mobile; ?>';
		var user_name = '<?php echo $user_name; ?>';
		$.ajax({
			url : "ajax/playstore_link_sms.php",  // create a new php page to handle ajax request
			type : "POST",
			data : {'cust_phone':cust_phone,'user_name':user_name,'type':'google_link'},
			success : function(data) {
				$('#google_link').attr('disabled',true);			
			},
			error: function(xhr, ajaxOptions, thrownError) {
				  //  alert(xhr.status + " "+ thrownError);
			}
		});
	});
	$('.alert-status').bootstrapSwitch();

	$('#pickup_switch').on('switchChange.bootstrapSwitch', function (event, state) {
		var s;
		if(state==true)
		s='Needs pick up';
		else
		s = 'Will visit the garage';
		var to=$("#pickup").val(s);
	});	
}
</script>

</body>
</html>
<?php
include("../config.php");
error_reporting(E_ALL); 
ini_set('display_errors', 1);

$conn = db_connect1();
session_start();

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$service_type = $_POST['service_type'];
$vehicle = $_POST['vehicle'];
$booking_status = $_POST['status'];

class overall {
    private $vehicle;
    private $service_type;
    private $booking_status;

    function __construct( $vehicle, $service_type,$booking_status){
        $this->vehicle = $vehicle;
        $this->service_type = $service_type;
        $this->status = $booking_status;
    }

    function v0_s0_st0() {
      return "";
    }
    function v0_s0_st1() {
      $ss = $this->status;
      if($ss == 'c'){
        return "AND flag='1'";
      }
      else{
        return "AND booking_status='$ss' AND flag!='1'";
      }
    }
    function v0_s1_st0() {
        return "AND service_type='$this->service_type'";
    }
    function v0_s1_st1() {
        $ss = $this->status;
        if($ss == 'c'){
          return "AND service_type='$this->service_type' AND flag='1'";
        }
        else{
          return "AND service_type='$this->service_type' AND booking_status='$ss' AND flag!='1' ";
        }    
    }
    function v1_s0_st0() {
        return "AND vehicle_type='$this->vehicle' ";
    }
    function v1_s0_st1() {
        $ss = $this->status;
        if($ss == 'c'){
          return "AND vehicle_type='$this->vehicle' AND flag='1'";
        }
        else{
          return "AND vehicle_type='$this->vehicle' AND booking_status='$ss' AND flag!='1'";
        }    
    }
    function v1_s1_st0() {
        return "AND vehicle_type='$this->vehicle' AND service_type='$this->service_type'";
    }
    function v1_s1_st1() {
        $ss = $this->status;
        if($ss == 'c'){
          return "AND vehicle_type='$this->vehicle' AND service_type='$this->service_type' AND flag='1'";
        }
        else{
          return "AND vehicle_type='$this->vehicle' AND service_type='$this->service_type' AND booking_status='$ss' AND flag!='1'";
        }    
    }
}

$vehicle_fun = $vehicle=='all' ? "" : $vehicle;
$service_type_fun = $service_type=='all' ? "" : $service_type;
$booking_status_fun = $booking_status=='all'  ? "" : $booking_status;


$overall_obj = new overall( $vehicle_fun, $service_type_fun, $booking_status_fun);


$vehicle_val = $vehicle=='all' ? "0" : "1";
$service_type_val = $service_type=='all' ? "0" : "1";
$booking_status_val = $booking_status=='all' ? "0" : "1";


$cond = $overall_obj->{"v{$vehicle_val}_s{$service_type_val}_st{$booking_status_val}"}();

$sql_booking = "SELECT booking_id,user_id,mec_id,locality,flag_unwntd,case flag_fo when '1' then crm_update_time when '0' then log end as log_calc FROM user_booking_tb WHERE booking_id!='' {$cond} HAVING DATE(log_calc) BETWEEN '$startdate' AND '$enddate' ";

$res_booking = mysqli_query($conn,$sql_booking);

$no = 0;

$count = mysqli_num_rows($res_booking);
if($count >=1){

while($row_booking = mysqli_fetch_object($res_booking)){
$booking_id = $row_booking->booking_id;
$user_id = $row_booking->user_id;
$locality = $row_booking->locality;

if($locality == ""){
    $sql_user_loc = "SELECT Locality_Home FROM user_register WHERE reg_id='$user_id'";
    $res_user_loc = mysqli_query($conn,$sql_user_loc);
    $row_user_loc = mysqli_fetch_array($res_user_loc);
    $locality_u = $row_user_loc['Locality_Home'];
    if($locality_u == ''){
        $locality_u = "Chennai";
    }
    $locality = $locality_u;
}

if($locality == "chennai" || $locality == "Chennai"){
    $locality="Central";
}

$sql_latlng = "SELECT lat,lng FROM localities WHERE localities='$locality'";
$res_latlng = mysqli_query($conn,$sql_latlng);
$row_latlng = mysqli_fetch_array($res_latlng);
$lat = $row_latlng['lat'];
$lng = $row_latlng['lng'];
$localities[] = array("latt"=>$lat,"lngg"=>$lng);
//print_r($localities);
}
?>
<?php
//foreach in array create a string like below
$data='';
    foreach ($localities as $row) {
         $lat= floatval(($row['latt']));
         $lng= floatval(($row['lngg']));
        
        if($data == ""){
            $data = '{lat: '.$lat.',lng: '.$lng.'}';
        }
        else{
            $data = $data.',{lat: '.$lat.',lng: '.$lng.'}'; 
        }
    }
//print_r($data);

?>

 <div id="map" style="width: 800px; height: 500px;">
</div>
<script>
    function initMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 3,
          center: {lat: 13.080818, lng: 80.246037}
        });

        // Create an array of alphabetical characters used to label the markers.
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // Add some markers to the map.
        // Note: The code uses the JavaScript Array.prototype.map() method to
        // create an array of markers based on a given "locations" array.
        // The map() method here has nothing to do with the Google Maps API.
        var markers = locations.map(function(location, i) {
          return new google.maps.Marker({
            position: location,
            label: labels[i % labels.length]
          });
        });

        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
      }
      var locations = <?php echo "[".$data."]"; ?>;
     console.log(locations);
</script>
<?php
} // if

else {
  //echo $sql_booking;
  echo "<h3> No Results Found!</h3>";
}
 ?>



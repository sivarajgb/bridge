<?php
include("config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();
$today=date('Y-m-d H:i:s');

$b2b_booking_id = $_REQUEST['b2b_book_id'];
$booking_id = $_REQUEST['book_id'];
$date = $_REQUEST['logdate'];
$type = $_REQUEST['vehtype'];

$enc_date = base64_encode($date);
$enc_veh = base64_encode($type);

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$status=mysqli_real_escape_string($conn1,$_POST['status']);
$final_bill_amount=mysqli_real_escape_string($conn1,$_POST['final_bill_amount']);
$rating=mysqli_real_escape_string($conn1,$_POST['rating']);
$realtime_updates=mysqli_real_escape_string($conn1,$_POST['get_rltime_updte']);
$next_service_date=mysqli_real_escape_string($conn1,$_POST['next_service_date']);

//update gobumpr booking table
$sql_gb = "UPDATE user_booking_tb SET service_status='$status',final_bill_amt='$final_bill_amount',rating='$rating',get_rltime_updte='$realtime_updates',followup_date='$next_service_date',crm_feedback_id='$crm_log_id' WHERE booking_id='$booking_id'";
$res_gb = mysqli_query($conn1,$sql_gb) or die(mysqli_error($conn1));

if($rating !='' && $rating > 0){
//update b2b booking table
$sql_b2b = "UPDATE b2b_booking_tbl SET b2b_Rating='$rating' WHERE b2b_booking_id='$b2b_booking_id'";
$res_b2b = mysqli_query($conn2,$sql_b2b) or die(mysqli_error($conn2));

//get details from gobumpr
$sql_book_gb = "SELECT user_id,vech_id,mec_id,service_type,source FROM user_booking_tb WHERE booking_id='$booking_id'";
$res_book_gb = mysqli_query($conn1,$sql_book_gb) or die(mysqli_error($conn1));
$row_book_gb = mysqli_fetch_object($res_book_gb);

$user_id = $row_book_gb->user_id;
$vech_id = $row_book_gb->vech_id;
$mec_id = $row_book_gb->mec_id;
$service_type = $row_book_gb->service_type;
$source = $row_book_gb->source;

//get details from b2b
$sql_book_b2b = "SELECT b2b_shop_id,b2b_customer_name,b2b_cust_phone,brand,model FROM b2b_booking_tbl WHERE b2b_booking_id='$b2b_booking_id'";
$res_book_b2b = mysqli_query($conn2,$sql_book_b2b) or die(mysqli_error($conn2));
$row_book_b2b = mysqli_fetch_object($res_book_b2b);

$b2b_shop_id = $row_book_b2b->b2b_shop_id;
$user_name = $row_book_b2b->b2b_customer_name;
$user_mobile = $row_book_b2b->b2b_cust_phone;
$brand = $row_book_b2b->brand;
$model = $row_book_b2b->model;

//insert or update user rating table in gobumpr
$sql_check_gb = "SELECT rating FROM user_rating_tbl WHERE axle_booking_id='$b2b_booking_id'";
$res_check_gb = mysqli_query($conn1,$sql_check_gb) or die(mysqli_error($conn1));
$num_rows = mysqli_num_rows($res_check_gb);
if($num_rows <= 0){
    $sql_rat_gb = "INSERT INTO user_rating_tbl (booking_id,mec_id,service_type,rating,log,source,axle_booking_id,name,brand,model,vech_id,user_id) VALUES('$booking_id','$mec_id','$service_type','$rating','$today','$source','$b2b_booking_id','$user_name','$brand','$model','$vech_id','$user_id') ";
} 
else{
    $sql_rat_gb = "UPDATE user_rating_tbl SET rating='$rating' AND log='$today' WHERE axle_booking_id='$b2b_booking_id' ";
}
//$sql_rat_gb;
$res_rat_gb = mysqli_query($conn1,$sql_rat_gb) or die(mysqli_error($conn1));

//update overall rating in gobumpr

$sql_o_rat_gb = "SELECT avg(rating) as rating FROM user_rating_tbl WHERE mec_id='$mec_id' AND status!='1' AND rating!='0'";
$res_o_rat_gb = mysqli_query($conn1,$sql_o_rat_gb) or die(mysqli_error($conn1));

$row_o_rat = mysqli_fetch_object($res_o_rat_gb);
$overall_rating_gb = $row_o_rat->rating;
$overall_rating_gb = round($overall_rating_gb,2);
$sql_mec_overall = "UPDATE admin_mechanic_table SET ratting='$overall_rating_gb' WHERE mec_id='$mec_id' ";
$res_mec_overall = mysqli_query($conn1,$sql_mec_overall) or die(mysqli_error($conn1));

//update overall rating in b2b
$sql_o_rat_b2b = "SELECT avg(b2b_Rating) as b2b_rating FROM b2b_booking_tbl WHERE b2b_shop_id='$b2b_shop_id' AND b2b_flag!='1' AND b2b_Rating!='0'";
$res_o_rat_b2b = mysqli_query($conn2,$sql_o_rat_b2b) or die(mysqli_error($conn2));
$row_o_rat_b2b = mysqli_fetch_object($res_o_rat_b2b);
$overall_rating_b2b = $row_o_rat_b2b->b2b_rating;
$overall_rating_b2b = round($overall_rating_b2b,2);


$sql_mec_overall = "UPDATE b2b_mec_tbl SET b2b_overall_rating='$overall_rating_b2b' WHERE b2b_shop_id='$b2b_shop_id' ";
$res_mec_overall = mysqli_query($conn2,$sql_mec_overall) or die(mysqli_error($conn2));

}

header('location:daily_reports_detailed.php?dt='.$enc_date.'&vt='.$enc_veh);

?>
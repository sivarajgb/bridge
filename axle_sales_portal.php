<?php
include("sidebar.php");
$conn = db_connect3();
// login or not
if((empty($_SESSION['crm_log_id'])) || $flag!="1") {
	
	header('location:logout.php');
	die();
}
?>

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>GoBumpr Bridge</title>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

  <!-- table sorter -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js"></script>

<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>

<style>
/*home page blocks */
body{
  font-size:12px !important;
}

.floating-box {
 display: inline-block;
 margin: 2px;
 padding-top: 12px;
 padding-left: 12px;
 width:300px;
}
.navbar-fixed-top{
  z-index:100 !important;
}
.upper-div{
  z-index:999999 !important;
}
#range > span:hover{cursor: pointer;}
 /* table */
#tbody{
   font-size:14px !important;
  border:1.5px solid #c4b8b8 !important;
  
}
thead:hover{
	cursor:pointer;
}

.results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px;
  color:#9E9E9E;
}
#tbody, tbody tr {
    -webkit-animation: opacity 5s ease-in-out;
    animation: opacity 5s ease-in-out;
}

</style>

</head>
<body id="body">
<?php include_once("header.php"); ?>
<script>
	$(document).ready(function(){
		$('#city').hide();
	})
</script>
<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>

<!-- search bar -->
<div  class="floating-box1 form-group col-lg-3 col-sm-3" style="cursor: pointer; margin-top:28px;right:120px;float:right;">
    
    <input type="text" class="search form-control" id="searchbar" name="searchbar" placeholder="Search" style="width:180px;float:right;">
    <span class="counter" style="margin-left:10px;padding:5px;float:right;"></span>
</div>

<div id="show" style="margin-top:102px;display:none;" align="center">

</div>  
<!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>


<!-- jQuery library -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script>
 function defaultview(){
	//Make AJAX request, using the selected value as the POST

	$.ajax({
	    url : "ajax/axle_sales_portal_view.php",  // create a new php page to handle ajax request
	    type : "POST",
	    success : function(data) {
			//alert(data);
		    //console.log(data);
            $('#show').show();
			$("#loading").hide();
			$('#show').html(data);
			//console.log(date);
            counterval();
			modalData();
			trash();
       },
	   error: function(xhr, ajaxOptions, thrownError) {
	        // alert(xhr.status + " "+ thrownError);
	    }
	});
}
</script>
<script>
function counterval(){
	var jobCount = $("#tbody tr").length;;
	 $('.counter').text(jobCount + ' shops');
}
</script>

<script>
$(document).ready( function (){
   $('#show').hide();
  $("#loading").show();
  defaultview();
});
</script>

            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Axle Request</h4>
        </div>
    <div id="loading1" style="display:none; margin-top:80px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>
      </div>
      
    </div>
  </div>

  
  <!-- Modal -->
            <div class="modal fade" id="myModal_" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Axle Request</h4>
        </div>
		<div class="modal-body" id="insert">
			<form class='form-inline'>
			<input type='hidden' class='form-control' id='b2b_id' name='b2b_id'>
			<div class='form-group'><label for='lead_state'>Lead State</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<select id='lead_state' class='form-control' name='lead_status'>
			</select></div><br><div class='form-group'><label for='comment'>Comment:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<textarea class='form-control' col='4' row='50' name='admin_comments' id='admin_comments' ></textarea>
			</div><br><div class='form-group'><label for='demo_state'>Demo done?</label>&nbsp;&nbsp;
			<select id='demo_state' class='form-control' name='demo_state'>
			</select></div><br><div class='form-group'><label for='trail_state'>Trail Status?</label>&nbsp;&nbsp;
			<select id='trail_state' class='form-control' name='trail_state'>
			</select></div><br><div class='form-group'><label for='followup_date'>Follow up Date</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<div class='input-group date'>
			<input type='text' id='followup_date' name='followup_date' class='form-control' value=''>
			</div></div><center><input type="button" class="btn" id="update" value='Update' style="background-color:rgba(255, 188, 0, 0.66);"/></center></form>
        </div>
      </div>
      
    </div>
  </div>
  
    <!-- Modal -->
  <div class="modal fade" id="trashModal" role="dialog">
    <div class="modal-dialog" style="width: 310px;margin-top: 200px;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
          <p>Do you want to move it to trash ?</p>
		  <input type='hidden' class='form-control' id='trash_b2b_id' name='trash_b2b_id'>
        </div>
        <div class="modal-footer">
            <p><i class="fa fa-check-circle" aria-hidden="true" style="float: left;font-size: 44px;color: green!important;margin-left: 99px;"></i>
            <i data-dismiss="modal" class="fa fa-times-circle" aria-hidden="true" style="float: right;font-size: 44px;color: red!important;margin-right: 99px;"></i></p>

        </div>
      </div>
      
    </div>
  </div>
  
<script type="text/javascript">

$('#update').on('click',function(event){
	var b2b_id = $('#b2b_id').val();
	var lead_state = $('#lead_state').val();
	var admin_comments = $('textarea#admin_comments').val();;
	var demo_state = $('#demo_state').val();
	var trail_state = $('#trail_state').val();
	var followup_date = $('#followup_date').val();
	console.log(b2b_id);
	console.log(lead_state);
	console.log(admin_comments);
	console.log(demo_state);
	console.log(trail_state);
	console.log(followup_date);
	$.ajax({
        url : "ajax/update_axle_sales_status.php",  // create a new php page to handle ajax request
        type : "POST",
        data : {"b2b_id":b2b_id,"lead_state":lead_state,"admin_comments":admin_comments,"demo_state":demo_state,"trail_state":trail_state,"followup_date":followup_date},
        success : function(data) {
			console.log(data);
			window.location.replace('axle_sales_portal.php');
		},
      error: function (xhr, ajaxOptions, thrownError) {
       alert(xhr.status + " "+ thrownError);
     }
   });
});

$('#followup_date').datepicker({
    format: "dd-mm-yyyy",
    todayBtn: "linked",
    todayHighlight: true,
	autoclose: true
});
var modal;
function modalData(){
	$(".modal_data").click(function(){
	window.modal = $(this);
	$("#myModal_").modal();
});
}

var trashModal;
function trash(){
	$(".fa-trash").click(function(){
	window.trashModal = $(this);
	$("#trashModal").modal();
});
}
$("#trashModal").on('show.bs.modal',function(event){
	var _trashModal = $(this);
	var t_b2b_id = trashModal.data("b2bid");
	_trashModal.find('#trash_b2b_id').attr('value',t_b2b_id);
	$(".fa-check-circle").click(function(){
		var b2b_id = $('#trash_b2b_id').val();
		$.ajax({
        url : "ajax/remove_axle_sales_lead.php",  // create a new php page to handle ajax request
        type : "POST",
        data : {'b2b_id':b2b_id},
        success : function(data) {
			console.log(data);
			window.location.replace('axle_sales_portal.php');
		},
		error: function (xhr, ajaxOptions, thrownError) {
       alert(xhr.status + " "+ thrownError);
     }
   });

	});
});

$("#myModal_").on('show.bs.modal', function(event){

var _modal = $(this);
var b2b_id=modal.data('b2bid');
var followup_date = modal.data('followup_date');
if(followup_date == "0000-00-00 00:00:00")
{
	followup_date = new Date();
	var xdate = followup_date.getDate();
	var xmonth = followup_date.getMonth()+1;
	var xyear = followup_date.getFullYear();
	followup_date = ""+xdate+"-"+xmonth+"-"+xyear;
	//$('#followup_date').datepicker('setDate', followup_date);
}
else
{	
	followup_date = new Date(followup_date);
	var xdate = followup_date.getDate();
	var xmonth = followup_date.getMonth()+1;
	var xyear = followup_date.getFullYear();
	followup_date = ""+xdate+"-"+xmonth+"-"+xyear;
	//$('#followup_date').datepicker('setDate', followup_date);
}
var lead_state = modal.data('status');
var admin_comments = modal.data('comments');
var sel = "";
switch(lead_state)
{
	case 'New Lead':
	sel += "<option value='new' selected>New Lead</option><option value='hot' >Hot Lead</option><option value='warm' >Warm Lead</option><option value='cold' >Cold Lead</option>";
	break;
	case 'Hot Lead':
	sel += "<option value='new' >New Lead</option><option value='hot' selected>Hot Lead</option><option value='warm' >Warm Lead</option><option value='cold' >Cold Lead</option>";
	break;
	case 'Warm Lead':
	sel += "<option value='new' >New Lead</option><option value='hot' >Hot Lead</option><option value='warm' selected>Warm Lead</option><option value='cold' >Cold Lead</option>";
	break;
	case 'Cold Lead':
	sel += "<option value='new' >New Lead</option><option value='hot' >Hot Lead</option><option value='warm' >Warm Lead</option><option value='cold' selected>Cold Lead</option>";
	break;
	default: 
	sel += "<option value='new' selected>New Lead</option><option value='hot' >Hot Lead</option><option value='warm' >Warm Lead</option><option value='cold' >Cold Lead</option>";
	break;
}
$('#lead_state').empty().append(sel);
var sel = "<option value=''>-Select-</option>";
var demo_status = modal.data('demo');
switch(demo_status)
{
	case 1:
	sel += "<option value='yes' selected>Yes</option><option value='no' >No</option>"
	break;
	case 0:
	sel += "<option value='yes'>Yes</option><option value='no' selected>No</option>";
}
$('#demo_state').empty().append(sel);
var sel = "<option value=''>-Select-</option>";
var trail_status = modal.data('trail');
switch(trail_status)
{
	case 1:
	sel += "<option value='live' selected>Live</option><option value='expired' >Expired</option>"
	break;
	case 0:
	sel += "<option value='live' >Live</option><option value='expired' selected>Expired</option>";
}
$('#trail_state').empty().append(sel);
	

_modal.find('#b2b_id').attr('value',b2b_id);
_modal.find('#admin_comments').text(admin_comments);
_modal.find('#followup_date').attr('value',followup_date);
//$('#followup_date').datepicker('setDate', followup_date);
});

</script>

</body>
</html>

<?php
include("sidebar.php");
$conn = db_connect3();
$conn1 = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id']))) {
	
	header('location:logout.php');
	die();
}
?>

<!DOCTYPE html>
<html>
<head>
	<!-- jQuery library -->
	
<meta charset="utf-8">
  <title>GoBumpr Bridge</title>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-1.10.2.min.js" integrity="sha256-C6CB9UYIS9UJeqinPHWTHVqh/E1uhG5Twh+Y5qFQmYg=" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<!-- table sorter-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.28.9/css/theme.grey.min.css" />
<!-- masking -->
<script type="text/javascript" src="js/inputmask.js"></script>
<script type="text/javascript" src="js/jquery.inputmask.js"></script>
  <script type="text/javascript" src="js/inputmask.extensions.js"></script>
  
  
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
  <script type="text/javascript" src="js/gauge.min.js"></script>

  <!-- auto complete -->
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/jquery-ui.structure.min.css">
<link rel="stylesheet" href="css/jquery-ui.theme.min.css">

<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script type="text/javascript" src="js/jquery.knob.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>
  <style>
   /* axle page cursor */
#range > span:hover{cursor: pointer;}
 #sl{
	cursor:pointer;
}

.floating-box {
	display: inline-block;
 float: right;
 margin: 12px;
 padding: 12px;
 width:112px;
 height:63px;
 box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  font-size: 13px;
  cursor:pointer;
}
.floating-box1 {
    display: inline-block;
	margin:10px;
	float:left;
	clear:both;
 }
 .floating-box2 {
 	display: inline-block;
  float: left;
	margin: 12px;
  padding: 12px;
	width:412px;
  height:53px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
   font-size: 15px;
   cursor:pointer;
 }
.rad input {visibility: hidden; width: 0px; height: 0px;}
label{
		cursor:pointer;
	}
	/*auto complete */
@charset "utf-8";.ui-autocomplete{z-index:1000 !important;cursor:default;list-style:none;}
.ui-widget{}
.ui-autocomplete{overflow-y:auto;overflow-x:hidden;}
.ui-menu{width:0px;display:none;}
.ui-autocomplete > li{padding:10px;padding-left:10px;}
ul{margin-bottom:0;}
.ui-autocomplete > li.ui-state-focus{background-color:#DDD;}
.ui-autocomplete .ui-front .ui-menu .ui-widget .ui-widget-content{border:0;}
.ui-helper-hidden-accessible{display:none;}
.gobumpr-icon{font-style:normal;font-weight:normal;speak:none;display:inline-block;text-decoration:inherit;text-align:center;font-variant:normal;text-transform:none;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
.ui-widget{background-color:white;width:100%;}
.ui-widget-content{padding-left:1px;display:block;width:20px;position:relative;line-height:12px;max-height:210px;border:.5px solid #DADADA;}
.ui-widget{}
.ui-autocomplete { position: absolute; cursor: default;}

/* table */
thead:hover{
	cursor:pointer;
}

.results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px;
  color:#ccc;
}

</style>
<style>
.tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    bottom: 100%;
    left: 50%;
    margin-left: -60px;
    
    /* Fade in tooltip - takes 1 second to go from 0% to 100% opac: */
    opacity: 0;
    transition: opacity 1s;
}

.tooltip:hover .tooltiptext {
    visibility: visible;
    opacity: 1;
}
</style>
</head>
<body>
<?php include_once("header.php"); ?>
<script>
	$(document).ready(function(){
		$('#city').show();
	})
</script>
<div class="overlay" data-sidebar-overlay></div>

<div class="padding"></div>

  <div  id="div1" style="margin-top:10px;float:left;margin-left:43px; border:2px solid #4CB192; border-radius:5px;width:34%;height:610px;">
		<!-- vehicle filter -->
		<div class=" col-xs-1" style="margin-top: 10px;float:left; margin-right:40px;">
		    <select id="veh_type" name="veh_type" class="form-control" style="width:69px;">
		      <option value="all" selected>All</option>
		      <option value="2w">2W</option>
		      <option value="4w">4W</option>
		    </select>
		</div>

		<!-- Search Bar -->
		<div class=" col-xs-2 form-group" style="margin-top: 10px;float:left; margin-right:40px;">
      	      <input type="text" class="searchs form-control" placeholder="Search" style="width:135px;">
		</div>
		
<!-- location filter -->
<div class=" col-xs-2  col-lg-offset-1" style="margin-top:10px;float:left;margin-left:5%;">
		<div class=" form-group" id="loc" style="width:125px;">
		        <div class="ui-widget">
		        	<input class="form-control autocomplete" id="location" type="text" name="location"  required placeholder=" Location">
		        </div>
		</div>
 </div>
 <div id="sl" class="form-group col-xs-1 col-lg-offset-1 col-xs-offset-0" style="margin-left:55px;font-size:16px;color:#95A186;">
 &nbsp;<i class="fa fa-search" aria-hidden="true"></i>
 </div>
 <div id="s2" class="form-group col-xs-1 col-lg-offset-1 col-xs-offset-0" style="cursor:pointer;margin-left:0px;font-size:16px;color:#95A186;">
 &nbsp;<i class="fa fa-refresh" aria-hidden="true"></i>
 </div>



 <!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
	<div class='uil-default-css' style='transform:scale(0.58);'>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
</div>
</div>
<!-- table -->
  <div id = "table" align="center" style="max-width:88%;margin-top:60px; margin-left:20px; display:none;height:510px !important;overflow:auto !important;">
  <table id="example" class="table table-striped table-bordered tablesorter table-hover results"  >
  <thead style="background-color: #D3D3D3;">
  <th>No</th>
	<th>Shop Name</th>
  <th>Credits</th>
  </thead>
  <tbody id="tbody">
  </tbody>
  </table>
  </div>


  </div> <!-- div 1 -->
  <div id="div2" style="margin-top:10px;margin-left:50px; float:left; border:2px solid #DFA64E; border-radius:5px;width:55%;height:610px;">

		<!-- date range picker -->
 	 <div id="reportrange" class=" col-sm-3 " style="cursor: pointer;">
 			<div class=" floating-box1">
 				<div id="range" class="form-control" style="min-width:312px;">
 				<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
 			 <span id="dateval"></span> <b class="caret"></b>
 		 </div>
 			 </div>
 	 </div>
	 <!-- master service type filter -->
<div class=" col-sm-2 col-lg-2" style="cursor: pointer; margin-left: 135px;">
    <div class=" floating-box1">
      <select id="master_service" name="master_service" class="form-control" style="width:170px;">
      <option value="all" selected>MasterService</option>
      <?php 
      $sql_master_service = "SELECT DISTINCT(master_service) FROM go_axle_service_price_tbl WHERE bridge_flag='0' ORDER BY  master_service ASC";
      $res_master_service = mysqli_query($conn1,$sql_master_service);
      while($row_master_service = mysqli_fetch_object($res_master_service)){
        $master_service = $row_master_service->master_service;
        ?>
        <option value="<?php echo $master_service; ?>"><?php echo $master_service; ?></option>
        <?php
      }
      ?>
      </select>
    </div>
</div>
<!-- service type filter -->
<div class=" col-sm-2 col-lg-2" style="cursor: pointer;margin-left:55px;">
    <div class=" floating-box1">
      <select id="service" name="service" class="form-control" style="width:190px;">
        <option selected value="all">All Services</option>
        <?php
        $sql = "SELECT DISTINCT service_type FROM go_axle_service_price_tbl WHERE bridge_flag='0'";
        $res = mysqli_query($conn1,$sql);
        while($row = mysqli_fetch_object($res)){
          ?>
          <option value="<?php echo $row->service_type; ?>"><?php echo $row->service_type; ?></option>
          <?php
        }
        ?>
      </select>
    </div>
</div>
<!--<canvas id="gaugeMeter" style="margin-top: 30px;margin-bottom: 50px;margin-left: 30px;"><span class="tooltiptext">Tooltip text</span></canvas>-->
<div class="chart-container" style="position: relative;float:left;width:49%;">
<canvas id="barChart1"></canvas>
</div>
<div class="chart-container" style="position: relative;float:right;width:49%;">
<canvas id="donutChart"></canvas>
</div>
<div class="chart-container" style="position: relative;float:left;width:49%;top: 10%;">
<canvas id="barChart"></canvas>
</div>
<div class="chart-container" style="position: relative;float:right;width:49%;top: 10%;">
<canvas id="pieChart"></canvas>
</div>
<div class="floating-box" id="toggler" align="center" style="top: 540px;left: 1100px;position: absolute;padding-top: 8px;padding-right: 8px;height:35px;float:right;background-color: #00031A5E;width: 160px;margin-top: 88px;">
		<p>Feedback Panel<span><i class="fa fa-line-chart" aria-hidden="true" style="display:inline;padding-left:5px;"></i></span></p>
	 </div>
	 <!-- loading -->
	 <div id="loading1" style="display:none; margin-top:140px;" align="center">
	 	<div class='uil-default-css' style='transform:scale(0.58);'>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 </div>
	 </div>
	 <div id="divs2">
	 
	 </div>
	 </div>
	 	 
  <!-- table sorter -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.28.9/js/jquery.tablesorter.min.js"></script>	<!-- date picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<!-- search bar  -->
<script>
$(document).ready(function() {
  $(".searchs").keyup(function () {
    var searchTerm = $(".searchs").val();
    var listItem = $('.results tbody').children('tr');
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

  $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });

  $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','false');
  });

  $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','true');
  });

  var jobCount = $('.results tbody tr[visible="true"]').length;
    $('.counter').text(jobCount + ' item');

  if(jobCount == '0') {$('.no-result').show();}
    else {$('.no-result').hide();
	}
		  });
});
</script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- date range picker -->
<script>
$('input[name="daterange"]').daterangepicker({
 locale: {
      format: 'DD-MM-YYYY'
    }
});
</script>
<script type="text/javascript">
$(function() {

  var start = end = moment().subtract(5, 'days');
	 // var start = moment();
    //var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});
</script>

<!-- select Location of service center -->
<script>
function locality() {
	var city = $('#city').val();
			    $( "#location" ).autocomplete({
source: function(request, response) {
	
            $.ajax({
                url: "ajax/get_b2b_loc.php",
                data: {
                    term: request.term,
					city:city,
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
					//console.log(data);
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
}
</script>
 <script>
$(
	locality()
);
</script>
<script>
function view_shops(){
	var veh = $("#veh_type").val();
	var loc = $("#location").val();
	var city = $("#city").val();
	$.ajax({
				url : "ajax/get_shops.php",  // create a new php page to handle ajax request
				type : "POST",
				data : {"veh": veh ,"loc":loc, "city": city },
				success : function(data) {
		$("#loading").hide();
		$("#table").show();
		if(data == 'no'){
			$('#table').html("<h3 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h3>");
		}
		else{
			 $('#tbody').html(data);
			 $("#example").trigger("update");
		}
	  },
			error: function(xhr, ajaxOptions, thrownError) {
				//alert(xhr.status + " "+ thrownError);
		 }
	 });
}
</script>
<!-- default shops view -->
<script>
$(document).ready(function(){
	$("#table").hide();
	$("#loading").show();
		view_shops();
});
</script>
<!-- shops view on changing veh type -->
<script>
$(document).ready(function(){
	$("#veh_type").change(function(){
		$("#table").hide();
		$("#loading").show();
		view_shops();
	});
});
</script>
<!-- shops view on changing location -->
<script>
$(document).ready(function(){
 $("#location").on("autocompletechange", function(){
	    $("#table").hide();
			$("#loading").show();
	 		view_shops();
	});
});
</script>
<!-- shops view on changing city -->
<script>
$(document).ready(function(){
 $("#city").change(function(){
	    $("#table").hide();
			$("#loading").show();
			$("#location").autocomplete('close').val('');			
			locality();
	 		view_shops();
	});
});
</script>
<script>




function get_values(id){
 var shop_id = id;
 //var item = "leads_sent";
 var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
 var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
 $.ajax({
			 url : "ajax/get_axle_count.php",  // create a new php page to handle ajax request
			 type : "POST",
			 data : {"shop_id": shop_id , "startdate": startDate, "enddate": endDate},
			 success : function(data) {
	 			$("#loading1").hide();
	 			$("#divs2").show();
				$("#divs2").html(data);
	 },
		 error: function(xhr, ajaxOptions, thrownError) {
			// alert(xhr.status + " "+ thrownError);
		}
	});
}
</script>

<!-- get count -->
<script>
function get_count(id){
	 var x= id;
	$('input[name="radio_shop"]').not(':checked').next('label').css('color', 'black');
	$('input[name="radio_shop"]:checked').next('label').css('color', '#ffa800');
	//$("#divs2").hide();
	//$("#loading1").show();
	//get_values(x);
	
}
</script>
<!-- get count on changing dates -->
<script>
$(document).ready(function() {
$('#dateval').on("DOMSubtreeModified", function (){
	var x=$('input[name="radio_shop"]:checked').val();
	//console.log(x);
	//get_count(x);
	get_data(x);
} );
});
</script>
<script>
$(document).ready(function()
	{
		$('[data-toggle="rating_tooltip"]').tooltip();
		$("#example").tablesorter(  {sortList: [[0,2], [0,0]]} );
	}
);
</script>

<script>

/*function loadGauge(val){
	var opts = {
	  angle: 0, // The span of the gauge arc
	  lineWidth: 0.4, // The line thickness
	  radiusScale: 0.95, // Relative radius
	  pointer: {
		length: 0.5, // // Relative to gauge radius
		strokeWidth: 0.031, // The thickness
		color: '#000000' // Fill color
	  },
	  limitMax: false,     // If false, max value increases automatically if value > maxValue
	  limitMin: false,     // If true, the min value of the gauge will be fixed
	  colorStart: '#6FADCF',   // Colors
	  colorStop: '#8FC0DA',    // just experiment with them
	  strokeColor: '#E0E0E0',  // to see which ones work best for you
	  generateGradient: true,
	  highDpiSupport: true,     // High resolution support
	  staticLabels: {
	  font: "16px sans-serif",  // Specifies font
	  labels: [0,1, 2,3,4,5],  // Print labels at these values
	  color: "#000000",  // Optional: Label text color
	  fractionDigits: 0  // Optional: Numerical precision. 0=round off.
	},
	staticZones: [
	   {strokeStyle: "#F03E3E", min: 0, max: 2}, // Red from 100 to 130
	   {strokeStyle: "#FFDD00", min: 2, max: 4}, // Yellow
	   {strokeStyle: "#30B32D", min: 4, max: 5}, // Green
	],
	  
	};
	var target = document.getElementById('gaugeMeter'); // your canvas element
	var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
	gauge.maxValue = 5; // set max gauge value
	gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
	gauge.animationSpeed = 10; // set animation speed (32 is default value)
	gauge.set(val); // set actual value
}*/
var donut = $("#donutChart");
var bar = $("#barChart");
var bar1 = $("#barChart1");
var pie = $("#pieChart");

function initDonut()
{
	myDonutChart = new Chart(donut, {
		type: 'pie',
		data: {
					labels: ["Ratings & Reviews", "Reviews"],
					datasets: [{
						data: [0,0],
						backgroundColor:['rgb(255,99,132)','rgb(54,162,235)']
						}]
						},
		 options: {
			cutoutPercentage : 70/*,
			responsive: true,
			maintainAspectRatio: false*/
		 }
		});
}

function loadDonut(data,z,label = ["Feedback Team", "RTT"])
{
	if(z>1)
	{
		myDonutChart.destroy();
	}
	
	myDonutChart = new Chart(donut, {
		type: 'pie',
		data: {
					labels: label,
					datasets: [{
						data: data,
						backgroundColor:['rgb(255,99,132)','rgb(54,162,235)']
						}]
						},
		 options: {
			cutoutPercentage : 70/*,
			responsive: true,
			maintainAspectRatio: false*/
		 }
		});
	//myDonutChart.update();
}

function initPie()
{
	myPieChart = new Chart(pie, {
    type: 'pie',
    data: {
        		labels: ["Not Updated","1", "2", "3","4","5"],
        		datasets: [{
        			data: [0, 0, 0,0,0,0],
       	 			backgroundColor:['','rgb(255,99,132)','rgb(54,162,235)','rgb(255,206,86)','rgb(255,159,64)','rgb(75,192,192)']
   	 				}]
					}/*,
     options: {
		responsive: true,
		maintainAspectRatio: false
     }*/
    });
}

function loadPie (z,data)
{
	if(z>1)
	{
		myPieChart.destroy();
	}
	myPieChart = new Chart(pie, {
    type: 'pie',
    data: {
        		labels: ["Not Updated","1", "2", "3","4","5"],
        		datasets: [{
        			data: data,
       	 			backgroundColor:['','rgb(255,99,132)','rgb(54,162,235)','rgb(255,206,86)','rgb(255,159,64)','rgb(75,192,192)']
   	 				}]
					}
	});
}

function initBar()
{
	var data = {
	  labels: ['No data'],
	  datasets: [{
		label: "Avg Billing vs Service Types",
		backgroundColor: "rgba(255,99,132,0.2)",
		borderColor: "rgba(255,99,132,1)",
		borderWidth: 1,
		hoverBackgroundColor: "rgba(255,99,132,0.4)",
		hoverBorderColor: "rgba(255,99,132,1)",
		data: [0],
	  }]
	};
	var option = {
	legend: {
		  display: false
		},
		tooltips: {
		  callbacks: {
			label: function(tooltipItem) {
			  return tooltipItem.yLabel;
			}
		  }
		},
	  scales: {
		yAxes: [{
		  stacked: true,
		  gridLines: {
			display: true,
			color: "rgba(255,99,132,0.2)"
		  }
		}],
		xAxes: [{
		  gridLines: {
			display: false
		  },
		  display: false
		  /*ticks: {
			autoSkip: false
		   }*/
		}]
	  }
	};
	var myBarChart = Chart.Bar(bar, {
	  data: data,
	  options: option
	});
}

function loadBar(z,val,label)
{
	if(z>1)
	{
		myBarChart.destroy();
	}
	var data = {
	  labels: label,
	  datasets: [{
		label: "Avg Billings",
		backgroundColor: "rgba(255,99,132,0.2)",
		borderColor: "rgba(255,99,132,1)",
		borderWidth: 1,
		hoverBackgroundColor: "rgba(255,99,132,0.4)",
		hoverBorderColor: "rgba(255,99,132,1)",
		data: val,
	  }]
	};
	var option = {
	legend: {
		  display: true
		},
		tooltips: {
		  callbacks: {
			label: function(tooltipItem) {
			  return tooltipItem.yLabel;
			}
		  }
		},
	  scales: {
		yAxes: [{
		  stacked: true,
		  gridLines: {
			display: true,
			color: "rgba(255,99,132,0.2)"
		  }
		}],
		xAxes: [{
		  gridLines: {
			display: false
		  },
		  display: false
		  /*ticks: {
			autoSkip: false
		   }*/
		}]
	  }
	};
	myBarChart = Chart.Bar(bar, {
	  data: data,
	  options: option
	});
}
	
function initBar1()
{
	var data = {
	  labels: ['No data'],
	  datasets: [{
		label: "Avg Conv Rate vs Service Types",
		backgroundColor: "rgba(255,99,132,0.2)",
		borderColor: "rgba(255,99,132,1)",
		borderWidth: 1,
		hoverBackgroundColor: "rgba(255,99,132,0.4)",
		hoverBorderColor: "rgba(255,99,132,1)",
		data: [0],
	  }]
	};
	var option = {
	legend: {
		  display: false
		},
		tooltips: {
		  callbacks: {
			label: function(tooltipItem) {
			  return tooltipItem.yLabel;
			}
		  }
		},
	  scales: {
		yAxes: [{
		  stacked: true,
		  gridLines: {
			display: true,
			color: "rgba(255,99,132,0.2)"
		  }
		}],
		xAxes: [{
		  gridLines: {
			display: false
		  },
		  display: false
		  /*ticks: {
			autoSkip: false
		   }*/
		}]
	  }
	};
	var myBarChart1 = Chart.Bar(bar1, {
	  data: data,
	  options: option
	});
}

function loadBar1(z,val,label)
{
	if(z>1)
	{
		myBarChart1.destroy();
	}
	var data = {
	  labels: label,
	  datasets: [{
		label: "Avg Conversion %",
		backgroundColor: "rgba(255,99,132,0.2)",
		borderColor: "rgba(255,99,132,1)",
		borderWidth: 1,
		hoverBackgroundColor: "rgba(255,99,132,0.4)",
		hoverBorderColor: "rgba(255,99,132,1)",
		data: val,
	  }]
	};
	var option = {
	legend: {
		  display: true
		},
		tooltips: {
		  callbacks: {
			label: function(tooltipItem) {
			  return tooltipItem.yLabel;
			}
		  }
		},
	  scales: {
		yAxes: [{
		  stacked: true,
		  gridLines: {
			display: true,
			color: "rgba(255,99,132,0.2)"
		  }
		}],
		xAxes: [{
		  gridLines: {
			display: false
		  },
		  display: false
		  /*ticks: {
			autoSkip: false
		   }*/
		}]
	  }
	};
	myBarChart1 = Chart.Bar(bar1, {
	  data: data,
	  options: option
	});
}

</script>

<!-- master seervice type -->
<script>
function get_data(id){
	{
		z=z+1;
		console.log(z);
		$('input[name="radio_shop"]').not(':checked').next('label').css('color', 'black');
		$('input[name="radio_shop"]:checked').next('label').css('color', '#ffa800');
		var shop_id = $('input[name="radio_shop"]:checked').val();
		var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD').toString();
		var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD').toString();
		var master_service = $("#master_service").val();
		var service = $("#service").val();
		var city = $('#city').val();
		$.ajax({
			url : "ajax/get_visuals_data.php",
			type : "POST",
			data : {"shop_id":shop_id,"startDate":startDate,"endDate":endDate,"master_service":master_service,"service":service,"city":city},
			dataType: "json",
			success : function(data) {
				var avg_rating = data[0].avg_rating;
				var ratingOnly = data[0].ratingOnly;
				var ratingReview = data[0].ratingReview;
				var avg_billing_label = data[1].service_type;
				var avg_billing_value = data[1].billing_avg;
				var ratingData = data[2].rate;
				var feedback_team = data[3][""];
				var rtt = data[3].axle;
				var conv_service_type = Object.keys(data[4]);
				var conv_rate_service_type = Object.values(data[4]);
				donutData = [feedback_team,rtt];
				rdata = [data[2].rate["0"],data[2].rate[1],data[2].rate[2],data[2].rate[3],data[2].rate[4],data[2].rate[5]];
				if(z==1)
				{
					initDonut();
					initBar();
					initBar1();
					initPie();
				}
					loadDonut(donutData,z);
					loadBar(z,avg_billing_value,avg_billing_label);
					loadBar1(z,conv_rate_service_type,conv_service_type);
					loadPie(z,rdata);
				//console.log(ratingReview);*/
			}
		});
	}
}

function get_services(){
  var master = $("#master_service").val();
  $("#service").empty();
  var x=$('input[name="radio_shop"]:checked').val();
  $.ajax({
	    url : "ajax/get_services_master.php",  // create a new php page to handle ajax request
	    type : "POST",
	    data : {"master": master},
	    success : function(data) {
      	$('#service').append(data);
		get_data(x);
  },
	    error: function(xhr, ajaxOptions, thrownError) {
	          //  alert(xhr.status + " "+ thrownError);
	    }
	});
}
</script>
<script>
$(document).ready( function (){
  z=0;
  get_services();
});
</script>
<script>
function refresh(){
	var veh = $("#veh_type").val();
	var loc = $("#location").val();
	var city = $("#city").val();
	$.ajax({
				url : "ajax/get_shops.php",  // create a new php page to handle ajax request
				type : "POST",
				data : {"veh": veh ,"loc":loc, "city": city },
				success : function(data) {
		$("#loading").hide();
		$("#table").show();
		if(data == 'no'){
			$('#table').html("<h3 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h3>");
		}
		else{
			 $('#tbody').html(data);
			 $("#example").trigger("update");
		}
	  },
			error: function(xhr, ajaxOptions, thrownError) {
				//alert(xhr.status + " "+ thrownError);
		 }
	 });
	 z=z+1;
	var shop_id = undefined;
	var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD').toString();
	var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD').toString();
	var master_service = $("#master_service").val();
	var service = $("#service").val();
	var city = $('#city').val();
	$.ajax({
		url : "ajax/get_visuals_data.php",
		type : "POST",
		data : {"shop_id":shop_id,"startDate":startDate,"endDate":endDate,"master_service":master_service,"service":service,"city":city},
		dataType: "json",
		success : function(data) {
			var avg_rating = data[0].avg_rating;
			var ratingOnly = data[0].ratingOnly;
			var ratingReview = data[0].ratingReview;
			var avg_billing_label = data[1].service_type;
			var avg_billing_value = data[1].billing_avg;
			var ratingData = data[2].rate;
			var feedback_team = data[3][""];
			var rtt = data[3].axle;
			var conv_service_type = Object.keys(data[4]);
			var conv_rate_service_type = Object.values(data[4]);
			donutData = [feedback_team,rtt];
			//loadGauge(avg_rating);
			rdata = [data[2].rate["0"],data[2].rate[1],data[2].rate[2],data[2].rate[3],data[2].rate[4],data[2].rate[5]];
			if(z==1)
			{
				initDonut();
				initBar();
				initBar1();
				initPie();
			}
				loadDonut(donutData,z);
				loadBar(z,avg_billing_value,avg_billing_label);
				loadBar1(z,conv_rate_service_type,conv_service_type);
				loadPie(z,rdata);
			
			//console.log(ratingReview);*/
		}
	});
}
$(document).ready( function (){
  $("#master_service").change(function(){
    get_services();
    
  });
  $("#service").change(function(){
    var x=$('input[name="radio_shop"]:checked').val();
	get_data(x);
    
  });
  $(".fa-refresh").on("click",function(){
	refresh();
  });
  $("#toggler").on("click",function(){
	  sdt = window.btoa($('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD').toString());
	  edt = window.btoa($('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD').toString());
	  sid = window.btoa($('input[name="radio_shop"]:checked').val());
	  window.location.href = "feedback_panel.php?sdt="+sdt+"&edt="+edt+"&sid="+sid;
  });
});
</script>
<script>
</script>
</body>
</html>

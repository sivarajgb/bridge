<?php
include '../config.php';
$conn = db_connect1();
session_start();
if (empty($_SESSION['crm_log_id'])) {

	header('location:logout.php');
	die();
}
$day = date('Y-m-d');

$crm_log_id = $_SESSION['crm_log_id'];
//print_r($_POST);

$start = date('Y-m-d', strtotime($_POST["start_date"]));
$end = date('Y-m-d', strtotime($_POST["end_date"]));
$veh = $_POST["veh"];
$city = $_POST["city"];
$data_per_1 = $_POST["data_limit1"];
$data_per_2 = $_POST["data_limit2"];
$data_per_3 = $_POST["data_limit3"];
$data_per_4 = $_POST["data_limit4"];
$data_per_5 = $_POST["data_limit5"];

$data_count = $data_per_1 + $data_per_2 + $data_per_3 + $data_per_4 + $data_per_5;
if ($data_count > '100') {
	header("Refresh:3; url=../ire_admin.php");
	echo "<h2 style='text-align:center; color:#CC0000;'>Please Enter Sum of 100 persent in all Levels.<h2>";
}
$data_limit1 = ($data_per_1 * 10);
$data_limit2 = ($data_per_2 * 10);
$data_limit3 = ($data_per_3 * 10);
$data_limit4 = ($data_per_4 * 10);
$data_limit5 = ($data_per_5 * 10);

$city_query = "SELECT * FROM go_bumpr.ire_limits_details where city='$city' and veh_type='$veh' and date(log)='$day'";
$res_city = mysqli_query($conn, $city_query);
$num = mysqli_num_rows($res_city);
if ($num > 0) {
	header("Refresh:3; url=../ire_admin.php");
	echo "<h2 style='text-align:center; color:#CC0000;'>City and Vehicle type is already registered today.<h2>";
} else {

	$query = "INSERT INTO go_bumpr.ire_limits_details (start,end,data_limit1,data_limit2,data_limit3,data_limit4,data_limit5,crm_update_time,log,city,data_per_1, data_per_2,data_per_3,data_per_4,data_per_5,veh_type,crm_log_id) VALUES ('$start', '$end', '$data_limit1', '$data_limit2', '$data_limit3', '$data_limit4', '$data_limit5',now(),now(), '$city', '$data_per_1', '$data_per_2', '$data_per_3', '$data_per_4', '$data_per_5','$veh','$crm_log_id')";

	$result = mysqli_query($conn, $query);

	if ($result) {
		$msg = "<h2 style='text-align:center; color:#FFCC00;'>IRP Limits are inserted successfully...!<h2>";
	} else {
		$msg = "Problem in insert..!";
	}
	header("Refresh:3; url=../ire_admin.php");
	echo $msg;
	header("Refresh:3; url=../ire_admin.php");
}
<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
include("../../config.php");
$conn = db_connect1();
//$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$flag = $_SESSION['flag'];
$super_flag = $_SESSION['super_flag'];

$src_crm = array('crm016', 'crm017', 'crm064', 'crm036', 'crm033', 'crm034', 'crm029');
$src_column = 0;
if (in_array($crm_log_id, $src_crm)) {
    $src_column = 1;
}

$startdate = date('Y-m-d', strtotime($_GET['startdate']));
$enddate = date('Y-m-d', strtotime($_GET['enddate']));
$person = $_GET['person'];
//$source = $_GET['source'];
$vehicle = $_GET['vehicle'];
//$service = $_GET['service'];
//$paid = $_GET['paid'];
$city = $_GET['city'];
//$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

//$col_name = $vehicle == '2w' ? 'l.bike_cluster' : 'l.car_cluster';

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;

$cond = '';

$cond = $cond . ($vehicle == 'all' ? "" : " AND a.vehicle_type='$vehicle'");
//$cond = $cond.($service == 'all' ? "" : "AND service_type='$service'");
$cond = $cond . ($person == 'all' ? "" : " AND crm_update_id='$person'");

$cond = $cond . ($city == 'all' ? "" : " AND a.city='$city'");
if ($flag == '1') {
    $sql_booking = "SELECT a.user_id,a.id,b.name,a.mobile_number,a.b2b_shop_id,a.service_date,a.mec_id,a.locality,a.service_type,a.city,a.shop_name,a.rating,a.status,a.log,a.vehicle_type,a.booking_id,ca.name as crm_name from ire_user_tbl a join user_register b on a.user_id=b.reg_id left join crm_admin ca on ca.crm_log_id=a.crm_update_id where  DATE(a.crm_update_time) BETWEEN '$startdate' and '$enddate' and a.status='0' {$cond}";
} else {
    $sql_booking = "SELECT a.user_id,a.id,b.name,a.mobile_number,a.b2b_shop_id,a.service_date,a.mec_id,a.locality,a.service_type,a.city,a.shop_name,a.rating,a.status,a.log,a.vehicle_type,a.booking_id,ca.name as crm_name from ire_user_tbl a join user_register b on a.user_id=b.reg_id left join crm_admin ca on ca.crm_log_id=a.crm_update_id where  DATE(a.crm_update_time) BETWEEN '$startdate' and '$enddate' and a.status='0' and  a.city='$city' and a.crm_update_id='$crm_log_id'";
}

mysqli_set_charset($conn, 'utf8');
//echo $sql_booking;die;
$res_booking = mysqli_query($conn, $sql_booking);
// $no = 0;
// $arr = array();
$count = mysqli_num_rows($res_booking);

if ($count > 0) {
    while ($row_booking = mysqli_fetch_object($res_booking)) {
        $tr = "<tr>";
        if ($flag == '1') {
            $td10 = "<td><input  type='checkbox' id='check_veh' name='check_veh[]' value=" . $row_booking->id . " /> </td>";
        }
        $td1 = "<td>" . $row_booking->name . "</td>";
        $td2 = "<td><a href='user_details_ire.php?t=" . base64_encode('ire') . "&bi=" . base64_encode($row_booking->booking_id) . "&ire=" . base64_encode($row_booking->id) . "'><i id=" . $row_booking->booking_id . " class='fa fa-eye' aria-hidden='true'></i></td>";
        $td3 = "<td>" . $row_booking->mobile_number . "</td>";
        $td4 = "<td>" . $row_booking->locality . "</td>";
        if ($src_column == '1') {
            $td13 = "<td>" . $row_booking->service_type . "</td>";
            $td14 = "<td>" . $row_booking->service_type . "</td>";
        }
        $td5 = "<td>" . $row_booking->service_type . "</td>";
        $td11 = "<td>" . $row_booking->vehicle_type . "</td>";
        $td6 = "<td>" . $row_booking->shop_name . "</td>";
        $td7 = "<td>" . $row_booking->service_date . "</td>";
        $td8 = "<td>" . $row_booking->rating . "</td>";
        $td9 = "<td>" . $row_booking->log . "</td>";
        $td12 = "<td>" . $row_booking->crm_name . "</td>";
        $tr_l = "</tr>";

        if ($src_column == '1') {
            $str = $tr . $td10 . $td1 . $td2 . $td3 . $td4 . $td11 . $td13 . $td14 . $td9 . $td12 . $tr_l;
        } else {
            $str = $tr . $td10 . $td1 . $td2 . $td3 . $td4 . $td11 . $td9 . $td12 . $tr_l;
        }
        $data[] = array('tr' => $str);

    }
    echo $data1 = json_encode($data);
} else {
    $tr = "<tr class='no-count'>";
    if ($flag == '1') {
        $td1 = "<td colspan='12' align='center' style='padding: 50px 0'><h3>No Result Found</h3></td>";
    } else {
        $td1 = "<td colspan='11' align='center' style='padding: 50px 0'><h3>No Result Found</h3></td>";
    }
    $tr_l = "</tr>";

    $str = $tr . $td1 . $tr_l;
    $data[] = array('tr' => $str);
    echo $data1 = json_encode($data);
}
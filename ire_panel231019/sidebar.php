<?php
include("../config.php");
session_start();
$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$crm_flag = $_SESSION['crm_flag'];
$flag = $_SESSION['flag'];
$feedback_admin = $_SESSION['feedback_admin'];
$crm_feedback = $_SESSION['crm_feedback'];
$super_flag = $_SESSION['super_flag'];
$reenguage = $_SESSION['reenguage'];
$cluster_admin = $_SESSION['cluster_admin'];

?>

<div class="overlay" data-sidebar-overlay></div>

<aside data-sidebar style="margin-top: -3px;position:fixed;overflow-y:auto !important;font-size: 14px !important;">
    <div class="padding">
        <h4 style="margin-top:-20px;margin-bottom:20px;margin-right:170px !important;">
            <img src="images/logo_B.png"
                 width="150" height="50"/>
        </h4>
        <?php
        if ($flag == 1) {
            ?>
            <a href="../../aleads.php?t=<?php echo base64_encode(l); ?>">
                <i class="fa fa-exclamation-circle"
                   aria-hidden="true"></i>&nbsp;&nbsp;NewBookings</a>
            <br>
            <br>
            <a href="../../abookings.php?t=<?php echo base64_encode(b); ?>"><i class="fa fa-check-circle"
                                                                         aria-hidden="true"></i>&nbsp;&nbsp;GoAxled</a>
            <br>
            <br>
            <a href="../../afollowups.php?t=<?php echo base64_encode(f); ?>"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;&nbsp;FollowUps</a>
            <br>
            <br>
            <a href="../../aothers.php?t=<?php echo base64_encode(o); ?>"><i class="fa fa-tasks" aria-hidden="true"></i>&nbsp;&nbsp;Others</a>
            <br>
            <br>
            <a href="../../aaxle.php"><i class="fa fa-adn" aria-hidden="true"></i>&nbsp;&nbsp;Axle</a><br>
            <br>
            <a href="../../overrides.php?t=<?php echo base64_encode(ov); ?>"><i class="fa fa-exchange" aria-hidden="true"></i>&nbsp;&nbsp;overrides</a>
            <br>
            <br>
            <a href="../../int_re_eng_panel.php"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;&nbsp;IRP</a>
            <br>
            <br>
            <a href="../../partner_usage.php"><i class="fa fa-line-chart" aria-hidden="true"></i>&nbsp;&nbsp;Partners</a><br>
            <br>
            <a href="../../allocation_interface.php"><i class="fa fa-sliders"
                                                  aria-hidden="true"></i>&nbsp;&nbsp;Allocation</a><br>
            <br>
            <a href="../../conversion_panel.php"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;&nbsp;Conversion</a>
            <br>
            <br>
            <a href="../../conversion_panel_feedback.php"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;&nbsp;Conversion
                Feedback</a><br>
            <br>
            <a href="../../conversion_panel_re_engagement.php"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;&nbsp;Conversion
                Re_Engagement</a><br>
            <br>
            <a href="../../sc_stats.php"><i class="fa fa-signal" aria-hidden="true"></i>&nbsp;&nbsp;SC-Stats</a><br>
            <br>

            <a href="../../daily_reports_v2.php?t=<?php echo base64_encode(reports); ?>"><i class="fa fa-list-alt"
                                                                                      aria-hidden="true"></i>&nbsp;&nbsp;Reports</a>
            <br>


            <?php
            if ($super_flag == '1') {
                ?>
                <br>
                <a href="../../credits_history.php"><i class="fa fa-history" aria-hidden="true"></i>&nbsp;&nbsp;CreditsHistory</a>
                <br>
                <br>
                <a href="../../shop_list.php"><i class="fa fa-toggle-on" aria-hidden="true"></i>&nbsp;&nbsp;ST-Toggle</a><br>
                <br>
                <a href="../../axle_sales_portal.php"><i class="fa fa-tachometer" aria-hidden="true"></i>&nbsp;&nbsp;AxleSales</a>
                <br>
                <?php
            }
            if ($feedback_admin == '1') {
                ?>
                <br>
                <a href="../../afdbkhome.php"><i class="fa fa-comments" aria-hidden="true"></i> FeedBack</a><br>
                <?php
            } elseif ($crm_feedback == '1') {
                ?>
                <br>
                <a href="../../fdbkhome.php"><i class="fa fa-comments" aria-hidden="true"></i> FeedBack</a><br>

                <br>
                <a href="../../axle.php"><i class="fa fa-adn" aria-hidden="true"></i>&nbsp;&nbsp;Axle</a><br>


                <?php
            }

        } else if ($cluster == 1) {
            ?>
            <a href="../../mleads.php?t=<?php echo base64_encode(l); ?>"><i class="fa fa-exclamation-circle"
                                                                      aria-hidden="true"></i>&nbsp;&nbsp;NewBookings</a>
            <br>
            <br>
            <!--<a href="../../mbookings.php?t=<?php echo base64_encode(b); ?>"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp;GoAxled</a><br>
          <br>-->
            <a href="../../mfollowups.php?t=<?php echo base64_encode(f); ?>"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;&nbsp;FollowUps</a>
            <br>
            <br>
            <a href="../../mothers.php?t=<?php echo base64_encode(o); ?>"><i class="fa fa-tasks" aria-hidden="true"></i>&nbsp;&nbsp;Others</a>
            <br>
            <!--  <br>
             <a href="../../aaxle.php"><i class="fa fa-adn" aria-hidden="true"></i>&nbsp;&nbsp;Axle</a><br>
             <br> -->
            <a href="../../int_re_eng_panel.php"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;&nbsp;IRP</a>
            <br>
            <!-- <br>
            <a href="../../partner_usage.php"><i class="fa fa-line-chart" aria-hidden="true"></i>&nbsp;&nbsp;Partners</a><br>
            <br> -->
            <a href="../../stats_master_service.php"><i class="fa fa-compass" aria-hidden="true"></i>&nbsp;&nbsp;Service-Stats</a>
            <br>
            <br>
            <a href="../../allocation_interface.php"><i class="fa fa-sliders"
                                                  aria-hidden="true"></i>&nbsp;&nbsp;Allocation</a><br>
            <br>
            <a href="../../conversion_panel.php"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;&nbsp;Conversion</a>
            <br>
            <a href="../../conversion_panel_feedback.php"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;&nbsp;Conversion
                Feedback</a><br>
            <br>

            <?php
            if ($super_flag == '1') {
                ?>
                <br>
                <a href="../../credits_history.php"><i class="fa fa-history" aria-hidden="true"></i>&nbsp;&nbsp;CreditsHistory</a>
                <br>
                <br>
                <a href="../../shop_list.php"><i class="fa fa-toggle-on" aria-hidden="true"></i>&nbsp;&nbsp;ST-Toggle</a><br>
                <br>
                <a href="../../axle_sales_portal.php"><i class="fa fa-tachometer" aria-hidden="true"></i>&nbsp;&nbsp;AxleSales</a>
                <br>
                <?php
            }
            if ($feedback_admin == '1') {
                ?>
                <br>
                <a href="../../afdbkhome.php"><i class="fa fa-comments" aria-hidden="true"></i> FeedBack</a><br>

                <?php
            } elseif ($crm_feedback == '1') {
                ?>
                <br>
                <a href="../../fdbkhome.php"><i class="fa fa-comments" aria-hidden="true"></i> FeedBack</a><br>

                <?php
            }

        } else {
            ?>

            <a href="../../leads.php?t=<?php echo base64_encode(l); ?>"><i class="fa fa-exclamation-circle"
                                                                     aria-hidden="true"></i>&nbsp;&nbsp;NewBookings</a>
            <br>
            <br>
            <!--<a href="../../bookings.php?t=<?php echo base64_encode(b); ?>"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp;GoAxled</a><br>
            <br>-->
            <a href="../../followups.php?t=<?php echo base64_encode(f); ?>"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;&nbsp;FollowUps</a>
            <br>
            <br>
            <a href="../../others.php?t=<?php echo base64_encode(o); ?>"><i class="fa fa-tasks" aria-hidden="true"></i>&nbsp;&nbsp;Others</a>
            <br>
            <!-- <br>
            <a href="../../axle.php"><i class="fa fa-adn" aria-hidden="true"></i>&nbsp;&nbsp;Axle</a><br> -->
            <br>
            <a href="../../int_re_eng_panel.php"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;&nbsp;IRP</a>
            <br>
            <!--             <br>
                        <a href="../../conversion_panel.php"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;&nbsp;Conversion</a><br> -->
            <?php
            if ($feedback_admin == '1') {
                ?>
                <br>
                <a href="../../afdbkhome.php"><i class="fa fa-comments" aria-hidden="true"></i> FeedBack</a><br>

                <?php
            } elseif ($crm_feedback == '1') {
                ?>
                <br>
                <a href="../../fdbkhome.php"><i class="fa fa-comments" aria-hidden="true"></i> FeedBack</a><br>
                <br>
                <a href="../../axle.php"><i class="fa fa-adn" aria-hidden="true"></i>&nbsp;&nbsp;Axle</a><br>


                <?php
            }
        }
        ?>
        <br>
        <a href="../../heatmap.php"><i class="fa fa-map" aria-hidden="true"></i>&nbsp;&nbsp;HeatMap</a><br>
        <br>
        <a href="../../stats_master_service.php"><i class="fa fa-compass" aria-hidden="true"></i>&nbsp;&nbsp;Service-Stats</a><br>
        <br>
        <a href="../../details_update.php"><img src="images/mining.png" style="width:16px;"></img>&nbsp;&nbsp;GoFinder</a><br>
        <br>
        <a href="../../logout.php"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;LogOut</a><br>
        <br>
        <a href="../../#" data-sidebar-button><i class="fa fa-reply" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>

    </div>
</aside>

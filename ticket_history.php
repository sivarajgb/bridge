<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id']))) {
	header('location:logout.php');
	die();
}
$crm_log_id = $_SESSION['crm_log_id'];
$today=date('Y-m-d H:i:s');

$ticket_id=base64_decode($_GET['t']);

if($ticket_id == ''){
  header('location:somethingwentwrong.php');
	die();
}
$_SESSION['modal_sess'] = 'close';
?>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
  <title>GoBumpr Bridge</title>
<!-- validate -->
<script sr="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <!-- masking -->
<script type="text/javascript" src="js/inputmask.js"></script>
<script type="text/javascript" src="js/jquery.inputmask.js"></script>
  <script type="text/javascript" src="js/inputmask.extensions.js"></script>
  <!-- auto complete -->
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/jquery-ui.structure.min.css"> 
<link rel="stylesheet" href="css/jquery-ui.theme.min.css">
<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>
  <style>
	<!-- auto complete -->
	@charset "utf-8";.ui-autocomplete{z-index:1000 !important;cursor:default;list-style:none;}
	.ui-widget{}
	.ui-autocomplete{overflow-y:auto;overflow-x:hidden;}
	.ui-menu{width:0px;display:none;}
	.ui-autocomplete > li{padding:10px;padding-left:10px;}
	ul{margin-bottom:0;}
	.ui-autocomplete > li.ui-state-focus{background-color:#DDD;}
	.ui-autocomplete .ui-front .ui-menu .ui-widget .ui-widget-content{border:0;}
	.ui-helper-hidden-accessible{display:none;}
	.gobumpr-icon{font-style:normal;font-weight:normal;speak:none;display:inline-block;text-decoration:inherit;text-align:center;font-variant:normal;text-transform:none;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
	.ui-widget{background-color:white;width:100%;}
	.ui-widget-content{padding-left:1px;display:block;width:20px;position:relative;line-height:12px;max-height:210px;border:.5px solid #DADADA;}
	.ui-widget{}
	.ui-autocomplete { position: absolute; cursor: default;}


<!-- vehicle type -->
.bike,
.car{
  cursor: pointer;
  user-select: none;
  -webkit-user-select: none;
  -webkit-touch-callout: none;
}
.bike > input,
.car > input{ /* HIDE ORG RADIO & CHECKBOX */
  visibility: hidden;
  position: absolute;
}
/* RADIO & CHECKBOX STYLES */
.bike > i,
.car > i{     /* DEFAULT <i> STYLE */
  display: inline-block;
  vertical-align: middle;
  width:  16px;
  height: 16px;
  border-radius: 50%;
  transition: 0.2s;
  box-shadow: inset 0 0 0 8px #fff;
  border: 1px solid gray;
  background: gray;
}

label > input{ /* HIDE RADIO */
  visibility: hidden; /* Makes input not-clickable */
  position: absolute; /* Remove input from document flow */
}
label > input + img{ /* IMAGE STYLES */
  cursor:pointer;
  border:2px solid transparent;
}
label > input:checked + img { /* (RADIO CHECKED) IMAGE STYLES */
   border-radius:12px;
   padding:5px;
   background-color:#ffa800;
  box-shadow: 0 0 3px 0 #394;
}
.borderless td, .borderless th {
    border: none !important;
}
.datepicker:before {
content: '';
display: inline-block;
border-left: 7px solid transparent;
border-right: 7px solid transparent;
border-bottom: 7px solid #ccc;
border-bottom-color: transparent !important;
position: absolute;
top: -7px;
left: 190px;  // I made a change here 
}

.datepicker:after {
content: '';
display: inline-block;
border-left: 6px solid transparent;
border-right: 6px solid transparent;
border-top-color: transparent !important;
border-top: 6px solid #ffffff;
position: absolute;
bottom: -6px;
left: 191px;  // I made a change here 
}
#datepick > span:hover{cursor: pointer;}

.floating-box {
	 display: inline-block;
	 margin: 22px;
 padding: 22px;
 width:203px;
 height:105px;
 box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  font-size: 17px;
}
select{
    color: red;
}
option{
  height: 25px;
}
option:hover {
  box-shadow: 0 0 10px 100px #ddd inset;
}
</style>

</head>
<body>
<?php include_once("header.php"); ?>
<script>
	$(document).ready(function(){
		$('#city').hide();
	})
</script>
<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>
    <?php
    $sql_get = "SELECT reg_id,name,mobile_number,mobile_number2,email_id,City,vehicle,Locality_Home FROM ticket_tb JOIN user_register ON ticket_tb.user_id = user_register.reg_id WHERE ticket_id='$ticket_id' ";
    $res_get = mysqli_query($conn,$sql_get);
    $row_get = mysqli_fetch_object($res_get);
    $user_id = $row_get->reg_id;
    $user_name = $row_get->name;
    $mobile = $row_get->mobile_number;
	$alt_mobile = $row_get->mobile_number2;
    $email = $row_get->email_id;
    $city = $row_get->City;
    $veh = $row_get->vehicle;
    $user_location = $row_get->Locality_Home;
  $sql_book = mysqli_query($conn,"SELECT t.booking_id,t.issue_faced,b.service_type,t.ticket_status,t.followup_date,t.closure_date,t.log,t.update_log,t.crm_create_id,b.shop_name,b.axle_flag,b.flag,(SELECT c.name FROM ticket_tb t JOIN crm_admin c ON t.crm_create_id = c.crm_log_id WHERE ticket_id = '$ticket_id') as crm_create_name,c.name as crm_update_name FROM ticket_tb t JOIN user_booking_tb b ON t.booking_id = b.booking_id JOIN crm_admin c ON t.crm_alloct_id = c.crm_log_id WHERE ticket_id = '$ticket_id' ;");
    $row_book = mysqli_fetch_array($sql_book);
    $row_count = mysqli_num_rows($sql_book);
       $booking_id=$row_book['booking_id'];
       $issue_faced=$row_book['issue_faced'];
       $s_type = $row_book['service_type'];
       $ticket_date=$row_book['log'];
       $contact_date=$row_book['update_log'];
       $closure_date=$row_book['closure_date'];
       $followup_date=$row_book['followup_date'];
       $crm_create_name=$row_book['crm_create_name'];
       $crm_update_name=$row_book['crm_update_name'];
       $ticket_status=$row_book['ticket_status'];
       $shop_name=$row_book['shop_name'];
       $axle=$row_book['axle_flag'];
       $booking_flag = $row_book['flag'];
    ?>
      <div id="user" style="border:2px solid #708090; border-radius:8px;margin-left:30px; width:380px;height:475px; padding:20px; margin-top:18px; float:left;">
    <table id="table1" class="table borderless" >
       <tr><td><strong>User Id</strong></td><td><?php echo $user_id; ?></td></tr>
       <tr><td><strong>Name</strong></td><td><?php echo $user_name; ?></td></tr>
       <tr><td><strong>Phn No.</strong></td><td><?php echo $mobile; ?></td></tr>
	   <tr><td><strong>E-mail</strong></td><td><?php echo $email; ?></td></tr>
       <tr><td><strong>Booking Id</strong></td><td><?php echo $booking_id; ?></td></tr>
	   <tr><td><strong>Service Type</strong></td><td><?php echo $s_type; ?></td></tr>
	   <tr><td><strong>Mechanic</strong></td><td><?php echo $shop_name; ?></td></tr>
	   <tr><td><strong>GoAxle</strong></td><td><?php if($booking_flag == '1'){ echo "Booking Cancelled"; } else if($axle=='' || $axle=='0'){ echo "Not Sent"; } else{ echo "Sent"; } ?></td></tr>
	
  </table>

 </div>
<div id="ticket_history" align="center" style="margin-left:30px;border:2px solid #708090; border-radius:8px; width:430px;height:475px; padding:20px; margin-top:18px; float:left;">
	<div style="height:295px;overflow-y:auto;">
		<table class="table">
			<thead>
				<th>Log</th>
				<th>Status</th>
				<th>Support</th>
			</thead>
			<tbody>
				<?php
				$sql_get_history = "SELECT t.*,c.name as crm_name FROM ticket_history t JOIN crm_admin c ON c.crm_log_id = t.crm_log_id WHERE t.ticket_id = '$ticket_id' ORDER BY t.log DESC";
				$res_get_history = mysqli_query($conn,$sql_get_history);
				{
					while($row_get_history = mysqli_fetch_object($res_get_history))
					{
						$log = $row_get_history->log;
						$status = $row_get_history->status;
						$comments = $row_get_history->comments;
						$crm_name = $row_get_history->crm_name;
					?>
					<tr>
					<td style="width: 116px;"><?php echo date('d-m-Y',strtotime($log));?></td>
					<td><?php echo  $status." - ".$comments;?></td>
					<td><?php echo $crm_name;?></td>
					</tr>
					<?php
					}
				}
				?>
				<tr>
				<td style="width: 116px;"><?php echo date('d-m-Y',strtotime($ticket_date))?></td>
				<td>Ticket Raised</td>
				<td><?php echo $crm_create_name;?></td>
				</tr>
			</tbody>
		</table>
    </div>
</div>
<div align="center" style="margin-left:30px;border:2px solid #708090; border-radius:8px; width:350px;height:475px; padding:20px; margin-top:18px; float:left;">

     <div  id="division">
    <table class="table borderless" style=" border-collapse: collapse;">
    <tr><td><strong>Ticket Id</strong></td><td><?php echo $ticket_id; ?></td></tr>
    <tr><td><strong>Ticket Date</strong></td><td><?php echo date('d-m-Y',strtotime($ticket_date)); ?></td></tr>
	<tr><td><strong>Issue Faced</strong></td><td><?php echo $issue_faced; ?></td></tr>
	<tr><td><strong>Ticket Status</strong></td><td><?php echo $ticket_status; ?></td></tr>
	<?php
	if($ticket_status!='Yet to Contact')
	{
	?>
		<tr><td><strong>Last Contact Date</strong></td><td><?php echo date('d-m-Y',strtotime($contact_date)); ?></td></tr>
	<?php	
	}
	if($ticket_status!='Resolved')
	{
	?>
		<tr><td><strong>FollowUp Date</strong></td><td><?php if($followup_date == '0000-00-00 00:00:00'){ echo 'Not Updated';}else{ echo date('d-m-Y',strtotime($followup_date));} ?></td></tr>
	<?php	
	}
	?>
    </table>
    </div>

    </div>
    <?php
	if($ticket_status!='Resolved')
	{
	?>
    <div align="center" style="margin-top:60px;">
       <div style="margin-top:550px;">
      <button type="button" class="btn btn-lg" data-toggle="modal" data-target="#editStatus" style="background-color:#4CB192;margin-right:30px;"><i class="fa fa-check-square" aria-hidden="true"></i>&nbsp;&nbsp;Edit Status</button>   
    </div>
	</div>
	<?php
	}
	?>

<!-- Modal -->
<div class="modal fade" id="editStatus" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content" style="border-radius:15px;">
	<div class="modal-header" style="background-color:#009688;border-top-right-radius:15px;border-top-left-radius:15px;">
	  <button type="button" class="close" data-dismiss="modal" style="font-size:36px;">&times;</button>
	  <strong><h4 class="modal-title" style="text-align:center;color:white;">Update Ticket</h4></strong>
	</div>
	<form method="get" action="update_ticket.php">
	<div class="modal-body">
	  <div class="row">
		<div class="col-xs-3 col-lg-offset-2 form-group">
		  <label style="padding-top:5px;" >Ticket Status</label>
		</div>
		<div class="col-xs-6 form-group">
		<select class="form-control" id="status" name="status" required>
		  <option value="" selected>Select Status</option>
		  <option value="Ongoing" >Ongoing</option>
		  <option value="Resolved" >Resolved</option>
		  <option value="Not Able to Resolve" >Not Able to Resolve</option>
		</select>
		</div>
	  </div>
	  <div class="row" id="comments">
		<div class="col-xs-3 col-lg-offset-2 form-group">
		  <label style="padding-top:5px;" >Comments</label>
		</div>
		<div class="col-xs-6">
			<textarea name="comments" id="comments" class="form-control" rows="4" cols="40" placeholder="Comments..." style="width:270px;"></textarea>
		</div>
	  </div>
	  <br>
	  <div class="row" id="closureDate">
		<div class="col-xs-3 col-lg-offset-2 form-group">
		  <label style="padding-top:5px;" >Closure Date</label>
		</div>
		<div class="col-xs-4">
		  <div class="form-group" style="margin-right:10px;">
			<input type="text" name="closure_date" class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" />
		  </div>
		</div>
	  </div>
	  <script>
		$("#status").on('change',function(){
			var status = $('#status').val();
			if(status == 'Resolved')
			{
				$("#followupDate").hide();
			}
			else
			{
				$("#followupDate").show();
			}
		});
	  </script>
	  <div class="row" id="followupDate" >
		<div class="col-xs-3 col-lg-offset-2 form-group">
		  <label style="padding-top:5px;" >FollowUp Date</label>
		</div>
		<div class="col-xs-4">
		  <div class="form-group" style="margin-right:10px;">
			<input type="text" name="followup_date" class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" />
		  </div>
		</div>
	  </div>
	</div>
	<div class="modal-footer">
	  <input type="hidden" id="ticket_id" name="ticket_id" value="<?php echo $ticket_id; ?>">
	  <center><input type="submit" class="btn btn-success" value="Update!"/></center>
	</div>
	</form>
  </div>
  
</div>
</div>


<!-- jQuery library -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script>
var date = new Date();
date.setDate(date.getDate());

$('.datepicker').datepicker({
   autoclose: true,
    startDate: date
});
$('input.datepicker').datepicker('setDate', 'today');
</script>
<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</body>
</html>

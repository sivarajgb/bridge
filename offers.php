<?php
require_once('include/config.php');
$param = mysqli_real_escape_string($con, $_GET['param']);
$city = mysqli_real_escape_string($con, $_GET['city']);

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

	$utm_campaign = $_GET['utm_campaign'];
	$utm_source = $_GET['utm_source'];
	$utm_medium = $_GET['utm_medium'];

$offer_url = $param;

$sql = "SELECT * FROM `offers_content` WHERE `page_url` LIKE '$offer_url'";

$result = mysqli_query($con, $sql);
$row = mysqli_fetch_assoc($result);

$vehicle_type = $row['vehicle_type'];
$image_page = $row['image_page'];
$service_type = $row['service_type'];
$title = $row['title_page'];
$city = $row['city'];
$description = $row['description'];
$bullets = $row['bullets'];
$usp_bullets = $row['usp_bullets'];
$svg_path = $row['svg_path'];
$booking_title = $row['booking_title'];
$mobile_link = $row['mobile_link'];
?>
<!DOCTYPE html>
<!--[if IE 8]><html class="ie ie8"> <![endif]-->
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html> <!--<![endif]-->
<head>

	<title><?php echo $title; ?> in <?php echo $city; ?> | GoBumpr</title>
	<meta property="og:title" content="Best <?php echo $title;?> Service Centers in <?php echo $city; ?> | GoBumpr">
	<title><?php echo $title; ?> in <?php echo $city; ?> | GoBumpr</title>
	<meta property="og:title" content="<?php echo $title; ?> in <?php echo $city; ?> | GoBumpr">
	<meta name="description" content="<?php echo $title;?> Service Centers in <?php echo $city; ?>. | GoBumpr">
	<meta property="og:description" content="<?php echo $title;?> Service Centers in <?php echo $city; ?>. | GoBumpr">
	<meta property="og:url" content="https://gobumpr.com/<?php echo $city;?>/offer/"<?php echo $param;?>>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:site_name" content="GoBumpr">
	<meta property="og:site" content="https://gobumpr.com">
	<meta property="og:type" content="website">
    <meta name="author" content="GoBumpr-Team">

    <!-- Favicons-->
    <link rel="shortcut icon" href="https://static.gobumpr.com/img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="https://static.gobumpr.com/img/gobumpr/home_page/GoBumpr_57.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="https://static.gobumpr.com/img/gobumpr/home_page/GoBumpr_72.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="https://static.gobumpr.com/img/gobumpr/home_page/GoBumpr_114.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="https://static.gobumpr.com/img/gobumpr/home_page/GoBumpr_144.png">

    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->


<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-1', 'auto');
  ga('send', 'pageview');

</script>


<style type="text/css">
@charset "UTF-8";.intro_title,.short_info i,img{vertical-align:middle}.container::after,.container:after,.content section::after,.modal-header::after,.row::after,.row:after{clear:both}#hero,#social_footer,#social_footer ul,#social_footer ul li a,#toTop,.box_style_1 h3.inner,.btn_full,.button_intro,.feature_home,.main_title,a.button_intro,ul.list_order li span{text-align:center}.modal,a{outline:#000}body,button.close{background-repeat:initial initial}[class*=" icon-"]::before{speak:none;line-height:1;-webkit-font-smoothing:antialiased;font-family:gobumpr-icons!important;font-style:normal!important;font-weight:400!important;font-variant:normal!important;text-transform:none!important}.close,strong{font-weight:700}@font-face{font-family:gobumpr-icons;src:url(../../css/fonts/gobumpr-icons.eot?#iefix) format('embedded-opentype'),url(../../css/fonts/gobumpr-icons.woff) format('woff'),url(../../css/fonts/gobumpr-icons.ttf) format('truetype'),url(../../css/fonts/gobumpr-icons.svg#gobumpr-icons) format('svg');font-weight:400;font-style:normal}.icon-android::before{content:'\68'}.icon-car::before{content:'\69'}.icon-mobile::before{content:'\6c'}.icon-filled-star::before{content:'\77'}.content section{padding:40px 0 0;display:none;max-width:1230px;margin:0 auto}.content section::after,.content section::before{content:'';display:table}.form-control,header,nav,section{display:block}sup{vertical-align:baseline;position:relative;font-size:75%;line-height:0;top:-.5em}img{border:0}.col-xs-12,.col-xs-3,.col-xs-9{float:left}*,::after,::before{box-sizing:border-box}small{font-size:85%}.col-lg-12,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-9,.col-sm-12,.col-sm-3,.col-sm-6,.col-sm-9,.col-xs-12,.col-xs-3,.col-xs-9{position:relative;min-height:1px;padding-right:15px;padding-left:15px}.col-xs-12{width:100%}@media (min-width:768px){.col-sm-12,.col-sm-3,.col-sm-6,.col-sm-9{float:left}.col-sm-12{width:100%}.col-sm-9{width:75%}.col-sm-6{width:50%}.col-sm-3{width:25%}.form-inline .form-control{display:inline-block;vertical-align:middle}}@media (min-width:992px){.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-9{float:left}.col-md-12{width:100%}.col-md-9{width:75%}.col-md-4{width:33.33333333%}.col-md-3{width:25%}.col-md-2{width:16.66666667%}}@media (min-width:1200px){.col-lg-12{float:left;width:100%}}.fade{opacity:0}.navbar-form{padding:10px 15px;border-top-width:1px;border-top-style:solid;border-top-color:transparent;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:transparent;-webkit-box-shadow:rgba(255,255,255,.0980392) 0 1px 0 inset,rgba(255,255,255,.0980392) 0 1px 0;box-shadow:rgba(255,255,255,.0980392) 0 1px 0 inset,rgba(255,255,255,.0980392) 0 1px 0;margin:8px -15px}@media (min-width:768px){.navbar-form .form-group{display:inline-block;margin-bottom:0;vertical-align:middle}.navbar-form .form-control{display:inline-block;width:auto;vertical-align:middle}.navbar-form{width:auto;padding-top:0;padding-bottom:0;margin-right:0;margin-left:0;border:0;-webkit-box-shadow:none;box-shadow:none}}@media (max-width:767px){.navbar-form .form-group{margin-bottom:5px}.navbar-form .form-group:last-child{margin-bottom:0}}.close{float:right;font-size:21px;line-height:1;color:#000;text-shadow:#fff 0 1px 0;opacity:.2}.modal{overflow:hidden;top:0;right:0;bottom:0;left:0;position:fixed;z-index:1050;display:none}.main-menu,.modal-body,.modal-content,.modal-dialog{position:relative}button.close{-webkit-appearance:none;padding:0;border:0;background-position:0 0}.btn_1,.button_intro,a.button_intro,body{background-position:initial initial}.modal.fade .modal-dialog{-webkit-transform:translate(0,-25%)}.modal-dialog{width:auto;margin:10px}.modal-content{-webkit-background-clip:padding-box;background-color:#fff;background-clip:padding-box;border:1px solid rgba(0,0,0,.2);outline:#000;-webkit-box-shadow:rgba(0,0,0,.498039) 0 3px 9px;box-shadow:rgba(0,0,0,.498039) 0 3px 9px;border-radius:6px}.modal-header{padding:15px;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#e5e5e5}.modal-header .close{margin-top:-2px}.modal-title{margin:0;line-height:1.42857143}.modal-body{padding:15px}@media (min-width:768px){.modal-dialog{width:600px;margin:30px auto}.modal-content{-webkit-box-shadow:rgba(0,0,0,.498039) 0 5px 15px;box-shadow:rgba(0,0,0,.498039) 0 5px 15px}}.container::after,.container::before,.modal-header::after,.modal-header::before,.row::after,.row::before{display:table;content:' '}@media (max-width:991px) and (min-width:768px){.hidden-sm{display:none!important}}@media (max-width:1199px) and (min-width:992px){.hidden-md{display:none!important}}#header_menu,.layer{display:none}.animated{-webkit-animation:1s both}.main-menu{z-index:9;width:auto}.layer,header{position:fixed;top:0}.header-form{box-shadow:none!important}.layer{left:0;width:100%;min-width:100%;min-height:100%;background-color:#000;opacity:0;z-index:9999}@media only screen and (min-width:992px){.main-menu{width:auto}}@media only screen and (max-width:991px){#header_menu{position:relative;text-align:center;padding:25px 15px 10px;display:block}.main-menu{overflow:auto;left:-100%;bottom:0;width:55%;height:100%;opacity:0;position:fixed;background-color:#fff;z-index:999999;-webkit-box-shadow:rgba(50,50,50,.54902) 1px 0 5px 0;box-shadow:rgba(50,50,50,.54902) 1px 0 5px 0}}.img_container,.sr-only,carousel-inner{overflow:hidden}.ui-autocomplete,body,html{overflow-x:hidden}@media only screen and (max-width:480px){.main-menu{width:100%;margin-bottom:20px!important}}.main_title p{font-family:Lato,Arial,sans-serif;font-weight:300;font-size:20px}.btn_1,.button_intro,a.button_intro{font-family:inherit;background-color:#ffa800;outline:#000;font-size:12px;background-repeat:initial initial;display:inline-block;font-weight:700;text-transform:uppercase}.tour_container,body{background-color:#fff}.button_intro,a.button_intro{text-align:center;border:none;padding:8px 25px;min-width:150px;color:#fff!important;border-radius:3px}#hero,.btn_1,footer a,ul#top_tools a{color:#fff}p{margin:0 0 20px}.btn_1{border:none;padding:7px 20px;border-radius:3px}#hero,.intro_title h3{text-transform:uppercase}#hero{position:relative;background-image:url(https://static.gobumpr.com/img/car.jpg);background-color:#4d536d;background-size:cover;width:100%;font-size:16px;display:table;z-index:99;background-position:50% 50%;background-repeat:no-repeat no-repeat}.intro_title{display:table-cell}.intro_title h3{font-size:45px;margin-bottom:5px;color:#fff;font-weight:700}#toTop:before,.short_info i{font-style:normal;font-weight:400}.rating{font-size:18px}.rating small{font-size:12px;color:#ccc}.rating .voted{color:#f90}.modal-dialog{margin-top:80px}.margin_30{margin-top:30px;margin-bottom:30px}.tour_container{-webkit-box-shadow:rgba(0,0,0,.0980392) 0 0 5px 0;box-shadow:rgba(0,0,0,.0980392) 0 0 5px 0;margin:0 0 30px}.img_container{position:relative;border:1px solid #fff}.tour_container .tour_title{padding:15px;position:relative}.tour_container .tour_title .rating{font-size:14px;margin-left:-3px}.tour_container .tour_title h3{margin:0;font-size:18px;text-transform:uppercase}.img_container img{-webkit-transform:scale(1.2)}.short_info{position:absolute;left:0;bottom:0;background-image:url(../img/shadow_tour.png);width:100%;padding:10px 10px 8px 5px;color:#fff;background-position:0 100%;background-repeat:repeat no-repeat}.ribbon.popular,.ribbon.top_rated{background-position:initial initial;background-repeat:no-repeat no-repeat}.short_info i{font-size:25px;display:inline-block;padding:0;margin:0}.short_info .price{float:right;font-size:28px;font-weight:700;display:inline-block}.short_info .price sup{font-size:18px;position:relative;top:-5px}.ribbon{position:absolute;top:0;left:-1px;width:78px;height:78px;z-index:1}.ribbon.popular{background-image:url(../img/ribbon_popular.png)}.ribbon.top_rated{background-image:url(../img/ribbon_top_rated.png)}@media (max-width:767px){.main_title{font-size:14px}.main_title h2{font-size:24px}.main_title p{font-size:16px}.margin_30{margin-top:15px;margin-bottom:15px}#hero{height:300px;font-size:12px}.intro_title h3{font-size:26px;margin-bottom:5px;color:#fff;font-weight:700;text-transform:uppercase}}@media (max-width:480px){#hero{height:200px}.intro_title{display:none}ul#top_tools{margin:0 25px 0 0}}body{margin:0}.col-xs-3,.col-xs-9{float:left}html{font-family:sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}aside,header,nav,section{display:block}a{background-color:transparent;color:#ffa800;text-decoration:none}h1{margin:.67em 0}h1,h2,h3,nav{margin-top:20px}textarea{overflow:auto}button,input,select,textarea{margin:0;font:inherit;color:inherit}button{overflow:visible;-webkit-appearance:button}button,select{text-transform:none}button::-moz-focus-inner,input::-moz-focus-inner{padding:0;border:0}*,:after,:before{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}html{font-size:10px}button,input,select,textarea{font-family:inherit;font-size:inherit;line-height:inherit}h1,h2,h3,h4{margin-bottom:10px;font-family:inherit;font-weight:500;line-height:1.1}.btn_full,.main_title h2,.parallax-content-2 div h1{text-transform:uppercase;font-weight:700}h1{font-size:36px}h2{font-size:30px}h4{margin-top:10px;font-size:18px}.btn_full,body{font-size:12px}ul{margin-top:0}@media (min-width:768px){.container{width:750px}}.container{margin-right:auto;margin-left:auto;padding-right:15px;padding-left:15px}@media (min-width:992px){.container{width:970px}}@media (min-width:1200px){.container{width:1170px}}.row{margin-right:-15px;margin-left:-15px}.col-md-10,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-6,.col-md-9,.col-sm-10,.col-sm-12,.col-sm-3,.col-sm-4,.col-sm-6,.col-sm-9,.col-xs-3,.col-xs-9{position:relative;min-height:1px;padding-right:15px;padding-left:15px}.col-xs-9{width:75%}.col-xs-3{width:25%}@media (min-width:768px){.col-sm-10,.col-sm-12,.col-sm-3,.col-sm-4,.col-sm-6,
.col-sm-9{float:left}.col-sm-12{width:100%}.col-sm-10{width:83.33333333%}.col-sm-9{width:75%}.col-sm-6{width:50%}.col-sm-4{width:33.33333333%}.col-sm-3{width:25%}}@media (min-width:992px){.col-md-10,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-6,.col-md-9{float:left}.col-md-12{width:100%}.col-md-10{width:83.33333333%}.col-md-9{width:75%}.col-md-6{width:50%}.col-md-4{width:33.33333333%}.col-md-3{width:25%}.col-md-2{width:16.66666667%}}.btn_full,.form-control{width:100%;display:block}label{display:inline-block;max-width:100%;margin-bottom:5px}.form-control{background-image:none;line-height:1.42857143;padding:6px 12px;border:1px solid #ccc}#toTop,body{line-height:20px}.form-control::-moz-placeholder{color:#999;opacity:1}.form-control:-ms-input-placeholder{color:#999}.form-control::-webkit-input-placeholder{color:#999}.form-control::-ms-expand{background-color:transparent;border:0}textarea.form-control{height:auto}.form-group{margin-bottom:15px}.container:after,.container:before,.row:after,.row:before{display:table;content:" "}@-ms-viewport{width:device-width}@media (min-width:1200px){.hidden-lg{display:none!important}}@media (max-width:767px){.hidden-xs{display:none!important}}@media (min-width:768px) and (max-width:991px){.hidden-sm{display:none!important}}@media (min-width:992px) and (max-width:1199px){.hidden-md{display:none!important}}body{font-family:Montserrat,Arial,sans-serif!important;background:#fff;color:#565a5c}body,h1,h2,h3,h4{-webkit-font-smoothing:antialiased}h1,h2,h3,h4{color:#333}h3{font-size:22px}.main_title{font-size:16px;margin-bottom:30px}.main_title h2{letter-spacing:-1px;font-size:30px;margin-bottom:0;margin-top:0}h2 span{color:#ffa800}.box_style_1 h3.inner,.btn_full,footer a,ul#top_tools a{color:#fff}a{color:#ffa80;outline:0}.btn_full{font-family:inherit;outline:0;padding:12px 20px;border:none;background:#ffa800;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;margin-bottom:10px;cursor:pointer}#form_div,#img_div,#social_footer ul li,ul#top_tools li{display:inline-block}header{position:fixed;left:0;top:0;width:100%;z-index:99999;padding:10px 0}ul#top_tools{flist-style:none;padding:0;position:absolute;right:15px;top:0;z-index:999}ul#top_tools li{padding:0 10px}footer ul{list-style:none;margin:0;padding:0 0 20px}#social_footer ul{margin:0;padding:0 0 10px}#social_footer ul li{margin:0 5px 10px}#social_footer ul li a{color:#fff;display:block;font-size:16px;width:35px;height:35px;border:1px solid rgba(255,255,255,.3);-webkit-border-radius:50%;-moz-border-radius:50%;border-radius:50%;line-height:36px!important}.feature_home{-webkit-box-shadow:0 0 5px 0 rgba(0,0,0,.1);position:relative;padding:30px;background:#fff;margin-bottom:30px;color:#888;-moz-box-shadow:0 0 5px 0 rgba(0,0,0,.1);box-shadow:0 0 5px 0 rgba(0,0,0,.1)}.parallax-content-2{position:absolute;left:0;bottom:0;z-index:999;padding:18px 0 20px;color:#333;font-size:13px;background:url(../img/shadow_single.png) bottom left repeat-x;width:100%}.parallax-content-2 div h1{font-size:36px;color:#333;margin:0}#toTop:before{content:"\45"!important;font-family:gobumpr-icons!important}.box_style_1{background:#fff;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;border:1px solid #ddd;margin-bottom:25px;padding:30px;position:relative;color:#666}#toTop{right:10px;width:40px;height:40px;background-color:rgba(0,0,0,.6);padding:10px;position:fixed;bottom:10px;display:none;color:#fff;font-size:20px}.margin_60{padding-top:60px;padding-bottom:60px}.parallax-window{background:0 0;position:relative}.dd-pointer{width:0;height:0;position:absolute;right:10px;top:50%;margin-top:-3px}.dd-pointer-down{border:5px solid transparent;border-top:solid 5px #999!important}input[type=email]{text-transform:none!important}input#registration_num_book{text-transform:uppercase!important}.form-control{-webkit-box-shadow:none;box-shadow:none;-webkit-appearance:none;padding-left:8px;background-color:#fff}label{font-weight:600;line-height:14px}[class*=" icon-"]:before,[data-icon]:before{font-family:gobumpr-icons!important;font-style:normal!important;font-weight:400!important;font-variant:normal!important;text-transform:none!important;speak:none;line-height:1;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}@media (max-width:767px){.main_title{font-size:14px}.main_title h2{font-size:24px}.margin_60{padding-top:30px;padding-bottom:30px}.parallax-window{height:240px;min-height:240px}.parallax-content-2{padding-bottom:15px}.parallax-content-2 div h1{font-size:22px}}@media (max-width:480px){.box_style_1{padding:15px}.box_style_1 h3.inner{margin:-15px -15px 15px}ul#top_tools{margin:0 25px 0 0}}.parallax-window{min-height:130px!important}@media only screen and (max-width:768px){.parallax-window{min-height:100px!important;height:130px!important;margin-top:20px}#main_div{width:100%}}.parallax-content-2{padding-bottom:0!important}@media only screen and (max-width:480px){.parallax-window{max-height:30%;min-height:30%}}#main_div{width:100%;display:flex;max-height:600px}#img_div{width:73.5%; min-height:16em;}#form_div{width:26.5%;min-height:300px}@media only screen and (max-width:998px){#form_div,#img_div,#main_div{width:100%;display:inline-block}#img_div{height:100%}}#logo_home span{margin:10px 0 0;padding:0}#logo_home span a{width:160px;height:34px;display:block;background-size:160px 34px;text-indent:-9999px;background-position:0 0;background-repeat:no-repeat no-repeat;background-image:url(https://static.gobumpr.com/img/gobumpr/logo/2.png)!important}.form-control{font-size:12px;color:#333;height:40px;border-radius:3px}input{text-transform:capitalize!important}@media (max-width:991px){ul#top_tools{margin:3px 45px 0 0}nav{margin-top:15px}}@media (max-width:480px){ul#top_tools{margin:0 25px 0 0}}@media only screen and (max-width:991px){#dropdown_icon{margin-top:3%!important}}.ui-autocomplete{z-index:1000!important;list-style:none;overflow-y:auto}ul{margin-bottom:0}@font-face{font-family:gobumpr-icons;src:url(https://gobumpr.com/css/fonts/gobumpr-icons.eot);src:url(https://gobumpr.com/css/css/fonts/gobumpr-icons.eot?#iefix#new_stuff) format("embedded-opentype"),url(https://gobumpr.com/css/fonts/gobumpr-icons.woff#new_stuff) format("woff"),url(https://gobumpr.com/css/fonts/gobumpr-icons.ttf#new_stuff#new_stuff) format("truetype"),url(https://gobumpr.com/css/fonts/gobumpr-icons.svg#new_stuff) format("svg");font-weight:400;font-style:normal}[data-icon]:before{content:attr(data-icon)}.icon-email:before{content:"\67"}.icon-android:before{content:"\68"}.icon-blog:before{content:"\6b"}.icon-mobile:before{content:"\6c"}.icon-calendar:before{content:"\76"}.icon-since:before{content:"\79"}.icon-24x7-phone:before{content:"\44"}.box_style_1 h3.inner{margin:-30px -30px 20px;background-color:#565a5c;padding:10px 20px 10px 18px;color:#fff;border:1px solid #fff;-webkit-border-top-left-radius:3px;-webkit-border-top-right-radius:3px;-moz-border-radius-topleft:3px;-moz-border-radius-topright:3px;border-top-left-radius:3px;border-top-right-radius:3px}#hero{height:400px;background:url(../img/car.jpg) center center no-repeat #4d536d!important}@media (max-width:767px){.main_title{font-size:14px}.main_title h2{font-size:24px}.margin_60{margin-top:20px!important}}strong#js-rotating span{color:#ffa800}.img_container{height:233px!important}button:disabled,button[disabled]{opacity:.65;cursor:not-allowed}ul.list_order{margin:0 0 30px;padding:0;line-height:30px;font-size:14px;list-style:none}ul.list_order li{position:relative;padding-left:40px;margin-bottom:10px}ul.list_order li span{background-color:#ffa800;color:#fff;position:absolute;left:0;top:0;font-size:18px;-webkit-border-radius:50%;-moz-border-radius:50%;border-radius:50%;width:30px;height:30px;line-height:30px}label.error{font-size:11px;position:relative;float:right;top:-75px;right:-30px;z-index:99;height:25px;line-height:25px;background-color:#e34f4f;color:#fff!important;font-weight:400;padding:0 6px}label.error:after{content:'';position:absolute;border-style:solid;border-width:0 6px 6px 0;border-color:transparent #e34f4f;display:block;width:0;z-index:1;bottom:-6px;left:20%}.glyphicon-chevron-right:before{content:"\4e"!important;font-family:gobumpr-icons!important}.glyphicon-chevron-left:before{content:"\48"!important;font-family:gobumpr-icons!important}@media only screen and (max-width:780px){#main_div{margin-top:55px!important}}#img_div{background-color:#ddd}.carousel-inner>.item>a>img,.carousel-inner>.item>img,.img-responsive,.thumbnail a>img,.thumbnail>img{display:block;max-width:100%;height:auto}.carousel-caption,.carousel-control{color:#fff;text-align:center;text-shadow:0 1px 2px rgba(0,0,0,.6)}.carousel,.carousel-inner{position:relative}.carousel-inner{width:100%}.carousel-inner>.item{position:relative;display:none;-webkit-transition:.6s ease-in-out left;-o-transition:.6s ease-in-out left;transition:.6s ease-in-out left}.carousel-inner>.item>a>img,.carousel-inner>.item>img{line-height:1}@media all and (transform-3d),(-webkit-transform-3d){.carousel-inner>.item{-webkit-transition:-webkit-transform .6s ease-in-out;-o-transition:-o-transform .6s ease-in-out;transition:transform .6s ease-in-out;-webkit-backface-visibility:hidden;backface-visibility:hidden;-webkit-perspective:1000px;perspective:1000px}.carousel-inner>.item.active.right,.carousel-inner>.item.next{left:0;-webkit-transform:translate3d(100%,0,0);transform:translate3d(100%,0,0)}.carousel-inner>.item.active.left,.carousel-inner>.item.prev{left:0;-webkit-transform:translate3d(-100%,0,0);transform:translate3d(-100%,0,0)}.carousel-inner>.item.active,.carousel-inner>.item.next.left,.carousel-inner>.item.prev.right{left:0;-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}}.carousel-inner>.active,.carousel-inner>.next,.carousel-inner>.prev{display:block}.carousel-inner>.active{left:0}.carousel-inner>.next,.carousel-inner>.prev{position:absolute;top:0;width:100%}.carousel-inner>.next{left:100%}.carousel-inner>.prev{left:-100%}
.carousel-inner>.next.left,.carousel-inner>.prev.right{left:0}.carousel-inner>.active.left{left:-100%}.carousel-inner>.active.right{left:100%}.carousel-control{filter:alpha(opacity=50);opacity:.5;position:absolute;top:0;bottom:0;left:0;width:15%;font-size:20px;background-color:rgba(0,0,0,0)}.carousel-control.left{background-image:-webkit-linear-gradient(left,rgba(0,0,0,.5) 0,rgba(0,0,0,.0001) 100%);background-image:-o-linear-gradient(left,rgba(0,0,0,.5) 0,rgba(0,0,0,.0001) 100%);background-image:-webkit-gradient(linear,left top,right top,from(rgba(0,0,0,.5)),to(rgba(0,0,0,.0001)));background-image:linear-gradient(to right,rgba(0,0,0,.5) 0,rgba(0,0,0,.0001) 100%);filter:progid: DXImageTransform.Microsoft.gradient(startColorstr='#80000000', endColorstr='#00000000', GradientType=1);background-repeat:repeat-x}.carousel-control.right{right:0;left:auto;background-image:-webkit-linear-gradient(left,rgba(0,0,0,.0001) 0,rgba(0,0,0,.5) 100%);background-image:-o-linear-gradient(left,rgba(0,0,0,.0001) 0,rgba(0,0,0,.5) 100%);background-image:-webkit-gradient(linear,left top,right top,from(rgba(0,0,0,.0001)),to(rgba(0,0,0,.5)));background-image:linear-gradient(to right,rgba(0,0,0,.0001) 0,rgba(0,0,0,.5) 100%);filter:progid: DXImageTransform.Microsoft.gradient(startColorstr='#00000000', endColorstr='#80000000', GradientType=1);background-repeat:repeat-x}.carousel-control:focus,.carousel-control:hover{color:#fff;text-decoration:none;filter:alpha(opacity=90);outline:0;opacity:.9}.carousel-control .glyphicon-chevron-left,.carousel-control .glyphicon-chevron-right,.carousel-control .icon-next,.carousel-control .icon-prev{position:absolute;top:50%;z-index:5;display:inline-block;margin-top:-10px}.carousel-control .glyphicon-chevron-left,.carousel-control .icon-prev{left:50%;margin-left:-10px}.carousel-control .glyphicon-chevron-right,.carousel-control .icon-next{right:50%;margin-right:-10px}.carousel-control .icon-next,.carousel-control .icon-prev{width:20px;height:20px;font-family:serif;line-height:1}.carousel-control .icon-prev:before{content:'\2039'}.carousel-control .icon-next:before{content:'\203a'}.carousel-indicators{position:absolute;bottom:10px;left:50%;z-index:15;width:60%;padding-left:0;margin-left:-30%;text-align:center;list-style:none}.carousel-indicators li{list-style:none;display:inline-block;width:10px;height:10px;margin:1px;text-indent:-999px;cursor:pointer;background-color:#000\9;background-color:rgba(0,0,0,0);border:1px solid #fff;border-radius:10px}.carousel-indicators .active{width:12px;height:12px;margin:0;background-color:#fff}.carousel-caption{position:absolute;right:15%;bottom:20px;left:15%;z-index:10;padding-top:20px;padding-bottom:20px}.carousel-caption .btn,.text-hide{text-shadow:none}@media screen and (min-width:768px){.carousel-control .glyphicon-chevron-left,.carousel-control .glyphicon-chevron-right,.carousel-control .icon-next,.carousel-control .icon-prev{width:30px;height:30px;margin-top:-10px;font-size:30px}.carousel-control .glyphicon-chevron-left,.carousel-control .icon-prev{margin-left:-10px}.carousel-control .glyphicon-chevron-right,.carousel-control .icon-next{margin-right:-10px}.carousel-caption{right:20%;left:20%;padding-bottom:30px}.carousel-indicators{bottom:20px}}.sr-only{position:absolute;width:1px;height:1px;padding:0;margin:-1px;clip:rect(0,0,0,0);border:0}.sr-only-focusable:active,.sr-only-focusable:focus{position:static;width:auto;height:auto;margin:0;overflow:visible;clip:auto}#myCarousel{overflow:hidden}.img-circle{border-radius:50%}
.usp_list li{
    font-size: 20px;
    list-style: none;
    text-align: left;
    min-height: 100px;
    margin-bottom: 1em;
}
.usp_list li:before{
    content:"";
}
.svg_img{
    width:5em;
    height:5em;
    margin-right: 14px;
}
@media (max-width:500px){
    .usp_list li{
        font-size:14px;
    }
    .usp_list {
        margin-left:-20px;
    }
}
.usp_list{
    display: table;
}
.usp_bullets{
    display:table-cell;
    padding-top: 25px;
}
<?php
    $svg_path_array = explode(";", $svg_path);

    foreach($svg_path_array as $index=>$url){
?>
.usp_list li:nth-child(<?php echo $index+1; ?>):before{
/*list-style-image:url(<?php echo $url ?>);*/
background:url(<?php echo $url ?>);
display: table-cell;
margin-right: 1em;
width:100px;
height:100px;
float: left;
}
<?php
    }//for
?>

<style>
</style>
<?php
?>
</head>
<body style="overflow-x: hidden;">

<!--[if lte IE 8]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="///browsehappy.com/">upgrade your browser</a>.</p>
<![endif]-->


	<div class="layer"></div>
    <!-- Mobile menu overlay mask -->

     <!-- Header================================================== -->
    <header style="position:absolute">

        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div id="logo_home">
                    	<span><a href="/"></a></span>
                    </div>
                </div>
                <nav class="col-md-9 col-sm-9 col-xs-9">

                    <div class="main-menu">
                        <div id="header_menu">
                            <img src="/img/gobumpr/logo/3.png" width="160" height="34" alt="GoBumpr logo" data-retina="true">
                        </div>
                    </div><!-- End main-menu -->
						<a class="hidden-lg hidden-md" style="float:right; margin-top:-6px; color:#000;" href="tel://<?php echo $mobile_link=='' ? "09003251754" : $mobile_link; ?>"><i class="icon icon-24x7-phone" style="font-size:22px; color:#ffa800"></i></a>
                    <ul id="top_tools" style="margin-top:-12px;" class="hidden-sm hidden-xs">
                    <li>
						<a href="https://play.google.com/store/apps/details?id=com.northerly.bumpr" style="color:#000;" target="_blank"><i class="icon icon-mobile" style="font-size:28px;vertical-align:-webkit-baseline-middle;vertical-align:-moz-middle-with-baseline;"></i>DOWNLOAD APP</a>
                    </li>
                    <li>
						<a style="color:#000;" href="tel://<?php echo $mobile_link=='' ? "09003251754" : $mobile_link; ?>"><i class="icon icon-24x7-phone" style="font-size:28px;vertical-align:-webkit-baseline-middle;vertical-align:-moz-middle-with-baseline;"></i> <?php echo $mobile_link=='' ? "09003251754" : $mobile_link; ?></a>
                    </li>
                    </ul>
                </nav>
            </div>
        </div><!-- container -->
    </header><!-- End Header -->

<div id="main_div" style="margin-top:70px;">
	<div id="img_div">
    <?php
    if (strpos($image_page, ';') === false) {?>
        <img src="<?php echo $image_page; ?>" alt="<?php echo $title;?>" style="width:100%;">
    <?php
    }//no carousel
    else{
        $img_array = explode(";", $image_page);
        ?>
    <div id="myCarousel" class="carousel slide" data-ride="carousel" style="display:none">
        <!-- Indicators -->
        <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">

        <div class="item active">
            <img src="<?php echo $img_array[0]; ?>" alt="<?php echo $title;?>" style="width:100%;">
        </div>
        <?php
                array_splice($img_array, 0, 1);
                array_values($img_array);
        foreach($img_array as $image_page_val){ ?>
            <div class="item">
                <img src="<?php echo $image_page_val; ?>" alt="<?php echo $title;?>" style="width:100%;">
            </div>
         <?php
        }//foreach
        ?>
        </div>
        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
        </a>
  </div>
        <?php
    }//carousel
    ?>

     </div>

	<div id="form_div">
        <div class="hidden-xs hidden-sm">
        <aside>
        <div class="theiaStickySidebar">
        <div class="box_style_1" style="min-height:460px; margin:0px !important; border: 0px;">

        	<div class="1st_fold">
                <h3 class="inner">- Avail Offer -</h3>
                <div class="entry_text">
                   <div style="top:10px; margin:0; width:100%;">
                        <input type="hidden" id="VehicleType_id" name="VehicleType" value="<?php echo $vehicle_type; ?>">
                        <div align="center" style="font-size:16px; text-align: center; margin-bottom:25px; color:#000">
                            <?php echo $description; ?>
                        </div>
                        <ul style="font-size:14px; margin-left:-6%;margin-top:10px; color:#4E5665">
                    <?php
                    $bullets_array = explode(";", $bullets);
                    foreach($bullets_array as $val) {
                        echo "<li>".$val."</li>";
                    }
                    ?>
                        </ul>
                    </div>
                        <br>
                        <center><button type="submit" class="btn_full entry_submit"> Book Now </button></center>
                </div>
            </div>

        <div class="row" id="2nd_fold" style="display:none; min-height:300px;">
            <div class="col-md-12">
            <form id="mobile_offer" method="post" action="">
               <div class="col-md-12 col-sm-12" style="margin-top:25%; margin-bottom:25%;">
                 <div class="form-group">
                      <label>Vehicle Model</label>
                      <input class="ui-autocomplete form-control" type="text" name="brand_model" id="brand_model_mob" required placeholder="Select Model">
                      <label>Locality</label>
                      <input class="ui-autocomplete form-control" type="text" name="locality" id="locality_mob" required placeholder="Select Locality">
                      <label>Mobile Number</label>
                      <input class="form-control" type="tel" name="mobile" id="mobile" maxlength="10" required placeholder="Enter Mobile Number">
                <?php
                require_once 'detector/detect.php';

                $device_type = Detect::deviceType();
                $ip_address = Detect::ip();
                $host_ip = Detect::ipHostname();
                $ip_org = Detect::ipOrg();
                $ip_country = Detect::ipCountry();
                $os = Detect::os();
                $browser = Detect::browser();
                $user_brand = Detect::brand();


                $user_agent = "type: ".$device_type.", IP: ".$ip_address.", IP Host: ".$host_ip.", IP Org: ".$ip_org.", IP Country: ".$ip_country.", OS: ".$os.", Browser: ".$browser.", User Brand: ".$user_brand;
                ?>
                <input type="hidden" type="text" value="<?php echo $city;?>" name="city" id="city">
                <input type="hidden" type="text" value="<?php echo $user_agent?>" name="user_agent" id="user_agent">
                <input type="hidden" type="text" value="<?php echo $service_type?>" name="offer_name" id="offer_name">
                <input type="hidden" type="text" value="<?php echo $vehicle_type?>" name="vehicle_type">
                <input type="hidden" type="text" value="<?php echo $utm_campaign?>" name="utm_campaign" id="utm_campaign">
                <input type="hidden" type="text" value="<?php echo $utm_medium?>" name="utm_medium" id="utm_medium">
                <input type="hidden" type="text" value="<?php echo $utm_source?>" name="utm_source" id="utm_source">
                </div>
               </div><br>

            <center><button type="submit" class="btn_full" id="offer_submit"> Book Now </button></center>
            </form>
            </div>
        </div>
        <div class="row" id="thankyou" style="display:none; min-height:300px;">
            <div  class="col-md-12" style="margin-top:25%; margin-bottom:25%;">
            <h4> Thank you for sharing your details with us, We will get into touch with you soon! </h4>
            </div>
        </div>
        </div>
        </div>
        </aside>
        </div>
        <div class="hidden-lg hidden-md">
        <aside>
        <div class="theiaStickySidebar">
        <div class="box_style_1" style="min-height:100px; border:0px !important;">

        	<div class="1st_fold">
                <h3 class="inner">- Avail Offer -</h3>
                <div class="entry_text">
                   <div style="top:10px; margin:0; width:100%;">
                        <input type="hidden" id="VehicleType_id" name="VehicleType" value="<?php echo $vehicle_type; ?>">
                        <div style="font-size:16px; text-align: center; margin-bottom:25px; color:#000" align="center">
                            <?php echo $description; ?>
                        </div>
                        <ul style="font-size:14px; margin-left:-6%;margin-top:10px; color:#4E5665">
                    <?php
                    $bullets_array = explode(";", $bullets);
                    foreach($bullets_array as $val) {
                        echo "<li>".$val."</li>";
                    }
                    ?>
                        </ul>
                    </div>
                        <br>
                        <center><button type="submit" class="btn_full entry_submit_mob"> Book Now </button></center>
                </div>
            </div>

        <div class="row" id="2nd_fold_mob" style="display:none;">
            <div  class="col-md-12">
            <form id="mobile_offer_mob" method="post" action="">
               <div class="col-md-12 col-sm-12">
                 <div class="form-group">
                      <label>Vehicle Model</label>
                      <input class="ui-autocomplete form-control" type="text" name="brand_model" id="brand_model_form" required placeholder="Select Model">
                      <label>Locality</label>
                      <input class="ui-autocomplete form-control" type="text" name="locality" id="locality_form" required placeholder="Select Locality">
                      <label>Mobile Number</label>
                      <input class="form-control" type="tel" name="mobile" id="mobile" maxlength="10" required placeholder="Enter Mobile Number">
            <?php
            require_once 'detector/detect.php';

            $device_type = Detect::deviceType();
            $ip_address = Detect::ip();
            $host_ip = Detect::ipHostname();
            $ip_org = Detect::ipOrg();
            $ip_country = Detect::ipCountry();
            $os = Detect::os();
            $browser = Detect::browser();
            $user_brand = Detect::brand();


            $user_agent = "type: ".$device_type.", IP: ".$ip_address.", IP Host: ".$host_ip.", IP Org: ".$ip_org.", IP Country: ".$ip_country.", OS: ".$os.", Browser: ".$browser.", User Brand: ".$user_brand;

            ?>
            <input type="hidden" type="text" value="<?php echo $city;?>" name="city" id="city">
            <input type="hidden" type="text" value="<?php echo $user_agent?>" name="user_agent" id="user_agent">
            <input type="hidden" type="text" value="<?php echo $service_type;?>" name="offer_name" id="offer_name">
            <input type="hidden" type="text" value="<?php echo $vehicle_type;?>" name="vehicle_type">
            <input type="hidden" type="text" value="<?php echo $utm_campaign?>" name="utm_campaign" id="utm_campaign">
            <input type="hidden" type="text" value="<?php echo $utm_medium?>" name="utm_medium" id="utm_medium">
            <input type="hidden" type="text" value="<?php echo $utm_source?>" name="utm_source" id="utm_source">
                </div>
               </div><br>

            <center><button type="submit" class="btn_full" id="offer_submit_mob"> Book Now </button></center>
            </form>
            </div>
        </div>
        </div>
        </div>
        </aside>
        </div>
    </div>
 </div>
        <span id="city_hidden" style="display:none"><?php echo $city=='' ? "chennai" : $city;?></span>
        <span id="mec_hidden" style="display:none">1</span>
        <span id="vehicle_hidden" style="display:none"><?php echo $vehicle_type;?></span>
        <span id="brand_hidden" style="display:none"><?php echo $brand_val = $offer_url=='chennai-re-bike-general-service-polish-1999-2017' || $offer_url=='royal-enfield-bike-service-and-polish-1799-2017'  || $offer_url=='royal-enfield-bike-service-and-polish-1999-2017' ? "Royal Enfield" : "";?></span>

 <div class="container margin_60">

         <div class="main_title">
            <h2 class="underline"><span>GoBumpr</span> Advantages</h2>
        </div>

    <div align="center">
    <ul class="usp_list">
            <?php
            $usp_bullets_array = explode(";", $usp_bullets);
            $usp_bullets_array = explode(";", $usp_bullets);
            /*$svg_path_array = explode(";", $svg_path);*/

            /*$list = array_combine($svg_path_array, $usp_bullets_array);*/
                foreach($usp_bullets_array as $bullet){
            ?>
            <li><span class="usp_bullets"> <?php echo $bullet; ?> </span></li>
            <?php
                }//bullets for
            ?>
        </ul>
    </div>
</div><!-- End row Laptop 6-->


<div class="container margin_60">

        <div class="main_title">
            <h2 class="underline"><span>Our Delighted</span> Customers</h2>
        </div>

        <div class="row">

            <div class="col-md-4 col-sm-4 wow zoomIn" data-wow-delay="0.2s">
                <div class="feature_home">
                    <img width="150" height="150" class="img-circle" src="https://static.gobumpr.com/img/gobumpr/home_page/aarthi.jpg" alt="GoBumpr Happy Customer - Aarthi">
                    <h3>Aarthi Parekh</h3>
                    <p style="font-size:13px;">
                      "Excellent Service! Had got both my Wego and Apache serviced through them. They did a wonderful job both in following up and get things done perfectly. Will use them in the future as well"
                    </p>
                </div>
            </div>

            <div class="col-md-4 col-sm-4 wow zoomIn" data-wow-delay="0.4s">
                <div class="feature_home">
                    <img width="150" height="150" class="img-circle" src="https://static.gobumpr.com/img/gobumpr/home_page/sathya.jpg" alt="GoBumpr Happy Customer - Sathya">
                    <h3>Sathya Anand</h3>
                    <p style="font-size:13px;">
                         "A real life saver. Awesome customer service - not just from the GoBumpr team but also from the on ground service partners. Goes to show the value they see in the platform. Highly recommended. Keep up!"
                    </p>
                </div>
            </div>

            <div class="col-md-4 col-sm-4 wow zoomIn" data-wow-delay="0.6s">
                <div class="feature_home">
                    <img width="150" height="150" class="img-circle" src="https://static.gobumpr.com/img/gobumpr/home_page/ashok.jpg" alt="GoBumpr Happy Customer - Ashok">
                    <h3>Ashok Prabhakar</h3>
                    <p style="font-size:13px;">
                         "This is what customer service means! Got my car battery replaced in a matter of hours with good deal via GoBumpr. Awesome response Good customer care. The app is user-friendly and I love it"
                    </p>
                </div>
            </div>

        </div><!--End row -->
    </div><!-- End container Testimonials 4-->

<span id="vehicle_type" style="display: none;"><?php echo $vehicle_type;?></span>

	<script async src="https://gobumpr.com/js/jquery-1.11.3.min.js" onLoad="init_funs();"></script>


<noscript id="deferred-styles">
    <!-- CSS -->
    <style>
        @charset "utf-8";.ui-autocomplete{z-index:1000 !important;cursor:default;list-style:none;}
        .ui-widget{}
        .ui-autocomplete{overflow-y:auto;overflow-x:hidden;}
        .ui-menu{width:0px;display:none;}
        .ui-autocomplete > li{padding:10px;padding-left:10px;}
        ul{margin-bottom:0;}
        .ui-autocomplete > li.ui-state-focus{background-color:#DDD;}
        .ui-autocomplete .ui-front .ui-menu .ui-widget .ui-widget-content{border:0;}
        .ui-helper-hidden-accessible{display:none;}
        .gobumpr-icon{font-style:normal;font-weight:normal;speak:none;display:inline-block;text-decoration:inherit;text-align:center;font-variant:normal;text-transform:none;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
        .ui-widget{background-color:white;width:100%;}
        .ui-widget-content{padding-left:1px;display:block;width:20px;position:relative;line-height:12px;max-height:210px;border:.5px solid #DADADA;}
    </style>
         <!-- Google web fonts -->
   <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
   <link href='//fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
   <link href='//fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'>

</noscript>
<script>
      var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("deferred-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
      };
      var raf = requestAnimationFrame || mozRequestAnimationFrame ||
          webkitRequestAnimationFrame || msRequestAnimationFrame;
      if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
      else window.addEventListener('load', loadDeferredStyles);
</script>


 <script>
function loadScript(src) {
    return new Promise(function (resolve, reject) {
        var s;
        s = document.createElement('script');
        s.src = src;
        s.onload = resolve;
        s.onerror = reject;
        document.head.appendChild(s);
    });
}
 function init_funs(){
     loadScript("https://code.jquery.com/ui/1.11.4/jquery-ui.min.js");
     loadScript("https://gobumpr.com/offers/promo_js_file.js?ver=7");
     loadScript("https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js");

     loadScript("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js")
     .then(function (){
        $("#myCarousel").show();

        $(".carousel").on("touchstart", function(event){
                var xClick = event.originalEvent.touches[0].pageX;
            $(this).one("touchmove", function(event){
                var xMove = event.originalEvent.touches[0].pageX;
                if( Math.floor(xClick - xMove) > 3 ){
                    $(".carousel").carousel('next');
                }
                else if( Math.floor(xClick - xMove) < -3 ){
                    $(".carousel").carousel('prev');
                }
            });
            $(".carousel").on("touchend", function(){
                    $(this).off("touchmove");
            });
        });
     })


    $(window).scroll(function(){
        'use strict';
            $('header').removeClass("sticky");
    });

    $(".entry_submit").on("click", function(event) {
		$('.entry_text').hide();
		$('.entry_text_mob').hide();
		$("#2nd_fold").show("slide", { direction: "left" }, 300);
	});
    $(".entry_submit_mob").on("click", function(event) {
		$('.entry_text').hide();
		$('.entry_text_mob').hide();
		$("#2nd_fold_mob").show("slide", { direction: "left" }, 300);
	});

 }
</script>
<script type="text/javascript" async src="https://gobumpr.com/js/utm_cookie.js"></script>




<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '582926561860139');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

</body>
</html>
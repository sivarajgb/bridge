<?php
include("sidebar.php");
//  include("config.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id'])) || $flag!="1") {

    if($_SESSION['crm_log_id']!='crm023' && $_SESSION['crm_log_id']!='crm037' && $_SESSION['crm_log_id']!='crm093'  && $_SESSION['crm_log_id']!='crm018')
  {
  header('location:logout.php');
  die();
  }
}
?>

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <title>GoBumpr Bridge</title>

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
  
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
   <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

  <!-- table sorter -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js"></script>

<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>

<style>
/*home page blocks */
.floating-box {
 display: inline-block;
 margin: 2px;
 padding-top: 12px;
 padding-left: 12px;
 width:300px;
}
.mySlides {display:none;}
.navbar-fixed-top{
  z-index:100 !important;
}
.upper-div{
  z-index:999999 !important;
}
#range > span:hover{cursor: pointer;}
 /* table */
/*#tbody{
   font-size:15px !important;
  border:1.5px solid #c4b8b8 !important;
  
}*/

  button.close {
       
        background: white;
        font-size: 20px;
        height: 20px;
        opacity: 1;
       /* width: 46px;*/
    }

    div.example {
 width:75%;
 float:left;
 margin-left:12 em; 
}

@media screen and (min-width: 600px) {
  div.example {
    margin-left:12em;
    width:75%;
  }
}

@media screen and (max-width: 600px) {
  div.example {
    margin-left: 3em;
  }
}

@media screen and (min-width: 600px) {

  #reportrange {
   margin-top: 2%;margin-left: 25%;
  }
}

@media screen and (max-width: 600px) {
  #reportrange {
    margin-top: 17%;
       margin-bottom: 3%;
  }
}
div.example table th{
      padding-bottom: 15px; 
      color: #808080;
      text-align: center;
      font-size: 13px !important;
      font-family: 'Montserrat';
}


@media screen and (min-width: 600px) {

 div.example table th {
  padding-bottom: 15px; 
      color: #808080;
      text-align: center;
      font-size: 13px !important;
      font-family: 'Montserrat';
  }
}

@media screen and (max-width: 600px) {
  div.example table th {
   padding-bottom: 15px; 
      color: #808080;
      text-align: center;
      font-size: 7px !important;
      font-family: 'Montserrat';
    
  }
}

div.example  table tr td {
    border-top: 1px solid  #ECF0F1;
    font-size:17px !important;
    border-bottom: 1px solid  #ECF0F1;
    padding: 20px 10px 10px 10px; 
   text-align: center;
   font-family: 'Montserrat'
}

@media screen and (min-width: 600px) {

  div.example  table tr td {
  border-top: 1px solid   #ECF0F1;
    font-size:17px !important;
    border-bottom: 1px solid  #ECF0F1;
    padding: 20px 10px 10px 10px;
    text-align: center;
    font-family: 'Montserrat'
  }
}

@media screen and (max-width: 600px) {
  div.example  table tr td {
    border-top: 1px solid   #ECF0F1;
    font-size:10px !important;
    border-bottom: 1px solid  #ECF0F1;
    text-align: center;
    font-family: 'Montserrat'
    
  }
}


.w3-display-left{
    margin-left: -280px !important;
}

@media screen and (min-width: 600px) {

  .w3-display-left  {
  margin-left: -280px !important;
  }
}

@media screen and (max-width: 600px) {
  .w3-display-left {
    margin-left: 0px !important;
    
  }
}

.w3-display-right{
    margin-right: -280px  !important;
}

@media screen and (min-width: 600px) {

  .w3-display-right  {
  margin-right: -280px !important;
  }
}

@media screen and (max-width: 600px) {
  .w3-display-right {
    margin-right: 0px !important;
    
  }
}
   

thead:hover{
  cursor:pointer;
}

.results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px;
  color:#9E9E9E;
}
#tbody, tbody tr {
    -webkit-animation: opacity 5s ease-in-out;
    animation: opacity 5s ease-in-out;
}
button, input, optgroup, select, textarea {
    margin: 0;
    font: inherit;
    color: black;
}

.imageclass {
  border-radius: 10%;
}

img:hover {
  box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
}
  
</style>

</head>
<body id="body" >
 <?php include_once("header.php"); ?>
<script>
  $(document).ready(function(){
    $('#city').show();
  })
</script>
<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>

<div id="reportrange" class=" col-sm-3 col-lg-3 col-xs-3" >
   <div class=" floating-box1">
     <div id="range" class="form-control" style="min-width:312px;">
     <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
    <span id="dateval"></span> <b class="caret"></b>
  </div>
    </div>
   </div>
<div class=" col-sm-2 col-lg-1" style="margin-top: 2%;margin-left: 2%;">
    <div class="floating-box1">
      <div style="max-width:120px;">
          <select id="vehicle" name="vehicle" class="form-control" style="width:120px;">
            <option value="all" selected="">All Vehicles</option>
            <option value="2w">2 Wheeler</option>
            <option value="4w">4 Wheeler</option>
            <option value="tyres">Tyres</option>
        
          </select>
      </div>
    </div>
  </div>


<div id="show" style="margin-top:102px;">

</div>  
<!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>
<div id="myModal" class="modal fade" role="dialog" >
  <div class="modal-dialog modal-lg" style="width: 100%;margin-top: 1px; position: fixed;">
    

    <!-- Modal content-->
    <div class="modal-content" id="ajax" style="background-color: rgba(0,0,0,0.8) ;" >
       
      </div>
    </div>

  </div>
</div> 


<!-- jQuery library -->
  
<script async src="js/jquery-3.2.1.min.js" onLoad="loadOtherScripts();"></script>

<script>
    function loadScript(src) {
        return new Promise(function (resolve, reject) {
            var s;
            s = document.createElement('script');
            s.src = src;
            s.onload = resolve;
            s.onerror = reject;
            document.head.appendChild(s);
        });
    }

    function loadOtherScripts() {
      loadScript('//cdn.jsdelivr.net/momentjs/latest/moment.min.js')
      .then(function() {
      Promise.all([ loadScript('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'),loadScript('js/daterangepicker.js'),loadScript('js/sidebar.js'),loadScript('https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js'),loadScript('js/jquery.table2excel.js')]).then(function () {
      console.log('scripts are loaded');
      dateRangePicker();
      momentInPicker();
      }).catch(function (error) {
      console.log('some error!' + error)
      })
      }).catch(function (error) {
     console.log('Moment call error!' + error)
      })
      }
  </script>

  <!-- date range picker -->
 <script>
function dateRangePicker(){
  $('input[name="daterange"]').daterangepicker({
    locale: {
          format: 'DD-MM-YYYY'
      }
  });
}
</script>
<script type="text/javascript">
function momentInPicker(){
  $(function() {

  var start = moment().subtract(1, 'days');
   // var start = moment();
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

  });
}
</script>

<script>
function defaultview(val){
   var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
  var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');

  var city = $('#city').val();
  var veh = $('#vehicle').val();
  
  console.log(startDate);
  //Make AJAX request, using the selected value as the POST

  $.ajax({
      url : "ajax/garage_approval_view.php",  // create a new php page to handle ajax request
      type : "POST",
      data : {"start_date": startDate ,"end_date":endDate,"city":city,"veh":veh},
      success : function(data) {
        //console.log(data);
            $('#show').html(data);
      $("#loading").show();
      $("#loading").hide();
      $('#show').show();

      $('.getdeal').click(function(){

    var mecid = $(this).data('id');
   //alert(mecid);
    $.post( "caurosal.php", { dl_id: mecid}, function (data){
          $('#ajax').html(data);
    // alert(booking_id);
    });
  });
            counterval();
       },
     error: function(xhr, ajaxOptions, thrownError) {
          // alert(xhr.status + " "+ thrownError);
      }
  });
}
</script>
<script>
function counterval(){
  var jobCount = $("#tbody tr").length;;
   $('.counter').text(jobCount + ' shops');
}
</script>


<script>
$(document).ready( function (){

$('#dateval').on("DOMSubtreeModified", function (){
      $("#show").hide();
      $("#loading").show();
      var life="no";
  defaultview(life);
    });

  $('#city').change(function (){
   $('#show').hide();
  $("#loading").show();
  var life="no";
  defaultview(life);
   });
   $('#vehicle').change(function (){
   $('#show').hide();
  $("#loading").show();
  var life="no";
  defaultview(life);
   });
   
});
</script>



</body>
</html>

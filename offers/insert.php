<!DOCTYPE html>
<html ng-app>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/ap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
 <link href="album.css" rel="stylesheet">

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js">
	
</script>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<script type="text/javascript">
	

</script>
 <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
<div class="w3-top">
  <div class="w3-bar w3-white w3-wide w3-padding w3-card-2">
   <b><i class="fa fa-database" style="font-size:25px;color:#5CC2C2"></i>
    </b> ADD NEW OFFER</a>
    <!-- Float links to the right. Hide them on small screens -->
    <div class="w3-right w3-hide-small">
      <a href="http://localhost/live%20offers/" class="w3-bar-item w3-button">BACK TO HOME</a>
    </div>
  </div>
</div>
    

<br><br><br>
<div class="form-group">
	<form method="post"  
	ng-app="myApp" action="insertion.php"
	name="up" enctype="multipart/form-data" >
<div class="container">

<div class="row">

	<label class="col-md-4"> Titlepage</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<input type="text" 
	 name="titlepage"
	 ng-maxlength="40"
	   ng-model="titlepage"
	  class="form-control" required>
	</div>
	<span ng-show="up.titlepage.$invalid">*Required(Not More Than 40 Characters)</span>
</div>

<div class="row">
	
	<label class="col-md-4">Titlethumb</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<input type="text"  name="titlethumb" ng-model="titlethumb"
	class="form-control" ng-maxlength="28" required>
	</div>
	<span ng-show="up.titlethumb.$invalid">*Required(Not More Than 28 Characters)</span>
</div>
<div class="row">
	
	<label class="col-md-4">Pageurl</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<input type="text" name="pageurl" ng-model="pageurl" 
	class="form-control" required>
	</div>
	<span ng-show="up.pageurl.$invalid">*Required</span>
</div>

<div class="row">
	
	<label class="col-md-4">City</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<input type="text" name="city"
	ng-model="city" class="form-control" required>
	</div>
	<span ng-show="up.city.$invalid" >*Required</span>
</div>

<div class="row">
	
	<label class="col-md-4">Imagepage</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
        
        <input type="file" id="file" name="files[]"  accept="image/*" />
        
	
	</div>
	<span ng-show="up.imagepage.$invalid" >*Required</span>
</div>

<div class="row">
	
	<label class="col-md-4">Imageappthumb</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
         <input type="file" id="file" name="files2[]" accept="image/*" />

	
	</div>
	<span ng-show="up.imageappthumb.$invalid" >*Required</span>
</div>

<div class="row">
	
	<label class="col-md-4">Vechile Type</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<input type="text" name="vechiletype" 
	ng-model="vechiletype"class="form-control" 
	ng-maxlength="3" required>
	</div>
	<span ng-show="up.vechiletype.$invalid" >*Required(Not More Than 3 Characters)</span>
</div>
<div class="row">
	
	<label class="col-md-4">ServiceShort</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<input type="text" name="serviceshort"  
	class="form-control">
	</div>
	<span>Optional</span>
</div>

<div class="row">
	
	<label class="col-md-4">SVG Path</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<input type="text" name="svgpath" class="form-control">
	</div>
	<span>optional</span>
</div>

<div class="row">
	<label class="col-md-4">Exclude Brand</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<input type="text" name="excludebrand" class="form-control">
	</div>
<span>Optional</span>
</div>


<div class="row">
	
	<label class="col-md-4">SoleBrand</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<input type="text" name="solebrand" class="form-control">
	</div>
	<span>optional</span>
</div>




<div class="row">
	
	<label class="col-md-4">Validity</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<input type="date" name="validity"
	ng-model="validity" class="form-control" required>
	</div>
	<span ng-show="up.validity.$invalid" >*Required</span>
</div>

<div class="row">
	
	<label class="col-md-4">Price</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<input type="number" name="price" ng-model="price"
	class="form-control" required>
	</div>
	<span ng-show="up.price.$invalid" >*Required</span>
</div>

<div class="row">
	
	<label class="col-md-4">Descrption</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<input type="text" name="descrption" class="form-control">
	</div>
	<span>Optional</span>
</div>

<div class="row">
	
	<label class="col-md-4">Bullets</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<textarea name="bullets" ng-model="bullets"
	class="form-control"   required></textarea>
	</div>
	<span ng-show="up.bullets.$invalid" >*Required</span>
</div>
<div class="row">
	
	<label class="col-md-4">UspBullets</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<textarea name="uspbullets" ng-model="uspbullets"
	class="form-control"   required></textarea>
	</div>
	<span>Optional</span>
</div>


<div class="row">
	
	<label class="col-md-4">Titlemeta</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<input type="text" name="titlemeta"
	ng-model="titlemeta" ng-maxlength="60"
	class="form-control" required>
	</div>
	<span ng-show="up.titlemeta.$invalid">*Required(Not More than 60 Characters)
	</span>
</div>

<div class="row">
	<label class="col-md-4">Live</label>
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<select name="live" name="live" 
ng-model="live" class="form-control" required>
  <option value="1">Activate</option>
  <option value="0">Deactivate</option>
</select>
</div>
<span ng-show="up.live.$invalid" >*Required</span>
</div>

<div class="row">
	
	<label class="col-md-4">ServiceType</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<input type="text" name="servicetype"
	ng-model="servicetype" class="form-control" required>
	</div>
	<span ng-show="up.servicetype.$invalid" >*Required</span>
</div>

<div class="row">
	
	<label class="col-md-4">MobileNo</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<input type="number" name="mobileno" 
	ng-model="mobileno" class="form-control" 
	ng-maxlength="10" ng-minlength="10" required>
	</div>
	<span ng-show="up.mobileno.$invalid" >*Required(Should be Valid Mobile No)</span>
</div>
<br>
<div class="row">
	
	<label class="col-md-4">Priority</label>	
	<div class="col-md-4">
	<div class="col-md-4"></div>
	<input type="text" name="priority"
	ng-model="priority" class="form-control"  ng-maxlength="3" required>
	</div>
	<span ng-show="up.priority.$invalid" >*Required</span>
</div>



<div class="row">
	<div class="col-md-4">
	<div class="col-md-8"></div>
	<input type="SUBMIT" name="submit"
	value="ADD OFFER" 
	 class="btn">
	</div>
</div>
</div>
	</form> 
	</div>
</body>
</html>

<?php

//$conn = mysqli_connect("localhost","root","","go_bumpr") or die(mysqli_error($conn));
//$conn1 = mysqli_connect("localhost","root","","b2b") or die(mysqli_error($conn1));
$conn=mysqli_connect("localhost","root","devprcs1@3bd","go_bumpr");
//ini_set('max_execution_time', 300); //300 seconds = 5 minutes

include_once('fpdf.php');
 
class PDF extends FPDF
{
// Page header
function Header()
{
     if ($this->page == 1)
          {
    // Logo
    // $this->Image('logo.png',10,-1,70);
    $this->SetFont('Arial','B',13);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(130,10,'Feeback Data Panel List',0,0,'C');
    // Line break
    $this->Ln(20);
    }
}
 
// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}


}

$startdate = date('Y-m-d',strtotime($_POST['startDate']));
$enddate =  date('Y-m-d',strtotime($_POST['endDate']));
//echo $startdate;
$city = $_POST['city'];
$shopname=$_POST['shopname'];
$vehicle = $_POST['vehicle'];
$service = $_POST['service'];
//$master_service = $_POST['master_service'];
//print_r($_POST);
$cond='';

$cond = $cond.($city == 'all' ? "" : "AND g.city='$city'");
$cond = $cond.($shopname == 'all' ? "" : "AND b.b2b_shop_id ='$shopname'");
$cond = $cond.($vehicle == 'all' ? "" : "AND g.vehicle_type='$vehicle'");
$cond = $cond.($service == 'all' ? "" : "AND g.service_type='$service'");
// if($master_service != "all" && $service == "all"){
//     //echo " entered";
//     $sql_serv = "SELECT service_type FROM go_axle_service_price_tbl WHERE bridge_flag='0' AND master_service='$master_service'";
//     $res_serv = mysqli_query($conn1,$sql_serv);
//     $services = '';
//     while($row_serv = mysqli_fetch_array($res_serv)){
//         if($services == ""){
//             $services = $row_serv['service_type']."'";
//         }
//         else{
//             $services = $services.",'".$row_serv['service_type']."'";
//         }
//     }
//     $cond = $cond." AND g.service_type IN ('$services)";
// } 


$result = mysqli_query($conn, "SELECT g.booking_id,g.city,g.booking_id,b.b2b_customer_name,b.b2b_cust_phone,b.brand,b.model,g.service_type,b.b2b_service_date,b.b2b_log,f.shop_name,g.vehicle_type,g.locality,g.service_status,g.followup_date,g.estimated_bill_amt,g.service_status_reason,g.rating,g.feedback,g.feedback_status,b.b2b_bill_amount,g.final_bill_amt,b.b2b_credit_amt,(CASE WHEN CAST(g.final_bill_amt AS UNSIGNED) > CAST( b.b2b_bill_amount AS UNSIGNED) THEN g.final_bill_amt ELSE b.b2b_bill_amount END) AS Total_Bill,b.b2b_shop_id  FROM go_bumpr.user_booking_tb as g join b2b.b2b_booking_tbl b on b.gb_booking_id = g.booking_id left join go_bumpr.feedback_track f on f.booking_id=g.booking_id WHERE f.flag=0 AND  DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_shop_id NOT IN (1014,1035,1670) {$cond} ORDER BY g.shop_name") or die("database error:". mysqli_error($conn));


$pdf = new FPDF('P','mm','A3');   
$pdf->SetLeftMargin(7);
//header
$pdf->AddPage('L');
//foter page
$pdf->AliasNbPages();

$width_cell=array(22,30,35,23,35,35,17,22,21,21,17,22,35,35,35);
//$pdf->SetFont('Arial','B',16);

//Background color of header//
$pdf->SetFillColor(976,245,458);

$pdf->SetFont('Arial','B',10);

// Header starts /// 
//First header column //
$pdf->Cell($width_cell[0],10,'Booking Id',1,0,'C');
//Second header column//
$pdf->Cell($width_cell[1],10,'Customer Name',1,0,'C');
//Third header column//
$pdf->Cell($width_cell[2],10,'Shop Name',1,0,'C'); 
//Fourth header column//
$pdf->Cell($width_cell[3],10,'Vehicle Type',1,0,'C');

$pdf->Cell($width_cell[4],10,'Model',1,0,'C');
//Third header column//
$pdf->Cell($width_cell[5],10,'Service Status',1,0,'C'); 

$pdf->Cell($width_cell[6],10,'Rating',1,0,'C'); 

$pdf->Cell($width_cell[7],10,'Goaxle',1,0,'C');

$pdf->Cell($width_cell[8],10,'Bill Amount',1,0,'C');

$pdf->Cell($width_cell[9],10,'Credits',1,0,'C');

$pdf->Cell($width_cell[10],10,'EBM',1,0,'C');

$pdf->Cell($width_cell[11],10,'Service Date',1,0,'C');

$pdf->Cell($width_cell[12],10,'Service Type',1,0,'C');
 
$pdf->Cell($width_cell[13],10,'Feedback',1,0,'C');

$pdf->Cell($width_cell[14],10,'Status Reason',1,0,'C');


// //Background color of header//
// $pdf->SetFillColor(193,229,252);
// foreach($header as $heading) {
// $pdf->Cell(40,12,$display_heading[$heading['Field']],1);
// }

// foreach($result as $row) {
// $pdf->Ln();
// foreach($row as $column)
// $pdf->Cell(35,10,$column,1,0,'C');
// }
$pdf->SetFont('Arial','',10);
$pdf->SetFillColor(193,229,252);
$w=35;
$h=10   ;
 while($row = mysqli_fetch_array($result)) { 
    $pdf->Ln();
    $booking_id=$row['booking_id'];
    $b2b_customer_name=$row['b2b_customer_name'];
    $shop_name=$row['shop_name'];
    $vehicle_type=$row['vehicle_type'];
    $service_status=$row['service_status'];
    $rating=$row['rating'];
    $feedback_status=$row['feedback_status'];
    $log=date('Y-m-d',strtotime($row['b2b_log']));
    $model=$row['brand']."-".$row['model'];
   // $final_bill_amt=$row['b2b_bill_amount'];
    $b2b_credit_amt=$row['b2b_credit_amt'];
    $b2b_service_date=$row['b2b_service_date'];
    $service_type=$row['service_type'];
    $service_status_reason=$row['service_status_reason'];
    $feedback=$row['feedback']; 

      $exception=mysqli_query($conn,"SELECT * FROM go_bumpr. `exception_mechanism_track` where booking_id='$booking_id'");
      $excep_count=mysqli_num_rows($exception);
     // echo $excep_count;
      if($excep_count==0)
      {
        $ebm="No";
      }
      else
      {
        $ebm="Yes";
      }

      $b2b_bill_amount=$row['b2b_bill_amount'];
      $final_bill_amt=$row['final_bill_amt'];

        if($final_bill_amt>$b2b_bill_amount)
        {
          $amount=$final_bill_amt;
        }
        else
        {
          $amount=$b2b_bill_amount;
        }

       if($feedback_status==11 ||$feedback_status==12 || $feedback_status==13 || $feedback_status==21 || $feedback_status==31 || $feedback_status==41 || $feedback_status==51 && ($service_status !='Completed'|| $service_status !='In Progress')  ) 
               {
                
              $status="RNR";
            
               }  
               else if($feedback_status==0){
              $status="Idle";
               } 
               else{
               $status=$row['service_status'];
               }

$pdf->Cell(22,10,$booking_id,1,0,'C');
$pdf->Cell(30,10,$b2b_customer_name,1,0);
$x=$pdf->GetX(); 
$pdf->myCell($w,$h,$x,$shop_name);
$pdf->Cell(23,10,$vehicle_type,1,0,'C');
$x=$pdf->GetX(); 
$pdf->myCell($w,$h,$x,$model);
$x=$pdf->GetX(); 
$pdf->myCell($w,$h,$x,$status);
//$pdf->Cell(42,10,$status,1,0,'C');
$pdf->Cell(17,10,$rating,1,0,'C');
$pdf->Cell(22,10,$log,1,0,'C');
$pdf->Cell(21,10,$amount,1,0,'C');
$pdf->Cell(21,10,$b2b_credit_amt,1,0,'C');
$pdf->Cell(17,10,$ebm,1,0,'C');
$pdf->Cell(22,10,$b2b_service_date,1,0,'C');
$x=$pdf->GetX(); 
$pdf->myCell($w,$h,$x,$service_type);
$x=$pdf->GetX(); 
$pdf->myCell($w,$h,$x,$feedback);

$x=$pdf->GetX(); 
$pdf->myCell($w,$h,$x,$service_status_reason);

 }
  $filename="FeedBackDataPanel.pdf"; 
  $pdf->Output('F','FeedBackDataPanel.pdf'); 
  echo $filename;
  
?>


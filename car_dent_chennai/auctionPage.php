<?php
    include('config.php');
    $conn = dbconnect();
    $conn2 = dbconnect2();
    date_default_timezone_set('Asia/Kolkata');
    $booking_id = base64_decode($_GET["id"]);
    $cd_query = "SELECT user_id,locality,log FROM tyre_booking_tb WHERE booking_id=".$booking_id;   
    $cd_result = mysqli_query($conn,$cd_query);
    $cd_values = mysqli_fetch_object($cd_result); 
    $user_id = $cd_values->user_id;
    $user_locality = $cd_values->locality;
    $left_it_to_expert = false;
    if($tyre_brand == ""){
        $left_it_to_expert = true;
    }
    $log_time_query = "SELECT b2b_log FROM b2b_booking_tbl_tyres WHERE gb_booking_id=".$booking_id." ORDER BY b2b_log ASC LIMIT 1;";
    $log_time_result = mysqli_query($conn2,$log_time_query);
    $value = mysqli_fetch_object($log_time_result);
    $log_time = $value->b2b_log;
    $ll_query = "SELECT lat,lng FROM localities WHERE localities='".$user_locality."'";
    $ll_result = mysqli_query($conn,$ll_query);
    $ll_values = mysqli_fetch_object($ll_result);
    $booking_lat = $ll_values->lat;
    $booking_lng = $ll_values->lng;
    $mob_query = "SELECT mobile_number FROM user_register WHERE reg_id=".$user_id;
    $mob_result= mysqli_query($conn,$mob_query);
    $mob_values= mysqli_fetch_object($mob_result);
    $mobile_number = $mob_values->mobile_number;
//    $end = "2018-06-20 15:54:38";
//    echo $log_time;
    $end = date("Y-m-d H:i:s");
    $ts1 = strtotime($log_time);
    $ts2 = strtotime($end);
    $seconds_diff = $ts2 - $ts1;
    $minutes = floor($seconds_diff/60);
    $seconds = round($seconds_diff%60);
    $time_elapsed_milliseconds = ($minutes*60 + $seconds);
//    echo $time_elapsed_milliseconds;
    $time_milliseconds = 86400 - $time_elapsed_milliseconds;
    $thirty_min_over_flag = ($time_milliseconds<84600)?true:false;
    $minutes = floor($time_milliseconds/60);
    $seconds = round($time_milliseconds%60);
    if($thirty_min_over_flag == false){
        $minutes = floor(($time_milliseconds - 84600)/60);
        $seconds = round(($time_milliseconds - 84600)%60);
    }
//    echo $log_time;
    if($time_milliseconds < 0){
        $GLOBALS["time_milliseconds"] = 0;
        $GLOBALS["minutes"]=0;
        $GLOBALS["seconds"]=0;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>GoBumpr Full Body Painting</title>
        <!-- Latest compiled and minified JavaScript  -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" >
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="icon" type="image/png" sizes="16x16" href="https://static.gobumpr.com/web-app/fav-icons/16.png">
        <link rel="icon" type="image/png" sizes="32x32" href="https://static.gobumpr.com/web-app/fav-icons/32.png">
        <link rel="icon" type="image/png" sizes="57x57" href="https://static.gobumpr.com/web-app/fav-icons/57.png">
        <link rel="icon" type="image/png" sizes="76x76" href="https://static.gobumpr.com/web-app/fav-icons/76.png">
        <link rel="icon" type="image/png" sizes="96x96" href="https://static.gobumpr.com/web-app/fav-icons/96.png">
        <link rel="icon" type="image/png" sizes="114x114" href="https://static.gobumpr.com/web-app/fav-icons/114.png">
        <link rel="icon" type="image/png" sizes="144x144" href="https://static.gobumpr.com/web-app/fav-icons/144.png">
        <link rel="icon" type="image/png" sizes="152x152" href="https://static.gobumpr.com/web-app/fav-icons/152x152.png">
        <link rel="stylesheet" href="../css/auctionPageCss.css">
		<!-- Google Analytics Code -->
		<script async>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-67994843-3', 'auto');
		  ga('send', 'pageview');

		</script>
		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '582926561860139');
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
		/></noscript>
		<!-- DO NOT MODIFY -->
		<!-- End Facebook Pixel Code -->
    </head>
    <body>
        <div class="wrapper">           
        <div class="white_background">
            <nav>
                 <div class="container navbar_content">
                     <a href="/tyrest/"><img src="https://static.gobumpr.com/img/logo-new-gb.svg" class="text-center" id="gobumpr_logo" alt="GoBumpr Full Body Painting services Online"></a>
                 </div>
            </nav>
            <div id="user_details_display" data-value="<?php echo $time_milliseconds;?>">
                 <div class="container">
                    <div class="row">
                        <div class="col-sm-5 col-xs-3 text-right">
                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                        </div>
                        <div class="col-sm-7 col-xs-9" id="id_mobile_display">
                            <p><strong>Booking Id : </strong><?php echo $booking_id ?></p>
                            <p><strong>Mobile number : </strong><?php echo $mobile_number; ?></p>
                        </div>
                     </div> 
                 </div>
            </div>
            <?php if($time_milliseconds >0 && $thirty_min_over_flag == false ){ ?>
            <div class="text-center" id="timer_section" data-value="<?php echo $time_milliseconds; ?>">
                <p class="time-elapsed"> Time Remaining <img src="https://static.gobumpr.com/tyres/time.svg" class="time_image" alt="GoBumpr FBP auction timer">
                    <span class="minutes">
                    <?php if($minutes <10) {echo "0".$minutes;} else {echo $minutes;}?>
                    </span> : 
                    <span class="seconds"> 
                    <?php if($seconds<10){echo "0".$seconds;} else {echo $seconds;}?>    
                    </span>
                </p>
            </div>
            <?php } ?>
        </div>    
    <div id="info" class="text-center">
       <p><span><i class="fa fa-info-circle" aria-hidden="true"></i></span>You will continue to receive deals until the timer runs out</p>
    </div>
    <div class="lds-ring text-center"><div class="text-center"></div><div></div><div></div><div></div></div>
    <div id="auction_details" class="text-center">
        <div class="container text-center" id="auctioneers_list" >
        </div>
        <p class="loading" style="display:none">Fetching deals<span>.</span><span>.</span><span>.</span></p>
        <button class="btn show-more" style="display:none;"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i> Show more</button>
    </div> 
    <footer>       
        <div class="footer">
            <div class="footer-content">
                <img src="https://static.gobumpr.com/tyres/thank-you.svg" class="thank-you-img" alt="Thank you image">
                <div class="thank-you-text">
                    <h4 class="happy_shopping_large"><strong>HAPPY SHOPPING</strong></h4>
                    <p>THANKS FOR SHOPPING WITH US</p>
                    <p>The selected dealers will contact you shortly.</p>                   
                </div>
            </div>
        </div>
    </footer>
    </div>
    <script 
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <script src="../js/timer.js"></script>
    </body>
</html>
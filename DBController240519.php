<?php
error_reporting(0);
date_default_timezone_set('Asia/Kolkata');


class DB_connection{

	public function connect_DB(){
		$conn = mysqli_connect("localhost"
,"root","devprcs1@3db","go_bumpr") or die(mysqli_error($con));
		return $conn;
	}
	
	public function GetBookingId($booking_id){
		$conn=$this->connect_DB();
		$query = "SELECT gb_booking_id FROM b2b.b2b_booking_tbl WHERE gb_booking_id='$booking_id' AND b2b_swap_flag!='1'";
		$result = mysqli_query($conn,$query);
		return $result;
		
	}
	public function GetVehicleDetails($booking_id){
		$conn=$this->connect_DB();
		$query = "SELECT a.mec_id,a.user_id,vech_id,a.shop_name,user_veh_id,a.vehicle_type as veh_type,user_vech_no,service_type,
					pick_up,service_date,service_description,pickup_address,pickup_full_address,estimate_amt,pickup_date_time,
					axle_flag,b2b_referral_id,b2b_referral_shop_id,crm_update_id,
					b.name,b.mobile_number,b.mobile_number2,b.email_id,c.brand,c.model,c.vehicle_id,c.fuel_type,c.id,
					d.axle_id,d.address5,d.flag,d.type,d.wkly_counter,d.status,d.exception_stage,b2b_avail,e.b2b_new_flg,
					f.vehicle_type,luxury,f.id FROM 
					go_bumpr.user_booking_tb a join go_bumpr.user_register b 
					on b.reg_id=a.user_id
					join go_bumpr.user_vehicle_table c
					on a.user_veh_id=c.id
					join admin_mechanic_table d
					on a.mec_id=d.mec_id
					LEFT JOIN b2b.b2b_mec_tbl e 
					ON e.b2b_shop_id = d.axle_id
					join admin_vehicle_table_new f
					on c.vehicle_id=f.id
				WHERE a.booking_id='$booking_id'";
		$result = mysqli_query($conn,$query);
		return $result;
	}
		public function GetAmount($veh_type,$b2b_service_type,$vtype,$city){
		$conn=$this->connect_DB();
			$column=strtolower($city).'_amt';
			if($veh_type == '2w'){
				$query="SELECT {$column} as amt FROM go_axle_service_price_tbl WHERE service_type='$b2b_service_type' AND type='$veh_type' AND vehicle_type = '$vtype' ";
			}else{
				$query= "SELECT {$column} as amt FROM go_axle_service_price_tbl WHERE service_type='$b2b_service_type' AND type='$veh_type'";	
			}
		// echo $query;die;
		$result = mysqli_query($conn,$query);
		return $result;
	}
	public function GetShopBalance($axle_id){
		$conn=$this->connect_DB();
		$query="SELECT b2b_cash_remaining,b2b_leads,b2b_re_leads,b2b_non_re_leads FROM b2b.b2b_credits_tbl WHERE b2b_shop_id='$axle_id'";  
		//echo $query;die;
		$result=mysqli_query($conn,$query);
		return $result;
	}
	public function GetShopLastStatus($axle_id){
		$conn=$this->connect_DB();
		$query="SELECT case when sum(b.b2b_credit_amt) is not null THEN sum(b.b2b_credit_amt) else 0 end as cash FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE s.b2b_acpt_flag='0' AND s.b2b_deny_flag='0' AND b.gb_booking_id!='0' AND b.b2b_shop_id='$axle_id' AND b.b2b_swap_flag!='1' AND date(b.b2b_log) > '2018-01-01'";
		//echo $query;die;
		$result=mysqli_query($conn,$query);
		return $result;
		
	}
	public function GetMaxBookinId($axle_id){
		$conn=$this->connect_DB();
		$query = "SELECT max(b2b_shop_booking_id) AS b2b_shop_booking_id FROM b2b.b2b_booking_tbl WHERE b2b_shop_id='$axle_id' and b2b_swap_flag=0 ";
		//echo $query;die;
		$res_s_booking_id = mysqli_query($conn,$query);
		$row_s_booking_id = mysqli_fetch_object($res_s_booking_id);
		$b2b_shop_booking_id=$row_s_booking_id->b2b_shop_booking_id;
		$b2b_shop_booking_id=$b2b_shop_booking_id+1;
		return $b2b_shop_booking_id;
	}
	
	public function B2BBookingID($booking_id){
		$conn=$this->connect_DB();
		$sql_b2b_get = "SELECT max(b2b_booking_id) as b2b_booking_id FROM b2b.b2b_booking_tbl WHERE gb_booking_id = '$booking_id'";
		$res_b2b_get = mysqli_query($conn,$sql_b2b_get);
		return $res_b2b_get;
	}
	public function InsertBookingTable($axle_id,$b2b_shop_booking_id,$user_name,$veh_brand,$veh_model,$fuel_type,$veh_no,$user_phn,$b2b_service_type,$service_date,$pickup,$booking_id,$amount,$referral_id,$referral_shop_id,$veh_type,$service_description,$pickup_date,$pickup_time,$pickup_full_address,$estimate_amt,$cust_alt_phn,$city,$b2b_booking_id){
		$conn=$this->connect_DB();
		//$service_description=mysqli_real_escape_string($conn,$service_description);
		//$pickup_full_address=mysqli_real_escape_string($conn,$pickup_full_address);
		$today= date('Y-m-d H:i:s');
		$sql_b2b_booking = "INSERT INTO b2b.b2b_booking_tbl(b2b_shop_id,b2b_shop_booking_id,b2b_customer_name,brand,model,fuel_type,b2b_vehicle_no,b2b_cust_phone,b2b_service_type,b2b_service_date,b2b_pick_up,b2b_source,b2b_log,gb_booking_id,b2b_credit_amt,b2b_referral_id,b2b_referral_shop_id,b2b_vehicle_type,b2b_customer_remarks,b2b_pick_date,b2b_pick_time,b2b_pick_address,b2b_bridge_estimate,b2b_alternate_phone) VALUES ('$axle_id','$b2b_shop_booking_id','$user_name','$veh_brand','$veh_model','$fuel_type','$veh_no','$user_phn','$b2b_service_type','$service_date','$pickup','web','$today','$booking_id','$amount','$referral_id','$referral_shop_id','$veh_type','$service_description','$pickup_date','$pickup_time','$pickup_full_address','$estimate_amt','$cust_alt_phn')";
		//echo $sql_b2b_booking;die;
		$res_b2b_booking = mysqli_query($conn,$sql_b2b_booking)or die(mysqli_error($conn));
		return 	$res_b2b_booking;
	}
	public function GetCrmId($b2b_booking_id,$city){
		$conn=$this->connect_DB();
		$today= date('Y-m-d H:i:s');

		$sql_status = "INSERT INTO b2b.b2b_status(b2b_booking_id,b2b_log) VALUES ('$b2b_booking_id','$today')";
		$res_status = mysqli_query($conn,$sql_status);

		$query = "SELECT crm_log_id FROM crm_admin where flag = 0 and crm_feedback = 1 and city= '$city' AND crm_log_id!='crm194' ORDER BY crm_log_id";
		$result = mysqli_query($conn,$query);
		return $result;
	}
	public function CrmTrack($city){
		$conn=$this->connect_DB();
		$query = "SELECT id,crm_log_id FROM feedback_track where flag = 0 and city= '$city'  ORDER BY ID desc LIMIT 1";
		$result = mysqli_query($conn,$query);
		return $result;
	}
	public function FeedBack($booking_id,$b2b_booking_id,$user_id,$mec_id,$axle_id,$shop_name,$veh_type,$service_type,$city,$assinged_crm_id,$next_crm_id){
		$conn=$this->connect_DB();
		$today= date('Y-m-d H:i:s');

		$query = "INSERT INTO feedback_track(booking_id,b2b_booking_id,user_id,mec_id,shop_id,shop_name,veh_type,service_type,city,crm_goaxle_id,crm_log_id,crm_update_time,log) VALUES ('$booking_id','$b2b_booking_id','$user_id','$mec_id','$axle_id','$shop_name','$veh_type','$service_type','$city','$assinged_crm_id','$next_crm_id','$today','$today')";
		//echo $query;die;
		$res_feedback = mysqli_query($conn,$query);
		return $res_feedback;
	}
	public function InsertTracker($b2b_booking_id,$b2b_shop_booking_id,$axle_id){
		$conn=$this->connect_DB();
		$sql_track ="INSERT INTO b2b.b2b_tracking_tbl(b2b_booking_id,b2b_shop_booking_id,b2b_shop_id) VALUES ('$b2b_booking_id','$b2b_shop_booking_id','$axle_id')";
		//echo $sql_track;die;
		$res_track=mysqli_query($conn,$sql_track);
		//insert into checkin report
		$sql_checkin ="INSERT INTO b2b.b2b_checkin_report(b2b_booking_id,b2b_shop_booking_id,b2b_flag) VALUES('$b2b_booking_id','$b2b_shop_booking_id',0)";    
		$res_checkin = mysqli_query($conn,$sql_checkin);
		//insert into vehicle at garage      
		$sql_veh_at_gar="INSERT INTO b2b.b2b_vehicle_at_garage(b2b_booking_id,b2b_shop_booking_id,b2b_flag) VALUES('$b2b_booking_id','$b2b_shop_booking_id',0)";
		$sqlins = mysqli_query($conn,$sql_veh_at_gar);
		//insert into service under progress      
		$sql_serv_un_prg="INSERT INTO b2b.b2b_service_under_progress(b2b_booking_id,b2b_shop_booking_id,b2b_flag) VALUES('$b2b_booking_id','$b2b_shop_booking_id',0)";
		$res_serv_un_prg = mysqli_query($conn,$sql_serv_un_prg);
		// insert into vehicel delivered      
		$sql_veh_del ="INSERT INTO b2b.b2b_vehicle_delivered(b2b_booking_id,b2b_shop_booking_id,b2b_flag) VALUES('$b2b_booking_id','$b2b_shop_booking_id',0)";
		$res_veh_del = mysqli_query($conn,$sql_veh_del);
		// insert into vehicel ready      
		$sql_veh_red="INSERT INTO b2b.b2b_vehicle_ready_for_delivey(b2b_booking_id,b2b_shop_booking_id,b2b_flag) VALUES('$b2b_booking_id','$b2b_shop_booking_id',0)";
		$res_veh_red = mysqli_query($conn,$sql_veh_red);

		$sql="SELECT b2b_mobile_number_1 FROM b2b.b2b_mec_tbl WHERE b2b_shop_id='$axle_id'";  
		$query=mysqli_query($conn,$sql);
		$row=mysqli_fetch_object($query);
		$mec_phone=$row->b2b_mobile_number_1;
		return $mec_phone;
	}
	public function GoAxleTracker($booking_id,$b2b_booking_id,$axle_id,$assinged_crm_id,$user_agent){
		$conn=$this->connect_DB();
		$today= date('Y-m-d H:i:s');

		$query = "INSERT INTO  goaxle_track (go_booking_id,b2b_booking_id,b2b_shop_id,sent_crm_id,sent_log,sent_device,status) VALUES ('$booking_id','$b2b_booking_id','$axle_id','$assinged_crm_id','$today','$user_agent','sent')";
		$result = mysqli_query($conn,$query);
		return $result;
	}
	public function WeekCounter($mec_id,$assinged_crm_id,$booking_id,$axle_id,$b2b_new_flg,$vtype){
		$conn=$this->connect_DB();
		$today= date('Y-m-d H:i:s');

		$booking_update=mysqli_query($conn,"UPDATE user_booking_tb SET axle_flag=1,crm_update_id='$assinged_crm_id',booking_status='2',crm_update_time='$today' WHERE booking_id='$booking_id'");
		$booking_status=mysqli_query($conn,"UPDATE override_tbl SET status='3' WHERE booking_id='$booking_id'");
		$weekly_counter=mysqli_query($conn,"UPDATE admin_mechanic_table SET wkly_counter= wkly_counter-1 ,floor_cnt=floor_cnt-1 WHERE mec_id='$mec_id'");
		//echo $b2b_new_flg;die;

		if($b2b_new_flg == 1)
		{
			//echo $vtype;die;
			if($vtype=="RE"){
				$lead_counter = mysqli_query($conn,"UPDATE b2b.b2b_credits_tbl SET b2b_leads = b2b_leads - 1 ,b2b_re_leads=b2b_re_leads - 1 WHERE b2b_shop_id = '$axle_id'");
			}elseif($vtype=="NRE"){
				$lead_counter = mysqli_query($conn,"UPDATE b2b.b2b_credits_tbl SET b2b_leads = b2b_leads - 1,b2b_non_re_leads=b2b_non_re_leads - 1 WHERE b2b_shop_id = '$axle_id'");
			}else{
				$lead_counter = mysqli_query($conn,"UPDATE b2b.b2b_credits_tbl SET b2b_leads = b2b_leads - 1 WHERE b2b_shop_id = '$axle_id'");
			}
		}
		return $weekly_counter;
	}

	public function UserDetails($booking_id){
		$conn=$this->connect_DB();
		$sql_user_bk = "SELECT a.user_id,b.reg_id,b.name,a.initial_service_type,b.referral_count,a.booking_id,b.name,b.mobile_number,b.mobile_number2,b.email_id,b.City,b.Locality_Home,b.Locality_Work,a.source,campaign,b.Last_service_date,c.*,b.Next_service_date,b.Last_called_on,b.Follow_up_date,comments,d.*,a.service_description,a.pickup_full_address,a.log,a.pickup_date_time,LOWER(pick_up) as pick_up
						FROM user_booking_tb a 
						left join user_register b on a.user_id=b.reg_id
						left join user_vehicle_table c on a.user_veh_id=c.id 
						left JOIN freedom_pass_booking d ON d.user_veh_id = c.id AND c.flag = 0
						WHERE a.booking_id='$booking_id'";
		//echo $sql_user_bk;die;
      $res_user_bk = mysqli_query($conn,$sql_user_bk);
	  return $res_user_bk;
	}

	public function goaxle_report($from_date,$to_date,$vehicle_type,$city){
		$conn=$this->connect_DB();
		$query="call go_bumpr.GB_GoaxleReport('$from_date','$to_date','$vehicle_type','$city')";
		//echo $query;
		$result=mysqli_query($conn,$query);
		return $result; 
	}
	public function view_user_count($count_date,$vehicle_type,$page,$city){
		$conn=$this->connect_DB();
		if($city=="all"){
		if($vehicle_type==""){
			if($page=="new_count"){
			$query="select user_id,booking_id,booking_status,axle_flag,cast(b2b_log as date)as date1,service_type,vehicle_type,city
				FROM user_booking_tb a join b2b.b2b_booking_tbl b on a.booking_id=b.gb_booking_id where booking_status='2' and axle_flag='1'
				and cast(b2b_log as date)='$count_date' and user_id in (select user_id
				FROM user_booking_tb  group by user_id HAVING  COUNT(DISTINCT user_id,cast(log as date)) = 1) and b2b_swap_flag!=1";
			}else{
			$query="select user_id,booking_id,booking_status,axle_flag,service_type,cast(b2b_log as date)as date1,vehicle_type,city
				FROM user_booking_tb a join b2b.b2b_booking_tbl b on a.booking_id=b.gb_booking_id where booking_status='2' and axle_flag='1'
				and cast(b2b_log as date)='$count_date' and user_id not in (select user_id
				FROM user_booking_tb  group by user_id HAVING  COUNT(DISTINCT user_id,cast(log as date)) = 1) and b2b_swap_flag!=1";
			}
		}else{
			if($page=="new_count"){
			$query="select user_id,booking_id,booking_status,axle_flag,cast(b2b_log as date)as date1,service_type,vehicle_type,city
				FROM user_booking_tb a join b2b.b2b_booking_tbl b on a.booking_id=b.gb_booking_id where booking_status='2' and axle_flag='1'
				and cast(b2b_log as date)='$count_date' and user_id in (select user_id
				FROM user_booking_tb  group by user_id HAVING  COUNT(DISTINCT user_id,cast(log as date)) = 1) and b2b_swap_flag!=1
				and vehicle_type='$vehicle_type'";
			}else{
			$query="select user_id,booking_id,booking_status,axle_flag,service_type,cast(b2b_log as date)as date1,vehicle_type,city
				FROM user_booking_tb a join b2b.b2b_booking_tbl b on a.booking_id=b.gb_booking_id where booking_status='2' and axle_flag='1'
				and cast(b2b_log as date)='$count_date' and user_id not in (select user_id
				FROM user_booking_tb  group by user_id HAVING  COUNT(DISTINCT user_id,cast(log as date)) = 1) and b2b_swap_flag!=1
				and vehicle_type='$vehicle_type'";
			}
		}
		}else{
			if($vehicle_type==""){
			if($page=="new_count"){
			$query="select user_id,booking_id,booking_status,axle_flag,cast(b2b_log as date)as date1,service_type,vehicle_type,city
				FROM user_booking_tb a join b2b.b2b_booking_tbl b on a.booking_id=b.gb_booking_id where booking_status='2' and axle_flag='1'
				and cast(b2b_log as date)='$count_date' and city='$city' and user_id in (select user_id
				FROM user_booking_tb  group by user_id HAVING  COUNT(DISTINCT user_id,cast(log as date)) = 1) and b2b_swap_flag!=1";
			}else{
			$query="select user_id,booking_id,booking_status,axle_flag,service_type,cast(b2b_log as date)as date1,vehicle_type,city
				FROM user_booking_tb a join b2b.b2b_booking_tbl b on a.booking_id=b.gb_booking_id where booking_status='2' and axle_flag='1'
				and cast(b2b_log as date)='$count_date' and city='$city' and user_id not in (select user_id
				FROM user_booking_tb  group by user_id HAVING  COUNT(DISTINCT user_id,cast(log as date)) = 1) and b2b_swap_flag!=1";
			}
		}else{
			if($page=="new_count"){
			$query="select user_id,booking_id,booking_status,axle_flag,cast(b2b_log as date)as date1,service_type,vehicle_type,city
				FROM user_booking_tb a join b2b.b2b_booking_tbl b on a.booking_id=b.gb_booking_id where booking_status='2' and axle_flag='1'
				and cast(b2b_log as date)='$count_date' and city='$city' and user_id in (select user_id
				FROM user_booking_tb  group by user_id HAVING  COUNT(DISTINCT user_id,cast(log as date)) = 1) and b2b_swap_flag!=1
				and vehicle_type='$vehicle_type'";
			}else{
			$query="select user_id,booking_id,booking_status,axle_flag,service_type,cast(b2b_log as date)as date1,vehicle_type,city
				FROM user_booking_tb a join b2b.b2b_booking_tbl b on a.booking_id=b.gb_booking_id where booking_status='2' and axle_flag='1'
				and cast(b2b_log as date)='$count_date' and city='$city' and user_id not in (select user_id
				FROM user_booking_tb  group by user_id HAVING  COUNT(DISTINCT user_id,cast(log as date)) = 1) and b2b_swap_flag!=1
				and vehicle_type='$vehicle_type'";
			}
		}
		}
		//echo $query;die;
		$result=mysqli_query($conn,$query);
		return $result;
	}
	public function view_user_details($user_id){
		$conn=$this->connect_DB();
		$query="SELECT name,a.user_id,booking_id,booking_status,axle_flag,service_type,a.city,cast(a.crm_update_time as date)as date1 from user_booking_tb a join user_register b on a.user_id=b.reg_id where a.user_id='$user_id'";
		//echo $query;die;
		$result=mysqli_query($conn,$query);
		return $result; 
	}
	public function MarketingReportView($from_date,$to_date,$vehicle_type,$city,$trans,$service,$service_type){
		$conn=$this->connect_DB();
		if($trans=="All"){
			$query="call go_bumpr.MarketingReportAll('$from_date','$to_date','$vehicle_type','$city')";
		}else{
		$query="call go_bumpr.MarketingReport('$from_date','$to_date','$vehicle_type','$city','$trans','$service','$service_type')";
		}
		//echo $query;
		$result=mysqli_query($conn,$query);
		return $result;
	}
}

?>
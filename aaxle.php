<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if(empty($_SESSION['crm_log_id'])) {
	header('location:logout.php');
	die();
}

if ($_SESSION['flag'] !=1 && $_SESSION['crm_feedback'] !=1 && $_SESSION['crm_se'] !=1) {
	header('location:logout.php');
	die();
}

?>

<!DOCTYPE html>
<html>
<head>
	<!-- jQuery library -->
	<script src="https://code.jquery.com/jquery-1.10.2.min.js" integrity="sha256-C6CB9UYIS9UJeqinPHWTHVqh/E1uhG5Twh+Y5qFQmYg=" crossorigin="anonymous"></script>
<meta charset="utf-8">
  <title>GoBumpr Bridge</title>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<!-- table sorter-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.28.9/css/theme.grey.min.css" />
<!-- masking -->
<script type="text/javascript" src="js/inputmask.js"></script>
<script type="text/javascript" src="js/jquery.inputmask.js"></script>
  <script type="text/javascript" src="js/inputmask.extensions.js"></script>

  <!-- auto complete -->
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/jquery-ui.structure.min.css">
<link rel="stylesheet" href="css/jquery-ui.theme.min.css">

<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>
  <style>
   /* axle page cursor */
#range > span:hover{cursor: pointer;}
 #sl{
	cursor:pointer;
}

.floating-box {
	display: inline-block;
 float: right;
 margin: 12px;
 padding: 12px;
 width:112px;
 height:63px;
 box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  font-size: 13px;
  cursor:pointer;
}
.floating-box1 {
    display: inline-block;
	margin:10px;
	float:left;
	clear:both;
 }
 .floating-box2 {
 	display: inline-block;
  float: left;
	margin: 12px;
  padding: 12px;
	width:412px;
  height:53px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
   font-size: 15px;
   cursor:pointer;
 }
.rad input {visibility: hidden; width: 0px; height: 0px;}
label{
		cursor:pointer;
	}
	/*auto complete */
@charset "utf-8";.ui-autocomplete{z-index:1000 !important;cursor:default;list-style:none;}
.ui-widget{}
.ui-autocomplete{overflow-y:auto;overflow-x:hidden;}
.ui-menu{width:0px;display:none;}
.ui-autocomplete > li{padding:10px;padding-left:10px;}
ul{margin-bottom:0;}
.ui-autocomplete > li.ui-state-focus{background-color:#DDD;}
.ui-autocomplete .ui-front .ui-menu .ui-widget .ui-widget-content{border:0;}
.ui-helper-hidden-accessible{display:none;}
.gobumpr-icon{font-style:normal;font-weight:normal;speak:none;display:inline-block;text-decoration:inherit;text-align:center;font-variant:normal;text-transform:none;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
.ui-widget{background-color:white;width:100%;}
.ui-widget-content{padding-left:1px;display:block;width:20px;position:relative;line-height:12px;max-height:210px;border:.5px solid #DADADA;}
.ui-widget{}
.ui-autocomplete { position: absolute; cursor: default;}

/* table */
thead:hover{
	cursor:pointer;
}

.results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px;
  color:#ccc;
}

</style>

</head>
<body>
<?php include_once("header.php"); ?>
<script>
	$(document).ready(function(){
		$('#city').show();
	})
</script>
<div class="overlay" data-sidebar-overlay></div>

<div class="padding"></div>

  <div  id="div1" style="margin-top:10px;float:left;margin-left:10px; border:2px solid #4CB192; border-radius:5px;width:50%;height:530px;">
		<!-- vehicle filter -->
		<div class=" col-xs-1" style="margin-top: 10px;float:left; margin-right:40px;">
		    <select id="veh_type" name="veh_type" class="form-control" style="width:69px;">
		      <option value="all" selected>All</option>
		      <option value="2w">2W</option>
		      <option value="4w">4W</option>
		    </select>
		</div>

		<!-- Search Bar -->
		<div class=" col-xs-2 form-group" style="margin-top: 10px;float:left; margin-right:40px;">
      	      <input type="text" class="searchs form-control" placeholder="Search" style="width:135px;">
		</div>
		
<!-- location filter -->
<div class=" col-xs-2  col-lg-offset-1" style="margin-top:10px;float:left;">
		<div class=" form-group" id="loc" style="width:125px;">
		        <div class="ui-widget">
		        	<input class="form-control autocomplete" id="location" type="text" name="location"  required placeholder=" Location">
		        </div>
		</div>
 </div>
 <div id="sl" class="form-group col-xs-1 col-lg-offset-1 col-xs-offset-0" style="margin-top:15px;margin-left:25px;font-size:16px;color:#95A186;">
 &nbsp;<i class="fa fa-search" aria-hidden="true"></i>
 </div>



 <!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
	<div class='uil-default-css' style='transform:scale(0.58);'>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	</div>
</div>
</div>
<!-- table -->
  <div id = "table" align="center" style="max-width:94%;margin-top:60px; margin-left:20px; display:none;height:450px !important;overflow:auto !important;">
  <table id="example" class="table table-striped table-bordered tablesorter table-hover results"  >
  <thead style="background-color: #D3D3D3;" id="example-head">
  
  </thead>
  <tbody id="tbody">
  </tbody>
  </table>
  </div>


  </div> <!-- div 1 -->
  <div id="div2" style="margin-top:10px;margin-left:10px; float:left; border:2px solid #DFA64E; border-radius:5px;width:48%;height:530px;">

		<!-- date range picker -->
 	 <div id="reportrange" class=" col-sm-3 " style="cursor: pointer; margin-left: 50px;">
 			<div class=" floating-box1">
 				<div id="range" class="form-control" style="min-width:312px;">
 				<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
 			 <span id="dateval"></span> <b class="caret"></b>
 		 </div>
 			 </div>
 	 </div>
	 <!-- loading -->
	 <div id="loading1" style="display:none; margin-top:140px;" align="center">
	 	<div class='uil-default-css' style='transform:scale(0.58);'>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 	<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
	 	</div>
	 </div>
	 </div>
	 <div id="divs2">
	<div id="div2_1" style="margin-top:50px; margin-left:-210px; float:left; border-radius:5px;width:15%;">
	<div id="ls" class="floating-box2" align="center" style="background-color:#f6ecd0;" data-toggle="modal" data-target="#myModal_leads_sent">
		 <p style="float:left;margin-left:20px;"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp;Leads Sent : &nbsp;&nbsp;</p>
		 <p id="leads_sent" style="float:left;">0</p>
	 </div>
 </div>
	 <div id="div2_2" style="margin-top:110px; margin-left:-210px; float:left; border-radius:5px;width:15%;">
 	 <div id="ls" class="floating-box2" align="center" style="background-color:#f6ecd0;" data-toggle="modal" data-target="#myModal_leads_accepted">
 		 <p style="float:left;margin-left:20px;"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>&nbsp;&nbsp;Leads Accepted : &nbsp;&nbsp;</p>
 		 <p id="leads_accepted" style="float:left;">0</p>
 	 </div>
  </div>
		<div id="div2_3" style="margin-top:170px; margin-left:-210px; float:left; border-radius:5px;width:15%;">
		 <div id="ls" class="floating-box2" align="center" style="background-color:	#e6ecd0;" data-toggle="modal" data-target="#myModal_checkins_reported">
			 <p style="float:left;margin-left:20px;"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>&nbsp;&nbsp;Check-Ins Reported : &nbsp;&nbsp;</p>
			 <p id="checkins_reported" style="float:left;">0</p>
		 </div>
	 </div>
		 <div id="div2_4" style="margin-top:230px; margin-left:-210px; float:left; border-radius:5px;width:15%;">
	 	 <div id="ls" class="floating-box2" align="center" style="background-color:#d6ecd0;" data-toggle="modal" data-target="#myModal_inspections_completed">
	 		 <p style="float:left;margin-left:20px;"><i class="fa fa-calculator" aria-hidden="true"></i>&nbsp;&nbsp;Inspection Completed : &nbsp;&nbsp;</p>
	 		 <p id="inspection_completed" style="float:left;">0</p>
	 	 </div>
	  </div>
		<div id="div2_5" style="margin-top:290px; margin-left:-210px; float:left; border-radius:5px;width:15%;">
 	 	 <div id="ls" class="floating-box2" align="center" style="background-color:#c6ecd0;" data-toggle="modal" data-target="#myModal_services_under_progress">
 	 		 <p style="float:left;margin-left:20px;"><i class="fa fa-wrench" aria-hidden="true"></i>&nbsp;&nbsp;Services under progress : &nbsp;&nbsp;</p>
 	 		 <p id="service_under_progress" style="float:left;">0</p>
 	 	 </div>
 	  </div>
		 <div id="div2_6" style="margin-top:350px; margin-left:-210px; float:left; border-radius:5px;width:15%;">
 	 	 <div id="ls" class="floating-box2" align="center" style="background-color:	#b6ecd0;" data-toggle="modal" data-target="#myModal_vehicles_ready">
 	 		 <p style="float:left;margin-left:20px;"><i class="fa fa-car" aria-hidden="true"></i>&nbsp;&nbsp;Vehicles Ready : &nbsp;&nbsp;</p>
 	 		 <p id="vehicle_ready" style="float:left;">0</p>
 	 	 </div>
 	  </div>
		<div id="div2_7" style="margin-top:410px; margin-left:-210px; float:left; border-radius:5px;width:15%;">
 	 	 <div id="ls" class="floating-box2" align="center" style="background-color:	#a6ecd0;" data-toggle="modal" data-target="#myModal_vehicles_delivered">
 	 		 <p style="float:left;margin-left:20px;"><i class="fa fa-thumbs-up" aria-hidden="true"></i>&nbsp;&nbsp;Vehicles Delivered : &nbsp;&nbsp;</p>
 	 		 <p id="vehicle_delivered" style="float:left;">0</p>
 	 	 </div>
	 </div>


    <div id="div3" style="margin-top:83px; margin-right:97px; float:right; border-radius:5px;width:15%;">
    <div class="floating-box" id="totalbill" align="center" style="float:left;background-color:#99BBC5; " data-toggle="modal" data-target="#myModal_total_bill">
			<p>Total Bill</p>
			<p id="tbill">0</p>
		</div>
		<div class="floating-box" id="avgbill" align="center" style="float:left;background-color: #FFE4B5;" data-toggle="modal" data-target="#myModal_avg_bill">
			<p>Avg. Bill</p>
			<p id="abill">0</p>
		</div>
		<br>
		<div class="floating-box" id="rating" align="center" style="float:left;background-color:#A7E9B1; " data-toggle="modal" data-target="#myModal_rating">
			<p>Rating</p>
			<p id="rat">0</p>
		</div>
		<div class="floating-box" id="reviews" align="center" style="float:left;background-color: #DF9B88;" data-toggle="modal" data-target="#myModal_reviews">
			<p>Reviews</p>
			<p id="rev">0</p>
		</div>
	<!--	<div class="floating-box" id="daily_report" align="center" style="float:left;background-color: #FFB74D;height:40px;margin-top:30px;">
			<a href="axle_daily_reports.php"><p>Daily Report</p></a>
		</div>-->
 </div> <!-- divs2 -->
</div>
<div id="metrics" style="display: none;margin-top: 50px;">
</div>
  <!-- table sorter -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.28.9/js/jquery.tablesorter.min.js"></script>	<!-- date picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<!-- search bar  -->
<script>
$(document).ready(function() {
  $(".searchs").keyup(function () {
    var searchTerm = $(".searchs").val();
    var listItem = $('.results tbody').children('tr');
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

  $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });

  $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','false');
  });

  $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','true');
  });

  var jobCount = $('.results tbody tr[visible="true"]').length;
    $('.counter').text(jobCount + ' item');

  if(jobCount == '0') {$('.no-result').show();}
    else {$('.no-result').hide();
	}
		  });
});
</script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- date range picker -->
<script>
$('input[name="daterange"]').daterangepicker({
 locale: {
      format: 'DD-MM-YYYY'
    }
});
</script>
<script type="text/javascript">
$(function() {

  var start = moment().subtract(30, 'days');
	 // var start = moment();
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});
</script>

<!-- select Location of service center -->
<script>
function locality() {
	var city = $('#city').val();
			    $( "#location" ).autocomplete({
source: function(request, response) {
	
            $.ajax({
                url: "ajax/get_b2b_loc.php",
                data: {
                    term: request.term,
					city:city,
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
					//console.log(data);
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
}
</script>
 <script>
$(
	locality()
);
</script>
<script>
function view_shops(){
	var veh = $("#veh_type").val();
	var loc = $("#location").val();
	var city = $("#city").val();
	$.ajax({
				url : "ajax/shop_view.php",  // create a new php page to handle ajax request
				type : "POST",
				data : {"veh": veh ,"loc":loc, "city": city },
				success : function(data) {
		$("#loading").hide();
		$("#table").show();
		if(veh=="2w"){
			$("#example-head").html("<th>No</th><th>Shop Name</th><th>Location</th><th>Credits</th><th>RE Leads</th><th>NRE Leads</th>");
		}else{
			$("#example-head").html("<th>No</th><th>Shop Name</th><th>Location</th><th>Credits</th><th>Leads</th>");
		}

		if(data == 'no'){
			$('#table').html("<h3 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h3>");
		}
		else{
			 $('#tbody').html(data);
			 $("#example").trigger("update");
		}
	  },
			error: function(xhr, ajaxOptions, thrownError) {
				//alert(xhr.status + " "+ thrownError);
		 }
	 });
}
</script>
<!-- default shops view -->
<script>
$(document).ready(function(){
	$("#table").hide();
	$("#loading").show();
		view_shops();
});
</script>
<!-- shops view on changing veh type -->
<script>
$(document).ready(function(){
	$("#veh_type").change(function(){
		$("#table").hide();
		$("#loading").show();
		view_shops();
	});
});
</script>
<!-- shops view on changing location -->
<script>
$(document).ready(function(){
 $("#location").on("autocompletechange", function(){
	    $("#table").hide();
			$("#loading").show();
	 		view_shops();
	});
});
</script>
<!-- shops view on changing city -->
<script>
function show_metrics()
{
	$("#loading1").show();
	var shop_id = $('input[name="radio_shop"]:checked').val();
 //var item = "leads_sent";
 var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
 var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
 
 $.ajax({
		 url : "ajax/get_metrics.php",  // create a new php page to handle ajax request
		 type : "POST",
		 data : {"shop_id": shop_id , "startdate": startDate, "enddate": endDate},
		 success : function(data) {
			$("#loading1").hide();
			//$("#metrics").css("display", "inline-flex");
			$("#metrics").show();
			$("#metrics").html(data);
			$("#back_toggler").show();
			$("#back_toggler").on("click",function(){
				$("#divs2").hide();
				$("#loading1").show();
				
				//console.log(x);
				get_count(shop_id);
		 });
		},
		 error: function(xhr, ajaxOptions, thrownError) {
			// alert(xhr.status + " "+ thrownError);
		}
	});
}
$(document).ready(function(){
 $("#city").change(function(){
	    $("#table").hide();
			$("#loading").show();
			$("#location").autocomplete('close').val('');			
			locality();
	 		view_shops();
	});
});
</script>
<div id="popup"></div>
<script>
function get_values(id){
 var shop_id = id;
 //var item = "leads_sent";
 var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
 var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
 var veh_type=$('#veh_type').val();
 $.ajax({
			 url : "ajax/get_axle_count.php",  // create a new php page to handle ajax request
			 type : "POST",
			 data : {"shop_id": shop_id , "startdate": startDate, "enddate": endDate,"veh_type":veh_type},
			 success : function(data) {
	 			$("#loading1").hide();
				$("#metrics").hide();
				$("#metrics1").hide();
				$("#back_toggler").hide();
	 			$("#divs2").show();
				$("#divs2").html(data);

				$("#toggler").show();
				$("#toggler").on("click",function(){
					$("#divs2").hide();
					$("#loading1").show();
					show_metrics();
				});
	 },
		 error: function(xhr, ajaxOptions, thrownError) {
			// alert(xhr.status + " "+ thrownError);
		}
	});
	$.ajax({
 			 url : "ajax/popup_user_list.php",  // create a new php page to handle ajax request
 			 type : "POST",
 			 data : {"shop_id": shop_id , "startdate": startDate, "enddate": endDate},
 			 success : function(data) {
  			$('#popup').html(data);
 	 },
 		 error: function(xhr, ajaxOptions, thrownError) {
 			// alert(xhr.status + " "+ thrownError);
 		}
 	});
}
</script>

<!-- get count -->
<script>
function get_count(id){
	 var x= id;
	$('input[name="radio_shop"]').not(':checked').next('label').css('color', 'black');
	$('input[name="radio_shop"]:checked').next('label').css('color', '#ffa800');
	$("#divs2").hide();
	$("#metrics").hide();
	$("#metrics1").hide();
	$("#back_toggler").hide();
	$("#loading1").show();
	get_values(x);
}
</script>
<!-- get count on changing dates -->
<script>
$(document).ready(function() {
$('#dateval').on("DOMSubtreeModified", function (){
	var x=$('input[name="radio_shop"]:checked').val();
	//console.log(x);
	get_count(x);
} );
});
</script>
<script>
$(document).ready(function()
	{
			$("#example").tablesorter(  {sortList: [[0,2], [0,0]]} );
	}
);
</script>

</body>
</html>

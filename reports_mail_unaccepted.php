<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
include('config.php');
$conn = db_connect1();

require_once('New/email/class.phpmailer.php');
require_once('New/email/class.smtp.php');

echo "<br>today:".$today = date('Y-m-d');
echo "<br>yesterday:".$yesterday = date("Y-m-d", strtotime('-1 days'));
echo "<br>day before yesterday:".$dbyesterday = date("Y-m-d", strtotime('-2 days'));

//$sql = "SELECT b.gb_booking_id,b.b2b_flag,s.b2b_acpt_flag,s.b2b_deny_flag,g.shop_name,(SELECT cr.b2b_partner_flag FROM b2b.b2b_credits_tbl as cr WHERE cr.b2b_shop_id = b.b2b_shop_id) as premium FROM b2b.b2b_booking_tbl AS b INNER JOIN go_bumpr.user_booking_tb AS g ON b.gb_booking_id = g.booking_id LEFT JOIN b2b.b2b_status AS s ON b.b2b_booking_id = s.b2b_booking_id WHERE b.b2b_log BETWEEN '$yesterday' AND '$today' AND b.b2b_flag!='1' AND g.crm_update_id NOT IN ('crm003','crm036') AND g.mec_id NOT IN (200360,200018,400001,400974,200629)";
$sql1 = "SELECT DISTINCT b.gb_booking_id,b.b2b_customer_name,b.b2b_vehicle_type,b.b2b_service_type,m.b2b_shop_id,m.b2b_shop_name,m.b2b_address5 FROM b2b.b2b_booking_tbl AS b INNER JOIN b2b.b2b_mec_tbl as m ON b.b2b_shop_id = m.b2b_shop_id LEFT JOIN b2b.b2b_status AS s ON b.b2b_booking_id = s.b2b_booking_id WHERE DATE(b.b2b_log)='$yesterday' AND b.gb_booking_id!='0'  AND b.b2b_swap_flag!='0' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND s.b2b_acpt_flag='0' AND s.b2b_deny_flag='0' ORDER BY m.b2b_shop_name ASC";
$res1 = mysqli_query($conn,$sql1) or die(mysqli_error($conn));

$sql2 = "SELECT DISTINCT b.gb_booking_id,b.b2b_customer_name,b.b2b_vehicle_type,b.b2b_service_type,m.b2b_shop_id,m.b2b_shop_name,m.b2b_address5 FROM b2b.b2b_booking_tbl AS b INNER JOIN b2b.b2b_mec_tbl as m ON b.b2b_shop_id = m.b2b_shop_id LEFT JOIN b2b.b2b_status AS s ON b.b2b_booking_id = s.b2b_booking_id WHERE DATE(b.b2b_log)='$yesterday' AND b.gb_booking_id!='0' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND s.b2b_acpt_flag='0' AND s.b2b_deny_flag='1' AND b.b2b_swap_flag='0' ORDER BY m.b2b_shop_name ASC";
$res2 = mysqli_query($conn,$sql2) or die(mysqli_error($conn));

$sql3 = "SELECT DISTINCT b.gb_booking_id,b.b2b_customer_name,b.b2b_vehicle_type,b.b2b_service_type,m.b2b_shop_id,m.b2b_shop_name,m.b2b_address5,b.b2b_log FROM b2b.b2b_booking_tbl AS b INNER JOIN b2b.b2b_mec_tbl as m ON b.b2b_shop_id = m.b2b_shop_id LEFT JOIN b2b.b2b_status AS s ON b.b2b_booking_id = s.b2b_booking_id WHERE DATE(b.b2b_log) BETWEEN '2018-01-01' AND '$dbyesterday' AND b.gb_booking_id!='0'  AND b.b2b_swap_flag!='0' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND s.b2b_acpt_flag='0' AND s.b2b_deny_flag='0' ORDER BY b.b2b_log ASC";
$res3 = mysqli_query($conn,$sql3) or die(mysqli_error($conn));

$sql4 = "SELECT DISTINCT b.gb_booking_id,b.b2b_customer_name,b.b2b_vehicle_type,b.b2b_service_type,m.b2b_shop_id,m.b2b_shop_name,m.b2b_address5,b.b2b_log FROM b2b.b2b_booking_tbl AS b INNER JOIN b2b.b2b_mec_tbl as m ON b.b2b_shop_id = m.b2b_shop_id LEFT JOIN b2b.b2b_status AS s ON b.b2b_booking_id = s.b2b_booking_id WHERE DATE(b.b2b_log) BETWEEN '2018-01-01' AND '$dbyesterday' AND b.gb_booking_id!='0' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND s.b2b_acpt_flag='0' AND s.b2b_deny_flag='1' AND b.b2b_swap_flag='0' ORDER BY b.b2b_log ASC";
$res4 = mysqli_query($conn,$sql4) or die(mysqli_error($conn));

// fetch the values 
if(mysqli_num_rows($res1) >= 1){ 
    $idle_table = '<table align="center" class="float-center"><tr class="header"><th align="center" class="list1">Booking ID</th><th align="center" class="list1">Customer Name</th><th align="center" class="list1">Type</th><th align="center" class="list1">Service Type</th><th align="center" class="list1">Shop Name</th><th align="center" class="list1">City</th></tr>';            
    while($row1 = mysqli_fetch_object($res1)){
        $idle_table = $idle_table.'<tr><td align="center">'.$row1->gb_booking_id.'</td><td align="center">'.$row1->b2b_customer_name.'</td><td align="center">'.$row1->b2b_vehicle_type.'</td><td align="center">'.$row1->b2b_service_type.'</td><td>'.$row1->b2b_shop_name.'</td><td align="center">'.$row1->b2b_address5.'</td></tr>';
    }
    $idle_table = $idle_table.'</tbody></table>';

    if(mysqli_num_rows($res2)>=1){
        $rejected_table = '<table align="center" class="float-center"><tr class="header"><th align="center" class="list1">Booking ID</th><th align="center" class="list1">Customer Name</th><th align="center" class="list1">Type</th><th align="center" class="list1">Service Type</th><th align="center" class="list1">Shop Name</th><th align="center" class="list1">City</th></tr>';            
        while($row2 = mysqli_fetch_object($res2)){
            $rejected_table = $rejected_table.'<tr><td align="center">'.$row2->gb_booking_id.'</td><td align="center">'.$row2->b2b_customer_name.'</td><td align="center">'.$row2->b2b_vehicle_type.'</td><td align="center">'.$row2->b2b_service_type.'</td><td>'.$row2->b2b_shop_name.'</td><td align="center">'.$row2->b2b_address5.'</td></tr>';
        }
        $rejected_table = $rejected_table.'</tbody></table>';
    }

    if(mysqli_num_rows($res3)>=1){
      $priority_table = '<table align="center" class="float-center"><tr class="header"><th align="center" class="list1">Booking ID</th><th align="center" class="list1">Customer Name</th><th align="center" class="list1">Type</th><th align="center" class="list1">Service Type</th><th align="center" class="list1">Shop Name</th><th align="center" class="list1">City</th><th align="center" class="list1">GoAxled Date</th></tr>';            
      while($row3 = mysqli_fetch_object($res3)){
          $priority_table = $priority_table.'<tr><td align="center">'.$row3->gb_booking_id.'</td><td align="center">'.$row3->b2b_customer_name.'</td><td align="center">'.$row3->b2b_vehicle_type.'</td><td align="center">'.$row3->b2b_service_type.'</td><td>'.$row3->b2b_shop_name.'</td><td align="center">'.$row3->b2b_address5.'</td><td align="center">'.date("d M Y",strtotime($row3->b2b_log)).'</td></tr>';
      }
      $priority_table = $priority_table.'</tbody></table>';
    } 

      if(mysqli_num_rows($res4)>=1){
        $priority_rejected_table = '<table align="center" class="float-center"><tr class="header"><th align="center" class="list1">Booking ID</th><th align="center" class="list1">Customer Name</th><th align="center" class="list1">Type</th><th align="center" class="list1">Service Type</th><th align="center" class="list1">Shop Name</th><th align="center" class="list1">City</th><th align="center" class="list1">GoAxled Date</th></tr>';            
        while($row4 = mysqli_fetch_object($res4)){
            $priority_rejected_table = $priority_rejected_table.'<tr><td align="center">'.$row4->gb_booking_id.'</td><td align="center">'.$row4->b2b_customer_name.'</td><td align="center">'.$row4->b2b_vehicle_type.'</td><td align="center">'.$row4->b2b_service_type.'</td><td>'.$row4->b2b_shop_name.'</td><td align="center">'.$row4->b2b_address5.'</td><td align="center">'.date("d M Y",strtotime($row4->b2b_log)).'</td></tr>';
        }
        $priority_rejected_table = $priority_rejected_table.'</tbody></table>';
    }

    // ******************************* send email ******************************************* //
    
        $to1='metrics@gobumpr.com';       
        //$to1='sirisha@gobumpr.com';
        if($to1!=''||$to1!=NULL){
            //SMTP Settings
            $mail1 = new PHPMailer();
            $mail1->IsSMTP();
            $mail1->SMTPAuth   = true;
            $mail1->SMTPSecure = "SSL";
            $mail1->Host       = "smtp.pepipost.com";
            $mail1->Username   = "letsgobumpr";
            $mail1->Password   = "GoBumpr25000!";
            $mail1->isHTML(true);

            $mail1->SetFrom('admin@gobumpr.com', 'Admin'); //from (verified email address)
            $mail1->Subject = "Unaccepted Leads Report - ".date('d M Y',strtotime($today));
            $body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            
            <head>
              <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
              <meta name="viewport" content="width=device-width">
              
              <style>
                .wrapper {
                  width: 100%;
                }
            
                body {
                  width: 100% !important;
                  min-width: 100%;
                  -webkit-text-size-adjust: 100%;
                  -ms-text-size-adjust: 100%;
                  margin: 0;
                  Margin: 0;
                  padding: 0;
                  -moz-box-sizing: border-box;
                  -webkit-box-sizing: border-box;
                  box-sizing: border-box;
                }
            
                center {
                  width: 100%;
                  min-width: 580px;
                }
            
                table {
                  border-spacing: 0;
                  border-collapse: collapse;
                }
            
                td {
                  word-wrap: break-word;
                  -webkit-hyphens: auto;
                  -moz-hyphens: auto;
                  hyphens: auto;
                  border-collapse: collapse !important;
                }
            
                table,
                tr,
                td {
                  padding: 0;
                  vertical-align: top;
                  text-align: left;
                }
            
                @media only screen {
                  html {
                    min-height: 100%;
                    background: white;
                  }
                }
            
                table.body {
                  background: white;
                  height: 100%;
                  width: 100%;
                  border: none;
                }
            
                table.row {
                  padding: 0;
                  width: 100%;
                  position: relative;
                }
            
                h2.text-center {
                  text-align: center;
                }
            
                table.float-center,
                td.float-center {
                  margin: 0 auto;
                  Margin: 0 auto;
                  float: none;
                  text-align: center;
                }
            
                body,
                table.body,
                h2,
                td,
                th {
                  color: #0a0a0a;
                  font-family: Helvetica, Arial, sans-serif;
                  font-weight: normal;
                  padding: 0;
                  margin: 0;
                  Margin: 0;
                  text-align: left;
                  line-height: 1.3;
                }
            
                h2 {
                  color: inherit;
                  word-wrap: normal;
                  font-family: Helvetica, Arial, sans-serif;
                  font-weight: normal;
                  margin-bottom: 10px;
                  Margin-bottom: 10px;
                }
            
                h2 {
                  font-size: 30px;
                }
            
                body,
                table.body,
                td,
                th {
                  font-size: 16px;
                  line-height: 1.3;
                }
            
                @media only screen and (max-width: 596px) {
                  table.body center {
                    min-width: 0 !important;
                  }
                }
                table {
                  font-family: arial, sans-serif;
                  border-collapse: collapse;
                  width: 100%;
                }
                table {
                  font-family: arial, sans-serif;
                  border-collapse: collapse;
                  width: 100%;
                }
              
                td,
                th {
                  border: 1px solid #afafaf;
                  text-align: left;
                  padding: 8px;
                }
              
                tr:nth-child(even) {
                  background-color: #f9f9f9;
                }
            
                .header > .list1{
                  background: #80DEEA;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                  display: table-cell;
                  vertical-align: middle;
                }
                .header > .list2{
                  background: #81D4FA;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                  display: table-cell;
                  vertical-align: middle;
                }
                .subheader > .list1{
                  background: #4DD0E1;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                }
                .subheader > .list2{
                  background: #4FC3F7;
                  color: #0a0a0a;
                  font-weight: bold;
                  text-align: center;
                  border: 1px solid #afafaf;
                }
                .noborder{
                  border: none;
                }
            
                
              </style>
            </head>
            
            <body>
              <table class="body" data-made-with-foundation="">
                <tr>
                  <td class="float-center noborder" align="center" valign="top">
                    <center data-parsed="">';
                    if(mysqli_num_rows($res3) >= 1){

                      $body = $body.'<table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                           <h2 class="text-center">Priority - Unaccepted Leads</h2></td>
                      </tr></tbody></table>
                    
                    '.$priority_table;
                     }
                    if(mysqli_num_rows($res4) >= 1){

                      $body = $body.'<table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                           <h2 class="text-center">Priority - Non-allocated Rejected Leads</h2></td>
                      </tr></tbody></table>
                    
                    '.$priority_rejected_table;
                     }
                     $body = $body.'<table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                        <h2 class="text-center">Unaccepted Leads - Yesterday</h2></td>
                      </tr></tbody></table>

                      '.$idle_table;
                      if(mysqli_num_rows($res2) >= 1){

                       $body = $body.'<table align="center" class="row float-center"><tbody><tr><td style="border-color:white !important;">
                            <h2 class="text-center">Non-allocated Rejected Leads - Yesterday</h2></td>
                       </tr></tbody></table>
                     
                     '.$rejected_table;
                      }

                      $body = $body.'</center>
                      </td>
                    </tr>
                  </table>
                </body>
                </html>';
            $body = preg_replace("[\\\]",'',$body);
            $mail1->MsgHTML($body);

            //recipient
            $mail1->AddAddress($to1,'UnAccepted Leads Report');
            $mail1->AddBCC("sathish.eri@gobumpr.com","Sathish Eri");
            $mail1->AddBCC("sundar@gobumpr.com","Sundar");
            $mail1->AddBCC("mukund.srivathsan@gobumpr.com","Mukund");

            if ($mail1->Send()) {
                echo "<br>Mail sent!<br>";
            }
        }  
}// if results exists
else{
    echo '<h3 align="center" style="margin-top:140px;">Sorry! No Unaccepted bookings found!!.</h3>';
}
?>
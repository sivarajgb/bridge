<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if ((empty($_SESSION['crm_log_id']))) {
	header('location:index.php');
	die();
}
if ((empty($_SESSION['modal_sess']))) {
	$_SESSION['modal_sess'] = 'open';
}
$crm_log_id = $_SESSION['crm_log_id'];
$ticket_admin = array('crm029', 'crm107', 'crm040', 'crm202');
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>GoBumpr Bridge</title>
	
	
	<style>
		
		html {
			--mdc-theme-primary: #000 !important;
			min-height: 100%;
		}
		
		* {
			box-sizing: border-box;
		}
		
		body {
			color: black;
			background: rgb(255, 255, 255) !important;
			margin: 0px;
			min-height: inherit;
		}
		
		[data-sidebar-overlay] {
			display: none;
			position: fixed;
			top: 0px;
			bottom: 0px;
			left: 0px;
			opacity: 0;
			width: 100%;
			min-height: inherit;
		}
		
		.overlay {
			background-color: rgb(222, 214, 196);
			z-index: 999990 !important;
		}
		
		aside {
			position: relative;
			height: 100%;
			width: 200px;
			top: 0px;
			left: 0px;
			background-color: rgb(236, 239, 241);
			box-shadow: rgba(0, 0, 0, 0.8) 0px 6px 6px 0px;
			z-index: 999999 !important;
		}
		
		[data-sidebar] {
			display: none;
			position: absolute;
			height: 100%;
			z-index: 100;
		}
		
		.padding {
			padding: 2em;
		}
		
		.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
			font-family: inherit;
			font-weight: 500;
			line-height: 1.1;
			color: inherit;
		}
		
		.h4, .h5, .h6, h4, h5, h6 {
			margin-top: 10px;
			margin-bottom: 10px;
		}
		
		.h4, h4 {
			font-size: 18px;
		}
		
		img {
			border: 0px;
			vertical-align: middle;
		}
		
		a {
			text-decoration: none;
			color: black;
		}
		
		aside a {
			color: rgb(0, 0, 0);
			font-size: 16px;
			text-decoration: none;
		}
		
		.fa {
			display: inline-block;
			font-style: normal;
			font-variant: normal;
			font-weight: normal;
			font-stretch: normal;
			line-height: 1;
			font-family: FontAwesome;
			font-size: inherit;
			text-rendering: auto;
			-webkit-font-smoothing: antialiased;
		}
		
		nav, ol {
			font-size: 18px;
			margin-top: -4px;
			background: rgb(0, 150, 136) !important;
		}
		
		.container-fluid {
			padding-right: 15px;
			padding-left: 15px;
			margin-right: auto;
			margin-left: auto;
		}
		
		.breadcrumb > li {
			display: inline-block;
		}
		
		ol, ul {
			margin-top: 0px;
			margin-bottom: 10px;
		}
		
		ol ol, ol ul, ul ol, ul ul {
			margin-bottom: 0px;
		}
		
		.nav {
			padding-left: 0px;
			margin-bottom: 0px;
			list-style: none;
		}
		
		.navbar-nav {
			margin: 0px;
			float: left;
		}
		
		.navbar-right {
			margin-right: -15px;
			float: right !important;
		}
		
		.nav > li {
			position: relative;
			display: block;
		}
		
		.navbar-nav > li {
			float: left;
		}
		
		.dropdown {
			position: relative;
			display: inline-block;
		}
		
		.form-group {
			margin-bottom: 15px;
		}
		
		button, input, optgroup, select, textarea {
			margin: 0px;
			font-style: inherit;
			font-variant: inherit;
			font-weight: inherit;
			font-stretch: inherit;
			font-size: inherit;
			line-height: inherit;
			font-family: inherit;
			color: inherit;
		}
		
		button, select {
			text-transform: none;
		}
		
		button, input, select, textarea {
			font-family: inherit;
			font-size: inherit;
			line-height: inherit;
		}
		
		.form-control {
			display: block;
			width: 100%;
			height: 34px;
			padding: 6px 12px;
			font-size: 14px;
			line-height: 1.42857;
			color: rgb(85, 85, 85);
			background-color: rgb(255, 255, 255);
			background-image: none;
			border: 1px solid rgb(204, 204, 204);
			border-radius: 4px;
			box-shadow: rgba(0, 0, 0, 0.075) 0px 1px 1px inset;
			transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
		}
		
		.navbar-fixed-bottom, .navbar-fixed-top {
			position: fixed;
			right: 0px;
			left: 0px;
			z-index: 1030;
			border-radius: 0px;
		}
		
		.navbar-fixed-top {
			top: 0px;
			border-width: 0px 0px 1px;
		}
		
		.btn-group, .btn-group-vertical {
			position: relative;
			display: inline-block;
			vertical-align: middle;
		}
		
		label {
			cursor: pointer;
		}
		
		b, strong {
			font-weight: 700;
		}
		
		input {
			line-height: normal;
		}
		
		input[type="checkbox"], input[type="radio"] {
			box-sizing: border-box;
			padding: 0px;
			margin: 4px 0px 0px;
			line-height: normal;
		}
		
		[data-toggle="buttons"] > .btn input[type="checkbox"], [data-toggle="buttons"] > .btn input[type="radio"], [data-toggle="buttons"] > .btn-group > .btn input[type="checkbox"], [data-toggle="buttons"] > .btn-group > .btn input[type="radio"] {
			position: absolute;
			clip: rect(0px 0px 0px 0px);
			pointer-events: none;
		}
		
		p {
			margin: 0px 0px 10px;
		}
		
		.col-sm-offset-1 {
			margin-left: 8.33333%;
		}
		
		.col-lg-offset-1 {
			margin-left: 8.33333%;
		}
		
		.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
			position: relative;
			min-height: 1px;
			padding-right: 15px;
			padding-left: 15px;
		}
		
		.col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
			float: left;
		}
		
		.col-xs-2 {
			width: 16.6667%;
		}
		
		.col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9 {
			float: left;
		}
		
		.col-sm-2 {
			width: 16.6667%;
		}
		
		.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9 {
			float: left;
		}
		
		.col-lg-1 {
			width: 8.33333%;
		}
		
		.floating-box1 {
			display: inline-block;
			margin-top: 10px;
			float: left;
			clear: both;
		}
		
		.col-sm-3 {
			width: 25%;
		}
		
		.col-lg-3 {
			width: 25%;
		}
		
		.glyphicon {
			position: relative;
			top: 1px;
			display: inline-block;
			font-family: "Glyphicons Halflings";
			font-style: normal;
			font-weight: 400;
			line-height: 1;
			-webkit-font-smoothing: antialiased;
		}
		
		.caret {
			display: inline-block;
			width: 0px;
			height: 0px;
			margin-left: 2px;
			vertical-align: middle;
			border-top: 4px dashed;
			border-right: 4px solid transparent;
			border-left: 4px solid transparent;
		}
		
		.col-sm-1 {
			width: 8.33333%;
		}
		
		.col-sm-4 {
			width: 33.3333%;
		}
		
		button {
			overflow: visible;
		}
		
		button, html input[type="button"], input[type="reset"], input[type="submit"] {
			-webkit-appearance: button;
			cursor: pointer;
		}
		
		.btn {
			display: inline-block;
			padding: 6px 12px;
			margin-bottom: 0px;
			font-size: 14px;
			font-weight: 400;
			line-height: 1.42857;
			text-align: center;
			white-space: nowrap;
			vertical-align: middle;
			touch-action: manipulation;
			cursor: pointer;
			user-select: none;
			background-image: none;
			border: 1px solid transparent;
			border-radius: 4px;
		}
		
		.pull-right {
			float: right;
		}
		
		.form-control:focus {
			border-color: rgb(102, 175, 233);
			outline: 0px;
			box-shadow: rgba(0, 0, 0, 0.075) 0px 1px 1px inset, rgba(102, 175, 233, 0.6) 0px 0px 8px;
		}
		
		.counter {
			padding: 8px;
			color: rgb(204, 204, 204);
		}
		
		.fade {
			opacity: 0;
			transition: opacity 0.15s linear;
		}
		
		.modal {
			position: fixed;
			top: 0px;
			right: 0px;
			bottom: 0px;
			left: 0px;
			z-index: 1050;
			display: none;
			overflow: hidden;
			outline: 0px;
		}
		
		.modal.fade .modal-dialog {
			transition: transform 0.3s ease-out;
			transform: translate(0px, -25%);
		}
		
		.modal-dialog {
			position: relative;
			width: 600px;
			margin: 30px auto;
		}
		
		.modal-content {
			position: relative;
			background-color: rgb(255, 255, 255);
			-webkit-background-clip: padding-box;
			background-clip: padding-box;
			border: 1px solid rgba(0, 0, 0, 0.2);
			border-radius: 6px;
			outline: 0px;
			box-shadow: rgba(0, 0, 0, 0.5) 0px 5px 15px;
		}
		
		.modal-header {
			padding: 15px;
			border-bottom: 1px solid rgb(229, 229, 229);
		}
		
		.close {
			float: right;
			font-size: 21px;
			font-weight: 700;
			line-height: 1;
			color: rgb(0, 0, 0);
			text-shadow: rgb(255, 255, 255) 0px 1px 0px;
			opacity: 0.2;
		}
		
		button.close {
			-webkit-appearance: none;
			padding: 0px;
			cursor: pointer;
			background: 0px 0px;
			border: 0px;
		}
		
		.modal-header .close {
			margin-top: -2px;
		}
		
		.h1, .h2, .h3, h1, h2, h3 {
			margin-top: 20px;
			margin-bottom: 10px;
		}
		
		.h3, h3 {
			font-size: 24px;
		}
		
		.modal-title {
			margin: 0px;
			line-height: 1.42857;
		}
		
		.modal-body {
			position: relative;
			padding: 15px;
		}
		
		.col-sm-offset-3 {
			margin-left: 25%;
		}
		
		.col-lg-6 {
			width: 50%;
		}
		
		.col-lg-offset-3 {
			margin-left: 25%;
		}
		
		.uil-default-css {
			position: relative;
			background: none;
			width: 200px;
			height: 200px;
		}
		
		.uil-default-css > div:nth-of-type(1) {
			animation: uil-default-anim 1s linear -0.5s infinite;
		}
		
		.uil-default-css > div:nth-of-type(2) {
			animation: uil-default-anim 1s linear -0.416667s infinite;
		}
		
		.uil-default-css > div:nth-of-type(3) {
			animation: uil-default-anim 1s linear -0.333333s infinite;
		}
		
		.uil-default-css > div:nth-of-type(4) {
			animation: uil-default-anim 1s linear -0.25s infinite;
		}
		
		.uil-default-css > div:nth-of-type(5) {
			animation: uil-default-anim 1s linear -0.166667s infinite;
		}
		
		.uil-default-css > div:nth-of-type(6) {
			animation: uil-default-anim 1s linear -0.0833333s infinite;
		}
		
		.uil-default-css > div:nth-of-type(7) {
			animation: uil-default-anim 1s linear 0s infinite;
		}
		
		.uil-default-css > div:nth-of-type(8) {
			animation: uil-default-anim 1s linear 0.0833333s infinite;
		}
		
		.uil-default-css > div:nth-of-type(9) {
			animation: uil-default-anim 1s linear 0.166667s infinite;
		}
		
		.uil-default-css > div:nth-of-type(10) {
			animation: uil-default-anim 1s linear 0.25s infinite;
		}
		
		.uil-default-css > div:nth-of-type(11) {
			animation: uil-default-anim 1s linear 0.333333s infinite;
		}
		
		.uil-default-css > div:nth-of-type(12) {
			animation: uil-default-anim 1s linear 0.416667s infinite;
		}
		
		table {
			border-spacing: 0px;
			border-collapse: collapse;
			background-color: transparent;
		}
		
		.table {
			width: 100%;
			max-width: 100%;
			margin-bottom: 20px;
		}
		
		.table-bordered {
			border: 1px solid rgb(221, 221, 221);
		}
		
		td, th {
			padding: 0px;
		}
		
		th {
			text-align: left;
		}
		
		.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
			padding: 8px;
			line-height: 1.42857;
			vertical-align: top;
			border-top: 1px solid rgb(221, 221, 221);
		}
		
		.table > thead > tr > th {
			vertical-align: bottom;
			border-bottom: 2px solid rgb(221, 221, 221);
		}
		
		.table > caption + thead > tr:first-child > td, .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table > thead:first-child > tr:first-child > th {
			border-top: 0px;
		}
		
		.table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
			border: 1px solid rgb(221, 221, 221);
		}
		
		.table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
			border-bottom-width: 2px;
		}
		
		#tbody {
			font-size: 15px !important;
			border: 1.5px solid rgb(196, 184, 184) !important;
		}
		
		#tbody, tbody tr {
			animation: opacity 5s ease-in-out;
		}
		
		.btn-info {
			color: rgb(255, 255, 255);
			background-color: rgb(91, 192, 222);
			border-color: rgb(70, 184, 218);
		}
		
		.col-xs-8 {
			width: 66.6667%;
		}
		
		.col-xs-offset-2 {
			margin-left: 16.6667%;
		}
		
		.modal-footer {
			padding: 15px;
			text-align: right;
			border-top: 1px solid rgb(229, 229, 229);
		}
		
		.dropdown-menu {
			position: absolute;
			top: 100%;
			left: 0px;
			z-index: 1000;
			display: none;
			float: left;
			min-width: 160px;
			padding: 5px 0px;
			margin: 2px 0px 0px;
			font-size: 14px;
			text-align: left;
			list-style: none;
			background-color: rgb(255, 255, 255);
			-webkit-background-clip: padding-box;
			background-clip: padding-box;
			border: 1px solid rgba(0, 0, 0, 0.15);
			border-radius: 4px;
			box-shadow: rgba(0, 0, 0, 0.176) 0px 6px 12px;
		}
		
		button[disabled], html input[disabled] {
			cursor: default;
		}
		
		.btn.disabled, .btn[disabled], fieldset[disabled] .btn {
			cursor: not-allowed;
			box-shadow: none;
			opacity: 0.65;
		}
		
		.btn-success {
			color: rgb(255, 255, 255);
			background-color: rgb(92, 184, 92);
			border-color: rgb(76, 174, 76);
		}
		
		.btn-group-sm > .btn, .btn-sm {
			padding: 5px 10px;
			font-size: 12px;
			line-height: 1.5;
			border-radius: 3px;
		}
		
		.btn-default {
			color: rgb(51, 51, 51);
			background-color: rgb(255, 255, 255);
			border-color: rgb(204, 204, 204);
		}
		
		.body {
			overflow-x: scroll !important;
		}
		
		.modal-backdrop {
			opacity: 0.85 !important;
		}
		
		#datepick > span:hover {
			cursor: pointer;
		}
		
		.top-box {
			display: inline-block;
			z-index: 1;
			height: 30px;
			width: 90px;
			background-color: #B6A3BF;
			-webkit-display: button;
			box-shadow: 0px 8px 8px 0px rgba(0, 0, 0, 0.2);
		}
		
		.floating-box {
			display: inline-block;
			margin: 10px;
			padding: 12px;
			width: 183px;
			height: 75px;
			box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
			z-index: 1;
		}
		
		.floating-box1 {
			display: inline-block;
			margin-top: 10px;
			float: left;
			clear: both;
		}
		
		input[] {
			visibility: hidden;
		}
		
		label {
			cursor: pointer;
		}
		
		input:checked + label {
			background: red;
		}
		
		/* table */
		#tbody {
			font-size: 15px !important;
			border: 1.5px solid #c4b8b8 !important;
			
		}
		
		thead:hover {
			cursor: pointer;
		}
		
		.results tr[visible='false'],
		.no-result {
			display: none;
		}
		
		.results tr[visible='true'] {
			display: table-row;
		}
		
		.counter {
			padding: 8px;
			color: #ccc;
		}
		
		#tbody, tbody tr {
			-webkit-animation: opacity 5s ease-in-out;
			animation: opacity 5s ease-in-out;
		}
		
		p[data-title]:hover:after {
			content: attr(data-title);
			padding: 4px 8px;
			color: #333;
			position: absolute;
			left: 0;
			top: 100%;
			white-space: nowrap;
			z-index: 20px;
			-moz-border-radius: 5px;
			-webkit-border-radius: 5px;
			border-radius: 5px;
			-moz-box-shadow: 0px 0px 4px red;
			-webkit-box-shadow: 0px 0px 4px red;
			box-shadow: 0px 0px 4px #222;
			background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
			background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #eeeeee), color-stop(1, #cccccc));
			background-image: -webkit-linear-gradient(top, #eeeeee, #cccccc);
			background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
			background-image: -ms-linear-gradient(top, #eeeeee, #cccccc);
			background-image: -o-linear-gradient(top, #eeeeee, #cccccc);
		}
	</style>

</head>
<body>
<?php include_once("header.php"); ?>

<div class="overlay" data-sidebar-overlay></div>


<div class="padding">
</div>


<!-- get crm names -->
<!--<div  class="navbar-fixed-top" style="margin-top:52px; padding-bottom:20px; padding-top:2px; background-color:#fff;" align="center">
     
<div id="crm_list" >
</div><!-- select box -->

<!--</div>-->

<div class="navbar-fixed-top"
	 style=" margin-top:57px;border-top:52px;padding-top:5px;background-color:#fff;z-index:1037;">
	<!-- service type filter -->
	<div style="max-width:130px;float:left;margin-left:25px;">
		<select id="service" name="service" class="form-control" style="width:130px;">
			<option value="all" selected>All Services</option>
			<?php
			$sql_service = "SELECT service_type FROM go_axle_service_price_tbl WHERE bridge_flag='0' ORDER BY service_type ASC";
			$res_service = mysqli_query($conn, $sql_service);
			while ($row_service = mysqli_fetch_object($res_service)) {
				$c_service = $row_service->service_type;
				?>
				<option value="<?php echo $c_service; ?>"> <?php echo $c_service; ?> </option>
				<?php
			}
			?>
		</select>
	</div>
</div>

<div class="navbar-fixed-top" style=" margin-top:95px; padding-bottom:8px; background-color:#fff;">
	
	<!-- source filter -->
	<div class=" col-xs-2 col-sm-2 col-lg-1" style="margin-left:10px;">
		<div class="floating-box1">
			<div style="max-width:140px;">
				<select id="tsource" name="tsource" class="form-control" style="width:120px;">
					<option value="all" selected>All Tickets</option>
					<option value="Feedback Call">Feedback Call</option>
					<option value="FB">Facebook</option>
					<option value="Google Biz">Google Business</option>
					<option value="Playstore">Google Playstore</option>
					<option value="Email">Email</option>
					<option value="On Call">On Call</option>
					<option value="Hotline">Hotline Chat</option>
				</select>
			</div>
		</div>
	</div>
	
	<!-- vehicle filter -->
	<div class=" col-sm-2 col-lg-1" style="margin-left: 52px;">
		<div class="floating-box1">
			<div style="max-width:140px;">
				<select id="vehicle" name="vehicle" class="form-control" style="width:140px;">
					<option value="all" selected>All Vehicles</option>
					<option value="2w">2 Wheeler</option>
					<option value="4w">4 Wheeler</option>
				</select>
			</div>
		</div>
	</div>
	
	<!-- date range picker -->
	<div id="reportrange" class=" col-sm-3 col-lg-3" style="cursor: pointer; margin-left:50px;">
		<div class=" floating-box1">
			<div id="range" class="form-control" style="min-width:312px;">
				<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
				<span id="dateval"></span> <b class="caret"></b>
			</div>
		</div>
	</div>
	
	<!-- alloted to  filter -->
	<div class=" col-sm-1 col-lg-1" style="margin-left:20px;">
		<div class="floating-box1">
			<div style="max-width:130px;">
				<select id="person" name="person" class="form-control" style="width:130px;">
				</select>
			</div>
		</div>
	</div>
	
	<!-- search bar -->
	<div class=" col-sm-4 ">
		
		<div class="floating-box1">
			<form id="myForm" method="post" action="check_ticket.php" onsubmit="return ValidationEvent()">
				<?php
				if ($flag == '1' || in_array($crm_log_id, $ticket_admin)) {
					?>
					<span class="form-group pull-right btn btn-small"
						  style=" width:140px;height:34px; margin-left:6px;background-color:#B2DFDB;font-size:15px;"
						  data-toggle="modal" data-target="#initialModal"><i class="fa fa-plus" aria-hidden="true"
																			 style="padding-top:3px;padding-right:5px;"></i>Add new Ticket</span>
				<?php } ?>
				<button class="form-group pull-right btn btn-small" type="submit" id="search_submit"
						name="search_submit"
						style=" width:50px;height:34px; margin-left:6px;background-color:#B2DFDB;font-size:15px;"><i
							class="fa fa-search" aria-hidden="true"></i></button>
				
				<div class="form-group pull-right">
					<input type="text" class="search form-control" id="su" name="searchuser" placeholder="Search">
					<input type="hidden" id="search_type" name="search_type" value="">
				</div>
				
				<span class="counter pull-right" style="margin-left:28px;"></span>
		</div>
		
		</form>
	</div>
</div>


<!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
	<div class='uil-default-css' style='transform:scale(0.58);'>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
	</div>
</div>

<div align="center" id="table" style="margin-top:140px;margin-left:10px;margin-right:10px; display: none;">
	<table id="example" class="table table-striped table-bordered tablesorter table-hover results">
		<thead style="background-color: #D3D3D3;" class="row">
		<!--<th>No</th>-->
		<th style="vertical-align:middle;"><p style="margin:0px!important;width:110px;text-align:center;">Ticket ID</p>
		</th>
		<th style="vertical-align:middle;"><p style="margin:0px!important;width:110px;text-align:center;">Booking ID</p>
		</th>
		<th style="vertical-align:middle;"><p style="margin:0px!important;width:110px;text-align:center;">Ticket
				Status</p></th>
		<th style="vertical-align:middle;"><p style="margin:0px!important;width:110px;text-align:center;">Customer
				Name</p></th>
		<?php if (!$_SESSION['eupraxia_flag']) { ?>
			<th style="vertical-align:middle;"><p style="margin:0px!important;width:110px;text-align:center;">Mobile</p>
			</th>
		<?php } ?>
		<!--<th style="vertical-align:middle;"><p style="margin:0px!important;width:110px;text-align:center;">Locality</p></th>-->
		<th style="vertical-align:middle;"><p style="margin:0px!important;width:110px;text-align:center;">Source</p>
		</th>
		<th style="vertical-align:middle;"><p style="margin:0px!important;width:110px;text-align:center;">Service
				Type</p></th>
		<th style="vertical-align:middle;"><p style="margin:0px!important;width:110px;text-align:center;">Shop Name</p>
		</th>
		<th style="vertical-align:middle;"><p style="margin:0px!important;width:110px;text-align:center;">GoAxle</p>
		</th>
		<th style="vertical-align:middle;"><p style="margin:0px!important;width:110px;text-align:center;">Customer
				Grievance</p></th>
		<th style="vertical-align:middle;"><p style="margin:0px!important;width:110px;text-align:center;">Allotted
				To</p></th>
		<th style="vertical-align:middle;"><p style="margin:0px!important;width:110px;text-align:center;">Last Contacted
				On</p></th>
		<th style="vertical-align:middle;"><p style="margin:0px!important;width:110px;text-align:center;">Closure
				Date</p></th>
		<th style="vertical-align:middle;"><p style="margin:0px!important;width:110px;text-align:center;">Ticket Entry
				Date</p></th>
		</thead>
		<tbody id="tbody">
		</tbody>
	</table>
</div>
<!-- Modal -->
<div class="modal fade" id="initialModal" role="dialog" style="top: 20%;">
	<div class="modal-dialog">
		
		<!-- Modal content-->
		<div class="modal-content" style="border-radius:25px;">
			<!--<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			  
			</div>-->
			<div class="modal-body">
				<div data-toggle="modal" data-target="#addNewModal" data-dismiss="modal" class="floating-box"
					 style="cursor:pointer;background-color:#009688;border-radius: 20px;text-align: center;height:104px;">
					<p style="margin: 20px 12px 24px 12px!important;width:auto;font-size: 18px;"><i class="fa fa-ticket"
																									aria-hidden="true"></i>&nbsp;&nbsp;Raise
						New Ticket</p>
				</div>
				<div class="floating-box" style="height: 39px;width: 129px;box-shadow:none;">
				</div>
				<div class="floating-box"
					 style="background-color:#009688;border-radius: 20px;text-align: center;height:104px;position: absolute;left: 64%;">
					<label style="margin: 12px 12px 14px 0px;font-size: 16px;"><i style="padding-right:15px;"
																				  class="fa fa-ticket"
																				  aria-hidden="true"></i><span>Check Status</label>
					</span>
					<form action='check_ticket.php' method='post'>
						<div class="mdc-text-field" style="width:130px;">
							<input type="text" id="my-text-field" class="mdc-text-field__input" name="searchuser"
								   placeholder="Ticket No.">
							<button class="form-group btn btn-small" type="submit"
									style=" width:50px;height:34px; margin-top:-35px;margin-left:93px;background-color:rgba(255, 255, 255, 0);font-size:15px;">
								<i class="fa fa-search" aria-hidden="true"></i></button>
							</span>
							<div class="mdc-text-field__bottom-line"></div>
						</div>
						<!--<input class="form-control" type="text" name="searchuser" placeholder="Ticket No.">-->
						<!--<input type="submit" style="display:none;">-->
						<input type="hidden" id="search_type2" name="search_type" value="ticket">
					</form>
				</div>
			</div>
			<!--<div class="modal-footer">
			
			</div>-->
		</div>
	
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="addNewModal" role="dialog" style="top: -20px;">
	<div class="modal-dialog">
		
		<!-- Modal content-->
		<div class="modal-content" style="border-radius:15px;">
			<div class="modal-header"
				 style="background-color:#009688;border-top-right-radius:15px;border-top-left-radius:15px;">
				<button type="button" class="close" data-dismiss="modal" style="font-size:36px;">&times;</button>
				<strong><h4 class="modal-title" style="text-align:center;color:white;">Raise New Ticket</h4></strong>
			</div>
			<div class="modal-body">
				
				<!--<form id="bookingid_search" action="" method="get">-->
				<form id="bookingid_search" method="get" action="" onsubmit="return ValidationEvent1()">
					<div class="row">
						<br>
						<div class="col-xs-5 form-group">
							<input class="form-control" type="text" id="searchVal" name="searchVal"
								   placeholder="Booking Id / Mobile No / E-mail">
							<input type="hidden" id="search_type1" name="search_type1" value="">
						</div>
						<div class="col-xs-1 form-group">
							<button class="form-group btn btn-small" type="submit"
									style=" width:50px;height:34px; margin-left:-26px;background-color:#B2DFDB;font-size:15px;">
								<i class="fa fa-search" aria-hidden="true"></i></button>
							</span>
							<!--<input type="submit" style="display:none;">-->
						</div>
				</form>
				<form id="create_ticket" action="" method="get">
					<div class="col-xs-5 form-group">
						<div class="ui-widget">
							<select class="form-control" id="source" type="text" name="source" style="display:none;"
									required>
								<option value="" selected><span style="color:#ccc;">- Choose Source -</span></option>
								<option value="Feedback Call">Feedback Call</option>
								<option value="FB">Facebook</option>
								<option value="Google Biz">Google Business</option>
								<option value="Playstore">Google Playstore</option>
								<option value="Email">Email</option>
								<option value="On Call">On Call</option>
								<option value="Hotline">Hotline Chat</option>
							</select>
						</div>
					</div>
			</div>
			<div class="row"></div>
			<div class="row">
				<div class="col-xs-11 form-group">
					<textarea class="form-control" rows="4" id="issue_faced" style="display:none;"
							  placeholder="Issue faced.." required></textarea>
				</div>
			</div>
			<div id="show_booking" style="display:none;">
				<hr>
				<table id="table1" class="table borderless">
				</table>
			</div>
			<div id="show_bookings" style="display:none;">
			</div>
		</div>
		<div class="modal-footer" id='insertAfter'>
			<center><input style="display:none;" id="create" type="submit" class="btn btn-success" value="Create !"/>
			</center>
			<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
			</form>
		</div>
	</div>

</div>
</div>


<noscript id="async-styles">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
		  integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
		  integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css"/>
	<link rel="stylesheet" href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css">

</noscript>
<script> // The CSS mentioned in "async-styles" will be loaded and styles will be applied.
    var loadDeferredStyles = function () {
        var addStylesNode = document.getElementById("async-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
        webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function () {
        window.setTimeout(loadDeferredStyles, 0);
    });
    else window.addEventListener('load', loadDeferredStyles);
</script>

<script async src="js/jquery-3.2.1.min.js" onLoad="loadOtherScripts();"></script>

<script>
    function loadScript(src) {
        return new Promise(function (resolve, reject) {
            var s;
            s = document.createElement('script');
            s.src = src;
            s.onload = resolve;
            s.onerror = reject;
            document.head.appendChild(s);
        });
    }

    function loadOtherScripts() {
        loadScript('js/moment.min.js')
            .then(function () {
                Promise.all([ /*loadScript('https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js'),*/loadScript('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'), loadScript('js/daterangepicker.js'), loadScript('js/sidebar.js'), loadScript('https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js')]).then(function () {
                    // console.log('scripts are loaded');
                    dateRangePicker();
                    momentInPicker();


                    intialFunction();

                    tablesort();
                    searchbar();
                    modal();
                }).catch(function (error) {
                    //  console.log('some error!' + error)
                })
            }).catch(function (error) {
            //  console.log('Moment call error!' + error)
        })
    }
</script>

<!-- table sorter -->
<script>
    function tablesort() {
        $(document).ready(function () {
                $("#example").tablesorter({sortList: [11, 0]});
            }
        );
    }
</script>
<!-- search bar  -->
<script>
    function searchbar() {
        $(document).ready(function () {
            $(".search").keyup(function () {
                var searchTerm = $(".search").val();
                var listItem = $('.results tbody').children('tr');
                var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

                $.extend($.expr[':'], {
                    'containsi': function (elem, i, match, array) {
                        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
                    }
                });

                $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function (e) {
                    $(this).attr('visible', 'false');
                });

                $(".results tbody tr:containsi('" + searchSplit + "')").each(function (e) {
                    $(this).attr('visible', 'true');
                });

                var jobCount = $('.results tbody tr[visible="true"]').length;
                $('.counter').text(jobCount + ' item');

                if (jobCount == '0') {
                    $('.no-result').show();
                } else {
                    $('.no-result').hide();
                }
            });
        });
    }
</script>

<!-- date range picker -->
<script>
    function dateRangePicker() {
        $('input[name="daterange"]').daterangepicker({
            locale: {
                format: 'DD-MM-YYYY'
            }
        });
    }
</script>
<script type="text/javascript">
    function momentInPicker() {
        $(function () {

            var start = moment().subtract(29, 'days');
            // var start = moment();
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

        });
    }
</script>
<!-- view table -->
<script>
    function viewtable() {
        var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
        var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
        var person = $('#person').val();
        var source = $('#tsource').val();
        var vehicle = $('#vehicle').val();
        var service = $('#service').val();
        var city = $('#city').val();
        var cluster = $('#cluster').val();
        //console.log(city);
        //Make AJAX request, using the selected value as the POST

        $.ajax({
            url: "ajax/ticket_tracker_view.php",  // create a new php page to handle ajax request
            type: "GET", json: false,
            data: {
                "startdate": startDate,
                "enddate": endDate,
                "person": person,
                "source": source,
                "vehicle": vehicle,
                "service": service,
                "city": city,
                "cluster": cluster
            },
            success: function (data) {
                // console.log(data);
                $("#loading").hide();
                $("#table").show();
                $("#tbody").empty();
                if (data === "no") {
                    $("#tbody").html("<h2 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h2>");
                } else {
                    //console.log(data);
                    function displayInfo(item, index) {
                        $('#tbody').append(item['tr']);
                        //console.log(item);
                    }

                    $.map($.parseJSON(data), displayInfo);
                    //  $('#tbody').html(data);
                    $("#example").trigger("update");
                }


                counterval();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                //  alert(xhr.status + " "+ thrownError);
            }
        });
    }
</script>
<script>
    function counterval() {
        var jobCount = $("#tbody tr").length;
        $('.counter').text(jobCount + ' item');
    }
</script>

<script>
    function getpeople() {
        //console.log("Get people");
        var city = $('#city').val();
        var cluster = $('#cluster').val();
        //console.log(city);
        /*$('#crm_list').empty();
		$.ajax({
					url : "ajax/get_people.php",  // create a new php page to handle ajax request
					type : "GET", json : false,
					data : {"city":city , "cluster" : cluster},
					success : function(data) {
				  $('#crm_list').html(data);
				  //console.log(data);
				},
				  error: function(xhr, ajaxOptions, thrownError) {
				  //  alert(xhr.status + " "+ thrownError);
				 }
			   });*/
        $('#person').empty();
        $.ajax({
            url: "ajax/get_people_filter.php",  // create a new php page to handle ajax request
            type: "GET", json: false,
            data: {"city": city, "cluster": cluster},
            success: function (data) {
                $('#person').append(data);
                //console.log(data);
                viewtable();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                //  alert(xhr.status + " "+ thrownError);
            }
        });
    }
</script>
<!-- automatic refresh -->
<script>
    function intialFunction() {
        $(document).ready(function () {
            $('#city').show();
            $('#cluster').show();
            $("#table").hide();
            $("#loading").show();
            getpeople();

            $("#person").change(function () {
                $("#table").hide();
                $("#loading").show();

                viewtable();
            });

            $('#dateval').on("DOMSubtreeModified", function () {
                $("#table").hide();
                $("#loading").show();
                viewtable();
            });

            $("#tsource").change(function () {
                $("#table").hide();
                $("#loading").show();
                viewtable();
            });

            $("#service").change(function () {
                $("#table").hide();
                $("#loading").show();
                viewtable();
            });

            $("#vehicle").change(function () {
                $("#table").hide();
                $("#loading").show();
                viewtable();
            });

            $('#city').change(function () {
                $("#table").hide();
                $("#loading").show();
                getpeople();
            });
            $('#cluster').change(function () {
                $("#table").hide();
                $("#loading").show();
                getpeople();
            });

            $("#selectall").click(function () {
                if (document.getElementById('selectall').checked) {
                    //console.log("checked");
                    var check_boxes = [];
                    $("input[name='check_veh[]']").each(function () {
                        this.checked = true;
                    });
                } else {
                    //  console.log("unchecked");
                    var check_boxes = [];
                    $("input[name='check_veh[]']").each(function () {
                        this.checked = false;
                    });
                }
            });

        });
        window.setInterval(function () {
            viewtable();
        }, 240000);

        //check online status
        function when_online() {
            document.title = "GoBumpr Bridge";
            $("#table").hide();
            $("#loading").show();
            viewtable();
        }

        function when_offline() {
            document.title = "You're Offline!";
            alert("You're Offline! Please check your internet connection");
        }

        // Update the online status icon based on connectivity
        window.addEventListener('online', when_online);
        window.addEventListener('offline', when_offline);
    }

</script>

<!-- search/add user -->
<script type="text/javascript">
    function ValidationEvent() {
        event.preventDefault();
        var mobile = document.getElementById("su").value;
        var phoneno = /^[1-9][0-9]*$/;
        // var emailid = /\S+@\S+\.\S+/;
        var emailid = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        var name = /^[a-zA-Z\s]+$/;

        //console.log(phoneno.test(mobile))
        if (phoneno.test(mobile)) {
            if ((mobile.length >= 7 && mobile.length < 10) || mobile.length > 10 || mobile.length <= 4) {
                document.getElementById("search_type").value = "";
                alert("Please enter a valid TicketId / Mobile number !");
            } else if (mobile.length <= 7) {
                document.getElementById("search_type").value = "ticket";
                document.getElementById("myForm").submit();
            } else {
                document.getElementById("search_type").value = "mobile";
                document.getElementById("myForm").submit();
            }
        }
        /*else if(emailid.test(mobile)){
		  document.getElementById("search_type").value="email";
		  document.getElementById("myForm").submit();
		}
		else if(name.test(mobile)){
		  document.getElementById("search_type").value="username";
		  document.getElementById("myForm").submit();
		}*/
        else {
            document.getElementById("search_type").value = "";
            alert("Please enter a valid Ticket Id / Mobile Number !");
        }
    }

    function call(searchValue) {
        var bookingid = $('#searchVal').val();
        if (searchValue) {
            bookingid = searchValue;
            console.log(bookingid);
        }
        var search_type1 = $('#search_type1').val();
        $.ajax({
            url: "ajax/get_booking_detail.php",
            type: "GET",
            dataType: "json",
            data: {'bookingid': bookingid},
            success: function (data) {
                //alert(data);
                var html = "";
                console.log(data);
                if (data[0]['status'] == 'success') {
                    /*var d = new Date(data[0]['sent_log']);
					if(d.getDate()<10)
					{
						sent_date = '0';
					}
					months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'];
					sent_date += d.getDate()+'-'+months[d.getMonth()]+'-'+d.getFullYear();*/
                    html = "<tbody><tr><td style='padding-left:50px;border-top:none;'><strong>Booking ID</strong></td><td style='border-top:none;'>";
                    html += data[0]['booking_id'] + "</td></tr>";
                    html += "<tr><td style='padding-left:50px;border-top:none;'><strong>Name</strong></td><td style='border-top:none;'>";
                    html += data[0]['name'] + "</td></tr>";
                    html += "<tr><td style='padding-left:50px;border-top:none;'><strong>Phn No.</strong></td><td style='border-top:none;'>";
                    html += data[0]['mobile_number'] + "</td></tr>";
                    html += "<tr><td style='padding-left:50px;border-top:none;'><strong>Go Axle Status</strong></td><td style='border-top:none;'>";
                    html += data[0]['axle_flag'] + "</td></tr>";
                    html += "<tr><td style='padding-left:50px;border-top:none;'><strong>Service Type</strong></td><td style='border-top:none;'>";
                    html += data[0]['service_type'] + "</td></tr>";
                    html += "<tr><td style='padding-left:50px;border-top:none;'><strong>Shop Name<strong></strong></strong></td><td style='border-top:none;'>";
                    html += data[0]['shop_name'];
                    html += "<input type='hidden' id='user_id' name='user_id' value='" + data[0]['user_id'] + "'>";
                    html += "<input type='hidden' id='bookingid' name='bookingid' value='" + data[0]['booking_id'] + "'>";
                    html += "</td></tr></tbody>";
                    $('#create').show();
                    $('#source').show();
                    $('#issue_faced').show();
                    $('#show_bookings').hide();
                } else if (data[0]['status'] == 'fail') {
                    html = "No results found";
                    $('#source').hide();
                    $('#issue_faced').hide();
                    $('#show_bookings').hide();
                } else if (data[0]['status'] == 'exists') {
                    last_contact_date = '';
                    closure_date = '';
                    /* var d = new Date(data[0]['sent_log']);
					if(d.getDate()<10)
					{
						sent_date = '0';
					} */
                    var d1 = new Date(data[0]['update_log']);
                    if (d1.getDate() < 10) {
                        last_contact_date = '0';
                    }
                    if (data[0]['closure_date'] != 'Not Updated' && data[0]['closure_date'] != null) {
                        console.log(data[0]['closure_date']);
                        var d2 = new Date(data[0]['closure_date']);
                        if (d2.getDate() < 10) {
                            closure_date = '0';
                        }
                    }
                    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
                    //sent_date += d.getDate()+'-'+months[d.getMonth()]+'-'+d.getFullYear();
                    last_contact_date += d1.getDate() + '-' + months[d1.getMonth()] + '-' + d1.getFullYear();
                    if (data[0]['closure_date'] != 'Not Updated' && data[0]['closure_date'] != null) {
                        closure_date += d2.getDate() + '-' + months[d2.getMonth()] + '-' + d2.getFullYear();
                    } else {
                        closure_date = 'Not Updated'
                    }
                    html = "<tbody><tr><td style='padding-left:50px;border-top:none;'><strong>Booking ID</strong></td><td style='border-top:none;'>";
                    html += data[0]['booking_id'] + "</td></tr>";
                    html += "<tr><td style='padding-left:50px;border-top:none;'><strong>Name</strong></td><td style='border-top:none;'>";
                    html += data[0]['name'] + "</td></tr>";
                    html += "<tr><td style='padding-left:50px;border-top:none;'><strong>Phn No.</strong></td><td style='border-top:none;'>";
                    html += data[0]['mobile_number'] + "</td></tr>";
                    html += "<tr><td style='padding-left:50px;border-top:none;'><strong>Go Axle Status</strong></td><td style='border-top:none;'>";
                    html += data[0]['axle_flag'] + "</td></tr>";
                    html += "<tr><td style='padding-left:50px;border-top:none;'><strong>Service Type</strong></td><td style='border-top:none;'>";
                    html += data[0]['service_type'] + "</td></tr>";
                    html += "<tr><td style='padding-left:50px;border-top:none;'><strong>Shop Name<strong></strong></strong></td><td style='border-top:none;'>";
                    html += data[0]['shop_name'] + "</td></tr>";
                    html += "<tr><td style='padding-left:50px;border-top:none;'><strong>Ticket Status<strong></strong></strong></td><td style='border-top:none;'>";
                    html += data[0]['ticket_status'] + "</td></tr>";
                    html += "<tr><td style='padding-left:50px;border-top:none;'><strong>Last Contact Date<strong></strong></strong></td><td style='border-top:none;'>";
                    html += last_contact_date + "</td></tr>";
                    html += "<tr><td style='padding-left:50px;border-top:none;'><strong>Closure Date<strong></strong></strong></td><td style='border-top:none;'>";
                    html += closure_date + "</td></tr>";
                    html += "<tr><td colspan='2' style='padding-left:50px;border-top:none;'><center><a href='ticket_history.php?t=" + window.btoa(data[0]['ticket_id']) + "' class='btn btn-success'>View !</a></center>";
                    html += "<input type='hidden' id='user_id' name='user_id' value='" + data[0]['user_id'] + "'>";
                    html += "<input type='hidden' id='bookingid' name='bookingid' value='" + data[0]['booking_id'] + "'>";
                    html += "</td></tr></tbody>";

                    html1 = "<center><a href='ticket_history.php?t=" + window.btoa(data[0]['ticket_id']) + "' class='btn btn-success'>View !</a></center>";
                    /*$("<a href='check.php'>HI</a>").insertAfter( "#insertAfter" );
					$('#create').parent().remove();*/
                    /* $('#insertAfter').empty();
					$('#insertAfter').append(html1);
					 */
                    $('#create').hide();
                    $('#source').hide();
                    $('#issue_faced').hide();
                }


                $('#table1').html(html);
                $('#show_booking').show();
                $('#show_bookings').hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
    }

    function clickable() {
        $('.booking_id_clickable').on('click', function () {
            var id = $(this).attr('id');
            call(id);
        });
    }

    function call1() {
        var search_val = $('#searchVal').val();
        var search_type1 = $('#search_type1').val();
        $.ajax({
            url: "ajax/get_booking_details.php",
            type: "GET",
            dataType: "json",
            data: {'search_val': search_val, 'search_type1': search_type1},
            success: function (data) {
                //alert(data);
                var html = "";
                console.log(data);
                if (data[0]['status'] == 'success') {
                    for (i = 0; i < data.length; i++) {
                        console.log(i);
                        if (i % 2 == 0) {
                            html += "<div class='row' style='margin-left:-24px;'>";
                            //data[i]['booking_id']+data[i]['service_type']+data[i]['brand']+" "+data[i]['model']+data[i]['axle_flag']
                        }
                        html += "<div class='col-xs-6 booking_id_clickable' id='" + data[i]['booking_id'] + "' >";
                        html += "<div class='floating-box' style='cursor:pointer;width:262px;height:100px;'>";
                        html += "<p><strong>" + data[i]['booking_id'] + " | " + data[i]['service_type'] + " </strong></p>";
                        html += "<p>" + data[i]['brand'] + " " + data[i]['model'] + "<p></p>" + data[i]['axle_flag'] + " </p>";
                        html += "</div></div>";
                        if (i % 2 != 0) {
                            html += "</div>";
                        }
                    }
                    $('#show_booking').hide();
                    $('#source').hide();
                    $('#create').hide();
                    $('#issue_faced').hide();
                    $('#show_bookings').html(html);
                    $('#show_bookings').show();
                    clickable();
                } else if (data[0]['status'] == 'fail') {
                    html = "No bookings found!";
                    $('#show_bookings').html(html);
                    $('#show_bookings').show();
                    $('#show_booking').hide();
                    $('#source').hide();
                    $('#issue_faced').hide();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
    }

    function ValidationEvent1() {
        event.preventDefault();
        var mobile = document.getElementById("searchVal").value;
        var phoneno = /^[1-9][0-9]*$/;
        // var emailid = /\S+@\S+\.\S+/;
        var emailid = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        //console.log(phoneno.test(mobile))
        if (phoneno.test(mobile)) {
            if ((mobile.length >= 7 && mobile.length < 10) || mobile.length > 10 || mobile.length <= 4) {
                document.getElementById("search_type1").value = "";
                alert("Please enter a valid BookingId / Mobile number !");
            } else if (mobile.length <= 7) {
                document.getElementById("search_type1").value = "booking";
                console.log('set');
                //document.getElementById("bookingid_search").submit();
                call();
            } else {
                document.getElementById("search_type1").value = "mobile";
                //document.getElementById("bookingid_search").submit();
                call1();
            }
        } else if (emailid.test(mobile)) {
            document.getElementById("search_type1").value = "email";
            //document.getElementById("bookingid_search").submit();
            call1();
        } else {
            document.getElementById("search_type1").value = "";
            alert("Please enter a valid Booking Id / Mobile Number / Email !");
        }
    }

    function modal() {
        var flag = '<?php echo $flag;?>';
        var sess_flag = '<?php echo $_SESSION['modal_sess'];?>';
        if (flag == '1' && sess_flag == 'open') {
            $(window).on('load', function () {
                $("#initialModal").modal({
                    refresh: true
                });
            });
        }
        $('#addNewModal').on("hidden.bs.modal", function () {
            $('#table1').html('');
            $('#show_bookings').html('');
            $('#searchVal').val('');
            $('#search_type1').val('');
        });
        /* $('#bookingid_search').on('submit',function(e){
	
		});	 */
        $('#create_ticket').on('submit', function (e) {
            e.preventDefault();
            var bookingid = $('#bookingid').val();
            var source = $('#source').val();
            var issue_faced = $('textarea#issue_faced').val();
            var user_id = $('#user_id').val();
            $.ajax({
                url: "ajax/create_ticket.php",
                type: "GET",
                data: {'bookingid': bookingid, 'user_id': user_id, 'source': source, 'issue_faced': issue_faced},
                success: function (data) {
                    if (data != 'failed') {
                        viewtable();
                        $("#addNewModal").modal("hide");
                        var url = 'ticket_history.php?t=' + window.btoa(data);
                        //alert(url);
                        var win = window.open(url, '_blank');
                        win.focus();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                }
            });
            console.log('submit');
        });
    }


</script>

</body>
</html>

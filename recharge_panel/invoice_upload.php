<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
include("config.php");
$conn1=db_connect1();
$shop_id = base64_decode($_GET['id']);
$payment_id = base64_decode($_GET['pay_id']);

$query_email = "SELECT distinct amt.email_id FROM go_bumpr.admin_mechanic_table as amt  LEFT JOIN b2b.b2b_credits_payment as cp ON cp.b2b_shop_id = amt.axle_id and cp.b2b_id = '$payment_id' WHERE cp.b2b_shop_id = '$shop_id' AND cp.b2b_id='$payment_id' ";
 //echo $query;
  $res_email = mysqli_query($conn1,$query_email);
  $row_email = mysqli_fetch_object($res_email); 
  $email_id = $row_email->email_id; 

function fetch_customer_data($conn1)
{
 $shop_id = base64_decode($_GET['id']);
 $payment_id = base64_decode($_GET['pay_id']);

  $query = "SELECT distinct cp.b2b_id,cp.b2b_cus_name,cp.b2b_amt,cp.b2b_credits,cp.b2b_leads,cp.b2b_credit_amt,cp.b2b_purpose,cp.discount,cp.invoice_no,amt.shop_name,amt.mec_id,amt.address5,amt.pincode,gh.start_date,gh.model,gh.credits,gh.leads,gh.amount,amt.gst_pan,amt.address1,amt.address2,amt.address4 FROM go_bumpr.admin_mechanic_table as amt LEFT JOIN go_bumpr.garage_model_history as gh ON gh.b2b_shop_id=amt.axle_id LEFT JOIN b2b.b2b_credits_payment as cp ON cp.b2b_shop_id = gh.b2b_shop_id and cp.b2b_id = '$payment_id' WHERE cp.b2b_shop_id = '$shop_id' AND cp.b2b_id='$payment_id' and gh.end_date='0000-00-00' ORDER BY gh.start_date DESC LIMIT 1";
  // echo $query;
  // die;
  $res_fetch = mysqli_query($conn1,$query);
  $row_fetch = mysqli_fetch_object($res_fetch); 
  $m_shop_name = $row_fetch->shop_name; 
  $m_city = $row_fetch->address5;
  $m_pincode = $row_fetch->pincode;
  $m_receipt_no = $row_fetch->invoice_no;
  $m_mec_id = $row_fetch->mec_id;
  $m_startdate1 = $row_fetch->start_date;
  $m_startdate = date('d/m/Y', strtotime($m_startdate1));
  $m_mec_id = $row_fetch->mec_id;
  $m_model = $row_fetch->model;
  $m_b2b_purpose = $row_fetch->b2b_purpose;
  $qty=($m_b2b_purpose=="Leads Recharge" || $m_b2b_purpose=="leads")?$row_fetch->b2b_leads:$row_fetch->b2b_credits;
  $m_amount = $row_fetch->b2b_amt;//including tax
  $amt=round($m_amount/1.18,2);//excluding tax
  $amt1=round($m_amount/1.09,2);//excluding tax
  $tax=$m_amount-$amt;
  $tax_amt=$m_amount/1.18;
  $tax_amt1 = $m_amount - $tax_amt ;
  $tax2 = round($tax_amt1/2, 2) ;
  $discount=($m_b2b_purpose=="Leads Recharge" || $m_b2b_purpose=="leads")?0.00:$row_fetch->discount;
  $discount_amount=round($discount*$amt/100,2);
  $subtotal=round($amt+$discount_amount,2);
  $rate=$subtotal/$qty;
  $sub = $rate * $qty;
  $total = $sub + $tax - $discount_amount;
  $gst_pan = $row_fetch->gst_pan;
  $address1 = $row_fetch->address1;
  $address2 = $row_fetch->address2;
  $address4 = $row_fetch->address4;
  $city1 = strcasecmp($m_city,'Chennai');
  $city2 = strcasecmp($m_city,'Bangalore');
  $city3 = strcasecmp($m_city,'Hyderabad');

  if($address1=='' && $address2=='')
  {
    $street = '';
  }
  else if($address1=='')
  {
    $street = $address2.',<br>';
  }
  else if($address2=='')
  {
    $street = $address1.',<br>';
  } 
  else
  {
     $street = $address1.', '.$address2.',<br>';
  }


 if($m_city == $city1)
  {
    $state1 = 'Tamilnadu';
    $state = '33 - Tamilnadu'; 
    $gst_tax = '9.0% CGST</p><p style="font-weight:bold;font-size:10pt;">9.0% SGST ';
    $tax1='<p style="font-size:10pt;">'.$tax2.'</p>';
    $tax3='<p style="font-size:10pt;">'.$tax2.'</p>';
  }
  else if($m_city == $city2)
  {
    $state1='Karnataka';
    $state = '29 - Karnataka';
    $gst_tax = '18.0% IGST ';
    $tax3='<p style="font-size:10pt;">'.$tax.'</p>';
  }
  else if($m_city == $city3)
  {
    $state1='Telangana';
    $state = '36 - Telangana';
    $gst_tax = '18.0% IGST ';
    $tax3='<p style="font-size:10pt;">'.$tax.'</p>';
  }
  if($address4=='')
  {
    $city = ucfirst(strtolower($m_city)).',<br>';
  }
  else
  {
     $city = $address4.', '.ucfirst(strtolower($m_city)).',<br>';
  }

  if($m_pincode=='')
  {
    $state2 = $state1.'.';
  }
  else
  {
    $state2 = $state1.' - '.$m_pincode.'.';
  }
  if($gst_pan=='')
  {
    $gst = '<br>GSTIN: NA';
  }
  else
  {
    $gst='<br>GSTIN: '.$gst_pan;
  } 

  $output.= '
  <!doctype html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <div id="invoice">
<div id="invoice">
<style>
#customers {
  font-family: timenewroman;
  border-collapse: collapse;
  width: 100%;
}

#customers td{
  padding: 10px;
}


#customers tr:hover {background-color: #f2f2f2;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #cce6ff;
  color: black;
  font-family: timenewroman;
}
</style>
</head>
<body>

  <table width="100%">
    <tr>
        <td align="left">
        
            <p style="font-weight:bold;font-size:12pt;font-family:timenewroman;">Northerly Automotive Solutions Pvt Ltd </p>
            <p style="font-size:10pt;font-family:timenewroman;margin-top:-5pt;">
              No.110 Chateau D Ampa, Building 5th Floor,<br> 
              Nelson Manickam Road, Rajaram Mehta Nagar,<br>
              Aminjikarai Chennai, Tamilnadu  600029 <br>
              GSTIN: 33AAFCN0434M1ZK
            </p>     
      <td align="right"><img src="images/logo-new-gb.png" alt="" width="200" style="margin-top:-10pt;"></td>
        </td>
    </tr>
  </table>
<hr>
<table align="center">
<tr>
<td><p style="font-weight:bold;font-size:16pt;color:#000000;font-family:timenewroman;">TAX INVOICE</p></td>
</tr>
</table>
  <table width="100%">
    <tr>
        <td>
        <span style="font-weight:bold;font-size:12pt;color:#000000; font-family:timenewroman;">RECIPIENT OF SUPPLY</span><br><span style="font-size:10pt;font-weight:bold;color:#000000; font-family:timenewroman;">'.$m_shop_name.'</span><br><span style="font-size:11pt;color:#000000; font-family:timenewroman;">'.$street.''.$city.''.$state2.''.$gst.'</span><br>
        </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>
        <span style="font-weight:bold;font-size:10pt;color:#000000; 
        font-family:timenewroman;">INVOICE NO:</span><span style="font-size:12pt;color:#000000; font-family:timenewroman;"> '.$m_receipt_no.'</span><br>
        <span style="font-weight:bold;font-size:10pt;color:#000000; font-family:timenewroman;">DATE:</span><span style="font-size:12pt;color:#000000; font-family:timenewroman;"> '.$m_startdate.'</span><br>
        <span style="font-weight:bold;font-size:10pt;color:#000000; font-family:timenewroman;">DUE DATE:</span><span style="font-size:12pt;color:#000000; font-family:timenewroman;"> '.$m_startdate.'</span><br>
        <span style="font-weight:bold;font-size:10pt;color:#000000; font-family:timenewroman;">TERMS:</span><span style="font-size:12pt;color:#000000; font-family:timenewroman;"> Due on receipt</span><br>
        </td>
  </tr>
  </table>
  <br>
  <table>
  <tr>
        <td>
        <span style="font-weight:bold;font-size:10pt;color:#000000; font-family:timenewroman;">
        PLACE OF SUPPLY </span><br><span style="font-size:12pt;color:#000000; font-family:timenewroman;">'.$state.'</span> 
        </td>
    </tr>
  </table>
  <hr>
<table align="left">
<tr>
<td><span style="font-weight:bold;font-size:10pt;color:#000000;font-family:timenewroman;">MECH ID:</span><span style="font-size:12pt;color:#000000; font-family:timenewroman;"> '.$m_mec_id.'</span></td>
</tr>
</table>
<br>
<br>
  <table id="customers">
  <tr>
    <th style="text-align:center;">NO</th>
    <th style="text-align:center;">HSN/SAC</th>
    <th style="text-align:center;">ACTIVITY</th>
    <th style="text-align:center;">QTY</th>
    <th style="text-align:center;">RATE</th>
    <th style="text-align:right;">AMOUNT</th>
  </tr>
  <tr style="background-color: #f2f2f2;">
    <td style="text-align:center;">1</td>
    <td style="text-align:center;">998729</td>
    <td style="text-align:center;"><strong>'.$m_model.'</strong> <br>'.$m_b2b_purpose.'</td>
    <td style="text-align:center;">'.$qty.'</td>
    <td style="text-align:center;">'.round($rate).'</td>
    <td style="text-align:right;">'.$sub.'</td>
    
  </tr>
  <tr>
  <td colspan="3">
    <strong>Payment in favour of:</strong> <br>
    Northerly Automotive Solutions Pvt. Ltd <br>
    Account no: 50200016402261 <br>
    Account Type: Current Account <br>
    Bank Name: HDFC Bank, <br>
    Branch Name: ITC Center Annasalai , Chennai - 2 <br>
    IFSC code: HDFC0003631
   </td> 
   <td colspan="2" style="text-align:left;">

   <p style="font-weight:bold;font-size:10pt;">GROSS TOTAL - <span style="font-size:8pt !important;font-weight:normal !important;">(A) </span> </p> 
   <p style="font-weight:bold;font-size:10pt;">DISCOUNT - <span style="font-size:8pt !important;font-weight:normal !important;">(B) </span></p> 
   <p style="font-weight:bold;font-size:10pt;">'.$gst_tax.'</p>
   <p style="font-weight:bold;font-size:10pt;">TOTAL TAXES - <span style="font-size:8pt !important;font-weight:normal !important;">(C) </span></p>
   <p style="font-weight:bold;font-size:10pt;">NET TOTAL - <span style="font-size:8pt !important;font-weight:normal !important;">(A-B+C) </span></p> 
   
   </td>
   <td style="text-align:right;">
   <p style="font-size:10pt;">'.$sub.'</p>
   <p style="font-size:10pt;"> -'.$discount_amount.'</p>
   '.$tax3.'
   '.$tax1.'
   <p style="font-size:10pt;">'.$total_tax.'</p>
   <p style="font-size:10pt;font-weight:bold;">'.$total.'.00</p>
   
   </td>
  </tr>
  </table>
<br>
<br>
<table>
<tr>
<td><span style="font-size:10pt;font-weight:bold;">Note:</span></td>
</tr>
<br>
<tr>
<td><span style="font-size:10pt;">1. The amount payable indicated remain final, without further negotiation.</span></td>
</tr>
<tr>
<td><span style="font-size:10pt;">2. All disputes are subject to chennai jurisdiction only.</span></td>
</tr>
</table>
<br>
<table>
<tr>
<td><span style="font-size:10pt;font-weight:bold;">This being an auto generated invoice – Signature not required</span></td>
</tr>
</table>
</body>
</html>

  ';

  
  return $output;
}

  include('pdf.php');
  $file_name = md5(rand()) . '.pdf';
  $html_code = '<link rel="stylesheet" href="bootstrap.min.css">';
  $html_code .= fetch_customer_data($conn1);
  $pdf = new Pdf();
  $pdf->load_html($html_code);
  $pdf->render();
  $file = $pdf->output();
  file_put_contents($file_name, $file);
  
  require 'class/class.phpmailer.php';
  $mail = new PHPMailer;
  $mail->IsSMTP();  
              //Sets Mailer to send message using SMTP
  $mail->Host = 'smtp.pepipost.com';    //Sets the SMTP hosts of your Email hosting, this for Godaddy
  $mail->Port = '587';
  $mail->SMTPDebug = 0;           //Sets the default SMTP server port
  $mail->SMTPAuth = true;             //Sets SMTP authentication. Utilizes the Username and Password variables
  $mail->Username = 'letsgobumpr';          //Sets SMTP username
  $mail->Password = 'GoBumpr25000!';          //Sets SMTP password
  $mail->SMTPSecure = 'TLS';              //Sets connection prefix. Options are "", "ssl" or "tls"
  $mail->From = 'narendran@gobumpr.com';      //Sets the From email address for the message
  $mail->FromName = 'Narendran Elumalai';      //Sets the From name of the message
  $mail->AddAddress($email_id, );    //Adds a "To" address
  $mail->AddCC('suruthi@gobumpr.com');
  $mail->WordWrap = 50;             //Sets word wrapping on the body of the message to a given number of characters
  $mail->IsHTML(true);              //Sets message type to HTML       
  $mail->AddAttachment($file_name);             //Adds an attachment from a path on the filesystem
  $mail->Subject = 'Recharge Invoice';      //Sets the Subject of the message
  $mail->Body = 'Please Find Invoice details in attach PDF File.';       //An HTML or plain text message body
  if($mail->Send())               //Send an Email. Return true on success or false on error
  {
  ?>

    <div style="font-weight: bold;text-align: center;font-size:40pt;margin-top: 180pt;background: linear-gradient(to right, #30CFD0 0%, #330867 100%);-webkit-background-clip: text;-webkit-text-fill-color: transparent;">Invoice sent successfully!!</div>
    <a href="credits_approval.php"><button style="cursor: pointer;color:#ff8c1a;font-weight: bold;text-align: center;font-size:16pt;width: 15%;margin-top: 30pt;margin-left:470pt;border: 3px solid #73AD21;padding: 10px;">Back</button></a>

  <?php
  }
  else
  {
    ?>
    <div style="font-weight: bold;text-align: center;font-size:40pt;margin-top: 180pt;background: linear-gradient(to right, #30CFD0 0%, #330867 100%);-webkit-background-clip: text;-webkit-text-fill-color: transparent;">Failed to send Invoice</div>
    <a href="credits_approval.php"><button style="cursor: pointer;color:#ff8c1a;font-weight: bold;text-align: center;font-size:16pt;width: 15%;margin-top: 30pt;margin-left:470pt;border: 3px solid #73AD21;padding: 10px;">Back</button></a>
    <?php
  }
  unlink($file_name);
?>
<!-- <script>
    window.setTimeout(function(){window.location.href="credits_approval.php"}, 3000);
</script> -->

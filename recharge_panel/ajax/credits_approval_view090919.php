<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();
date_default_timezone_set('Asia/Kolkata');
$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$today = date("Y-m-d");

// $startdate = date('Y-m-d',strtotime($_POST['startdate']));
// $enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$city = $_POST['city'];
$veh = $_POST['veh'];
$start_date = date('Y-m-d',strtotime($_POST['start_date']));
$end_date = date('Y-m-d',strtotime($_POST['end_date']));
//echo $start_date;

$_SESSION['crm_city'] = $city;
$sd = date('d',strtotime($_POST['start_date']));
$t = date("d");
$no = 0;
$i_no = 0;
$all_no = 0;
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<style type="text/css">
  p {
    margin: 0 0 10px 15px;
}
#table th 
  {     text-align:center;
  
  }   
  #table tr:nth-child(even ) {    
    background-color: #f2f2f2   
  }       
        
    .square_btn{    
      display: inline-block;    
      padding: 0.5em 1em;   
      text-decoration: none;    
      background: #f7f7f7;    
      border-left: solid 6px #ff7c5c;   
      color: #ff7c5c;   
      font-weight: bold;    
      box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.29);    
  }   }
  .square_btn:active {    
      box-shadow: inset 0 0 2px rgba(128, 128, 128, 0.1);   
      transform: translateY(2px);   
  }   
  .square_btn2{   
      display: block;   
      padding: 0.5em 1em;   
      text-decoration: none;    
      background: #f7f7f7;    
      border-left: solid 6px #009900;   
      color: #009900;   
      font-weight: bold;    
      box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.29);    
      cursor: none;   
      
  }
  .square_btn3{   
      display: block;   
      padding: 0.5em 1em;   
      text-decoration: none;    
      background: #f7f7f7;    
      border-left: solid 6px #009900;   
      color: #ff0000;   
      font-weight: bold;    
      box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.29);    
      cursor: none;   
      
  }
   .square_btn3:active {    
      box-shadow: inset 0 0 2px rgba(128, 128, 128, 0.1);   
      transform: translateY(2px);   
  }   
   .square_btn4{   
      display: block;   
      padding: 0.5em 1em;   
      text-decoration: none;    
      background: #f7f7f7;    
      border-left: solid 6px #ff0000;   
      color: #ff0000;   
      font-weight: bold;    
      box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.29);    
      
  }
</style>
   <!--  <span class="counter" style="top:0;"></span> -->

    <div id="div1" style="width:auto;float:left;margin-left: 1%;margin-right: 1%;">
    <table class="table table-bordered tablesorter table-hover results" id="table" style="font-size:12px;" >
    <thead style="background: #a6a6a6;display: table-header-group;text-align: center;color: #000000;">
    <th>No</th>
    <th>Date</th>
    <th>Mec ID</th>
    <th>ShopName</th>
    <th>View</th>
    <th>Locality</th>
    <th>Amount</th>
    <th>Credits</th>
    <th>Leads</th>
    <th>RE Leads</th>
    <th>NRE Leads</th>
    <th>Bank Name</th>
    <th>Cheque No</th>
    <th>Cash Receipt No</th>
    <th>Transaction ID</th>
    <th>Payment Mode</th>
    <th>Requested By</th>
    <th>comments</th>
    <th>Last Recharge date</th>
  <th>Approval</th>
  <th>Reject</th>
  <!--<?php if($shop_status != 'active')
  { ?>
  <th>Reason <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
  <?php } ?>-->
  <!-- <th>Log <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th> -->
    </thead>
    <tbody id="tbody">

    <?php
 $cond = '';

  
 $cond = $cond.($veh == "all" ? " AND m.b2b_vehicle_type!='' and m.b2b_vehicle_type!='both'" : "AND m.b2b_vehicle_type='$veh'");      $cond = $cond.($veh == "all" ? "" : "AND m.type='$veh'");
     $cond = $cond.($city == "all" ? " ": "AND m.b2b_address5='$city'");

  $sql_credits="SELECT a.mec_id,b.flag_delete,b.b2b_id,b.b2b_shop_id,b.b2b_credits,b.b2b_leads,b.premium,b.b2b_leads_pack,b.b2b_purpose,b.b2b_other_ref,b.b2b_cheque_bank,b.b2b_cheque_no,b.instmj_pmt_id,b.b2b_log,ca.name,b.b2b_amt,b.b2b_payment_mode,m.b2b_shop_name,m.b2b_address4,b.b2b_comments,b.b2b_approve_flag,b.b2b_neft_trn_id,m.b2b_vehicle_type,b.b2b_pmt_scs_flg,b.b2b_receipt_no FROM b2b_credits_payment b LEFT JOIN b2b_mec_tbl m ON b.b2b_shop_id=m.b2b_shop_id LEFT JOIN go_bumpr.crm_admin ca ON ca.crm_log_id = b.b2b_crm_log_id LEFT JOIN go_bumpr.admin_mechanic_table a ON b.b2b_shop_id=a.axle_id WHERE m.b2b_shop_id>1000 AND b.sales_approve_flag='0' $cond and  DATE(b.b2b_log) BETWEEN '$start_date' AND '$end_date' ORDER BY b.b2b_approve_flag DESC,b.flag_delete ASC ,b.b2b_id DESC;"; 
  //echo $sql_credits;
    $res_credits = mysqli_query($conn2,$sql_credits);

    $num_credits = mysqli_num_rows($res_credits);
     if($num_credits>0)
     {
      while($row_credits = mysqli_fetch_object($res_credits)){
        $shop_id = '';
        $date=date('d-M-Y h:i:s A',strtotime($row_credits->b2b_log));
        $b2b_id=$row_credits->b2b_id;
        $mec_id= $row_credits->mec_id;
        $delete_flag= $row_credits->flag_delete;
        $shop_name = $row_credits->b2b_shop_name;
        $b2b_shop_id=$row_credits->b2b_shop_id;
        $b2b_credits=$row_credits->b2b_credits;
        $purpose=$row_credits->b2b_purpose;
        $b2b_leads=$row_credits->b2b_leads;
        $leads_pack=$row_credits->b2b_leads_pack;
        $b2b_bank=$row_credits->b2b_cheque_bank;
        $b2b_cheque=$row_credits->b2b_cheque_no;
        $mode=$row_credits->b2b_payment_mode;
        $pmt_scs_flg=$row_credits->b2b_pmt_scs_flg;
        $b2b_amount=$row_credits->b2b_amt;
        $locality=$row_credits->b2b_address4;
        $crm_name=$row_credits->name;
        $neft=$row_credits->b2b_neft_trn_id;
        $approve_flag=$row_credits->b2b_approve_flag;
        $mode=$row_credits->b2b_payment_mode;
        $comments = $row_credits->b2b_comments;
        $ref = $row_credits->b2b_other_ref;
        $veh=$row_credits->b2b_vehicle_type;
        $instmj_pmt_id=$row_credits->instmj_pmt_id;
        $b2b_receipt=$row_credits->b2b_receipt_no;
        if($mode=="NEFT"){
          $ref=$neft;
        }elseif ($mode=="Instamojo") {
          $ref=$instmj_pmt_id;
        }
        $re_leads=$nre_leads=0;
        if($purpose=="Leads Recharge"){
          $content='Are you sure you want to approve '.$b2b_leads.' leads to '.$shop_name.'?';
          $content2='Are you sure you want to reject the request to add '.$b2b_leads.' leads to '.$shop_name.'?';
          if($veh=="2w"){
            if($leads_pack=="Royal Enfield & Sports"){
              $re_leads=$b2b_leads;
              $nre_leads=0;
            }else{
              $nre_leads=$b2b_leads;
              $re_leads=0;
            }
          }
        }else{
          $content='Are you sure you want to approve '.$b2b_credits.' credits to '.$shop_name.'?';
          $content2='Are you sure you want to reject the request to add '.$b2b_credits.' credits to '.$shop_name.'?';
        }

        $vehicle_type = 'all';
        $unused_credits = 100;
        $enc_b2b_id = base64_encode($b2b_id);
        $enc_b2b_shop_id = base64_encode($b2b_shop_id);
        $enc_shop_id = base64_encode($shop_id);

        $last_rechg_res=mysqli_query($conn2,"SELECT b2b_pmt_scs_date FROM b2b_credits_payment WHERE b2b_shop_id='$b2b_shop_id' ORDER BY b2b_pmt_scs_date DESC LIMIT 1;");
        $last_rechg=mysqli_fetch_array($last_rechg_res);
        ?>
        <tr style="text-align: center;">
            <td><?php echo $no = $no+1; ?></td>
            <td> <?php echo $date;?></td>
            <td> <?php echo $mec_id;?></td>
      <td><?php echo $shop_name;?></td>
    <td><a href="shop_credits_history.php?si=<?php echo $enc_b2b_shop_id; ?>&t=<?php echo base64_encode($b2b_credits);?>" target="_blank" ><i id="'.$shop_id.'" class="fa fa-eye" aria-hidden="true"></i></td>
      <td><?php echo $locality; ?></td>
      <td><?php echo $b2b_amount; ?></td>
      <td><?php echo $b2b_credits; ?></td>
      <td><?php echo $b2b_leads; ?></td>
      <td><?php echo $re_leads; ?></td>
      <td><?php echo $nre_leads; ?></td>
      <td><?php echo ($b2b_bank=='')? '-':$b2b_bank; ?></td>
      <td><?php echo ($b2b_cheque=='')?'-':$b2b_cheque; ?></td>
      <td><?php echo ($b2b_receipt=='')?'-':$b2b_receipt; ?></td>
      <td><?php echo ($ref=='')?'-':$ref; ?></td>
      <td><?php echo $mode; ?></td>
       <td><?php echo $crm_name; ?></td>

      <td id="tdid"><?php echo ($comments=='')?'-':$comments; ?></td>
      <td><?php echo $last_rechg[0]; ?></td>

      
      <?php if($approve_flag==1 && $delete_flag!=1){ ?>
        <td>
      <button class="square_btn" onclick='showDetails( "<?php echo $enc_b2b_id; ?>","<?php echo $enc_b2b_shop_id; ?>","<?php echo $content; ?>")'>Approve</button> </td>
      <?php }

      else if($delete_flag==1){  ?>  
      <td colspan="2" align="center" style="padding: 20pt;">  
      <button class="square_btn4" disabled="">Rejected</button>
      </td>
         <?php } 
      else{  ?>    
        <td colspan="2" align="center" style="padding: 20pt;">  
      <button class ="square_btn2" disabled>Approved</button>
      </td>
         <?php }?>
  
      
      <?php if($approve_flag==1 && $delete_flag!=1){ ?>
        <td>
      <button class="square_btn" onclick='reject( "<?php echo $enc_b2b_id; ?>","<?php echo $enc_b2b_shop_id; ?>","<?php echo $content2; ?>")'>Reject</button></td> 
      <?php }
      
       } }
    
    
else{
  ?>  
      <th colspan="21" style="font-size: 18px;text-align:center;padding-top: 40px;">No results found...! </th>
 <?php
  } 


?>
    </tbody>
    </table>
 
  <!--<script type="text/javascript">
    $('.example1').on('click', function () { 
   $.confirm({
    buttons: {
        Yes: function(helloButton){
            // shorthand method to define a button
            // the button key will be used as button name
        },
        No: function(heyButton){
            // access the button using jquery
            this.$$Yes.trigger('click'); // click the 'hello' button
            this.$$No.prop('disabled', true); // disable the current button using jquery method
                        
            // jconfirm button methods, all methods listed here
            this.buttons.hello.setText('Helloooo'); // setText for 'hello' button
            this.buttons.hey.disable(); // disable with button function provided by jconfirm
            this.buttons.hey.enable(); // enable with button function provided by jconfirm
            // the button's instance is passed as the first argument, for quick access
            heyButton === this.buttons.hey
        },
        
    }
});
  });

</script>-->
<script type="text/javascript">
   function showDetails(b_id,shop_id,content) {
    $.confirm({
      
      content:content+'<br><br>' +
    '<form action="" class="formName">' +
    '<div class="form-group">' +
    
    '<input type="text" placeholder="Enter (Mec ID-Receipt No)" class="name form-control" required />' +
    '</div>' +
    '</form>',

      
    title:'',
    type: 'green',
    typeAnimated: true,
    backgroundDismiss: true,
    buttons: {
        formSubmit: {
            text: 'Submit',
            btnClass: 'btn-blue',
            action: function () {
                var name = this.$content.find('.name').val();
                if(!name){
                    $.alert('Enter MEC ID-Receipt No');
                    return false;
                }else{
                   
                  $.ajax({
                    data : {name:name,shop_id:shop_id,id:b_id},
                    type : "POST",
                    url : "ajax/recharge_approval_validate.php",
                    success : function(data){
                      switch (data) {
                        case "exhausted":
                          $.alert({
                              title: 'Alert!',
                              content: 'No of approval attempts exceeded the limit',
                          });
                          break;
                      
                        case "success":
                          window.location.assign("recharge_credits_approval_mail.php?id="+shop_id+"&pay_id="+b_id);
                          break;
                        case "fail":
                          $.alert({
                              title: 'Alert!',
                              content: 'Invalid Mec ID-Receipt No',
                          });
                          break;
                      }
                    }
                  });
                }
                
            }
        },
        cancel: function () {
            //close
        },
    },
});
  }
  </script>
<script type="text/javascript">
   function reject(b_id,shop_id,content) {
    $.confirm({
      
      content:content,

      
    title:'',
    type: 'red',
    typeAnimated: true,
    backgroundDismiss: true,
    buttons: {
      
        Yes: {
            text: 'Yes', // With spaces and symbols
             btnClass: 'btn-success',
            action: function () {
                //$.alert('You clicked on "heyThere"');
        // "http://www.google.com?id="+b_id+"&shop_id="+shop_id
                // window.location.assign("http://bridge.gobumpr.com/receipts-sheet-v2/credits_approval_mail.php?id="+shop_id+"&pay_id="+b_id);
                window.location.assign("ajax/credits_reject.php?id="+shop_id+"&pay_id="+b_id);
            }
        },
        No: {
            text: 'No', // With spaces and symbols
             btnClass: 'btn-danger',
            action: function () {
                //$.alert('You clicked on "heyThere"');
            }
        }
    }
});
  }
  </script>

    
<?php  

include("../config.php");
$conn2 = db_connect2();
error_reporting(-1);
ini_set('display_errors', 'On');
set_error_handler("var_dump");
use PHPMailer\PHPMailer\PHPMailer;

require_once('../PHPMail/src/PHPMailer.php');
require_once('../PHPMail/src/SMTP.php');
require_once('../PHPMail/src/Exception.php');

$id=$_POST['id'];
$query="UPDATE b2b_credits_payment SET sales_approve_flag=0 WHERE b2b_id={$id};";
 $res=mysqli_query($conn2,$query);

$query="SELECT * FROM b2b_credits_payment WHERE b2b_id={$id};";
$res=mysqli_query($conn2,$query);
$row=mysqli_fetch_object($res);
$shop_id=$row->b2b_shop_id;
$service_center=$row->b2b_cus_name;
$mobile=$row->b2b_cus_no;
$credits=$row->b2b_credits;
$total_amt=$row->b2b_amt;
$purpose=$row->b2b_purpose;
$receipt=$row->b2b_receipt_no;
$chequeno=$row->b2b_cheque_no;
$chequedate=$row->b2b_cheque_date;
$bank=$row->b2b_cheque_bank;
$neft=$row->b2b_neft_trn_id;
$ref=$row->b2b_other_ref;
$mode=$row->b2b_payment_mode;
$leads=$row->b2b_leads;
$lead_cat=$row->b2b_leads_pack;
$instaid=$row->instmj_pmt_id;
$payment_date=$row->payment_date;
$actual_amt=round($total_amt/1.18);
$actual_credit=round($actual_amt/100);
$tax=round(0.18*$actual_amt);
if($purpose=="Credits Recharge"){
  $discount=$row->discount;
  $discount_amt=round(($actual_amt*$discount)/100);
}else{
  $discount_amt=0;
  $discount=0;
}
$sql_shop="SELECT b2b_vehicle_type,b2b_address4,b2b_address5 FROM b2b_mec_tbl WHERE b2b_shop_id='$shop_id';";

$res_shop=mysqli_query($conn2,$sql_shop);
$row_shop=mysqli_fetch_object($res_shop);
$veh=$row_shop->b2b_vehicle_type;
$address4=$row_shop->b2b_address4;
$address5=$row_shop->b2b_address5;
//SMTP Settings
$mail = new PHPMailer();
$mail->IsSMTP();
$mail->SMTPAuth   = true;
$mail->SMTPSecure = "TLS";
$mail->Host       = "smtp.pepipost.com";
$mail->Username   = "letsgobumpr";
$mail->Password   = "GoBumpr25000!";
$mail->SetFrom('narendran@gobumpr.com', 'Narendran Elumalai');
//$mail->SetFrom('Admin@gobumpr.com', 'Admin');
$mail->Subject = "Need Approval to recharge";
$mail->Port = 587;  
$mail->SMTPDebug = 2;
$mail->isHTML(true);

$to_mail = 'suruthi@gobumpr.com';
//$to_mail = 'finance@gobumpr.com';

$body_start='<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <style type="text/css">
  td, th {
    padding:0.5em 1em;
    font-size:1.2em;
  }
  table, tr , td{
    border-collapse:collapse;
  }
  th{
    background-color:#FF8C00;
    color:white;
    height:60px;
  }
  button, a{
    cursor:pointer !important;
  }

  </style>

</head>
<body>

      <table style="margin:auto;width:50vw">
      <tr>        
        <th colspan="2" style="text-align:center;">Recharge Request Waiting for Approval</td>
      </tr>';
  if($error_flag==1){
    $body_start.='<tr><td colspan="2" style="color:black"><br>This particular recharge exceeds the floor price/discount mentioned. Requires your approval before proceeding to recharge this account.<br><br></td></tr>';
  }
  //   if($locality[3]!=" "){
  //     $body_service='
  //         <tr>
  //           <td style="color:#808080">Service Center</td>
  //           <td>'.$service_center.' ('.$locality[3].')</td>
  //          </tr>

  //          <tr>
  //         <td style="color:#808080">Mobile Number</td>
  //           <td colspan="2">'.$mobile.'</td>
  //          </tr>
  //         ';
  
  // }else{
    $body_service='
          <tr>
          <td style="color:#808080">Service Center</td>
            <td style="color:black">'.$service_center.', '.$address4.', '.$address5.'</td>
           </tr>

           <tr>
          <td style="color:#808080">Mobile Number</td>
            <td style="color:black">'.$mobile.'</td>
           </tr>
           <tr>
          <td style="color:#808080">Vehicle Type</td>
            <td style="color:black">'.$veh.'</td>
           </tr>
          ';  
  // }
    $body_pay='
      
        <tr  style="padding-left: 0px;color: #FF8C00">
          <td>Payment Details</td>
        </tr>';
    $body_pay_lead_cat='<tr>         
            <td style="color:#808080">Leads Package</td>
            <td>'.$lead_cat.'</td>
          
        </tr>';
    
    $body_pay_mode='<tr>
          <td style="color:#808080">Payment Mode</td>
          <td>'.$mode.'</td>
        </tr>';
    $body_pay_cash='<tr>
            <td style="color:#808080">Transaction ID</td>
            <td>'.$receipt.'</td>
          </tr>';
    $body_pay_insta='<tr>
            <td style="color:#808080">Instamojo ID</td>
            <td >'.$instaid.'</td>
          </tr>';
    $body_pay_neft='
          <tr>
            <td style="color:#808080">Transaction ID</td>
            <td>'.$neft.'</td>
          </tr>';
    $body_pay_others='<tr>
            <td style="color:#808080">Transaction ID</td>
            <td>'.$ref.'</td>
          </tr>';
    $body_pay_cheque='<tr>
          <td style="color:#808080">Cheque No</td>
          <td>'.$chequeno.'</td>
        </tr>
        <tr>
          <td  style="color:#808080">Cheque Date</td>
          <td  id="inv-cheque-date">'.$chequedate.'</td>
        </tr>
        <tr>
          <td style="color:#808080">Bank</td>
          <td >'.$bank.'</td>
        </tr>';
    $body_pay_inv='<tr>
        <td  style="color: #FF8C00">
          Invoice Details
        </td></tr>';
    $body_pay_inv_lead='<tr>
          <td style="color:#808080">'.$leads.' Leads</td>
          <td class=" col-sm-6 col-xs-6" id="inv-actual-lead-amt">Rs. '.$actual_amt.'</td>
        </tr>';
    $body_pay_inv_credit='<tr>
          <td style="color:#808080">'.$actual_credit.' Credits</td>
          <td>Rs. '.$actual_amt.'</td>
        </tr>';
      if($discount>0){
        $body_pay_inv2='<tr>
              <td style="color:#808080"> GST @18%</td>
              <td>Rs. '.$tax.'</td>
            </tr>
            <tr>
              <td style="color:#808080"> Discount - '.$discount.'%</td>
              <td>Rs. '.$discount_amt.'</td>
            </tr>
            ';
      }else{
        $body_pay_inv2='<tr>
              <td style="color:#808080"> GST @18%</td>
              <td>Rs.'.$tax.'</td>
            </tr>';
      }
    $body_pay_total_lead='<tr><td id="inv-total" style="color:#808080">Total amount for '.$leads.' leads</td>';
    $body_pay_total_credit='<tr><td style="color:#808080">Total amount for '.$credits.' credits</td>';
    $body_pay_inv3='<td>Rs. '.$total_amt.'</td>
        </tr>';
    $body_pay_btn='<tr><td colspan="2" style="text-align:center;cursor:pointer;"><a href="credits_approval.php?pay_id='.base64_encode($id).'&id='.base64_encode($shop_id).'"><button style="background-color:#ff8c00;border:none;outline:none;width:200px;height:40px;cursor:pointer !important;">View Details</button></a></td></tr></table>
</body>
</html>';
if($leads==0){
  $body=$body_start.$body_service.$body_pay;
  switch($mode){
    case 'Cash':
      $body.=($body_pay_mode.$body_pay_cash);
      break;
    case 'Cheque':
      $body.=($body_pay_mode.$body_pay_cheque);
      break;
    case 'Paytm':
      $body.=($body_pay_mode.$body_pay_others);
      break;
    case 'UPI':
      $body.=($body_pay_mode.$body_pay_others);
      break;
    case 'Instamojo':
      $body.=($body_pay_mode.$body_pay_insta);
      break;
    case 'NEFT':
      $body.=($body_pay_mode.$body_pay_neft);
      break;
  }
  $body.=($body_pay_inv.$body_pay_inv_credit.$body_pay_inv2.$body_pay_total_credit.$body_pay_inv3.$body_pay_btn);

}else{
  $body=$body_start.$body_service.$body_pay.$body_pay_lead_cat;
  switch($mode){
    case 'Cash':
      $body.=($body_pay_mode.$body_pay_cash);
      break;
    case 'Cheque':
      $body.=($body_pay_mode.$body_pay_cheque);
      break;
    case 'Paytm':
      $body.=($body_pay_mode.$body_pay_others);
      break;
    case 'UPI':
      $body.=($body_pay_mode.$body_pay_others);
      break;
    case 'Instamojo':
      $body.=($body_pay_mode.$body_pay_insta);
      break;
    case 'NEFT':
      $body.=($body_pay_mode.$body_pay_neft);
      break;
  }
  $body.=($body_pay_inv.$body_pay_inv_lead.$body_pay_inv2.$body_pay_total_lead.$body_pay_inv3.$body_pay_btn);
}
$body = preg_replace("[\\\]",'',$body);


$mail->MsgHTML($body);
// echo $body;die;
$mail->AddAddress($to_mail,"Partner");
// $mail->AddCC('kishore.ram@gobumpr.com');
$mail->AddCC('kishore.kumar@gobumpr.com');
//$mail->AddCC('mukund.srivathsan@gobumpr.com');
if (!$mail->Send()) {
    echo "Mailer Error: ".$mail->ErrorInfo;
} else {
    echo "Message sent!";
}
?>
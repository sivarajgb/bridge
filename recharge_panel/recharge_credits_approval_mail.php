<?php

// error_reporting(E_ALL);
// ini_set('display_errors', 1);
date_default_timezone_set('Asia/Kolkata');
$today = date('Y-m-d H:i:s');
include("config.php");
session_start();
$crm_id=$_SESSION['crm_log_id'];
$conn = db_connect2();
$conn1 = db_connect1();
if(!$conn){
  echo "Database Connection Error!";
}
else{
 $shop_id = base64_decode($_GET['id']);
 $payment_id = base64_decode($_GET['pay_id']);

  $sql_fetch = "SELECT b.b2b_amt,b.b2b_credits,b.b2b_leads,b.b2b_credit_amt,b.b2b_approve_flag,b.b2b_cus_name,b.premium,b.b2b_leads_pack,b.b2b_log,a.mec_id FROM b2b_credits_payment b LEFT JOIN go_bumpr.admin_mechanic_table a ON b.b2b_shop_id=a.axle_id WHERE b2b_id = {$payment_id}";
  
  $res_fetch = mysqli_query($conn,$sql_fetch);
  $row_fetch = mysqli_fetch_array($res_fetch);
  // var_dump($row_fetch);
  $log=$row_fetch['b2b_log'];
  $credits = $row_fetch['b2b_credits'];
  $leads=$row_fetch['b2b_leads'];
  $leads_pack=$row_fetch['b2b_leads_pack'];
  $credit_amt = $row_fetch['b2b_credit_amt'];
  $approve_flag = $row_fetch['b2b_approve_flag'];
  $shop_name = $row_fetch['b2b_cus_name'];
  $amt=round($row_fetch['b2b_amt']/1.18);
  $premium=$row_fetch['premium'];
  $mec_id=$row_fetch['mec_id'];
  $re_leads=0;
  $nre_leads=0;
  $re_lead_price=0;
  $nre_lead_price=0;
  if($leads==0){
    $lead_price=0;
    $model="Pre Credit";
    $amount=$credit_amt;
  }else{
    $lead_price=round($amt/$leads);
    if($premium==1){
      $model="Premium 2.0";
    }else{
      $model="Leads 3.0";
    }

    if($model!="Pre Credit"){
      if($leads_pack=="Royal Enfield & Sports"){
        $re_lead_price=$lead_price;
        $re_leads=$leads;
      }else if($leads_pack=="Non Royal Enfield"){
        $nre_lead_price=$lead_price;
        $nre_leads=$leads;
      }
    }
    $amount=$amt;
  }
}
 ?>
 <!DOCTYPE html>
 <html lang="en">
 <head>
 <meta charset="utf-8">
 <title>GoBumpr Bridge</title>
 <style>
 body
{
   
   font-family: sans-serif;
   color: #000;
   font-style: normal;
  background-image: url("images/bgimage.jpg");
  
 }
.btn2{
    display: inline-block;
    text-decoration: none;
    background: #87befd;
    color: #000000;
    font-weight: bold;
    width: 130px;
    height: 50px;
    line-height: 30px;
    border-radius: 20%;
    text-align: center;
    vertical-align: middle;
    overflow: hidden;
    box-shadow: 0px 0px 0px 5px #87befd;
    border: dashed 1px #FFF;
    transition: .4s;
}

.btn2:hover{
    background: #668ad8;
     box-shadow: 0px 0px 0px 5px #668ad8;
}
 </style>
 </head>
 <body>
  <div style="box-shadow: 1px 3px 3px 2px gray;padding:1.5em 2em;border-radius: 1%;width: 50%;margin-top: 150pt;margin-left: 280pt;background: #ffffff;text-align: center;">
  <?php if($approve_flag != '1'){  ?>

    <?php if($leads==0){ ?>

    <p style="font-size:37px; color:#000;"><?php echo $credits; ?> Leads have been added to "<?php echo strtoupper($shop_name); ?>" !</p>
  
  <?php }else { ?>
    <p style="font-size:37px; color:#000;"><?php echo $leads; ?> Leads have been added to "<?php echo strtoupper($shop_name); ?>" !</p>
  
  <?php } } else{ 
      $sql_veh_type="SELECT b2b_vehicle_type FROM b2b_mec_tbl WHERE b2b_shop_id='$shop_id';";
      $res_veh_type=mysqli_query($conn,$sql_veh_type);
      $row_veh_type=mysqli_fetch_array($res_veh_type);
      $veh=$row_veh_type['b2b_vehicle_type'];
      //get credits
      $sql_get_credits = "SELECT b2b_credits,b2b_leads,b2b_re_leads,b2b_non_re_leads,b2b_cash_remaining FROM b2b_credits_tbl WHERE b2b_shop_id='$shop_id'";
      $res_get_credits = mysqli_query($conn,$sql_get_credits);
      $row_get_credits = mysqli_fetch_object($res_get_credits);

      $avl_credits = $row_get_credits->b2b_credits;
      $avl_leads=$row_get_credits->b2b_leads;
      $avl_re_leads=$row_get_credits->b2b_re_leads;
      $avl_nre_leads=$row_get_credits->b2b_non_re_leads;
      $avl_amount = $row_get_credits->b2b_cash_remaining;
      //add credits
      $total_leads=$avl_leads+$leads;
      $total_credits = $avl_credits+$credits;
      $total_amount = $avl_amount+$credit_amt;
      $total_re_leads=$avl_re_leads+$re_leads;
      $total_nre_leads=$avl_nre_leads+$nre_leads;
      if($veh=="4w"){        
        $sql_updt_credits = "UPDATE b2b_credits_tbl SET b2b_credits='$total_credits',b2b_leads='$total_leads',b2b_cash_remaining='$total_amount',b2b_mod_log='$today' WHERE b2b_shop_id='$shop_id' ;";
        $res_updt_credits = mysqli_query($conn,$sql_updt_credits);
      }else{

        $sql_updt_credits = "UPDATE b2b_credits_tbl SET b2b_credits='$total_credits',b2b_re_leads='$total_re_leads',b2b_non_re_leads='$total_nre_leads',b2b_cash_remaining='$total_amount',b2b_mod_log='$today',b2b_leads='$total_leads' WHERE b2b_shop_id='$shop_id' ;";
        // echo $sql_updt_credits;die;
        
        $res_updt_credits = mysqli_query($conn,$sql_updt_credits);
      }
      if($model=="Pre Credit"){
        $updt_query="UPDATE b2b_mec_tbl SET b2b_cred_model='1',b2b_new_flg='0' WHERE b2b_shop_id='$shop_id';";
      }else{
        $updt_query="UPDATE b2b_mec_tbl SET b2b_cred_model='1',b2b_new_flg='1' WHERE b2b_shop_id='$shop_id';";
      }
      // echo $updt_query;
      $res_updt_flg=mysqli_query($conn,$updt_query);

      //remove flag
      $sql_updt_payment = "UPDATE b2b_credits_payment SET b2b_approve_flag='0',b2b_mod_log='$today',b2b_pmt_scs_flg='1',b2b_pmt_scs_date='$today',b2b_crm_update_id='$crm_id' WHERE b2b_id = '$payment_id'";
      
      $res_updt_payment = mysqli_query($conn,$sql_updt_payment);

      $updt_flag=0;

      $sql_select_model="SELECT id,model,re_lead_price,re_leads,nre_lead_price,nre_leads,amount,leads,lead_price FROM garage_model_history WHERE b2b_shop_id='$shop_id' ORDER BY start_date DESC LIMIT 1;";
      // echo $sql_select_model;
      $res_select_model=mysqli_query($conn1,$sql_select_model);
      if(mysqli_num_rows($res_select_model)>0){
        $row_select_model=mysqli_fetch_array($res_select_model);
        // var_dump($row_select_model);
        // echo $leads_pack;
        $model_id=$row_select_model['id'];
        $model_val=$row_select_model['model'];
        $db_re_lead_price=$row_select_model['re_lead_price'];
        $db_nre_lead_price=$row_select_model['nre_lead_price'];
        $db_re_leads=$row_select_model['re_leads'];
        $db_nre_leads=$row_select_model['nre_leads'];
        $db_amt=$row_select_model['amount'];
        $db_leads=$row_select_model['leads'];
        $db_lead_price=$row_select_model['lead_price'];
        if($model=="Premium 2.0"){
          if($leads_pack=="Royal Enfield & Sports"){
            if($db_leads=="0"){
              $updt_flag=1;
            }
          }else if($leads_pack=="Non Royal Enfield"){
            if($db_nre_leads=="0"){
              $updt_flag=1;
            }
          }
        }else if($model=="Leads 3.0"){
          if($leads_pack=="Royal Enfield & Sports"){
            if($db_re_leads=="0"){
              $updt_flag=1;
            }
          }else if($leads_pack=="Non Royal Enfield"){
            if($db_nre_leads=="0"){
              $updt_flag=1;
            }
          }
        }
      }else{
        $model_id="";
        $model_val="";
      }
      // echo $updt_flag;
      $today1=date('Y-m-d');
      $yesterday=date('Y-m-d',strtotime("-1 days"));
      if($model==$model_val && $veh=='2w' && $model!='Pre Credit' && $updt_flag==1){
        $new_nre_leads=$nre_leads+$db_nre_leads;
        $new_nre_lead_price=($nre_lead_price==0)?$db_nre_lead_price:$nre_lead_price;
        $new_amt=$amount+$db_amt;
        if($model=="Leads 3.0"){
          $new_re_lead_price=($re_lead_price==0)?$db_re_lead_price:$re_lead_price;
          $new_re_leads=$re_leads+$db_re_leads;
          $sql_updt_model="UPDATE garage_model_history SET re_leads='$new_re_leads',nre_leads='$new_nre_leads',re_lead_price='$new_re_lead_price',nre_lead_price='$new_nre_lead_price',amount='$new_amt' WHERE id='$model_id';";
          $res_updt_model=mysqli_query($conn1,$sql_updt_model);
        }else{
          $new_re_lead_price=($re_lead_price==0)?$db_lead_price:$re_lead_price;
          $new_re_leads=$re_leads+$db_leads;
          $sql_updt_model="UPDATE garage_model_history SET leads='$new_re_leads',nre_leads='$new_nre_leads',lead_price='$new_re_lead_price',nre_lead_price='$new_nre_lead_price',amount='$new_amt' WHERE id='$model_id';";
          // echo $sql_updt_model;
          $res_updt_model=mysqli_query($conn1,$sql_updt_model);
        }
      }else{
        if($model_id!=""){
          $sql_updt_model="UPDATE garage_model_history SET end_date='$yesterday' WHERE id='$model_id';";
          $res_updt_model=mysqli_query($conn1,$sql_updt_model);
        }
        if($veh=='2w' && $model!='Pre Credit'){
          if($model=="Leads 3.0"){
            $sql_ins_model="INSERT INTO garage_model_history (b2b_shop_id,shop_name,start_date,end_date,model,credits,amount,nre_lead_price,nre_leads,re_lead_price,re_leads,flag,premium_tenure,leads,lead_price) VALUES ('$shop_id','$shop_name','$today1','0000-00-00','$model','$credits','$amount','$nre_lead_price','$nre_leads','$re_lead_price','$re_leads',0,0,0,0);";
          }else if($model=="Premium 2.0"){
            $sql_ins_model="INSERT INTO garage_model_history (b2b_shop_id,shop_name,start_date,end_date,model,credits,leads,amount,lead_price,nre_lead_price,nre_leads,flag,premium_tenure,re_lead_price,re_leads) VALUES ('$shop_id','$shop_name','$today1','0000-00-00','$model','$credits','$re_leads','$amount','$re_lead_price','$nre_lead_price','$nre_leads',0,0,0,0);";
          }
        }else{
            $sql_ins_model="INSERT INTO garage_model_history (b2b_shop_id,shop_name,start_date,end_date,model,credits,leads,amount,lead_price,nre_lead_price,nre_leads,re_lead_price,re_leads,flag,premium_tenure) VALUES ('$shop_id','$shop_name','$today1','0000-00-00','$model','$credits','$leads','$amount','$lead_price','$nre_lead_price','$nre_leads','$re_lead_price','$re_leads',0,0);";
        }
        // echo $sql_ins_model;
        $res_ins_model=mysqli_query($conn1,$sql_ins_model);
      }

  ?>
  <?php if($leads==0){ ?>
    <p style="font-size:37px; color:#000;"><?php echo $credits; ?> Credits have been added to "<?php echo strtoupper($shop_name); ?>" !</p>
  <?php }else { ?>
    <p style="font-size:37px; color:#000;"><?php echo $leads; ?> Leads have been added to "<?php echo strtoupper($shop_name); ?>" !</p>
  <?php } } 
  //find request number 
  $today_req=date('Y-m-d',strtotime($log));
  $request_query="SELECT b2b_id FROM b2b_credits_payment WHERE DATE(b2b_log)='$today_req' ORDER BY b2b_id";
  // echo $request_query;
  $request_res=mysqli_query($conn,$request_query);
  $request_arr=mysqli_fetch_all($request_res);
  // var_dump($request_arr);
  $request = array_search($payment_id, array_column($request_arr, 0))+1; 
  $txn_date=date('Ymd');
  $crm_res=mysqli_query($conn1,"SELECT name FROM crm_admin WHERE crm_log_id='$crm_id'");
  $crm=mysqli_fetch_array($crm_res);
  $crm=explode(" ",$crm[0]);
  $txn_id=$txn_date.str_pad($request, 3, '0', STR_PAD_LEFT).strtoupper($crm[0]).$mec_id;
  $invoice_no=$txn_date.str_pad($request, 3, '0', STR_PAD_LEFT);

  $det_query = mysqli_query($conn1,"SELECT address5,shop_name FROM go_bumpr.admin_mechanic_table WHERE axle_id = {$shop_id}");
  $det=mysqli_fetch_object($det_query);
  $garage=$det->shop_name;
  $city=$det->address5;
  $month=date("M-Y");
  $file=$garage.' '.$city.' '.$month.'.pdf';
  ?>

  <span style="font-size: 12pt;font-weight: bold;color: #000000">Transaction ID :</span> <?php echo $txn_id; ?><br><br>
  <a href="invoice.php?pay_id=<?php echo base64_encode($payment_id);?>&t_id=<?php echo base64_encode($txn_id);?>&id=<?php echo base64_encode($shop_id);?>" target="_blank"><button class="btn2">Preview Invoice</button></a> 

  <a href="invoice_upload.php?pay_id=<?php echo base64_encode($payment_id);?>&t_id=<?php echo base64_encode($txn_id);?>&id=<?php echo base64_encode($shop_id);?>"><button class="btn2" style="margin-left:20pt;">Send Invoice</button></a>
</div>


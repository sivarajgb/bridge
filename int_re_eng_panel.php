<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if ((empty($_SESSION['crm_log_id']))) {
	header('location:logout.php');
	die();
}

$crm_log_id = $_SESSION['crm_log_id'];
$src_crm = array('crm016', 'crm018', 'crm064', 'crm036', 'crm033', 'crm017');
$src_column = 0;
if (in_array($crm_log_id, $src_crm)) {
	$src_column = 1;
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>GoBumpr Bridge</title>
	
	<!-- Facebook Pixel Code -->
	<script async>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '582926561860139');
        fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
				   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"/></noscript>
	<!-- DO NOT MODIFY -->
	<!-- End Facebook Pixel Code -->
	<!-- Google Analytics Code -->
	<script async>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-67994843-2', 'auto');
        ga('send', 'pageview');
	
	</script>
	
	<style>
		html {
			min-height: 100%;
		}
		
		* {
			box-sizing: border-box;
		}
		
		body {
			color: black;
			background: rgb(255, 255, 255) !important;
			margin: 0px;
			min-height: inherit;
		}
		
		[data-sidebar-overlay] {
			display: none;
			position: fixed;
			top: 0px;
			bottom: 0px;
			left: 0px;
			opacity: 0;
			width: 100%;
			min-height: inherit;
		}
		
		.overlay {
			background-color: rgb(222, 214, 196);
			z-index: 999990 !important;
		}
		
		aside {
			position: relative;
			height: 100%;
			width: 200px;
			top: 0px;
			left: 0px;
			background-color: rgb(236, 239, 241);
			box-shadow: rgba(0, 0, 0, 0.8) 0px 6px 6px 0px;
			z-index: 999999 !important;
		}
		
		[data-sidebar] {
			display: none;
			position: absolute;
			height: 100%;
			z-index: 100;
		}
		
		.padding {
			padding: 2em;
		}
		
		.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
			font-family: inherit;
			font-weight: 500;
			line-height: 1.1;
			color: inherit;
		}
		
		.h4, .h5, .h6, h4, h5, h6 {
			margin-top: 10px;
			margin-bottom: 10px;
		}
		
		.h4, h4 {
			font-size: 18px;
		}
		
		img {
			border: 0px;
			vertical-align: middle;
		}
		
		a {
			text-decoration: none;
			color: black;
		}
		
		aside a {
			color: rgb(0, 0, 0);
			font-size: 16px;
			text-decoration: none;
		}
		
		.fa {
			display: inline-block;
			font-style: normal;
			font-variant: normal;
			font-weight: normal;
			font-stretch: normal;
			line-height: 1;
			font-family: FontAwesome;
			font-size: inherit;
			text-rendering: auto;
			-webkit-font-smoothing: antialiased;
		}
		
		nav, ol {
			font-size: 18px;
			margin-top: -4px;
			background: rgb(0, 150, 136) !important;
		}
		
		.container-fluid {
			padding-right: 15px;
			padding-left: 15px;
			margin-right: auto;
			margin-left: auto;
		}
		
		.breadcrumb > li {
			display: inline-block;
		}
		
		ol, ul {
			margin-top: 0px;
			margin-bottom: 10px;
		}
		
		ol ol, ol ul, ul ol, ul ul {
			margin-bottom: 0px;
		}
		
		.nav {
			padding-left: 0px;
			margin-bottom: 0px;
			list-style: none;
		}
		
		.navbar-nav {
			margin: 0px;
			float: left;
		}
		
		.navbar-right {
			margin-right: -15px;
			float: right !important;
		}
		
		.nav > li {
			position: relative;
			display: block;
		}
		
		.navbar-nav > li {
			float: left;
		}
		
		.dropdown {
			position: relative;
			display: inline-block;
		}
		
		.form-group {
			margin-bottom: 15px;
		}
		
		button, input, optgroup, select, textarea {
			margin: 0px;
			font-style: inherit;
			font-variant: inherit;
			font-weight: inherit;
			font-stretch: inherit;
			font-size: inherit;
			line-height: inherit;
			font-family: inherit;
			color: inherit;
		}
		
		button, select {
			text-transform: none;
		}
		
		button, input, select, textarea {
			font-family: inherit;
			font-size: inherit;
			line-height: inherit;
		}
		
		.form-control {
			display: block;
			width: 100%;
			height: 34px;
			padding: 6px 12px;
			font-size: 14px;
			line-height: 1.42857;
			color: rgb(85, 85, 85);
			background-color: rgb(255, 255, 255);
			background-image: none;
			border: 1px solid rgb(204, 204, 204);
			border-radius: 4px;
			box-shadow: rgba(0, 0, 0, 0.075) 0px 1px 1px inset;
			transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
		}
		
		.navbar-fixed-bottom, .navbar-fixed-top {
			position: fixed;
			right: 0px;
			left: 0px;
			z-index: 1030;
			border-radius: 0px;
		}
		
		.navbar-fixed-top {
			top: 0px;
			border-width: 0px 0px 1px;
		}
		
		.btn-group, .btn-group-vertical {
			position: relative;
			display: inline-block;
			vertical-align: middle;
		}
		
		label {
			cursor: pointer;
		}
		
		b, strong {
			font-weight: 700;
		}
		
		input {
			line-height: normal;
		}
		
		input[type="checkbox"], input[type="radio"] {
			box-sizing: border-box;
			padding: 0px;
			margin: 4px 0px 0px;
			line-height: normal;
		}
		
		[data-toggle="buttons"] > .btn input[type="checkbox"], [data-toggle="buttons"] > .btn input[type="radio"], [data-toggle="buttons"] > .btn-group > .btn input[type="checkbox"], [data-toggle="buttons"] > .btn-group > .btn input[type="radio"] {
			position: absolute;
			clip: rect(0px 0px 0px 0px);
			pointer-events: none;
		}
		
		p {
			margin: 0px 0px 10px;
		}
		
		.col-sm-offset-1 {
			margin-left: 8.33333%;
		}
		
		.col-lg-offset-1 {
			margin-left: 8.33333%;
		}
		
		.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
			position: relative;
			min-height: 1px;
			padding-right: 15px;
			padding-left: 15px;
		}
		
		.col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
			float: left;
		}
		
		.col-xs-2 {
			width: 16.6667%;
		}
		
		.col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9 {
			float: left;
		}
		
		.col-sm-2 {
			width: 16.6667%;
		}
		
		.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9 {
			float: left;
		}
		
		.col-lg-1 {
			width: 8.33333%;
		}
		
		.floating-box1 {
			display: inline-block;
			margin-top: 10px;
			float: left;
			clear: both;
		}
		
		.col-sm-3 {
			width: 25%;
		}
		
		.col-lg-3 {
			width: 25%;
		}
		
		.glyphicon {
			position: relative;
			top: 1px;
			display: inline-block;
			font-family: "Glyphicons Halflings";
			font-style: normal;
			font-weight: 400;
			line-height: 1;
			-webkit-font-smoothing: antialiased;
		}
		
		.caret {
			display: inline-block;
			width: 0px;
			height: 0px;
			margin-left: 2px;
			vertical-align: middle;
			border-top: 4px dashed;
			border-right: 4px solid transparent;
			border-left: 4px solid transparent;
		}
		
		.col-sm-1 {
			width: 8.33333%;
		}
		
		.col-sm-4 {
			width: 33.3333%;
		}
		
		button {
			overflow: visible;
		}
		
		button, html input[type="button"], input[type="reset"], input[type="submit"] {
			-webkit-appearance: button;
			cursor: pointer;
		}
		
		.btn {
			display: inline-block;
			padding: 6px 12px;
			margin-bottom: 0px;
			font-size: 14px;
			font-weight: 400;
			line-height: 1.42857;
			text-align: center;
			white-space: nowrap;
			vertical-align: middle;
			touch-action: manipulation;
			cursor: pointer;
			user-select: none;
			background-image: none;
			border: 1px solid transparent;
			border-radius: 4px;
		}
		
		.pull-right {
			float: right;
		}
		
		.form-control:focus {
			border-color: rgb(102, 175, 233);
			outline: 0px;
			box-shadow: rgba(0, 0, 0, 0.075) 0px 1px 1px inset, rgba(102, 175, 233, 0.6) 0px 0px 8px;
		}
		
		.counter {
			padding: 8px;
			color: rgb(204, 204, 204);
		}
		
		.fade {
			opacity: 0;
			transition: opacity 0.15s linear;
		}
		
		.modal {
			position: fixed;
			top: 0px;
			right: 0px;
			bottom: 0px;
			left: 0px;
			z-index: 1050;
			display: none;
			overflow: hidden;
			outline: 0px;
		}
		
		.modal.fade .modal-dialog {
			transition: transform 0.3s ease-out;
			transform: translate(0px, -25%);
		}
		
		.modal-dialog {
			position: relative;
			width: 600px;
			margin: 30px auto;
		}
		
		.modal-content {
			position: relative;
			background-color: rgb(255, 255, 255);
			-webkit-background-clip: padding-box;
			background-clip: padding-box;
			border: 1px solid rgba(0, 0, 0, 0.2);
			border-radius: 6px;
			outline: 0px;
			box-shadow: rgba(0, 0, 0, 0.5) 0px 5px 15px;
		}
		
		.modal-header {
			padding: 15px;
			border-bottom: 1px solid rgb(229, 229, 229);
		}
		
		.close {
			float: right;
			font-size: 21px;
			font-weight: 700;
			line-height: 1;
			color: rgb(0, 0, 0);
			text-shadow: rgb(255, 255, 255) 0px 1px 0px;
			opacity: 0.2;
		}
		
		button.close {
			-webkit-appearance: none;
			padding: 0px;
			cursor: pointer;
			background: 0px 0px;
			border: 0px;
		}
		
		.modal-header .close {
			margin-top: -2px;
		}
		
		.h1, .h2, .h3, h1, h2, h3 {
			margin-top: 20px;
			margin-bottom: 10px;
		}
		
		.h3, h3 {
			font-size: 24px;
		}
		
		.modal-title {
			margin: 0px;
			line-height: 1.42857;
		}
		
		.modal-body {
			position: relative;
			padding: 15px;
		}
		
		.row {
			margin-right: -15px;
			margin-left: -15px;
		}
		
		.col-sm-offset-3 {
			margin-left: 25%;
		}
		
		.col-lg-6 {
			width: 50%;
		}
		
		.col-lg-offset-3 {
			margin-left: 25%;
		}
		
		.uil-default-css {
			position: relative;
			background: none;
			width: 200px;
			height: 200px;
		}
		
		.uil-default-css > div:nth-of-type(1) {
			animation: uil-default-anim 1s linear -0.5s infinite;
		}
		
		.uil-default-css > div:nth-of-type(2) {
			animation: uil-default-anim 1s linear -0.416667s infinite;
		}
		
		.uil-default-css > div:nth-of-type(3) {
			animation: uil-default-anim 1s linear -0.333333s infinite;
		}
		
		.uil-default-css > div:nth-of-type(4) {
			animation: uil-default-anim 1s linear -0.25s infinite;
		}
		
		.uil-default-css > div:nth-of-type(5) {
			animation: uil-default-anim 1s linear -0.166667s infinite;
		}
		
		.uil-default-css > div:nth-of-type(6) {
			animation: uil-default-anim 1s linear -0.0833333s infinite;
		}
		
		.uil-default-css > div:nth-of-type(7) {
			animation: uil-default-anim 1s linear 0s infinite;
		}
		
		.uil-default-css > div:nth-of-type(8) {
			animation: uil-default-anim 1s linear 0.0833333s infinite;
		}
		
		.uil-default-css > div:nth-of-type(9) {
			animation: uil-default-anim 1s linear 0.166667s infinite;
		}
		
		.uil-default-css > div:nth-of-type(10) {
			animation: uil-default-anim 1s linear 0.25s infinite;
		}
		
		.uil-default-css > div:nth-of-type(11) {
			animation: uil-default-anim 1s linear 0.333333s infinite;
		}
		
		.uil-default-css > div:nth-of-type(12) {
			animation: uil-default-anim 1s linear 0.416667s infinite;
		}
		
		table {
			border-spacing: 0px;
			border-collapse: collapse;
			background-color: transparent;
		}
		
		.table {
			width: 100%;
			max-width: 100%;
			margin-bottom: 20px;
		}
		
		.table-bordered {
			border: 1px solid rgb(221, 221, 221);
		}
		
		td, th {
			padding: 0px;
		}
		
		th {
			text-align: left;
		}
		
		.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
			padding: 8px;
			line-height: 1.42857;
			vertical-align: top;
			border-top: 1px solid rgb(221, 221, 221);
		}
		
		.table > thead > tr > th {
			vertical-align: bottom;
			border-bottom: 2px solid rgb(221, 221, 221);
		}
		
		.table > caption + thead > tr:first-child > td, .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table > thead:first-child > tr:first-child > th {
			border-top: 0px;
		}
		
		.table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
			border: 1px solid rgb(221, 221, 221);
		}
		
		.table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
			border-bottom-width: 2px;
		}
		
		#tbody {
			font-size: 15px !important;
			border: 1.5px solid rgb(196, 184, 184) !important;
		}
		
		#tbody, tbody tr {
			animation: opacity 5s ease-in-out;
		}
		
		.btn-info {
			color: rgb(255, 255, 255);
			background-color: rgb(91, 192, 222);
			border-color: rgb(70, 184, 218);
		}
		
		.col-xs-8 {
			width: 66.6667%;
		}
		
		.col-xs-offset-2 {
			margin-left: 16.6667%;
		}
		
		.modal-footer {
			padding: 15px;
			text-align: right;
			border-top: 1px solid rgb(229, 229, 229);
		}
		
		.dropdown-menu {
			position: absolute;
			top: 100%;
			left: 0px;
			z-index: 1000;
			display: none;
			float: left;
			min-width: 160px;
			padding: 5px 0px;
			margin: 2px 0px 0px;
			font-size: 14px;
			text-align: left;
			list-style: none;
			background-color: rgb(255, 255, 255);
			-webkit-background-clip: padding-box;
			background-clip: padding-box;
			border: 1px solid rgba(0, 0, 0, 0.15);
			border-radius: 4px;
			box-shadow: rgba(0, 0, 0, 0.176) 0px 6px 12px;
		}
		
		button[disabled], html input[disabled] {
			cursor: default;
		}
		
		.btn.disabled, .btn[disabled], fieldset[disabled] .btn {
			cursor: not-allowed;
			box-shadow: none;
			opacity: 0.65;
		}
		
		.btn-success {
			color: rgb(255, 255, 255);
			background-color: rgb(92, 184, 92);
			border-color: rgb(76, 174, 76);
		}
		
		.btn-group-sm > .btn, .btn-sm {
			padding: 5px 10px;
			font-size: 12px;
			line-height: 1.5;
			border-radius: 3px;
		}
		
		.btn-default {
			color: rgb(51, 51, 51);
			background-color: rgb(255, 255, 255);
			border-color: rgb(204, 204, 204);
		}
		
		.body {
			overflow-x: scroll !important;
		}
		
		#datepick > span:hover {
			cursor: pointer;
		}
		
		.top-box {
			display: inline-block;
			z-index: 1;
			height: 30px;
			width: 90px;
			background-color: #B6A3BF;
			-webkit-display: button;
			box-shadow: 0px 8px 8px 0px rgba(0, 0, 0, 0.2);
		}
		
		.floating-box {
			display: inline-block;
			margin: 10px;
			padding: 12px;
			width: 183px;
			height: 75px;
			box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
			z-index: 1;
		}
		
		.floating-box1 {
			display: inline-block;
			margin-top: 10px;
			float: left;
			clear: both;
		}
		
		input[] {
			visibility: hidden;
		}
		
		label {
			cursor: pointer;
		}
		
		input:checked + label {
			background: red;
		}
		
		/* table */
		#tbody {
			font-size: 15px !important;
			border: 1.5px solid #c4b8b8 !important;
			
		}
		
		thead:hover {
			cursor: pointer;
		}
		
		.results tr[visible='false'],
		.no-result {
			display: none;
		}
		
		.results tr[visible='true'] {
			display: table-row;
		}
		
		.counter {
			padding: 8px;
			color: #ccc;
		}
		
		#tbody, tbody tr {
			-webkit-animation: opacity 5s ease-in-out;
			animation: opacity 5s ease-in-out;
		}
		
		p[data-title]:hover:after {
			content: attr(data-title);
			padding: 4px 8px;
			color: #333;
			position: absolute;
			left: 0;
			top: 100%;
			white-space: nowrap;
			z-index: 20px;
			-moz-border-radius: 5px;
			-webkit-border-radius: 5px;
			border-radius: 5px;
			-moz-box-shadow: 0px 0px 4px red;
			-webkit-box-shadow: 0px 0px 4px red;
			box-shadow: 0px 0px 4px #222;
			background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
			background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #eeeeee), color-stop(1, #cccccc));
			background-image: -webkit-linear-gradient(top, #eeeeee, #cccccc);
			background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
			background-image: -ms-linear-gradient(top, #eeeeee, #cccccc);
			background-image: -o-linear-gradient(top, #eeeeee, #cccccc);
		}
	</style>
</head>
<body>
<?php include_once("header.php"); ?>

<div class="overlay" data-sidebar-overlay></div>

<div class="padding"></div>

<div class="navbar-fixed-top" style=" margin-top:52px; padding-top: 8px; padding-bottom:10px; background-color:#fff;">
	
	<!-- level filter -->
	<div class=" col-sm-2 col-lg-1" style="margin-left: 22px;min-width:140px;">
		<div class="floating-box1">
			<div style="max-width:120px;">
				<select id="level" name="level" class="form-control" style="min-width:140px;">
					<option value="all" selected>All Levels</option>
					<option value="2">L2-GoAxle</option>
					<option value="3">L3-Completed</option>
					<option value="4">L4-Dissatisfied Customer</option>
					<option value="5">L5-Delighted Customer</option>
				</select>
			</div>
		</div>
	</div>
	
	<!-- vehicle filter -->
	<div class=" col-sm-2 col-lg-1" style="margin-left: 22px;">
		<div class="floating-box1">
			<div style="max-width:120px;">
				<select id="vehicle" name="vehicle" class="form-control" style="width:120px;">
					<option value="all" selected>All Vehicles</option>
					<option value="2w">2 Wheeler</option>
					<option value="4w">4 Wheeler</option>
				</select>
			</div>
		</div>
	</div>
	
	<!-- date range picker -->
	<div id="reportrange" class=" col-sm-3 col-lg-3" style="cursor: pointer; margin-left:22px;">
		<div class=" floating-box1">
			<div id="range" class="form-control" style="min-width:312px;">
				<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
				<span id="dateval"></span> <b class="caret"></b>
			</div>
		</div>
	</div>
	
	<div class="col-sm-2 col-lg-2" style="cursor: pointer;">
		<div class=" floating-box1">
			<select id="service" name="service" class="form-control" style="width:130px;">
				<option value="all" selected>All Services</option>
				<?php
				$sql_service = "SELECT service_type FROM go_axle_service_price_tbl WHERE bridge_flag='0' ORDER BY service_type ASC";
				$res_service = mysqli_query($conn, $sql_service);
				while ($row_service = mysqli_fetch_object($res_service)) {
					$c_service = $row_service->service_type;
					?>
					<option value="<?php echo $c_service; ?>"> <?php echo $c_service; ?> </option>
					<?php
				}
				?>
			</select>
		</div>
	</div>
	<!-- search bar -->
	<div class=" col-sm-4 ">
		<div class="floating-box1">
			<form id="myForm" method="post" action="check_user.php" onsubmit="return ValidationEvent()">
				<button class="form-group pull-right btn btn-small" type="submit" id="search_submit"
						name="search_submit"
						style=" width:50px;height:34px; margin-left:12px;background-color:#B2DFDB;font-size:15px;"><i
							class="fa fa-search" aria-hidden="true"></i></button>
				<div class="form-group pull-right">
					<input type="text" class="search form-control" id="su" name="searchuser" placeholder="Search">
					<input type="hidden" id="search_type" name="search_type" value="">
				</div>
				<span class="counter pull-right"></span>
			</form>
		</div>
	</div>

</div>

<!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
	<div class='uil-default-css' style='transform:scale(0.58);'>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
		<div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
		</div>
	</div>
</div>

<!-- table -->
<div align="center" id="table" style="margin-top:78px;margin-left:10px;margin-right:10px; display: none;">
	<table id="example" class="table table-striped table-bordered tablesorter table-hover results">
		<thead style="background-color: #D3D3D3;">
		<th>CustomerName</th>
		<th>View</th>
		<?php if (!$_SESSION['eupraxia_flag']) { ?>
			<th>Mobile</th>
		<?php } ?>
		<th>Locality</th>
		<th>Service</th>
		<th>Vehicle</th>
		<th>ShopName</th>
		<th>BookingDate</th>
		<th>GoAxleStatus</th>
		<th>Rating</th>
		<th>Review</th>
		</thead>
		<tbody id="tbody">
		</tbody>
	</table>
</div>
<noscript id="async-styles">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
		  integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
		  integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css"/>
</noscript>
<script> // The CSS mentioned in "async-styles" will be loaded and styles will be applied.
    var loadDeferredStyles = function () {
        var addStylesNode = document.getElementById("async-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
        webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function () {
        window.setTimeout(loadDeferredStyles, 0);
    });
    else window.addEventListener('load', loadDeferredStyles);
</script>

<script async src="js/jquery-3.2.1.min.js" onLoad="loadOtherScripts();"></script>

<script>
    function loadScript(src) {
        return new Promise(function (resolve, reject) {
            var s;
            s = document.createElement('script');
            s.src = src;
            s.onload = resolve;
            s.onerror = reject;
            document.head.appendChild(s);
        });
    }

    function loadOtherScripts() {
        loadScript('js/moment.min.js')
            .then(function () {
                Promise.all([loadScript('js/daterangepicker.js'), loadScript('js/sidebar.js'), loadScript('https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js')]).then(function () {
                    // console.log('scripts are loaded');
                    dateRangePicker();
                    momentInPicker();
                    intialFunction();
                    tablesort();
                    searchbar();
                }).catch(function (error) {
                    //  console.log('some error!' + error)
                })
            }).catch(function (error) {
            //  console.log('Moment call error!' + error)
        })
    }
</script>

<!-- table sorter -->
<script>
    function tablesort() {
        $(document).ready(function () {
                $("#example").tablesorter({sortList: [11, 0]});
            }
        );
    }
</script>
<!-- search bar  -->
<script>
    function searchbar() {
        $(document).ready(function () {
            $(".search").keyup(function () {
                var searchTerm = $(".search").val();
                var listItem = $('.results tbody').children('tr');
                var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

                $.extend($.expr[':'], {
                    'containsi': function (elem, i, match, array) {
                        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
                    }
                });

                $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function (e) {
                    $(this).attr('visible', 'false');
                });

                $(".results tbody tr:containsi('" + searchSplit + "')").each(function (e) {
                    $(this).attr('visible', 'true');
                });

                var jobCount = $('.results tbody tr[visible="true"]').length;
                $('.counter').text(jobCount + ' item');

                if (jobCount == '0') {
                    $('.no-result').show();
                } else {
                    $('.no-result').hide();
                }
            });
        });
    }
</script>

<!-- date range picker -->
<script>
    function dateRangePicker() {
        $('input[name="daterange"]').daterangepicker({
            locale: {
                format: 'DD-MM-YYYY'
            }
        });
    }
</script>
<script type="text/javascript">
    function momentInPicker() {
        $(function () {

            var start = moment().subtract(270, 'days');
            // var start = moment();
            var end = moment().subtract(60, 'days');
            var maxDate = moment().subtract(60, 'days');

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                maxDate: maxDate,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

        });
    }
</script>
<!-- default view -->
<script>
    function viewtable() {
        var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
        var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
        var level = $('#level').val();
        var service = $('#service').val();
        var vehicle = $('#vehicle').val();
        var city = $('#city').val();
        var cluster = $('#cluster').val();
        //console.log(startDate);
        //console.log(endDate);
        //Make AJAX request, using the selected value as the POST
        $.ajax({
            url: "ajax/int_re_eng_panel_view.php",  // create a new php page to handle ajax request
            type: "GET", json: false,
            data: {
                "startdate": startDate,
                "enddate": endDate,
                "service": service,
                "level": level,
                "vehicle": vehicle,
                "city": city,
                "cluster": cluster
            },
            success: function (data) {
                //alert(data);
                //console.log(data);
                $("#loading").hide();
                $("#table").show();
                $("#tbody").empty();
                if (data === "no") {
                    $("#tbody").html("<h2 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h2>");
                } else {
                    function displayInfo(item, index) {
                        $('#tbody').append(item['tr']);
                        //console.log(item);
                    }

                    $.map($.parseJSON(data), displayInfo);
                    $("#example").trigger("update");
                }
                counterval();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // alert(xhr.status + " "+ thrownError);
            }
        });
    }
</script>
<script>
    function counterval() {
        var jobCount = $("#tbody tr").length;
        ;
        $('.counter').text(jobCount + ' item');
    }
</script>
<!-- initial function -->
<script>
    function intialFunction() {
        $(document).ready(function () {
            $('#city').show();
            $('#cluster').show();
            $("#table").hide();
            $("#loading").show();
            viewtable();

            $('#dateval').on("DOMSubtreeModified", function () {
                //console.log("date changed");
                $("#table").hide();
                $("#loading").show();
                viewtable();
            });

            $("#level").on("change", function () {
                //console.log("source changed");
                $("#table").hide();
                $("#loading").show();
                viewtable();
            });

            $("#vehicle").change(function () {
                $("#table").hide();
                $("#loading").show();
                viewtable();
            });

            $('#city').change(function () {
                $("#table").hide();
                $("#loading").show();
                viewtable();
            });

            $('#cluster').change(function () {
                $("#table").hide();
                $("#loading").show();
                viewtable();
            });

            $("#service").on("change", function () {
                $("#table").hide();
                $("#loading").show();
                viewtable();
            });
        });
    }
</script>

<!-- search/add user -->
<script type="text/javascript">
    function ValidationEvent() {
        event.preventDefault();
        var mobile = document.getElementById("su").value;
        var phoneno = /^[1-9][0-9]*$/;
        // var emailid = /\S+@\S+\.\S+/;
        var emailid = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        var name = /^[a-zA-Z\s]+$/;

        //console.log(phoneno.test(mobile))
        if (phoneno.test(mobile)) {
            if ((mobile.length >= 7 && mobile.length < 10) || mobile.length > 10 || mobile.length <= 4) {
                document.getElementById("search_type").value = "";
                alert("Please enter a valid BookingId / Mobile number !");
            } else if (mobile.length <= 7) {
                document.getElementById("search_type").value = "booking";
                document.getElementById("myForm").submit();
            } else {
                document.getElementById("search_type").value = "mobile";
                document.getElementById("myForm").submit();
            }
        } else if (emailid.test(mobile)) {
            document.getElementById("search_type").value = "email";
            document.getElementById("myForm").submit();
        } else if (name.test(mobile)) {
            document.getElementById("search_type").value = "username";
            document.getElementById("myForm").submit();
        } else {
            document.getElementById("search_type").value = "";
            alert("Please enter a valid Booking Id / Mobile Number / Email / User Name !");
        }
    }
</script>

</body>
</html>

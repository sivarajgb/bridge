<?php
include("../config.php");
$conn = db_connect1();
session_start();
date_default_timezone_set('Asia/Kolkata');
// $crm_log_id = $_SESSION['crm_log_id'] ;
// $crm_name = $_SESSION['crm_name'];
// $flag=$_SESSION['flag'];
$today = date("Y-m-d");
$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$city =  $_POST['city'];

// $city = $_POST['city'];  
// $_SESSION['crm_city'] = $city;
$no = 0;
?>


  <div id="div1" style="width:100%;float:left;" align="center">
    <table class="table table-striped table-bordered tablesorter table-hover results" id="table" style="width: 80%;font-size:12px;">
    <thead style="background-color:#B2DFDB;align:center;display: table-header-group;">
    <th>S.No<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Booking ID <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Name <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Mobile Number <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Vehicle<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Locality<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Year of Purchase<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Kms Driven<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Choice of inspection<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Inspection Request Log<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>User Register Log<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>UTM Source<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>

    </thead>
    <tbody id="tbody">

    <?php
  $cond = '';
  $cond = $cond. ($city == 'all' ? "": " AND u.city='$city' ");
  // $cond = $cond.($veh == "all" ? "" : "AND m.type='$veh'");
  // $cond = $cond.($shop_status == "active" ? "AND b2b_m.b2b_avail='1'" : "AND b2b_m.b2b_avail='0'");
    $sql = "SELECT u.user_id,u.year_of_purchase,u.kms_driven,u.doorstep,u.utm_source,u.locality,r.name,r.mobile_number,v.brand,v.model,u.id,u.log as requested_date,r.log as register_log FROM used_cars_booking u LEFT JOIN user_register r ON r.reg_id = u.user_id LEFT JOIN user_vehicle_table v ON v.id = u.user_veh_id WHERE DATE(u.log) BETWEEN '$startdate' and '$enddate' and u.flag=0 {$cond};";
    $res = mysqli_query($conn,$sql);

      // $list_credits = array();
     $num = mysqli_num_rows($res);
    if($num>0)
    {
	$n=0;
    while($row = mysqli_fetch_object($res))
    {
		$n = $n+1;
        $booking_id = 'GBPRSC'.$row->id;
        $name = $row->name;
        $mobile_number = $row->mobile_number;
        $brand = $row->brand;
        $model = $row->model;
		$vehicle = $brand.' '.$model;
$locality=$row->locality;
        $purchase_year = $row->year_of_purchase;
        $utm_source = $row->utm_source;
        $doorstep = $row->doorstep;
		$inspection_choice = $doorstep == 1 ? "Valuation at your doorstep" : "Valuation at our outlet" ;
        $kms_driven = $row->kms_driven;
		switch($kms_driven)
		{
			case '20000' : 	$kms_driven = 'less than 20K kms';
							break;
			case '20001' :	$kms_driven = '20,001 - 40K kms';
							break;
			case '40001' :	$kms_driven = '40,001 - 60K kms';
							break;
			case '60001' :	$kms_driven = '60,001 - 100K kms';
							break;
			case '100001' :	$kms_driven = 'more than 100K kms';
							break;
		}
        $requested_date = $row->requested_date;
        $user_register_log = $row->register_log;
        ?>
        <tr>
		
      <td><?php echo $n;?></td>
      <td><?php echo $booking_id;?></td>
      <td><?php echo $name;?></td>
      <td><?php echo $mobile_number;?></td>
      <td><?php echo $vehicle;?></td>
      <td><?php echo $locality;?></td>
      <td><?php echo $purchase_year;?></td>
      <td><?php echo $kms_driven;?></td>
      <td><?php echo $inspection_choice;?></td>
	  <td><?php echo date('d M Y H:i a', strtotime($requested_date));?></td>
      <td><?php echo date('d M Y H:i a', strtotime($user_register_log));?></td>
	  <td><?php echo $utm_source;?></td>

        </tr>
    
    <?php
}
}
else{
  ?>  
      <th colspan="7" style="font-size: 14px;text-align:center;padding-top: 40px;">No results found...! </th>
 <?php
}
?>

    </tbody>
    </table>
  </div>
<!-- search bar  -->
<script>
$(document).ready(function() {
  var no = "<?php echo $no; ?>";

if(no!='0')
{
$('.counter').text(no + ' shops');
}
  $(".search").keyup(function () {
    var searchTerm = $(".search").val();
    var listItem = $('.results tbody').children('tr');
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

  $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });

  $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','false');
  });

  $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','true');
  });
  var jobCount = $('.results tbody tr[visible="true"]').length;
    $('.counter').text(jobCount + ' shops');

  if(jobCount == '0') {$('.no-result').show();}
    else {$('.no-result').hide();
  }
 });
});
</script>


<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#table").tablesorter( {sortList: [[0,0], [1,0]]} );
    }
);
</script>
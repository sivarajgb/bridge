<?php
include("../config.php");
$conn = db_connect1();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];

$city = $_POST['city'];
$start = $_POST['start'];
$end = $_POST['end'];
/*$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate = date('Y-m-d',strtotime($_POST['enddate']));*/
?>

<div class="modal fade" id="myModal_duplicate_allservices" role="dialog" >
	<div class="modal-dialog" style="width:860px;">

	 <!-- Modal content-->
	 <div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				 <h3 class="modal-title">Duplicate Booking Details</h3>
			</div>
			<div class="modal-body" style="max-width:93%; margin-top:6px;margin-left:29px; height:450px !important;overflow:auto !important;">
				<table class="table table-bordered table-hover">
					<thead style="background-color: #D3D3D3;">
						<th>No.</th>
						<th>Booking ID</th>
						<th>Customer Name</th>
						<th>Mobile</th>
						<th>Brand</th>
						<th>Model</th>
						<th>Service Type</th>
						<th>Service Date</th>
					</thead>
					<tbody>
        <?php  
					if($city == "all"){
						$sql_dup = "SELECT b.booking_id,r.name,r.mobile_number,v.brand,v.model,b.service_type,b.service_date FROM user_booking_tb b,user_register r,admin_vehicle_table_new v WHERE b.user_id = r.reg_id AND b.vech_id = v.id AND  b.flag_unwntd='1' AND b.flag ='1' AND b.activity_status = '26' AND b.mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' ";
					}
					else{
						$sql_dup = "SELECT b.booking_id,r.name,r.mobile_number,v.brand,v.model,b.service_type,b.service_date FROM user_booking_tb b,user_register r,admin_vehicle_table_new v WHERE b.user_id = r.reg_id AND b.vech_id = v.id AND  b.flag_unwntd='1' AND b.flag ='1' AND b.activity_status = '26' AND b.city='$city' AND b.mec_id NOT IN(400001,200018,200379,400974) AND b.log BETWEEN '$start' AND '$end' ";
					}
                     $res_dup = mysqli_query($conn,$sql_dup);
                     $n_dup = 0;
                    while($row_dup = mysqli_fetch_object($res_dup)){
                      ?>
                      <tr style="cursor:pointer;" class='click' data-href="user_details.php?t=Yw==&bi=<?php echo base64_encode($row_dup->booking_id);?>">
                        <td><?php echo $n_dup=$n_dup+1; ?> </td>
                        <td><?php echo $row_dup->booking_id; ?></td>
                        <td><?php echo $row_dup->name; ?></td>
                        <td><?php echo $row_dup->mobile_number; ?></td>
                        <td><?php echo $row_dup->brand; ?></td>
                        <td><?php echo $row_dup->model; ?></td>
                        <td><?php echo $row_dup->service_type; ?></td>
                        <td><?php if($row_dup->service_date=='0000-00-00') echo "To be updated.";else echo $row_dup->service_date; ?></td>
                      </tr>
                      <?php
                    };
                    ?>
					</tbody>
				</table>
</div> <!-- modal body -->
</div> <!-- modal content -->
</div>  <!-- modal dailog -->
</div>  <!-- modal -->


<div class="modal fade" id="myModal_duplicate_2wServices" role="dialog" >
	<div class="modal-dialog" style="width:860px;">

	 <!-- Modal content-->
	 <div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				 <h3 class="modal-title">Duplicate Booking Details</h3>
			</div>
			<div class="modal-body" style="max-width:93%; margin-top:6px;margin-left:29px; height:450px !important;overflow:auto !important;">
				<table class="table table-bordered table-hover">
					<thead style="background-color: #D3D3D3;">
						<th>No.</th>
						<th>Booking ID</th>
						<th>Customer Name</th>
						<th>Mobile</th>
						<th>Brand</th>
						<th>Model</th>
						<th>Service Type</th>
						<th>Service Date</th>
					</thead>
					<tbody>
        <?php  
					if($city == "all"){
						$sql_dup = "SELECT b.booking_id,r.name,r.mobile_number,v.brand,v.model,b.service_type,b.service_date FROM user_booking_tb b,user_register r,admin_vehicle_table_new v WHERE b.user_id = r.reg_id AND b.vech_id = v.id AND b.vehicle_type = '2w' AND b.flag_unwntd='1' AND b.flag ='1' AND b.activity_status = '26' AND b.mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' ";
					}
					else{
						$sql_dup = "SELECT b.booking_id,r.name,r.mobile_number,v.brand,v.model,b.service_type,b.service_date FROM user_booking_tb b,user_register r,admin_vehicle_table_new v WHERE b.user_id = r.reg_id AND b.vech_id = v.id AND b.vehicle_type = '2w' AND  b.flag_unwntd='1' AND b.flag ='1' AND b.activity_status = '26' AND b.city='$city' AND b.mec_id NOT IN(400001,200018,200379,400974) AND b.log BETWEEN '$start' AND '$end' ";
					}
                     $res_dup = mysqli_query($conn,$sql_dup);
                     $n_dup = 0;
                    while($row_dup = mysqli_fetch_object($res_dup)){
                      ?>
                      <tr style="cursor:pointer;" class='click' data-href="user_details.php?t=Yw==&bi=<?php echo base64_encode($row_dup->booking_id);?>">
					  <td><?php echo $n_dup=$n_dup+1; ?> </td>
                        <td><?php echo $row_dup->booking_id; ?></td>
                        <td><?php echo $row_dup->name; ?></td>
                        <td><?php echo $row_dup->mobile_number; ?></td>
                        <td><?php echo $row_dup->brand; ?></td>
                        <td><?php echo $row_dup->model; ?></td>
                        <td><?php echo $row_dup->service_type; ?></td>
                        <td><?php if($row_dup->service_date=='0000-00-00') echo "To be updated.";else echo $row_dup->service_date; ?></td>
                      </tr>
                      <?php
                    };
                    ?>
					</tbody>
				</table>
</div> <!-- modal body -->
</div> <!-- modal content -->
</div>  <!-- modal dailog -->
</div>  <!-- modal -->


<div class="modal fade" id="myModal_duplicate_4wServices" role="dialog" >
	<div class="modal-dialog" style="width:860px;">

	 <!-- Modal content-->
	 <div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				 <h3 class="modal-title">Duplicate Booking Details</h3>
			</div>
			<div class="modal-body" style="max-width:93%; margin-top:6px;margin-left:29px; height:450px !important;overflow:auto !important;">
				<table class="table table-bordered table-hover">
					<thead style="background-color: #D3D3D3;">
						<th>No.</th>
						<th>Booking ID</th>
						<th>Customer Name</th>
						<th>Mobile</th>
						<th>Brand</th>
						<th>Model</th>
						<th>Service Type</th>
						<th>Service Date</th>
					</thead>
					<tbody>
        <?php  
					if($city == "all"){
						$sql_dup = "SELECT b.booking_id,r.name,r.mobile_number,v.brand,v.model,b.service_type,b.service_date FROM user_booking_tb b,user_register r,admin_vehicle_table_new v WHERE b.user_id = r.reg_id AND b.vech_id = v.id AND b.vehicle_type = '4w' AND b.flag_unwntd='1' AND b.flag ='1' AND b.activity_status = '26' AND b.mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' ";
					}
					else{
						$sql_dup = "SELECT b.booking_id,r.name,r.mobile_number,v.brand,v.model,b.service_type,b.service_date FROM user_booking_tb b,user_register r,admin_vehicle_table_new v WHERE b.user_id = r.reg_id AND b.vech_id = v.id AND b.vehicle_type = '4w' AND b.flag_unwntd='1' AND b.flag ='1' AND b.activity_status = '26' AND b.city='$city' AND b.mec_id NOT IN(400001,200018,200379,400974) AND b.log BETWEEN '$start' AND '$end' ";
					}
                     $res_dup = mysqli_query($conn,$sql_dup);
                     $n_dup = 0;
                    while($row_dup = mysqli_fetch_object($res_dup)){
                      ?>
                      <tr style="cursor:pointer;" class='click' data-href="user_details.php?t=Yw==&bi=<?php echo base64_encode($row_dup->booking_id);?>">
                        <td><?php echo $n_dup=$n_dup+1; ?> </td>
                        <td><?php echo $row_dup->booking_id; ?></td>
                        <td><?php echo $row_dup->name; ?></td>
                        <td><?php echo $row_dup->mobile_number; ?></td>
                        <td><?php echo $row_dup->brand; ?></td>
                        <td><?php echo $row_dup->model; ?></td>
                        <td><?php echo $row_dup->service_type; ?></td>
                        <td><?php if($row_dup->service_date=='0000-00-00') echo "To be updated.";else echo $row_dup->service_date; ?></td>
					  </tr>
                      <?php
                    };
                    ?>
					</tbody>
				</table>
</div> <!-- modal body -->
</div> <!-- modal content -->
</div>  <!-- modal dailog -->
</div>  <!-- modal -->

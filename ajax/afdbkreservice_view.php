<?php
//error_reporting(E_ALL);
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$super_flag = $_SESSION['super_flag'];

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$vehicle = $_GET['vehicle'];
$person = $_GET['person'];
$service = $_GET['service'];
$shop_id = $_GET['shop_id'];
$city = $_GET['city'];

$_SESSION['crm_city'] = $city;
$source_type =$_GET['source_type'];

$cond ='';

$st_enc = base64_encode($startdate);
$ed_enc = base64_encode($enddate);
$dt_enc = $st_enc.':'.$ed_enc;

$cond = $cond.($vehicle == 'all' ? "" : "AND f.veh_type='$vehicle'");
$cond = $cond.($service == 'all' ? "" : "AND f.service_type='$service'");
$cond = $cond.($shop_id == 'all' ? "" : "AND f.shop_id='$shop_id'");
$cond = $cond.($city == 'all' ? "" : "AND f.city='$city'");
$cond = $cond.($person == 'all' ? "" : "AND f.crm_log_id='$person'");

if($source_type=='all'){
		$cond.="";
	}
	else if($source_type=='coreops'){
		$cond.=" and ub.source!='Re-Engagement Bookings'";
	}
	else if($source_type=='reng'){
		$cond.=" and ub.source='Re-Engagement Bookings'";
	}

$sql_goaxles = "SELECT f.booking_id,b.b2b_log,b.b2b_customer_name,b.b2b_cust_phone,f.shop_name,f.crm_log_id,b.brand,b.model,f.service_type,b.b2b_bill_amount,f.rating,b.b2b_Feedback,b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_vehicle_ready,f.comments,f.final_bill_amt,c.b2b_partner_flag,f.service_status_reason,crm.name as crm_name FROM  b2b.b2b_booking_tbl AS b LEFT JOIN go_bumpr.feedback_track AS f ON b.b2b_booking_id = f.b2b_booking_id  LEFT JOIN b2b.b2b_credits_tbl c ON f.shop_id = c.b2b_shop_id LEFT JOIN crm_admin crm ON f.crm_log_id = crm.crm_log_id join go_bumpr.user_booking_tb as ub on f.booking_id = ub.booking_id  WHERE f.feedback_status = 1 AND f.reservice_flag = 1 AND f.flag = 0 {$cond} AND f.shop_id NOT IN (1014,1035,1670) AND DATE(b.b2b_log) BETWEEN '$startdate' AND '$enddate' ORDER BY c.b2b_partner_flag DESC";
$res_goaxles = mysqli_query($conn,$sql_goaxles);
$no = 0;
$arr = array();
$count = mysqli_num_rows($res_goaxles);
if($count >0){
  while($row_goaxles = mysqli_fetch_object($res_goaxles)){
    $booking_id = $row_goaxles->booking_id;
    $goaxle_date = $row_goaxles->b2b_log;
    $name = $row_goaxles->b2b_customer_name;
    $mobile_number = $row_goaxles->b2b_cust_phone;
    $service_center = $row_goaxles->shop_name;
    $vehicle = $row_goaxles->brand." ".$row_goaxles->model;
	$service_type = $row_goaxles->service_type;
	$garage_bill = $row_goaxles->b2b_bill_amount;
	$cust_rating = $row_goaxles->rating;
	$cust_feedback = $row_goaxles->b2b_Feedback;
	$inspection_stage = $row_goaxles->b2b_check_in_report;
	$estimate_stage = $row_goaxles->b2b_vehicle_at_garage;
	$deliver_stage = $row_goaxles->b2b_vehicle_ready;
	$final_bill_paid = $row_goaxles->final_bill_amt;
	$comments = $row_goaxles->comments;
	$alloted_to = $row_goaxles->crm_name;
	$alloted_to_id = $row_goaxles->crm_log_id;
	$reason=$row_goaxles->service_status_reason;
	$premium = $row_goaxles->b2b_partner_flag;
	$no = $no+1;
	if($alloted_to_id =='' || $alloted_to_id==' ' || $alloted_to_id=='0'){
		$tr = '<tr style="background-color:#EF5350 !important;">';
	}
	else{
		$tr = '<tr>';
	}
	
      
          $td1 = '<td style="vertical-align: middle;">'.$no.'</td>';
          $td1 = $td1.'<td style="vertical-align: middle;">'.$booking_id.'</td>';
          $td2 = '<td style="vertical-align: middle;">'.date('d M Y', strtotime($goaxle_date)).'</td>';
          $td3 = '<td style="vertical-align: middle;"><a href="feedback_details.php?bi='.base64_encode($booking_id).'&pg='.base64_encode('r').'&dt='.base64_encode($dt_enc).'"><i id="'.$booking_id.'" class="fa fa-eye" aria-hidden="true"></i></td>';
		  $td4 = '<td style="vertical-align: middle;">'.$name.'</td>';
          $td5 = '<td style="vertical-align: middle;">'.$mobile_number.'</td>';
          $td6 = '<td style="vertical-align: middle;">'.$service_center;
		  if ($premium == 2) {  $td6 = $td6."<img src='images/authorized.png' style='width:22px;' title='Premium Garage'>";}
		  $td6 = $td6.'</td>';
		  $td7 = '<td style="vertical-align: middle;">'.$vehicle.'</td>';	
		  $td8 = '<td style="vertical-align: middle;">'.$service_type.'</td>';	
		  $td18 = '<td style="vertical-align: middle;">'.$comments.'</td>';			  
		  $td9 = '<td style="vertical-align: middle;">'.$alloted_to.'</td>';	
		  $td10 = '<td style="vertical-align: middle;">';
		  $garage_bill!='' ? $td10 = $td10.'<i class="fa fa-inr" aria-hidden="true" style="padding-right: 2px;padding-left: 1px;"></i>'.$garage_bill : $td10 = $td10.'Not updated';
		  $td10 = $td10.'</td>';
		  $td11 = '<td style="vertical-align: middle;">';
		  $cust_rating!='' ? $td11 = $td11.$cust_rating : $td11 = $td11.'Not updated';
		  $td11 = $td11.'</td>';
		  $td12 = '<td style="vertical-align: middle;">';
		  $cust_feedback!='' ? $td12 = $td12.$cust_feedback : $td12 = $td12.'Not updated';
		  $td12 = $td12.'</td>';
		  $td13 = '<td style="vertical-align: middle;">'.$final_bill_paid.'</td>';
		  $td14 = '<td style="vertical-align: middle;">'.$reason.'</td>';
		  $td15 = "<td style='vertical-align: middle;'>";
		  $inspection_stage == '1' ? $td15 = $td15."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Done!'></i>" : $td15 = $td15."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Not updated!'></i>";
		  $td15 = $td15."</td>";
		  $td16 = "<td style='vertical-align: middle;'>";
		  $estimate_stage == '1' ? $td16 = $td16."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Done!'></i>" : $td16 = $td16."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Not updated!'></i>";
		  $td16 = $td16."</td>";
		  $td17 = "<td style='vertical-align: middle;'>";
		  $deliver_stage == '1' ? $td17 = $td17."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Done!'></i>" : $td17 = $td17."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Not updated!'></i>";
		  $td17 = $td17."</td>";
          
		  
		  $tr_l = '</tr>';
      
          $str = $tr.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$td9.$td18.$td10.$td11.$td12.$td13.$td14.$td15.$td16.$td17.$tr_l;
          $data[] = array('tr'=>$str);
            
        }
       
        echo $data1 = json_encode($data);
} // if
else {
  echo "no";
}


?>

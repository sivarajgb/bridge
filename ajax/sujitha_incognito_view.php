<?php
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$starttime = date('H:i:s',strtotime($_GET['startTime']));
$endtime =  date('H:i:s',strtotime($_GET['endTime']));
$start = $startdate.' '.$starttime;
$end = $enddate.' '.$endtime;
// $startdate = date('Y-m-d',strtotime($_GET['startdate']));
// $enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$city = $_GET['city'];

$_SESSION['crm_city'] = $city;

$cond1 ='';
$cond1 = $cond1.($city == 'all' ? "" : "AND city='$city'");

$sql1 = "SELECT crm_log_id,name FROM crm_admin WHERE crm_flag = 1 and flag = 0 {$cond1} ORDER BY crm_log_id;";
$res1 = mysqli_query($conn,$sql1);
$crm_list = "('";
$i = 0;
$no_of_rows = mysqli_num_rows($res1);
while($row1 = mysqli_fetch_object($res1))
{
	$i++;
	$crm_list = $crm_list.$row1->crm_log_id."'";
	if($i != $no_of_rows)
	{
		$crm_list = $crm_list.",'";
	}
	$crms_arr[$row1->crm_log_id]['name'] = $row1->name;
	$crms_arr[$row1->crm_log_id]['crm_log_id'] = $row1->crm_log_id;
	$crms_arr[$row1->crm_log_id]['goaxles'] = 0;
	$crms_arr[$row1->crm_log_id]['converted'] = 0;
	$crms_arr[$row1->crm_log_id]['unique'] = 0;
	$crms_arr[$row1->crm_log_id]['total'] = 0;
}
$crm_list = $crm_list.")";
// print_r($crms_arr);

$sql2 = "SELECT touchpoints.crm_log_id,touchpoints.unique_count,touchpoints.total_count FROM (SELECT crm_log_id,count(DISTINCT user_id) as unique_count,count(user_id) as total_count FROM admin_comments_tbl where crm_log_id in $crm_list and log BETWEEN '$start' AND '$end' GROUP BY crm_log_id) as touchpoints;";
$res2 = mysqli_query($conn,$sql2);
while($row2 = mysqli_fetch_object($res2))
{
	$crms_arr[$row2->crm_log_id]['unique'] = $row2->unique_count;
	$crms_arr[$row2->crm_log_id]['total'] = $row2->total_count;
	$crms_arr[$row2->crm_log_id]['crm_log_id'] = $row2->crm_log_id;
}
// print_r($crms_arr);

$sql_booking = "SELECT b.booking_id,b.booking_status,b.service_status,b.crm_update_id,b2b_b.b2b_check_in_report,b2b_b.b2b_vehicle_at_garage,b2b_b.b2b_vehicle_ready FROM user_booking_tb b LEFT JOIN b2b.b2b_booking_tbl b2b_b ON b.booking_id = b2b_b.gb_booking_id WHERE b.booking_status = '2' AND b.crm_update_time BETWEEN '$start' AND '$end' AND b2b_b.b2b_swap_flag!=1;";
$res_booking = mysqli_query($conn,$sql_booking);
while($row_booking = mysqli_fetch_object($res_booking))
{
	$service_status = $row_booking->service_status;
	$check_in_stage = $row_booking->b2b_check_in_report;
	$at_garage_stage = $row_booking->b2b_vehicle_at_garage;
	$ready_stage = $row_booking->b2b_vehicle_ready;
	
	if(!in_array($row_booking->crm_update_id,array_keys($crms_arr)))
	{
		continue;
	}
	
	$crms_arr[$row_booking->crm_update_id]['goaxles'] = $crms_arr[$row_booking->crm_update_id]['goaxles'] + 1;
	if(($service_status == 'Completed' || $service_status == 'In Progress') || ($check_in_stage == 1 || $at_garage_stage == 1 || $ready_stage == 1))
	{
		$crms_arr[$row_booking->crm_update_id]['converted'] = $crms_arr[$row_booking->crm_update_id]['converted'] + 1;
	}
}
// print_r($crms_arr);
$i=0;
$crm = array_keys($crms_arr);
$unique_count_userids="SELECT DISTINCT(user_id) as id,book_id,ac.status,ac.category,ac.comments,ur.name,ur.mobile_number,ac.crm_log_id FROM admin_comments_tbl as ac,user_register as ur where ac.crm_log_id in $crm_list AND ur.reg_id=ac.user_id AND ac.log BETWEEN '$start' AND '$end' order by crm_log_id"; 
$user_id_unique = mysqli_query($conn,$unique_count_userids);

$total_count_userids="SELECT user_id as id,book_id,ac.status,ac.category,ac.comments,ur.name,ur.mobile_number,ac.crm_log_id FROM admin_comments_tbl as ac,user_register as ur where ac.crm_log_id in $crm_list AND ur.reg_id=ac.user_id AND ac.log BETWEEN '$start' AND '$end'"; 
$user_id_total = mysqli_query($conn,$total_count_userids);

$goaxles = "SELECT b.booking_id,b.crm_log_id as crm_update_id,b.shop_name,ur.name,b.crm_log_id,b.crm_update_time FROM user_booking_tb b LEFT JOIN b2b.b2b_booking_tbl b2b_b ON b.booking_id = b2b_b.gb_booking_id LEFT JOIN user_register ur ON b.user_id = ur.reg_id WHERE b.booking_status = '2' AND b.crm_update_time BETWEEN '$start' AND '$end' AND b2b_b.b2b_swap_flag!=1 AND b.crm_log_id in $crm_list";
$goaxles = "SELECT b.booking_id,b.crm_update_id,b.shop_name,ur.name,b.crm_log_id,b.crm_update_time FROM user_booking_tb b LEFT JOIN b2b.b2b_booking_tbl b2b_b ON b.booking_id = b2b_b.gb_booking_id LEFT JOIN user_register ur ON b.user_id = ur.reg_id WHERE b.booking_status = '2' AND b.crm_update_time BETWEEN '$start' AND '$end' AND b2b_b.b2b_swap_flag!=1";
$total_goaxles = mysqli_query($conn,$goaxles);

$converted = "SELECT b.user_id,b.booking_id,b.shop_name,b.crm_log_id as crm_update_id,b.crm_update_time,ur.name,b2b_b.b2b_check_in_report,b2b_b.b2b_vehicle_at_garage,b2b_b.b2b_vehicle_ready FROM user_booking_tb b LEFT JOIN b2b.b2b_booking_tbl b2b_b ON b.booking_id = b2b_b.gb_booking_id LEFT JOIN user_register ur ON b.user_id = ur.reg_id WHERE b.booking_status = '2' AND b.crm_update_time BETWEEN '$start' AND '$end' AND b2b_b.b2b_swap_flag!=1 AND (service_status in ('Completed','In Progress') or b2b_b.b2b_check_in_report=1 or b2b_b.b2b_vehicle_at_garage=1 or b2b_b.b2b_vehicle_ready=1) AND b.crm_log_id in $crm_list";
$converted = "SELECT b.user_id,b.booking_id,b.shop_name,b.crm_update_id,b.crm_update_time,ur.name,b2b_b.b2b_check_in_report,b2b_b.b2b_vehicle_at_garage,b2b_b.b2b_vehicle_ready FROM user_booking_tb b LEFT JOIN b2b.b2b_booking_tbl b2b_b ON b.booking_id = b2b_b.gb_booking_id LEFT JOIN user_register ur ON b.user_id = ur.reg_id WHERE b.booking_status = '2' AND b.crm_update_time BETWEEN '$start' AND '$end' AND b2b_b.b2b_swap_flag!=1 AND (service_status in ('Completed','In Progress') or b2b_b.b2b_check_in_report=1 or b2b_b.b2b_vehicle_at_garage=1 or b2b_b.b2b_vehicle_ready=1)";
$total_converted = mysqli_query($conn,$converted);

$out  = [];
$out2 = [];
$out3 = [];
$out4 = [];
while($userids_unique = mysqli_fetch_object($user_id_unique)){	
		$out[$userids_unique->crm_log_id][$i]['status']	= $userids_unique->status;
		$out[$userids_unique->crm_log_id][$i]['category']	= $userids_unique->category;
		$out[$userids_unique->crm_log_id][$i]['mobile_number']	= $userids_unique->mobile_number;
		$out[$userids_unique->crm_log_id][$i]['name']	= $userids_unique->name;
		$out[$userids_unique->crm_log_id][$i]['bookingid']	= $userids_unique->book_id;
		$out[$userids_unique->crm_log_id][$i]['comments']	= $userids_unique->comments;
	$i++;
 }
 
while($userids_total = mysqli_fetch_object($user_id_total)){	
		$out2[$userids_total->crm_log_id][$i]['status']	= $userids_total->status;
		$out2[$userids_total->crm_log_id][$i]['category']	= $userids_total->category;
		$out2[$userids_total->crm_log_id][$i]['mobile_number']	= $userids_total->mobile_number;
		$out2[$userids_total->crm_log_id][$i]['name']	= $userids_total->name;
		$out2[$userids_total->crm_log_id][$i]['bookingid']	= $userids_total->book_id;
		$out2[$userids_total->crm_log_id][$i]['comments']	= $userids_total->comments;
	$i++;

}

	
while($userids_goaxles = mysqli_fetch_object($total_goaxles)){	
		$out3[$userids_goaxles->crm_update_id][$i]['bookingid']	= $userids_goaxles->booking_id;
		$out3[$userids_goaxles->crm_update_id][$i]['crmupdatetime']	= $userids_goaxles->crm_update_time;
		$out3[$userids_goaxles->crm_update_id][$i]['shopname']	= $userids_goaxles->shop_name;
		$out3[$userids_goaxles->crm_update_id][$i]['name']	= $userids_goaxles->name;
	$i++;

}	

while($userids_converted = mysqli_fetch_object($total_converted)){	
		$out4[$userids_converted->crm_update_id][$i]['bookingid']	= $userids_converted->booking_id;
		$out4[$userids_converted->crm_update_id][$i]['crmupdatetime']	= $userids_converted->crm_update_time;
		$out4[$userids_converted->crm_update_id][$i]['shopname']	= $userids_converted->shop_name;
		$out4[$userids_converted->crm_update_id][$i]['name']	= $userids_converted->name;
	$i++;

}

foreach($crms_arr as $crm)
{
	// if($crm['unique']==0)
	// {
		// continue;
	// }
	
	if($crm['unique'] < $crm['goaxles'])
	{
		$crm['unique'] = $crm['unique'] + ($crm['goaxles'] - $crm['unique']);
	}
	
	$tr1 = "<tr>";
	$td1 = "<td>".$crm['name']."</td>";
	$td2 = "<td onclick=show_hide_row('".$crm['crm_log_id']."')>".$crm['unique']."</td>";
	$td3 = "<td onclick=show_hide_row_total('".$crm['crm_log_id']."')>".$crm['total']."</td>";
	$td4 = "<td onclick=show_hide_row_goaxles('".$crm['crm_log_id']."')>".$crm['goaxles']."</td>";
	$td5 = "<td onclick=show_hide_row_converted('".$crm['crm_log_id']."')>".$crm['converted']."</td>";
	if($crm['goaxles']==0){
	$goaxle_rate =0;	
	}
	else{
	$goaxle_rate = round((($crm['goaxles']/$crm['unique'])*100),2);
	}
	 if(is_nan($goaxle_rate))
	{
		$goaxle_rate = 0;
	} 
	$td6 = "<td>".$goaxle_rate." %</td>";
	if($crm['converted']==0){
	$conversion_rate=0;	
	}
	else{
	$conversion_rate =  round((($crm['converted']/$crm['goaxles'])*100),2);
	}
	if(is_nan($conversion_rate))
	{
		$conversion_rate = 0;
	} 
	
	$td7 = "<td>".$conversion_rate." %</td>";
	$tr1_l = "</tr>";


	$str = $tr1.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$tr1_l;
	$data1[] = $str;
}
	
	$data['tbl_data'] = $data1;

	$data['modal_data'] = $out;

	$data['modal_data_total'] = $out2; 

	$data['modal_data_goaxles'] = $out3;
		
	$data['modal_data_converted'] = $out4; 

	echo json_encode($data);
?>
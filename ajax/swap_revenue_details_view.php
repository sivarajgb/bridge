<?php
include("../config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();
$today=date('Y-m-d');

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$shop_id=$_POST['id'];
$vehicle = $_POST['vehicle_type'];
$city = $_POST['city'];

$_SESSION['crm_city'] = $city;



$cond= ($vehicle=='all')?"" : "AND bb.b2b_vehicle_type = '$vehicle'";

?>


<table id="example2" class="table table-striped table-bordered tablesorter table-hover">
<thead style="background-color:#B2DFDB;">

<th style="text-align:center;">Booking ID</th>
<th style="text-align:center;">Customer Name</th>
<th style="text-align:center;">Mobile Number</th>
<th style="text-align:center;">GoAxle Date</th>
<th style="text-align:center;">Conversion</th>
<th style="text-align:center;">Service Center</th>
<th style="text-align:center;">Locality</th>
<th style="text-align:center;">Business Model</th>
<th style="text-align:center;">Credits Consumed</th>
<th style="text-align:center;">Leads Consumed</th>
</thead>
<tbody id="tbody">

<?php 
	$sql_goaxle="SELECT
	    DISTINCT bb.b2b_booking_id,
	    bb.gb_booking_id AS booking_id,
	    bb.b2b_shop_id,
	    bb.b2b_log,
	    bb.b2b_customer_name,
	    bb.b2b_cust_phone,
	    m.b2b_address4,
	    m.b2b_shop_name,
		g.model AS business_model,
	    bb.b2b_credit_amt,
	    m.b2b_new_flg,
	    c.b2b_leads,
	    c.b2b_credits,
	    bb.b2b_check_in_report,
	    b.service_status
	FROM
	    b2b.b2b_booking_tbl bb
	        LEFT JOIN
	    user_booking_tb b ON bb.gb_booking_id = b.booking_id
	        LEFT JOIN
		garage_model_history g ON g.b2b_shop_id = bb.b2b_shop_id
	        AND DATE(bb.b2b_log) >= DATE(g.start_date)
	        AND g.id=(select id from go_bumpr.garage_model_history where b2b_shop_id=bb.b2b_shop_id AND DATE(bb.b2b_log) >= DATE(start_date) order by start_date desc limit 1) 
	        LEFT JOIN
		b2b.b2b_status s ON s.b2b_booking_id = bb.b2b_booking_id
		    LEFT JOIN
		b2b.b2b_mec_tbl m ON m.b2b_shop_id=bb.b2b_shop_id
			LEFT JOIN
		b2b.b2b_credits_tbl c ON c.b2b_shop_id=bb.b2b_shop_id
			
	WHERE
	    (DATE(bb.b2b_log) BETWEEN '$startdate' and '$enddate')
	        AND bb.b2b_shop_id NOT IN (1014 , 1035, 1670, 1673)
	        AND b.user_id NOT IN (21816 , 41317,
	        859,
	        3132,
	        20666,
	        56511,
	        2792,
	        128,
	        19,
	        7176,
	        19470,
	        1,
	        951,
	        103699,
	        113453,
	        108783)
	        AND b.service_type NOT IN ('GoBumpr Tyre Fest')
	        AND bb.b2b_swap_flag = 1
	        AND s.b2b_acpt_flag = 1
	        AND bb.b2b_shop_id='$shop_id' ORDER BY bb.b2b_booking_id;";
	$res_goaxle = mysqli_query($conn1,$sql_goaxle);
	// echo $sql_goaxle;
	while($row_goaxle = mysqli_fetch_object($res_goaxle))
	{

		$booking_id=$row_goaxle->booking_id;
		$customer_name=$row_goaxle->b2b_customer_name;
		$mobile_number=$row_goaxle->b2b_cust_phone;
		$goaxle_date=$row_goaxle->b2b_log;
		$model=trim($row_goaxle->business_model);
		$conversion=$row_goaxle->b2b_rating;
		
		$shop_name=$row_goaxle->b2b_shop_name;
		$locality=$row_goaxle->b2b_address4;
		$credit_amt=$row_goaxle->b2b_credit_amt;
		$service_status=$row_goaxle->service_status;
		$check_in_report=$row_goaxle->b2b_check_in_report;
		$credits=0;
		$leads=0;
		$conversion="No";
		if($service_status=="Completed" || $service_status=="In Progress" || $check_in_report==1){
			$conversion="Yes";
		}
		switch ($model) {
			case 'Premium 1.0':
				$credits=$credit_amt/100;
				break;			
			case 'Premium 2.0':
				$leads=1;
				break;
			case 'Leads 3.0':
				$leads=1;
				break;
			default:
				$credits=$credit_amt/100;
				
		} ?>
		<tr>
		<td><?php echo $booking_id ;?></td>
		<td><?php echo $customer_name;?></td>
		<td><?php echo $mobile_number;?></td>
		<td style="text-align:center;"><?php echo $goaxle_date;?></td>
		<td style="text-align:center;"><?php echo $conversion;?></td>
		<td style="text-align:center;"><?php echo $shop_name;?></td>
		<td style="text-align:center;"><?php echo $locality;?></td>
		<td style="text-align:center;"><?php echo $model;?></td>
		<td style="text-align:center;"><?php echo $credits;?></td>
		<td style="text-align:center;"><?php echo $leads;?></td>
		</tr>
	<?php } ?>
</tbody>
</table>


  <div class="box1" id="total_credit">
<?php
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate = date('Y-m-d',strtotime($_GET['enddate']));
$service = $_GET['service'];
$reason = $_GET['reason'];
$city = $_GET['city'];
$services = "";
$cond ='';

switch($city){
    case "all": $cond = $cond.""; break;
    default: $cond = $cond." AND u.city='$city' ";
}

if($service != 'all'){
    $sql_services =  "SELECT DISTINCT service_type FROM go_axle_service_price_tbl WHERE master_service='$service'";
    $res_services = mysqli_query($conn,$sql_services);
    $services = "";
    while($row_services = mysqli_fetch_object($res_services)){
        $service_type = $row_services->service_type;
        if($services == ""){
            $services = "'$service_type'";
        }
        else{
            $services = $services.",'$service_type'";
        }
    }
    $cond = $cond."AND em.service_type IN($services)";
}

switch($reason){
  case "all": $cond = $cond.""; break;
    case "Estimate not given": $cond = $cond." AND exception_reason ='Estimate not given'"; break;
    case "Ready stage not updated": $cond = $cond." AND exception_reason ='Ready stage not updated' "; break;
    case "Check-in report not updated": $cond = $cond." AND exception_reason ='Check-in report not updated' "; break;
}

//total credits deducted
$sql_credits_deducted = "SELECT sum(em.credits_deducted) as sum,count(em.credits_deducted) as count,sum(case when am.premium='1' then 1 else 0 end) as vehicles from exception_mechanism_track as em LEFT JOIN user_booking_tb as u on em.booking_id=u.booking_id LEFT JOIN admin_mechanic_table as am on am.mec_id=em.mec_id where DATE(exception_log) BETWEEN '$startdate' and '$enddate' AND em.flag=0 {$cond}";
$res_credits_deducted = mysqli_query($conn,$sql_credits_deducted);
$row_credits_deducted = mysqli_fetch_object($res_credits_deducted);
$sum_credits_deducted=$row_credits_deducted->sum;
$count_credits_deducted=$row_credits_deducted->count;
$vehicles_deducted=$row_credits_deducted->vehicles;
$credits_deducted = $sum_credits_deducted/100;

?>
<div class="row">
    <div id="credits_deducted" class="col-xs-4 col-md-4"  style="padding-top: 3px;">
      <div id="box" style="height: 80px; border-right: 1px solid gray;">
          <h4 style="background-color: rgba(37, 33, 57, 0.84);width: 180px;padding:6px;color: white;text-align: center;border-radius: 0px 30px 30px 0px;">Credits deducted</h4>
          <p id="tbill" style="font-size: 30px;text-align: center;"><?php echo $credits_deducted; ?></p>
       </div>
    </div>

    <div id="vehicles_deducted" class="col-xs-4 col-md-4"  style="padding-top: 3px;">
      <div id="box" style="height: 80px; border-right: 1px solid gray;">
          <h4 style="background-color: rgba(37, 33, 57, 0.84);width: 180px;padding:6px;color: white;text-align: center;border-radius: 30px 30px 30px 30px;margin-left: 10px;">Vehicles Added</h4>
          <p id="tbill" style="font-size: 30px;padding-left: 90px;"><?php echo $vehicles_deducted; ?></p>
       </div>
    </div>
    
    <div id="exception_count" class="col-xs-4 col-md-4">
       <h4 style="background-color: rgba(37, 33, 57, 0.84);width: 180px;padding:6px;color: white;float: right;text-align: center;border-radius: 30px 0px 0px 30px;">Exceptions</h4>
       <p id="tbill" style="font-size: 30px;text-align: center;padding-top:55px;padding-left: 40px;"><?php echo $count_credits_deducted; ?></p>
    </div>
</div>
</div>
<div class="box1" id="reason_credits" style="margin-top: 20px;">

<?php

$services = "";

$cond ='';

switch($city){
    case "all": $cond = $cond.""; break;
    default: $cond = $cond." AND u.city='$city' ";
}

if($service != 'all'){
    $sql_services =  "SELECT DISTINCT service_type FROM go_axle_service_price_tbl WHERE master_service='$service'";
    $res_services = mysqli_query($conn,$sql_services);
    $services = "";
    while($row_services = mysqli_fetch_object($res_services)){
        $service_type = $row_services->service_type;
        if($services == ""){
            $services = "'$service_type'";
        }
        else{
            $services = $services.",'$service_type'";
        }
    }
    $cond = $cond."AND em.service_type IN($services)";
}

switch($reason){
    case "Estimate not given": $cond = $cond." AND exception_reason ='Estimate not given' "; break;
    case "Ready stage not updated": $cond = $cond." AND exception_reason ='Ready stage not updated' "; break;
    case "Check-in report not updated": $cond = $cond." AND exception_reason ='Check-in report not updated' "; break;
}

//reason_credits
$sql_reason_count = "SELECT count(*) as count, em.exception_reason from exception_mechanism_track as em LEFT JOIN user_booking_tb as u on em.booking_id=u.booking_id where DATE(exception_log) BETWEEN '$startdate' and '$enddate' AND em.flag=0 {$cond} group by exception_reason";
$res_reason_count = mysqli_query($conn,$sql_reason_count);
$num = mysqli_num_rows($res_reason_count);
 if($num>0)
  {
while($row_reason_count = mysqli_fetch_object($res_reason_count))
{
?>  

  <div class="col-xs-4 col-md-4" align="center">
  <div style="height: 60px;padding-top: 10px;">
      <span style="font-size: 16px;font-weight: bold; color: rgba(37, 33, 57, 0.84);width:100%;vertical-align: center;"><?php echo $row_reason_count->exception_reason; ?></span>
    </div>
      <span style="text-align: center;font-size: 25px;"> 
      <?php echo $row_reason_count->count; ?></span>

  </div>
  
 <?php
}
}
else{
  ?>  
      <p style="font-size: 18px;text-align:center;padding-top: 40px;"><?php echo ("No results found...!"); ?> </p>
 <?php
}
?>

<!-- $sql_reason_credits = "SELECT sum(credits_deducted) as sum,exception_reason FROM gobumpr_test.exception_mechanism_track   where DATE(exception_log) BETWEEN '$startdate' and '$enddate' {$cond} group by exception_reason order by exception_log desc ";
$res_reason_credits = mysqli_query($conn,$sql_reason_credits);
 if($res_reason_credits->num_rows!=0)
  {
while($row_reason_credits = $res_reason_credits->fetch_assoc())
{
?>  

  <div class="col-xs-4 col-md-4" align="center">
  <div style="height: 60px;padding-top: 10px;">
      <span style="font-size: 16px;font-weight: bold; color: rgba(37, 33, 57, 0.84);width:100%;vertical-align: center;"><?php //echo $row_reason_credits['exception_reason']; ?></span>
    </div>
      <span style="text-align: center;font-size: 25px;"> 
      <?php //echo $row_reason_credits['sum']/100; ?></span>

  </div>
  
 <?php
//}
//}
//else{
  ?>  
      <p style="font-size: 18px;text-align:center;padding-top: 40px;"><?php //echo ("No results found...!"); ?> </p>
 <?php
//}
?> -->
</div>
<div id="box" style="margin-left: 15px;margin-top: 20px;">
  <div class="row">
 <div class="floating-box col-xs-6 col-md-6" id="exception_services"  style="background-color:rgba(210, 209, 218, 0.14);float: left;margin-left: 50px;">
  <h3 class="box_title" style="color: rgba(37, 33, 57, 0.84);font-weight: bold;font-size: 18px;">Frequent exception services</h3>
  <?php


$cond ='';

switch($city){
    case "all": $cond = $cond.""; break;
    default: $cond = $cond." AND u.city='$city' ";
}

if($service != 'all'){
    $sql_services =  "SELECT DISTINCT service_type FROM go_axle_service_price_tbl WHERE master_service='$service'";
    $res_services = mysqli_query($conn,$sql_services);
    $services = "";
    while($row_services = mysqli_fetch_object($res_services)){
        $service_type = $row_services->service_type;
        if($services == ""){
            $services = "'$service_type'";
        }
        else{
            $services = $services.",'$service_type'";
        }
    }
    $cond = $cond."AND em.service_type IN($services)";
}

switch($reason){
    case "Estimate not given": $cond = $cond." AND exception_reason ='Estimate not given' "; break;
    case "Ready stage not updated": $cond = $cond." AND exception_reason ='Ready stage not updated' "; break;
    case "Check-in report not updated": $cond = $cond." AND exception_reason ='Check-in report not updated' "; break;
}

//reason_credits
$sql_exception_services = "SELECT count(*) as count, em.service_type from exception_mechanism_track as em LEFT JOIN user_booking_tb as u on em.booking_id=u.booking_id where DATE(exception_log) BETWEEN '$startdate' and '$enddate' AND em.flag=0 {$cond} group by em.service_type order by count(*) desc LIMIT 5";
$res_exception_services = mysqli_query($conn,$sql_exception_services);
$num = mysqli_num_rows($res_exception_services);
 if($num>0)
 {
while($row_exception_services = mysqli_fetch_object($res_exception_services))
{
?>  
<div class="row">
  <?php
      $str_service_type = ucwords(strtolower($row_exception_services->service_type));
      $count= $count_credits_deducted; 
      $service_count=$row_exception_services->count;
      $pro_width=$service_count/$count_credits_deducted;
      ?>
      <div style="font-size: 14px;color:black;vertical-align: middle;margin-left: 10px;"><?php echo $str_service_type; ?>
      <div class="progress"  style="width:30%;float: right;margin-right: 10px;">
  <div class="progress-bar" role="progressbar" style="width:  <?php echo $pro_width*100; ?>%;background-color:rgba(15, 21, 61, 0.49);font-weight: bold;font-size: 14px; " aria-valuenow="" aria-valuemin="0" aria-valuemax="50"> <?php echo $service_count; ?>
</div> </div> 
</div>
  </div>
      <!-- <p style="font-size: 17px;margin-left: 25px;color:black;vertical-align: middle;"><?php //echo $row_exception_services['service_type']; ?> <span style="float:right;margin-right: 25px;text-align: justify;"> 
      <?php //echo $row_exception_services['count']; ?></span></p> -->
 <?php
}
}
else{
  ?>  
      <p style="font-size: 18px;text-align: center;padding-top: 40px;"><?php echo ("No results found...!"); ?> </p>
 <?php
}
?>
</div> 

  <div class="floating-box col-xs-6 col-md-6" id="exception_garages" style="background-color:rgba(210, 209, 218, 0.14);float: right;">
    <h3 class="box_title" style="color: rgba(37, 33, 57, 0.84);font-weight: bold;font-size: 18px;">Frequent exception garages</h3>
     <?php

$services = "";

$cond ='';

switch($city){
    case "all": $cond = $cond.""; break;
    default: $cond = $cond." AND u.city='$city' ";
}

if($service != 'all'){
	$sql_services =  "SELECT DISTINCT service_type FROM go_axle_service_price_tbl WHERE master_service='$service'";
    $res_services = mysqli_query($conn,$sql_services);
    $services = "";
    while($row_services = mysqli_fetch_object($res_services)){
        $service_type = $row_services->service_type;
        if($services == ""){
            $services = "'$service_type'";
        }
        else{
            $services = $services.",'$service_type'";
        }
    }
    $cond = $cond."AND em.service_type IN($services)";
}

switch($reason){
    case "Estimate not given": $cond = $cond." AND exception_reason ='Estimate not given' "; break;
    case "Ready stage not updated": $cond = $cond." AND exception_reason ='Ready stage not updated' "; break;
    case "Check-in report not updated": $cond = $cond." AND exception_reason ='Check-in report not updated' "; break;
}

//reason_credits
$sql_exception_garages = "SELECT count(*) as count,em.shop_name from exception_mechanism_track as em LEFT JOIN user_booking_tb as u on em.booking_id=u.booking_id where DATE(exception_log) BETWEEN '$startdate' and '$enddate' AND em.flag=0 {$cond} group by em.shop_name order by count(*) desc LIMIT 5";
$res_exception_garages = mysqli_query($conn,$sql_exception_garages);
 $num = mysqli_num_rows($res_reason_count);
 if($num>0)
  {
while($row_exception_garages = mysqli_fetch_object($res_exception_garages))
{
?>  
<div class="row">
  <?php
      $str_shop_name = ucwords(strtolower($row_exception_garages->shop_name));
      $count= $count_credits_deducted; 
      $service_count=$row_exception_garages->count;
      $pro_width=$service_count/$count_credits_deducted;
      ?>
      <div style="font-size: 14px;color:black;vertical-align: middle;margin-left: 10px;"><?php echo $str_shop_name;?> 
      <div class="progress"  style="width:30%;float: right;margin-right: 10px;">
  <div class="progress-bar" role="progressbar" style="width: <?php echo $pro_width*100; ?>%;background-color:rgba(15, 21, 61, 0.49);font-weight: bold;font-size: 14px;" aria-valuenow="" aria-valuemin="0" aria-valuemax="50"> <?php echo $service_count; ?>
</div> </div> 
</div>
  </div>
 <?php
}
}
else{
  ?>  
      <p style="font-size: 18px;text-align: center;padding-top: 40px;"><?php echo ("No results found...!"); ?> </p>
 <?php
}
?>
</div>
</div>
</div>
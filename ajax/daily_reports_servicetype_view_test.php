<?php
include("../config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();
$today=date('Y-m-d');

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enc_date = base64_encode($startdate);
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$vehicle = $_POST['vehicle_type'];
$master = $_POST['master'];
$enc_veh = base64_encode($vehicle);

$total_veh = 0;
$total_credits =0;

?>
<div align="center" id = "table" style="width:45%; margin-left:20px;margin-right:20px;float:left;">
<table id="example1" class="table table-striped table-bordered tablesorter table-hover results" style="max-width:95%;">
<thead style="background-color: #D3D3D3;">
<?php if($master == "all"){ ?>
    <th style="text-align:center;">ServiceType</th>
<?php } 
else{ ?>
<th style="text-align:center;">MasterServiceType</th>
<?php } ?>
<th style="text-align:center;">AcceptedGoaxles <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Credits <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
</thead>
<tbody id="tbody">
<?php

switch($master){
    case 'all':   $sql_st = "SELECT DISTINCT b2b_service_type FROM b2b_booking_tbl WHERE b2b_vehicle_type='$vehicle' AND DATE(b2b_log) BETWEEN '$startdate' and '$enddate' ORDER BY b2b_service_type ASC";
    $res_st = mysqli_query($conn2,$sql_st);
    while($row_st = mysqli_fetch_object($res_st)){
        $service_type = $row_st->b2b_service_type;
    
        $sql_booking = "SELECT COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_flag!='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_service_type='$service_type' AND b.b2b_vehicle_type='$vehicle' AND b.b2b_shop_id NOT IN (1014,1035)";
        $res_booking = mysqli_query($conn2,$sql_booking);
        $row_booking = mysqli_fetch_array($res_booking);
        $no_of_vehicles = $row_booking['bookings'];
        $no_of_credits = $row_booking['amount']/100;
    
        $total_veh = $total_veh+$no_of_vehicles;
        $total_credits = $total_credits+$no_of_credits;
    
        if($no_of_vehicles == '0' && $no_of_credits == '0'){
            continue;
        }
        ?>
        <tr>
            <td><?php echo $service_type ; ?></td>
            <td style="text-align:center;"><?php echo $no_of_vehicles ; ?></td>
            <td style="text-align:center;"><?php echo $no_of_credits ; ?></td>
        </tr>
        <?php
        }
        break;
    case 'master': 
    
    $sql_st = "SELECT DISTINCT g.master_service FROM go_bumpr.go_axle_service_price_tbl as g INNER JOIN b2b.b2b_booking_tbl as b ON (g.service_type=b.b2b_service_type AND g.type=b.b2b_vehicle_type) WHERE b.b2b_vehicle_type='$vehicle' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ORDER BY g.master_service ASC";
    $res_st = mysqli_query($conn1,$sql_st);   
    
    while($row_st = mysqli_fetch_array($res_st)){
            
        $master_service = $row_st['master_service'];

        $sql_services = "SELECT service_type FROM go_axle_service_price_tbl WHERE master_service='$master_service' AND type='$vehicle'";
        $res_services = mysqli_query($conn1,$sql_services);
        $services = '';
        while($row_services=mysqli_fetch_array($res_services)){
            if($services == ""){
                $services = "'".$row_services['service_type']."'";
            }
            else{
                $services = $services.",'".$row_services['service_type']."'";
            }
        }
        //echo $services;

        //$service_type = $row_st->b2b_service_type;
        
        $sql_booking = "SELECT COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_flag!='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_service_type IN($services) AND b.b2b_vehicle_type='$vehicle' AND b.b2b_shop_id NOT IN (1014,1035)";
        $res_booking = mysqli_query($conn2,$sql_booking);
        $row_booking = mysqli_fetch_array($res_booking);
        $no_of_vehicles = $row_booking['bookings'];
        $no_of_credits = $row_booking['amount']/100;
        
        $total_veh = $total_veh+$no_of_vehicles;
        $total_credits = $total_credits+$no_of_credits;
        
        if($no_of_vehicles == '0' && $no_of_credits == '0'){
            continue;
        }  
        ?>
        <tr>
            <td><?php echo $master_service ; ?></td>
            <td style="text-align:center;"><?php if($no_of_vehicles == ''){ echo '0'; } else { echo $no_of_vehicles ;} ?></td>
            <td style="text-align:center;"><?php echo $no_of_credits ; ?></td>
        </tr>
        <?php
        }
    break;
}
?>
</tbody>
<tbody class="avoid-sort">
<tr>
<td style="text-align:center;">Total</td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_veh; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_credits; ?></td>
</tr>
</tbody>
</table>
</div>
<div style="width:45%; margin-left:20px;margin-right:20px;float:left;">
<table id="example2" class="table table-striped table-bordered tablesorter table-hover results" style="max-width:95%;">
<thead style="background-color: #D3D3D3;">
<th style="text-align:center;">Alloted To</th>
<th style="text-align:center;">AcceptedGoaxles <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Credits <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
</thead>
<tbody id="tbody">
<?php
$sql_pr = "SELECT DISTINCT crm_log_id,name FROM crm_admin ORDER BY name ASC";
$res_pr = mysqli_query($conn1,$sql_pr); 

while($row_pr = mysqli_fetch_object($res_pr)){
    $crm_person_id = $row_pr->crm_log_id;
    $crm_person_name = $row_pr->name;
    
    $sql_booking_pr = "SELECT COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_shop_id NOT IN (1014,1035) AND g.crm_update_id='$crm_person_id' AND g.vehicle_type='$vehicle'";
    $res_booking_pr = mysqli_query($conn1,$sql_booking_pr);
    $row_booking_pr = mysqli_fetch_array($res_booking_pr);
    $no_of_vehicles_pr = $row_booking_pr['bookings'];
    $no_of_credits_pr = $row_booking_pr['amount']/100;

    $total_veh_pr = $total_veh_pr+$no_of_vehicles_pr;
    $total_credits_pr = $total_credits_pr+$no_of_credits_pr;

    if($no_of_vehicles_pr == '0' && $no_of_credits_pr == '0'){
        continue;
    }

    $list_person[] = array("id" => $crm_person_id,"vehicles" =>$no_of_vehicles_pr ,"credits" =>$no_of_credits_pr);  


?>
<tr>
    <td id="<?php echo $crm_person_id; ?>"><?php echo $crm_person_name ; ?></td>
    <td style="text-align:center;"><?php echo $no_of_vehicles_pr ; ?></td>
    <td style="text-align:center;"><?php echo $no_of_credits_pr ; ?></td>
</tr>
<?php
}
foreach ($list_person as $key => $row) {
    $id[$key] = $row['id'];
    $value[$key] = $row['vehicles'];
    $credits[$key] = $row['credits'];
}

array_multisort($value, SORT_DESC, $credits , SORT_DESC , $list_person);
$usedcredits_sorted = array_values($list_person);
//print_r($usedcredits_sorted);
?>
<script>
$(document).ready(function() {
    //first
    var id = '<?php echo $usedcredits_sorted[0]['id']; ?>';
    $("#<?php echo $usedcredits_sorted[0]['id']; ?>").append('&nbsp;<i class="fa fa-trophy" aria-hidden="true" style="font-size:19px;color:#D4AF37;"></i>');

    //second
    var id = '<?php echo $usedcredits_sorted[1]['id']; ?>';
    $("#<?php echo $usedcredits_sorted[1]['id']; ?>").append('&nbsp;<i class="fa fa-trophy" aria-hidden="true" style="font-size:19px;color:#A9A9A9;"></i>');

    //third
    var id = '<?php echo $usedcredits_sorted[2]['id']; ?>';
    $("#<?php echo $usedcredits_sorted[2]['id']; ?>").append('&nbsp;<i class="fa fa-trophy" aria-hidden="true" style="font-size:19px;color:#b87333;"></i>');

});
</script>
</tbody>
<tbody class="avoid-sort">
<tr>
<td style="text-align:center;">Total</td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_veh_pr; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_credits_pr; ?></td>
</tr>
</tbody>
</table>
</div>
<div style="width:85%; margin-left:20px;margin-left:60px;">
<table id="example3" class="table table-striped table-bordered tablesorter table-hover results" style="max-width:95%;">
<thead style="background-color: #D3D3D3;">
<th style="text-align:center;">Service Center</th>
<th style="text-align:center;">Locality</th>
<th style="text-align:center;">VehiclesSent <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Credits <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Accepted <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Rejected <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Idle <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
</thead>
<tbody id="tbody">
<?php

$sql_garage = "SELECT DISTINCT m.b2b_shop_id,m.b2b_shop_name,m.b2b_address4 FROM b2b_mec_tbl as m INNER JOIN b2b_credits_tbl as c ON m.b2b_shop_id = c.b2b_shop_id ";
$res_garage = mysqli_query($conn2,$sql_garage);

while($row_garage = mysqli_fetch_object($res_garage)){
    $shop_id = $row_garage->b2b_shop_id;
    $shop_name = $row_garage->b2b_shop_name;
    $locality = $row_garage->b2b_address4;
    
    $sql_booking_garage = "SELECT b.b2b_booking_id,b.b2b_credit_amt,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id WHERE b.b2b_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_shop_id NOT IN (1014,1035) AND b.b2b_shop_id='$shop_id' AND g.vehicle_type='$vehicle'";
    $res_booking_garage = mysqli_query($conn1,$sql_booking_garage);
    
    $no_of_vehicles_garage = 0;
    $no_of_credits_garage = 0;
    $accepted = 0;
    $rejected = 0;
    $idle = 0;


    while($row_booking_garage = mysqli_fetch_array($res_booking_garage)){
        
        $acpt = $row_booking_garage['b2b_acpt_flag'];
        $deny = $row_booking_garage['b2b_deny_flag'];
        $credit = $row_booking_garage['b2b_credit_amt'];
        $credit = $credit/100;
    
        $no_of_vehicles_garage = $no_of_vehicles_garage+1;
        

        if($acpt == '1'){
            $accepted=$accepted+1;
            $no_of_credits_garage = $no_of_credits_garage+$credit;
        }
        if($deny == '1'){
            $rejected=$rejected+1;
        }
        if($acpt == '0' && $deny == '0'){
            $idle=$idle+1;
        }

    }


    $total_veh_garage = $total_veh_garage+$no_of_vehicles_garage;
    $total_credits_garage = $total_credits_garage+$no_of_credits_garage;
    $total_accepted = $total_accepted+$accepted;
    $total_rejected=$total_rejected+$rejected;
    $total_idle = $total_idle+$idle;

    if($no_of_vehicles_garage == '0' && $no_of_credits_garage == '0'){
        continue;
    }

?>
<tr>
    <td><?php echo $shop_name ; ?></td>
    <td><?php echo $locality ; ?></td>
    <td style="text-align:center;"><?php echo $no_of_vehicles_garage ; ?></td>
    <td style="text-align:center;"><?php echo $no_of_credits_garage ; ?></td>
    <td style="text-align:center;"><?php echo $accepted ; ?></td>
    <td style="text-align:center;"><?php echo $rejected ; ?></td>
    <td style="text-align:center;"><?php echo $idle ; ?></td>
</tr>
<?php
}
?>
</tbody>
<tbody class="avoid-sort">
<tr>
<td colspan="2" style="text-align:center;">Total</td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_veh_garage; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_credits_garage; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_accepted; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_rejected; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_idle; ?></td>
</tr>
</tbody>
</table>
</div>

<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example1").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[2,2],[0,0]]} );
    }
);
</script>
<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example2").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[2,2],[0,0]]} );
    }
);
</script>
<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example3").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[3,3],[0,0]]} );
    }
);
</script>

<?php
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();

// error_reporting(E_ALL); ini_set('display_errors', 1); 

$tab = $_GET['tab'];

//0-all && 1-particular
class goaxles {
    function cr0_vg0_sup0_vr0_vd0(){
        return "Accepted";
    }
    function cr0_vg0_sup0_vr0_vd1(){
        return "Delivered";
    }
    function cr0_vg0_sup0_vr1_vd0(){
        return "Ready";
    }
    function cr0_vg0_sup0_vr1_vd1(){
        return "Delivered";
    }
    function cr0_vg0_sup1_vr0_vd0(){
        return "In Progress";
    }
    function cr0_vg0_sup1_vr0_vd1(){
        return "Delivered";
    }
    function cr0_vg0_sup1_vr1_vd0(){
        return "Ready";
    }
    function cr0_vg0_sup1_vr1_vd1(){
        return "Delivered";
    }
    function cr0_vg1_sup0_vr0_vd0(){
        return "In Garage";
    }
    function cr0_vg1_sup0_vr0_vd1(){
        return "Delivered";
    }
    function cr0_vg1_sup0_vr1_vd0(){
        return "Ready";
    }
    function cr0_vg1_sup0_vr1_vd1(){
        return "Delivered";
    }
    function cr0_vg1_sup1_vr0_vd0(){
        return "In Progress";
    }
    function cr0_vg1_sup1_vr0_vd1(){
        return "Delivered";
    }
    function cr0_vg1_sup1_vr1_vd0(){
        return "Ready";
    }
    function cr0_vg1_sup1_vr1_vd1(){
        return "Delivered";
    }
    function cr1_vg0_sup0_vr0_vd0(){
        return "Checked In";
    }
    function cr1_vg0_sup0_vr0_vd1(){
        return "Delivered";
    }
    function cr1_vg0_sup0_vr1_vd0(){
        return "Ready";
    }
    function cr1_vg0_sup0_vr1_vd1(){
        return "Delivered";
    }
    function cr1_vg0_sup1_vr0_vd0(){
        return "In Progress";
    }
    function cr1_vg0_sup1_vr0_vd1(){
        return "Delivered";
    }
    function cr1_vg0_sup1_vr1_vd0(){
        return "Ready";
    }
    function cr1_vg0_sup1_vr1_vd1(){
        return "Delivered";
    }
    function cr1_vg1_sup0_vr0_vd0(){
        return "In Garage";
    }
    function cr1_vg1_sup0_vr0_vd1(){
        return "Delivered";
    }
    function cr1_vg1_sup0_vr1_vd0(){
        return "Ready";
    }
    function cr1_vg1_sup0_vr1_vd1(){
        return "Delivered";
    }
    function cr1_vg1_sup1_vr0_vd0(){
        return "In Progress";
    }
    function cr1_vg1_sup1_vr0_vd1(){
        return "Delivered";
    }
    function cr1_vg1_sup1_vr1_vd0(){
        return "Ready";
    }
    function cr1_vg1_sup1_vr1_vd1(){
        return "Delivered";
    }
}
$goaxles_obj = new goaxles(); 


switch($tab){
    case 'person': 
       // print_r($_POST); 
        $vehicle = $_POST['vehicle'];
        $crmlogid = $_POST['crmlogid'];
        $startdate = $_POST['startdate'];
        $enddate = $_POST['enddate'];
        $status = $_POST['status'];
        $city = $_POST['city'];
        $source_type =$_POST['source_type'];
        // $cluster = $city != 'Chennai' ? 'all' : $_POST['cluster'];
        $cluster='all';
         $cond2 ='';
         if($source_type=='all'){
        $conds.="";
    }
    else if($source_type=='coreops'){
        $conds.=" and b.source!='Re-Engagement Bookings'";
    }
    else if($source_type=='reng'){
        $conds.=" and b.source='Re-Engagement Bookings'";
    }

        switch($vehicle){
            case '2w': $lc = "AND l.bike_cluster='$cluster'"; break;
            case '4w' :  $lc = "AND l.car_cluster='$cluster'" ; break;
            default :  $lc = "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'" ;
        }



        switch($status){
            case 'Goaxle': $cond = " AND f.flag='0'"; $redir = base64_encode("l") ;break;
             case 'InProgress': $cond = " AND (f.feedback_status in('3') or  f.service_status ='In Progress' ) and f.flag='0'"; $redir = base64_encode("b") ; break;
            case 'Completed': $cond = " AND (f.feedback_status in('1') or  f.service_status ='Completed' ) and f.flag='0'"; $redir = 
                base64_encode("a") ; break;
            case 'FollowUp': $cond = " AND (f.feedback_status IN('2','11','12','21','22','31','41','51') and f.service_status !='In Progress') and f.flag='0'"; $redir = base64_encode("f") ;break;
            case 'Others': $cond = " AND f.flag = '0' AND f.feedback_status in('-2','-3','4') and f.flag='0'"; $redir = base64_encode("o") ; break;
            case 'Cancel': $cond = " AND f.feedback_status='5' AND f.flag='0'"; $redir = base64_encode("c") ; break;
            case 'Idle': $cond = " AND f.flag ='0' and f.feedback_status='0'"; $redir = base64_encode("l") ; break;
            case 'Reservice': $cond = " AND f.reservice_flag = '1' AND f.feedback_status = '1'"; $redir = base64_encode("c") ; break;
            default : $cond = " AND f.flag!='1'"; $redir = base64_encode("l") ; break; 
        }

        $cond2 = $crmlogid == 'all' ? "" :" AND f.crm_log_id='$crmlogid' ";

        $cond2 = $cond2.($city == 'all' ? "" : " AND b.city='$city'");
        $cond2 = $cond2.($vehicle == 'all' ? "" : " AND b.vehicle_type='$vehicle'");


        //$cond2 = $cond2.($cluster == 'all' ? "" : "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'");


        $sql_person ="SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM user_booking_tb as b LEFT JOIN feedback_track as f ON b.booking_id=f.booking_id WHERE f.shop_id NOT IN (1014 , 1035, 1670) AND b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317)  AND f.log BETWEEN '$startdate' AND '$enddate' {$cond}{$cond2}{$conds} ORDER BY b.log DESC";
        //echo $sql_person;
        $res_person = mysqli_query($conn,$sql_person);

        if(mysqli_num_rows($res_person) >= 1 ){
            ?>
            <table class="table table-striped table-bordered tablesorter table-hover">
            <thead style="background-color: #D3D3D3;">
            <!--<th>No</th>-->
            <th>BookingId</th>
            <th>CustomerName</th>
            <th>ServiceType</th>
            <th>ServiceCenter</th>
            <th>PickUp</th>
            <?php if($status == 'booking'){ ?> <th>GoAxleStatus</th> <?php } ?>
            </thead>
            <tbody>
            <?php
            while($row_person = mysqli_fetch_object($res_person)){
                $booking_id = $row_person->booking_id;
                $user_id = $row_person->user_id;
                $veh_id = $row_person->user_veh_id;
                $res_user = mysqli_query($conn,"SELECT name FROM user_register WHERE reg_id='$user_id'");
                $row_user = mysqli_fetch_object($res_user);
                $user_name = $row_user->name;
                
                if($status == 'Goaxle'){
                    $st = $row_person->booking_status;
                    switch($st){
                         case '1': $redir = base64_encode("l") ; break;
                        case '2': $redir = base64_encode("b") ; break;
                        case '3': $redir = base64_encode("a") ; break;
                        case '4': 
                        case '5':
                        case '6':
                        case '7': $redir = base64_encode("f") ; break;
                        case '0': $redir = base64_encode("o") ; break;
                        default: $redir = base64_encode("l") ;
                    }
                }
                
                
                if($status == 'booking'){
                 //   error_reporting(E_ALL); ini_set('display_errors', 1);

                    $sql_b2b = "SELECT DISTINCT b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_service_under_progress,b.b2b_vehicle_ready,b.b2b_vehicle_delivered,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.gb_booking_id='$booking_id' AND b.b2b_swap_flag!='1'";
                    $res_b2b = mysqli_query($conn2,$sql_b2b) or die(mysqli_error($res_b2b));
                    $row_b2b = mysqli_fetch_object($res_b2b);
                    $acpt = $row_b2b->b2b_acpt_flag;
                    $deny = $row_b2b->b2b_deny_flag;

                    if(mysqli_num_rows($res_b2b) >=1 ){
                        if($deny == '1' && $acpt == '0'){
                            $goaxle = "Rejected";
                        }
                        else if($deny == '0' && $acpt == '0'){
                            $goaxle = "No Action";
                        }
                        else{
                            $check_in_report = $row_b2b->b2b_check_in_report;
                            $vehicle_at_garage = $row_b2b->b2b_vehicle_at_garage;
                            $service_under_progress = $row_b2b->b2b_service_under_progress;
                            $vehicle_ready = $row_b2b->b2b_vehicle_ready;
                            $vehicle_delivered = $row_b2b->b2b_vehicle_delivered;
                            
                            $check_in_report_val = $check_in_report == '0' ? "0" : "1";
                            $vehicle_at_garage_val = $vehicle_at_garage == '0' ? "0" : "1";
                            $service_under_progress_val = $service_under_progress == '0' ? "0" : "1";
                            $vehicle_ready_val = $vehicle_ready=='0' ? "0" : "1";
                            $vehicle_delivered_val = $vehicle_delivered == '0' ? "0" : "1";
                    
                            $goaxle = $goaxles_obj->{"cr{$check_in_report_val}_vg{$vehicle_at_garage_val}_sup{$service_under_progress_val}_vr{$vehicle_ready_val}_vd{$vehicle_delivered_val}"}();
                        }
                    }
                    else{
                        $goaxle = "Record not found!";
                    }
                }
                ?>
                <tr>
                <td><a target="_blank" href="user_details.php?t=<?php echo $redir; ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>"><?php echo $booking_id; ?></a></td>
                <td><?php echo $user_name; ?></td>
                <td><?php echo $row_person->service_type; ?></td>
                <td><?php echo $row_person->shop_name; ?></td>
                <td><?php $p = $row_person->pick_up; echo $pc = $p== '1' ? 'Yes': 'No'; ?></td>
            <?php if($status == 'booking'){ ?> <td> <?php echo $goaxle; ?></td> <?php } ?>
                </tr>
                <?php
            }
            ?>
            </tbody>
            </table>
            <?php
        }
        else{
            echo "<h4> No Records Found!</h4>";
        }
        break;
    case 'rtt': 
       // print_r($_POST); 
        $crmlogid = $_POST['crmlogid'];
        $startdate = $_POST['startdate'];
        $enddate = $_POST['enddate'];
        $status = $_POST['status'];
        $city = $_POST['city'];
        $rating = $_POST['rating'];
        $source_type =$_POST['source_type'];
        // $cluster = $city != 'Chennai' ? 'all' : $_POST['cluster'];
        $cluster='all';
        $cond2 ='';
         if($source_type=='all'){
        $conds.="";
    }
    else if($source_type=='coreops'){
        $conds.=" and b.source!='Re-Engagement Bookings'";
    }
    else if($source_type=='reng'){
        $conds.=" and b.source='Re-Engagement Bookings'";
    }
        
        //echo $status;

        switch($status){
            case 'Goaxle': $cond = " AND f.flag='0'";$redir = base64_encode("l") ; break;
            case 'CheckIns': $cond = " AND f.flag='0' AND bb.b2b_check_in_report = 1 and bb.b2b_vehicle_at_garage != 1 AND bb.b2b_vehicle_ready != 1"; $redir = base64_encode("b") ; break;
            case 'AllStages': $cond = " AND f.flag='0' AND bb.b2b_check_in_report = 1 and bb.b2b_vehicle_at_garage = 1 AND bb.b2b_vehicle_ready = 1"; $redir = base64_encode("f") ;break;
            case 'NoRTT': $cond = " AND f.flag='0' AND bb.b2b_check_in_report = 0 and bb.b2b_vehicle_at_garage = 0 AND bb.b2b_vehicle_ready =0"; $redir = base64_encode("o") ; break;
            default : $cond = " AND f.flag='0'"; $redir = base64_encode("l") ; break; 
        }

        //$cond2 = $crmlogid == 'all' ? "" :" AND f.crm_log_id='$crmlogid' ";

        $cond2 = $cond2.($city == 'all' ? "" : " AND b.city='$city'");
        $cond2 = $cond2.($rating == 'all' ? "" : " AND b.vehicle_type='$rating'");
        // if($rating == ''){
        //     $cond_rating = $cond_rating.($rating == 'all' ? "" : " AND ISNULL(ur.rating)");
        // }else{
        //     $cond_rating = $cond_rating.($rating == 'all' ? "" : " AND b.vehicle_type='$rating'");
        // }
        
        //$cond2 = $cond2.($cluster == 'all' ? "" : "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'");
        

        $sql_rtt ="SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id 
        FROM go_bumpr.user_booking_tb b  
LEFT JOIN feedback_track f ON f.booking_id = b.booking_id 
join b2b.b2b_booking_tbl bb on f.b2b_booking_id=bb.b2b_booking_id
WHERE b.mec_id NOT IN (400001 , 200018, 200379, 400974)
AND f.shop_id NOT IN (1014 , 1035, 1670) AND b.user_id NOT IN (21816 , 41317)
AND f.log BETWEEN '$startdate' AND '$enddate' {$cond2}{$cond}{$conds} 
        ORDER BY b.log DESC";
        //echo $sql_rtt;
        $res_rtt = mysqli_query($conn,$sql_rtt);

        if(mysqli_num_rows($res_rtt) >= 1 ){
            ?>
            <table class="table table-striped table-bordered tablesorter table-hover">
            <thead style="background-color: #D3D3D3;">
            <!--<th>No</th>-->
            <th>BookingId</th>
            <th>CustomerName</th>
            <th>ServiceType</th>
            <th>ServiceCenter</th>
            <th>PickUp</th>
            <?php if($status == 'booking'){ ?> <th>GoAxleStatus</th> <?php } ?>
            </thead>
            <tbody>
            <?php
            while($row_rtt = mysqli_fetch_object($res_rtt)){
                $booking_id = $row_rtt->booking_id;
                $user_id = $row_rtt->user_id;
                $veh_id = $row_rtt->user_veh_id;
                $res_user = mysqli_query($conn,"SELECT name FROM user_register WHERE reg_id='$user_id'");
                $row_user = mysqli_fetch_object($res_user);
                $user_name = $row_user->name;
                
                if($status == 'Goaxle'){
                    $st = $row_rtt->booking_status;
                    switch($st){
                        case '1': $redir = base64_encode("l") ; break;
                        case '2': $redir = base64_encode("b") ; break;
                        case '3': 
                        case '4':
                        case '5':
                        case '6': $redir = base64_encode("f") ; break;
                        case '0': $redir = base64_encode("o") ; break;
                        default: $redir = base64_encode("l") ;
                    }
                }
                
                
                if($status == 'booking'){
                 //   error_reporting(E_ALL); ini_set('display_errors', 1);

                    $sql_b2b = "SELECT DISTINCT b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_service_under_progress,b.b2b_vehicle_ready,b.b2b_vehicle_delivered,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.gb_booking_id='$booking_id' AND b.b2b_swap_flag!='1'";
                    $res_b2b = mysqli_query($conn2,$sql_b2b) or die(mysqli_error($res_b2b));
                    $row_b2b = mysqli_fetch_object($res_b2b);
                    $acpt = $row_b2b->b2b_acpt_flag;
                    $deny = $row_b2b->b2b_deny_flag;

                    if(mysqli_num_rows($res_b2b) >=1 ){
                        if($deny == '1' && $acpt == '0'){
                            $goaxle = "Rejected";
                        }
                        else if($deny == '0' && $acpt == '0'){
                            $goaxle = "No Action";
                        }
                        else{
                            $check_in_report = $row_b2b->b2b_check_in_report;
                            $vehicle_at_garage = $row_b2b->b2b_vehicle_at_garage;
                            $service_under_progress = $row_b2b->b2b_service_under_progress;
                            $vehicle_ready = $row_b2b->b2b_vehicle_ready;
                            $vehicle_delivered = $row_b2b->b2b_vehicle_delivered;
                            
                            $check_in_report_val = $check_in_report == '0' ? "0" : "1";
                            $vehicle_at_garage_val = $vehicle_at_garage == '0' ? "0" : "1";
                            $service_under_progress_val = $service_under_progress == '0' ? "0" : "1";
                            $vehicle_ready_val = $vehicle_ready=='0' ? "0" : "1";
                            $vehicle_delivered_val = $vehicle_delivered == '0' ? "0" : "1";
                    
                            $goaxle = $goaxles_obj->{"cr{$check_in_report_val}_vg{$vehicle_at_garage_val}_sup{$service_under_progress_val}_vr{$vehicle_ready_val}_vd{$vehicle_delivered_val}"}();
                        }
                    }
                    else{
                        $goaxle = "Record not found!";
                    }
                }
                ?>
                <tr>
                <td><a target="_blank" href="user_details.php?t=<?php echo $redir; ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>"><?php echo $booking_id; ?></a></td>
                <td><?php echo $user_name; ?></td>
                <td><?php echo $row_rtt->service_type; ?></td>
                <td><?php echo $row_rtt->shop_name; ?></td>
                <td><?php $p = $row_rtt->pick_up; echo $pc = $p== '1' ? 'Yes': 'No'; ?></td>
            <?php if($status == 'booking'){ ?> <td> <?php echo $goaxle; ?></td> <?php } ?>
                </tr>
                <?php
            }
            ?>
            </tbody>
            </table>
            <?php
        }
        else{
            echo "<h4> No Records Found!</h4>";
        }
        break;

      case 'rating': 
       // print_r($_POST); 
        $crmlogid = $_POST['crmlogid'];
        $startdate = $_POST['startdate'];
        $enddate = $_POST['enddate'];
        $status = $_POST['status'];
        $city = $_POST['city'];
        $rating = $_POST['rating'];
        // $cluster = $city != 'Chennai' ? 'all' : $_POST['cluster'];
        $cluster='all';
         $source_type =$_POST['source_type'];
          
        
        
        //echo $status;

        switch($status){
            case 'Goaxle': $cond = " AND flag='0'";$redir = base64_encode("l") ; break;
            case 'CheckIns': $cond = " AND flag='0' AND bb.b2b_check_in_report = 1 and bb.b2b_vehicle_at_garage != 1 AND bb.b2b_vehicle_ready != 1"; $redir = base64_encode("b") ; break;
            case 'AllStages': $cond = " AND flag='0' AND bb.b2b_check_in_report = 1 and bb.b2b_vehicle_at_garage = 1 AND bb.b2b_vehicle_ready = 1"; $redir = base64_encode("f") ;break;
            case 'NoRTT': $cond = " AND flag='0' AND bb.b2b_check_in_report = 0 and bb.b2b_vehicle_at_garage = 0 AND bb.b2b_vehicle_ready =0"; $redir = base64_encode("o") ; break;
            default : $cond = " AND flag='0'"; $redir = base64_encode("l") ; break; 
        }

        //$cond2 = $crmlogid == 'all' ? "" :" AND f.crm_log_id='$crmlogid' ";

        $cond2 = $cond2.($city == 'all' ? "" : " AND b.city='$city'");
        //$cond_rating = $cond_rating.($rating == 'all' ? "" : " AND ur.rating='$rating'");
        if($rating == ''){
            $cond_rating = $cond_rating.($rating == 'all' ? "" : " AND ISNULL(rating)");
        }else{
            $cond_rating = $cond_rating.($rating == 'all' ? "" : " AND urt.rating='$rating'");
        }
        if($source_type=='all'){
         $cond2.="";
    }
    else if($source_type=='coreops'){
         $cond2.=" and b.source!='Re-Engagement Bookings'";
    }
    else if($source_type=='reng'){
         $cond2.=" and b.source='Re-Engagement Bookings'";
    }
        
$sql_rating ="SELECT DISTINCT urt.booking_id,urt.name,urt.service_type,urt.mec_id,admt.shop_name FROM go_bumpr.user_rating_tbl urt LEFT JOIN go_bumpr.admin_mechanic_table admt ON urt.mec_id =  admt.mec_id left join go_bumpr.user_booking_tb as b on urt.booking_id = b.booking_id WHERE urt.axle_booking_id !=0 and urt.medium='axle' AND urt.rating !=0 AND urt.booking_id !=0 AND urt.log BETWEEN '$startdate' AND '$enddate'{$cond_rating} {$cond2}";
        
                

        //echo  $sql_rating;
        $res_rating = mysqli_query($conn,$sql_rating);

        if(mysqli_num_rows($res_rating) >= 1 ){
            ?>
            <table class="table table-striped table-bordered tablesorter table-hover">
            <thead style="background-color: #D3D3D3;">
            <!--<th>No</th>-->
            <th>BookingId</th>
            <th>CustomerName</th>
            <th>ServiceType</th>
            <th>ServiceCenter</th>
            <!--<th>PickUp</th>-->
            <?php if($status == 'booking'){ ?> <th>GoAxleStatus</th> <?php } ?>
            </thead>
            <tbody>
            <?php
            while($row_rating = mysqli_fetch_object($res_rating)){
                $booking_id = $row_rating->booking_id;
                $user_id = $row_rating->user_id;
                $veh_id = $row_rating->user_veh_id;
                $res_user = mysqli_query($conn,"SELECT name FROM user_register WHERE reg_id='$user_id'");
                $row_user = mysqli_fetch_object($res_user);
                //$user_name = $row_user->name;
                $user_name = $row_rating->name;

                
                if($status == 'Goaxle'){
                    $st = $row_rating->booking_status;
                    switch($st){
                        case '1': $redir = base64_encode("l") ; break;
                        case '2': $redir = base64_encode("b") ; break;
                        case '3': 
                        case '4':
                        case '5':
                        case '6': $redir = base64_encode("f") ; break;
                        case '0': $redir = base64_encode("o") ; break;
                        default: $redir = base64_encode("l") ;
                    }
                }
                
                
                if($status == 'booking'){
                 //   error_reporting(E_ALL); ini_set('display_errors', 1);

                    $sql_b2b = "SELECT DISTINCT b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_service_under_progress,b.b2b_vehicle_ready,b.b2b_vehicle_delivered,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.gb_booking_id='$booking_id' AND b.b2b_swap_flag!='1'";
                    $res_b2b = mysqli_query($conn2,$sql_b2b) or die(mysqli_error($res_b2b));
                    $row_b2b = mysqli_fetch_object($res_b2b);
                    $acpt = $row_b2b->b2b_acpt_flag;
                    $deny = $row_b2b->b2b_deny_flag;

                    if(mysqli_num_rows($res_b2b) >=1 ){
                        if($deny == '1' && $acpt == '0'){
                            $goaxle = "Rejected";
                        }
                        else if($deny == '0' && $acpt == '0'){
                            $goaxle = "No Action";
                        }
                        else{
                            $check_in_report = $row_b2b->b2b_check_in_report;
                            $vehicle_at_garage = $row_b2b->b2b_vehicle_at_garage;
                            $service_under_progress = $row_b2b->b2b_service_under_progress;
                            $vehicle_ready = $row_b2b->b2b_vehicle_ready;
                            $vehicle_delivered = $row_b2b->b2b_vehicle_delivered;
                            
                            $check_in_report_val = $check_in_report == '0' ? "0" : "1";
                            $vehicle_at_garage_val = $vehicle_at_garage == '0' ? "0" : "1";
                            $service_under_progress_val = $service_under_progress == '0' ? "0" : "1";
                            $vehicle_ready_val = $vehicle_ready=='0' ? "0" : "1";
                            $vehicle_delivered_val = $vehicle_delivered == '0' ? "0" : "1";
                    
                            $goaxle = $goaxles_obj->{"cr{$check_in_report_val}_vg{$vehicle_at_garage_val}_sup{$service_under_progress_val}_vr{$vehicle_ready_val}_vd{$vehicle_delivered_val}"}();
                        }
                    }
                    else{
                        $goaxle = "Record not found!";
                    }
                }
                ?>
                <tr>
                <td><a target="_blank" href="user_details.php?t=<?php echo $redir; ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>"><?php echo $booking_id; ?></a></td>
                <td><?php echo $user_name; ?></td>
                <td><?php echo $row_rating->service_type; ?></td>
                <td><?php echo $row_rating->shop_name; ?></td>
                <!-- <td><?php $p = $row_rating->pick_up; //echo $pc = $p== '1' ? 'Yes': 'No'; ?></td> -->
            <?php if($status == 'booking'){ ?> <td> <?php echo $goaxle; ?></td> <?php } ?>
                </tr>
                <?php
            }
            ?>
            </tbody>
            </table>
            <?php
        }
        else{
            echo "<h4> No Records Found!</h4>";
        }
        break;

    case 'service': 
        $vehicle = $_POST['vehicle'];
        $master_service = $_POST['masterservice'];
        $startdate = $_POST['startdate'];
        $enddate = $_POST['enddate'];
        $status = $_POST['status'];
        $city = $_POST['city'];
        // $cluster = $city != 'Chennai' ? 'all' : $_POST['cluster'];
        $source_type =$_POST['source_type'];
        $cluster='all';
        $cond2 ='';
         if($source_type=='all'){
        $conds.="";
    }
    else if($source_type=='coreops'){
        $conds.=" and b.source!='Re-Engagement Bookings'";
    }
    else if($source_type=='reng'){
        $conds.=" and b.source='Re-Engagement Bookings'";
    }
        

        $cond ='';

        switch($vehicle){
            case '2w': $lc = "AND l.bike_cluster='$cluster'"; break;
            case '4w' :  $lc = "AND l.car_cluster='$cluster'" ; break;
            default :  $lc = "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'" ;
        }

        $cond = $cond.($vehicle == 'all' ? "" : "AND b.vehicle_type='$vehicle'");

        $cond = $cond.($city == 'all' ? "" : "AND b.city='$city'");

        //$cond = $cond.($cluster == 'all' ? "" : $lc );
        if($master_service != 'all'){

            $cond = $cond." AND b.service_type = '$master_service' ";
        }
       
        switch($status){
            case 'Goaxle': $cond2 = " AND f.flag='0'"; $redir = base64_encode("l") ;break;
            case 'InProgress': $cond2 = " AND (f.feedback_status in('3') or  f.service_status ='In Progress' ) and f.flag='0'"; $redir = base64_encode("b") ; break;
            case 'Completed': $cond2 = " AND (f.feedback_status in('1') or  f.service_status ='Completed' ) and f.flag='0'"; $redir = base64_encode("a") ; break;
            case 'FollowUp': $cond2 = " AND (f.feedback_status IN('2','11','12','21','22','31','41','51') and f.service_status !='In Progress') and f.flag='0'"; $redir = base64_encode("f") ;break;
            case 'Others': $cond2 = " AND f.flag = '0' AND f.feedback_status in('-2','-3','4') and f.flag='0'"; $redir = base64_encode("o") ; break;
            case 'Cancel': $cond2 = " AND f.feedback_status='5' AND f.flag='0'"; $redir = base64_encode("c") ; break;
            case 'Idle': $cond2 = " AND f.flag ='0' and f.feedback_status='0'"; $redir = base64_encode("l") ; break;
            case 'Reservice': $cond2 = " AND f.reservice_flag = '1' AND f.feedback_status = '1'"; $redir = base64_encode("c") ; break;
            default : $cond2 = " AND f.flag!='1'"; $redir = base64_encode("l") ; break;
        }

        $sql_service = "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM user_booking_tb as b LEFT JOIN feedback_track as f ON b.booking_id=f.booking_id WHERE f.shop_id NOT IN (1014 , 1035, 1670) AND b.mec_id NOT IN(400001,200018,200379,400974)  AND b.user_id NOT IN(21816,41317)  AND f.log BETWEEN '$startdate' AND '$enddate'  {$cond} {$cond2}{$conds} ORDER BY b.log DESC" ;
        
        //echo $sql_service;
        $res_service = mysqli_query($conn, $sql_service);

        if(mysqli_num_rows($res_service) >= 1){
            ?>
            <table class="table table-striped table-bordered tablesorter table-hover">
            <thead style="background-color: #D3D3D3;">
            <!--<th>No</th>-->
            <th>BookingId</th>
            <th>CustomerName</th>
            <th>ServiceType</th>
            <th>ServiceCenter</th>
            <th>PickUp</th>
            <?php if($status == 'booking') { ?> <th>GoAxleStatus</th> <?php } ?>
            </thead>
            <tbody>
            <?php
            while($row_service = mysqli_fetch_object($res_service)){
                $booking_id = $row_service->booking_id;
                $user_id = $row_service->user_id;
                $veh_id = $row_service->user_veh_id;
                $res_user = mysqli_query($conn,"SELECT name FROM user_register WHERE reg_id='$user_id'");
                $row_user = mysqli_fetch_object($res_user);
                $user_name = $row_user->name;
                
                if($status == 'Goaxle'){
                    $st = $row_service->booking_status;
                    switch($st){
                        case '1': $redir = base64_encode("l") ; break;
                        case '2': $redir = base64_encode("b") ; break;
                        case '3': $redir = base64_encode("a") ; break;
                        case '4': 
                        case '5':
                        case '6':
                        case '7': $redir = base64_encode("f") ; break;
                        case '0': $redir = base64_encode("o") ; break;
                        default: $redir = base64_encode("l") ;
                    }
                }
                if($status == 'booking'){
                    //   error_reporting(E_ALL); ini_set('display_errors', 1);
   
                       $sql_b2b = "SELECT DISTINCT b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_service_under_progress,b.b2b_vehicle_ready,b.b2b_vehicle_delivered,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.gb_booking_id='$booking_id' AND b.b2b_swap_flag!='1'";
                       $res_b2b = mysqli_query($conn2,$sql_b2b) or die(mysqli_error($res_b2b));
                       $row_b2b = mysqli_fetch_object($res_b2b);
                       $acpt = $row_b2b->b2b_acpt_flag;
                       $deny = $row_b2b->b2b_deny_flag;
   
                       if(mysqli_num_rows($res_b2b) >=1 ){
                           if($deny == '1' && $acpt == '0'){
                               $goaxle = "Rejected";
                           }
                           else if($deny == '0' && $acpt == '0'){
                               $goaxle = "No Action";
                           }
                           else{
                               $check_in_report = $row_b2b->b2b_check_in_report;
                               $vehicle_at_garage = $row_b2b->b2b_vehicle_at_garage;
                               $service_under_progress = $row_b2b->b2b_service_under_progress;
                               $vehicle_ready = $row_b2b->b2b_vehicle_ready;
                               $vehicle_delivered = $row_b2b->b2b_vehicle_delivered;
                               
                               $check_in_report_val = $check_in_report == '0' ? "0" : "1";
                               $vehicle_at_garage_val = $vehicle_at_garage == '0' ? "0" : "1";
                               $service_under_progress_val = $service_under_progress == '0' ? "0" : "1";
                               $vehicle_ready_val = $vehicle_ready=='0' ? "0" : "1";
                               $vehicle_delivered_val = $vehicle_delivered == '0' ? "0" : "1";
                       
                               $goaxle = $goaxles_obj->{"cr{$check_in_report_val}_vg{$vehicle_at_garage_val}_sup{$service_under_progress_val}_vr{$vehicle_ready_val}_vd{$vehicle_delivered_val}"}();
                           }
                       }
                       else{
                           $goaxle = "Record not found!";
                       }
                   }
                ?>
                <tr>
                <td><a target="_blank" href="user_details.php?t=<?php echo $redir; ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>"><?php echo $booking_id; ?></a></td>
                <td><?php echo $user_name; ?></td>
                <td><?php echo $row_service->service_type; ?></td>
                <td><?php echo $row_service->shop_name; ?></td>
                <td><?php $p = $row_service->pick_up; echo $pc = $p== '1' ? 'Yes': 'No'; ?></td>
                <?php if($status == 'booking') { ?> <td><?php echo $goaxle; ?></td> <?php } ?>
                </tr>
                <?php
            }
            ?>
            </tbody>
            </table>
            <?php
        }
        else{
            echo "<h4> No Records Found!</h4>";
        }  
        //print_r($_POST);
        break;
    case 'source': 
        $vehicle = $_POST['vehicle'];
        $source = $_POST['source'];
        $startdate = $_POST['startdate'];
        $enddate = $_POST['enddate'];
        $status = $_POST['status'];
        $city = $_POST['city'];
        // $cluster = $city != 'Chennai' ? 'all' : $_POST['cluster'];
       $source_type =$_POST['source_type'];
        $cluster='all';
        $cond2 ='';
         if($source_type=='all'){
        $conds.="";
    }
    else if($source_type=='coreops'){
        $conds.=" and b.source!='Re-Engagement Bookings'";
    }
    else if($source_type=='reng'){
        $conds.=" and b.source='Re-Engagement Bookings'";
    }
        $cond ='';

        switch($vehicle){
            case '2w': $lc = "AND l.bike_cluster='$cluster'"; break;
            case '4w' :  $lc = "AND l.car_cluster='$cluster'" ; break;
            default :  $lc = "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'" ;
        }

        $cond = $cond.($vehicle == 'all' ? "" : "AND b.vehicle_type='$vehicle'");
        $cond = $cond.($city == 'all' ? "" : "AND b.city='$city'");
        $cond = $cond.($source == 'all' ? "" : "AND b.source='$source'");
        //$cond = $cond.($cluster == 'all' ? "" : $lc );

        switch($status){
            case 'Goaxle': $cond2 = " AND f.flag='0'";$redir = base64_encode("l") ; break;
            case 'InProgress': $cond2= " AND (f.feedback_status in('3') or  f.service_status ='In Progress' ) and f.flag='0'"; $redir = base64_encode("b") ; break;
            case 'Completed': $cond2 = " AND (f.feedback_status in('1') or  f.service_status ='Completed' ) and f.flag='0'"; $redir = base64_encode("a") ; break;
            case 'FollowUp': $cond2 = " AND (f.feedback_status IN('2','11','12','21','22','31','41','51') and f.service_status !='In Progress') and f.flag='0'"; $redir = base64_encode("f") ;break;
            case 'Others': $cond2 = " AND f.flag = '0' AND f.feedback_status in('-2','-3','4') and f.flag='0'"; $redir = base64_encode("o") ; break;
            case 'Cancel': $cond2 = " AND f.feedback_status='5' AND f.flag='0'"; $redir = base64_encode("c") ; break;
            case 'Idle': $cond2 = " AND f.flag ='0' and f.feedback_status='0'"; $redir = base64_encode("l") ; break;
            case 'Reservice': $cond2 = " AND f.reservice_flag = '1' AND f.feedback_status = '1'"; $redir = base64_encode("c") ; break;
            default : $cond2 = " AND f.flag !='1'"; $redir = base64_encode("l") ; break; 
        }

        $sql_source = "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM user_booking_tb as b LEFT JOIN feedback_track as f ON b.booking_id=f.booking_id WHERE f.shop_id NOT IN (1014 , 1035, 1670) AND b.mec_id NOT IN(400001,200018,200379,400974)  AND b.user_id NOT IN(21816,41317)  AND f.log BETWEEN '$startdate' AND '$enddate' {$cond} {$cond2} {$conds}ORDER BY b.log DESC" ;

        //echo $sql_source;
        $res_source = mysqli_query($conn, $sql_source);

        if(mysqli_num_rows($res_source) >= 1){
            ?>
            <table class="table table-striped table-bordered tablesorter table-hover">
            <thead style="background-color: #D3D3D3;">
            <!--<th>No</th>-->
            <th>BookingId</th>
            <th>CustomerName</th>
            <th>ServiceType</th>
            <th>ServiceCenter</th>
            <th>PickUp</th>
            <?php if($status == 'booking'){ ?> <th>GoAxleStatus</th> <?php } ?>
            </thead>
            <tbody>
            <?php
            while($row_source = mysqli_fetch_object($res_source)){
                $booking_id = $row_source->booking_id;
                $user_id = $row_source->user_id;
                $veh_id = $row_source->user_veh_id;
                $res_user = mysqli_query($conn,"SELECT name FROM user_register WHERE reg_id='$user_id'");
                $row_user = mysqli_fetch_object($res_user);
                $user_name = $row_user->name;
                
                if($status == 'Goaxle'){
                    $st = $row_source->booking_status;
                    switch($st){
                        case '1': $redir = base64_encode("l") ; break;
                        case '2': $redir = base64_encode("b") ; break;
                        case '3': $redir = base64_encode("a") ; break;
                        case '4': 
                        case '5':
                        case '6':
                        case '7': $redir = base64_encode("f") ; break;
                        case '0': $redir = base64_encode("o") ; break;
                        default: $redir = base64_encode("l") ;
                    }
                }
                if($status == 'booking'){
                    //   error_reporting(E_ALL); ini_set('display_errors', 1);
   
                       $sql_b2b = "SELECT DISTINCT b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_service_under_progress,b.b2b_vehicle_ready,b.b2b_vehicle_delivered,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.gb_booking_id='$booking_id' AND b.b2b_swap_flag!='1'";
                       $res_b2b = mysqli_query($conn2,$sql_b2b) or die(mysqli_error($res_b2b));
                       $row_b2b = mysqli_fetch_object($res_b2b);
                       $acpt = $row_b2b->b2b_acpt_flag;
                       $deny = $row_b2b->b2b_deny_flag;
   
                       if(mysqli_num_rows($res_b2b) >=1 ){
                           if($deny == '1' && $acpt == '0'){
                               $goaxle = "Rejected";
                           }
                           else if($deny == '0' && $acpt == '0'){
                               $goaxle = "No Action";
                           }
                           else{
                               $check_in_report = $row_b2b->b2b_check_in_report;
                               $vehicle_at_garage = $row_b2b->b2b_vehicle_at_garage;
                               $service_under_progress = $row_b2b->b2b_service_under_progress;
                               $vehicle_ready = $row_b2b->b2b_vehicle_ready;
                               $vehicle_delivered = $row_b2b->b2b_vehicle_delivered;

                             
   
                               $check_in_report_val = $check_in_report == '0' ? "0" : "1";
                               $vehicle_at_garage_val = $vehicle_at_garage == '0' ? "0" : "1";
                               $service_under_progress_val = $service_under_progress == '0' ? "0" : "1";
                               $vehicle_ready_val = $vehicle_ready=='0' ? "0" : "1";
                               $vehicle_delivered_val = $vehicle_delivered == '0' ? "0" : "1";
                       
                               $goaxle = $goaxles_obj->{"cr{$check_in_report_val}_vg{$vehicle_at_garage_val}_sup{$service_under_progress_val}_vr{$vehicle_ready_val}_vd{$vehicle_delivered_val}"}();
                           }
                       }
                       else{
                           $goaxle = "Record not found!";
                       }
                   }
               
                ?>
                <tr>
                <td><a target="_blank" href="user_details.php?t=<?php echo $redir; ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>"><?php echo $booking_id; ?></a></td>
                <td><?php echo $user_name; ?></td>
                <td><?php echo $row_source->service_type; ?></td>
                <td><?php echo $row_source->shop_name; ?></td>
                <td><?php $p = $row_source->pick_up; echo $pc = $p== '1' ? 'Yes': 'No'; ?></td>
                <?php if($status == 'booking'){ ?> <td><?php echo $goaxle; ?></td> <?php } ?>
                </tr>
                <?php
            }
            ?>
            </tbody>
            </table>
            <?php
        }
        else{
            echo "<h4> No Records Found!</h4>";
        }  
        break;
    case 'nonconv': 
        $activity = $_POST['activity'];
        $vehicle = $_POST['vehicle'];
        $startdate = $_POST['startdate'];
        $enddate = $_POST['enddate'];
        $city = $_POST['city'];
        // $cluster = $city != 'Chennai' ? 'all' : $_POST['cluster'];
       $source_type =$_POST['source_type'];
        $cluster='all';
        $cond2 ='';
         if($source_type=='all'){
        $conds.="";
    }
    else if($source_type=='coreops'){
        $conds.=" and b.source!='Re-Engagement Bookings'";
    }
    else if($source_type=='reng'){
        $conds.=" and b.source='Re-Engagement Bookings'";
    }

        $cond ='';

        switch($vehicle){
            case '2w': $lc = "AND l.bike_cluster='$cluster'"; break;
            case '4w' :  $lc = "AND l.car_cluster='$cluster'" ; break;
            default :  $lc = "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'" ;
        }

        $cond = $cond.($vehicle == 'all' ? "" : "AND b.vehicle_type='$vehicle'");
        $cond = $cond.($city == 'all' ? "" : "AND b.city='$city'");
        $cond = $cond.($cluster == 'all' ? "" : $lc );

        $sql_nonconv = $city == "Chennai" ? "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.flag,b.booking_status,b.user_veh_id FROM user_booking_tb as b  LEFT JOIN localities as l ON b.locality=l.localities WHERE b.activity_status='$activity' AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226) AND b.service_type != 'IOCL Check-up' AND b.source !='Re-Engagement Bookings' AND  b.mec_id NOT IN(400001,200018,200379,400974) AND b.log BETWEEN '$startdate' AND '$enddate' {$cond}" : "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.flag,b.booking_status,b.user_veh_id FROM user_booking_tb as b  WHERE b.activity_status='$activity' AND b.user_id NOT IN(21816,41317) AND b.mec_id NOT IN(400001,200018,200379,400974)  and b.log BETWEEN '$startdate' AND '$enddate' {$cond} {$conds}" ;

        //echo $sql_nonconv;
        $res_nonconv = mysqli_query($conn,$sql_nonconv);

        if(mysqli_num_rows($res_nonconv) >= 1 ){
            ?>
            <table class="table table-striped table-bordered tablesorter table-hover">
            <thead style="background-color: #D3D3D3;">
            <!--<th>No</th>-->
            <th>BookingId</th>
            <th>CustomerName</th>
            <th>ServiceType</th>
            <th>ServiceCenter</th>
            <th>PickUp</th>
            </thead>
            <tbody>
            <?php
            while($row_nonconv = mysqli_fetch_object($res_nonconv)){
                $booking_id = $row_nonconv->booking_id;
                $user_id = $row_nonconv->user_id;
                $veh_id = $row_nonconv->user_veh_id;
                $flag = $row_nonconv->flag;
                $st = $row_nonconv->booking_status;

                $res_user = mysqli_query($conn,"SELECT name FROM user_register WHERE reg_id='$user_id'");
                $row_user = mysqli_fetch_object($res_user);
                $user_name = $row_user->name;
                
                
                if($flag == '1'){
                    $redir = base64_encode("c") ; 
                }
                else if($st == '0'){
                    $redir = base64_encode("o") ; 
                }
                else{
                    $redir = base64_encode("l") ; 
                }

                
              ?>
                <tr>
                <td><a target="_blank" href="user_details.php?t=<?php echo $redir; ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>"><?php echo $booking_id; ?></a></td>
                <td><?php echo $user_name; ?></td>
                <td><?php echo $row_nonconv->service_type; ?></td>
                <td><?php echo $row_nonconv->shop_name; ?></td>
                <td><?php $p = $row_nonconv->pick_up; echo $pc = $p== '1' ? 'Yes': 'No'; ?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
            </table>
            <?php
        }
        else{
            echo "<h4> No Records Found!</h4>";
        }

       // print_r($_POST); 
        break;
    default: echo "<h4>Sorry! No records to Display!!!</h4>"; 
}


?>
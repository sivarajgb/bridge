<?php
error_reporting(E_ALL); ini_set('display_errors', 1);
include("../config.php");
$conn = db_connect1(); 
session_start();


$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$cluster_admin = $_SESSION['cluster_admin'];

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$city = $_GET['city'];
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;


function run_query($conn, $query){
	$query_result = mysqli_query($conn,$query);
	$query_rows = mysqli_fetch_object($query_result);
	return $query_object = $query_rows->count;
}
function run_query1($conn, $query){
	$count_no_leads = 0;
	$count_no_bookings = 0;
	$count_no_cancelled = 0;
	$count_no_others = 0;
	$count_no_unwanted = 0;
	$query_result = mysqli_query($conn,$query);
	while($query_rows = mysqli_fetch_object($query_result))
	{
		$booking_id = $query_rows->booking_id;
		$booking_status = $query_rows->booking_status;
		$flag = $query_rows->flag;
		$flag_unwntd = $query_rows->flag_unwntd;
		if($booking_status == 2 && $flag != 1 && $flag_unwntd == 0)
		{
			$count_no_bookings = $count_no_bookings + 1;
		}
		else if($booking_status == 1 && $flag != 1 && $flag_unwntd == 0)
		{
			$count_no_leads = $count_no_leads + 1;
		}
		else if($flag == 1)
		{
			$count_no_cancelled = $count_no_cancelled + 1;
		}
		else if($booking_status == 0 && $flag != 1)
		{
			$count_no_others = $count_no_others + 1;
		}
	}
	$rtn['count_no_bookings'] = $count_no_bookings;
	$rtn['count_no_leads'] = $count_no_leads;
	$rtn['count_no_cancelled'] = $count_no_cancelled;
	$rtn['count_no_others'] = $count_no_others;
	return $rtn;
}

$cond ='';
$cond1 = '';
$cond2 = '';
 
$cond = $cond.($city == 'all' ? "" : "AND b.city='$city'");

if($cluster_admin == '1'){
	$sql_get_clus = "SELECT crm_cluster FROM crm_admin WHERE crm_log_id='$crm_log_id'";
	$res_get_clus = mysqli_query($conn,$sql_get_clus);
	$row_get_clus = mysqli_fetch_object($res_get_clus);
	$crm_cluster = $row_get_clus->crm_cluster;

	$sql_get_people = "SELECT crm_log_id FROM crm_admin WHERE crm_cluster='$crm_cluster'";
	$res_get_people = mysqli_query($conn,$sql_get_people);

	$people = "";

	while($row_get_people = mysqli_fetch_array($res_get_people)){
		if($people == ""){
			$people = $row_get_people['crm_log_id']."'";
		}
		else{
			$people = $people.",'".$row_get_people['crm_log_id']."'";
		}
	}
	$cond1 = $cond1.($cluster == 'all' ? " AND b.crm_update_id IN('".$people.")" : " AND b.crm_update_id IN('".$people.") AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'");
	$cond2 = $cond2.($cluster == 'all' ? " AND b.crm_update_id IN('crm003','crm036','".$people.")" : " AND b.crm_update_id IN('crm003','crm036','".$people.") AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'");
}

/* $select_sql =  "SELECT count(DISTINCT b.booking_id) as count FROM user_booking_tb as b LEFT JOIN localities as l ON b.locality = l.localities WHERE b.booking_id!='' {$cond}";
$select_sql1 =  "SELECT count(DISTINCT b.booking_id) as count FROM user_booking_tb as b LEFT JOIN localities as l ON b.locality = l.localities LEFT JOIN b2b.b2b_booking_tbl as bb ON b.booking_id=bb.gb_booking_id  LEFT JOIN b2b.b2b_status as s ON bb.b2b_booking_id=s.b2b_booking_id WHERE b.booking_id!='' {$cond}"; */
$select_sql =  "SELECT count(DISTINCT b.booking_id) as count FROM user_booking_tb as b LEFT JOIN localities as l ON b.locality = l.localities WHERE b.booking_id!='' {$cond}";
$select_sql1 =  "SELECT DISTINCT b.booking_id,b.booking_status,b.flag,b.flag_unwntd FROM user_booking_tb as b LEFT JOIN localities as l ON b.locality = l.localities WHERE b.booking_id!='' {$cond}";

// leads count
/* $no_leads = run_query($conn, "{$select_sql1} {$cond1} AND ((bb.gb_booking_id!='0' AND bb.b2b_flag!='1' AND s.b2b_acpt_flag='0' AND b.booking_status='2') OR b.booking_status='1') AND b.flag!='1' AND b.flag_unwntd = '0' AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
if($startdate=='2016-01-01')
{
	$no_leads = run_query($conn, "{$select_sql1} {$cond1} AND ((bb.gb_booking_id!='0' AND bb.b2b_flag!='1' AND s.b2b_acpt_flag='0' AND b.booking_status='2') OR b.booking_status='1') AND b.flag!='1' AND b.flag_unwntd = '0'");
} */
$no_leads_arr = run_query1($conn, "{$select_sql1} {$cond1} AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
if($startdate=='2016-01-01')
{
	$no_leads_arr = run_query1($conn, "{$select_sql1} {$cond1}");
}
$no_leads = $no_leads_arr['count_no_leads'];

// bookings count
/* $no_bookings = run_query($conn, "{$select_sql1} {$cond2} AND bb.gb_booking_id!='0' AND bb.b2b_flag!='1' AND s.b2b_acpt_flag='1' AND b.flag!='1' AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
if($startdate=='2016-01-01')
{
	$no_bookings = run_query($conn, "{$select_sql1} {$cond2} AND bb.gb_booking_id!='0' AND bb.b2b_flag!='1' AND s.b2b_acpt_flag='1' AND b.flag!='1' ");
} */
$no_bookings = $no_leads_arr['count_no_bookings'];

// followups count
$no_followups = run_query($conn, "{$select_sql} {$cond1} AND b.booking_status IN(3,4,5,6) AND b.flag!='1' AND DATE(b.followup_date) BETWEEN '$startdate' AND '$enddate'");
if($startdate=='2016-01-01')
{
	$no_followups = run_query($conn, "{$select_sql} {$cond1} AND b.booking_status IN(3,4,5,6) AND b.flag!='1' ");
}

// cancelled count
/* $no_cancelled = run_query($conn, "{$select_sql} {$cond1} AND b.flag='1' AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
if($startdate=='2016-01-01')
{
	$no_cancelled = run_query($conn, "{$select_sql} {$cond1} ANDb.flag='1' ");
} */
$no_cancelled = $no_leads_arr['count_no_cancelled'];

//others count
/* $no_others = run_query($conn, "{$select_sql} {$cond1} AND b.booking_status='0' AND b.flag!='1' AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
if($startdate=='2016-01-01')
{
	$no_others = run_query($conn, "{$select_sql} {$cond1} AND b.booking_status='0' AND b.flag!='1' ");
} */
$no_others = $no_leads_arr['count_no_others'];


// overall count
/* $no_overall = run_query($conn, "{$select_sql} {$cond2} AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
if($startdate=='2016-01-01')
{
$select_sql = "SELECT count(b.booking_id) as count FROM user_booking_tb as b WHERE {$cond}";
$no_overall = run_query($conn, "{$select_sql} {$cond2} ");
} */

// unwanted leads count
/* $no_unwanted = run_query($conn, "{$select_sql} {$cond1} AND b.booking_status='1' AND b.flag!='1' AND b.flag_unwntd = '1' AND DATE(b.log) BETWEEN '$startdate' AND '$enddate' ");
if($startdate=='2016-01-01')
{
	$no_unwanted = run_query($conn, "{$select_sql} {$cond1} AND b.booking_status='1' AND b.flag!='1' AND b.flag_unwntd = '1' ");
}
if($no_unwanted == '' || $no_unwanted == '0'){
	$no_unwanted = 0;
} */


// echo $data = json_encode(array('leadsCount'=>$no_leads, 'bookingsCount'=>$no_bookings, 'followupsCount'=>$no_followups, 'cancelledCount'=>$no_cancelled, 'othersCount'=>$no_others, 'overallCount'=>$no_overall, 'unwantedCount'=>$no_unwanted,'incompleteCount'=>$no_incomplete));
echo $data = json_encode(array('leadsCount'=>$no_leads, 'bookingsCount'=>$no_bookings, 'followupsCount'=>$no_followups, 'cancelledCount'=>$no_cancelled, 'othersCount'=>$no_others));
//echo $select_sql;
?>


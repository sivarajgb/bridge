<?php
error_reporting(E_ALL); ini_set('display_errors', 1);
include("../config.php");
$conn = db_connect1(); 
session_start();


$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$city = $_GET['city'];
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;


function run_query($conn, $query){
	$query_result = mysqli_query($conn,$query);
	$query_rows = mysqli_fetch_object($query_result);
	return $query_object = $query_rows->count;
}
function run_query1($conn, $query){
	$count_no_leads = 0;
	$count_no_bookings = 0;
	$count_no_followups = 0;
	$count_no_cancelled = 0;
	$count_no_others = 0;
	$count_no_overall = 0;
	$count_no_unwanted = 0;
	$query_result = mysqli_query($conn,$query);
	// $followup_arr = array(3,4,5,6);
	while($query_rows = mysqli_fetch_object($query_result))
	{
		$booking_id = $query_rows->booking_id;
		$booking_status = $query_rows->booking_status;
		$flag = $query_rows->flag;
		$flag_unwntd = $query_rows->flag_unwntd;
		$count_no_overall = $count_no_overall + 1;
		if($booking_status == 2 && $flag != 1 && $flag_unwntd == 0)
		{
			$count_no_bookings = $count_no_bookings + 1;
		}
		else if($booking_status == 1 && $flag != 1 && $flag_unwntd == 0)
		{
			$count_no_leads = $count_no_leads + 1;
		}
		// else if(in_array($booking_status,$followup_arr) && $flag != 1)
		// {
			// $count_no_followups = $count_no_followups + 1;
		// }
		else if($flag == 1)
		{
			$count_no_cancelled = $count_no_cancelled + 1;
		}
		else if($booking_status == 0 && $flag != 1)
		{
			$count_no_others = $count_no_others + 1;
		}
		// booking_status='1' AND b.flag!='1' AND b.flag_unwntd = '1'
		else if($booking_status == 1 && $flag != 1 && $flag_unwntd == 1)
		{
			$count_no_unwanted = $count_no_unwanted + 1;
		}
	}
	$rtn['count_no_bookings'] = $count_no_bookings;
	$rtn['count_no_leads'] = $count_no_leads;
	$rtn['count_no_followups'] = $count_no_followups;
	$rtn['count_no_cancelled'] = $count_no_cancelled;
	$rtn['count_no_others'] = $count_no_others;
	$rtn['count_no_overall'] = $count_no_overall;
	$rtn['count_no_unwanted'] = $count_no_unwanted;
	return $rtn;
}

$cond ='';
 
$cond = $cond.($city == 'all' ? "" : "AND b.city='$city'");
$cond = $cond.($cluster == 'all' ? "" : "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'");

$select_sql =  "SELECT count(DISTINCT b.booking_id) as count FROM user_booking_tb as b LEFT JOIN localities as l ON b.locality = l.localities WHERE b.booking_id!='' {$cond}";
$select_sql1 =  "SELECT DISTINCT b.booking_id,b.booking_status,b.flag,b.flag_unwntd FROM user_booking_tb as b LEFT JOIN localities as l ON b.locality = l.localities WHERE b.booking_id!='' {$cond}";

// leads count
$no_leads_arr = run_query1($conn, "{$select_sql1} AND b.override_flag !='1' AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
if($startdate=='2016-01-01')
{
	$no_leads_arr = run_query1($conn, "{$select_sql1} AND b.override_flag !='1'");
}
$no_leads = $no_leads_arr['count_no_leads'];

// bookings count

//$no_bookings = run_query1($conn, "{$select_sql1} AND b.booking_status='2' AND b.flag!='1' AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
// if($startdate=='2016-01-01')
// {
	// $no_bookings = run_query2($conn, "{$select_sql1} AND bb.gb_booking_id!='0' AND bb.b2b_flag!='1' AND s.b2b_acpt_flag='1' AND b.flag!='1' ");
// }
$no_bookings = $no_leads_arr['count_no_bookings'];

// followups count
$no_followups = run_query($conn, "{$select_sql} AND b.booking_status IN(3,4,5,6) AND b.flag!='1' AND DATE(b.followup_date) BETWEEN '$startdate' AND '$enddate'");
if($startdate=='2016-01-01')
{
	$no_followups = run_query($conn, "{$select_sql}");
}
// $no_followups = $no_leads_arr['count_no_followups'];

// cancelled count
// $no_cancelled = run_query($conn, "{$select_sql} AND b.flag='1' AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
// if($startdate=='2016-01-01')
// {
	// $no_cancelled = run_query($conn, "{$select_sql} ANDb.flag='1' ");
// }
$no_cancelled = $no_leads_arr['count_no_cancelled'];

//others count
// $no_others = run_query($conn, "{$select_sql} AND b.booking_status='0' AND b.flag!='1' AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
// if($startdate=='2016-01-01')
// {
	// $no_others = run_query($conn, "{$select_sql} AND b.booking_status='0' AND b.flag!='1' ");
// }
$no_others = $no_leads_arr['count_no_others'];

// overall count
// $no_overall = run_query($conn, "{$select_sql} AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
// if($startdate=='2016-01-01')
// {
// $select_sql = "SELECT count(b.booking_id) as count FROM user_booking_tb as b WHERE {$cond}";
// $no_overall = run_query($conn, "{$select_sql} ");
// }
//$no_overall = $no_leads_arr['count_no_overall'];

// unwanted leads count
// $no_unwanted = run_query($conn, "{$select_sql} AND b.booking_status='1' AND b.flag!='1' AND b.flag_unwntd = '1' AND DATE(b.log) BETWEEN '$startdate' AND '$enddate' ");
// if($startdate=='2016-01-01')
// {
	// $no_unwanted = run_query($conn, "{$select_sql} AND b.booking_status='1' AND b.flag!='1' AND b.flag_unwntd = '1' ");
// }
// if($no_unwanted == '' || $no_unwanted == '0'){
	// $no_unwanted = 0;
// }
//$no_unwanted = $no_leads_arr['count_no_unwanted'];

// incomplete count
/* $no_incomplete = run_query($conn, "{$select_sql}  AND b.booking_status IN(3,4,5,6) AND b.flag!='1' AND DATE_ADD(b.followup_date,INTERVAL 2 DAY) BETWEEN '$startdate' AND '$enddate'");
if($startdate=='2016-01-01')
{
	$no_incomplete = run_query($conn, "{$select_sql} AND DATE_ADD(b.followup_date,INTERVAL 2 DAY)");
} */
$sql_override = "SELECT count(DISTINCT o.booking_id) as count FROM override_tbl as o LEFT JOIN user_booking_tb as b on b.booking_id=o.booking_id LEFT JOIN localities as l ON b.locality = l.localities BETWEEN '$startdate' AND '$enddate' WHERE o.booking_id!='' {$cond} AND DATE(o.log) BETWEEN '$startdate' AND '$enddate' ";
$res_override = mysqli_query($conn,$sql_override);
$count = mysqli_num_rows($res_override);
if($count >0){
while($row_override = mysqli_fetch_object($res_override)){
	$override_count = $row_override->count;
}
}
else {
	$override_count = "0";
}

		echo $data = json_encode(array('leadsCount'=>$no_leads, 'bookingsCount'=>$no_bookings, 'followupsCount'=>$no_followups, 'cancelledCount'=>$no_cancelled, 'othersCount'=>$no_others,'OverrideCount'=>$override_count));
//echo $select_sql;
?>


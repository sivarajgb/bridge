<?php
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

 $startdate = date('Y-m-d',strtotime($_POST['startdate']));
 $enddate =  date('Y-m-d',strtotime($_POST['enddate']));
 $source = $_POST['source'];

if($source == "all"){
  $sql_booking = "SELECT booking_id,user_id,mec_id,source,vehicle_type,user_veh_id,service_type,service_date,shop_name,axle_flag,locality,utm_source,priority,flag_fo,crm_update_id,crm_update_time,user_pmt_flg FROM user_booking_tb WHERE (crm_update_id = '$crm_log_id' OR crm_update_id = '') AND booking_status='1' AND flag!='1' AND flag_unwntd = '0' AND DATE(crm_update_time) BETWEEN '$startdate' and '$enddate' ORDER BY crm_update_time DESC"; 
}
else{
$sql_booking = "SELECT booking_id,user_id,mec_id,source,vehicle_type,user_veh_id,service_type,service_date,shop_name,axle_flag,locality,utm_source,priority,flag_fo,crm_update_id,crm_update_time,user_pmt_flg FROM user_booking_tb WHERE (crm_update_id = '$crm_log_id' OR crm_update_id = '') AND source = '$source' AND booking_status='1' AND flag!='1' AND flag_unwntd = '0' AND DATE(crm_update_time) BETWEEN '$startdate' and '$enddate' ORDER BY crm_update_time DESC";
}

$res_booking = mysqli_query($conn,$sql_booking);

$no = 0;

$count = mysqli_num_rows($res_booking);
if($count >0){
while($row_booking = mysqli_fetch_object($res_booking)){
$booking_id = $row_booking->booking_id;
$user_id = $row_booking->user_id;
$mec_id = $row_booking->mec_id;
$log = $row_booking->crm_update_time;
$bsource = $row_booking->source;
$veh_type = $row_booking->vehicle_type;
$veh_id = $row_booking->user_veh_id;
$service_type = $row_booking->service_type;
$service_date = $row_booking->service_date;
$shop_name = $row_booking->shop_name;
$axle_flag = $row_booking->axle_flag;
$locality = $row_booking->locality;
$utm_source = $row_booking->utm_source;
$priority = $row_booking->priority;
$flag_fo = $row_booking->flag_fo;
$alloted_to_id = $row_booking->crm_update_id;
$payment_flag = $row_booking->user_pmt_flg;

if($no>0)
{
	if(($temp_booking->user_id==$user_id)&&($temp_booking->service_type==$service_type)&&(round(abs(strtotime($log) - strtotime($temp_booking->crm_update_time)) / 60,2)<5))
		{
			$update_booking = "UPDATE user_booking_tb SET flag='1',flag_unwntd='1' WHERE booking_id = '$booking_id'";
			$res=mysqli_query($conn,$update_booking) or die(mysqli_error($conn));
			$temp_booking = $row_booking;
			continue;
		}
}
$temp_booking = $row_booking;

// get user details from user register table using the user id from booking table
$sql_user = "SELECT name,mobile_number,lat_lng,Locality_Home FROM user_register WHERE reg_id = '$user_id' ";
$res_user = mysqli_query($conn,$sql_user);
$row_user = mysqli_fetch_object($res_user);

$user_name = $row_user->name;
$user_mobile = $row_user->mobile_number;
$address = $row_user->lat_lng;
$home = $row_user->Locality_Home;

if($veh_id!='' || $veh_id!='0'){
        $veh_query = "SELECT brand, model FROM user_vehicle_table WHERE id = '$veh_id'";
        $veh_res = mysqli_query($conn, $veh_query);
        $veh_row = mysqli_fetch_object($veh_res);
        $veh_brand = $veh_row->brand;
        $veh_model = $veh_row->model;
}
// go axle status
$sql_axle = mysqli_query($conn2,"SELECT max(b2b_booking_id) as b2b_booking_id FROM b2b_booking_tbl WHERE gb_booking_id='$booking_id'");
$row_axle= mysqli_fetch_object($sql_axle);
$bid=$row_axle->b2b_booking_id;

$sql_b2b_status = mysqli_query($conn2,"SELECT b2b_acpt_flag,b2b_deny_flag,b2b_acpt_log FROM b2b_status WHERE b2b_booking_id='$bid'");
$row_b2b_status = mysqli_fetch_object($sql_b2b_status);
$accept = $row_b2b_status->b2b_acpt_flag;
$deny = $row_b2b_status->b2b_deny_flag;

 if($alloted_to_id =='' || $alloted_to_id==' ' || $alloted_to_id=='0'){
        ?>
            <tr style="background-color:#EF5350 !important;">
        <?php
    }
    else{
        ?>
            <tr>
        <?php
    }
    ?>

    <td><?php echo $no=$no+1 ; ?></td>
    <td><p style="float:left;padding:10px;"><?php echo $booking_id; ?></p>
    <?php
        if($flag_fo == '1'){
          ?>
    <p class="p" style="font-size:16px;color:#D81B60;float:left;padding:5px;" title="Resheduled Booking!"><i class="fa fa-recycle" aria-hidden="true"></i></p>
    <?php
    switch($priority){
          case '1': ?>
    <p style="font-size:16px;color:#00ACC1;float:left;padding:5px;" title="Low Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>
    <?php   break;
          case '2':?>
    <p style="font-size:16px;color:#FFB300;float:left;padding:5px;" title="Medium Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>
    <?php   break;
          case '3':?>
    <p style="font-size:16px;color:#E53935;float:left;padding:5px;" title="High Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>
    <?php   break;
        }
        }
        if($payment_flag == '1'){
          ?>
              <p style="font-size:16px;color:#1c57af;float:left;padding:5px;" title="Paid!"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></p>                    
          <?php
          }
        ?>
    </td>
    <td><?php
    if($axle_flag!='1'){
    if($mec_id == '' || $mec_id == 0 || $mec_id == 1 || $service_type == '' || $service_date == '' || $veh_id == '0' ){?>
      <a href="user_details.php?t=<?php echo base64_encode("l"); ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>" style="background-color:#ffa800;padding:6px;padding-right:8px;border-radius:15px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>
    <?php }
    else{
     ?>
    <a href="go_axle.php?bi=<?php echo $booking_id; ?>&t=<?php echo 'b'; ?>" style="background-color:#ffa800;padding:6px;padding-right:8px;border-radius:15px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>
    <?php }
     }
    else if($accept == 1 && $deny == 0 ){ ?>
    <p style="background-color:#6FA3DE;padding:7px;width:30px;border-radius:18px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>
    <?php }
    else if($accept==0 && $deny==1){ ?>
    <p style="background-color:#D25F34;padding:7px;width:30px;border-radius:18px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>
    <?php }
    else if($accept==0 && $deny==0){ ?>
    <p style="background-color:#69ED85;padding:7px;width:30px;border-radius:18px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>
    <?php } ?></td>
    <td><?php
    switch($service_type){
      case 'general_service' :
      if($veh_type =='2w'){
        echo "General Service";
      }
      else{
        echo "Car service and repair";
      }
      break;
      case 'break_down':  echo "Breakdown Assistance"; break;
      case 'tyre_puncher': echo "Tyre Puncture";  break;
      case 'other_service': echo "Repair Job"; break;
      case 'car_wash':
      if($veh_type == '2w'){
        echo "water Wash";
      }
      else{
        echo "Car wash exterior";
      }break;
      case 'engine_oil': echo "Repair Job"; break;
      case 'free_service': echo "General Service"; break;
      case 'car_wash_both':
      case 'completecarspa': echo "Complete Car Spa"; break;
      case 'car_wash_ext': echo "Car wash exterior"; break;
      case 'car_wash_int': echo "Interior Detailing"; break;
      case 'Doorstep car spa': echo "Doorstep Car Spa"; break;
      case 'Doorstep_car_wash': echo "Doorstep Car Wash"; break;
      case 'free_diagnastic':
      case 'free_diagnostic':
      case 'diagnostics':
      if($veh_type == '2w'){
        echo "Diagnostics/Check-up";
     }
     else{
       echo "Vehicle Diagnostics";
     }
         break;
      case 'water_wash': echo "water Wash"; break;
      case 'exteriorfoamwash': echo "Car wash exterior"; break;
      case 'interiordetailing': echo "Interior Detailing"; break;
      case 'rubbingpolish': echo "Car Polish"; break;
      case 'underchassisrustcoating': echo "Underchassis Rust Coating"; break;
      case 'headlamprestoration': echo "Headlamp Restoration"; break;
      default: echo $service_type;
      }  ?>
     </td>
    <td><a href="user_details.php?t=<?php echo base64_encode("l"); ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>"><i class="fa fa-eye" aria-hidden="true"></i></td>
    <td><?php echo $user_name; ?></td>
    <td><?php echo $user_mobile; ?></td>
     <td><?php if($bsource == "Hub Booking"){ if($locality!=''){ echo $locality; } else if($home == ''){ echo $address; } else { echo $home; } } else if($bsource == "GoBumpr App" || $bsource == "App"){ echo $address; } else{  echo $locality; } ?></td>
    <td><?php echo $veh_brand." ".$veh_model; ?></td>
    <td><?php 
      if (($mec_id=='0'&&$shop_name=='')||($shop_name=='0')) {
        # code...
      
      //if ($service_type=='Car Wash 199'||$service_type=='Wash & Check up 499'||$service_type=='Bumper Repainting 1999') {
        ?>



          <button type="button" style="margin-top: 25px;" class="btn btn-info btn-lg" data-brand="<?php echo $veh_brand;?> " data-id="<?php echo $booking_id ?>" data-toggle="modal" data-locality="<?php echo $locality ?>" data-vehicletype="<?php echo $veh_type ?>"   data-servicetype="<?php echo $service_type ?>" data-target="#myModal">Xpress GoAxle</button>

 <?php }
 else {
      echo $shop_name;
      } ?></td>
    <td><?php if($service_date == "0000-00-00"){ echo "Not Updated Yet"; } else {  echo date('d M Y', strtotime($service_date)); } ?></td>
    <td><?php echo $bsource; ?></td>
    <td><?php if($utm_source == ''){ echo "NA";  } else{ echo $utm_source; } ?></td>
    <td><?php echo date('d M Y h.i A', strtotime($log)); ?></td>
  </tr>
  <?php
}
} // if
else {
  echo "no";
}
 ?>

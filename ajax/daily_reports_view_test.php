<?php
include("../config.php");
$conn = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$extnl_ld_fflag = $_SESSION['extnl_ld_fflag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$master_service = $_POST['master_service'];
$service = $_POST['service'];
$person = $_POST['person'];
$today = date('Y-m-d');
$services = '';

$programatic = $_POST['programatic'];
$city = $_POST['city'];

    //0-all && 1-particular
class bookings {
    private $master_service;
    private $service;
    private $person;
    private $services;

    function __construct($master_service,$service,$person){
        $this->master_service = $master_service;
        $this->service = $service;
        $this->person = $person;
        
        if($this->master_service != "" && $this->service == ""){
            //echo " entered";
            $conn1 = db_connect1();
            $sql_serv = "SELECT service_type FROM go_axle_service_price_tbl WHERE bridge_flag='0' AND master_service='$this->master_service'";
            $res_serv = mysqli_query($conn1,$sql_serv);
            $this->services = '';
            while($row_serv = mysqli_fetch_array($res_serv)){
                if($this->services == ""){
                    $this->services = $row_serv['service_type']."'";
                }
                else{
                    $this->services = $this->services.",'".$row_serv['service_type']."'";
                }
            }
        }
    }
    function s0_p0(){
        $master = $this->master_service;
        if($master == ""){
            return "";
        }
        else{
            return "AND g.service_type IN('$this->services)";
        }
    }
    function s0_p1(){
        $master = $this->master_service;
        if($master == ""){
            return "AND g.crm_update_id='$this->person'";
        }
        else{
            return "AND g.crm_update_id='$this->person' AND g.service_type IN('$this->services)";
        }
    }
    function s1_p0(){
        return "AND g.service_type='$this->service'";
    }
    function s1_p1(){
        return "AND g.crm_update_id='$this->person' AND g.service_type='$this->service'";
    }
}

$master_service_fun = $master_service=='all' ? "" : $master_service;
$service_type_fun = $service=='all' ? "" : $service;
$person_fun = $person=='all' ? "" : $person;
$bookings_obj = new bookings($master_service_fun,$service_type_fun,$person_fun);
    
    
$service_val = $service=='all' ? "0" : "1";
$person_val = $person=='all' ? "0" : "1";

$cond = $bookings_obj->{"s{$service_val}_p{$person_val}"}();

?>
<div id="division" style="float:left; margin-top:10px;margin-left:35px;width:65%;">
<div id="div1" style="width:89%;float:left;">
<table class="table table-striped table-bordered tablesorter table-hover results" id="table">
<thead style="background-color:#B2DFDB;align:center;display: table-header-group;">
<th style="text-align:center;">Date <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Day </th>
<th style="text-align:center;">Car Credits <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Bike Credits <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Total <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
</thead>
<tbody id="tbody">

<?php


$start = strtotime($startdate);
$end = strtotime($enddate);
$noofdays = date('t',strtotime($start));
// Loop between timestamps, 24 hours at a time
for ( $i = $start; $i <= $end; $i = $i + 86400 ) {
    $thisDate = date('d M Y', $i);
    $day = date('l', strtotime($thisDate));

    $date = date('Y-m-d', $i);

    $enc_date=base64_encode($date);
    $enc_2w = base64_encode('2w');
    $enc_4w = base64_encode('4w');

if($programatic == "yes"){
    $sql_b2b = "SELECT b.b2b_vehicle_type,b.b2b_credit_amt,b.gb_booking_id,b.b2b_log FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id=g.booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log)='$date' AND b.b2b_shop_id NOT IN (1014,1035) AND g.city='$city' {$cond}";    
}
else{
    $sql_b2b = "SELECT b.b2b_vehicle_type,b.b2b_credit_amt,b.gb_booking_id,b.b2b_log FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id=g.booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log)='$date' AND g.crm_update_id NOT IN ('crm003','crm036') AND b.b2b_shop_id NOT IN (1014,1035) AND g.city='$city' {$cond}";    
}
    $res_b2b = mysqli_query($conn,$sql_b2b);

    //initialize
    $bike = 0;
    $car = 0;
    $total = 0 ;

    while($row_b2b = mysqli_fetch_object($res_b2b)){
        $veh_type = $row_b2b->b2b_vehicle_type;
        $creditamount = $row_b2b->b2b_credit_amt;
        $gb_booking_id = $row_b2b->gb_booking_id;
        $log = $row_b2b->b2b_log;

        switch($veh_type){
            case '2w': $bike = $bike+$creditamount; break;
            case '4w': $car = $car+$creditamount; break;
        }//switch
    } // while

    $bikecredits = $bike/100;
    $carcredits = $car/100;
    $totalcredits = $bikecredits+$carcredits;
if($date > $today){
    continue;
}
    $list_credits[] = array("date" => $date, "bikecredits" => $bikecredits,"carcredits" =>$carcredits , "totalcredits" => $totalcredits);
?>
    <tr>
    <td style="text-align:center;"><!--<a href="daily_reports_detailed.php?t=<?php echo base64_encode(feedback); ?>&dt=<?php echo $enc_date;?>" >--><?php echo $thisDate; ?><!--</a>--></td>
    <td style="text-align:center;"><?php echo $day;?></td>
<?php

$car_cpd = 150; //car credits per day
$bike_cpd = 40;  //bike credits per day
$total_cpd = $car_cpd+$bike_cpd;

$car_120p = 120*($car_cpd/100);  // 120% of car credits
$bike_120 = 120*($bike_cpd/100); // 120% of car credits
$total_120 = $car_120p+$bike_120;



 if($carcredits >= $car_120p){ ?><td style="background-color:#7bde95;text-align:center;"><a href="daily_reports_servicetype.php?t=<?php echo base64_encode(reports); ?>&dt=<?php echo $enc_date;?>&vt=<?php echo $enc_4w; ?>" ><?php echo $carcredits; ?></a></td> <?php }
 else if($carcredits <$car_120p && $carcredits>= $car_cpd){ ?><td style="background-color:#f3ae2b;text-align:center;"><a href="daily_reports_servicetype.php?t=<?php echo base64_encode(reports); ?>&dt=<?php echo $enc_date;?>&vt=<?php echo $enc_4w; ?>" ><?php echo $carcredits; ?></a></td> <?php }
 else{ ?><td style="text-align:center;background-color:#e88c87;"><a href="daily_reports_servicetype.php?dt=<?php echo $enc_date;?>&vt=<?php echo $enc_4w; ?>&t=<?php echo base64_encode(reports); ?>" ><?php echo $carcredits; ?></a></td> <?php }
 if($bikecredits >= $bike_120){ ?><td style="background-color:#7bde95;text-align:center;"><a href="daily_reports_servicetype.php?t=<?php echo base64_encode(reports); ?>&dt=<?php echo $enc_date;?>&vt=<?php echo $enc_2w; ?>" ><?php echo $bikecredits; ?></a></td> <?php }
 else if($bikecredits <$bike_120 && $bikecredits >=$bike_cpd ){ ?><td style="background-color:#f3ae2b;text-align:center;"><a href="daily_reports_servicetype.php?t=<?php echo base64_encode(reports); ?>&dt=<?php echo $enc_date;?>&vt=<?php echo $enc_2w; ?>" ><?php echo $bikecredits; ?></a></td> <?php }
 else{ ?><td style="text-align:center;background-color:#e88c87;"><a href="daily_reports_servicetype.php?t=<?php echo base64_encode(reports); ?>&dt=<?php echo $enc_date;?>&vt=<?php echo $enc_2w; ?>" ><?php echo $bikecredits; ?></a></td> <?php }
 if($totalcredits >= $total_120){ ?><td style="background-color:#7bde95;text-align:center;"><?php echo $totalcredits; ?></td> <?php }
 else if($totalcredits < $total_120 && $totalcredits >= $total_cpd){ ?><td style="background-color:#f3ae2b;text-align:center;"><?php echo $totalcredits; ?></td> <?php }
 else{ ?><td style="text-align:center;background-color:#e88c87;"><?php echo $totalcredits; ?></td> <?php } ?>
</tr>
<?php
}// for
$carsum =0 ;$carcount=0;$caravg=0;
//get non zero car credits and calculate avg
foreach($list_credits as $key=>$row){
    if($row['carcredits'] != 0){
        $carsum=$carsum+$row['carcredits'];
        $carcount = $carcount+1;
    }
    else{
        continue;
    }
}
$caravg = ceil($carsum/$carcount);

$bikesum =0 ;$bikecount=0;$bikeavg=0;
//get non zero bike credits and calculate avg
foreach($list_credits as $key=>$row){
    if($row['bikecredits'] != 0){
        $bikesum=$bikesum+$row['bikecredits'];
        $bikecount = $bikecount+1;
    }
    else{
        continue;
    }
}
$bikeavg = ceil($bikesum/$bikecount);
//get non zero total credits
$achieved_car=0;$acheivedper_car =0;
foreach($list_credits as $key=>$row){
        $achieved_car=$achieved_car+$row['carcredits'];
}
$acheivedper_car = ceil(($achieved_car/($car_cpd*20))*100);
$achieved_bike=0;$acheivedper_bike =0;
foreach($list_credits as $key=>$row){
        $achieved_bike=$achieved_bike+$row['bikecredits'];
}
$acheivedper_bike = ceil(($achieved_bike/($bike_cpd*20))*100);
//calculated projected consumpsion credits= average credits*no of days in that month
$carprocon = ceil($caravg*$noofdays);
$bikeprocon = ceil($bikeavg*$noofdays);

?>

</tbody>
</table>
</div>
<div id="rightpane" style="margin-top:-30px;position:fixed;right:30px;width:30%;">
<div style="float:left;width:90%;">
<table class="table table-bordered" style="border:1px solid #00BCD4;margin:15px;padding:15px;overflow:auto;" >
<thead>
<th colspan="2" style="border:1px solid #B2EBF2;text-align:center;background-color:#B2EBF2;" >Target</th>
</thead>
<tbody>
<tr>
<td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;">Car</td>
<td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;"><?php echo $car_cpd." Credits/Day"; ?></td>
</tr>
<tr>
<td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;">Bike</td>
<td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;"><?php echo $bike_cpd." Credits/Day"; ?></td>
</tr>
</tbody>
</table>
</div>
<div style="float:left;width:38%;">
<table class="table table-bordered" style="border:1px solid #00BCD4;margin:15px;padding:15px;" >
<thead>
<th colspan="2" style="border:1px solid #95ea85;text-align:center;background-color:#95ea85;" >Daily Average</th>
</thead>
<tbody>
<tr>
<td style="border:1px solid #95ea85;text-align:center;font-size:13px;">Car</td>
<td style="border:1px solid #95ea85;text-align:center;font-size:13px;"><?php echo $caravg; ?></td>
</tr>
<tr>
<td style="border:1px solid #95ea85;text-align:center;font-size:13px;">Bike</td>
<td style="border:1px solid #95ea85;text-align:center;font-size:13px;"><?php echo $bikeavg; ?></td>
</tr>
<tr>
<td style="border:1px solid #95ea85;text-align:center;font-size:13px;">Total</td>
<td style="border:1px solid #95ea85;text-align:center;font-size:13px;"><?php echo $caravg+$bikeavg; ?></td>
</tr>
</tbody>
</table>
</div>
<div style="float:left;width:49%;margin-left:10px;">
<table class="table table-bordered" style="border:1px solid #00BCD4;margin:15px;padding:15px;" >
<thead>
<th colspan="2" style="border:1px solid #d4c166;text-align:center;background-color:#d4c166;font-size:14px;" >Projected Consumption</th>
</thead>
<tbody>
<tr>
<td style="border:1px solid #d4c166;text-align:center;font-size:13px;">Car</td>
<td style="border:1px solid #d4c166;text-align:center;font-size:13px;"><?php echo $carprocon; ?></td>
</tr>
<tr>
<td style="border:1px solid #d4c166;text-align:center;font-size:13px;">Bike</td>
<td style="border:1px solid #d4c166;text-align:center;font-size:13px;"><?php echo $bikeprocon; ?></td>
</tr>
<tr>
<td style="border:1px solid #d4c166;text-align:center;font-size:13px;">Total</td>
<td style="border:1px solid #d4c166;text-align:center;font-size:13px;"><?php echo $carprocon+$bikeprocon; ?></td>
</tr>
</tbody>
</table>
</div>
<div style="float:left;width:90%;">
<table class="table table-bordered" style="border:1px solid #00BCD4;margin:15px;padding:15px;" >
<thead>
<th style="border:1px solid #a3e08f;text-align:center;font-size:13px;background-color:#B2DFDB;"><i class="fa fa-crop" aria-hidden="true"></i></th>
<th style="border:1px solid #B2DFDB;text-align:center;font-size:13px;background-color:#B2DFDB;">Car</th>
<th style="border:1px solid #B2DFDB;text-align:center;font-size:13px;background-color:#B2DFDB;">Bike</th>
</thead>
<tbody>
<tr>
<td style="border:1px solid #B2DFDB;text-align:center;font-size:13px;background-color:#a3e08f;">Target</td>
<td style="border:1px solid #B2DFDB;text-align:center;font-size:13px;"><?php echo $car_cpd*20; ?></td>
<td style="border:1px solid #B2DFDB;text-align:center;font-size:13px;"><?php echo $bike_cpd*20; ?></td>
</tr>
<tr>
<td style="border:1px solid #B2DFDB;text-align:center;font-size:13px;background-color:#a3e08f;">Achieved</td>
<td style="border:1px solid #B2DFDB;text-align:center;font-size:13px;"><?php echo $achieved_car." [".$acheivedper_car." %]"; ?></td>
<td style="border:1px solid #B2DFDB;text-align:center;font-size:13px;"><?php echo $achieved_bike." [".$acheivedper_bike." %]"; ?></td>
</tr>
</tbody>
</table>
</div>
<div style="width:90%;margin-left:8px;">
<table class="table table-bordered" style="border:1px solid #00BCD4;margin:15px;" >
<tbody>
<tr>
<td style="border:2px solid #fff;text-align:left;font-size:11px;background-color:#7bde95;vertical-align:middle;">&nbsp;</td>
<td style="border:2px solid #fff;text-align:left;font-size:11px;">>120% of Target</td>
<td style="border:2px solid #fff;text-align:left;font-size:11px;background-color:#f3ae2b;vertical-align:middle;">&nbsp;</td>
<td style="border:2px solid #fff;text-align:left;font-size:11px;">Target Achieved</td>
<td style="border:2px solid #fff;text-align:left;font-size:11px;background-color:#e88c87;vertical-align:middle;">&nbsp;</td>
<td style="border:2px solid #fff;text-align:left;font-size:11px;">Target not Achieved</td>
</tr>

</tbody>
</table>
</div>
</div>
<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#table").tablesorter( {sortList: [[0,0], [1,0]]} );
    }
);
</script>

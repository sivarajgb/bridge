<?php
header('Content-Type: application/json');
include '../DBController.php';
$conn= new DB_connection();
$page=$_POST[page];
$count_date=date('Y-m-d',strtotime($_POST['count_date']));
$vehicle_type=$_POST['vehicle_type'];
$pid=$_POST['pid'];
$city=$_POST['city'];

if($pid=="ViewUserBooking"){
$html.="<table class='table col-lg-12 user_history' width='100%'>
					<thead>
						<tr>
							<th>S.no</th>
							<th>Date</th>
							<th>BookingId</th>
							<th>UserId</th>
							<th>Service Type</th>
							<th>City</th>
						</tr>
					</thead>
					<tbody>";
 $result=$conn->view_user_count($count_date,$vehicle_type,$page,$city);
 $i=1;
	while($row=mysqli_fetch_assoc($result)){
		$html.="<tr>
					<td>".$i."</td>
					<td>".date('d M Y',strtotime($row[date1]))."</td>
					<td>".$row[booking_id]."</td>
					<td class='all_user_count'><a data-user_id='".$row[user_id]."' data-booking_id='".$row[booking_id]."' data-toggle='modal' data-target='#myModal_user_details' class='user_booking_details'>".$row[user_id]."</a></td>
					<td>".$row[service_type]."</td>
					<td>".$row[city]."</td>
				</tr>";
		$i++;
	}
	echo $data1 = json_encode($html);
	exit;
}elseif($pid=="ViewUserDetails"){
	
	$user_id=$_POST[user_id];
	$booking_id=$_POST[booking_id];
	
	$result=$conn->UserDetails($booking_id);
	$row_edit_user=mysqli_fetch_object($result);
	$user_id=$row_edit_user->reg_id;
	$u_name = $row_edit_user->name;
	$u_mobile = $row_edit_user->mobile_number;
	$u_alt_mobile = $row_edit_user->mobile_number2;
	$u_mail  = $row_edit_user->email_id;
	$u_city  = $row_edit_user->City;
	$u_loc_home  = $row_edit_user->Locality_Home;
	$u_loc_work  = $row_edit_user->Locality_Work;
	$u_source = $row_edit_user->source;
	$u_campaign = $row_edit_user->campaign;
	$u_last_serviced  = $row_edit_user->Last_service_date;
	$u_next_serviced  = $row_edit_user->Next_service_date;
	$u_last_called  = $row_edit_user->Last_called_on;
	$u_followup  = $row_edit_user->Follow_up_date;
	$u_comments  = $row_edit_user->comments;
	$u_regno=$row_edit_user->reg_no;
	$u_brand=$row_edit_user->brand;
	$u_model=$row_edit_user->model;
	$pickup_fulladdress = $row_edit_user->pickup_full_address;
	$pickup_datetime = $row_edit_user->pickup_date_time;
	$service_description = $row_edit_user->service_description;
	$referral=$row_edit_user->referral_count;
	$initial_book=$row_edit_user->initial_service_type;

$html.="<div class='card mb3'>
            <div class='card-header-green'><i class='fa fa-user'></i> User Details
            </div>
            <div class='card-body'>
               <div class='col-md-12' id='user_details_div'>
                      <div class='col-md-3' style='margin-top:5%;'>
							<center><img alt='User Pic' src='images/man.png' class='img-circle img-responsive' width='150px'></center>
							<center><h4><p>".$u_name."</p><p style='color:#01351f;'><img src='images/id-card.png' width='25px'> ".$user_id."</p></h4></center>
					  </div>
                      <div class='col-md-9'>
                          <table class='table table-responsive' id='user_details_tbl'>
                            <tr>
								<td><img src='images/smartphone.png' width='20px'></td>
								<td colspan='3'>".$u_mobile."</td>
							</tr>
                            <tr><td><img src='images/email.png' width='20px'></td>
								<td colspan='3'>".$u_mail."</td>
							</tr>
							<tr>
								<td><img src='images/house.png' width='20px'></i></td>
								<td>".$u_loc_home."</td>
								<td><img src='images/office.png' width='20px'></i></td>
								<td>".$u_loc_work."</td>
							</tr>
                            <tr>
								<td><img src='images/motorcycle.png' width='20px'></i></td>
								<td colspan=3>".$u_brand.",".$u_model.",".$u_regno."</td>
							</tr>
							<tr>
								<td><img src='images/maps-and-flags.png' width='20px'></td>
								<td colspan='3'>".$u_city."</td>
							</tr>
								<tr><td><img src='images/car1.png' width='20px'></td>
								<td>".$initial_book."</td>
								<td><img src='images/refer.png' width='20px'></td>
								<td>".$referral." </td>
							</tr>
                          </table>
						</div>
					</div>
				</div>
			</div>
		</div>
	<table class='table col-lg-12 user_history' width='100%'>
					<thead>
						<tr>
							<th>S.no</th>
							<th>UserName</th>
							<th>BookingId</th>
							<th>Goaxle Status</th>
							<th>Service Type</th>
							<th>Booked On</th>
							<th>City</th>
						</tr>
					</thead>
					<tbody>";
 $result=$conn->view_user_details($user_id);
 $i=1;
	while($row=mysqli_fetch_assoc($result)){
		if($row[booking_status]=='2' && $row[axle_flag]=='1'){
			$status="Goaxled";
		}else{
			$status=" Not Goaxled";
		}
		$html.="<tr>
					<td>".$i."</td>
					<td>".$row[name]."</td>
					<td>".$row[booking_id]."</td>
					<td>".$status."</td>
					<td>".$row[service_type]."</td>
					<td>".date('d M Y',strtotime($row[date1]))."</td>
					<td>".$row[city]."</td>
				</tr>";
		$i++;
	}
	echo $data1 = json_encode($html);
	exit;
}
?>
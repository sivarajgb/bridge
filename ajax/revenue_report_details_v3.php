<?php
// error_reporting(E_ALL); ini_set('display_errors', 1);
include("../config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();
$today=date('Y-m-d');

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enc_date = base64_encode($startdate);
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$vehicle = $_POST['vehicle_type'];
$city = $_POST['city'];

$_SESSION['crm_city'] = $city;

$enc_veh = base64_encode($vehicle);
if($vehicle=='all')
{
	$cond = "";
	$cond1 = "(g.vehicle_type = '2w' or g.vehicle_type = '4w')";
}
else
{
	$cond = "AND b.vehicle_type = '$vehicle'";
	$cond1 = "g.vehicle_type = '$vehicle'";
}
$cond2= $city == 'all' ? "" : "AND b.city ='$city'";

$total_veh = 0;
$total_credits =0;
$total_completed_s=0;
$total_goaxled =0;
$total_leads =0;
$total_revenue=0;
$totalbookings=array();
?>

<div align="center" id = "table" style="margin-left:20px;margin-right:20px;">
<table id="example1" class="table table-striped table-bordered tablesorter table-hover results">
<thead style="background-color: #D3D3D3;">
<?php if($master == "all"){ ?>
    <th style="text-align:center;">ServiceType</th>
<?php } 
else{ ?>
<th style="text-align:center;">Service Type</th>
<?php } ?>
<th style="text-align:center;">GoAxles <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Credits <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Completed <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Revenue <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
</thead>
<tbody id="tbody">

<?php 
	$sql_get_services_all = "SELECT service_type FROM go_axle_service_price_tbl";
	$res_get_services_all = mysqli_query($conn1,$sql_get_services_all);
	while($row_get_services_all = mysqli_fetch_array($res_get_services_all)){
		$service_type = $row_get_services_all['service_type'];
		$serviceData[$service_type]['revenue']=0;
		$serviceData[$service_type]['goaxle']=0;
		$serviceData[$service_type]['completed']=0;
		$serviceData[$service_type]['credits']=0;
	}
	$sql_goaxle="SELECT
	    DISTINCT bb.b2b_booking_id,
	    bb.gb_booking_id AS booking_id,
	    bb.b2b_shop_id,
	    bb.b2b_log,
	    m.b2b_address4,
	    b.shop_name,
	    
	    b.service_type,
	    b.vehicle_type,
	    bb.brand,
		g.model AS business_model,
	    g.lead_price,
	    g.re_lead_price,
	    g.nre_lead_price,
	    g.amount,
	    g.premium_tenure,
	    g.start_date,
	    g.end_date,
	    bb.b2b_credit_amt,
	    bb.b2b_check_in_report,
	    b.service_status,
	    m.b2b_new_flg,
	    c.b2b_partner_flag,
	    c.b2b_leads,
	    c.b2b_credits
	FROM
	    b2b.b2b_booking_tbl bb
	        LEFT JOIN
	    user_booking_tb b ON bb.gb_booking_id = b.booking_id
	        LEFT JOIN
		garage_model_history g ON g.b2b_shop_id = bb.b2b_shop_id
	        AND DATE(bb.b2b_log) >= DATE(g.start_date)
	        AND g.id=(select id from go_bumpr.garage_model_history where b2b_shop_id=bb.b2b_shop_id AND DATE(bb.b2b_log) >= DATE(start_date) order by start_date desc limit 1) 
	        LEFT JOIN
		b2b.b2b_status s ON s.b2b_booking_id = bb.b2b_booking_id
		    LEFT JOIN
		b2b.b2b_mec_tbl m ON m.b2b_shop_id=bb.b2b_shop_id
			LEFT JOIN
		b2b.b2b_credits_tbl c ON c.b2b_shop_id=bb.b2b_shop_id
	WHERE
	    (DATE(bb.b2b_log) BETWEEN '$startdate' and '$enddate')
	        AND bb.b2b_shop_id NOT IN (1014 , 1035, 1670, 1673)
	        AND b.user_id NOT IN (21816 , 41317,
	        859,
	        3132,
	        20666,
	        56511,
	        2792,
	        128,
	        19,
	        7176,
	        19470,
	        1,
	        951,
	        103699,
	        113453,
	        108783)
	        AND b.service_type NOT IN ('GoBumpr Tyre Fest')
	        {$cond2}
	        {$cond}
	        AND bb.b2b_swap_flag != 1
	        AND bb.b2b_flag != 1
	        AND s.b2b_acpt_flag = 1 ORDER BY bb.b2b_booking_id;";
	$res_goaxle = mysqli_query($conn1,$sql_goaxle);
	
	while($row_goaxle = mysqli_fetch_object($res_goaxle))
	{
		$log = date('Y-m-d', strtotime($row_goaxle->b2b_log));
		// echo $log;
		
		$model_flag=0;
		$model_start=$row_goaxle->start_date;
		$model_end=$row_goaxle->end_date;
		$veh_type=$row_goaxle->vehicle_type;
		$brand=$row_goaxle->brand;
		$price=$row_goaxle->price;
		$business_model = trim($row_goaxle->business_model);
		$revenue=0;
		$credit_amt=$row_goaxle->b2b_credit_amt;
		$lead_price = $row_goaxle->lead_price;
		$re_lead_price = $row_goaxle->re_lead_price;
		$nre_lead_price = $row_goaxle->nre_lead_price;
		// $exception_log=$row_goaxle->exception_log;
		// echo $exception_log;
		$leads=$row_goaxle->b2b_leads;
		$credits=$row_goaxle->b2b_credits;
		$locality = $row_goaxle->b2b_address4;
		$shop_name=$row_goaxle->shop_name;
		$service_type=$row_goaxle->service_type;
		$service_status=$row_goaxle->service_status;
		$check_in_report=$row_goaxle->b2b_check_in_report;
		$premium=$row_goaxle->b2b_partner_flag;
		$new_flag=$row_goaxle->b2b_new_flg;
		$shop_id=$row_goaxle->b2b_shop_id;
		$shopData[$shop_id]['model']=$business_model;
		$shopData[$shop_id]['lead_price']=$lead_price;
		$shopData[$shop_id]['re_lead_price']=$re_lead_price;
		$shopData[$shop_id]['nre_lead_price']=$nre_lead_price;
		$shopData[$shop_id]['type']=$veh_type;
		if($new_flag==1){
			
			$account="L".$leads;
		}else{
			$account=$credits;
		}
		if($business_model=="Pre Credit"){
		 	$credits = $row_goaxle->b2b_credit_amt;
			$revenue = $credits;
			$model_flag=0;
		}else if($business_model=="Premium 1.0"){
			$amount = $row_goaxle->amount;
			$premium_tenure = $row_goaxle->premium_tenure;
			$model_flag=1;
			$revenue=round(($amount/$premium_tenure)/25);
		}else if($business_model=="Premium 2.0"){
			if($veh_type=="4w"){
				$revenue = $lead_price;
			}else{
				if($brand=="Royal Enfield"){
					$revenue=$lead_price;
				}else{
					$revenue=$nre_lead_price;
				}
			}
			$model_flag=2;
		}else if($business_model=="Leads 3.0"){
						
			if($veh_type=="4w"){
				$revenue = $lead_price;
			}else{
				if($brand=="Royal Enfield"){
					$revenue=($re_lead_price!=0)?$re_lead_price:$lead_price;
				}else{
					$revenue=($nre_lead_price!=0)?$nre_lead_price:$lead_price;
				}
			}
			$model_flag=3;
		}else{
			$credits = $row_goaxle->b2b_credit_amt;
			$revenue = $credits;
			$model_flag=0;
			$shopData[$shop_id]['model']="Pre Credit";
		}
		$shopData[$shop_id]['revenue']+=round($revenue,2);
		$shopData[$shop_id]['credits']+=($credit_amt/100);
		// $shopData[$shop_id]['model']=$actual_model;
		$shopData[$shop_id]['account']=$account;
		$shopData[$shop_id]['shop_name']=$shop_name;
		$serviceData[$service_type]['revenue']+=round($revenue,2);
		$serviceData[$service_type]['credits']+=($credit_amt/100);
		if($service_status=="Completed" || $service_status=="In Progress" || $check_in_report==1){
			$serviceData[$service_type]['completed']+=1;
			$shopData[$shop_id]['completed']+=1;			
		}
		if(!array_key_exists('completed',$shopData[$shop_id])){
			$shopData[$shop_id]['completed']=0;
		}
		$serviceData[$service_type]['goaxle']+=1;
		$shopData[$shop_id]['goaxle']+=1;
		$shopData[$shop_id]['locality']=$locality;
		// var_dump($dateData);die;
	}
	foreach ($serviceData as $key => $value) { 
		if($value['goaxle']>0){ 
			$total_veh+=$value['goaxle'];
			$total_credits+=$value['credits'];
			$total_completed_s+=$value['completed'];
			$total_revenue+=$value['revenue'];			
		?>
		<tr>
            <td><?php echo $key ; ?></td>
            <td style="text-align:center;"><?php echo $value['goaxle'] ; ?></td>
            <td style="text-align:center;"><?php echo $value['credits'] ; ?></td>
            <td style="text-align:center;"><?php echo $value['completed'] ; ?></td>
            <td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" ></i> <?php echo $value['revenue'] ; ?></td>
        </tr>
	<?php } }
?>	
</tbody>
<tbody class="avoid-sort">
<tr>
<td style="text-align:center;">Total</td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_veh; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_credits; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_completed_s; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><i class="fa fa-inr" aria-hidden="true" ></i> <?php echo $total_revenue; ?></td>
</tr>
</tbody>
</table>
</div>
<div style="margin-left:20px;margin-right:20px;clear:both;">
<table id="example3" class="table table-striped table-bordered tablesorter table-hover results">
<thead style="background-color: #D3D3D3;">
<th style="text-align:center;">Service Center</th>
<th style="text-align:center;">Locality</th>
<th style="text-align:center;">Business Model</th>
<?php if($vehicle=="2w") { ?>
	<th style="text-align:center;">RE Lead Price <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
	<th style="text-align:center;">NRE Lead Price <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<?php }else{ ?>
	<th style="text-align:center;">Lead Price <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<?php } ?>
<th style="text-align:center;">Revenue per month<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Accepted <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Credits <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Completed <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">End Conv. <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Revenue <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
</thead>
<tbody id="tbody">
<?php
	$total_shop_accepted=0;
	$total_shop_credits=0;
	$total_shop_revenue=0;
	$total_shop_completed=0;
	foreach ($shopData as $key => $value) { 
		if($value['goaxle']>0){ 
			$total_shop_accepted+=$value['goaxle'];
			$total_shop_credits+=$value['credits'];
			$total_shop_revenue+=$value['revenue'];
			$total_shop_completed+=$value['completed'];
			switch ($value['model']) {
				case 'Pre Credit':
					$lead_price=0;
					$re_lead_price=0;
					$nre_lead_price=0;
					break;
				case "Premium 1.0":
					$lead_price=0;
					$re_lead_price=0;
					$nre_lead_price=0;
					break;
				case "Premium 2.0":
					$lead_price=$value['lead_price'];
					$re_lead_price=$value['lead_price'];
					$nre_lead_price=$value['nre_lead_price'];
					if($value['type']=="2w"){
						$lead_price=($re_lead_price+$nre_lead_price)/2;
					}
					break;
				case "Leads 3.0":
					$lead_price=$value['lead_price'];
					$re_lead_price=$value['re_lead_price'];
					$nre_lead_price=$value['nre_lead_price'];
					if($value['type']=="2w"){
						if($re_lead_price==0){
							$re_lead_price=$lead_price;
						}
						if($nre_lead_price==0){
							$nre_lead_price=$lead_price;
						}
						if($lead_price==0){
							if($re_lead_price==0){
								$lead_price=$nre_lead_price;
							}else if($nre_lead_price==0){
								$lead_price=$re_lead_price;
							}else{
								$lead_price=($re_lead_price+$nre_lead_price)/2;
							}
						}
					}
					break;
			}			
		?>
		<tr>
            <td><?php echo $value['shop_name'].' ('.$value['account'].')' ; ?></td>
            <td><?php echo $value['locality'] ; ?></td>
            <td><?php echo $value['model'] ; ?></td>
            <?php if($vehicle=="2w"){ ?>
            	<td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" ></i> <?php echo $re_lead_price ?></td>
            	<td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" ></i> <?php echo $nre_lead_price ; ?></td>
            <?php }else{ ?>
            	<td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" ></i> <?php echo $lead_price ; ?></td>

            <?php } if($value['model']=="Pre Credit"){ ?>
	            
	            <td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" ></i> 0</td>
            <?php }else if($value['model']=="Leads 3.0"){ ?>
            	
	            <td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" ></i> 0</td>
	        <?php }else if($value['model']=="Premium 2.0"){ ?>
            	
	            <td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" ></i> 0</td>
	        <?php }else if($value['model']=="Premium 1.0"){ ?>
	            
            	<td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" ></i> <?php echo ($value['revenue']/$value['goaxle'])*25 ; ?></td>
	       	<?php } ?>
            <td style="text-align:center;"><?php echo $value['goaxle'] ; ?></td>
            <td style="text-align:center;"><?php echo $value['credits'] ; ?></td>
            <td style="text-align:center;"><?php echo $value['completed'] ; ?></td>
            <td style="text-align:center;"><?php echo round(($value['completed']/$value['goaxle'])*100) ." %" ; ?></td>
            <td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" ></i> <?php echo $value['revenue'] ; ?></td>
        </tr>
	<?php } }
?>
</tbody>
<tbody class="avoid-sort">
<tr>
<td colspan="5" style="text-align:center;">Total</td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_shop_accepted; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_shop_credits; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_shop_completed; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo round(($total_shop_completed/$total_shop_accepted)*100).' %'; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><i class="fa fa-inr" aria-hidden="true" > <?php echo $total_shop_revenue; ?></td>
</tr>
</tbody>
</table>
</div>

<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example1").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[3,3],[0,0]]} );
    }
);
</script>
<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example3").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[4,4],[0,0]]} );
    }
);
</script>
<?php


?>
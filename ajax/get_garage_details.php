<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$flag = $_SESSION['flag'];
$super_flag = $_SESSION['super_flag'];

$src_crm = array('crm016', 'crm017', 'crm064', 'crm036', 'crm033', 'crm034', 'crm029');
$src_column = 0;
if (in_array($crm_log_id, $src_crm)) {
	$src_column = 1;
}

$city = $_GET['city'];

$_SESSION['crm_city'] = $city;

$shop_id = $_POST['garage_id'];

$query = "select * from go_bumpr.admin_mechanic_table as gm inner join b2b.b2b_mec_tbl as bm on gm.mec_id = bm.gobumpr_id left join b2b.b2b_credits_tbl as bc on bm.b2b_shop_id = bc.b2b_shop_id where gm.mec_id = $shop_id group by gm.mec_id";

mysqli_set_charset($conn, 'utf8');
$query_result = mysqli_query($conn, $query);

$row_data = mysqli_fetch_object($query_result);

$dataset1 .= '<table class="table table-bordered">
<tbody>
	<tr><th width="20px">Mec Id   </th><td>' . $row_data->mec_id . '</td></tr>
	<tr><th>Shop Name  </th><td>' . $row_data->shop_name . '</td></tr>
	<tr>
	<th>Vehicle Type   </th><td>';

if ($row_data->b2b_vehicle_type == "2w") {
	$dataset1 .= "2 Wheeler";
} elseif ($row_data->b2b_vehicle_type == "4w") {
	$dataset1 .= "4 Wheeler";
} else {
	$dataset1 .= $row_data->b2b_vehicle_type;
}

$dataset1 .= '</td></tr><tr><th>Address   </th><td>' . $row_data->address4 . ',' . $row_data->address5 . '</td></tr>
	<tr><th>Lat-lng   </th><td>' . $row_data->lat . ',' . $row_data->lng . '</td></tr>
	<tr><th>Brand   </th><td>' . $row_data->brand . '</td></tr>
	<tr><th>Weekly Counter   </th><td>' . $row_data->wkly_counter . '</td></tr>
	<tr><th>Credits   </th><td>' . $row_data->b2b_credits . '</td></tr>
	<tr><th>Leads   </th><td>' . $row_data->b2b_leads . '</td></tr>
	<tr><th>Re-Leads   </th><td>' . $row_data->b2b_re_leads . '</td></tr>
	<tr><th>Non-Re-Leads   </th><td>' . $row_data->b2b_non_re_leads . '</td></tr>
</tbody>
</table>';

$data['success'] = true;
$data['garageData'] = str_replace(array("\r", "\n"), "", $dataset1);

// Free result set
mysqli_free_result($query_result);

echo json_encode($data);
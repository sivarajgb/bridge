<?php
include("../config.php");
$conn = db_connect3();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];

$veh_type = $_POST['veh'];
$city = $_POST['city'];
$loc = $_POST['loc'];

$_SESSION['crm_city'] = $city;


//0-all && 1-particular
class shops {
  private $veh;
  private $loc;

  function __construct($veh, $loc){
    $this->veh = $veh;
    $this->loc = $loc;
  }
  function v0_l0(){
    return "";
  }
  function v0_l1(){
    return "AND m.b2b_address4 ='$this->loc'";
  }
  function v1_l0(){
    return "AND m.b2b_vehicle_type = '$this->veh'";
  }
  function v1_l1(){
    return "AND m.b2b_vehicle_type = '$this->veh' AND m.b2b_address4 ='$this->loc'";
  }
}

$shops_obj = new shops($veh_type, $loc);

$veh_val = $veh_type=="all" ? "0" : "1";
$loc_val = $loc=="" ? "0" : "1";

$cond = $shops_obj->{"v{$veh_val}_l{$loc_val}"}();

$sql_shops = "SELECT DISTINCT m.b2b_shop_id,m.b2b_shop_name,c.b2b_credits FROM b2b_mec_tbl as m INNER JOIN b2b_credits_tbl as c ON m.b2b_shop_id = c.b2b_shop_id WHERE m.b2b_cred_model=1 AND m.b2b_address5 ='$city' {$cond} ORDER BY m.b2b_shop_name ASC ";
$res_shops = mysqli_query($conn,$sql_shops);

$no = 0;

$count = mysqli_num_rows($res_shops);
if($count >0){
while($row_shops = mysqli_fetch_object($res_shops)){
$shop_id = $row_shops->b2b_shop_id;
$shop_name = $row_shops->b2b_shop_name;
$credits = $row_shops->b2b_credits;
  ?>
  <tr>
    <td><?php echo $no=$no+1 ; ?></td>
     <td><div class="rad"><input type="radio" name="radio_shop" id="<?php echo $shop_id;?>" onclick="get_data(<?php echo $shop_id; ?>);" value="<?php echo $shop_id; ?>"/><label for="<?php echo $shop_id;?>"><?php echo $shop_name; ?></label></div></td>
    <td><?php echo $credits; ?></td>
  </tr>
  <?php 
}
} // if
else {
  echo "<h3 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h3>";
}
 ?>

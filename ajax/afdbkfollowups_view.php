<?php
//error_reporting(E_ALL);
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$super_flag = $_SESSION['super_flag'];
$feedback_admin = $_SESSION['feedback_admin'];

$se_team = array('crm117','crm087','crm015','crm123');
$se_access = 0;
if(in_array($crm_log_id,$se_team))
{
	$se_access = 1;
}

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$person = $_GET['person'];
$vehicle = $_GET['vehicle'];
$service = $_GET['service'];
$service_status = $_GET['service_status'];
$shop_id = $_GET['shop_id'];
$city = $_GET['city'];
$service_priority=$_GET['service_priority'];
$_SESSION['crm_city'] = $city;
$source_type =$_GET['source_type'];

$cond ='';

$cond = $cond.($vehicle == 'all' ? "" : "AND f.veh_type='$vehicle'");
$cond = $cond.($service == 'all' ? "" : "AND f.service_type='$service'");
// $cond = $cond.($service_status == 'all' ? "AND f.feedback_status IN (2,3,11,12,21,22,31,41,51) " : $service_status == 'RNR' ? "AND f.feedback_status IN (11,12,21,22,31,41,51) " : $service_status == 'In Progress' ? "AND f.feedback_status = 3 " : $service_status == 'Yet to Service his Vehicle' ? "AND f.feedback_status = 2 " : "AND f.service_status='$service_status'");
if($service_status == 'all')
{
	$cond = $cond."AND f.feedback_status IN (2,3,11,12,21,22,31,41,51)";
}
// else if($service_status == 'In Progress')
// {
// 	$cond = $cond."AND f.feedback_status = 3 ";
// }
else if($service_status == 'Yet to Service his Vehicle')
{
	$cond = $cond."AND f.feedback_status = 2 ";
}
else if($service_status == 'RNR')
{
	$cond = $cond."AND f.feedback_status IN (11,12,21,22,31,41,51)";
}
// $cond = $cond.($service_status == 'all' ? "" : "AND f.service_status='$service_status'");
$cond = $cond.($shop_id == 'all' ? "" : "AND f.shop_id='$shop_id'");
$cond = $cond.($person == 'all' ? "" : ($person == 'no' ? " AND (f.crm_log_id='' OR f.crm_log_id IS NULL) " : "AND f.crm_log_id='$person'"));
$cond = $cond.($city == 'all' ? "" : "AND f.city='$city'");
$cond = $cond.($service_priority == 'all' ? "" : "AND f.svc_priority='$service_priority'");

if($source_type=='all'){
		$cond.="";
	}
	else if($source_type=='coreops'){
		$cond.=" and ub.source!='Re-Engagement Bookings'";
	}
	else if($source_type=='reng'){
		$cond.=" and ub.source='Re-Engagement Bookings'";
	}

$sql_goaxles = "SELECT f.booking_id,b.b2b_log,b.b2b_customer_name,b.b2b_cust_phone,f.shop_name,f.crm_log_id,f.feedback_status,b.brand,b.model,f.service_type,f.service_status,f.service_status_reason,DATE(f.feedback_followup) as feedback_followup,b.b2b_bill_amount,b.b2b_Rating,b.b2b_Feedback,b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_vehicle_ready,c.b2b_partner_flag,crm.name as crm_name,f.svc_priority FROM  b2b.b2b_booking_tbl AS b LEFT JOIN go_bumpr.feedback_track AS f ON b.b2b_booking_id = f.b2b_booking_id   LEFT JOIN b2b.b2b_credits_tbl c ON f.shop_id = c.b2b_shop_id LEFT JOIN crm_admin crm ON f.crm_log_id = crm.crm_log_id  join go_bumpr.user_booking_tb as ub on f.booking_id = ub.booking_id WHERE f.flag = 0 and f.service_status!='In Progress' {$cond} AND f.shop_id NOT IN (1014,1035,1670) AND DATE(b.b2b_log) BETWEEN '$startdate' AND '$enddate' ORDER BY b.b2b_log DESC";
//echo $sql_goaxles;
// $sql_goaxles = "SELECT f.booking_id,b.b2b_log,b.b2b_customer_name,b.b2b_cust_phone,f.shop_name,f.crm_log_id,f.feedback_status,b.brand,b.model,f.service_type,f.service_status,f.service_status_reason,DATE(f.feedback_followup) as feedback_followup,b.b2b_bill_amount,b.b2b_Rating,b.b2b_Feedback,b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_vehicle_ready,c.b2b_partner_flag,crm.name as crm_name FROM feedback_track f LEFT JOIN b2b_optimization.b2b_booking_tbl b ON b.b2b_booking_id = f.b2b_booking_id LEFT JOIN b2b_optimization.b2b_credits_tbl c ON f.shop_id = c.b2b_shop_id LEFT JOIN crm_admin crm ON f.crm_log_id = crm.crm_log_id WHERE f.feedback_status IN (2,3,11,12,21,22,31,41,51) AND f.flag = 0 {$cond} AND f.shop_id NOT IN (1014,1035,1670) AND DATE(f.feedback_followup) BETWEEN '$startdate' AND '$enddate' ORDER BY DATE(f.feedback_followup)";
// echo $sql_goaxles;
$res_goaxles = mysqli_query($conn,$sql_goaxles);
$no = 0;
$arr = array();
$count = mysqli_num_rows($res_goaxles);
if($count >0){
  while($row_goaxles = mysqli_fetch_object($res_goaxles)){
    $booking_id = $row_goaxles->booking_id;
    $goaxle_date = $row_goaxles->b2b_log;
    $name = $row_goaxles->b2b_customer_name;
    $mobile_number = $row_goaxles->b2b_cust_phone;
    $service_center = $row_goaxles->shop_name;
    $vehicle = $row_goaxles->brand." ".$row_goaxles->model;
	$service_type = $row_goaxles->service_type;
	$feedback_status = $row_goaxles->feedback_status;
	$service_status = $row_goaxles->service_status;
	if(($service_status == '')&&($feedback_status > 10))
	{
		$service_status = 'RNR';
	}
	$service_status_reason = $row_goaxles->service_status_reason;
	if($row_goaxles->feedback_followup == '0000-00-00'){ $feedback_followup_date = 'Not Updated'; } else { $feedback_followup_date = date('d-m-Y', strtotime($row_goaxles->feedback_followup));};
	$garage_bill = $row_goaxles->b2b_bill_amount;
	$cust_rating = $row_goaxles->b2b_Rating;
	$cust_feedback = $row_goaxles->b2b_Feedback;
	$inspection_stage = $row_goaxles->b2b_check_in_report;
	$estimate_stage = $row_goaxles->b2b_vehicle_at_garage;
	$deliver_stage = $row_goaxles->b2b_vehicle_ready;
	$alloted_to = $row_goaxles->crm_name;
	$premium = $row_goaxles->b2b_partner_flag;
	$alloted_to_id = $row_goaxles->crm_log_id;
	$service_priority = $row_goaxles->svc_priority;
	$no = $no+1;
    if($alloted_to_id =='' || $alloted_to_id==' ' || $alloted_to_id=='0'){
		$tr = '<tr style="background-color:#EF5350 !important;">';
	}
	else{
		$tr = '<tr>';
	}
		if($se_access==1)
		  {
			$td1 = '';
		  }
		  else
		  {
			$td1 = '<td style="vertical-align: middle;"><input  type="checkbox" id="check_veh" name="check_veh[]" value="'.$booking_id.'" /> </td>';
		  }
          
          $td2 = '<td style="vertical-align: middle;">'.$no.'</td>';
          $td2 = $td2.'<td><p style="vertical-align: middle; padding-top:40px">'.$booking_id.'</p>';
					switch($service_priority){
						case '1': $td2 = $td2.'<p style="font-size:16px;color:#00ACC1;float:left;padding:5px;" title="Low Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';break;
						case '2':$td2 = $td2.'<p style="font-size:16px;color:#FFB300;float:left;padding:5px;" title="Medium Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';break;
						case '3':$td2 = $td2.'<p style="font-size:16px;color:#E53935;float:left;padding:5px;" title="High Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';break;
					}
					$td2=$td2.'</td>';
          $td3 = '<td style="vertical-align: middle;">'.date('d M Y', strtotime($goaxle_date)).'</td>';
          $td4 = '';
		  $st_enc = base64_encode($startdate);
		  $ed_enc = base64_encode($enddate);
		  $dt_enc = $st_enc.':'.$ed_enc;
       if($se_access==1)
		  {
			$td5 = '';
		  }
		  else
		  {
			$td5 = '<td style="vertical-align: middle;"><a href="feedback_details.php?bi='.base64_encode($booking_id).'&pg='.base64_encode('f').'&dt='.base64_encode($dt_enc).'"><i id="'.$booking_id.'" class="fa fa-eye" aria-hidden="true"></i></td>';
		  }
      
          $td6 = '<td style="vertical-align: middle;">'.$name.'</td>';
          if($se_access==1)
		  {
			$td7 = '';
		  }
		  else
		  {
			$td7 = '<td style="vertical-align: middle;">'.$mobile_number.'</td>';
		  }
          $td8 = '<td style="vertical-align: middle;">'.$service_center;
		  if ($premium == 2) {  
			  $td8 = $td8."<img src='images/authorized.png' style='width:22px;' title='Premium Garage'>";
		  }
		  $td8 = $td8.'</td>';
		  $td9 = '<td style="vertical-align: middle;">'.$vehicle.'</td>';	
		  $td10 = '<td style="vertical-align: middle;">'.$service_type.'</td>';	
		  $td11 = '<td style="vertical-align: middle;">'.$alloted_to.'</td>';	
		  $td11 = $td11.'<td style="vertical-align: middle;">'.$service_status.'</td>';	
		  $td11 = $td11.'<td style="vertical-align: middle;">'.$service_status_reason;
		  if (strpos($service_status_reason, 'Rescheduled') !== false) { 
			  $td11 = $td11."<br>- ".$feedback_followup_date;
		  }
		  $td11 = $td11.'</td>';
		  $td11 = $td11.'<td style="vertical-align: middle;">'.$feedback_followup_date.'</td>';
		  $td12 = '<td style="vertical-align: middle;">';
		  $garage_bill!='' ? $td12 = $td12.'<i class="fa fa-inr" aria-hidden="true" style="padding-right: 2px;padding-left: 1px;"></i>'.$garage_bill : $td12 = $td12.'Not updated';
		  $td12 = $td12.'</td>';
		  $td13 = '<td style="vertical-align: middle;">';
		  $cust_rating!='' ? $td13 = $td13.$cust_rating : $td13 = $td13.'Not updated';
		  $td13 = $td13.'</td>';
		  $td14 = '<td style="vertical-align: middle;">';
		  $cust_feedback!='' ? $td14 = $td14.$cust_feedback : $td14 = $td14.'Not updated';
		  $td14 = $td14.'</td>';
		  $td15 = "<td style='vertical-align: middle;'>";
		  $inspection_stage == '1' ? $td15 = $td15."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Done!'></i>" : $td15 = $td15."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Not updated!'></i>";
		  $td15 = $td15."</td>";
		  $td16 = "<td style='vertical-align: middle;'>";
		  $estimate_stage == '1' ? $td16 = $td16."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Done!'></i>" : $td16 = $td16."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Not updated!'></i>";
		  $td16 = $td16."</td>";
		  $td17 = "<td style='vertical-align: middle;'>";
		  $deliver_stage == '1' ? $td17 = $td17."<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Done!'></i>" : $td17 = $td17."<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Not updated!'></i>";
		  $td17 = $td17."</td>";
          
			
			
		  $tr_l = '</tr>';
      
          $str = $tr.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$td9.$td10.$td11.$td12.$td13.$td14.$td15.$td16.$td17.$tr_l;
          $data[] = array('tr'=>$str);
            
        }
       
        echo $data1 = json_encode($data);
} // if
else {
  echo "no";
}


?>

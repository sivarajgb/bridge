<?php
error_reporting(E_ALL); ini_set('display_errors', 1);
include("../config.php");
$conn = db_connect1(); 
session_start();


$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$city = $_GET['city'];
$shop_id = $_GET['shop_id'];
$master_service = $_GET['master_service'];
$service = $_GET['service'];
$vehicle = $_GET['vehicle'];

$_SESSION['crm_city'] = $city;
$source_type =$_GET['source_type'];

function run_query($conn, $query){
	$count_no_goaxles = 0;
	$count_no_others = 0;
	$count_no_completed = 0;
	$count_no_cancelled = 0;
	$query_result = mysqli_query($conn,$query);
	while($query_rows = mysqli_fetch_object($query_result))
	{
		$booking_id = $query_rows->booking_id;
		$service_status = $query_rows->service_status;
		$feedback_status = $query_rows->feedback_status;
		$b2b_acpt_flag = $query_rows->b2b_acpt_flag;
		$b2b_deny_flag = $query_rows->b2b_deny_flag;


		if($feedback_status == 0 && $service_status == '' && $b2b_acpt_flag==1 && $b2b_deny_flag==0)
		{	
			$count_no_goaxles = $count_no_goaxles + 1;
		}
		else if($feedback_status == 0 && $service_status != '')
		{
			$count_no_others = $count_no_others + 1;
		}
		else if($feedback_status == 1 || $service_status == 'Completed')
		{
			$count_no_completed = $count_no_completed + 1;
		}
		else if($feedback_status == 5 && $service_status == 'Cancelled')
		{
			$count_no_cancelled = $count_no_cancelled + 1;
		}
	}
	$rtn['count_no_goaxles'] = $count_no_goaxles;
	$rtn['count_no_others'] = $count_no_others;
	$rtn['count_no_completed'] = $count_no_completed;
	$rtn['count_no_cancelled'] = $count_no_cancelled;
	return $rtn;
}


function run_query2($conn, $query){

	//echo $query;

	$count_no_followups = 0;
	$count_no_reservics = 0;
	$query_result = mysqli_query($conn,$query);
	while($query_rows = mysqli_fetch_object($query_result))
	{
		$service_status = $query_rows->service_status;
		$feedback_status = $query_rows->feedback_status;
		$reservice_flag = $query_rows->reservice_flag;

		if($feedback_status == 2  || $feedback_status > 10)
		{	
			$count_no_followups = $count_no_followups + 1;
		}
		else if($feedback_status == 1 && $service_status == 'Completed' && $reservice_flag == 1)
		{
			$count_no_reservics = $count_no_reservics + 1;
		}
	}
	$rtn['count_no_followups'] = $count_no_followups;
	$rtn['count_no_reservics'] = $count_no_reservics;
	return $rtn;
}


$cond ='';
 
$cond = $cond.($city == 'all' ? "" : "AND f.city='$city'");
$cond = $cond.($vehicle == 'all' ? "" : "AND f.veh_type='$vehicle'");
$cond = $cond.($shop_id == 'all' ? "" : "AND f.shop_id='$shop_id'");

if($master_service != "all" && $service == "all"){
    $conn1 = db_connect1();
    $sql_serv = "SELECT service_type FROM go_axle_service_price_tbl WHERE bridge_flag='0' AND master_service='$master_service'";
    $res_serv = mysqli_query($conn1,$sql_serv);
    $services = '';
    while($row_serv = mysqli_fetch_array($res_serv)){
        if($services == ""){
            $services = $row_serv['service_type']."'";
        }
        else{
            $services = $services.",'".$row_serv['service_type']."'";
        }
    }
    $cond = $cond." AND f.service_type IN ('$services)";
}
$cond = $cond.($service != 'all' ?  " AND f.service_type='$service'" : '' ) ;

if($source_type=='all'){
		$cond.="";
	}
	else if($source_type=='coreops'){
		$cond.=" and ub.source!='Re-Engagement Bookings'";
	}
	else if($source_type=='reng'){
		$cond.=" and ub.source='Re-Engagement Bookings'";
	}

$select_sql = "SELECT f.booking_id,f.service_status,f.feedback_status,s.b2b_acpt_flag,s.b2b_deny_flag,f.reservice_flag  FROM  b2b.b2b_booking_tbl AS b LEFT JOIN
    go_bumpr.feedback_track AS f ON b.b2b_booking_id = f.b2b_booking_id LEFT JOIN        
    b2b.b2b_status s ON f.b2b_booking_id = s.b2b_booking_id JOIN go_bumpr.user_booking_tb AS ub ON f.booking_id = ub.booking_id WHERE s.b2b_acpt_flag = '1' AND b.b2b_swap_flag = 0 AND b.gb_booking_id != '0' {$cond} AND f.shop_id NOT IN (1014,1035,1670)";
 //echo $select_sql;
$count_arr = run_query($conn, "{$select_sql} AND DATE(b.b2b_log) BETWEEN '$startdate' AND '$enddate'");


$selct_sql2="SELECT f.booking_id,f.feedback_status,f.service_status,f.reservice_flag from b2b.b2b_booking_tbl AS b LEFT JOIN go_bumpr.feedback_track AS f ON b.b2b_booking_id = f.b2b_booking_id JOIN go_bumpr.user_booking_tb AS ub ON f.booking_id = ub.booking_id where f.flag!=1 and f.service_status!='In Progress' {$cond} AND f.shop_id NOT IN (1014,1035,1670) ";
$count_arr2=run_query2($conn, "{$selct_sql2} AND DATE(b.b2b_log) BETWEEN '$startdate' AND '$enddate'");
//echo $selct_sql2;
 echo $data = json_encode(array('goaxlesCount'=>$count_arr['count_no_goaxles'], 'completedCount'=>$count_arr['count_no_completed'], 'othersCount'=>$count_arr['count_no_others'],'cancelledCount'=>$count_arr['count_no_cancelled'],'reservicecount'=>$count_arr2['count_no_reservics'], 'followupsCount'=>$count_arr2['count_no_followups']));
//echo $select_sql;
?>


<?php
include("../config.php");
$conn = db_connect1();
session_start();

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$flag = $_SESSION['flag'];

$startdate = date('Y-m-d', strtotime($_GET['startdate']));
$enddate = date('Y-m-d', strtotime($_GET['enddate']));
$source = $_GET['source'];
$city = $_GET['city'];
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];


$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;

$cond = '';

$cond = $cond . ($source == 'all' ? "" : "AND b.source='$source'");
$cond = $cond . ($city == 'all' ? "" : "AND b.city='$city'");
$cond = $cond . ($cluster == 'all' ? "" : "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%" . $cluster . "%'");

$sql_booking = "SELECT DISTINCT b.booking_id,b.user_id,b.log,b.source,b.vehicle_type,b.vech_id,b.user_veh_id,b.service_type,b.service_date,b.shop_name,b.crm_update_id,b.locality,b.utm_source,b.priority,b.flag_fo,b.user_pmt_flg,c.name as crm_name,u.name as user_name,u.mobile_number as user_mobile,u.lat_lng as user_lat_lng,u.Locality_Home as user_locality,u.user_level,v.brand,v.model FROM user_booking_tb  as b LEFT JOIN crm_admin as c ON b.crm_update_id=c.crm_log_id LEFT JOIN user_register as u ON b.user_id=u.reg_id LEFT JOIN user_vehicle_table as v ON b.user_veh_id=v.id LEFT JOIN localities as l ON b.locality=l.localities WHERE  b.crm_update_id = '$crm_log_id' AND b.flag='1' AND DATE(b.crm_update_time) BETWEEN '$startdate' and '$enddate' {$cond} ORDER BY b.crm_update_time DESC";
if ($crm_log_id == 'crm012') {
	$sql_booking = "SELECT DISTINCT b.booking_id,b.user_id,b.log,b.source,b.vehicle_type,b.vech_id,b.user_veh_id,b.service_type,b.service_date,b.shop_name,b.crm_update_id,b.locality,b.utm_source,b.priority,b.flag_fo,b.user_pmt_flg,c.name as crm_name,u.name as user_name,u.mobile_number as user_mobile,u.lat_lng as user_lat_lng,u.Locality_Home as user_locality,u.user_level,v.brand,v.model FROM user_booking_tb  as b LEFT JOIN crm_admin as c ON b.crm_update_id=c.crm_log_id LEFT JOIN user_register as u ON b.user_id=u.reg_id LEFT JOIN user_vehicle_table as v ON b.user_veh_id=v.id LEFT JOIN localities as l ON b.locality=l.localities WHERE  (b.crm_update_id = '$crm_log_id' OR b.crm_update_id IN ('crm012','crm055','crm041')) AND b.flag='1' AND DATE(b.crm_update_time) BETWEEN '$startdate' and '$enddate' {$cond} ORDER BY b.crm_update_time DESC";
}
$res_booking = mysqli_query($conn, $sql_booking);

$no = 0;

$count = mysqli_num_rows($res_booking);
if ($count > 0) {
	while ($row_booking = mysqli_fetch_object($res_booking)) {
		$booking_id = $row_booking->booking_id;
		$user_id = $row_booking->user_id;
		$log = $row_booking->log;
		$bsource = $row_booking->source;
		$veh_type = $row_booking->vehicle_type;
		$veh_id = $row_booking->user_veh_id;
		$service_type = $row_booking->service_type;
		$service_date = $row_booking->service_date;
		$shop_name = $row_booking->shop_name;
		$locality = $row_booking->locality;
		$utm_source = $row_booking->utm_source;
		$priority = $row_booking->priority;
		$flag_fo = $row_booking->flag_fo;
		$payment_flag = $row_booking->user_pmt_flg;
		$alloted_to_id = $row_booking->crm_update_id;
		$alloted_to = $row_booking->crm_name;
		$user_name = $row_booking->user_name;
		$user_mobile = $row_booking->user_mobile;
		$address = $row_booking->user_lat_lng;
		$home = $row_booking->user_locality;
		$user_level = $row_booking->user_level;
		$veh_brand = $row_booking->brand;
		$veh_model = $row_booking->model;
		
		
		$tr = '<tr>';
		$no = $no + 1;
		$td1 = '<td>' . $no . '</td>';
		$td2 = '<td><p style="float:left;padding:10px;">' . $booking_id . '</p>';
		if ($flag_fo == '1') {
			$td2 = $td2 . '<p class="p" style="font-size:16px;color:#D81B60;float:left;padding:5px;" title="Resheduled Booking!"><i class="fa fa-recycle" aria-hidden="true"></i></p>';
			switch ($priority) {
				case '1':
					$td2 = $td2 . '<p style="font-size:16px;color:#00ACC1;float:left;padding:5px;" title="Low Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';
					break;
				case '2':
					$td2 = $td2 . '<p style="font-size:16px;color:#FFB300;float:left;padding:5px;" title="Medium Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';
					break;
				case '3':
					$td2 = $td2 . '<p style="font-size:16px;color:#E53935;float:left;padding:5px;" title="High Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';
					break;
			}
		}
		if ($payment_flag == '1') {
			$td2 = $td2 . '<p style="font-size:16px;color:#1c57af;float:left;padding:5px;" title="Paid!"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></p>';
		}
		$td2 = $td2 . '</td>';
		$td3 = '<td>';
		
		switch ($service_type) {
			case 'general_service' :
				if ($veh_type == '2w') {
					$td3 = $td3 . "General Service";
				} else {
					$td3 = $td3 . "Car service and repair";
				}
				break;
			case 'break_down':
				$td3 = $td3 . "Breakdown Assistance";
				break;
			case 'tyre_puncher':
				$td3 = $td3 . "Tyre Puncture";
				break;
			case 'other_service':
				$td3 = $td3 . "Repair Job";
				break;
			case 'car_wash':
				if ($veh_type == '2w') {
					$td3 = $td3 . "water Wash";
				} else {
					$td3 = $td3 . "Car wash exterior";
				}
				break;
			case 'engine_oil':
				$td3 = $td3 . "Repair Job";
				break;
			case 'free_service':
				$td3 = $td3 . "General Service";
				break;
			case 'car_wash_both':
			case 'completecarspa':
				$td3 = $td3 . "Complete Car Spa";
				break;
			case 'car_wash_ext':
				$td3 = $td3 . "Car wash exterior";
				break;
			case 'car_wash_int':
				$td3 = $td3 . "Interior Detailing";
				break;
			case 'Doorstep car spa':
				$td3 = $td3 . "Doorstep Car Spa";
				break;
			case 'Doorstep_car_wash':
				$td3 = $td3 . "Doorstep Car Wash";
				break;
			case 'free_diagnastic':
			case 'free_diagnostic':
			case 'diagnostics':
				if ($veh_type == '2w') {
					$td3 = $td3 . "Diagnostics/Check-up";
				} else {
					$td3 = $td3 . "Vehicle Diagnostics";
				}
				break;
			case 'water_wash':
				$td3 = $td3 . "water Wash";
				break;
			case 'exteriorfoamwash':
				$td3 = $td3 . "Car wash exterior";
				break;
			case 'interiordetailing':
				$td3 = $td3 . "Interior Detailing";
				break;
			case 'rubbingpolish':
				$td3 = $td3 . "Car Polish";
				break;
			case 'underchassisrustcoating':
				$td3 = $td3 . "Underchassis Rust Coating";
				break;
			case 'headlamprestoration':
				$td3 = $td3 . "Headlamp Restoration";
				break;
			default:
				$td3 = $td3 . $service_type;
		}
		$td3 = $td3 . '</td>';
		
		$td4 = '<td><a href="user_details.php?t=' . base64_encode("c") . '&v58i4=' . base64_encode($veh_id) . '&bi=' . base64_encode($booking_id) . '"><i class="fa fa-eye" aria-hidden="true"></i></td>';
		$td5 = '<td>' . $user_name;
		switch ($user_level) {
			case '5':
				$td5 = $td5 . '<p style="font-size:16px;color:green;padding:5px;" title="Level 5 User"><i class="fa fa-angellist" aria-hidden="true"></i></p>';
				break;
			case '4':
				$td5 = $td5 . '<p style="font-size:16px;color:red;padding:5px;" title="Level 4 User"><i class="fa fa-bug" aria-hidden="true"></i></p>';
				break;
			case '3':
				$td5 = $td5 . '<p style="font-size:16px;color:#26269e;padding:5px;" title="Level 3 User"><i class="fa fa-handshake-o" aria-hidden="true"></i></p>';
				break;
			case '2':
				$td5 = $td5 . '<p style="font-size:16px;color:brown;padding:5px;" title="Level 2 User"><i class="fa fa-anchor" aria-hidden="true"></i></p>';
				break;
			case '1':
				$td5 = $td5 . '<p style="font-size:16px;color:orange;padding:5px;" title="Level 1 User"><i class="fa fa-tripadvisor" aria-hidden="true"></i></p>';
				break;
			default:
		}
		$td5 = $td5 . '</td>';
		if (!$_SESSION['eupraxia_flag']) {
			$td6 = '<td>' . $user_mobile . '</td>';
		}
		$td7 = '<td>';
		if ($bsource == "Hub Booking") {
			if ($locality != '') {
				$td7 = $td7 . $locality;
			} else if ($home == '') {
				$td7 = $td7 . $address;
			} else {
				$td7 = $td7 . $home;
			}
		} else if ($bsource == "GoBumpr App" || $bsource == "App") {
			$td7 = $td7 . $locality;
		} else {
			$td7 = $td7 . $locality;
		}
		$td7 = $td7 . '</td>';
		$td8 = '<td>' . $veh_brand . " " . $veh_model . '</td>';
		$td9 = '<td>' . $shop_name . '</td>';
		$td10 = '<td>';
		if ($service_date == "0000-00-00") {
			$td10 = $td10 . "Not Updated Yet";
		} else {
			$td10 = $td10 . date('d M Y', strtotime($service_date));
		}
		$td10 = $td10 . '</td>';
		if ($crm_log_id == 'crm012') {
			$td10 = $td10 . "<td>" . $alloted_to . "</td>";
		}
		$td11 = '<td>' . $bsource . '</td>';
		$td12 = '<td>';
		if ($utm_source == '') {
			$td12 = $td12 . "NA";
		} else {
			$td12 = $td12 . $utm_source;
		}
		$td12 = $td12 . '</td>';
		$td13 = '<td>' . date('d M Y h.i A', strtotime($log)) . '</td>';
		$tr_l = '</tr>';
		
		if ($_SESSION['eupraxia_flag']) {
			$str = $tr . $td1 . $td2 . $td3 . $td4 . $td5 . $td7 . $td8 . $td9 . $td10 . $td11 . $td12 . $td13 . $tr_l;
		} else {
			$str = $tr . $td1 . $td2 . $td3 . $td4 . $td5 . $td6 . $td7 . $td8 . $td9 . $td10 . $td11 . $td12 . $td13 . $tr_l;
		}
		$data[] = array('tr' => $str);
	}
	echo $data1 = json_encode($data);
} // if
else {
	echo "no";
}
?>

<?php
include("../config.php");
$conn = db_connect1();
session_start();
date_default_timezone_set('Asia/Kolkata');
// $crm_log_id = $_SESSION['crm_log_id'] ;
// $crm_name = $_SESSION['crm_name'];
// $flag=$_SESSION['flag'];
$today = date("Y-m-d");
$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$city =  $_POST['city'];

// $city = $_POST['city'];  
// $_SESSION['crm_city'] = $city;
$no = 0;
?>


  <div id="div1" style="width:100%;float:left;" align="center">
    <table class="table table-striped table-bordered tablesorter table-hover results" id="table" style="width: 80%;font-size:12px;">
    <thead style="background-color:#B2DFDB;align:center;display: table-header-group;">
    <th>Name <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Mobile Number <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Vehicle<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Subscription ID<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>User Register Log<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Pass Purchase Log<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Valid Till<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>

    </thead>
    <tbody id="tbody">

    <?php
  $cond = '';
  $cond = $cond. ($city == 'all' ? "": " AND f.city='$city' ");
  // $cond = $cond.($veh == "all" ? "" : "AND m.type='$veh'");
  // $cond = $cond.($shop_status == "active" ? "AND b2b_m.b2b_avail='1'" : "AND b2b_m.b2b_avail='0'");
    $sql = "SELECT DISTINCT f.user_id,r.name,r.mobile_number,v.brand,v.model,f.freedom_pass_code,f.log as start_date,r.log as register_log FROM freedom_pass_booking f LEFT JOIN user_register r ON r.reg_id = f.user_id LEFT JOIN user_vehicle_table v ON v.id = f.user_veh_id WHERE DATE(f.log) BETWEEN '$startdate' and '$enddate' and f.flag=0 {$cond};";
    $res = mysqli_query($conn,$sql);

      // $list_credits = array();
     $num = mysqli_num_rows($res);
    if($num>0)
    {
    while($row = mysqli_fetch_object($res))
    {
        $name = $row->name;
        $mobile_number = $row->mobile_number;
        $brand = $row->brand;
        $model = $row->model;
		$vehicle = $brand.' '.$model;
        $freedom_pass_code = $row->freedom_pass_code;
        $start_date = $row->start_date;
        $user_register_log = $row->register_log;
		$valid_till = date('d-M-Y', strtotime('+ 1 year', strtotime($start_date)));
        ?>
        <tr>
		
      <td><?php echo $name;?></td>
      <td><?php echo $mobile_number;?></td>
      <td><?php echo $vehicle;?></td>
      <td><?php echo $freedom_pass_code;?></td>
      <td><?php echo date('d M Y H:i a', strtotime($user_register_log));?></td>
      <td><?php echo date('d M Y H:i a', strtotime($start_date));?></td>
      <td><?php echo $valid_till;?></td>

        </tr>
    
    <?php
}
}
else{
  ?>  
      <th colspan="7" style="font-size: 14px;text-align:center;padding-top: 40px;">No results found...! </th>
 <?php
}
?>

    </tbody>
    </table>
  </div>
<!-- search bar  -->
<script>
$(document).ready(function() {
  var no = "<?php echo $no; ?>";

if(no!='0')
{
$('.counter').text(no + ' shops');
}
  $(".search").keyup(function () {
    var searchTerm = $(".search").val();
    var listItem = $('.results tbody').children('tr');
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

  $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });

  $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','false');
  });

  $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','true');
  });
  var jobCount = $('.results tbody tr[visible="true"]').length;
    $('.counter').text(jobCount + ' shops');

  if(jobCount == '0') {$('.no-result').show();}
    else {$('.no-result').hide();
  }
 });
});
</script>


<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#table").tablesorter( {sortList: [[0,0], [1,0]]} );
    }
);
</script>
<?php
include("../config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();
$today=date('Y-m-d');

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

 $startdate = date('Y-m-d',strtotime($_POST['startdate']));
 $enddate =  date('Y-m-d',strtotime($_POST['enddate']));
 $person = $_POST['person'];
 $service = $_POST['service_type'];
 $vehicle = $_POST['vehicle_type'];


//0-all && 1-particular
class bookings {
  private $vehicle;
  private $person;
  private $service;

  function __construct($vehicle, $person, $service){
    $this->vehicle = $vehicle;
    $this->person = $person;
    $this->service = $service;
  }
  function v0_p0_s0(){
    return "";
  }
  function v0_p0_s1(){
    return "AND b.b2b_service_type='$this->service'";
  }
  function v0_p1_s0(){
    return "AND g.crm_update_id='$this->person'";
  }
  function v0_p1_s1(){
    return "AND g.crm_update_id='$this->person' AND b.b2b_service_type='$this->service'";
  }
  function v1_p0_s0(){
    return "AND g.vehicle_type='$this->vehicle'";
  }
  function v1_p0_s1(){
    return "AND g.vehicle_type='$this->vehicle' AND b.b2b_service_type='$this->service'";
  }
  function v1_p1_s0(){
    return "AND b.b2b_vehicle_type='$this->vehicle' AND g.crm_update_id='$this->person'";
  }
  function v1_p1_s1(){
    return "AND b.b2b_vehicle_type='$this->vehicle' AND g.crm_update_id='$this->person' AND b.b2b_service_type='$this->service'";
  }
}

$bookings_obj = new bookings($vehicle, $person, $service);

$person_val = $person=='all' ? "0" : "1";
$vehicle_val = $vehicle=='all' ? "0" : "1";
$service_val = $service=='all' ? "0" : "1";
$cond = $bookings_obj->{"v{$vehicle_val}_p{$person_val}_s{$service_val}"}();

$sql_booking = "SELECT b.b2b_booking_id,b.gb_booking_id,b.b2b_shop_id,b.b2b_customer_name,b.b2b_cust_phone,b.brand,b.model,b.b2b_service_type,b.b2b_service_date,b.b2b_credit_amt,b.b2b_log,g.shop_name,g.vehicle_type,g.locality,g.source,g.utm_source,g.utm_medium,g.crm_update_id,g.service_status,g.final_bill_amt,g.rating,g.get_rltime_updte,g.followup_date FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_swap_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_shop_id NOT IN (1014,1035) {$cond} ORDER BY b.b2b_log ASC";
$res_booking = mysqli_query($conn2,$sql_booking);

$no = 0;

$count = mysqli_num_rows($res_booking);
if($count >=1){
?>
<div align="center" id = "table" style="max-width:95%; margin-top:110px;margin-left:10px;margin-right:10px;">
<table id="example" class="table table-striped table-bordered tablesorter table-hover results" style="max-width:95%;">
 <thead style="background-color: #D3D3D3;">
  <th>No</th>
  <th>TimeStamp</th>
  <th>BookingId</th>
  <th>CustomerName</th>
  <th>Mobile</th>
  <th>ShopName</th>
  <th>Vehicle</th>
  <th>ServiceType</th>
  <th>Credits</th>
  <th>Status</th>
  <th>Amount</th>
  <th>Locality</th>
  <th>BookingSource</th>
  <th>UTMSource</th>
  <th>LeadOwner</th>
  <th>ServiceDate</th>
  </thead>
  <tbody id="tbody">

<?php

while($row_booking = mysqli_fetch_object($res_booking)){
$b2b_booking_id = $row_booking->b2b_booking_id;
$booking_id = $row_booking->gb_booking_id;
$shop_id = $row_booking->b2b_shop_id;
$user_name = $row_booking->b2b_customer_name;
$user_mobile = $row_booking->b2b_cust_phone;
$brand = $row_booking->brand;
$model = $row_booking->model;
$service_type = $row_booking->b2b_service_type;
$service_date = $row_booking->b2b_service_date;
$credit_amount = $row_booking->b2b_credit_amt;
$log = $row_booking->b2b_log;
$shop_name=$row_booking->shop_name;
$vehicle_type = $row_booking->vehicle_type;
$locality = $row_booking->locality;
$booking_source = $row_booking->source;
$utm_source = $row_booking->utm_source;
$utm_medium = $row_booking->utm_medium;
$alloted_to_id = $row_booking->crm_update_id;
$service_status = $row_booking->service_status;
$final_bill = $row_booking->final_bill_amt;
$rating = $row_booking->rating;
$realtime_updates = $row_booking->get_rltime_updte;
$next_service_date = $row_booking->followup_date;

// get crm person name from crm admin table
$sql_alloted_person = "SELECT crm_log_id,name FROM crm_admin WHERE crm_log_id='$alloted_to_id' ";
$query_alloted_person = mysqli_query($conn1,$sql_alloted_person);
$row_alloted_person = mysqli_fetch_object($query_alloted_person);
$alloted_to = $row_alloted_person->name;

?>

  <tr>
    <td><?php echo $no=$no+1 ; ?></td>
    <td><?php echo date('d M Y h.i A', strtotime($log)); ?></td>
    <td><p style="float:left;padding:10px;"><?php echo $booking_id; ?></p></td>
    <td><?php echo $user_name; ?></td>
    <td><?php echo $user_mobile; ?></td>
    <td><?php echo $shop_name; ?></td>
    <td><?php echo $brand." ".$model; ?></td>
    <td><?php echo $service_type; ?></td>
    <td><?php echo $credit_amount/100; ?></td>
    
    <td><?php
    if($service_status != '' ){  echo $service_status; } ?><?php if($service_status !='Completed' ){ ?><button class="btn btn-sm" style="background-color:#9fe88d;" data-toggle="modal" data-target="#myModal<?php echo $booking_id; ?>" title="Edit his Service Status!"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Edit</button><?php } ?>
    <!-- Modal -->
    <div class="modal fade" id="myModal<?php echo $booking_id; ?>" role="dialog" >
      <div class="modal-dialog" align="center">
        <!-- Modal content-->
        <div class="modal-content"  style="width:450px;">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3 class="modal-title" align="left">Booking Id (<?php echo $booking_id; ?>)</h3>
          </div>
          <div class="modal-body">
            <form id="feedback<?php echo $booking_id; ?>" class="form" method="post" action="feedback.php">
              <div class="row">
                <div class="col-xs-8 col-lg-offset-2 form-group">
                <br>
                <select class="form-control" id="status<?php echo $booking_id; ?>" name="status" required>
                  <?php 
                  switch($service_status){
                    case 'In Progress':?>
                  <option value="In Progress" selected>In Progress</option>
                  <option value="Completed" >Completed</option>
                  <option value="Yet to Service his Vehicle" >Yet to Service his Vehicle</option><?php break;
                    case 'Completed':?>
                  <option value="In Progress" >In Progress</option>
                  <option value="Completed" selected>Completed</option>
                  <option value="Yet to Service his Vehicle" >Yet to Service his Vehicle</option><?php break;
                    case 'Yet to Service his Vehicle':?>
                  <option value="In Progress" >In Progress</option>
                  <option value="Completed" >Completed</option>
                  <option value="Yet to Service his Vehicle" selected>Yet to Service his Vehicle</option><?php break;
                    default:?><option value="" selected>Select Status</option>
                  <option value="In Progress" >In Progress</option>
                  <option value="Completed" >Completed</option>
                  <option value="Yet to Service his Vehicle" >Yet to Service his Vehicle</option><?php
                  }
                  ?>
                </select>
                </div>
              </div>
              <div class="row"></div>
<div id="completeddiv<?php echo $booking_id; ?>" style="display:none;" >
              <div class="row">
                <div class="col-lg-2 col-xs-1 col-lg-offset-2 form-group">
                  <label style="padding-top:5px;">&nbsp;Amount</label>
                </div>
                <div class="col-xs-6">
                  <div class="form-group">
                    <input type="number" name="final_bill_amount" class="form-control" style="max-width:280px;" value="<?php echo $final_bill; ?>"/>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-4 col-lg-4 col-lg-offset-2 form-group">
                  <label style="padding-top:5px;">NextServiceDate</label>
                </div>
                <div class="col-xs-4">
                  <div class="form-group">
                  <?php
                  if($service_status = 'Completed'){
                    ?>
                    <input type="text" name="next_service_date" class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" value="<?php echo date("d-m-Y",strtotime($next_service_date)); ?>"/>
                    <?php
                  }
                  else{
                    ?>
                    <input type="text" name="next_service_date" class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" />
                    <?php
                  }
                  ?>
                  </div>
                </div>
              </div>
              <div class="row"></div>

              <div class="row">
                <div class="col-lg-2 col-xs-1 col-lg-offset-2 form-group">
                  <label style="padding-top:5px;">Rating</label>
                </div>
                <div class="col-lg-6 col-xs-6" style="font-size:8px;">
                  <input id="rating<?php echo $booking_id; ?>" name="rating" type="text" class="rating" data-min="0" data-max="5" data-step="0.5" data-stars=5 data-symbol="&#xe005;" data-default-caption="{rating} hearts" data-star-captions="{}" title="" data-show-clear="false" data-show-caption="false" value="<?php echo $rating; ?>">
                 <!-- <input id="rating<?php echo $booking_id; ?>" name="rating" type="number" class="rating" min=1 max=5 step=0.5 data-size="lg" data-rtl="true"> -->
                  </div>
              </div>
              <div class="row"></div>
              <div class="row">
                <div class="form-group">
                  <label style="padding-top:5px;">Got realtime tracking updates?</label>&nbsp;&nbsp;&nbsp;
                  <?php
                  if($realtime_updates == '1'){
                    ?>
                    <input type="radio" name="get_rltime_updte" value="1" checked> Yes</input>&nbsp;&nbsp;
                    <input type="radio" name="get_rltime_updte" value="0"> No</input>
                    <?php
                  }
                  else if($realtime_updates == '0'){
                    ?>
                    <input type="radio" name="get_rltime_updte" value="1"> Yes</input>&nbsp;&nbsp;
                    <input type="radio" name="get_rltime_updte" value="0" checked> No</input>
                    <?php
                  }
                  else{
                    ?>
                    <input type="radio" name="get_rltime_updte" value="1"> Yes</input>&nbsp;&nbsp;
                    <input type="radio" name="get_rltime_updte" value="0"> No</input>
                    <?php
                  }
                  ?>
                </div>
              </div>

                </div> <!-- completed div -->
                <script>
                        $(document).ready(function(){
                          var status = $("#status<?php echo $booking_id; ?>").val();
                          if(status == 'Completed'){
                            $("#completeddiv<?php echo $booking_id; ?>").show();
                          }
                          else{
                            $("#completeddiv<?php echo $booking_id; ?>").hide();
                          }
                        });
                      </script> 
               <script>
                        $(document).ready(function(){
                          $("#status<?php echo $booking_id; ?>").change(function(){
                          var status = $("#status<?php echo $booking_id; ?>").val();
                          if(status == 'Completed'){
                            $("#completeddiv<?php echo $booking_id; ?>").show();
                          }
                          else{
                            $("#completeddiv<?php echo $booking_id; ?>").hide();
                          }
                          });
                        });
</script> 
<script>
                    $(function () {
                      // initialize with defaults
                      $("#rating<?php echo $booking_id; ?>").rating();
                    });
                  </script>
              <div class="row">
                <br>
                <div class="form-group" align="center">
                  <input class="form-control" type="submit" value="Apply" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;width:90px;"/>
                </div>
              </div>
              <input type="hidden" name="b2b_book_id" value="<?php echo $b2b_booking_id; ?>" >
              <input type="hidden" name="book_id" value="<?php echo $booking_id; ?>" >
              <input type="hidden" name="logdate" value="<?php echo date('Y-m-d',strtotime($log)); ?>" >
              <input type="hidden" name="vehtype" value="<?php echo $vehicle_type; ?>" >
            </form>
          </div> <!-- modal body -->
        </div> <!-- modal content -->
      </div>  <!-- modal dailog -->
    </div>  <!-- modal -->
    </td>
    <td><?php if($final_bill != '' || $final_bill != '0'){  echo $final_bill; } ?></td>
    <td><?php echo $locality; ?></td>
    <td><?php echo $booking_source; ?></td>
    <td><?php if($utm_source == ''){ echo "NA";  } else{ echo $utm_source; } ?>-<?php if($utm_medium == ''){ echo "NA";  } else{ echo $utm_medium; } ?></td>
    <td><?php echo $alloted_to; ?></td>     
    <td><?php if($service_date == "0000-00-00"){ echo "Not Updated Yet"; } else { echo date('d M Y', strtotime($service_date)); } ?></td>
  </tr>
  <?php
}
?>
<script>
var date = new Date();
date.setDate(date.getDate());

$('.datepicker').datepicker({
   autoclose: true,
    startDate: date
});
if($('input.datepicker').val()){
    $('input.datepicker').datepicker('setDate', 'today');
 }
</script>

  </tbody>
  </table>
</div>
</div>
<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example").tablesorter( {sortList: [[0,0], [1,0]]} );
    }
);
</script>
<?php
} // if
else {
  //echo $sql_booking;
  ?>
  <div align="center" style="margin-top:140px;">
  <h2>No Results Found !!! </h2>
  </div>
  <?php
}
 ?>

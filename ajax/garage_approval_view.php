<?php

include("../config.php");
$conn = db_connect1();
session_start();
date_default_timezone_set('Asia/Kolkata');
$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$today = date("Y-m-d");
$city = $_POST['city'];
$veh = $_POST['veh'];
$start_date = date('Y-m-d',strtotime($_POST['start_date']));
$end_date = date('Y-m-d',strtotime($_POST['end_date']));

$_SESSION['crm_city'] = $city;
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<style type="text/css">
  p {
    margin: 0 0 10px 15px;
}
</style>
 <div class="example" >
 <table  border="0"  style="width: 100%;" id="table1" >
   <thead style="margin-bottom: 10px !important;">
      <th>IMAGE</th>
      <th >GARAGE NAME</th>
      <th >TYPE</th>
      <th>OWNERNAME</th>
      <th >MOBILENUMBER</th>
      <th >LOCALITY</th>
      <th >GST PAN NO</th>
      <th>DATE</th> 
      <th>ONBOARD BY</th> 
      <th >APPROVAL</th>
   </thead>
    <tbody id="tbody">
    <?php
	$no = 0;
    $cond = '';
    $cond = $cond.($veh == "all" ? "" : "AND type='$veh'");
    $sql_credits="SELECT si.images as image,am.* FROM admin_mechanic_table am left join shop_images si on am.mec_id=si.mec_id WHERE am.axle_id='' AND am.type!='' and am.type!='both' and am.address5='$city' $cond and am.req_axle='1' and am.status='1' and DATE(am.log) BETWEEN '$start_date' AND '$end_date' order by am.log ASC "; 
    
    $res_credits = mysqli_query($conn,$sql_credits);
    $num_credits = mysqli_num_rows($res_credits);
     if($num_credits>0)
     {
        while($row_credits = mysqli_fetch_object($res_credits)){
            $mech_id = $row_credits->mec_id;
            $date=date('d-M-Y, h:i A',strtotime($row_credits->log));
            $images=explode(',',$row_credits->image);
            $shop_name = $row_credits->shop_name;
            $owner_name=$row_credits->owner_name;
            $type=$row_credits->type;
            $gst_pan=$row_credits->gst_pan;
            $mobile_number1=$row_credits->mobile_number1;
            $mobile_number2=$row_credits->mobile_number2;
            $locality=$row_credits->address4;
            $updated_by=$row_credits->updated_by;
            $enc_mech_id = base64_encode($mech_id);
            ?>

            <tr >
           <!--  <td><?php echo $no = $no+1; ?></td> -->
               <td  >
               <!--  <a target="_blank" href="caurosal.php?id=<?php echo base64_encode($mech_id);?>"> -->
                 <a data-target="#myModal" data-toggle="modal" class="getdeal"  data-id="<?php echo $mech_id; ?>">
                    <?php if($images[0]!=''){?>
                    <?php echo '<img src="'.$images[0].'" class="imageclass" alt="" width="70px" height="70px">'; ?>
                <?php }
                else
                {
               ?>
                    <?php echo '<img src="images/download_image.png" class="imageclass" alt="" width="70px" height="70px">'; ?>

               <?php
                }
                ?>
                </a></td>

                <td><span style="color: #27a97e ;"><?php echo $shop_name;?></span><br><span style="font-size: 12px; text-align: left !important; color:#ffa800"><?php echo $mech_id; ?></span></td>

            
           <!--  <td><?php if($type=='2w'){?> <i class="fa fa-motorcycle" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo $type;?><?php } elseif($type=='4w'){ ?><i class="fa fa-car" aria-hidden="true"></i>&nbsp;&nbsp; <?php echo $type;?> <?php }?> </td> -->
               <td><?php echo $type;?></td>
               <td style="color: #808080;" > <?php echo $owner_name;?> </td>
               <td><?php echo $mobile_number1;?></td>
               <td><?php echo $locality; ?></td>
               <td><?php echo $gst_pan;?></td>
               <td style="color: #808080;"><?php echo date('d F Y',strtotime($date));?></td>
               <td><?php echo $updated_by;?></td>
               <td><button class="example1 btn btn-sm" style="background-color: #808080;color:#ffffff;  " onclick='showDetails("<?php echo $enc_mech_id; ?>","<?php echo $shop_name;?>")'>APPROVE</button></td>
          
        </tr>
            <?php 
        }
    }
    else{
        ?>  
            <th colspan="14" style="font-size: 18px !important;text-align:center;padding-top: 40px;">No results found...! </th>
        <?php
        } 
        ?>
    </tbody>
    </table>
 </div>
<script type="text/javascript">
    function showDetails(mec_id,shop_name){ 
        $.confirm({
            content:'Are you sure you want to approve  '+shop_name+'?',
            title:'',
            buttons: {
                Yes: {
                    text: 'Yes', // With spaces and symbols
                    btnClass: 'btn-success',
                    action: function () {
        
              window.open("new-service-centers/approval_mail.php?id="+ mec_id);                   }
                },
                No: {
                    text: 'No', // With spaces and symbols
                    btnClass: 'btn-danger',
                    action: function () {
                    }
                }
            }
        });
    }
</script>
<?php
include("../config.php");
//error_reporting(E_ALL); ini_set('display_errors', 1);
$conn = db_connect2();
session_start();

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$master_service = $_GET['master_service'];
$service = $_GET['service'];
$person = $_GET['person'];
$today = date('Y-m-d');
$services = '';

$programatic = $_GET['programatic'];
$city = $_GET['city'];
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;

$cond ='';

$cond = $cond.($person == 'all' ? "" : "AND g.crm_update_id='$person'");
$cond = $cond.($city == 'all' ? "" : "AND g.city='$city'");
$cond = $cond.($cluster == 'all' ? "" :  "AND (case when g.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'");

if($master_service != "all" && $service == "all"){
    //echo " entered";
    $conn1 = db_connect1();
    $sql_serv = "SELECT service_type FROM go_axle_service_price_tbl WHERE bridge_flag='0' AND master_service='$master_service'";
    $res_serv = mysqli_query($conn1,$sql_serv);
    $services = '';
    while($row_serv = mysqli_fetch_array($res_serv)){
        if($services == ""){
            $services = $row_serv['service_type']."'";
        }
        else{
            $services = $services.",'".$row_serv['service_type']."'";
        }
    }
    $cond = $cond." AND g.service_type IN ('$services)";
} 

$cond = $cond.($service != 'all' ?  " AND g.service_type='$service'" : '' ) ;

$cond = $cond.($programatic == 'yes' ? "" : " AND g.crm_update_id NOT IN ('crm003','crm036','crm018')");

$start = strtotime($startdate);
$end = strtotime($enddate);
$noofdays = date('t',($start));

$str = '';
switch($city){
    case 'Chennai' :$car_cpd = 220; //car credits per day
                    $bike_cpd = 112;  //bike credits per day
                    break;
    case 'Bangalore' :$car_cpd = 100; //car credits per day
                    $bike_cpd = 12;  //bike credits per day
                    break;
    case 'all' :$car_cpd = 320; //car credits per day
                    $bike_cpd = 124;  //bike credits per day
                    break;
    default : $car_cpd = 0; //car credits per day
                $bike_cpd = 0;  //bike credits per day
}
for ( $i = $start; $i <= $end; $i = $i + 86400 ) {
    $thisDate = date('d M Y', $i);
    $day = date('l', strtotime($thisDate));

    $date = date('Y-m-d', $i);
	$datesArr[] = $date;
}
$dates = join("','",$datesArr);
$sql_b2b = "SELECT DISTINCT b.gb_booking_id,b.b2b_vehicle_type,b.b2b_credit_amt,b.b2b_log FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id=g.booking_id LEFT JOIN go_bumpr.localities as l ON g.locality=l.localities WHERE s.b2b_acpt_flag='1' AND b.b2b_swap_flag!='1' AND b.gb_booking_id!='0' AND (DATE(b.b2b_log) IN ('$dates')) AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.service_type NOT IN('GoBumpr Tyre Fest') {$cond}  ORDER BY b.b2b_log";    
$res_b2b = mysqli_query($conn,$sql_b2b);
$dateData = [];
while($row_b2b = mysqli_fetch_object($res_b2b))
{
	$log = date('Y-m-d', strtotime($row_b2b->b2b_log));
	$veh_type = $row_b2b->b2b_vehicle_type;
	$dateData[$log]['veh_type'][] = $veh_type;
	$creditamount = $row_b2b->b2b_credit_amt;
	$dateData[$log]['creditamount'][] = $creditamount;
	$gb_booking_id = $row_b2b->gb_booking_id;
	$dateData[$log]['gb_booking_id'][] = $gb_booking_id;
	$log = $row_b2b->b2b_log;
}
$carsum =0 ;$carcount=0;$caravg=0;
$bikesum =0 ;$bikecount=0;$bikeavg=0;
for ( $i = $start; $i <= $end; $i = $i + 86400 ) {
	$bike = 0;
    $car = 0;
    $total = 0 ;
	$thisDate = date('d M Y', $i);
    $day = date('l', strtotime($thisDate));

    $date = date('Y-m-d', $i);
	$enc_date=base64_encode($date);
    $enc_2w = base64_encode('2w');
    $enc_4w = base64_encode('4w');
	// print_r($dateData);
	$carcount = $carcount+1;$bikecount = $bikecount+1;
	if(count($dateData[$date]['veh_type'])>0)
	{
		for($k = 0;$k<count($dateData[$date]['veh_type']);$k++)
		{
			switch($dateData[$date]['veh_type'][$k]){
				case '2w': $bike = $bike+$dateData[$date]['creditamount'][$k]; break;
				case '4w': $car = $car+$dateData[$date]['creditamount'][$k]; break;
			}//switch
		}		
	}
	$bikecredits = $bike/100;
    $carcredits = $car/100;
	$carsum=$carsum+$carcredits;
	$bikesum=$bikesum+$bikecredits;
    $totalcredits = $bikecredits+$carcredits;
	
    // if ($thisDate=='28 Apr 2018') {
    //     $carcredits=$carcredits+10;
    //     # code...
    // }
	
	$tr = '<tr>';
    $td1 = '<td style="text-align:center;">'.$thisDate.'</td>';
    $td2 = '<td style="text-align:center;">'.$day.'</td>';

    $total_cpd = $car_cpd+$bike_cpd;

    $car_120p = 120*($car_cpd/100);  // 120% of car credits
    $bike_120 = 120*($bike_cpd/100); // 120% of car credits
    
	$car_90p = 90*($car_cpd/100);  // 90% of car credits
    $bike_90 = 90*($bike_cpd/100); // 90% of car credits
    
	$total_120 = $car_120p+$bike_120;
	$total_90 = $car_90p+$bike_90;
	
	if($carcredits > $car_cpd){ 
        $td3 ='<td style="background-color:#7aa8ec;text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_4w.'" >'.$carcredits.'</a></td>';
    }
    else if($carcredits == $car_cpd){ 
        $td3 ='<td style="background-color:#7bde95;text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_4w.'" >'.$carcredits.'</a></td>';
    }
	else if($carcredits >= $car_90p && $carcredits < $car_cpd){ 
		$td3 = '<td style="background-color:#f3ae2b;text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_4w.'" >'.$carcredits.'</a></td>';
    }
    else{ 
        $td3 = '<td style="text-align:center;background-color:#e88c87;"><a href="daily_reports_servicetype.php?dt='.$enc_date.'&vt='.$enc_4w.'&t='.base64_encode(reports).'" >'.$carcredits.'</a></td>';
    }
    if($bikecredits > $bike_cpd){ 
        $td4 = '<td style="background-color:#7aa8ec;text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_2w.'" >'.$bikecredits.'</a></td>';
    }
    else if($bikecredits == $bike_cpd ){ 
        $td4 = '<td style="background-color:#7bde95;text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_2w.'" >'.$bikecredits.'</a></td>'; 
    }
	else if($bikecredits >= $bike_90 && $bikecredits < $bike_cpd){ 
        $td4 = '<td style="background-color:#f3ae2b;text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_2w.'" >'.$bikecredits.'</a></td>'; 
    }
    else{ 
        $td4 = '<td style="text-align:center;background-color:#e88c87;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_2w.'" >'.$bikecredits.'</a></td>'; 
    }
	
    if($totalcredits > $total_cpd){ 
        $td5 = '<td style="background-color:#7aa8ec;text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.base64_encode(all).'" >'.$totalcredits.'</td>'; 
    }
    else if($totalcredits == $total_cpd){ 
        $td5 = '<td style="background-color:#7bde95;text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.base64_encode(all).'" >'.$totalcredits.'</td>'; 
    }
    else if($totalcredits >= $total_90 && $totalcredits < $total_cpd){ 
        $td5 = '<td style="background-color:#f3ae2b;text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.base64_encode(all).'" >'.$totalcredits.'</td>'; 
    }
    else{ 
        $td5 = '<td style="text-align:center;background-color:#e88c87;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.base64_encode(all).'" >'.$totalcredits.'</td>';
    }

    $tr_l = '</tr>';

    $str = $tr.$td1.$td2.$td3.$td4.$td5.$tr_l;
    $data1[] = array('tr'=>$str);
}
$caravg = $carcount == 0 ? 0 : ceil($carsum/$carcount);
$bikeavg = $bikecount == 0 ? 0 : ceil($bikesum/$bikecount);
$acheivedper_car = $car_cpd == 0 ? 0 : ceil(($carsum/($car_cpd*25))*100);
$acheivedper_bike = $bike_cpd == 0 ? 0 : ceil(($bikesum/($bike_cpd*25))*100);
$carprocon = ceil($caravg*$noofdays);
$bikeprocon = ceil($bikeavg*$noofdays);
$cartarget = $car_cpd*25;
$biketarget = $bike_cpd*25;
$carachieved = $carsum." [".$acheivedper_car." %]";
$bikeachieved = $bikesum." [".$acheivedper_bike." %]";
$data2 = array('car_cpd'=>$car_cpd , 'bike_cpd'=>$bike_cpd , 'caravg'=>$caravg , 'bikeavg'=>$bikeavg , 'totalavg'=>$caravg+$bikeavg, 'carprocon'=>$carprocon , 'bikeprocon'=>$bikeprocon , 'totalprocon'=>$carprocon+$bikeprocon , 'cartarget'=>$cartarget , 'biketarget'=>$biketarget , 'carachieved'=>$carachieved , 'bikeachieved'=> $bikeachieved);

// Loop between timestamps, 24 hours at a time
/* for ( $i = $start; $i <= $end; $i = $i + 86400 ) {
    $thisDate = date('d M Y', $i);
    $day = date('l', strtotime($thisDate));

    $date = date('Y-m-d', $i);

    $enc_date=base64_encode($date);
    $enc_2w = base64_encode('2w');
    $enc_4w = base64_encode('4w');

    $sql_b2b = "SELECT DISTINCT b.gb_booking_id,b.b2b_vehicle_type,b.b2b_credit_amt,b.b2b_log FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id=g.booking_id LEFT JOIN go_bumpr.localities as l ON g.locality=l.localities WHERE s.b2b_acpt_flag='1' AND b.b2b_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log)='$date'  AND b.b2b_shop_id NOT IN (1014,1035,1670) {$cond}";    
    $res_b2b = mysqli_query($conn,$sql_b2b);
    //echo $sql_b2b;
    //initialize
    $bike = 0;
    $car = 0;
    $total = 0 ;

    while($row_b2b = mysqli_fetch_object($res_b2b)){
        $veh_type = $row_b2b->b2b_vehicle_type;
        $creditamount = $row_b2b->b2b_credit_amt;
        $gb_booking_id = $row_b2b->gb_booking_id;
        $log = $row_b2b->b2b_log;

        switch($veh_type){
            case '2w': $bike = $bike+$creditamount; break;
            case '4w': $car = $car+$creditamount; break;
        }//switch
    } // while

    $bikecredits = $bike/100;
    $carcredits = $car/100;
    $totalcredits = $bikecredits+$carcredits;
    if($date > $today){
        continue;
    }

    $tr = '<tr>';
    $td1 = '<td style="text-align:center;">'.$thisDate.'</td>';
    $td2 = '<td style="text-align:center;">'.$day.'</td>';

    $total_cpd = $car_cpd+$bike_cpd;

    $car_120p = 120*($car_cpd/100);  // 120% of car credits
    $bike_120 = 120*($bike_cpd/100); // 120% of car credits
    $total_120 = $car_120p+$bike_120;


    if($carcredits >= $car_120p){ 
        $td3 ='<td style="background-color:#7bde95;text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_4w.'" >'.$carcredits.'</a></td>';
    }
    else if($carcredits <$car_120p && $carcredits>= $car_cpd){ 
        $td3 = '<td style="background-color:#f3ae2b;text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_4w.'" >'.$carcredits.'</a></td>';
    }
    else{ 
        $td3 = '<td style="text-align:center;background-color:#e88c87;"><a href="daily_reports_servicetype.php?dt='.$enc_date.'&vt='.$enc_4w.'&t='.base64_encode(reports).'" >'.$carcredits.'</a></td>';
    }
    if($bikecredits >= $bike_120){ 
        $td4 = '<td style="background-color:#7bde95;text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_2w.'" >'.$bikecredits.'</a></td>';
    }
    else if($bikecredits <$bike_120 && $bikecredits >=$bike_cpd ){ 
        $td4 = '<td style="background-color:#f3ae2b;text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_2w.'" >'.$bikecredits.'</a></td>'; 
    }
    else{ 
        $td4 = '<td style="text-align:center;background-color:#e88c87;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_2w.'" >'.$bikecredits.'</a></td>'; 
    }
    if($totalcredits >= $total_120){ 
        $td5 = '<td style="background-color:#7bde95;text-align:center;">'.$totalcredits.'</td>'; 
    }
    else if($totalcredits < $total_120 && $totalcredits >= $total_cpd){ 
        $td5 = '<td style="background-color:#f3ae2b;text-align:center;">'.$totalcredits.'</td>'; 
    }
    else{ 
        $td5 = '<td style="text-align:center;background-color:#e88c87;">'.$totalcredits.'</td>';
    }

    $tr_l = '</tr>';

    $str = $tr.$td1.$td2.$td3.$td4.$td5.$tr_l;
    $data[] = array('tr'=>$str);
} */

$rtn_data['data1'] = $data1;
$rtn_data['data2'] = $data2;
echo $rtn_data1 = json_encode($rtn_data);
?>

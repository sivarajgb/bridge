<?php
include("../config.php");
$conn = db_connect3();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$shop_id = $_POST['shop_id'];
$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate = date('Y-m-d',strtotime($_POST['enddate']));

function calculate_median($arr) {
    $count = count($arr); //total numbers in array
	$middleval = floor(($count-1)/2); // find the middle value, or the lowest middle value
	if($count % 2) { // odd number, middle is the median
        $median = $arr[$middleval];
    } else { // even number, calculate avg of 2 medians
        $low = $arr[$middleval];
        $high = $arr[$middleval+1];
        $median = (($low+$high)/2);
    }
    return $median;
}
$sql = "SELECT AVG(TIMESTAMPDIFF(MINUTE,g.sent_log,b.b2b_mod_log)) AS avg,MIN(TIMESTAMPDIFF(MINUTE,g.sent_log,b.b2b_mod_log)) AS minimum,MAX(TIMESTAMPDIFF(MINUTE,g.sent_log,b.b2b_mod_log)) AS maximum FROM go_bumpr.goaxle_track g,b2b.b2b_status b WHERE g.b2b_booking_id = b.b2b_booking_id AND g.b2b_shop_id= '$shop_id' AND DATE(g.sent_log) between '$startdate' and '$enddate'; ";
$res = mysqli_query($conn,$sql);
$row = mysqli_fetch_object($res);
$avg_acptd = round($row->avg,0);
$min_acptd = $row->minimum;
$max_acptd = $row->maximum;
$sql_med = "SELECT TIMESTAMPDIFF(MINUTE,g.sent_log,b.b2b_mod_log) AS diff FROM go_bumpr.goaxle_track g,b2b.b2b_status b WHERE g.b2b_booking_id = b.b2b_booking_id AND g.b2b_shop_id= '$shop_id' AND TIMESTAMPDIFF(MINUTE,g.sent_log,b.b2b_mod_log) is not null AND DATE(g.sent_log) between '$startdate' and '$enddate' ORDER BY diff ASC; ";
$res_med = mysqli_query($conn,$sql_med);
$arr = array();
while($row_med = mysqli_fetch_object($res_med))
{
	$arr[] = $row_med->diff;
}
$med_acptd = calculate_median($arr);
$sql1 = "SELECT AVG(TIMESTAMPDIFF(MINUTE,s.b2b_acpt_log,b.b2b_contacted_log)) AS avg,MIN(TIMESTAMPDIFF(MINUTE,s.b2b_acpt_log,b.b2b_contacted_log)) AS minimum,MAX(TIMESTAMPDIFF(MINUTE,s.b2b_acpt_log,b.b2b_contacted_log)) AS maximum FROM b2b.b2b_booking_tbl b, b2b.b2b_status s WHERE b.b2b_booking_id = s.b2b_booking_id AND b.b2b_shop_id= '$shop_id' AND b.b2b_contacted='1' AND DATE(b.b2b_log) between '$startdate' and '$enddate';";
$res1 = mysqli_query($conn,$sql1);
$row1 = mysqli_fetch_object($res1);
$avg_contact = round($row1->avg,0);
$min_contact = $row1->minimum;
$max_contact = $row1->maximum;
$sql1_med = "SELECT TIMESTAMPDIFF(MINUTE,s.b2b_acpt_log,b.b2b_contacted_log) AS diff FROM b2b.b2b_booking_tbl b,b2b.b2b_status s WHERE b.b2b_booking_id = s.b2b_booking_id AND b.b2b_shop_id= '$shop_id' AND TIMESTAMPDIFF(MINUTE,b.b2b_log,b.b2b_contacted_log) is not null AND DATE(b.b2b_log) between '$startdate' and '$enddate' ORDER BY diff ASC;";
$res1_med = mysqli_query($conn,$sql1_med);
$arr1 = array();
while($row1_med = mysqli_fetch_object($res1_med))
{
	$arr1[] = $row1_med->diff;
}
$med_contact = calculate_median($arr1);
/*echo $sql;
echo $sql1_med;
print_r($arr);
print_r($arr1);*/
?> 
<div style="margin-top: 70px;text-align: center;font-variant: all-petite-caps;font-size: 20px;">Time taken for Accept/Reject Lead</div>
<div id="metrics1" style="display: inline-flex;margin-top: 20px;margin-left: 90px;">
<div class="floating-box" id="avg_acptd_time" align="center" style="float:left;background-color:#99BBC5; ">
			<p>Avg. Time</p>
			<p><?php echo $avg_acptd==""?0:$avg_acptd; ?> mins</p>
		</div>
		<div class="floating-box" id="min_acptd_time" align="center" style="float:left;background-color: #FFE4B5;">
			<p>Min. Time</p>
			<p><?php echo $min_acptd==""?0:$min_acptd; ?> mins</p>
		</div>
		<br>
		<div class="floating-box" id="max_acptd_time" align="center" style="float:left;background-color:#A7E9B1; ">
			<p>Max. Time</p>
			<p><?php echo $max_acptd==""?0:$max_acptd; ?> mins</p>
		</div>
		<div class="floating-box" id="med_acptd_time" align="center" style="float:left;background-color: #DF9B88;">
			<p>Median Time</p>
			<p><?php echo $med_acptd==""?0:$med_acptd; ?> mins</p>
		</div>
</div>
<div style="margin-top: 70px;text-align: center;font-variant: all-petite-caps;font-size: 20px;">Time taken to contact the customer</div>
<div id="metrics2" style="display: inline-flex;margin-top: 20px;margin-left: 90px;">
<div class="floating-box" id="avg_contact_time" align="center" style="float:left;background-color:#99BBC5; ">
			<p>Avg. Time</p>
			<p><?php echo $avg_contact==""?0:$avg_contact; ?> mins</p>
		</div>
		<div class="floating-box" id="min_contact_time" align="center" style="float:left;background-color: #FFE4B5;">
			<p>Min. Time</p>
			<p><?php echo $min_contact==""?0:$min_contact; ?> mins</p>
		</div>
		<br>
		<div class="floating-box" id="max_contact_time" align="center" style="float:left;background-color:#A7E9B1; ">
			<p>Max. Time</p>
			<p><?php echo $max_contact==""?0:$max_contact; ?> mins</p>
		</div>
		<div class="floating-box" id="med_contact_time" align="center" style="float:left;background-color: #DF9B88;">
			<p>Median Time</p>
			<p><?php echo $med_contact==""?0:$med_contact; ?> mins</p>
		</div>
		</div>
		<center>
		<div class="floating-box" id="back_toggler" align="center" style="display:none;padding-top: 8px;padding-right: 18px;height:35px;float:initial;background-color: #00031A5E;margin-top: 45px;">
			<p>Back<span><i class="fa fa-long-arrow-left" aria-hidden="true" style="display:inline;padding-left:5px;"></i></span></p>
		</div>
		</center>
<?php
include("../config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();
$today=date('Y-m-d');

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$shop_id=$_POST['id'];
$vehicle = $_POST['vehicle_type'];
$city = $_POST['city'];

$_SESSION['crm_city'] = $city;



$cond= ($vehicle=='all')?"" : "AND bb.b2b_vehicle_type = '$vehicle'";

?>


<table id="example2" class="table table-striped table-bordered tablesorter table-hover">
<thead style="background-color:#B2DFDB;">

<th style="text-align:center;">Booking ID</th>
<th style="text-align:center;">Customer Name</th>
<th style="text-align:center;">Mobile Number</th>
<th style="text-align:center;">GoAxle Date</th>
<th style="text-align:center;">Customer Rating</th>
<th style="text-align:center;">Reason for exception</th>
<th style="text-align:center;">Service Center</th>
<th style="text-align:center;">Locality</th>
<th style="text-align:center;">Business Model</th>
<th style="text-align:center;">Credits Consumed</th>
<th style="text-align:center;">Leads Consumed</th>
</thead>
<tbody id="tbody">

<?php 
	$sql_goaxle="SELECT 
	DISTINCT e.booking_id,
	e.b2b_shop_id,
	e.shop_name,
    e.exception_reason,
    bb.b2b_customer_name,
    bb.b2b_cust_phone,
    bb.b2b_log,
    bb.b2b_rating,
    bb.brand,
	g.model AS business_model,
    bb.b2b_credit_amt,
    e.exception_log,
    m.b2b_new_flg,
    m.b2b_address4
FROM
    exception_mechanism_track e 
    LEFT JOIN
    b2b.b2b_booking_tbl bb ON bb.gb_booking_id=e.booking_id AND bb.b2b_shop_id=e.b2b_shop_id
    LEFT JOIN 
    garage_model_history g ON e.b2b_shop_id = g.b2b_shop_id
        AND DATE(e.exception_log) >= DATE(g.start_date)
        AND g.id=(select id from go_bumpr.garage_model_history where b2b_shop_id=e.b2b_shop_id AND DATE(e.exception_log) >= DATE(start_date) order by start_date desc limit 1) 
    LEFT JOIN
		b2b.b2b_mec_tbl m ON m.b2b_shop_id=e.b2b_shop_id
WHERE
    (DATE(e.exception_log) BETWEEN '$startdate' and '$enddate') AND e.b2b_shop_id='$shop_id' AND e.flag=0 {$cond} ORDER BY e.exception_log";
	$res_goaxle = mysqli_query($conn1,$sql_goaxle);
	// echo $sql_goaxle;
	while($row_goaxle = mysqli_fetch_object($res_goaxle))
	{

		$booking_id=$row_goaxle->booking_id;
		$customer_name=$row_goaxle->b2b_customer_name;
		$mobile_number=$row_goaxle->b2b_cust_phone;
		$goaxle_date=$row_goaxle->b2b_log;
		$model=trim($row_goaxle->business_model);
		$rating=$row_goaxle->b2b_rating;
		$reason=$row_goaxle->exception_reason;
		$shop_name=$row_goaxle->shop_name;
		$locality=$row_goaxle->b2b_address4;
		$credit_amt=$row_goaxle->b2b_credit_amt;
		$credits=0;
		$leads=0;
		switch ($model) {
			case 'Premium 1.0':
				$credits=$credit_amt/100;
				break;			
			case 'Premium 2.0':
				$leads=2;
				break;
			case 'Leads 3.0':
				$leads=1;
				break;
			default:
				$credits=$credit_amt/100;
				
		} ?>
		<tr>
		<td><?php echo $booking_id ;?></td>
		<td><?php echo $customer_name;?></td>
		<td><?php echo $mobile_number;?></td>
		<td style="text-align:center;"><?php echo $goaxle_date;?></td>
		<td style="text-align:center;"><?php echo $rating;?></td>
		<td><?php echo $reason;?></td>
		<td><?php echo $shop_name;?></td>
		<td><?php echo $locality;?></td>
		<td><?php echo $model;?></td>
		<td style="text-align:center;"><?php echo $credits;?></td>
		<td style="text-align:center;"><?php echo $leads;?></td>
		</tr>
	<?php } ?>
</tbody>
</table>


<?php
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$starttime = date('H:i:s',strtotime($_GET['startTime']));
$endtime =  date('H:i:s',strtotime($_GET['endTime']));
$start = $startdate.' '.$starttime;
$end = $enddate.' '.$endtime;
// $startdate = date('Y-m-d',strtotime($_GET['startdate']));
// $enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$city = $_GET['city'];

$_SESSION['crm_city'] = $city;

$cond1 ='';
$cond1 = $cond1.($city == 'all' ? "" : "AND city='$city'");

$sql1 = "SELECT crm_log_id,name FROM crm_admin WHERE crm_feedback = 1 and flag = 0 {$cond1} ORDER BY crm_log_id;";
$res1 = mysqli_query($conn,$sql1);
$crm_list = "('";
$i = 0;
$no_of_rows = mysqli_num_rows($res1);
while($row1 = mysqli_fetch_object($res1))
{
	$i++;
	$crm_list = $crm_list.$row1->crm_log_id."'";
	if($i != $no_of_rows)
	{
		$crm_list = $crm_list.",'";
	}
	$crms_arr[$row1->crm_log_id]['name'] = $row1->name;
	$crms_arr[$row1->crm_log_id]['unique'] = 0;
	$crms_arr[$row1->crm_log_id]['total'] = 0;
	$crms_arr[$row1->crm_log_id]['completed'] = 0;
	$crms_arr[$row1->crm_log_id]['rescheduled'] = 0;
	$crms_arr[$row1->crm_log_id]['in_progress'] = 0;
	$crms_arr[$row1->crm_log_id]['rnr'] = 0;
	$crms_arr[$row1->crm_log_id]['others'] = 0;
}
$crm_list = $crm_list.")";
// print_r($crms_arr);

$sql2 = "SELECT touchpoints.crm_log_id,touchpoints.unique_count,touchpoints.total_count FROM (SELECT crm_log_id,count(DISTINCT user_id) as unique_count,count(user_id) as total_count FROM admin_comments_tbl where crm_log_id in $crm_list and log BETWEEN '$start' AND '$end' GROUP BY crm_log_id) as touchpoints;";
$res2 = mysqli_query($conn,$sql2);
while($row2 = mysqli_fetch_object($res2))
{
	$crms_arr[$row2->crm_log_id]['unique'] = $row2->unique_count;
	$crms_arr[$row2->crm_log_id]['total'] = $row2->total_count;
}
// print_r($crms_arr);

$sql_booking = "SELECT DISTINCT f.booking_id,f.service_status,f.feedback_status,f.service_status_reason,f.crm_log_id,f.crm_update_time FROM feedback_track f LEFT JOIN admin_comments_tbl com ON com.book_id = f.booking_id WHERE com.log BETWEEN '2018-05-20 21:00:00' AND '2018-05-21 21:00:00' AND f.flag!=1 AND com.status = 'FeedBack';";
$res_booking = mysqli_query($conn,$sql_booking);
while($row_booking = mysqli_fetch_object($res_booking))
{
	$feedback_status = $row_booking->feedback_status;
	$service_status = $row_booking->service_status;
	$service_status_reason = $row_booking->service_status_reason;
	
	if(!in_array($row_booking->crm_log_id,array_keys($crms_arr)))
	{
		continue;
	}
	
	if($feedback_status == 1 && $service_status == 'Completed')
	{
		$crms_arr[$row_booking->crm_log_id]['completed'] = $crms_arr[$row_booking->crm_log_id]['completed'] + 1;
	}
	else if($feedback_status == 2 && $service_status == 'Yet to Service his Vehicle')
	{
		$crms_arr[$row_booking->crm_log_id]['rescheduled'] = $crms_arr[$row_booking->crm_log_id]['rescheduled'] + 1;
	}
	else if($feedback_status == 3 && $service_status == 'In Progress')
	{
		$crms_arr[$row_booking->crm_log_id]['in_progress'] = $crms_arr[$row_booking->crm_log_id]['in_progress'] + 1;
	}
	else if($feedback_status>10 && $feedback_status<52)
	{
		$crms_arr[$row_booking->crm_log_id]['rnr'] = $crms_arr[$row_booking->crm_log_id]['rnr'] + 1;
	}
	else if($feedback_status == 0 && $service_status == 'Yet to Service his Vehicle')
	{
		$crms_arr[$row_booking->crm_log_id]['others'] = $crms_arr[$row_booking->crm_log_id]['others'] + 1;
	}

}
// print_r($crms_arr);
foreach($crms_arr as $crm)
{
	// if($crm['unique']==0)
	// {
		// continue;
	// }
	
	// if($crm['unique'] < $crm['goaxles'])
	// {
		// $crm['unique'] = $crm['unique'] + ($crm['goaxles'] - $crm['unique']);
	// }
	
	$tr1 = "<tr>";
	$td1 = "<td>".$crm['name']."</td>";
	$td2 = "<td>".$crm['unique']."</td>";
	$td3 = "<td>".$crm['total']."</td>";
	$td4 = "<td>".$crm['completed']."</td>";
	$td5 = "<td>".$crm['rescheduled']."</td>";
	$td6 = "<td>".$crm['in_progress']."</td>";
	$td7 = "<td>".$crm['rnr']."</td>";
	$td8 = "<td>".$crm['others']."</td>";
	$tr1_l = "</tr>";
	
	$str = $tr1.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$tr1_l;
	$data1[] = $str;
}

$data['tbl_data'] = $data1;

echo json_encode($data);
?>
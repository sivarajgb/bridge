<?php
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$cluster_admin = $_SESSION['cluster_admin'];

$src_crm = array('crm016','crm018','crm064','crm036','crm033');
$src_column = 0;
if(in_array($crm_log_id,$src_crm))
{
	$src_column = 1;
}

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$person = $_GET['person'];
$source = $_GET['source'];
$vehicle = $_GET['vehicle'];
$city = $_GET['city'];
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

$col_name = $vehicle == '2w' ? 'l.bike_cluster' : 'l.car_cluster';

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;

$cond ='';
 
$cond = $cond.($vehicle == 'all' ? "" : "AND b.vehicle_type='$vehicle'");
$cond = $cond.($source == 'all' ? "" : "AND b.source='$source'");
$cond = $cond.($person == 'all' ? "" : "AND b.crm_update_id='$person'");
$cond = $cond.($city == 'all' ? "" : "AND b.city='$city'");

if($cluster_admin == '1'){
  $sql_get_clus = "SELECT crm_cluster FROM crm_admin WHERE crm_log_id='$crm_log_id'";
  $res_get_clus = mysqli_query($conn,$sql_get_clus);
  $row_get_clus = mysqli_fetch_object($res_get_clus);
  $crm_cluster = $row_get_clus->crm_cluster;

  $sql_get_people = "SELECT crm_log_id FROM crm_admin WHERE crm_cluster='$crm_cluster'";
  $res_get_people = mysqli_query($conn,$sql_get_people);

  $people = "";

  while($row_get_people = mysqli_fetch_array($res_get_people)){
    if($people == ""){
      $people = $row_get_people['crm_log_id']."'";
    }
    else{
      $people = $people.",'".$row_get_people['crm_log_id']."'";
    }
  }

  $cond = $cond.($cluster == 'all' ? " AND b.crm_update_id IN('".$people.") " : ($vehicle != 'all' ? " AND ".$col_name."='$cluster' AND b.crm_update_id IN('".$people.")" : " AND b.crm_update_id IN('".$people.") AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'"));
}

$sql_booking = "SELECT DISTINCT b.booking_id,b.user_id,b.crm_update_time,b.source,b.vehicle_type,b.user_veh_id,b.service_type,b.service_date,b.shop_name,b.axle_flag,b.crm_update_id,b.locality,b.utm_source,b.followup_date,b.booking_status,b.priority,b.user_pmt_flg,c.name as crm_name,u.name as user_name,u.mobile_number as user_mobile,u.lat_lng as user_latlng,u.Locality_Home as user_locality,u.user_level,v.brand,v.model FROM user_booking_tb as b LEFT JOIN crm_admin as c ON b.crm_update_id=c.crm_log_id LEFT JOIN user_register as u ON b.user_id=u.reg_id LEFT JOIN user_vehicle_table as v ON b.user_veh_id=v.id LEFT JOIN localities as l ON b.locality=l.localities  WHERE  b.booking_status IN(3,4,5,6) AND b.flag!='1' AND DATE_ADD(b.followup_date,INTERVAL 2 DAY) BETWEEN '$startdate' and '$enddate' {$cond} ORDER BY b.priority DESC,b.followup_date ASC";
$res_booking = mysqli_query($conn,$sql_booking);
 
$no = 0;

$count = mysqli_num_rows($res_booking);
if($count >0){

while($row_booking = mysqli_fetch_object($res_booking)){
$booking_id = $row_booking->booking_id;
$user_id = $row_booking->user_id;
$log = $row_booking->followup_date;
$source = $row_booking->source;
$veh_type = $row_booking->vehicle_type;
$veh_id = $row_booking->user_veh_id;
$service_type = $row_booking->service_type;
$user_id = $row_booking->user_id;
$service_date = $row_booking->service_date;
$shop_name = $row_booking->shop_name;
$axle_flag = $row_booking->axle_flag;
$alloted_to_id = $row_booking->crm_update_id;
$locality = $row_booking->locality;
$utm_source = $row_booking->utm_source;
$booking_status = $row_booking->booking_status;
$priority = $row_booking->priority;
$payment_flag = $row_booking->user_pmt_flg;
$alloted_to = $row_booking->crm_name;
$user_name = $row_booking->user_name;
$user_mobile = $row_booking->user_mobile;
$address = $row_booking->user_lat_lng;
$home = $row_booking->user_locality;
$user_level = $row_booking->user_level;
$veh_brand = $row_booking->brand;
$veh_model = $row_booking->model;

$tr = '<tr>';

$td1 = '<td><p style="float:left;padding:10px;">'.$booking_id.'</p>';
if($booking_status == '4'){
  $td1 = $td1.'<p class="p" style="font-size:16px;color:#F4511E;float:left;padding:5px;" title="RNR 1"><i class="fa fa-tty" aria-hidden="true"></i></p>';
}
if($booking_status == '5'){
  $td1 = $td1.'<p class="p" style="font-size:16px;color:#C62828;float:left;padding:5px;" title="RNR 2"><i class="fa fa-tty" aria-hidden="true"></i></p>';
}
switch($priority){
  case '1': $td1 = $td1.'<p style="font-size:16px;color:#00ACC1;float:left;padding:5px;" title="Low Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';break;
  case '2': $td1 = $td1.'<p style="font-size:16px;color:#FFB300;float:left;padding:5px;" title="Medium Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';break;
  case '3': $td1 = $td1.'<p style="font-size:16px;color:#E53935;float:left;padding:5px;" title="High Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';break;
}
if($payment_flag == '1'){
  $td1 = $td1.'<p style="font-size:16px;color:#1c57af;float:left;padding:5px;" title="Paid!"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></p>';                    
}
$td1 = $td1.'</td>';

$td2 = '<td>';
switch($service_type){
      case 'general_service' :
      if($veh_type =='2w'){
        $td2 = $td2."General Service";
      }
      else{
        $td2 = $td2."Car service and repair";
      }
      break;
      case 'break_down':  $td2 = $td2."Breakdown Assistance"; break;
      case 'tyre_puncher': $td2 = $td2."Tyre Puncture";  break;
      case 'other_service': $td2 = $td2."Repair Job"; break;
      case 'car_wash':
      if($veh_type == '2w'){
        $td2 = $td2."water Wash";
      }
      else{
        $td2 = $td2."Car wash exterior";
      }break;
      case 'engine_oil': $td2 = $td2."Repair Job"; break;
      case 'free_service': $td2 = $td2."General Service"; break;
      case 'car_wash_both':
      case 'completecarspa': $td2 = $td2."Complete Car Spa"; break;
      case 'car_wash_ext': $td2 = $td2."Car wash exterior"; break;
      case 'car_wash_int': $td2 = $td2."Interior Detailing"; break;
      case 'Doorstep car spa': $td2 = $td2."Doorstep Car Spa"; break;
      case 'Doorstep_car_wash': $td2 = $td2."Doorstep Car Wash"; break;
      case 'free_diagnastic':
      case 'free_diagnostic':
      case 'diagnostics':
      if($veh_type == '2w'){
        $td2 = $td2."Diagnostics/Check-up";
     }
     else{
       $td2 = $td2."Vehicle Diagnostics";
     }
         break;
      case 'water_wash': $td2 = $td2."water Wash"; break;
      case 'exteriorfoamwash': $td2 = $td2."Car wash exterior"; break;
      case 'interiordetailing': $td2 = $td2."Interior Detailing"; break;
      case 'rubbingpolish': $td2 = $td2."Car Polish"; break;
      case 'underchassisrustcoating': $td2 = $td2."Underchassis Rust Coating"; break;
      case 'headlamprestoration': $td2 = $td2."Headlamp Restoration"; break;
      default: $td2 = $td2.$service_type;
} 
$td2 = $td2.'</td>';
$td3 = '<td>'.$user_name;
switch($user_level){
  case '5': $td3 = $td3.'<p style="font-size:16px;color:green;padding:5px;" title="Level 5 User"><i class="fa fa-angellist" aria-hidden="true"></i></p>';
  break;
  case '4': $td3 = $td3.'<p style="font-size:16px;color:red;padding:5px;" title="Level 4 User"><i class="fa fa-bug" aria-hidden="true"></i></p>';
  break;
  case '3': $td3 = $td3.'<p style="font-size:16px;color:#26269e;padding:5px;" title="Level 3 User"><i class="fa fa-handshake-o" aria-hidden="true"></i></p>';
  break;
  case '2': $td3 = $td3.'<p style="font-size:16px;color:brown;padding:5px;" title="Level 2 User"><i class="fa fa-anchor" aria-hidden="true"></i></p>';
  break;
  case '1': $td3 = $td3.'<p style="font-size:16px;color:orange;padding:5px;" title="Level 1 User"><i class="fa fa-tripadvisor" aria-hidden="true"></i></p>';
  break;
  default:
}
$td3 = $td3.'</td>';
$td4 = '<td>'.$user_mobile.'</td>';
$td5 = '<td>';
if($source == "Hub Booking"){ 
  if($locality!=''){ 
    $td5 = $td5.$locality; 
  } 
  else if($home == ''){ 
    $td5 = $td5.$address; 
  } 
  else { 
    $td5 = $td5.$home; 
  } 
} 
else if($source == "GoBumpr App" || $source == "App"){ 
  $td5 = $td5.$address; 
} 
else{  
  $td5 = $td5.$locality; 
} 

$td5 = $td5.'</td>';
$td6 = '<td>'.$veh_brand." ".$veh_model.'</td>';
$td7 = '<td>'.$shop_name.'</td>';

$td8 = '<td>';
if($service_date == "0000-00-00"){ 
  $td8 = $td8."Not Updated Yet"; 
} 
else { 
  $td8 = $td8.date('d M Y', strtotime($service_date));
} 
$td8 = $td8.'</td>';
$td9 = '<td>'.$alloted_to.'</td>';
if($src_column==1){
		 $td9 = $td9."<td>$bsource</td><td>";
		 if($utm_source == ''){ $td9 = $td9."NA";  } else{ $td9 = $td9.$utm_source; }
		 $td9 = $td9."</td>";
}

$td10 = '<td>'.date('d M Y h.i A', strtotime($log)).'</td>';
$tr_l ='</tr>';
  
$str = $tr.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$td9.$td10.$tr_l;
$data[] = array('tr'=>$str);
}
echo $data1 = json_encode($data);
} // if
else {
  echo "no";
}
 ?>

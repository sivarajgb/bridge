<?php
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();

// error_reporting(E_ALL); ini_set('display_errors', 1); 

$tab = $_GET['tab'];

//0-all && 1-particular
class goaxles
{
    function cr0_vg0_sup0_vr0_vd0()
    {
        return "Accepted";
    }

    function cr0_vg0_sup0_vr0_vd1()
    {
        return "Delivered";
    }

    function cr0_vg0_sup0_vr1_vd0()
    {
        return "Ready";
    }

    function cr0_vg0_sup0_vr1_vd1()
    {
        return "Delivered";
    }

    function cr0_vg0_sup1_vr0_vd0()
    {
        return "In Progress";
    }

    function cr0_vg0_sup1_vr0_vd1()
    {
        return "Delivered";
    }

    function cr0_vg0_sup1_vr1_vd0()
    {
        return "Ready";
    }

    function cr0_vg0_sup1_vr1_vd1()
    {
        return "Delivered";
    }

    function cr0_vg1_sup0_vr0_vd0()
    {
        return "In Garage";
    }

    function cr0_vg1_sup0_vr0_vd1()
    {
        return "Delivered";
    }

    function cr0_vg1_sup0_vr1_vd0()
    {
        return "Ready";
    }

    function cr0_vg1_sup0_vr1_vd1()
    {
        return "Delivered";
    }

    function cr0_vg1_sup1_vr0_vd0()
    {
        return "In Progress";
    }

    function cr0_vg1_sup1_vr0_vd1()
    {
        return "Delivered";
    }

    function cr0_vg1_sup1_vr1_vd0()
    {
        return "Ready";
    }

    function cr0_vg1_sup1_vr1_vd1()
    {
        return "Delivered";
    }

    function cr1_vg0_sup0_vr0_vd0()
    {
        return "Checked In";
    }

    function cr1_vg0_sup0_vr0_vd1()
    {
        return "Delivered";
    }

    function cr1_vg0_sup0_vr1_vd0()
    {
        return "Ready";
    }

    function cr1_vg0_sup0_vr1_vd1()
    {
        return "Delivered";
    }

    function cr1_vg0_sup1_vr0_vd0()
    {
        return "In Progress";
    }

    function cr1_vg0_sup1_vr0_vd1()
    {
        return "Delivered";
    }

    function cr1_vg0_sup1_vr1_vd0()
    {
        return "Ready";
    }

    function cr1_vg0_sup1_vr1_vd1()
    {
        return "Delivered";
    }

    function cr1_vg1_sup0_vr0_vd0()
    {
        return "In Garage";
    }

    function cr1_vg1_sup0_vr0_vd1()
    {
        return "Delivered";
    }

    function cr1_vg1_sup0_vr1_vd0()
    {
        return "Ready";
    }

    function cr1_vg1_sup0_vr1_vd1()
    {
        return "Delivered";
    }

    function cr1_vg1_sup1_vr0_vd0()
    {
        return "In Progress";
    }

    function cr1_vg1_sup1_vr0_vd1()
    {
        return "Delivered";
    }

    function cr1_vg1_sup1_vr1_vd0()
    {
        return "Ready";
    }

    function cr1_vg1_sup1_vr1_vd1()
    {
        return "Delivered";
    }
}

$goaxles_obj = new goaxles();


switch ($tab) {
    case 'person':
        // print_r($_POST);
        $crmlogid = $_POST['crmlogid'];
        $startdate = $_POST['startdate'];
        $enddate = $_POST['enddate'];
        $status = $_POST['status'];
        $city = $_POST['city'];
        $bservice_type = $_POST['bservice'];
        $bvehicle = $_POST['vehicle'];
        $bmaster_service = $_POST['master_service'];
        $crm_person = $_POST['person'];
        $bsource = $_POST['bsource'];
        // $cluster = $city != 'Chennai' ? 'all' : $_POST['cluster'];
        $cluster = 'all';

        switch ($status) {
            case 'lead':
                $cond = " AND b.flag_unwntd!='1'";
                break;
            case 'booking':
                $cond = " AND b.booking_status = '2' AND b.flag!='1' AND b.flag_unwntd!='1'";
                $redir = base64_encode("b");
                break;
            case 'envconv':
                $cond = " AND b.booking_status = '2' AND b.flag!='1' AND b.flag_unwntd!='1' and (b.service_status in('Completed','In Progress') or bb.b2b_vehicle_at_garage='1')";
                $redir = base64_encode("b");
                break;
            case 'followup':
                $cond = " AND (b.booking_status IN(3,4,5,6) OR (b.booking_status='1' AND b.flag_fo='1')) AND b.flag!='1' AND b.flag_unwntd!='1'";
                $redir = base64_encode("f");
                break;
            case 'cancelled':
                $cond = " AND b.flag = '1' AND b.flag_unwntd!='1'";
                $redir = base64_encode("c");
                break;
            case 'others':
                $cond = " AND b.booking_status = '0' AND b.flag!='1' AND b.flag_unwntd!='1'";
                $redir = base64_encode("o");
                break;
            case 'idle':
                $cond = " AND b.booking_status = '1' AND b.flag_unwntd!='1' AND b.flag_fo!='1' AND b.flag!='1'";
                $redir = base64_encode("l");
                break;
            case 'duplicate':
                $cond = " AND b.activity_status = '26' AND b.flag_unwntd='1' AND b.flag='1'";
                $redir = base64_encode("c");
                break;
            default :
                $cond = " AND b.flag_unwntd!='1'";
                $redir = base64_encode("l");
                break;
        }

        if ($bsource == 'website') {
            $both_source = "AND b.source in ('website','bookNow')";
        } else {
            $both_source = "AND b.source='$bsource'";
        }

        $cond2 = $crmlogid == 'all' ? "" : " AND b.crm_update_id='$crmlogid' ";

        $cond2 = $cond2 . ($city == 'all' ? "" : "AND b.city='$city'");
        $cond2 = $cond2 . ($cluster == 'all' ? "" : "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%" . $cluster . "%'");
        $cond2 = $cond2 . ($crm_person == 'all' ? "" : "AND b.crm_update_id='$crm_person'");
        $cond2 = $cond2 . ($bservice_type == 'all' ? "" : "AND b.service_type='$bservice_type'");
        $cond2 = $cond2 . ($bsource == 'all' ? "" : $both_source);
        $cond2 = $cond2 . ($bvehicle == 'all' ? "" : "AND b.vehicle_type='$bvehicle'");
        $cond2 = $cond2 . ($bmaster_service == 'all' ? "" : "AND gs.master_service IN ('$bmaster_service')");


        $sql_person = $city == "Chennai" ? "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM user_booking_tb as b LEFT JOIN localities as l ON b.locality=l.localities LEFT JOIN b2b.b2b_booking_tbl bb on bb.gb_booking_id=b.booking_id left join go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884,189598,113453,252884,296395,298600,160597,133986,270162,191883,21816,298572,110974,287322,241042,53865,289516,14485,1678,30865,125455,338469) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.source != 'Sulekha Booking' AND b.log BETWEEN '$startdate' AND '$enddate' {$cond2}{$cond} and (ISNULL(b2b_swap_flag) or b2b_swap_flag!='1') group by b.booking_id ORDER BY b.log DESC" : "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM user_booking_tb as b LEFT JOIN b2b.b2b_booking_tbl bb on bb.gb_booking_id=b.booking_id left join go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884,189598,113453,252884,296395,298600,160597,133986,270162,191883,21816,298572,110974,287322,241042,53865,289516,14485,1678,30865,125455,338469) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.source != 'Sulekha Booking' AND b.log BETWEEN '$startdate' AND '$enddate' {$cond2}{$cond} and (ISNULL(b2b_swap_flag) or b2b_swap_flag!='1') group by b.booking_id ORDER BY b.log DESC";
        // echo $sql_person;
        $res_person = mysqli_query($conn, $sql_person);

        if (mysqli_num_rows($res_person) >= 1) {
            ?>
            <table class="table table-striped table-bordered tablesorter table-hover">
                <thead style="background-color: #D3D3D3;">
                <!--<th>No</th>-->
                <th>BookingId</th>
                <th>CustomerName</th>
                <th>ServiceType</th>
                <th>ServiceCenter</th>
                <th>PickUp</th>
                <?php if ($status == 'booking') { ?>
                    <th>GoAxleStatus</th> <?php } ?>
                </thead>
                <tbody>
                <?php
                while ($row_person = mysqli_fetch_object($res_person)) {
                    $booking_id = $row_person->booking_id;
                    $user_id = $row_person->user_id;
                    $veh_id = $row_person->user_veh_id;
                    $res_user = mysqli_query($conn, "SELECT name FROM user_register WHERE reg_id='$user_id'");
                    $row_user = mysqli_fetch_object($res_user);
                    $user_name = $row_user->name;

                    if ($status == 'lead') {
                        $st = $row_person->booking_status;
                        switch ($st) {
                            case '1':
                                $redir = base64_encode("l");
                                break;
                            case '2':
                                $redir = base64_encode("b");
                                break;
                            case '3':
                            case '4':
                            case '5':
                            case '6':
                                $redir = base64_encode("f");
                                break;
                            case '0':
                                $redir = base64_encode("o");
                                break;
                            default:
                                $redir = base64_encode("l");
                        }
                    }


                    if ($status == 'booking') {
                        //   error_reporting(E_ALL); ini_set('display_errors', 1);

                        $sql_b2b = "SELECT DISTINCT b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_service_under_progress,b.b2b_vehicle_ready,b.b2b_vehicle_delivered,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.gb_booking_id='$booking_id' AND b.b2b_swap_flag!='1'";
                        $res_b2b = mysqli_query($conn2, $sql_b2b) or die(mysqli_error($res_b2b));
                        $row_b2b = mysqli_fetch_object($res_b2b);
                        $acpt = $row_b2b->b2b_acpt_flag;
                        $deny = $row_b2b->b2b_deny_flag;

                        if (mysqli_num_rows($res_b2b) >= 1) {
                            if ($deny == '1' && $acpt == '0') {
                                $goaxle = "Rejected";
                            } else if ($deny == '0' && $acpt == '0') {
                                $goaxle = "No Action";
                            } else {
                                $check_in_report = $row_b2b->b2b_check_in_report;
                                $vehicle_at_garage = $row_b2b->b2b_vehicle_at_garage;
                                $service_under_progress = $row_b2b->b2b_service_under_progress;
                                $vehicle_ready = $row_b2b->b2b_vehicle_ready;
                                $vehicle_delivered = $row_b2b->b2b_vehicle_delivered;

                                $check_in_report_val = $check_in_report == '0' ? "0" : "1";
                                $vehicle_at_garage_val = $vehicle_at_garage == '0' ? "0" : "1";
                                $service_under_progress_val = $service_under_progress == '0' ? "0" : "1";
                                $vehicle_ready_val = $vehicle_ready == '0' ? "0" : "1";
                                $vehicle_delivered_val = $vehicle_delivered == '0' ? "0" : "1";

                                $goaxle = $goaxles_obj->{"cr{$check_in_report_val}_vg{$vehicle_at_garage_val}_sup{$service_under_progress_val}_vr{$vehicle_ready_val}_vd{$vehicle_delivered_val}"}();
                            }
                        } else {
                            $goaxle = "Record not found!";
                        }
                    }
                    ?>
                    <tr>
                        <td><a target="_blank"
                               href="user_details.php?t=<?php echo $redir; ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>"><?php echo $booking_id; ?></a>
                        </td>
                        <td><?php echo $user_name; ?></td>
                        <td><?php echo $row_person->service_type; ?></td>
                        <td><?php echo $row_person->shop_name; ?></td>
                        <td><?php $p = $row_person->pick_up;
                            echo $pc = $p == '1' ? 'Yes' : 'No'; ?></td>
                        <?php if ($status == 'booking') { ?>
                            <td> <?php echo $goaxle; ?></td> <?php } ?>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <?php
        } else {
            echo "<h4> No Records Found!</h4>";
        }
        break;
    case 'service':
        $vehicle = $_POST['vehicle'];
        $master_service = $_POST['masterservice'];
        $startdate = $_POST['startdate'];
        $enddate = $_POST['enddate'];
        $vehicle = $_POST['vehicle'];
        $status = $_POST['status'];
        $city = $_POST['city'];
        $bservice_type = $_POST['bservice'];
        $bvehicle = $_POST['bvehicle'];
        if ($bvehicle != 'all') {
            $vehicle = 'all';
        }
        $bmaster_service = $_POST['master_service'];
        //echo $bmaster_service;
        $crm_person = $_POST['person'];
        $bsource = $_POST['bsource'];
        // $cluster = $city != 'Chennai' ? 'all' : $_POST['cluster'];
        $cluster = 'all';

        $cond = '';


        switch ($vehicle) {
            case '2w':
                $lc = "AND l.bike_cluster='$cluster'";
                break;
            case '4w' :
                $lc = "AND l.car_cluster='$cluster'";
                break;
            default :
                $lc = "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%" . $cluster . "%'";
        }

        $cond = $cond . ($vehicle == 'all' ? "" : "AND b.vehicle_type='$vehicle'");
        $cond = $cond . ($city == 'all' ? "" : "AND b.city='$city'");
        $cond = $cond . ($cluster == 'all' ? "" : $lc);
        if ($master_service != 'all') {
            $conn1 = db_connect1();
            $sql_serv = "SELECT service_type FROM go_axle_service_price_tbl WHERE master_service='$master_service'";
            $res_serv = mysqli_query($conn1, $sql_serv);
            $services = '';
            while ($row_serv = mysqli_fetch_array($res_serv)) {
                if ($services == "") {
                    $services = $row_serv['service_type'] . "'";
                } else {
                    $services = $services . ",'" . $row_serv['service_type'] . "'";
                }
            }

            $cond = $cond . "AND b.service_type IN('$services)";

        }

        if ($bsource == 'website') {
            $both_source = "AND b.source in ('website','bookNow')";
        } else {
            $both_source = "AND b.source='$bsource'";
        }

        $cond = $cond . ($city == 'all' ? "" : " AND b.city='$city'");
        $cond = $cond . ($crm_person == 'all' ? "" : " AND b.crm_update_id='$crm_person'");
        $cond = $cond . ($bservice_type == 'all' ? "" : " AND b.service_type='$bservice_type'");
        $cond = $cond . ($bsource == 'all' ? "" : $both_source);
        $cond = $cond . ($bvehicle == 'all' ? "" : " AND b.vehicle_type='$bvehicle'");
        $cond = $cond . ($bmaster_service == 'all' ? "" : "   AND gs.master_service IN ('$bmaster_service')");

        switch ($status) {
            case 'lead':
                $cond2 = " AND b.flag_unwntd!='1'";
                break;
            case 'booking':
                $cond2 = " AND b.booking_status = '2' AND b.flag!='1' AND b.flag_unwntd!='1'";
                $redir = base64_encode("b");
                break;
            case 'envconv':
                $cond2 = " AND b.booking_status = '2' AND b.flag!='1' AND b.flag_unwntd!='1' and (b.service_status in('Completed','In Progress') or bb.b2b_vehicle_at_garage='1')";
                $redir = base64_encode("b");
                break;
            case 'followup':
                $cond2 = " AND (b.booking_status IN(3,4,5,6) OR (b.booking_status='1' AND b.flag_fo='1')) AND b.flag!='1' AND b.flag_unwntd!='1'";
                $redir = base64_encode("f");
                break;
            case 'cancelled':
                $cond2 = " AND b.flag = '1' AND b.flag_unwntd!='1'";
                $redir = base64_encode("c");
                break;
            case 'others':
                $cond2 = " AND b.booking_status = '0' AND b.flag!='1' AND b.flag_unwntd!='1'";
                $redir = base64_encode("o");
                break;
            case 'idle':
                $cond2 = " AND b.booking_status = '1' AND b.flag_unwntd!='1' AND b.flag_fo!='1' AND b.flag!='1'";
                $redir = base64_encode("l");
                break;
            case 'duplicate':
                $cond2 = " AND b.activity_status = '26' AND b.flag_unwntd='1' AND b.flag='1'";
                $redir = base64_encode("c");
                break;
            default :
                $cond2 = " AND b.flag_unwntd!='1'";
                $redir = base64_encode("l");
                break;
        }

        $sql_service = $city == 'Chennai' ? "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id,gs.master_service FROM user_booking_tb as b LEFT JOIN localities as l ON b.locality=l.localities LEFT JOIN b2b.b2b_booking_tbl bb on bb.gb_booking_id=b.booking_id left join go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884,189598,113453,252884,296395,298600,160597,133986,270162,191883,21816,298572,110974,287322,241042,53865,289516,14485,1678,30865,125455,338469) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.source != 'Sulekha Booking' AND b.log BETWEEN '$startdate' AND '$enddate' {$cond} {$cond2} and (ISNULL(b2b_swap_flag) or b2b_swap_flag!='1') group by b.booking_id" : "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id,gs.master_service FROM user_booking_tb as b LEFT JOIN b2b.b2b_booking_tbl bb on bb.gb_booking_id=b.booking_id left join go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884,189598,113453,252884,296395,298600,160597,133986,270162,191883,21816,298572,110974,287322,241042,53865,289516,14485,1678,30865,125455,338469) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.source != 'Sulekha Booking' AND b.log BETWEEN '$startdate' AND '$enddate' {$cond} {$cond2} and (ISNULL(b2b_swap_flag) or b2b_swap_flag!='1') group by b.booking_id";

        //echo $sql_service;
        $res_service = mysqli_query($conn, $sql_service);

        if (mysqli_num_rows($res_service) >= 1) {
            ?>
            <table class="table table-striped table-bordered tablesorter table-hover">
                <thead style="background-color: #D3D3D3;">
                <!--<th>No</th>-->
                <th>BookingId</th>
                <th>CustomerName</th>
                <th>ServiceType</th>
                <th>ServiceCenter</th>
                <th>PickUp</th>
                <?php if ($status == 'booking') { ?>
                    <th>GoAxleStatus</th> <?php } ?>
                </thead>
                <tbody>
                <?php
                while ($row_service = mysqli_fetch_object($res_service)) {
                    $booking_id = $row_service->booking_id;
                    $user_id = $row_service->user_id;
                    $veh_id = $row_service->user_veh_id;
                    $res_user = mysqli_query($conn, "SELECT name FROM user_register WHERE reg_id='$user_id'");
                    $row_user = mysqli_fetch_object($res_user);
                    $user_name = $row_user->name;

                    if ($status == 'lead') {
                        $st = $row_service->booking_status;
                        switch ($st) {
                            case '1':
                                $redir = base64_encode("l");
                                break;
                            case '2':
                                $redir = base64_encode("b");
                                break;
                            case '3':
                            case '4':
                            case '5':
                            case '6':
                                $redir = base64_encode("f");
                                break;
                            case '0':
                                $redir = base64_encode("o");
                                break;
                            default:
                                $redir = base64_encode("l");
                        }
                    }
                    if ($status == 'booking') {
                        //   error_reporting(E_ALL); ini_set('display_errors', 1);

                        $sql_b2b = "SELECT DISTINCT b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_service_under_progress,b.b2b_vehicle_ready,b.b2b_vehicle_delivered,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.gb_booking_id='$booking_id' AND b.b2b_swap_flag!='1'";
                        $res_b2b = mysqli_query($conn2, $sql_b2b) or die(mysqli_error($res_b2b));
                        $row_b2b = mysqli_fetch_object($res_b2b);
                        $acpt = $row_b2b->b2b_acpt_flag;
                        $deny = $row_b2b->b2b_deny_flag;

                        if (mysqli_num_rows($res_b2b) >= 1) {
                            if ($deny == '1' && $acpt == '0') {
                                $goaxle = "Rejected";
                            } else if ($deny == '0' && $acpt == '0') {
                                $goaxle = "No Action";
                            } else {
                                $check_in_report = $row_b2b->b2b_check_in_report;
                                $vehicle_at_garage = $row_b2b->b2b_vehicle_at_garage;
                                $service_under_progress = $row_b2b->b2b_service_under_progress;
                                $vehicle_ready = $row_b2b->b2b_vehicle_ready;
                                $vehicle_delivered = $row_b2b->b2b_vehicle_delivered;

                                $check_in_report_val = $check_in_report == '0' ? "0" : "1";
                                $vehicle_at_garage_val = $vehicle_at_garage == '0' ? "0" : "1";
                                $service_under_progress_val = $service_under_progress == '0' ? "0" : "1";
                                $vehicle_ready_val = $vehicle_ready == '0' ? "0" : "1";
                                $vehicle_delivered_val = $vehicle_delivered == '0' ? "0" : "1";

                                $goaxle = $goaxles_obj->{"cr{$check_in_report_val}_vg{$vehicle_at_garage_val}_sup{$service_under_progress_val}_vr{$vehicle_ready_val}_vd{$vehicle_delivered_val}"}();
                            }
                        } else {
                            $goaxle = "Record not found!";
                        }
                    }
                    ?>
                    <tr>
                        <td><a target="_blank"
                               href="user_details.php?t=<?php echo $redir; ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>"><?php echo $booking_id; ?></a>
                        </td>
                        <td><?php echo $user_name; ?></td>
                        <td><?php echo $row_service->service_type; ?></td>
                        <td><?php echo $row_service->shop_name; ?></td>
                        <td><?php $p = $row_service->pick_up;
                            echo $pc = $p == '1' ? 'Yes' : 'No'; ?></td>
                        <?php if ($status == 'booking') { ?>
                            <td><?php echo $goaxle; ?></td> <?php } ?>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <?php
        } else {
            echo "<h4> No Records Found!</h4>";
        }
        //print_r($_POST);
        break;
    case 'source':
        $vehicle = $_POST['vehicle'];
        $source = $_POST['source'];
        $startdate = $_POST['startdate'];
        $enddate = $_POST['enddate'];
        $status = $_POST['status'];
        $city = $_POST['city'];
        $bservice_type = $_POST['bservice'];
        $bvehicle = $_POST['bvehicle'];
        if ($bvehicle != 'all') {
            $vehicle = 'all';
        }
        $bmaster_service = $_POST['master_service'];
        $crm_person = $_POST['person'];
        $bsource = $_POST['bsource'];
        // $cluster = $city != 'Chennai' ? 'all' : $_POST['cluster'];
        $cluster = 'all';
        $cond = '';

        switch ($vehicle) {
            case '2w':
                $lc = "AND l.bike_cluster='$cluster'";
                break;
            case '4w' :
                $lc = "AND l.car_cluster='$cluster'";
                break;
            default :
                $lc = "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%" . $cluster . "%'";
        }
        if ($source == 'website') {
            $both_source1 = " AND b.source in ('website','bookNow')";
        } else {
            $both_source1 = " AND b.source='$source'";
        }

        if ($bsource == 'website') {
            $both_source = "AND b.source in ('website','bookNow')";
        } else {
            $both_source = "AND b.source='$bsource'";
        }
        $cond = $cond . ($vehicle == 'all' ? "" : "AND b.vehicle_type='$vehicle'");
        $cond = $cond . ($city == 'all' ? "" : "AND b.city='$city'");
        $cond = $cond . ($source == 'all' ? "" : $both_source1);
        $cond = $cond . ($cluster == 'all' ? "" : $lc);
        $cond = $cond . ($crm_person == 'all' ? "" : "AND b.crm_update_id='$crm_person'");
        $cond = $cond . ($bservice_type == 'all' ? "" : "AND b.service_type='$bservice_type'");
        $cond = $cond . ($bsource == 'all' ? "" : $both_source);
        $cond = $cond . ($bvehicle == 'all' ? "" : "AND b.vehicle_type='$bvehicle'");
        $cond = $cond . ($bmaster_service == 'all' ? "" : "AND gs.master_service IN ('$bmaster_service')");

        switch ($status) {
            case 'lead':
                $cond2 = " AND b.flag_unwntd!='1'";
                break;
            case 'booking':
                $cond2 = " AND b.booking_status = '2' AND b.flag!='1' AND b.flag_unwntd!='1'";
                $redir = base64_encode("b");
                break;
            case 'envconv':
                $cond2 = " AND b.booking_status = '2' AND b.flag!='1' AND b.flag_unwntd!='1' and (b.service_status in('Completed','In Progress') or bb.b2b_vehicle_at_garage='1')";
                $redir = base64_encode("b");
                break;
            case 'followup':
                $cond2 = " AND (b.booking_status IN(3,4,5,6) OR (b.booking_status='1' AND b.flag_fo='1')) AND b.flag!='1' AND b.flag_unwntd!='1'";
                $redir = base64_encode("f");
                break;
            case 'cancelled':
                $cond2 = " AND b.flag = '1' AND b.flag_unwntd!='1'";
                $redir = base64_encode("c");
                break;
            case 'others':
                $cond2 = " AND b.booking_status = '0' AND b.flag!='1' AND b.flag_unwntd!='1'";
                $redir = base64_encode("o");
                break;
            case 'idle':
                $cond2 = " AND b.booking_status = '1' AND b.flag_unwntd!='1' AND b.flag_fo!='1' AND b.flag!='1'";
                $redir = base64_encode("l");
                break;
            case 'duplicate':
                $cond2 = " AND b.activity_status = '26' AND b.flag_unwntd='1' AND b.flag='1'";
                $redir = base64_encode("c");
                break;
            default :
                $cond2 = " AND b.flag_unwntd!='1'";
                $redir = base64_encode("l");
                break;
        }

        $sql_source = $city == 'Chennai' ? "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM user_booking_tb as b LEFT JOIN localities as l ON b.locality=l.localities LEFT JOIN b2b.b2b_booking_tbl bb on bb.gb_booking_id=b.booking_id left join go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884,189598,113453,252884,296395,298600,160597,133986,270162,191883,21816,298572,110974,287322,241042,53865,289516,14485,1678,30865,125455,338469) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.source != 'Sulekha Booking' AND b.log BETWEEN '$startdate' AND '$enddate' {$cond} {$cond2} and (ISNULL(b2b_swap_flag) or b2b_swap_flag!='1') group by b.booking_id" : "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM user_booking_tb as b LEFT JOIN b2b.b2b_booking_tbl bb on bb.gb_booking_id=b.booking_id left join go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884,189598,113453,252884,296395,298600,160597,133986,270162,191883,21816,298572,110974,287322,241042,53865,289516,14485,1678,30865,125455,338469) AND b.service_type != 'IOCL Check-up' AND b.source !='Re-Engagement Bookings' AND b.source != 'Sulekha Booking' AND b.log BETWEEN '$startdate' AND '$enddate' {$cond} {$cond2} and (ISNULL(b2b_swap_flag) or b2b_swap_flag!='1') group by b.booking_id";
        $res_source = mysqli_query($conn, $sql_source);

        if (mysqli_num_rows($res_source) >= 1) {
            ?>
            <table class="table table-striped table-bordered tablesorter table-hover">
                <thead style="background-color: #D3D3D3;">
                <!--<th>No</th>-->
                <th>BookingId</th>
                <th>CustomerName</th>
                <th>ServiceType</th>
                <th>ServiceCenter</th>
                <th>PickUp</th>
                <?php if ($status == 'booking') { ?>
                    <th>GoAxleStatus</th> <?php } ?>
                </thead>
                <tbody>
                <?php
                while ($row_source = mysqli_fetch_object($res_source)) {
                    $booking_id = $row_source->booking_id;
                    $user_id = $row_source->user_id;
                    $veh_id = $row_source->user_veh_id;
                    $res_user = mysqli_query($conn, "SELECT name FROM user_register WHERE reg_id='$user_id'");
                    $row_user = mysqli_fetch_object($res_user);
                    $user_name = $row_user->name;

                    if ($status == 'lead') {
                        $st = $row_source->booking_status;
                        switch ($st) {
                            case '1':
                                $redir = base64_encode("l");
                                break;
                            case '2':
                                $redir = base64_encode("b");
                                break;
                            case '3':
                            case '4':
                            case '5':
                            case '6':
                                $redir = base64_encode("f");
                                break;
                            case '0':
                                $redir = base64_encode("o");
                                break;
                            default:
                                $redir = base64_encode("l");
                        }
                    }
                    if ($status == 'booking') {
                        //   error_reporting(E_ALL); ini_set('display_errors', 1);

                        $sql_b2b = "SELECT DISTINCT b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_service_under_progress,b.b2b_vehicle_ready,b.b2b_vehicle_delivered,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.gb_booking_id='$booking_id' AND b.b2b_swap_flag!='1'";
                        $res_b2b = mysqli_query($conn2, $sql_b2b) or die(mysqli_error($res_b2b));
                        $row_b2b = mysqli_fetch_object($res_b2b);
                        $acpt = $row_b2b->b2b_acpt_flag;
                        $deny = $row_b2b->b2b_deny_flag;

                        if (mysqli_num_rows($res_b2b) >= 1) {
                            if ($deny == '1' && $acpt == '0') {
                                $goaxle = "Rejected";
                            } else if ($deny == '0' && $acpt == '0') {
                                $goaxle = "No Action";
                            } else {
                                $check_in_report = $row_b2b->b2b_check_in_report;
                                $vehicle_at_garage = $row_b2b->b2b_vehicle_at_garage;
                                $service_under_progress = $row_b2b->b2b_service_under_progress;
                                $vehicle_ready = $row_b2b->b2b_vehicle_ready;
                                $vehicle_delivered = $row_b2b->b2b_vehicle_delivered;


                                $check_in_report_val = $check_in_report == '0' ? "0" : "1";
                                $vehicle_at_garage_val = $vehicle_at_garage == '0' ? "0" : "1";
                                $service_under_progress_val = $service_under_progress == '0' ? "0" : "1";
                                $vehicle_ready_val = $vehicle_ready == '0' ? "0" : "1";
                                $vehicle_delivered_val = $vehicle_delivered == '0' ? "0" : "1";

                                $goaxle = $goaxles_obj->{"cr{$check_in_report_val}_vg{$vehicle_at_garage_val}_sup{$service_under_progress_val}_vr{$vehicle_ready_val}_vd{$vehicle_delivered_val}"}();
                            }
                        } else {
                            $goaxle = "Record not found!";
                        }
                    }

                    ?>
                    <tr>
                        <td><a target="_blank"
                               href="user_details.php?t=<?php echo $redir; ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>"><?php echo $booking_id; ?></a>
                        </td>
                        <td><?php echo $user_name; ?></td>
                        <td><?php echo $row_source->service_type; ?></td>
                        <td><?php echo $row_source->shop_name; ?></td>
                        <td><?php $p = $row_source->pick_up;
                            echo $pc = $p == '1' ? 'Yes' : 'No'; ?></td>
                        <?php if ($status == 'booking') { ?>
                            <td><?php echo $goaxle; ?></td> <?php } ?>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <?php
        } else {
            echo "<h4> No Records Found!</h4>";
        }
        break;

        case 'utm_source':
        $vehicle = $_POST['vehicle'];
        $UTM = $_POST['UTM'];
        $startdate = $_POST['startdate'];
        $enddate = $_POST['enddate'];
        $status = $_POST['status'];
        $city = $_POST['city'];
        $bservice_type = $_POST['bservice'];
        $bvehicle = $_POST['bvehicle'];
        if ($bvehicle != 'all') {
            $vehicle = 'all';
        }
        $bmaster_service = $_POST['master_service'];
        $crm_person = $_POST['person'];
        $bsource = $_POST['bsource'];
        // $cluster = $city != 'Chennai' ? 'all' : $_POST['cluster'];
        $cluster = 'all';
        $cond = '';
        switch ($vehicle) {
            case '2w':
                $lc = "AND l.bike_cluster='$cluster'";
                break;
            case '4w' :
                $lc = "AND l.car_cluster='$cluster'";
                break;
            default :
                $lc = "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%" . $cluster . "%'";
        }
        // if ($source == 'website') {
        //     $both_source1 = " AND b.source in ('website','bookNow')";
        // } else {
        //     $both_source1 = " AND b.source='$source'";
        // }
            $UTM = lcfirst(strtolower((end(explode(",", $UTM)))));
             if($UTM == '123'){
                $UTM = 'Mobile';
            }
        if ($bsource == 'website') {
            $both_source = "AND b.source in ('website','bookNow')";
        } else {
            $both_source = "AND b.source='$bsource'";
        }
        $cond = $cond . ($vehicle == 'all' ? "" : "AND b.vehicle_type='$vehicle'");
        $cond = $cond . ($city == 'all' ? "" : "AND b.city='$city'");
        $cond = $cond . ($UTM == 'all' ? "" : ($UTM == '' ? " AND (SUBSTRING_INDEX(SUBSTRING_INDEX(b.utm_source, ',', -1), ',', -1)=''  or isnull(SUBSTRING_INDEX(SUBSTRING_INDEX(b.utm_source, ',', -1), ',', -1)))" :  "AND SUBSTRING_INDEX(b.utm_source, ',', -1)='$UTM'"));
        // if($UTM == ''){
        //  $cond = $cond ." AND (SUBSTRING_INDEX(SUBSTRING_INDEX(b.utm_source, ',', -1), ',', -1)=''  or isnull(SUBSTRING_INDEX(SUBSTRING_INDEX(b.utm_source, ',', -1), ',', -1)))";
        // }
        $cond = $cond . ($cluster == 'all' ? "" : $lc);
        $cond = $cond . ($crm_person == 'all' ? "" : "AND b.crm_update_id='$crm_person'");
        $cond = $cond . ($bservice_type == 'all' ? "" : "AND b.service_type='$bservice_type'");
        $cond = $cond . ($bsource == 'all' ? "" : $both_source);
        $cond = $cond . ($bvehicle == 'all' ? "" : "AND b.vehicle_type='$bvehicle'");
        $cond = $cond . ($bmaster_service == 'all' ? "" : "AND gs.master_service IN ('$bmaster_service')");
        switch ($status) {
            case 'lead':
                $cond2 = " AND b.flag_unwntd!='1'";
                break;
            case 'booking':
                $cond2 = " AND b.booking_status = '2' AND b.flag!='1' AND b.flag_unwntd!='1'";
                $redir = base64_encode("b");
                break;
            case 'envconv':
                $cond2 = " AND b.booking_status = '2' AND b.flag!='1' AND b.flag_unwntd!='1' and (b.service_status in('Completed','In Progress') or bb.b2b_vehicle_at_garage='1')";
                $redir = base64_encode("b");
                break;
            case 'followup':
                $cond2 = " AND (b.booking_status IN(3,4,5,6) OR (b.booking_status='1' AND b.flag_fo='1')) AND b.flag!='1' AND b.flag_unwntd!='1'";
                $redir = base64_encode("f");
                break;
            case 'cancelled':
                $cond2 = " AND b.flag = '1' AND b.flag_unwntd!='1'";
                $redir = base64_encode("c");
                break;
            case 'others':
                $cond2 = " AND b.booking_status = '0' AND b.flag!='1' AND b.flag_unwntd!='1'";
                $redir = base64_encode("o");
                break;
            case 'idle':
                $cond2 = " AND b.booking_status = '1' AND b.flag_unwntd!='1' AND b.flag_fo!='1' AND b.flag!='1'";
                $redir = base64_encode("l");
                break;
            case 'duplicate':
                $cond2 = " AND b.activity_status = '26' AND b.flag_unwntd='1' AND b.flag='1'";
                $redir = base64_encode("c");
                break;
            default :
                $cond2 = " AND b.flag_unwntd!='1'";
                $redir = base64_encode("l");
                break;
        }
        $sql_source = $city == 'Chennai' ? "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM user_booking_tb as b LEFT JOIN localities as l ON b.locality=l.localities LEFT JOIN b2b.b2b_booking_tbl bb on bb.gb_booking_id=b.booking_id left join go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884,189598,113453,252884,296395,298600,160597,133986,270162,191883,21816,298572,110974,287322,241042,53865,289516,14485,1678,30865,125455,338469) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.source != 'Sulekha Booking' AND b.log BETWEEN '$startdate' AND '$enddate' {$cond} {$cond2} and (ISNULL(b2b_swap_flag) or b2b_swap_flag!='1') group by b.booking_id" : "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM user_booking_tb as b LEFT JOIN b2b.b2b_booking_tbl bb on bb.gb_booking_id=b.booking_id left join go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884,189598,113453,252884,296395,298600,160597,133986,270162,191883,21816,298572,110974,287322,241042,53865,289516,14485,1678,30865,125455,338469) AND b.service_type != 'IOCL Check-up' AND b.source !='Re-Engagement Bookings' AND b.source != 'Sulekha Booking' AND b.log BETWEEN '$startdate' AND '$enddate' {$cond} {$cond2} and (ISNULL(b2b_swap_flag) or b2b_swap_flag!='1') group by b.booking_id";
        //echo $sql_source;  exit;
        $res_source = mysqli_query($conn, $sql_source);
        if (mysqli_num_rows($res_source) >= 1) {
            ?>
            <table class="table table-striped table-bordered tablesorter table-hover">
                <thead style="background-color: #D3D3D3;">
                <!--<th>No</th>-->
                <th>BookingId</th>
                <th>CustomerName</th>
                <th>ServiceType</th>
                <th>ServiceCenter</th>
                <th>PickUp</th>
                <?php if ($status == 'booking') { ?>
                    <th>GoAxleStatus</th> <?php } ?>
                </thead>
                <tbody>
                <?php
                while ($row_source = mysqli_fetch_object($res_source)) {
                    $booking_id = $row_source->booking_id;
                    $user_id = $row_source->user_id;
                    $veh_id = $row_source->user_veh_id;
                    $res_user = mysqli_query($conn, "SELECT name FROM user_register WHERE reg_id='$user_id'");
                    $row_user = mysqli_fetch_object($res_user);
                    $user_name = $row_user->name;
                    
                    if ($status == 'lead') {
                        $st = $row_source->booking_status;
                        switch ($st) {
                            case '1':
                                $redir = base64_encode("l");
                                break;
                            case '2':
                                $redir = base64_encode("b");
                                break;
                            case '3':
                            case '4':
                            case '5':
                            case '6':
                                $redir = base64_encode("f");
                                break;
                            case '0':
                                $redir = base64_encode("o");
                                break;
                            default:
                                $redir = base64_encode("l");
                        }
                    }
                    if ($status == 'booking') {
                        //   error_reporting(E_ALL); ini_set('display_errors', 1);
                        $sql_b2b = "SELECT DISTINCT b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_service_under_progress,b.b2b_vehicle_ready,b.b2b_vehicle_delivered,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.gb_booking_id='$booking_id' AND b.b2b_swap_flag!='1'";
                        $res_b2b = mysqli_query($conn2, $sql_b2b) or die(mysqli_error($res_b2b));
                        $row_b2b = mysqli_fetch_object($res_b2b);
                        $acpt = $row_b2b->b2b_acpt_flag;
                        $deny = $row_b2b->b2b_deny_flag;
                        if (mysqli_num_rows($res_b2b) >= 1) {
                            if ($deny == '1' && $acpt == '0') {
                                $goaxle = "Rejected";
                            } else if ($deny == '0' && $acpt == '0') {
                                $goaxle = "No Action";
                            } else {
                                $check_in_report = $row_b2b->b2b_check_in_report;
                                $vehicle_at_garage = $row_b2b->b2b_vehicle_at_garage;
                                $service_under_progress = $row_b2b->b2b_service_under_progress;
                                $vehicle_ready = $row_b2b->b2b_vehicle_ready;
                                $vehicle_delivered = $row_b2b->b2b_vehicle_delivered;
                                $check_in_report_val = $check_in_report == '0' ? "0" : "1";
                                $vehicle_at_garage_val = $vehicle_at_garage == '0' ? "0" : "1";
                                $service_under_progress_val = $service_under_progress == '0' ? "0" : "1";
                                $vehicle_ready_val = $vehicle_ready == '0' ? "0" : "1";
                                $vehicle_delivered_val = $vehicle_delivered == '0' ? "0" : "1";
                                $goaxle = $goaxles_obj->{"cr{$check_in_report_val}_vg{$vehicle_at_garage_val}_sup{$service_under_progress_val}_vr{$vehicle_ready_val}_vd{$vehicle_delivered_val}"}();
                            }
                        } else {
                            $goaxle = "Record not found!";
                        }
                    }
                    ?>
                    <tr>
                        <td><a target="_blank"
                               href="user_details.php?t=<?php echo $redir; ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>"><?php echo $booking_id; ?></a>
                        </td>
                        <td><?php echo $user_name; ?></td>
                        <td><?php echo $row_source->service_type; ?></td>
                        <td><?php echo $row_source->shop_name; ?></td>
                        <td><?php $p = $row_source->pick_up;
                            echo $pc = $p == '1' ? 'Yes' : 'No'; ?></td>
                        <?php if ($status == 'booking') { ?>
                            <td><?php echo $goaxle; ?></td> <?php } ?>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <?php
        } else {
            echo "<h4> No Records Found!</h4>";
        }
        break;

    case 'nonconv':
        $activity = $_POST['activity'];
        $vehicle = $_POST['bvehicle'];
        $startdate = $_POST['startdate'];
        $enddate = $_POST['enddate'];
        $city = $_POST['city'];
        $nvc = $_POST['nvc'];
        $bservice_type = $_POST['bservice'];
        $bvehicle = $_POST['bvehicle'];
        $bmaster_service = $_POST['master_service'];
        $crm_person = $_POST['person'];
        $bsource = $_POST['bsource'];
        // $cluster = $city != 'Chennai' ? 'all' : $_POST['cluster'];
        $cluster = 'all';

        $cond = '';
        $cond2 = '';

        switch ($vehicle) {
            case '2w':
                $lc = "AND l.bike_cluster='$cluster'";
                break;
            case '4w' :
                $lc = "AND l.car_cluster='$cluster'";
                break;
            default :
                $lc = "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%" . $cluster . "%'";
        }

        $cond = $cond . ($vehicle == 'all' ? "" : "AND b.vehicle_type='$vehicle'");
        $cond = $cond . ($city == 'all' ? "" : "AND b.city='$city'");
        $cond = $cond . ($cluster == 'all' ? "" : $lc);
        $cond = $cond . ($nvc == '1' ? " AND b.flag='1'" : "");
        $cond = $cond . ($crm_person == 'all' ? "" : "AND b.crm_update_id='$crm_person'");
        $cond = $cond . ($bservice_type == 'all' ? "" : "AND b.service_type='$bservice_type'");
        $cond = $cond . ($bsource == 'all' ? "" : "AND b.source='$bsource'");
        $cond = $cond . ($bvehicle == 'all' ? "" : "AND b.vehicle_type='$bvehicle'");
        $cond = $cond . ($bmaster_service == 'all' ? "" : "AND gs.master_service IN ('$bmaster_service')");

        $sql_nonconv = $city == "Chennai" ? "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.flag,b.booking_status,b.user_veh_id FROM user_booking_tb as b  LEFT JOIN localities as l ON b.locality=l.localities left join b2b.b2b_booking_tbl bb on b.booking_id=bb.gb_booking_id left join go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type WHERE b.activity_status='$activity' AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884,189598,113453,252884,296395,298600,160597,133986,270162,191883,21816,298572,110974,287322,241042,53865,289516,14485,1678,30865,125455,338469) AND b.service_type != 'IOCL Check-up' AND b.source !='Re-Engagement Bookings' AND b.source != 'Sulekha Booking' AND  b.mec_id NOT IN(400001,200018,200379,400974) AND b.log BETWEEN '$startdate' AND '$enddate' {$cond} {$cond2} group by b.booking_id" : "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.flag,b.booking_status,b.user_veh_id FROM user_booking_tb as b  left join b2b.b2b_booking_tbl bb on b.booking_id=bb.gb_booking_id left join go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type WHERE b.activity_status='$activity' AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884,189598,113453,252884,296395,298600,160597,133986,270162,191883,21816,298572,110974,287322,241042,53865,289516,14485,1678,30865,125455,338469) AND b.mec_id NOT IN(400001,200018,200379,400974) AND b.service_type != 'IOCL Check-up' AND b.source !='Re-Engagement Bookings' AND b.source != 'Sulekha Booking' and b.log BETWEEN '$startdate' AND '$enddate' {$cond} {$cond2} group by b.booking_id";

        // echo $sql_nonconv;
        $res_nonconv = mysqli_query($conn, $sql_nonconv);

        if (mysqli_num_rows($res_nonconv) >= 1) {
            ?>
            <table class="table table-striped table-bordered tablesorter table-hover">
                <thead style="background-color: #D3D3D3;">
                <!--<th>No</th>-->
                <th>BookingId</th>
                <th>CustomerName</th>
                <th>ServiceType</th>
                <th>ServiceCenter</th>
                <th>PickUp</th>
                </thead>
                <tbody>
                <?php
                while ($row_nonconv = mysqli_fetch_object($res_nonconv)) {
                    $booking_id = $row_nonconv->booking_id;
                    $user_id = $row_nonconv->user_id;
                    $veh_id = $row_nonconv->user_veh_id;
                    $flag = $row_nonconv->flag;
                    $st = $row_nonconv->booking_status;

                    $res_user = mysqli_query($conn, "SELECT name FROM user_register WHERE reg_id='$user_id'");
                    $row_user = mysqli_fetch_object($res_user);
                    $user_name = $row_user->name;


                    if ($flag == '1') {
                        $redir = base64_encode("c");
                    } else if ($st == '0') {
                        $redir = base64_encode("o");
                    } else {
                        $redir = base64_encode("l");
                    }


                    ?>
                    <tr>
                        <td><a target="_blank"
                               href="user_details.php?t=<?php echo $redir; ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>"><?php echo $booking_id; ?></a>
                        </td>
                        <td><?php echo $user_name; ?></td>
                        <td><?php echo $row_nonconv->service_type; ?></td>
                        <td><?php echo $row_nonconv->shop_name; ?></td>
                        <td><?php $p = $row_nonconv->pick_up;
                            echo $pc = $p == '1' ? 'Yes' : 'No'; ?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <?php
        } else {
            echo "<h4> No Records Found!</h4>";
        }

        break;
    case 'service1':
        // echo "hai";
        // exit;
        $vehicle = $_POST['vehicle'];
        $master_service = $_POST['masterservice'];
        $startdate = $_POST['startdate'];
        $enddate = $_POST['enddate'];
        $vehicle = $_POST['vehicle'];
        $status = $_POST['status'];
        $city = $_POST['city'];
        $bservice_type = $_POST['bservice'];
        $bvehicle = $_POST['bvehicle'];
        $vehtype = $_POST['vehtype'];
        $vehicletype = $_POST['vehicletype'];
        // print_r($_POST);
        if ($bvehicle != 'all') {
            $vehicle = 'all';
        }
        $bmaster_service = $_POST['master_service'];
        // echo $bmaster_service;
        $crm_person = $_POST['person'];
        $bsource = $_POST['bsource'];
        // $cluster = $city != 'Chennai' ? 'all' : $_POST['cluster'];
        $cluster = 'all';

        $cond = '';


        switch ($vehicle) {
            case '2w':
                $lc = "AND l.bike_cluster='$cluster'";
                break;
            case '4w' :
                $lc = "AND l.car_cluster='$cluster'";
                break;
            default :
                $lc = "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%" . $cluster . "%'";
        }

        // if ($bsource == 'website') {
        //     $both_source = "AND b.source in ('website','bookNow')";
        // } else {
        //     $both_source = "AND b.source='$bsource'";
        // }
        $cond = $cond . ($vehicle == 'all' ? "" : "AND b.vehicle_type='$vehicle'");
        $cond = $cond . ($vehtype == 'all' ? "" : "AND b.vehicle_type='$vehtype'");
        $cond = $cond . ($cluster == 'all' ? "" : $lc);
        $cond = $cond . ($vehicletype == 'all' ? "" : " AND gs.vehicle_type ='$vehicletype'");
        $cond = $cond . ($city == 'all' ? "" : " AND b.city='$city'");
        $cond = $cond . ($crm_person == 'all' ? "" : " AND b.crm_update_id='$crm_person'");
        $cond = $cond . ($bservice_type == 'all' ? "" : " AND b.service_type='$bservice_type'");
        $cond = $cond . ($bsource == 'all' ? "" : $both_source);
        $cond = $cond . ($bvehicle == 'all' ? "" : " AND b.vehicle_type='$bvehicle'");
        $cond = $cond . ($bmaster_service == 'all' ? "" : "   AND gs.master_service IN ('$bmaster_service')");
        if ($master_service != 'all') {
            $conn1 = db_connect1();
            $sql_serv = "SELECT service_type FROM go_axle_service_price_tbl WHERE master_service='$master_service'";
            $res_serv = mysqli_query($conn1, $sql_serv);
            $services = '';
            while ($row_serv = mysqli_fetch_array($res_serv)) {
                if ($services == "") {
                    $services = $row_serv['service_type'] . "'";
                } else {
                    $services = $services . ",'" . $row_serv['service_type'] . "'";
                }
            }
             $cond = $cond . " AND b.service_type IN('$services)";

        }

        switch ($status) {

            case 'Followuplead':
                $cond2 = " AND b.flag_unwntd!='1'";
                $redir = base64_encode("l");
                break;
            case 'Goaxle':
                $cond2 = " AND b.booking_status = '2' AND b.flag!='1' AND b.flag_unwntd!='1' AND b.axle_flag='1' and b.flag_unwntd!='1'";
                $redir = base64_encode("b");
                break;
            case 'Endconversion':
                $cond2 = " AND b.booking_status='2' and b.flag_unwntd!='1' and b.flag!='1' and (b.service_status='Completed' or b.service_status='In Progress') and b.flag_unwntd!='1'";
                $redir = base64_encode("e");
                break;
            case 'idle':
                $cond2 = " AND (b.booking_status='3' or b.booking_status='4' or b.booking_status='5' or b.booking_status='6'or b.booking_status='1') and b.flag!='1' and b.flag_unwntd!='1'";
                $redir = base64_encode("f");
                break;
            case 'others':
                $cond2 = " And b.booking_status='0' and b.flag!='1' and b.flag_unwntd!='1'";
                $redir = base64_encode("o");
                break;
            case 'cancel':
                $cond2 = " AND b.flag='1' and b.flag_unwntd!='1'";
                $redir = base64_encode("c");
                break;
            default :
                $cond2 = " AND b.flag_unwntd!='1'";
                $redir = base64_encode("l");
                break;
        }
        $ser_query = $city == 'Chennai' ? "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM user_booking_tb as b left join go_bumpr.user_vehicle_table as uv on b.user_veh_id=uv.id left join go_bumpr.go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type  WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884,189598,113453,252884,296395,298600,160597,133986,270162,191883,21816,298572,110974,287322,241042,53865,289516,14485,1678,30865,125455,338469) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.source != 'Sulekha Booking' and (case when uv.brand='Royal Enfield' then gs.vehicle_type='RE' else gs.vehicle_type in('NRE','')end) and b.followup_date BETWEEN '$startdate' AND '$enddate' {$cond} {$cond2} group by b.booking_id" : "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM user_booking_tb as b  left join go_bumpr.user_vehicle_table as uv on b.user_veh_id=uv.id left join go_bumpr.go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type  WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884,189598,113453,252884,296395,298600,160597,133986,270162,191883,21816,298572,110974,287322,241042,53865,289516,14485,1678,30865,125455,338469) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.source != 'Sulekha Booking' and (case when uv.brand='Royal Enfield' then gs.vehicle_type='RE' else gs.vehicle_type in('NRE','')end) and b.followup_date BETWEEN '$startdate' AND '$enddate' {$cond} {$cond2} group by b.booking_id";

        // echo $ser_query;exit;
        $ser_row = mysqli_query($conn, $ser_query);

        if (mysqli_num_rows($ser_row) >= 1) {
            ?>
            <table class="table table-striped table-bordered tablesorter table-hover">
                <thead style="background-color: #D3D3D3;">
                <!--<th>No</th>-->
                <th>BookingId</th>
                <th>CustomerName</th>
                <th>ServiceType</th>
                <th>ServiceCenter</th>
                <th>PickUp</th>
                <?php if ($status == 'booking') { ?>
                    <th>GoAxleStatus</th> <?php } ?>
                </thead>
                <tbody>
                <?php
                while ($row_ser = mysqli_fetch_object($ser_row)) {
                    $booking_id = $row_ser->booking_id;
                    $user_id = $row_ser->user_id;
                    $veh_id = $row_ser->user_veh_id;
                    $res_user = mysqli_query($conn, "SELECT name FROM user_register WHERE reg_id='$user_id'");
                    $row_user = mysqli_fetch_object($res_user);
                    $user_name = $row_user->name;

                    if ($status == 'lead') {
                        $st = $row_ser->booking_status;
                        switch ($st) {
                            case '1':
                                $redir = base64_encode("l");
                            case '2':
                                $redir = base64_encode("b");
                                break;
                            case '3':
                                $redir = base64_encode("e");
                            case '4':
                                $redir = base64_encode("f");
                            case '5':
                                $redir = base64_encode("o");
                            case '6':
                                $redir = base64_encode("c");
                                break;
                            default:
                                $redir = base64_encode("l");
                        }
                    }
                    if ($status == 'booking') {
                        //   error_reporting(E_ALL); ini_set('display_errors', 1);

                        $sql_b2b = "SELECT DISTINCT b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_service_under_progress,b.b2b_vehicle_ready,b.b2b_vehicle_delivered,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.gb_booking_id='$booking_id' AND b.b2b_swap_flag!='1'";
                        $res_b2b = mysqli_query($conn2, $sql_b2b) or die(mysqli_error($res_b2b));
                        $row_b2b = mysqli_fetch_object($res_b2b);
                        $acpt = $row_b2b->b2b_acpt_flag;
                        $deny = $row_b2b->b2b_deny_flag;

                        if (mysqli_num_rows($res_b2b) >= 1) {
                            if ($deny == '1' && $acpt == '0') {
                                $goaxle = "Rejected";
                            } else if ($deny == '0' && $acpt == '0') {
                                $goaxle = "No Action";
                            } else {
                                $check_in_report = $row_b2b->b2b_check_in_report;
                                $vehicle_at_garage = $row_b2b->b2b_vehicle_at_garage;
                                $service_under_progress = $row_b2b->b2b_service_under_progress;
                                $vehicle_ready = $row_b2b->b2b_vehicle_ready;
                                $vehicle_delivered = $row_b2b->b2b_vehicle_delivered;

                                $check_in_report_val = $check_in_report == '0' ? "0" : "1";
                                $vehicle_at_garage_val = $vehicle_at_garage == '0' ? "0" : "1";
                                $service_under_progress_val = $service_under_progress == '0' ? "0" : "1";
                                $vehicle_ready_val = $vehicle_ready == '0' ? "0" : "1";
                                $vehicle_delivered_val = $vehicle_delivered == '0' ? "0" : "1";

                                $goaxle = $goaxles_obj->{"cr{$check_in_report_val}_vg{$vehicle_at_garage_val}_sup{$service_under_progress_val}_vr{$vehicle_ready_val}_vd{$vehicle_delivered_val}"}();
                            }
                        } else {
                            $goaxle = "Record not found!";
                        }
                    }
                    ?>
                    <tr>
                        <td><a target="_blank"
                               href="user_details.php?t=<?php echo $redir; ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>"><?php echo $booking_id; ?></a>
                        </td>
                        <td><?php echo $user_name; ?></td>
                        <td><?php echo $row_ser->service_type; ?></td>
                        <td><?php echo $row_ser->shop_name; ?></td>
                        <td><?php $p = $row_ser->pick_up;
                            echo $pc = $p == '1' ? 'Yes' : 'No'; ?></td>
                        <?php if ($status == 'booking') { ?>
                            <td><?php echo $goaxle; ?></td> <?php } ?>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <?php
        } else {
            echo "<h4> No Records Found!</h4>";
        }
        break;
    case 'person1':

        $crmlogid = $_POST['crmlogid'];
        $startdate = $_POST['startdate'];
        $enddate = $_POST['enddate'];
        $status = $_POST['status'];
        $city = $_POST['city'];
        $bservice_type = $_POST['bservice'];
        $bvehicle = $_POST['vehicle'];
        $bmaster_service = $_POST['master_service'];
        $crm_person = $_POST['person'];
        $bsource = $_POST['bsource'];
        // print_r($_POST);
        // echo $vehtype;
        // $cluster = $city != 'Chennai' ? 'all' : $_POST['cluster'];
        $cluster = 'all';

        switch ($status) {

            case 'Followuplead' :
                $cond = " AND b.flag_unwntd!='1'";
                $redir = base64_encode("l");
                break;
            case 'Goaxle':
                $cond = " AND b.booking_status = '2' AND b.flag!='1' AND b.flag_unwntd!='1' AND b.axle_flag='1' and b.flag_unwntd!='1'";
                $redir = base64_encode("b");
                break;
            case 'Endconversion':
                $cond = " AND b.booking_status='2' and b.flag_unwntd!='1' and b.flag!='1' and (b.service_status='Completed' or b.service_status='In Progress') and b.flag_unwntd!='1'";
                $redir = base64_encode("e");
                break;
            case 'idle':
                $cond = " AND (b.booking_status='3' or b.booking_status='4' or b.booking_status='5' or b.booking_status='6' or b.booking_status='1') and b.flag_unwntd!='1' and b.flag!='1' ";
                $redir = base64_encode("f");
                break;
            case 'others':
                $cond = " And b.booking_status='0' and b.flag!='1' and b.flag_unwntd!='1'";
                $redir = base64_encode("o");
                break;
            case 'cancel':
                $cond = " AND b.flag='1' and b.flag_unwntd!='1'";
                $redir = base64_encode("c");
                break;
            default :
                $cond = " AND b.flag_unwntd!='1'";
                $redir = base64_encode("l");
                break;
        }

        if ($bsource == 'website') {
            $both_source = "AND b.source in ('website','bookNow')";
        } else {
            $both_source = "AND b.source='$bsource'";
        }

        $cond2 = $crmlogid == 'all' ? "" : " AND b.crm_update_id='$crmlogid' ";
        $cond2 = $cond2 . ($city == 'all' ? "" : "AND b.city='$city'");
        $cond2 = $cond2 . ($cluster == 'all' ? "" : "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%" . $cluster . "%'");
        $cond2 = $cond2 . ($crm_person == 'all' ? "" : "AND b.crm_update_id='$crm_person'");
        $cond2 = $cond2 . ($bservice_type == 'all' ? "" : "AND b.service_type='$bservice_type'");
        $cond2 = $cond2 . ($bsource == 'all' ? "" : $both_source);
        $cond2 = $cond2 . ($bvehicle == 'all' ? "" : "AND b.vehicle_type='$bvehicle'");
        // $cond2 = $cond2 . ($bmaster_service == 'all' ? "" : "AND gs.master_service IN ('$bmaster_service')");


        $per_query = $city == "Chennai" ? "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM user_booking_tb as b left join go_bumpr.user_vehicle_table as uv on b.user_veh_id=uv.id left join go_bumpr.go_axle_service_price_tbl d ON trim(b.service_type)=trim(d.service_type) AND b.vehicle_type=d.type WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884,189598,113453,252884,296395,298600,160597,133986,270162,191883,21816,298572,110974,287322,241042,53865,289516,14485,1678,30865,125455,338469) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.source != 'Sulekha Booking' AND b.followup_date BETWEEN '$startdate' AND '$enddate' and (case when uv.brand='Royal Enfield' then d.vehicle_type='RE' else d.vehicle_type in('NRE','')end) {$cond2}{$cond} group by b.booking_id " : "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM user_booking_tb as b left join go_bumpr.user_vehicle_table as uv on b.user_veh_id=uv.id left join go_bumpr.go_axle_service_price_tbl d ON trim(b.service_type)=trim(d.service_type) AND b.vehicle_type=d.type WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884,189598,113453,252884,296395,298600,160597,133986,270162,191883,298572,110974,287322,241042,53865,289516,14485,1678,30865,125455,338469) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.source != 'Sulekha Booking' AND b.followup_date BETWEEN '$startdate' AND '$enddate' and (case when uv.brand='Royal Enfield' then d.vehicle_type='RE' else d.vehicle_type in('NRE','')end) {$cond2}{$cond} group by b.booking_id ";
        // echo $per_query;exit;
        $per_row = mysqli_query($conn, $per_query);

        if (mysqli_num_rows($per_row) >= 1) {
            ?>
            <table class="table table-striped table-bordered tablesorter table-hover">
                <thead style="background-color: #D3D3D3;">
                <!--<th>No</th>-->
                <th>BookingId</th>
                <th>CustomerName</th>
                <th>ServiceType</th>
                <th>ServiceCenter</th>
                <th>PickUp</th>
                <?php if ($status == 'booking') { ?>
                    <th>GoAxleStatus</th> <?php } ?>
                </thead>
                <tbody>
                <?php
                while ($row_per = mysqli_fetch_object($per_row)) {
                    $booking_id = $row_per->booking_id;
                    $user_id = $row_per->user_id;
                    $veh_id = $row_per->user_veh_id;
                    $res_user = mysqli_query($conn, "SELECT name FROM user_register WHERE reg_id='$user_id'");
                    $row_user = mysqli_fetch_object($res_user);
                    $user_name = $row_user->name;

                    if ($status == 'lead') {
                        $st = $row_per->booking_status;
                        switch ($st) {
                            case '1':
                                $redir = base64_encode("b");
                                break;
                            case '2':
                                $redir = base64_encode("l");
                                break;
                            case '3':
                                $redir = base64_encode("e");
                                break;
                            case '4':
                                $redir = base64_encode("f");
                                break;
                            case '5':
                                $redir = base64_encode("o");
                                break;
                            case '6':
                                $redir = base64_encode("c");
                                break;                     
                            default:
                                $redir = base64_encode("l");
                        }
                    }


                    if ($status == 'booking') {
                        //   error_reporting(E_ALL); ini_set('display_errors', 1);

                        $sql_b2b = "SELECT DISTINCT b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_service_under_progress,b.b2b_vehicle_ready,b.b2b_vehicle_delivered,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.gb_booking_id='$booking_id' AND b.b2b_swap_flag!='1'";
                        $res_b2b = mysqli_query($conn2, $sql_b2b) or die(mysqli_error($res_b2b));
                        $row_b2b = mysqli_fetch_object($res_b2b);
                        $acpt = $row_b2b->b2b_acpt_flag;
                        $deny = $row_b2b->b2b_deny_flag;

                        if (mysqli_num_rows($res_b2b) >= 1) {
                            if ($deny == '1' && $acpt == '0') {
                                $goaxle = "Rejected";
                            } else if ($deny == '0' && $acpt == '0') {
                                $goaxle = "No Action";
                            } else {
                                $check_in_report = $row_b2b->b2b_check_in_report;
                                $vehicle_at_garage = $row_b2b->b2b_vehicle_at_garage;
                                $service_under_progress = $row_b2b->b2b_service_under_progress;
                                $vehicle_ready = $row_b2b->b2b_vehicle_ready;
                                $vehicle_delivered = $row_b2b->b2b_vehicle_delivered;

                                $check_in_report_val = $check_in_report == '0' ? "0" : "1";
                                $vehicle_at_garage_val = $vehicle_at_garage == '0' ? "0" : "1";
                                $service_under_progress_val = $service_under_progress == '0' ? "0" : "1";
                                $vehicle_ready_val = $vehicle_ready == '0' ? "0" : "1";
                                $vehicle_delivered_val = $vehicle_delivered == '0' ? "0" : "1";

                                $goaxle = $goaxles_obj->{"cr{$check_in_report_val}_vg{$vehicle_at_garage_val}_sup{$service_under_progress_val}_vr{$vehicle_ready_val}_vd{$vehicle_delivered_val}"}();
                            }
                        } else {
                            $goaxle = "Record not found!";
                        }
                    }
                    ?>
                    <tr>
                        <td><a target="_blank"
                               href="user_details.php?t=<?php echo $redir; ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>"><?php echo $booking_id; ?></a>
                        </td>
                        <td><?php echo $user_name; ?></td>
                        <td><?php echo $row_per->service_type; ?></td>
                        <td><?php echo $row_per->shop_name; ?></td>
                        <td><?php $p = $row_per->pick_up;
                            echo $pc = $p == '1' ? 'Yes' : 'No'; ?></td>
                        <?php if ($status == 'booking') { ?>
                            <td> <?php echo $goaxle; ?></td> <?php } ?>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <?php
        } else {
            echo "<h4> No Records Found!</h4>";
        }
        break;

    case 'source1':
        // print_r($_POST);exit();
        $vehicle = $_POST['vehicle'];
        $source = $_POST['source'];
        $startdate = $_POST['startdate'];
        $enddate = $_POST['enddate'];
        $status = $_POST['status'];
        $city = $_POST['city'];
        $bservice_type = $_POST['bservice'];
        $bvehicle = $_POST['bvehicle'];
        // $vehtype = $_POST['vehtype'];
        // print_r($_POST);
        if ($bvehicle != 'all') {
            $vehicle = 'all';
        }
        $bmaster_service = $_POST['master_service'];
        $crm_person = $_POST['person'];
        $bsource = $_POST['bsource'];
        // $cluster = $city != 'Chennai' ? 'all' : $_POST['cluster'];
        $cluster = 'all';
        $cond = '';

        switch ($vehicle) {
            case '2w':
                $lc = "AND l.bike_cluster='$cluster'";
                break;
            case '4w' :
                $lc = "AND l.car_cluster='$cluster'";
                break;
            default :
                $lc = "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%" . $cluster . "%'";
        }

        $cond = $cond . ($vehicle == 'all' ? "" : " AND b.vehicle_type='$vehicle'");
        $cond = $cond . ($city == 'all' ? "" : " AND b.city='$city'");
        if ($source == 'Website') {
            $cond = $cond . ($source == 'all' ? "" : " AND ISNULL(ub.user_source)");
        } else {
            $cond = $cond . ($source == 'all' ? "" : " AND b.source='$source'");
        }
        $cond = $cond . ($cluster == 'all' ? "" : $lc);
        $cond = $cond . ($crm_person == 'all' ? "" : " AND b.crm_update_id='$crm_person'");
        $cond = $cond . ($bservice_type == 'all' ? "" : " AND b.service_type='$bservice_type'");
        $cond = $cond . ($bsource == 'all' ? "" : $both_source);
        $cond = $cond . ($bvehicle == 'all' ? "" : " AND b.vehicle_type='$bvehicle'");
        $cond = $cond . ($bmaster_service == 'all' ? "" : " AND gs.master_service IN ('$bmaster_service')");

        switch ($status) {

            case 'Followuplead':
                $cond2 = " AND b.flag_unwntd!='1'";
                $redir = base64_encode("l");
                break;
            case 'Goaxle':
                $cond2 = " AND b.booking_status = '2' AND b.flag!='1' AND b.flag_unwntd!='1' AND b.axle_flag='1' and b.flag_unwntd!='1'";
                $redir = base64_encode("b");
                break;
            case 'Endconversion':
                $cond2 = " AND b.booking_status='2' and b.flag_unwntd!='1' and b.flag!='1' and (b.service_status='Completed' or b.service_status='In Progress') and b.flag_unwntd!='1'";
                $redir = base64_encode("e");
                break;
            case 'idle':
                $cond2 = " AND (b.booking_status='3' or b.booking_status='4' or b.booking_status='5' or b.booking_status='6'or b.booking_status='1') and b.flag_unwntd!='1' and b.flag!='1'";
                $redir = base64_encode("f");
                break;
            case 'others':
                $cond2 = " And b.booking_status='0' and b.flag!='1' and b.flag_unwntd!='1'";
                $redir = base64_encode("o");
                break;
            case 'cancel':
                $cond2 = " AND b.flag='1' and b.flag_unwntd!='1'";
                $redir = base64_encode("c");
                break;
            default :
                $cond2 = " AND b.flag_unwntd!='1'";
                $redir = base64_encode("l");
                break;
        }

        $src_query = $city == 'Chennai' ? "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM user_booking_tb as b left join go_bumpr.user_vehicle_table as uv on b.user_veh_id=uv.id left join go_bumpr.user_source_tbl ub on b.source=ub.user_source left join go_bumpr.go_axle_service_price_tbl d ON trim(b.service_type)=trim(d.service_type) AND b.vehicle_type=d.type WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884,189598,113453,252884,296395,298600,160597,133986,270162,191883,21816,298572,110974,287322,241042,53865,289516,14485,1678,30865,125455,338469) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.source != 'Sulekha Booking' AND b.followup_date BETWEEN '$startdate' AND '$enddate' and (case when uv.brand='Royal Enfield' then d.vehicle_type='RE' else d.vehicle_type in('NRE','')end)  {$cond} {$cond2} group by b.booking_id" : "SELECT DISTINCT b.booking_id,b.user_id,b.shop_name,b.service_type,b.pick_up,b.booking_status,b.user_veh_id FROM user_booking_tb as b left join go_bumpr.user_vehicle_table as uv on b.user_veh_id=uv.id left join go_bumpr.user_source_tbl ub on b.source=ub.user_source left join go_bumpr.go_axle_service_price_tbl d ON trim(b.service_type)=trim(d.service_type) AND b.vehicle_type=d.type WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884,189598,113453,252884,296395,298600,160597,133986,270162,191883,21816,298572,110974,287322,241042,53865,289516,14485,1678,30865,125455,338469) AND b.service_type != 'IOCL Check-up' AND b.source !='Re-Engagement Bookings' AND b.source != 'Sulekha Booking' AND b.followup_date BETWEEN '$startdate' AND '$enddate' and (case when uv.brand='Royal Enfield' then d.vehicle_type='RE' else d.vehicle_type in('NRE','')end) {$cond} {$cond2} group by b.booking_id";
        // echo $src_query;exit();

        $src_row = mysqli_query($conn, $src_query);

        if (mysqli_num_rows($src_row) >= 1) {
            ?>
            <table class="table table-striped table-bordered tablesorter table-hover">
                <thead style="background-color: #D3D3D3;">
                <!--<th>No</th>-->
                <th>BookingId</th>
                <th>CustomerName</th>
                <th>ServiceType</th>
                <th>ServiceCenter</th>
                <th>PickUp</th>
                <?php if ($status == 'booking') { ?>
                    <th>GoAxleStatus</th> <?php } ?>
                </thead>
                <tbody>
                <?php
                while ($row_src = mysqli_fetch_object($src_row)) {
                    $booking_id = $row_src->booking_id;
                    $user_id = $row_src->user_id;
                    $veh_id = $row_src->user_veh_id;
                    $res_user = mysqli_query($conn, "SELECT name FROM user_register WHERE reg_id='$user_id'");
                    $row_user = mysqli_fetch_object($res_user);
                    $user_name = $row_user->name;

                    if ($status == 'lead') {
                        $st = $row_source->booking_status;
                        switch ($st) {
                            case '1':
                                $redir = base64_encode("b");
                                break;
                            case '2':
                                $redir = base64_encode("l");
                                break;
                            case '3':
                               $redir = base64_encode("e");
                                break;
                            case '4':
                                $redir = base64_encode("f");
                                break;
                            case '5':
                                $redir = base64_encode("o");
                                break;
                            case '6':
                                $redir = base64_encode("c");
                                break;
                            default:
                                $redir = base64_encode("l");
                        }
                    }
                    if ($status == 'booking') {
                        //   error_reporting(E_ALL); ini_set('display_errors', 1);

                        $sql_b2b = "SELECT DISTINCT b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_service_under_progress,b.b2b_vehicle_ready,b.b2b_vehicle_delivered,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.gb_booking_id='$booking_id' AND b.b2b_swap_flag!='1'";
                        $res_b2b = mysqli_query($conn2, $sql_b2b) or die(mysqli_error($res_b2b));
                        $row_b2b = mysqli_fetch_object($res_b2b);
                        $acpt = $row_b2b->b2b_acpt_flag;
                        $deny = $row_b2b->b2b_deny_flag;

                        if (mysqli_num_rows($res_b2b) >= 1) {
                            if ($deny == '1' && $acpt == '0') {
                                $goaxle = "Rejected";
                            } else if ($deny == '0' && $acpt == '0') {
                                $goaxle = "No Action";
                            } else {
                                $check_in_report = $row_b2b->b2b_check_in_report;
                                $vehicle_at_garage = $row_b2b->b2b_vehicle_at_garage;
                                $service_under_progress = $row_b2b->b2b_service_under_progress;
                                $vehicle_ready = $row_b2b->b2b_vehicle_ready;
                                $vehicle_delivered = $row_b2b->b2b_vehicle_delivered;


                                $check_in_report_val = $check_in_report == '0' ? "0" : "1";
                                $vehicle_at_garage_val = $vehicle_at_garage == '0' ? "0" : "1";
                                $service_under_progress_val = $service_under_progress == '0' ? "0" : "1";
                                $vehicle_ready_val = $vehicle_ready == '0' ? "0" : "1";
                                $vehicle_delivered_val = $vehicle_delivered == '0' ? "0" : "1";

                                $goaxle = $goaxles_obj->{"cr{$check_in_report_val}_vg{$vehicle_at_garage_val}_sup{$service_under_progress_val}_vr{$vehicle_ready_val}_vd{$vehicle_delivered_val}"}();
                            }
                        } else {
                            $goaxle = "Record not found!";
                        }
                    }

                    ?>
                    <tr>
                        <td><a target="_blank"
                               href="user_details.php?t=<?php echo $redir; ?>&v58i4=<?php echo base64_encode($veh_id); ?>&bi=<?php echo base64_encode($booking_id); ?>"><?php echo $booking_id; ?></a>
                        </td>
                        <td><?php echo $user_name; ?></td>
                        <td><?php echo $row_src->service_type; ?></td>
                        <td><?php echo $row_src->shop_name; ?></td>
                        <td><?php $p = $row_src->pick_up;
                            echo $pc = $p == '1' ? 'Yes' : 'No'; ?></td>
                        <?php if ($status == 'booking') { ?>
                            <td><?php echo $goaxle; ?></td> <?php } ?>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <?php
        } else {
            echo "<h4> No Records Found!</h4>";
        }
        break;
    default:
        echo "<h4>Sorry! No records to Display!!!</h4>";
        break;
}


?>
<?php

// error_reporting(E_ALL);
// ini_set('display_errors', 1);
include('../config.php');
$conn1 = db_connect1();
$conn2 = db_connect2();
$type = $_POST['selecttype'];
//$service = $_POST['selectservice'];
$loc = $_POST['selectloc'];
$veh_no = $_POST['veh_no'];
$service = $_POST['service'];
$crm = $_POST['crm'];
$city = $_POST['city'];
$latitude=$_POST['latitude'];
$longtitude=$_POST['longtitude'];

?>
<?php
function humanTiming($time)
{
	$time = time() - $time; // to get the time since that moment
	echo $time;
	$time = ($time < 1) ? 1 : $time;
	$tokens = array(
		31536000 => 'year',
		2592000 => 'month',
		604800 => 'week',
		86400 => 'day',
		3600 => 'hour',
		60 => 'minute',
		1 => 'second'
	);
	
	foreach ($tokens as $unit => $text) {
		if ($time < $unit) continue;
		$numberOfUnits = floor($time / $unit);
		$time_val = $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
		if ($time_val == "49 years") {
			$time_value = "No Goaxles";
			return $time_value;
		} else {
			return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
		}
		//return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
	}
	
}

function GetDrivingDistance($o_lat, $o_long, $d_lat_lng)
{
	$url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyBEFvPWpawLCHyQQQR_4Qx4kKeyojpDw7I&origins=" . $o_lat . "," . $o_long . "&destinations=" . $d_lat_lng . "&mode=driving";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$response = curl_exec($ch);
	curl_close($ch);
	$response_a = json_decode($response, true);
	// return $response_a['rows'][0]['elements'];
	
	$distanceArr = array();
	if ($response_a['status'] == 'OK') {
		foreach ($response_a['rows'][0]['elements'] as $ele) {
			array_push($distanceArr, $ele['distance']['text']);
		}
	}
	return $distanceArr;
	// if ($response_a['rows'][0]['elements'][0]['status'] == 'OK') {
	// 	$dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
	// 	$time = $response_a['rows'][0]['elements'][0]['duration']['text'];
	// }
	// return array('distance' => $dist, 'time' => $time);
	// return $dist;
}

$sql_brand = "SELECT u.brand,u.type FROM user_vehicle_table u WHERE u.id = '$veh_no'";
$res_brand = mysqli_query($conn1, $sql_brand);
$row_brand = mysqli_fetch_object($res_brand);

$brand = $row_brand->brand;
if ($brand == 'Royal Enfield') {
	$vtype = 'RE';
} else {
	$vtype = 'NRE';
}

if ($city != '') {
	$sql_get_ll = "SELECT lat,lng FROM localities WHERE localities='$loc' and city = '$city'";
} else {
	$sql_get_ll = "SELECT lat,lng FROM localities WHERE localities='$loc';";
}
$res_get_ll = mysqli_query($conn1, $sql_get_ll);
$row_get_ll = mysqli_fetch_array($res_get_ll);

// $lat = $row_get_ll['lat'];
// $lng = $row_get_ll['lng'];
$lat = $latitude;
$lng = $longtitude;


if ($type == '2w') {
	$sql_service = "SELECT service_type_column FROM go_axle_service_price_tbl WHERE service_type='$service' and vehicle_type = '$vtype' AND bridge_flag='0' and type='2w'";
} else {
	$sql_service = "SELECT service_type_column FROM go_axle_service_price_tbl WHERE service_type='$service' AND type='4w'";
}

$res_service = mysqli_query($conn1, $sql_service);
$row_service = mysqli_fetch_array($res_service);

$service_col = $row_service['service_type_column'];
$pic_range = array("Grade A (Pickup Radius)" => "5");

if ($brand == 'Royal Enfield') {
	$cond = $cond . " AND m.re_wkly_counter > 0 ";
} else {
	$cond = $cond . " AND m.wkly_counter > 0";
}

switch ($type) {
	case "2w":
		foreach ($pic_range as $pick_key => $pick_val) {
			$query = "SELECT DISTINCT ct.b2b_shop_id, m.premium, b.b2b_new_flg, m.mec_id, m.shop_name, m.address4, m.axle_id, m.wkly_counter, m.flag, m.brand2, m.type, ct.b2b_leads, ct.b2b_credits, wkly_leads, b2b_re_leads, b2b_non_re_leads, m.lat, m.lng, b2b_log, (6371*ACOS(COS(RADIANS('$lat'))*COS(RADIANS(m.lat))*COS(RADIANS(m.lng)-RADIANS('$lng'))+SIN(RADIANS('$lat'))*SIN(RADIANS(m.lat)))) AS dist
FROM go_bumpr.admin_mechanic_table AS m
INNER JOIN go_bumpr.admin_service_type AS s ON m.mec_id=s.mec_id
JOIN b2b.b2b_mec_tbl b ON b.b2b_shop_id = m.axle_id
JOIN b2b.b2b_credits_tbl ct ON m.axle_id = ct.b2b_shop_id
WHERE b.b2b_avail = 1 AND (m.type='2w' OR (m.type='4w' AND s.prem_bik_dtlg_1499='1')) AND m.axle_id>1000 AND (6371*ACOS(COS(RADIANS('$lat'))*COS(RADIANS(m.lat))*COS(RADIANS(m.lng)-RADIANS('$lng'))+SIN(RADIANS('$lat'))*SIN(RADIANS(m.lat)))) <= 8 AND m.status!='1' AND s.$service_col='1' {$cond} AND ((b.b2b_new_flg =0 AND ct.b2b_credits > 0) OR (b.b2b_new_flg = 1 AND ct.b2b_leads > 0))
ORDER BY b.b2b_log asc";
			
			$result = mysqli_query($conn1, $query);
			?>
			<option value="">Select Mechanic</option>
			
			<?php
			$rows = [];
			while ($row = mysqli_fetch_object($result)) {
				$rows[] = $row;
			}
			
			foreach ($rows as $row) {
				$mec_id = $row->mec_id;
				$shop_name = $row->shop_name;
				$mec_type = $row->type;
				$address4 = $row->address4;
				$axle_id = $row->axle_id;
				$distance = $row->dist;
				$brand_serviced = $row->brand2;
				$brand_array = explode(',', $brand_serviced);
				//parse_str($brand_serviced, $brand_array);
				//print_r($arr);
				$new_flag = $row->b2b_new_flg;
				$shop_lat = $row->lat;
				$shop_lng = $row->lng;
				$last_lead = $row->b2b_log;
				$premium = $row->premium;
				
				if ($brand == "Royal Enfield") {
					$counter = $row->re_wkly_counter;
					
				} else {
					$counter = $row->wkly_counter;
				}
				$flag = $row->flag;
				
				if ($brand_serviced != "all" && !in_array($brand, $brand_array)) {
					continue;
				}
				$sql_crd = "SELECT * FROM (SELECT wkly_leads,b2b_credits,b2b_leads,b2b_re_leads,b2b_non_re_leads,ct.b2b_shop_id FROM go_bumpr.admin_mechanic_table adm join b2b_credits_tbl ct on adm.axle_id=ct.b2b_shop_id WHERE ct.b2b_shop_id='$axle_id')A
			LEFT JOIN
			(SELECT b2b_log,b2b_shop_id from b2b_booking_tbl where b2b_shop_id='$axle_id' and b2b_swap_flag!='1'  order by b2b_log DESC limit 1) B on A.b2b_shop_id=B.b2b_shop_id
			LEFT JOIN
			(SELECT count(b2b_booking_id) as wkly_counter,b2b_shop_id from b2b_booking_tbl where b2b_shop_id='$axle_id' and cast(b2b_log as date)=CURDATE() and b2b_swap_flag!='1')C ON C.b2b_shop_id=B.b2b_shop_id group by A.b2b_shop_id";
				//echo $sql_crd;
				$res_crd = mysqli_query($conn2, $sql_crd);
				$row_count = mysqli_num_rows($res_crd);
				$row_crd = mysqli_fetch_object($res_crd);
				if ($row_crd->wkly_counter == "") {
					$ga = '0';
				} else {
					$ga = $row_crd->wkly_counter;
				}
				$wkly_leads = $row_crd->re_wkly_leads;
				
				$time = strtotime($row_crd->b2b_log);
				$elapsed_time = humanTiming($time);
				
				if ($row_count <= 0) {
					$leads = '-';
					$credits = '-';
				} else {
					if ($new_flag == 1) {
						if ($vtype == "RE") {
							if ($mec_type == '4w') {
								$leads = $row_crd->b2b_leads;
							} else {
								$leads = $row_crd->b2b_re_leads;
							}
						} else {
							if ($mec_type == '4w') {
								$leads = $row_crd->b2b_leads;
							} else {
								$leads = $row_crd->b2b_non_re_leads;
							}
						}
						if ($leads <= 0) {
							echo $shop_name . " leads not available";
							continue;
						}
						$credits = 0;
					} else {
						$credits = $row_crd->b2b_credits;
						if ($credits <= 0) {
							echo $shop_name . " credits not available";
							continue;
						}
						$leads = 0;
					}
				}
				
				$distanceKM = getDrivingDistance($lat, $shop_lat, $lng, $shop_lng);
				$mec_array_loc[] = array("mec_id" => $mec_id, "shop_name" => $shop_name, "address4" => $address4, "leads" => $leads, "credits" => $credits, "new_flag" => $new_flag, "lat_lng" => $shop_lat . "," . $shop_lng, "ga" => $ga, "wkly_leads" => $wkly_leads, "elapsed_time" => $elapsed_time, "last_sent" => $row_crd->b2b_log);
			}
			
			$lat_lng = array_column($mec_array_loc, 'lat_lng');
			$lat_lng_str = implode($lat_lng, '|');
			$distanceKM = getDrivingDistance($lat, $lng, $lat_lng_str);
			
			$new_array = [];
			foreach ($distanceKM as $key => $value) {
				$mec_array_loc[$key]['distance'] = $value;
				unset($mec_array_loc[$key]['lat_lng']);
			}
			
			foreach ($mec_array_loc as $key => $row) {
				// $crd[$key] = print_r($row['credits']);
				$lead[$key] = $row['leads'];
				$crd[$key] = $row['credits'];
				$dist[$key] = $row['distance'];
				$log[$key] = $row['last_sent'];
			}
			array_multisort($log, SORT_ASC, $dist, SORT_ASC, $mec_array_loc);
			$sorted_array = array_values($mec_array_loc);
			
			$i = 1;
			foreach ($sorted_array as $val) {
				if ($i > 5) continue;
				if ($val['new_flag'] == 1) {
					$html .= "<option value=" . $val['mec_id'] . ">" . $val['shop_name'] . " - " . $val['address4'] . " (L" . $val['leads'] . ") [ G: " . $val['ga'] . ", C: " . $val['wkly_leads'] . ", T: " . $val['elapsed_time'] . ", D: " . $val['distance'] . "]</option>";
				} else {
					$html .= "<option value=" . $val['mec_id'] . ">" . $val['shop_name'] . " - " . $val['address4'] . " (C" . $val['credits'] . ") [ G: " . $val['ga'] . ", C: " . $val['wkly_leads'] . ", T: " . $val['elapsed_time'] . ", D: " . $val['distance'] . "] </option>";
				}
				$i++;
			}
		}
		echo $html;
		break;
	case "4w":
		foreach ($pic_range as $pick_key => $pick_val) {
			$query = "SELECT DISTINCT ct.b2b_shop_id, m.premium, b.b2b_new_flg, m.mec_id, m.shop_name, m.address4, m.axle_id, m.wkly_counter, m.flag, m.brand, m.type, ct.b2b_leads, ct.b2b_credits, wkly_leads, b2b_re_leads, b2b_non_re_leads, m.lat, m.lng, b2b_log, (6371*ACOS(COS(RADIANS('$lat'))*COS(RADIANS(m.lat))*COS(RADIANS(m.lng)-RADIANS('$lng'))+SIN(RADIANS('$lat'))*SIN(RADIANS(m.lat)))) AS dist
FROM go_bumpr.admin_mechanic_table AS m
INNER JOIN go_bumpr.admin_service_type AS s ON m.mec_id=s.mec_id
JOIN b2b.b2b_mec_tbl b ON b.b2b_shop_id = m.axle_id
JOIN b2b.b2b_credits_tbl ct ON m.axle_id = ct.b2b_shop_id
WHERE b.b2b_avail = 1 AND m.type='4w' AND m.axle_id>1000 AND (6371*ACOS(COS(RADIANS('$lat'))*COS(RADIANS(m.lat))*COS(RADIANS(m.lng)-RADIANS('$lng'))+SIN(RADIANS('$lat'))*SIN(RADIANS(m.lat)))) <= 8 AND m.status!='1' AND s.$service_col='1' AND ((b.b2b_new_flg =0 AND ct.b2b_credits > 0) OR (b.b2b_new_flg = 1 AND ct.b2b_leads > 0)) AND m.wkly_counter > 0 ORDER BY b.b2b_log asc";
			
			$result = mysqli_query($conn1, $query);
			?>
			<option value="">Select Mechanic</option>
			
			<?php
			$rows = [];
			while ($row = mysqli_fetch_object($result)) {
				$rows[] = $row;
			}
			
			foreach ($rows as $row) {
				$mec_id = $row->mec_id;
				$shop_name = $row->shop_name;
				$address4 = $row->address4;
				$axle_id = $row->axle_id;
				$distance = $row->dist;
				$brand_serviced = $row->brand;
				$brand_array = explode(',', $brand_serviced);
				//parse_str($brand_serviced, $brand_array);
				//print_r($brand_array);
				$new_flag = $row->b2b_new_flg;
				$shop_lat = $row->lat;
				$shop_lng = $row->lng;
				$last_lead = $row->b2b_log;
				$premium = $row->premium;
				$counter = $row->wkly_counter;
				$flag = $row->flag;
				
				if ($brand_serviced != "all" && !in_array($brand, $brand_array)) {
					continue;
				}
				
				$sql_crd = "SELECT * FROM (SELECT wkly_leads,b2b_credits,b2b_leads,b2b_re_leads,b2b_non_re_leads,ct.b2b_shop_id FROM go_bumpr.admin_mechanic_table adm join b2b_credits_tbl ct on adm.axle_id=ct.b2b_shop_id WHERE ct.b2b_shop_id='$axle_id')A
			LEFT JOIN
			(SELECT b2b_log,b2b_shop_id from b2b_booking_tbl where b2b_shop_id='$axle_id' and b2b_swap_flag!='1' order by b2b_log DESC limit 1) B on A.b2b_shop_id=B.b2b_shop_id
			LEFT JOIN
			(SELECT count(b2b_booking_id) as wkly_counter,b2b_shop_id from b2b_booking_tbl where b2b_shop_id='$axle_id' and cast(b2b_log as date)=CURDATE() and b2b_swap_flag!='1')C ON C.b2b_shop_id=B.b2b_shop_id group by A.b2b_shop_id";
				//echo $sql_crd;
				$res_crd = mysqli_query($conn2, $sql_crd);
				$row_count = mysqli_num_rows($res_crd);
				$row_crd = mysqli_fetch_object($res_crd);
				
				if ($row_crd->wkly_counter == "") {
					$ga = '0';
				} else {
					$ga = $row_crd->wkly_counter;
				}
				$wkly_leads = $row_crd->wkly_leads;
				
				$time = strtotime($row_crd->b2b_log);
				$elapsed_time = humanTiming($time);
				
				if ($row_count <= 0) {
					$credits = '-';
					$leads = '-';
				} else {
					if ($new_flag == 1) {
						$leads = $row_crd->b2b_leads;
						$credits = 0;
					} else {
						$credits = $row_crd->b2b_credits;
						$leads = 0;
					}
				}
				
				$mec_array_loc[] = array("mec_id" => $mec_id, "shop_name" => $shop_name, "address4" => $address4, "leads" => $leads, "credits" => $credits, "new_flag" => $new_flag, "lat_lng" => $shop_lat . "," . $shop_lng, "ga" => $ga, "wkly_leads" => $wkly_leads, "elapsed_time" => $elapsed_time, "last_sent" => $row_crd->b2b_log);
			}
			
			$lat_lng = array_column($mec_array_loc, 'lat_lng');
			$lat_lng_str = implode($lat_lng, '|');
			$distanceKM = getDrivingDistance($lat, $lng, $lat_lng_str);
			
			$new_array = [];
			foreach ($distanceKM as $key => $value) {
				$mec_array_loc[$key]['distance'] = $value;
				unset($mec_array_loc[$key]['lat_lng']);
			}
			
			foreach ($mec_array_loc as $key => $row) {
				// $crd[$key] = print_r($row['credits']);
				$lead[$key] = $row['leads'];
				$crd[$key] = $row['credits'];
				$dist[$key] = $row['distance'];
				$log[$key] = $row['last_sent'];
			}
			
			array_multisort($log, SORT_ASC, $dist, SORT_ASC, $mec_array_loc);
			$sorted_array = array_values($mec_array_loc);
			
			$i = 1;
			foreach ($sorted_array as $val) {
				if ($i > 5) continue;
				if ($val['new_flag'] == 1) {
					$html .= "<option value=" . $val['mec_id'] . ">" . $val['shop_name'] . "-" . $val['address4'] . " (L" . $val['leads'] . ")[ G: " . $val['ga'] . ", C: " . $val['wkly_leads'] . ", T: " . $val['elapsed_time'] . ", D: " . $val['distance'] . "]</option>";
				} else {
					$html .= "<option value=" . $val['mec_id'] . ">" . $val['shop_name'] . "-" . $val['address4'] . "(C" . $val['credits'] . ")[ G: " . $val['ga'] . ", C: " . $val['wkly_leads'] . ", T: " . $val['elapsed_time'] . ", D: " . $val['distance'] . "] </option>";
				}
				$i++;
			}
		}
		echo $html;
		break;
}

?>

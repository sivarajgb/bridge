<?php
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$flag = $_SESSION['flag'];


$src_crm = array('crm016', 'crm018', 'crm064', 'crm036', 'crm033', 'crm017');
$src_column = 0;
if (in_array($crm_log_id, $src_crm)) {
	$src_column = 1;
}

$startdate = date('Y-m-d', strtotime($_GET['startdate']));
$enddate = date('Y-m-d', strtotime($_GET['enddate']));
$level = $_GET['level'];
$service = $_GET['service'];
$vehicle = $_GET['vehicle'];
$city = $_GET['city'];
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

$col_name = $vehicle == '2w' ? 'l.bike_cluster' : 'l.car_cluster';

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;

$cond = '';

$cond = $cond . ($vehicle == 'all' ? "" : "AND b.vehicle_type='$vehicle'");
$cond = $cond . ($service == 'all' ? "" : "AND b.service_type='$service'");
$cond = $cond . ($level == 'all' ? "" : "AND u.user_level='$level'");
$cond = $cond . ($city == 'all' ? "" : "AND b.city='$city'");
// $cond = $cond.($cluster == 'all' ? "" : ($vehicle != 'all' ? "AND ".$col_name."='$cluster'" : "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'"));

// $sql_booking = "SELECT DISTINCT b.user_id,b.booking_id,b.mec_id,b.service_type,b.shop_name,b.vehicle_type,b.user_veh_id,b.locality,b.feedback,b.rating,u.name as user_name,u.mobile_number as user_mobile,u.Locality_Home as user_locality,u.user_level,v.brand,v.model,b.log,b.axle_flag FROM user_booking_tb as b LEFT JOIN user_register as u ON b.user_id=u.reg_id LEFT JOIN user_vehicle_table as v ON b.user_veh_id=v.id LEFT JOIN localities as l ON b.locality=l.localities WHERE (b.flag!='1' AND b.flag_unwntd!='1')AND b.axle_flag='1' AND u.user_level NOT IN(0,1) AND DATE(b.log) BETWEEN '$startdate' and '$enddate' {$cond} ORDER BY b.log DESC";
$sql_booking = "SELECT DISTINCT u.reg_id,u.name as user_name,u.mobile_number as user_mobile,u.Locality_Home as user_locality,u.user_level,b.booking_id,b.mec_id,b.service_type,b.shop_name,b.vehicle_type,b.user_veh_id,b.locality,b.feedback,b.rating,b.axle_flag,b.log FROM user_register u LEFT JOIN (SELECT MAX(booking_id) as booking_id,booking_tbl.user_id,booking_tbl.log,city,mec_id,service_type,shop_name,vehicle_type,user_veh_id,locality,feedback,rating,axle_flag,brand,model FROM user_booking_tb booking_tbl LEFT JOIN user_vehicle_table v ON v.id = booking_tbl.user_veh_id GROUP BY booking_tbl.user_id) b ON b.user_id = u.reg_id WHERE DATE(b.log) BETWEEN '$startdate' and '$enddate' {$cond} ORDER BY RAND(),b.log DESC LIMIT 20;";
//echo $sql_booking;
$res_booking = mysqli_query($conn, $sql_booking);

$no = 0;

$count = mysqli_num_rows($res_booking);
if ($count > 0) {
	while ($row_booking = mysqli_fetch_object($res_booking)) {
		$booking_id = $row_booking->booking_id;
		$user_id = $row_booking->user_id;
		$veh_type = $row_booking->vehicle_type;
		$veh_id = $row_booking->user_veh_id;
		$service_type = $row_booking->service_type;
		$user_id = $row_booking->user_id;
		$shop_name = $row_booking->shop_name;
		$locality = $row_booking->locality;
		$user_name = $row_booking->user_name;
		$user_mobile = $row_booking->user_mobile;
		$address = $row_booking->user_lat_lng;
		$home = $row_booking->user_locality;
		$user_level = $row_booking->user_level;
		$veh_brand = $row_booking->brand;
		$veh_model = $row_booking->model;
		$rating = $row_booking->rating;
		$feedback = $row_booking->feedback;
		$log = $row_booking->log;
		$goaxle_status = $row_booking->axle_flag;
		
		$tr = '<tr>';
		
		$td1 = '<td>' . $user_name;
		switch ($user_level) {
			case '5':
				$td1 = $td1 . '<p style="font-size:16px;color:green;padding:5px;" title="Level 5 User"><i class="fa fa-angellist" aria-hidden="true"></i></p>';
				break;
			case '4':
				$td1 = $td1 . '<p style="font-size:16px;color:red;padding:5px;" title="Level 4 User"><i class="fa fa-bug" aria-hidden="true"></i></p>';
				break;
			case '3':
				$td1 = $td1 . '<p style="font-size:16px;color:#26269e;padding:5px;" title="Level 3 User"><i class="fa fa-handshake-o" aria-hidden="true"></i></p>';
				break;
			case '2':
				$td1 = $td1 . '<p style="font-size:16px;color:brown;padding:5px;" title="Level 2 User"><i class="fa fa-anchor" aria-hidden="true"></i></p>';
				break;
			case '1':
				$td1 = $td1 . '<p style="font-size:16px;color:orange;padding:5px;" title="Level 1 User"><i class="fa fa-tripadvisor" aria-hidden="true"></i></p>';
				break;
			default:
		}
		$td1 = $td1 . '</td>';
		
		$td2 = '<td><a href="user_details.php?t=' . base64_encode("irp") . '&v58i4=' . base64_encode($veh_id) . '&bi=' . base64_encode($booking_id) . '"><i class="fa fa-eye" aria-hidden="true"></i></td>';
		
		if (!$_SESSION['eupraxia_flag']) {
			$td3 = '<td>' . $user_mobile . '</td>';
		}
		$td4 = '<td>';
		if ($bsource == "Hub Booking") {
			if ($locality != '') {
				$td4 = $td4 . $locality;
			} else if ($home == '') {
				$td4 = $td4 . $address;
			} else {
				$td4 = $td4 . $home;
			}
		} else if ($bsource == "GoBumpr App" || $bsource == "App") {
			$td4 = $td4 . $locality;
		} else {
			$td4 = $td4 . $locality;
		}
		$td4 = $td4 . '</td>';
		$td5 = '<td>';
		switch ($service_type) {
			case 'general_service' :
				if ($veh_type == '2w') {
					$td5 = $td5 . "General Service";
				} else {
					$td5 = $td5 . "Car service and repair";
				}
				break;
			case 'break_down':
				$td5 = $td5 . "Breakdown Assistance";
				break;
			case 'tyre_puncher':
				$td5 = $td5 . "Tyre Puncture";
				break;
			case 'other_service':
				$td5 = $td5 . "Repair Job";
				break;
			case 'car_wash':
				if ($veh_type == '2w') {
					$td5 = $td5 . "water Wash";
				} else {
					$td5 = $td5 . "Car wash exterior";
				}
				break;
			case 'engine_oil':
				$td5 = $td5 . "Repair Job";
				break;
			case 'free_service':
				$td5 = $td5 . "General Service";
				break;
			case 'car_wash_both':
			case 'completecarspa':
				$td5 = $td5 . "Complete Car Spa";
				break;
			case 'car_wash_ext':
				$td5 = $td5 . "Car wash exterior";
				break;
			case 'car_wash_int':
				$td5 = $td5 . "Interior Detailing";
				break;
			case 'Doorstep car spa':
				$td5 = $td5 . "Doorstep Car Spa";
				break;
			case 'Doorstep_car_wash':
				$td5 = $td5 . "Doorstep Car Wash";
				break;
			case 'free_diagnastic':
			case 'free_diagnostic':
			case 'diagnostics':
				if ($veh_type == '2w') {
					$td5 = $td5 . "Diagnostics/Check-up";
				} else {
					$td5 = $td5 . "Vehicle Diagnostics";
				}
				break;
			case 'water_wash':
				$td5 = $td5 . "water Wash";
				break;
			case 'exteriorfoamwash':
				$td5 = $td5 . "Car wash exterior";
				break;
			case 'interiordetailing':
				$td5 = $td5 . "Interior Detailing";
				break;
			case 'rubbingpolish':
				$td5 = $td5 . "Car Polish";
				break;
			case 'underchassisrustcoating':
				$td5 = $td5 . "Underchassis Rust Coating";
				break;
			case 'headlamprestoration':
				$td5 = $td5 . "Headlamp Restoration";
				break;
			default:
				$td5 = $td5 . $service_type;
		}
		$td5 = $td5 . '</td>';
		$td6 = '<td>' . $veh_brand . " " . $veh_model . '</td>';
		$td7 = '<td>' . $shop_name . '</td>';
		
		$td8 = '<td>' . date('d M Y', strtotime($log)) . '</td>';
		if ($goaxle_status == 1) {
			$goaxle_status = 'Sent';
		} else {
			$goaxle_status = 'Not Sent';
		}
		$td8 = $td8 . '<td>' . $goaxle_status . '</td>';
		$td9 = '<td>' . $rating . '</td>';
		$td10 = '<td>' . $feedback . '</td>';
		$tr_l = '</tr>';
		
		if ($_SESSION['eupraxia_flag']) {
			$str = $tr . $td1 . $td2 . $td4 . $td5 . $td6 . $td7 . $td8 . $td9 . $td10 . $tr_l;
		} else {
			$str = $tr . $td1 . $td2 . $td3 . $td4 . $td5 . $td6 . $td7 . $td8 . $td9 . $td10 . $tr_l;
		}
		$data[] = array('tr' => $str);
	}
	echo $data1 = json_encode($data);
} // if
else {
	echo "no";
}
?>

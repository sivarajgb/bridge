<?php
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$city = $_GET['city'];
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;

$cond ='';

$cond = $cond.($city == 'all' ? "" : "AND b.city='$city'");
$cond = $cond.($cluster == 'all' ? "" : "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'");

$sql_booking = "SELECT DISTINCT b.booking_id,b.user_id,b.mec_id,b.log,b.crm_update_time,b.service_type,b.followup_date,b.axle_flag,b.crm_update_id,b.locality,b.booking_status,b.priority,b.flag_fo,b.flag,b.source,c.name as crm_name,u.name as user_name,u.mobile_number as user_mobile,u.lat_lng as user_latlng,u.Locality_Home as user_locality,u.user_level FROM user_booking_tb as b LEFT JOIN crm_admin as c ON b.crm_update_id=c.crm_log_id LEFT JOIN user_register as u ON b.user_id=u.reg_id LEFT JOIN localities as l ON b.locality=l.localities WHERE b.source='External Bookings' AND DATE(b.log) BETWEEN '$startdate' and '$enddate' {$cond} ORDER BY b.crm_update_time DESC";  
$res_booking = mysqli_query($conn,$sql_booking) or die(mysqli_error($conn));

$no = 0;

$count = mysqli_num_rows($res_booking);
if($count >=1){

while($row_booking = mysqli_fetch_object($res_booking)){
$booking_id = $row_booking->booking_id;
$user_id = $row_booking->user_id;
$mec_id = $row_booking->mec_id;
$log = $row_booking->log;
$update_time = $row_booking->crm_update_time;
$service_type = $row_booking->service_type;
$bsource = $row_booking->source;
$followup_date = $row_booking->followup_date;
$axle_flag = $row_booking->axle_flag;
$alloted_to_id = $row_booking->crm_update_id;
$locality = $row_booking->locality;
$status = $row_booking->booking_status;
$priority = $row_booking->priority;
$flag_fo = $row_booking->flag_fo;
$bflag = $row_booking->flag;
$alloted_to = $row_booking->crm_name;
$user_name = $row_booking->user_name;
$user_mobile = $row_booking->user_mobile;
$address = $row_booking->user_lat_lng;
$home = $row_booking->user_locality;
$user_level = $row_booking->user_level;


// go axle status
$sql_axle = mysqli_query($conn2,"SELECT max(b.b2b_booking_id) as b2b_booking_id,s.b2b_acpt_flag,s.b2b_deny_flag  FROM b2b_booking_tbl as b LEFT JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.gb_booking_id='$booking_id'");
$row_axle= mysqli_fetch_object($sql_axle);
$bid=$row_axle->b2b_booking_id;
$accept = $row_axle->b2b_acpt_flag;
$deny = $row_axle->b2b_deny_flag;

$tr = '<tr>';
$no=$no+1;
$td1 = '<td>'.$no.'</td>';
    
$td2 = '<td><p style="float:left;padding:10px;">'.$booking_id.'</p>';
if($flag_fo == '1'){
  $td2 = $td2.'<p class="p" style="font-size:16px;color:#D81B60;float:left;padding:5px;" title="Resheduled Booking!"><i class="fa fa-recycle" aria-hidden="true"></i></p>';
  switch($priority){
    case '1':$td2 = $td2.'<p style="font-size:16px;color:#00ACC1;float:left;padding:5px;" title="Low Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';break;
    case '2':$td2 = $td2.'<p style="font-size:16px;color:#FFB300;float:left;padding:5px;" title="Medium Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';break;
    case '3':$td2 = $td2.'<p style="font-size:16px;color:#E53935;float:left;padding:5px;" title="High Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';break;
    default:$td2 = $td2.'<p style="font-size:16px;color:#757575;float:left;padding:5px;" title="Priority Not available!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';
  }
}
$td2 = $td2.'</td>';
$td3 = '<td>';
if($bflag == '1'){
  $td3 = $td3."Cancelled";
}
else{
  switch($status){
  case '1':$td3 = $td3."Lead"; break;
  case '2':$td3 = $td3."Booking"; break;
  case '3':$td3 = $td3."Followup"; break;
  case '4':$td3 = $td3."RNR1"; break;
  case '5':$td3 = $td3."RNR2"; break;
  default:$td3 = $td3."Other";
  }
} 
$td3 = $td3.'</td>';
    
$td4 = '<td><div class="row">';
if($axle_flag!='1'){
  $td4 = $td4.'<p style="background-color:#FFA800;padding:7px;width:30px;border-radius:18px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
}
else if($accept == 1 && $deny == 0 ){ 
  $td4 = $td4.'<p style="background-color:#6FA3DE;padding:7px;width:30px;border-radius:18px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
}
else if($accept==0 && $deny==1){ 
  $td4 = $td4.'<p style="background-color:#D25F34;padding:7px;width:30px;border-radius:18px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
}
else if($accept==0 && $deny==0){ 
  $td4 = $td4.'<p style="background-color:#69ED85;padding:7px;width:30px;border-radius:18px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
}
$td4 = $td4.'</div></td>';

$td5 = '<td>'; 
switch($service_type){
      case 'general_service' :
      if($veh_type =='2w'){
        $td5 = $td5."General Service";
      }
      else{
        $td5 = $td5."Car service and repair";
      }
      break;
      case 'break_down':  $td5 = $td5."Breakdown Assistance"; break;
      case 'tyre_puncher': $td5 = $td5."Tyre Puncture";  break;
      case 'other_service': $td5 = $td5."Repair Job"; break;
      case 'car_wash':
      if($veh_type == '2w'){
        $td5 = $td5."water Wash";
      }
      else{
        $td5 = $td5."Car wash exterior";
      }break;
      case 'engine_oil': $td5 = $td5."Repair Job"; break;
      case 'free_service': $td5 = $td5."General Service"; break;
      case 'car_wash_both':
      case 'completecarspa': $td5 = $td5."Complete Car Spa"; break;
      case 'car_wash_ext': $td5 = $td5."Car wash exterior"; break;
      case 'car_wash_int': $td5 = $td5."Interior Detailing"; break;
      case 'Doorstep car spa': $td5 = $td5."Doorstep Car Spa"; break;
      case 'Doorstep_car_wash': $td5 = $td5."Doorstep Car Wash"; break;
      case 'free_diagnastic':
      case 'free_diagnostic':
      case 'diagnostics':
      if($veh_type == '2w'){
        $td5 = $td5."Diagnostics/Check-up";
     }
     else{
       $td5 = $td5."Vehicle Diagnostics";
     }
         break;
      case 'water_wash': $td5 = $td5."water Wash"; break;
      case 'exteriorfoamwash': $td5 = $td5."Car wash exterior"; break;
      case 'interiordetailing': $td5 = $td5."Interior Detailing"; break;
      case 'rubbingpolish': $td5 = $td5."Car Polish"; break;
      case 'underchassisrustcoating': $td5 = $td5."Underchassis Rust Coating"; break;
      case 'headlamprestoration': $td5 = $td5."Headlamp Restoration"; break;
      default: $td5 = $td5.$service_type;
} 
$td5 = $td5.'</td>';

$td6 = '<td><a href="user_details.php?t='.base64_encode("eb").'&v58i4='.base64_encode($veh_id).'&bi='.base64_encode($booking_id).'"><i class="fa fa-eye" aria-hidden="true"></i></td>';
$td7 = '<td>'.$user_name;
switch($user_level){
  case '5': $td7 = $td7.'<p style="font-size:16px;color:green;padding:5px;" title="Level 5 User"><i class="fa fa-angellist" aria-hidden="true"></i></p>';
  break;
  case '4': $td7 = $td7.'<p style="font-size:16px;color:red;padding:5px;" title="Level 4 User"><i class="fa fa-bug" aria-hidden="true"></i></p>';
  break;
  case '3': $td7 = $td7.'<p style="font-size:16px;color:#26269e;padding:5px;" title="Level 3 User"><i class="fa fa-handshake-o" aria-hidden="true"></i></p>';
  break;
  case '2': $td7 = $td7.'<p style="font-size:16px;color:brown;padding:5px;" title="Level 2 User"><i class="fa fa-anchor" aria-hidden="true"></i></p>';
  break;
  case '1': $td7 = $td7.'<p style="font-size:16px;color:orange;padding:5px;" title="Level 1 User"><i class="fa fa-tripadvisor" aria-hidden="true"></i></p>';
  break;
  default:
}
$td7 = $td7.'</td>';
$td8 = '<td>'.$user_mobile.'</td>';
$td9 = '<td>';
if($bsource == "Hub Booking"){ 
  if($locality!=''){ 
    $td9 = $td9.$locality; 
  } 
  else if($home == ''){ 
    $td9 = $td9.$address; 
  } 
  else { 
    $td9 = $td9.$home; 
  } 
} 
else if($bsource == "GoBumpr App" || $bsource == "App"){ 
  $td9 = $td9.$address; 
} 
else{  
  $td9 = $td9.$locality; 
}
$td9 = $td9.'</td>';
$td10 = '<td>';
if($followup_date == "0000-00-00" || $followup_date == "1970-01-01" || $followup_date == ''){
  $td10 = $td10."Not Updated Yet"; 
} 
else { 
 $td10 = $td10.date('d M Y', strtotime($followup_date)); 
} 
$td10 = $td10.'</td>';
$td11 = '<td>'.$alloted_to.'</td>';
$td12 = '<td>'.date('d M Y h.i A', strtotime($log)).'</td>';
$td13 = '<td>'.date('d M Y h.i A', strtotime($update_time)).'</td>';
$tr_l ='</tr>';
  
$str = $tr.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$td9.$td10.$td11.$td12.$td13.$tr_l;
$data[] = array('tr'=>$str);
}
echo $data1 = json_encode($data);
} // if
else {
  //echo $sql_booking;
  echo "no";
}
 ?>

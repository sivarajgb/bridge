<?php
//error_reporting(E_ALL);ini_set('display_errors',1);
include("../config.php");
$conn = db_connect1();
session_start();

date_default_timezone_set('Asia/Calcutta');

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$src_crm = array('crm016','crm018','crm064','crm036','crm033','crm017','crm034');
$src_column = 0;
if(in_array($crm_log_id,$src_crm))
{
	$src_column = 1;
}

function elapsed_time($distant_timestamp, $max_units = 3) {
  $i = 0;
  $distant_timestamp = strtotime($distant_timestamp);
  $time = time() - $distant_timestamp; // to get the time since that moment
  $tokens = [
      31536000 => 'year',
      2592000 => 'month',
      604800 => 'week',
      86400 => 'day',
      3600 => 'hr',
      60 => 'min',
      1 => 'sec'
  ];

  $responses = [];
  while ($i < $max_units && $time > 0) {
      foreach ($tokens as $unit => $text) {
          if ($time < $unit) {
              continue;
          }
          $i++;
          $numberOfUnits = floor($time / $unit);

          $responses[] = $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
          $time -= ($unit * $numberOfUnits);
          break;
      }
  }

  if (!empty($responses)) {
      return implode(' ', $responses) ;
  }

  return 'Just now';
}


 $startdate = date('Y-m-d',strtotime($_GET['startdate']));
 $enddate =  date('Y-m-d',strtotime($_GET['enddate']));
 $person = $_GET['person'];
 $source = $_GET['source'];
 $vehicle = $_GET['vehicle'];
 $reason = $_GET['reason'];
 $city = $_GET['city'];
 $cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

 $col_name = $vehicle == '2w' ? 'l.bike_cluster' : 'l.car_cluster';
 
 $_SESSION['crm_city'] = $city;
 $_SESSION['crm_cluster'] = $cluster;

 $cond ='';

 $cond = $cond.($vehicle == 'all' ? "" : "AND b.vehicle_type='$vehicle'");
 $cond = $cond.($reason == 'all' ? "" : "AND o.reason='$reason'");
 $cond = $cond.($source == 'all' ? "" : "AND b.source='$source'");
 $cond = $cond.($person == 'all' ? "" : "AND b.crm_update_id='$person'");
 $cond = $cond.($city == 'all' ? "" : "AND b.city='$city'");
 $cond = $cond.($cluster == 'all' ? "" : ($vehicle != 'all' ? "AND ".$col_name."='$cluster'" : "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'"));
 $order_log = elapsed_time($o_log);
 //  bb.gb_booking_id!='0' AND bb.b2b_swap_flag!='1' AND b.cap_flag=0 AND b.booking_status='2' AND b.flag!='1'
//  SELECT DISTINCT b.booking_id ,b.user_id,b.mec_id,b.crm_update_time,b.source,b.vehicle_type,b.user_veh_id,b.service_type,b.initial_service_type,b.service_date,b.shop_name,b.axle_flag,b.crm_update_id,b.locality,b.utm_source,b.priority,b.crown_reject,b.flag_fo,b.user_pmt_flg,c.name as crm_name,u.name as user_name,u.mobile_number as user_mobile,u.lat_lng as user_latlng,u.Locality_Home as user_locality,u.user_level,v.brand,v.model,s.b2b_acpt_flag,s.b2b_deny_flag,s.b2b_ntavail_flag,s.b2b_brnd_flag,s.b2b_deny_log,s.b2b_acpt_log FROM user_booking_tb as b LEFT JOIN crm_admin as c ON b.crm_update_id=c.crm_log_id LEFT JOIN user_register as u ON b.user_id=u.reg_id LEFT JOIN user_vehicle_table as v ON b.user_veh_id=v.id LEFT JOIN localities as l ON b.locality = l.localities LEFT JOIN b2b.b2b_booking_tbl as bb ON b.booking_id=bb.gb_booking_id  LEFT JOIN b2b.b2b_status as s ON bb.b2b_booking_id=s.b2b_booking_id WHERE  bb.gb_booking_id!='0' AND bb.b2b_swap_flag!='1' AND b.cap_flag=0 AND b.booking_status='2' AND b.flag!='1' AND DATE(b.crm_update_time) BETWEEN '$startdate' and '$enddate' {$cond} ORDER BY b.crm_update_time DESC
$sql_overrides = "SELECT DISTINCT o.booking_id,o.reason,o.comments,o.status,o.log,b.axle_flag,b.shop_name,b.service_type,b.vehicle_type,b.locality,u.Locality_Home as user_locality,u.user_level,v.brand,v.model,m.pickup_available,m.door_step,s.b2b_acpt_flag,s.b2b_deny_flag,bb.b2b_swap_flag,b.booking_status,o.update_log FROM user_booking_tb as b LEFT JOIN override_tbl as o ON o.booking_id=b.booking_id LEFT JOIN user_register as u ON b.user_id=u.reg_id LEFT JOIN user_vehicle_table as v ON b.user_veh_id=v.id LEFT JOIN admin_mechanic_table as m ON m.mec_id=b.mec_id  LEFT JOIN b2b.b2b_booking_tbl as bb ON b.booking_id=bb.gb_booking_id  LEFT JOIN b2b.b2b_status as s ON bb.b2b_booking_id=s.b2b_booking_id WHERE o.status ='0' and o.booking_id is not null AND DATE(o.log)  BETWEEN '$startdate' and '$enddate' {$cond} ORDER BY o.update_log DESC,o.status asc";

$res_overrides = mysqli_query($conn,$sql_overrides);

$no = 0;

$count = mysqli_num_rows($res_overrides);
if($count >0){
while($row_overrides = mysqli_fetch_object($res_overrides)){
$booking_id = $row_overrides->booking_id;
$reason = $row_overrides->reason;
$comments = $row_overrides->comments;
$status = $row_overrides->status;
$o_log = $row_overrides->log;
$axle_flag = $row_overrides->axle_flag;
$shop_name = $row_overrides->shop_name;
$service_type = $row_overrides->service_type;
$veh_type = $row_overrides->vehicle_type;
$pickup_location = $row_overrides->locality;
$customer_location = $row_overrides->user_locality;
$veh_brand = $row_overrides->brand;
$veh_model = $row_overrides->model;
$pickup = $row_overrides->pickup_available;
$doorstep = $row_overrides->door_step;
$acpt = $row_overrides->b2b_acpt_flag;
$deny = $row_overrides->b2b_deny_flag;
$swap_flag = $row_overrides->b2b_swap_flag;
$booking_status = $row_overrides->booking_status;
$update_log=$row_overrides->update_log;

$tr_time=time()-strtotime($update_log);
if($tr_time > "1800"){
  $tr = '<tr style="background-color:#f74563;">';
}else{
$tr = '<tr>';
}

$td1 = '<td><p style="float:left;padding:10px;">'.$booking_id.'</p></td>';

$td2 = '<td><p style="float:left;padding:10px;">'.$reason.'</p></td>';

$td3 = '<td><p style="float:left;padding:10px;">'.$comments.'</p></td>';

$td4 = '<td><a href="user_details.php?t='.base64_encode("or").'&bi='.base64_encode($booking_id).'"><i class="fa fa-eye" aria-hidden="true"></i></td>';

$td5 = '<td>';
switch($service_type){
  case 'general_service' :
  if($veh_type =='2w'){
    $td5 = $td5."General Service";
  }
  else{
    $td5 = $td5."Car service and repair";
  }
  break;
  case 'break_down':  $td5 = $td5."Breakdown Assistance"; break;
  case 'tyre_puncher': $td5 = $td5."Tyre Puncture";  break;
  case 'other_service': $td5 = $td5."Repair Job"; break;
  case 'car_wash':
  if($veh_type == '2w'){
    $td5 = $td5."water Wash";
  }
  else{
    $td5 = $td5."Car wash exterior";
  }break;
  case 'engine_oil': $td5 = $td5."Repair Job"; break;
  case 'free_service': $td5 = $td5."General Service"; break;
  case 'car_wash_both':
  case 'completecarspa': $td5 = $td5."Complete Car Spa"; break;
  case 'car_wash_ext': $td5 = $td5."Car wash exterior"; break;
  case 'car_wash_int': $td2 = $td2."Interior Detailing"; break;
  case 'Doorstep car spa': $td2 = $td2."Doorstep Car Spa"; break;
  case 'Doorstep_car_wash': $td5 = $td5."Doorstep Car Wash"; break;
  case 'free_diagnastic':
  case 'free_diagnostic':
  case 'diagnostics':
  if($veh_type == '2w'){
    $td5 = $td5."Diagnostics/Check-up";
  }
  else{
    $td5 = $td5."Vehicle Diagnostics";
  }
      break;
  case 'water_wash': $td5 = $td5."water Wash"; break;
  case 'exteriorfoamwash': $td5 = $td5."Car wash exterior"; break;
  case 'interiordetailing': $td5 = $td5."Interior Detailing"; break;
  case 'rubbingpolish': $td5 = $td5."Car Polish"; break;
  case 'underchassisrustcoating': $td5 = $td5."Underchassis Rust Coating"; break;
  case 'headlamprestoration': $td5 = $td5."Headlamp Restoration"; break;
  default: $td5 = $td5.$service_type;
} 
$td5 = $td5.'</td>';
  
// $td6 = '<td>';
// if($bsource == "Hub Booking"){ 
//   if($locality!=''){ 
//     $td6 = $td6.$locality; 
//   } 
//   else if($home == ''){ 
//     $td6 = $td6.$address; 
//   } 
//   else { 
//     $td6 = $td6.$home; 
//   } 
// } 
// else if($bsource == "GoBumpr App" || $bsource == "App"){ 
//   $td6 = $td6.$locality; 
// } 
// else{  
//   $td6 = $td6.$locality; 
// }
// $td6 = $td6.'</td>';
$td6 ='<td>';
if($pickup == 1){
  $td6 = $td6.'Yes';
}
else $td6 = $td6.'No';
$td6 = $td6.'</td>';

$td7 ='<td>';
if($doorstep == 1){
  $td7 = $td7.'Yes';
}
else $td7 = $td7.'No';
$td7 = $td7.'</td>';

$td8 = '<td>'.$customer_location.'</td>';

$td9 = '<td>'.$pickup_location.'</td>';

$td10 = '<td>'.$veh_brand." ".$veh_model.'</td>';

$td11 = '<td>'.elapsed_time($update_log).'</td>';

$td12 = '<td><div class="row">';

if($axle_flag!='1' && $booking_status!='2'){
  if($status== "0"){
    $td12= $td12.'<a class="se_goaxle" href="go_axle.php?bi='.$booking_id.'&t=b&r=o" style="background-color:#ffa800;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';  }
  else {
    $td12= $td12.'<a class="se_goaxle" href="go_axle.php?bi='.$booking_id.'&t=b&r=o" style="background-color:red;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';  }
  // if($mec_id == '' || $mec_id == 0 || $mec_id == 1 || $service_type == '' || $service_date == '' || $veh_id == '0' ){
  //   $td2 = $td2.'<a href="user_details.php?t='.base64_encode("b").'&v58i4='.base64_encode($veh_id).'&bi='.base64_encode($booking_id).'" style="background-color:#ffa800;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
  // }
  // else{
  //   $td2 = $td2.'<a href="go_axle.php?bi='.$booking_id.'&t=b" style="background-color:#ffa800;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
  // }
}
else {
   if($accept == 1 && $deny == 0 ){
  $td12 = $td12.'<p style="background-color:#6FA3DE;padding:7px;width:30px;border-radius:18px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';

}
else if($accept==0 && $deny==1){ 
  $td12 = $td12.'<p style="background-color:#D25F34;padding:7px;width:30px;border-radius:18px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
 
}
else if($accept==0 && $deny==0){
  $td12 = $td12.'<p style="background-color:#69ED85;padding:7px;width:30px;border-radius:18px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
}
}

  $td12= $td12.'</div></td>';

  $td13 = '<td>'.$shop_name.'</td>';

  $td14 = '<td>'.date('Y-m-d h:m A',strtotime($o_log)).'</td>';
$tr_l ='</tr>';
  
$str = $tr.$td1.$td12.$td2.$td3.$td4.$td13.$td5.$td6.$td7.$td8.$td9.$td10.$td14.$td11.$tr_l;
$data[] = array('tr'=>$str);
}
echo $data1 = json_encode($data);
} // if
else {
  echo "no";
}
 ?>

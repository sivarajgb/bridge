<?php 
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();

$crm_log_id = $_POST['crm_log_id'];
$today=date('Y-m-d H:i:s');
$booking_id=$_POST['book_id'];
// $booking_id = $_GET['booking_id'];
// $booking_id = 102859;

$initial_list = $premium_list = $precredit_list = $last_goaxle_hrs_list = $avg_action_list = $avg_contact_list = $final_list = "";
$range = $new_garage = 0;
$final_shop = "";


$select_api_key = "SELECT api_key FROM crm_admin WHERE crm_log_id = '$crm_log_id'";
$res_api_key = mysqli_query($conn,$select_api_key);
$row_api_key = mysqli_fetch_object($res_api_key);
$api_key = $row_api_key->api_key;
if($api_key == '' or $api_key == null)
{
	$api_key = 'AIzaSyBf_jj-_oOA-ukhgaGnkic4iz316ih_cdo';
}
// echo $api_key;

$select_booking = "SELECT l.lat as booking_lat,l.lng as booking_lng,b.*,v.brand,v.model FROM user_booking_tb b LEFT JOIN localities l ON l.localities = b.locality LEFT JOIN user_vehicle_table v ON v.id=b.user_veh_id WHERE booking_id = '$booking_id'";
// echo $select_booking;
// echo "<br>";
$res_booking = mysqli_query($conn,$select_booking);
$row_booking = mysqli_fetch_object($res_booking);
$booking_lat = $row_booking->booking_lat;
$booking_lng = $row_booking->booking_lng;
$city = $row_booking->city;
$brand = $row_booking->brand;
$vehicle_type = $row_booking->vehicle_type;
$service_type = $row_booking->service_type;
$locality = $row_booking->locality;
$pickup_address = $row_booking->pickup_address;
if($pickup_address != '' && $pickup_address != '-')
{
	$locality = $pickup_address;
}

if(isset($_POST['service']))
{
	$service_type = $_POST['service'];
}
if(isset($_POST['selecttype']))
{
	$vehicle_type = $_POST['selecttype'];
}
if(isset($_POST['selectloc']))
{
	$locality = $_POST['selectloc'];
}
	$select_locality = "SELECT l.lat as booking_lat,l.lng as booking_lng FROM localities l WHERE localities = '$locality'";
	$res_locality = mysqli_query($conn,$select_locality);
	$row_locality = mysqli_fetch_object($res_locality);
	$booking_lat = $row_locality->booking_lat;
	$booking_lng = $row_locality->booking_lng;
// }

$radius = '0';

$select_service = "SELECT service_type_column,amt FROM go_axle_service_price_tbl WHERE service_type  = '$service_type';";
// echo $select_mec;
// echo "<br>";
$res_service = mysqli_query($conn,$select_service);
$row_service = mysqli_fetch_object($res_service);
$service_type_column = $row_service->service_type_column;
$service_type_credits = $row_service->amt/100;

$re_specialist = $_POST['re_specialist'];
$doorstep_only = $_POST['doorstep_only'];

$cond = "";
$limit = 9;
if($vehicle_type == '2w' && $re_specialist == 1)
{
	$cond = " AND brand2 = 'Royal Enfield'";
}
if($vehicle_type == '4w' && $doorstep_only == 1)
{
	$cond = " AND door_step = '1'";
	$limit = 15;
}

$select_mec = "SELECT s.mec_id as mec,m.*,(6371*ACOS(COS(RADIANS('$booking_lat'))*COS(RADIANS(m.lat))*COS(RADIANS(m.lng)-RADIANS('$booking_lng'))+SIN(RADIANS('$booking_lat'))*SIN(RADIANS(m.lat)))) as dist FROM go_bumpr.admin_mechanic_table m LEFT JOIN go_bumpr.admin_service_type s ON m.mec_id = s.mec_id LEFT JOIN b2b.b2b_mec_tbl b2b ON b2b.b2b_shop_id = m.axle_id WHERE b2b.b2b_avail = 1 AND s.$service_type_column = 1 AND m.status=0 AND m.type='$vehicle_type' AND m.wkly_counter > 0 AND m.address5='$city' AND m.axle_id NOT IN (1014,1035,1670) {$cond} AND (6371*ACOS(COS(RADIANS('$booking_lat'))*COS(RADIANS(m.lat))*COS(RADIANS(m.lng)-RADIANS('$booking_lng'))+SIN(RADIANS('$booking_lat'))*SIN(RADIANS(m.lat)))) < {$limit} ORDER BY dist LIMIT 20";
// $select_mec = "SELECT s.mec_id as mec,m.*,(6371*ACOS(COS(RADIANS('$booking_lat'))*COS(RADIANS(m.lat))*COS(RADIANS(m.lng)-RADIANS('$booking_lng'))+SIN(RADIANS('$booking_lat'))*SIN(RADIANS(m.lat)))) as dist FROM go_bumpr.admin_mechanic_table m LEFT JOIN go_bumpr.admin_service_type s ON m.mec_id = s.mec_id WHERE s.$service_type_column = 1 AND m.status=0 AND m.type='$vehicle_type' AND m.wkly_counter > 0 AND m.address5='$city' AND m.axle_id NOT IN (1014,1035,1670) ORDER BY dist";
// echo $select_mec;
// echo "<br>";

$res_mec = mysqli_query($conn,$select_mec);
$i = $k = $z = 0;
$count = mysqli_num_rows($res_mec);
$select_b2b = "SELECT query.b2b_credits,query.b2b_shop_id,query.b2b_booking_id,AVG(CASE WHEN query.rating != 0 THEN query.rating END) as avg_rating,AVG(TIMESTAMPDIFF(MINUTE,query.b2b_log,query.b2b_mod_log)) AS avg_action,AVG(TIMESTAMPDIFF(MINUTE,query.b2b_mod_log,query.b2b_contacted_log)) AS avg_contact,TIMESTAMPDIFF(HOUR,query.b2b_log,DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 330 MINUTE)) diff_in_hrs,query.b2b_acpt_flag,query.b2b_deny_flag FROM (";
while($row_mec = mysqli_fetch_object($res_mec))
{
	$mec_id = $row_mec->mec_id;
	$axle_id = $row_mec->axle_id;
	$shop_name = $row_mec->shop_name;
	$mec_lat = $row_mec->lat;
	$mec_lng = $row_mec->lng;
	$pick_range = $row_mec->pick_range;
	$distance = $row_mec->dist;
	if($vehicle_type == '4w')
	{
		$brand_serviced = $row_mec->brand;
	}
	else
	{
		$brand_serviced = $row_mec->brand2;
	}
	$address4 = $row_mec->address4;
	$premium = $row_mec->premium;
	$brandsArr = '';
	
	// $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$booking_lat.",".$booking_lng."&destinations=".$mec_lat.",".$mec_lng."&mode=driving&key=$api_key";
	// $ch = curl_init();
    // curl_setopt($ch, CURLOPT_URL, $url);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    // $response = curl_exec($ch);
    // curl_close($ch);
    // $response_a = json_decode($response, true);
	// $status = $response_a['status'];
	// if($status != "OK")
	// {
		// $api_key = 'AIzaSyBf_jj-_oOA-ukhgaGnkic4iz316ih_cdo';
		// continue;
	// }
    // $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
	
	
	if($brand_serviced != 'all' && $brand_serviced != '')
	{
		$brandsArr = explode(',',$brand_serviced);
		if(!in_array($brand,$brandsArr))
		{
			continue;
		}
	}
	$brand_not_serviced = $row_mec->brand_ns;
	$brandsNoArr = '';
	if($brand_not_serviced != 'all' && $brand_not_serviced != '')
	{
		$brandsNoArr = explode(',',$brand_not_serviced);
		if(in_array($brand,$brandsNoArr))
		{
			continue;
		}
	}
	$z = $z + 1;
	// $dist = (float)str_replace("km","",$dist);
	$subquery = 0;
	if($distance <= 5)
	{
		$select_b2b_subquery = "(SELECT b.b2b_shop_id,b.b2b_booking_id,b.b2b_Rating as rating,b.b2b_log,s.b2b_acpt_flag,s.b2b_deny_flag,s.b2b_mod_log,b.b2b_contacted_log,c.b2b_credits FROM b2b_booking_tbl b LEFT JOIN b2b_credits_tbl c ON c.b2b_shop_id = b.b2b_shop_id LEFT JOIN b2b_status s ON s.b2b_booking_id = b.b2b_booking_id WHERE b.b2b_shop_id = '$axle_id' AND b.gb_booking_id != 0 ORDER BY b.b2b_booking_id DESC LIMIT 10)";
		$subquery = 1;
		$garages[$axle_id]['mec_id'] = $mec_id;
		$garages[$axle_id]['axle_id'] = $axle_id;
		$garages[$axle_id]['address4'] = $address4;
		$garages[$axle_id]['shop_name'] = $shop_name;
		$garages[$axle_id]['premium'] = $premium;
		$garages[$axle_id]['distance'] = round($distance,2).' Kms';
		// $garages[$axle_id]['distance_navigation'] = round($dist,2).' Kms';
		$garages[$axle_id]['pick_range'] = $pick_range.' Kms';
		$i = $i + 1;
	}
	if($subquery == 1 && $z != 1 && $i != 1)
	{
		$select_b2b = $select_b2b."UNION ALL".$select_b2b_subquery;
	}
	else if($subquery == 1 && $z != 1 && $i == 1)
	{
		$select_b2b = $select_b2b.$select_b2b_subquery;
	}
	else if($subquery == 1 && $z == 1 && $i == 1)
	{
		$select_b2b = $select_b2b.$select_b2b_subquery;
	}
}
if(sizeof($garages) > 0)
{
	$counter = 0;
	foreach($garages as $garage)
	{
		if($counter > 0)
		{
			$initial_list = $initial_list.",";
		}
		$initial_list = $initial_list.$garage['mec_id'];
		$counter = $counter + 1;
	}
	$range = 0;
}
// echo "less than 5";
// echo "<br>";
// echo json_encode($garages);
// echo "<br>";
if(sizeof($garages) < 2)
{
	$initial_list = "";
	$i = $k = $z = 0;
	$select_b2b_subquery = '';
	$select_b2b = "SELECT query.b2b_credits,query.b2b_shop_id,query.b2b_booking_id,AVG(CASE WHEN query.rating != 0 THEN query.rating END) as avg_rating,AVG(TIMESTAMPDIFF(MINUTE,query.b2b_log,query.b2b_mod_log)) AS avg_action,AVG(TIMESTAMPDIFF(MINUTE,query.b2b_mod_log,query.b2b_contacted_log)) AS avg_contact,TIMESTAMPDIFF(HOUR,query.b2b_log,DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 330 MINUTE)) diff_in_hrs,query.b2b_acpt_flag,query.b2b_deny_flag FROM (";
	$res_mec = mysqli_query($conn,$select_mec);
	while($row_mec = mysqli_fetch_object($res_mec))
	{
		$mec_id = $row_mec->mec_id;
		$axle_id = $row_mec->axle_id;
		$shop_name = $row_mec->shop_name;
		$mec_lat = $row_mec->lat;
		$mec_lng = $row_mec->lng;
		$pick_range = $row_mec->pick_range;
		$distance = $row_mec->dist;
		if($vehicle_type == '4w')
		{
			$brand_serviced = $row_mec->brand;
		}
		else
		{
			$brand_serviced = $row_mec->brand2;
		}
		$address4 = $row_mec->address4;
		$premium = $row_mec->premium;
		$brandsArr = '';
		
		// $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$booking_lat.",".$booking_lng."&destinations=".$mec_lat.",".$mec_lng."&mode=driving&key=$api_key";
		// $ch = curl_init();
		// curl_setopt($ch, CURLOPT_URL, $url);
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		// $response = curl_exec($ch);
		// curl_close($ch);
		// $response_a = json_decode($response, true);
		// $status = $response_a['status'];
		// if($status != "OK")
		// {
			// $api_key = 'AIzaSyBf_jj-_oOA-ukhgaGnkic4iz316ih_cdo';
			// continue;
		// }
		// $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
		
		
		if($brand_serviced != 'all' && $brand_serviced != '')
		{
			$brandsArr = explode(',',$brand_serviced);
			if(!in_array($brand,$brandsArr))
			{
				continue;
			}
		}
		$brand_not_serviced = $row_mec->brand_ns;
		$brandsNoArr = '';
		if($brand_not_serviced != 'all' && $brand_not_serviced != '')
		{
			$brandsNoArr = explode(',',$brand_not_serviced);
			if(in_array($brand,$brandsNoArr))
			{
				continue;
			}
		}
		$z = $z + 1;
		// $dist = (float)str_replace("km","",$dist);
		$subquery = 0;
		if($distance <= $pick_range)
		{
			$select_b2b_subquery = "(SELECT b.b2b_shop_id,b.b2b_booking_id,b.b2b_Rating as rating,b.b2b_log,s.b2b_acpt_flag,s.b2b_deny_flag,s.b2b_mod_log,b.b2b_contacted_log,c.b2b_credits FROM b2b_booking_tbl b LEFT JOIN b2b_credits_tbl c ON c.b2b_shop_id = b.b2b_shop_id LEFT JOIN b2b_status s ON s.b2b_booking_id = b.b2b_booking_id WHERE b.b2b_shop_id = '$axle_id' AND b.gb_booking_id != 0 ORDER BY b.b2b_booking_id DESC LIMIT 10)";
			$subquery = 1;
			$garages[$axle_id]['mec_id'] = $mec_id;
			$garages[$axle_id]['axle_id'] = $axle_id;
			$garages[$axle_id]['address4'] = $address4;
			$garages[$axle_id]['shop_name'] = $shop_name;
			$garages[$axle_id]['premium'] = $premium;
			$garages[$axle_id]['distance'] = round($distance,2).' Kms';
			// $garages[$axle_id]['distance_navigation'] = round($dist,2).' Kms';
			$garages[$axle_id]['pick_range'] = $pick_range.' Kms';
			$i = $i + 1;
		}
		if($subquery == 1 && $z != 1 && $i != 1)
		{
			$select_b2b = $select_b2b."UNION ALL".$select_b2b_subquery;
		}
		else if($subquery == 1 && $z != 1 && $i == 1)
		{
			$select_b2b = $select_b2b.$select_b2b_subquery;
		}
		else if($subquery == 1 && $z == 1 && $i == 1)
		{
			$select_b2b = $select_b2b.$select_b2b_subquery;
		}
	}
	$counter = 0;
	foreach($garages as $garage)
	{
		if($counter > 0)
		{
			$initial_list = $initial_list.",";
		}
		$initial_list = $initial_list.$garage['mec_id'];
		$counter = $counter + 1;
	}
	$range = 1;
	// echo "pickup";
	// echo "<br>";
	// echo json_encode($garages);
	// echo "<br>";
}

$select_b2b = $select_b2b.") query GROUP BY query.b2b_shop_id;";
// echo $select_b2b;
$res_b2b = mysqli_query($conn2,$select_b2b);
$axle_ids = array_keys($garages);
$counter0 = 0;
$axle_id_list = "(";
foreach($axle_ids as $axle_id)
{
	if($counter0 > 0)
	{
		$axle_id_list = $axle_id_list.",";
	}
	$axle_id_list = $axle_id_list."'".$axle_id."'";
	$counter0 = $counter0+1;
}
$axle_id_list = $axle_id_list.")";
$remaing_credits_query = "SELECT b.b2b_shop_id,SUM(CASE WHEN s.b2b_acpt_flag = 0 and s.b2b_deny_flag = 0 AND b.gb_booking_id != 0 and b.b2b_swap_flag!='1' THEN b.b2b_credit_amt ELSE 0 END) as remaining_credits FROM b2b_booking_tbl b LEFT JOIN b2b_status s ON s.b2b_booking_id = b.b2b_booking_id WHERE b.b2b_shop_id IN $axle_id_list GROUP BY b.b2b_shop_id;";
$remaing_credits_res = mysqli_query($conn2,$remaing_credits_query);
while($remaing_credits_row = mysqli_fetch_object($remaing_credits_res))
{
	echo "s";
	$garages[$remaing_credits_row->b2b_shop_id]['remaining_credits'] = $remaing_credits_row->remaining_credits;
	// if($garages[$remaing_credits_row->b2b_shop_id]['remaining_credits'] > 0)
	// {
		// $garages[$remaing_credits_row->b2b_shop_id]['remaining_credits'] = $garages[$remaing_credits_row->b2b_shop_id]['remaining_credits']/100;
	// }
}
$i = 0;
// echo "b4while";
while($row_b2b = mysqli_fetch_object($res_b2b))
{
	// echo "afterwhile";
	// echo "--";
	$b2b_booking_id = $row_b2b->b2b_booking_id;
	$b2b_shop_id = $row_b2b->b2b_shop_id;
	$avg_rating = $row_b2b->avg_rating;
	if($avg_rating == '')
	{
		$avg_rating = 0;
	}
	$diff_in_hrs = $row_b2b->diff_in_hrs;
	$avg_action = $row_b2b->avg_action;
	$avg_contact = $row_b2b->avg_contact;
	$accept = $row_b2b->b2b_acpt_flag;
	$deny = $row_b2b->b2b_deny_flag;
	$b2b_credits = $row_b2b->b2b_credits;
	// if($accept == 0 && $deny == 0)
	// {
		// continue;
	// }
	// else if($deny == 1)
	// {
		// continue;
	// }
	$garages[$b2b_shop_id]['accept'] = $accept;
	$garages[$b2b_shop_id]['deny'] = $deny;
	$garages[$b2b_shop_id]['b2b_credits'] = $b2b_credits;
	$garages[$b2b_shop_id]['avg_rating'] = $avg_rating;
	$garages[$b2b_shop_id]['diff_in_hrs'] = $diff_in_hrs;
	$garages[$b2b_shop_id]['avg_action'] = $avg_action;
	$garages[$b2b_shop_id]['avg_contact'] = $avg_contact;
	if($accept == 0 && $deny == 0)
	{
		$garages[$b2b_shop_id]['last_goaxle'] = 'Idle';
	}
	else if($accept == 1 && $deny == 0)
	{
		$garages[$b2b_shop_id]['last_goaxle'] = 'Accepted';
	}
	else if($accept == 0 && $deny == 1)
	{
		$garages[$b2b_shop_id]['last_goaxle'] = 'Rejected';
	}
	$i = $i + 1;
	// echo $b2b_shop_id." - ".$garages[$b2b_shop_id]['remaining_credits']." - ".$garages[$b2b_shop_id]['last_goaxle'];
}
// echo "==========";
// echo "<br>";
// echo "==========";
// echo "<br>";
// echo "before credits and last goaxle status check";
// echo "<br>";
// echo json_encode($garages);
// echo "<br>";
$i = $k = 0;
$counter1 = $counter2 = 0;
foreach($garages as $index => $garage)
{
	if(($garage['b2b_credits']-$garage['remaining_credits'])>=$service_type_credits)
	{
		// echo $garage['axle_id'];
		if(array_key_exists("premium",$garage) && $garage['premium']==1 && $garage['last_goaxle'] != 'Idle')
		{
			if($garage['last_goaxle'] == 'Rejected' && $garage['diff_in_hrs']>24)
			{
				foreach($garage as $key=>$val)
				{
					$premiumPartners[$garage['axle_id']][$key] = $val;
				}
			}
			foreach($garage as $key=>$val)
			{
				$premiumPartners[$garage['axle_id']][$key] = $val;
			}
		}
		else if($garage['last_goaxle'] != 'Idle')
		{
			if($garage['last_goaxle'] == 'Rejected' && $garage['diff_in_hrs']>24)
			{
				foreach($garage as $key=>$val)
				{
					$preCreditPartners[$garage['axle_id']][$key] = $val;
				}
			}
			foreach($garage as $key=>$val)
			{
				$preCreditPartners[$garage['axle_id']][$key] = $val;
			}
		}
	}
	$i = $i + 1;
}

$newGarage = array_filter($garages, function ($var) {
    return (!array_key_exists("b2b_credits",$var));
});
// echo "Premium:";
// echo json_encode($premiumPartners);
// echo "<br>";
// echo "=====";
// echo "<br>";
// echo "PreCredit:";
// echo json_encode($preCreditPartners);
// echo "<br>";
// echo "<br>";
// echo "new garage";
// print_r($newGarage);
// echo "=====";
if(!empty($newGarage))
{
	foreach($newGarage as $newGrg)
	{
		$axle_id = $newGrg['axle_id'];
		$sql_crd = "SELECT b2b_credits FROM b2b_credits_tbl WHERE b2b_shop_id='$axle_id'";
		$res_crd = mysqli_query($conn2,$sql_crd);
		$row_crd = mysqli_fetch_object($res_crd);
		if($row_crd->b2b_credits <= 0)
		{
			unset($newGarage[$axle_id]);
		}
	}
}
if(!empty($newGarage))
{
	$counter = 0;
	foreach($newGarage as $newGrg)
	{
		if($counter > 0)
		{
			$final_list = $final_list.",";
		}
		$final_list = $final_list.$newGrg['mec_id'];
		$counter = $counter + 1;
		$new_garage = 1;
	}
	$distance_navigation_sort = array();
	foreach ($newGarage as $key => $row)
	{
		$distance_navigation_sort[$key] = $row['distance'];
	}
	array_multisort($distance_navigation_sort, SORT_ASC, $newGarage);
	$newGarage = array_slice($newGarage,0,round(sizeof($newGarage)/2,0));
}
if(!empty($newGarage))
{
	foreach($newGarage as $arrValue)
	{
		$shop = $arrValue;
		$final_shop = $shop['mec_id'];
	}
}

// if(empty($newGarage))
// {
	/*foreach($garages as $index => $garage)
	{
		if($garage['last_goaxle']>72)
		{
			foreach($garage as $key=>$val)
			{
				$idlePartners[$garage['axle_id']][$key] = $val;
			}
		}
	}*/
	// $idlePartners = array_filter($garages, function ($var) {
		// return ($var['diff_in_hrs']>72);
	// });
// }

// print_r($idlePartners);

if(empty($newGarage))
{
	$final_shop = $final_list = "";
	$premiumHighest = array_filter($premiumPartners, function ($var) {
    return ($var['avg_rating'] >= 4);
	});
	$premiumAverage = array_filter($premiumPartners, function ($var) {
		return ($var['avg_rating'] < 4 && $var['avg_rating'] >= 3.5 );
	});
	$premiumLow = array_filter($premiumPartners, function ($var) {
		return ($var['avg_rating'] < 3.5);
	});

	$preCreditHighest = array_filter($preCreditPartners, function ($var) {
		return ($var['avg_rating'] >= 4);
	});
	$preCreditAverage = array_filter($preCreditPartners, function ($var) {
		return ($var['avg_rating'] < 4 && $var['avg_rating'] >= 3.5 );
	});
	$preCreditLow = array_filter($preCreditPartners, function ($var) {
		return ($var['avg_rating'] < 3.5);
	});

// $premiumPartners = array();
// $premiumHighest = array();
// $premiumAverage = array();
// $premiumLow = array();
// $preCreditPartners = array();
// $preCreditHighest = array();
// $preCreditAverage = array();
// $preCreditLow = array();

if(!empty($premiumPartners))
{
	if(!empty($premiumHighest))
	{
		$selectFrom = $premiumHighest;
	}
	else if(!empty($premiumAverage))
	{
		$selectFrom = $premiumAverage;
	}
	else
	{
		$selectFrom = $premiumLow;
	}
	// echo "<br>";
	// echo "<br>";
	// echo "Initial Ranking";
	// echo json_encode($selectFrom);
	$counter = 0;
	foreach($selectFrom as $garage)
	{
		if($counter>0)
		{
			$last_goaxle_hrs_list = $last_goaxle_hrs_list.",";
		}
		$last_goaxle_hrs_list = $last_goaxle_hrs_list.$garage['mec_id'];
		$counter = $counter +1;
	}
	if(sizeof($selectFrom)>1)
	{
		$last_goaxle_sort = array();
		foreach ($selectFrom as $key => $row)
		{
			$last_goaxle_sort[$key] = $row['diff_in_hrs'];
		}
		array_multisort($last_goaxle_sort, SORT_DESC, $selectFrom);
		$selectFrom = array_slice($selectFrom,0,round(sizeof($selectFrom)/2,0));
		// echo "<br>";
		// echo "<br>";
		// echo "Ranking after last goaxle";
		// echo json_encode($selectFrom);
		$counter = 0;
		foreach($selectFrom as $garage)
		{
			if($counter>0)
			{
				$avg_action_list = $avg_action_list.",";
			}
			$avg_action_list = $avg_action_list.$garage['mec_id'];
			$counter = $counter +1;
		}
		if(sizeof($selectFrom)>1)
		{
			$avg_action_sort = array();
			foreach ($selectFrom as $key => $row)
			{
				$avg_action_sort[$key] = $row['avg_action'];
			}
			array_multisort($avg_action_sort, SORT_ASC, $selectFrom);
			$selectFrom = array_slice($selectFrom,0,round(sizeof($selectFrom)/2,0));
			// echo "<br>";
			// echo "<br>";
			// echo "Ranking after avg action";
			// echo json_encode($selectFrom);
		}
		foreach($selectFrom as $arrValue)
		{
			$shop = $arrValue;
			$final_shop = $shop['mec_id'];
			break;
		}
	}
	else
	{
		foreach($selectFrom as $arrValue)
		{
			$shop = $arrValue;
			$final_shop = $shop['mec_id'];
			break;
		}
	}
	$counter = 0;
	foreach($selectFrom as $garage)
	{
		if($counter > 0)
		{
			$final_list = $final_list.",";
		}
		$final_list = $final_list.$garage['mec_id'];
		$counter = $counter + 1;
	}
}
else
{
	if(!empty($preCreditHighest))
	{
		$selectFrom = $preCreditHighest;
	}
	else if(!empty($preCreditAverage))
	{
		$selectFrom = $preCreditAverage;
	}
	else
	{
		$selectFrom = $preCreditLow;
	}
	// echo "<br>";
	// echo "<br>";
	// echo "Initial Ranking PreCredit";
	// echo json_encode($selectFrom);
	$counter = 0;
	foreach($selectFrom as $garage)
	{
		if($counter>0)
		{
			$last_goaxle_hrs_list = $last_goaxle_hrs_list.",";
		}
		$last_goaxle_hrs_list = $last_goaxle_hrs_list.$garage['mec_id'];
		$counter = $counter +1;
	}
	if(sizeof($selectFrom)>1)
	{
		$last_goaxle_sort = array();
		foreach ($selectFrom as $key => $row)
		{
			$last_goaxle_sort[$key] = $row['diff_in_hrs'];
		}
		array_multisort($last_goaxle_sort, SORT_DESC, $selectFrom);
		$selectFrom = array_slice($selectFrom,0,round(sizeof($selectFrom)/2,0));
		// echo "<br>";
		// echo "<br>";
		// echo "Ranking after last goaxle PreCredit";
		// echo json_encode($selectFrom);
		$counter = 0;
		foreach($selectFrom as $garage)
		{
			if($counter>0)
			{
				$avg_action_list = $avg_action_list.",";
			}
			$avg_action_list = $avg_action_list.$garage['mec_id'];
			$counter = $counter +1;
		}
		if(sizeof($selectFrom)>1)
		{
			$avg_action_sort = array();
			foreach ($selectFrom as $key => $row)
			{
				$avg_action_sort[$key] = $row['avg_action'];
			}
			array_multisort($avg_action_sort, SORT_ASC, $selectFrom);
			$selectFrom = array_slice($selectFrom,0,round(sizeof($selectFrom)/2,0));
			// echo "<br>";
			// echo "<br>";
			// echo "Ranking after avg action precredit";
			// echo json_encode($selectFrom);
			$counter = 0;
			foreach($selectFrom as $garage)
			{
				if($counter>0)
				{
					$avg_contact_list = $avg_contact_list.",";
				}
				$avg_contact_list = $avg_contact_list.$garage['mec_id'];
				$counter = $counter +1;
			}
			if(sizeof($selectFrom)>1)
			{
				$avg_contact_sort = array();
				foreach ($selectFrom as $key => $row)
				{
					$avg_contact_sort[$key] = $row['avg_contact'];
				}
				array_multisort($avg_contact_sort, SORT_ASC, $selectFrom);
				// echo "<br>";
				// echo "<br>";
				// echo "Ranking after avg contact PreCredit";
				// echo json_encode($selectFrom);
			}
		}
		foreach($selectFrom as $arrValue)
		{
			$shop = $arrValue;
			$final_shop = $shop['mec_id'];
			break;
		}
	}
	else
	{
		foreach($selectFrom as $arrValue)
		{
			$shop = $arrValue;
			$final_shop = $shop['mec_id'];
			break;
		}
	}
	$counter = 0;
	foreach($selectFrom as $garage)
	{
		if($counter > 0)
		{
			$final_list = $final_list.",";
		}
		$final_list = $final_list.$garage['mec_id'];
		$counter = $counter + 1;
	}
}
// echo "<br>";
// echo "<br>";
// echo "Final Ranking";
// echo json_encode($selectFrom);
}
// echo "premium: <br>";
// echo json_encode($premiumPartners);
// echo "<br>";
// echo "<br>";
// echo "precredit <br>";
// echo json_encode($preCreditPartners);
// print_r($preCreditPartners);


// echo "<br>";
// echo "<br>";
// echo "Final Shop";
// echo json_encode($shop);
$axle_id = $shop['axle_id'];
$shop_name = $shop['shop_name'];
$mec_id = $shop['mec_id'];
$distance = $shop['distance'];
// $dist = $shop['distance_navigation'];
$address4 = $shop['address4'];
$sql_crd = "SELECT b2b_credits FROM b2b_credits_tbl WHERE b2b_shop_id='$axle_id'";
$res_crd = mysqli_query($conn2,$sql_crd);
$row_crd = mysqli_fetch_object($res_crd);
if(mysqli_num_rows($res_crd)<=0){
	$credits='-';
}else{
	$credits = $row_crd->b2b_credits;
}
?>
<option value="<?php echo $mec_id;?>"><?php echo $shop_name.'-'.$address4.' ('.$credits.')'.' - '.$distance; ?></option>
<!--<div>
	<div class="w3-container" style="margin-top:20px;">
		<div class="w3-card-4" >
			<header class="w3-container" style="background:#ccc;">
				<h3><?php echo /*$val['axle_id'].*/$shop_name.' ('.$credits.')'; ?></h3>
			</header>

		<div class="w3-container" style="height:70px;">
			<input type="hidden" class="form-control" id="select-mechanic" value="<?php echo $mec_id;?>">
			<p style="float:left;margin: 20px 0 10px 0;"><?php echo $address4.", ".$dist." away."; ?></p>
			<p class="goaxle" data-mec-id="<?php echo $mec_id;?>" style="float: right;width: 40px;height: 40px;background: #ffa800;-moz-border-radius: 50px;-webkit-border-radius: 50px;border-radius: 50px;margin-top: 15px;"><i class="fa fa-rocket" aria-hidden="true" style="font-size: 26px;margin-top: 8px;margin-left: 6px;"></i></p>
		</div>
		</div>
	</div>
</div>-->


<?php 
$pc_ins = "INSERT INTO project_crown_mechanism 
	   (booking_id,location,initial_list,range_pickup,premium,precredit,last_goaxle_hrs,avg_action,avg_contact,final_list,final_shop,new_garage,log)
VALUES ('$booking_id','$locality','$initial_list','$range','$premium_list','$precredit_list','$last_goaxle_hrs_list','$avg_action_list','$avg_contact_list','$final_list','$final_shop','$new_garage','$today')";
$res = mysqli_query($conn,$pc_ins);
?>
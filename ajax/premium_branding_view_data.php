<?php
//error_reporting(E_ALL); ini_set('display_errors', 1);
include("../config.php");
//print_r($_GET);die;
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$flag=$_SESSION['flag'];
 
$today = date('Y-m-d');

$start_date = $_POST['start'];

$shop_id = $_POST['shop'];
$type = $_POST['type'];
$total = $_POST['total'];

if($type == 'completed'){
    $sql_counts = "SELECT b.b2b_service_type, 
    COUNT(CASE WHEN s.b2b_acpt_flag = 1 THEN 1 END) 
    AS count 
    FROM 
    go_bumpr.user_booking_tb as g 
    INNER JOIN 
    b2b.b2b_booking_tbl as b 
    ON 
    g.booking_id=b.gb_booking_id
    LEFT JOIN 
    b2b.b2b_status as s 
    ON
    b.b2b_booking_id=s.b2b_booking_id 
    LEFT JOIN 
    go_bumpr.go_axle_service_price_tbl as ms 
    ON
    (g.service_type=ms.service_type AND g.vehicle_type=ms.type)
    WHERE b.b2b_shop_id='$shop_id' AND 
    s.b2b_acpt_flag='1' AND 
    b.b2b_log BETWEEN '$start_date' AND '$today' 
    GROUP BY b.b2b_service_type  
    ORDER BY count DESC"; 
    
}

else{
    $sql_counts = "SELECT b.b2b_service_type,
    count(DISTINCT b.b2b_booking_id) 
    as count 
    FROM 
    b2b.b2b_booking_tbl as b
    LEFT JOIN 
    b2b.b2b_mec_tbl  as mt 
    ON 
    b.b2b_shop_id=mt.b2b_shop_id
    LEFT JOIN 
    b2b.b2b_status as s 
    ON
    b.b2b_booking_id=s.b2b_booking_id 
    LEFT JOIN 
    go_bumpr.go_axle_service_price_tbl as ms 
    ON 
    (b.b2b_service_type=ms.service_type AND b.b2b_vehicle_type=ms.type) 
    JOIN 
    go_bumpr.user_booking_tb as g
    ON
    b.gb_booking_id=g.booking_id
    WHERE
     mt.b2b_shop_id='$shop_id' AND 
     s.b2b_acpt_flag='1' AND
    b.b2b_log BETWEEN '$start_date' AND '$today' 
    GROUP BY b.b2b_service_type 
    ORDER BY count DESC";
}
//echo  $sql_counts;
$res_counts = mysqli_query($conn,$sql_counts);

if(mysqli_num_rows($res_counts)>0){

    $data.='<div style="width:90%;margin-left:8px;">
    <table class="table table-bordered" style="border:1px solid #00BCD4;margin:15px;" >
    <thead style="background-color:#B2DFDB;align:center;display: table-header-group;">
    <th>ServiceType</th>
    <th>Count</th>
    <th>Percentage(%)</th>
    </thead>
    <tbody>';
    while($row_counts = mysqli_fetch_array($res_counts)){
        $service = $row_counts['b2b_service_type'];
        $count = $row_counts['count'];
        if($count == 0 ){
            continue;
        }
        $count_per = round(($count/$total)*100,1);
        $data.=  '<tr>
        <td style="text-align:center;">'.$service.'</td>
        <td style="text-align:center;">'.$count.'</td>
        <td style="text-align:center;">'.$count_per.' %</td>
        </tr>';
        
    }
    
    $data.='</tbody>
    </table>
    </div>';
    
echo $res = $data;
}
else 
{
  echo "no results found!!!";
}
?>

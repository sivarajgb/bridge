<?php
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$source = $_GET['source'];
$paid = $_GET['paid'];
$city = $_GET['city']; 
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;

$cond ='';
 
$cond = $cond.($source == 'all' ? "" : "AND b.source='$source'");
$cond = $cond.($city == 'all' ? "" : "AND b.city='$city'");
$cond = $cond.($cluster == 'all' ? "" : "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'");
$cond = $cond.($paid == 'all' ? "" : ($paid == 'paid' ? "AND b.user_pmt_flg='1'" : "AND b.user_pmt_flg='0'"));


// $sql_booking = "SELECT DISTINCT b.booking_id,b.user_id,b.mec_id,b.source,b.vehicle_type,b.user_veh_id,b.service_type,b.service_date,b.shop_name,b.axle_flag,b.locality,b.utm_source,b.priority,b.flag_fo,b.crm_update_id,b.crm_update_time,b.user_pmt_flg,b.rnr_flag,c.name as crm_name,u.name as user_name,u.mobile_number as user_mobile,u.lat_lng as user_lat_lng,u.Locality_Home as user_locality,u.user_level,v.brand,v.model,b.booking_status FROM user_booking_tb as b LEFT JOIN crm_admin as c ON b.crm_update_id=c.crm_log_id LEFT JOIN user_register as u ON b.user_id=u.reg_id LEFT JOIN user_vehicle_table as v ON b.user_veh_id=v.id LEFT JOIN localities as l ON b.locality=l.localities WHERE b.booking_status='1' AND (b.crm_update_id = '$crm_log_id' OR b.crm_update_id = '') AND b.flag!='1' AND b.flag_unwntd!='1' AND DATE(b.crm_update_time) BETWEEN '$startdate' and '$enddate' {$cond} ORDER BY b.crm_update_time DESC";
$sql_booking = "SELECT DISTINCT b.booking_id,b.user_id,b.mec_id,b.source,b.vehicle_type,b.user_veh_id,b.service_type,b.service_date,b.shop_name,b.axle_flag,b.locality,b.utm_source,b.priority,b.flag_fo,b.crm_update_id,b.crm_update_time,b.user_pmt_flg,b.bike_1499_lp,b.booking_otp_confirm,b.rnr_flag,c.name as crm_name, u.name as user_name,u.mobile_number as user_mobile,u.lat_lng as user_lat_lng,u.Locality_Home as user_locality,u.user_level,v.brand,v.model,b.booking_status,o.status FROM user_booking_tb as b LEFT JOIN override_tbl as o on o.booking_id=b.booking_id LEFT JOIN crm_admin as c ON b.crm_update_id=c.crm_log_id LEFT JOIN user_register as u ON b.user_id=u.reg_id LEFT JOIN user_vehicle_table as v ON b.user_veh_id=v.id LEFT JOIN localities as l ON b.locality=l.localities WHERE b.booking_status='1' AND b.override_flag!='1' AND (ISNULL(o.status) or o.status !='2') AND (b.crm_update_id = '$crm_log_id' OR b.crm_update_id = '') AND b.flag!='1' AND b.flag_unwntd!='1' and b.ire_flag!='1'  AND  DATE(b.crm_update_time) BETWEEN '$startdate' and '$enddate' {$cond}
ORDER BY crm_update_time DESC";
// echo $sql_booking;exit;

mysqli_set_charset($conn, 'utf8');
$res_booking = mysqli_query($conn,$sql_booking);

$no = 0;

$count = mysqli_num_rows($res_booking);
if($count >0){
  while($row_booking = mysqli_fetch_object($res_booking)){
    $booking_id = $row_booking->booking_id;
    $booking_status = $row_booking->booking_status;
    $user_id = $row_booking->user_id;
    $mec_id = $row_booking->mec_id;
    $log = $row_booking->crm_update_time;
    $bsource = $row_booking->source;
    $veh_type = $row_booking->vehicle_type;
    $veh_id = $row_booking->user_veh_id;
    $service_type = $row_booking->service_type;
    $service_date = $row_booking->service_date;
    $shop_name = $row_booking->shop_name;
    $axle_flag = $row_booking->axle_flag;
    $locality = $row_booking->locality;
    $utm_source = $row_booking->utm_source;
    $priority = $row_booking->priority;
    $flag_fo = $row_booking->flag_fo;
    $alloted_to_id = $row_booking->crm_update_id;
    $payment_flag = $row_booking->user_pmt_flg;
        $bike_lp=$row_booking->bike_1499_lp;
    $otp_verify=$row_booking->booking_otp_confirm;
    $rnr_flag = $row_booking->rnr_flag;
    $alloted_to = $row_booking->crm_name;
    $user_name = $row_booking->user_name;
    $user_mobile = $row_booking->user_mobile;
    $address = $row_booking->user_lat_lng;
    $home = $row_booking->user_locality;
    $user_level = $row_booking->user_level;
    $veh_brand = $row_booking->brand;
    $veh_model = $row_booking->model;
    $o_status = $row_booking->status;
	/* $acpt = 0;
	$deny = 0;
	if($booking_status == 2)
	{
		$sql = "SELECT s.b2b_acpt_flag, s.b2b_deny_flag FROM b2b.b2b_booking_tbl AS bb LEFT JOIN b2b.b2b_status AS s ON bb.b2b_booking_id = s.b2b_booking_id WHERE bb.gb_booking_id = '$booking_id' AND bb.b2b_flag != '1'";
		$res = mysqli_query($conn,$sql);
		$row = mysqli_fetch_object($res);
		if($row->b2b_acpt_flag == 1)
		{
			continue;
		}
		else
		{
			$acpt = $row->b2b_acpt_flag;
			$deny = $row->b2b_deny_flag;
		}
	} */
	
    if($no>0)
    {
      if(($temp_booking->user_id==$user_id)&&($temp_booking->user_veh_id==$veh_id)&&($temp_booking->service_type==$service_type)&&(round(abs(strtotime($log) - strtotime($temp_booking->crm_update_time)) / 60,2)<5))
      {
        $update_booking = "UPDATE user_booking_tb SET flag='1',flag_unwntd='1' WHERE booking_id = '$booking_id'";
        $res=mysqli_query($conn,$update_booking) or die(mysqli_error($conn));
        $temp_booking = $row_booking;
        continue;
      }
    }
    $temp_booking = $row_booking;
    $no =$no+1;
    $arr[] = array ("booking_id" => $booking_id, "user_id" => $user_id, "mec_id" => $mec_id,
      "log" => $log, "bsource" => $bsource, "veh_type"=>$veh_type, "veh_id" => $veh_id,
      "service_type" => $service_type, "service_date" => $service_date, "shop_name" => $shop_name,
      "alloted_to_id" => $alloted_to_id, "axle_flag" => $axle_flag, "veh_brand" => $veh_brand,
      "veh_model" => $veh_model, "locality" => $locality, "utm_source" => $utm_source, 
      "booking_status" => $booking_status, "priority" => $priority, "flag_fo" => $flag_fo,
      "payment_flag" => $payment_flag,"bike_lp" =>$bike_lp,"otp_verify" => $otp_verify,"rnr_flag" => $rnr_flag , "alloted_to"=>$alloted_to, 
      "user_name"=>$user_name,"user_mobile"=>$user_mobile,"address"=>$address , "home"=>home,"user_level"=>$user_level, "veh_brand"=>$veh_brand,"veh_model"=>$veh_model,"override_status"=>$o_status// ,"acpt"=>$acpt,"deny"=>$deny
	  );
  }
  //var_dump($arr);
  function sort_crm_time($a, $b) {
    if ($a['log'] == $b['log']) {
      return 0;
    }
    return ($a['log'] > $b['log']) ? -1 : 1;
  }
  usort($arr, "sort_crm_time");

  //var_dump($arr);
  $no =0;
  foreach($arr as $a)
  {
   // echo $no;
      $booking_id = $a['booking_id'];
      $user_id = $a['user_id'];
      $mec_id = $a['mec_id'];
      $log = $a['log'];
      $bsource = $a['bsource'];
      $veh_type = $a['veh_type'];
      $veh_id = $a['veh_id'];
      $service_type = $a['service_type'];
      $service_date = $a['service_date'];
      $shop_name = $a['shop_name'];
      $alloted_to_id = $a['alloted_to_id'];
      $axle_flag = $a['axle_flag'];
      $veh_brand = $a['veh_brand'];
      $veh_model = $a['veh_model'];
      $locality = $a['locality'];
      $utm_source = $a['utm_source'];
      $booking_status = $a['booking_status'];
      $priority = $a['priority'];
      $flag_fo = $a['flag_fo'];
      $payment_flag = $a['payment_flag'];
      $bike_lp = $a['bike_lp'];
      $otp_verify = $a['otp_verify'];
      $rnr_flag = $a['rnr_flag'];
      $alloted_to = $a['alloted_to'];
      $user_name = $a['user_name'];
      $user_mobile = $a['user_mobile'];
      $address = $a['address'];
      $home = $a['home'];
      $user_level = $a['user_level'];
      $veh_brand = $a['veh_brand'];
      $veh_model = $a['veh_model'];
      $override_status = $a['override_status'];
      // $accept = $a['acpt'];
      // $deny = $a['deny'];
	  
      $tr = ($alloted_to_id =='' || $alloted_to_id==' ' || $alloted_to_id=='0') ? '<tr style="background-color:#EF5350 !important;">' : '<tr>';
  
      $td1 = '<td><p style="float:left;padding:10px;">'.$booking_id.'</p>';
      if($flag_fo == '1'){
        $td1 = $td1.'<p class="p" style="font-size:16px;color:#D81B60;float:left;padding:5px;" title="Resheduled Booking!"><i class="fa fa-recycle" aria-hidden="true"></i></p>';
        switch($priority){
          case '1': $td1 = $td1.'<p style="font-size:16px;color:#00ACC1;float:left;padding:5px;" title="Low Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';break;
          case '2':$td1 = $td1.'<p style="font-size:16px;color:#FFB300;float:left;padding:5px;" title="Medium Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';break;
          case '3':$td1 = $td1.'<p style="font-size:16px;color:#E53935;float:left;padding:5px;" title="High Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';break;
        }
      }
      if($payment_flag == '1'){
        $td1 = $td1.'<p style="font-size:16px;color:#1c57af;float:left;padding:5px;" title="Paid!"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></p>';                    
      }
                    if($bike_lp == '1' && $otp_verify=='1'){
                  $td1 = $td1.'<p style="font-size:25px;color:green;float:left;padding:5px;" title="OTP Verified!"><i class="fa fa-thumbs-up" aria-hidden="true"></i></p>';
              }
                                        else if($bike_lp == '1'){
                  $td1 = $td1.'<p style="font-size:25px;color:#ffa800;float:left;padding:5px;" title="OTP Dropout!"><i class="fa fa-flag-o" aria-hidden="true"></i></p>';
              }

      if($rnr_flag == '1'){
        $td1 = $td1.'<p style="font-size:16px;color:#1c57af;float:left;padding:5px;" title="RNR!"><i class="fa fa-phone" aria-hidden="true"></i></p>';                    
      }
	  if($axle_flag == '1'){
                  $td1 = $td1.'<p style="font-size:16px;color:#666666;float:left;padding:5px;" title="UnAccepted!"><i class="fa fa-stop-circle" aria-hidden="true"></i></p>';
              }
      $td1 = $td1.'</td>';
          
      $td2 = '<td>';
      if($axle_flag!='1'){
        if($mec_id == '' || $mec_id == 0 || $mec_id == 1 || $service_type == '' || $service_date == '' || $veh_id == '0' ){
            $td2 = $td2.'<a href="user_details.php?t='.base64_encode("l").'&v58i4='.base64_encode($veh_id).'&bi='.base64_encode($booking_id).'" style="background-color:#ffa800;padding:6px;padding-right:8px;border-radius:15px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
        }
        else{
          $td2 = $td2.'<a href="go_axle.php?bi='.$booking_id.'&t=b" style="background-color:#ffa800;padding:6px;padding-right:8px;border-radius:15px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
        }
      }
	  else if($axle_flag == '1')
	  {
		$td2 = $td2.'<p style="background-color:#666666;padding:7px;width:30px;border-radius:18px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>'; 
    }
    if($override_status == '1'){
      $td2 = $td2.'<p style="font-size:16px;color:#D25F34;float:left;padding:5px;" title="override!"><i class="fa fa-exchange" aria-hidden="true"></i></p>';
  }
    
      // Single Form LP Source Booking Intimation
      if ($bsource == 'Single Form LP') {
          $td2 = $td2 . '<p style="float:left;padding:5px;" title="LP"><img src="images/cash-back.png" style="width:20px;"/> </p>';
      }
       
      $td2 = $td2.'</td>';
        
      $td3 = '<td>';
      switch($service_type){
            case 'general_service' :
            if($veh_type =='2w'){
              $td3 = $td3."General Service";
            }
            else{
              $td3 = $td3."Car service and repair";
            }
            break;
            case 'break_down':  $td3 = $td3."Breakdown Assistance"; break;
            case 'tyre_puncher': $td3 = $td3."Tyre Puncture";  break;
            case 'other_service': $td3 = $td3."Repair Job"; break;
            case 'car_wash':
            if($veh_type == '2w'){
              $td3 = $td3."water Wash";
            }
            else{
              $td3 = $td3."Car wash exterior";
            }break;
            case 'engine_oil': $td3 = $td3."Repair Job"; break;
            case 'free_service': $td3 = $td3."General Service"; break;
            case 'car_wash_both':
            case 'completecarspa': $td3 = $td3."Complete Car Spa"; break;
            case 'car_wash_ext': $td3 = $td3."Car wash exterior"; break;
            case 'car_wash_int': $td3 = $td3."Interior Detailing"; break;
            case 'Doorstep car spa': $td3 = $td3."Doorstep Car Spa"; break;
            case 'Doorstep_car_wash': $td3 = $td3."Doorstep Car Wash"; break;
            case 'free_diagnastic':
            case 'free_diagnostic':
            case 'diagnostics':
            if($veh_type == '2w'){
              $td3 = $td3."Diagnostics/Check-up";
          }
          else{
            $td3 = $td3."Vehicle Diagnostics";
          }
              break;
            case 'water_wash': $td3 = $td3."water Wash"; break;
            case 'exteriorfoamwash': $td3 = $td3."Car wash exterior"; break;
            case 'interiordetailing': $td3 = $td3."Interior Detailing"; break;
            case 'rubbingpolish': $td3 = $td3."Car Polish"; break;
            case 'underchassisrustcoating': $td3 = $td3."Underchassis Rust Coating"; break;
            case 'headlamprestoration': $td3 = $td3."Headlamp Restoration"; break;
            default: $td3 = $td3.$service_type;
      }  
      $td3 = $td3.'</td>';
  
      $td4 = '<td><a href="user_details.php?t='.base64_encode("l").'&v58i4='.base64_encode($veh_id).'&bi='.base64_encode($booking_id).'"><i id="'.$booking_id.'" class="fa fa-eye" aria-hidden="true"></i></td>';
      $td5 = '<td>'.$user_name;
      switch($user_level){
        case '5': $td5 = $td5.'<p style="font-size:16px;color:green;padding:5px;" title="Level 5 User"><i class="fa fa-angellist" aria-hidden="true"></i></p>';
        break;
        case '4': $td5 = $td5.'<p style="font-size:16px;color:red;padding:5px;" title="Level 4 User"><i class="fa fa-bug" aria-hidden="true"></i></p>';
        break;
        case '3': $td5 = $td5.'<p style="font-size:16px;color:#26269e;padding:5px;" title="Level 3 User"><i class="fa fa-handshake-o" aria-hidden="true"></i></p>';
        break;
        case '2': $td5 = $td5.'<p style="font-size:16px;color:brown;padding:5px;" title="Level 2 User"><i class="fa fa-anchor" aria-hidden="true"></i></p>';
        break;
        case '1': $td5 = $td5.'<p style="font-size:16px;color:orange;padding:5px;" title="Level 1 User"><i class="fa fa-tripadvisor" aria-hidden="true"></i></p>';
        break;
        default:
    }
    $td5 = $td5.'</td>';
      if(!$_SESSION['eupraxia_flag']) {
          $td6 = '<td>' . $user_mobile . '</td>';
      }
      $td7 = '<td>';
      if($bsource == "Hub Booking"){ 
        if($locality!=''){ 
          $td7 = $td7.$locality; 
        } 
        else if($home == ''){ 
          $td7 = $td7.$address; 
        } else 
        { $td7 = $td7.$home; 
        } 
      } else if($bsource == "GoBumpr App" || $bsource == "App"){ 
        $td7 = $td7.$locality; } 
        else{  
          $td7 = $td7.$locality; 
      } 
      $td7 = $td7.'</td>';
      $td8 = '<td>'.$veh_brand." ".$veh_model.'</td>';
      $td9 = '<td>'.$shop_name.'</td>';
      $td10 = '<td>';
      if($service_date == "0000-00-00"){ 
        $td10 = $td10."Not Updated Yet"; } 
        else {  
          $td10 = $td10.date('d M Y', strtotime($service_date)); 
      }
      $td10 = $td10.'</td>';
      if($crm_log_id=='crm012'){ 
        $td10 = $td10."<td>$alloted_to</td>"; 
      }
      $td11 = '<td>'.date('d M Y h.i A', strtotime($log)).'</td>';
        
      $tr_l ='</tr>';
      
      if($_SESSION['eupraxia_flag']) {
          $str = $tr . $td1 . $td2 . $td3 . $td4 . $td5 . $td7 . $td8 . $td9 . $td10 . $td11 . $tr_l;
      }else{
          $str = $tr . $td1 . $td2 . $td3 . $td4 . $td5 . $td6 . $td7 . $td8 . $td9 . $td10 . $td11 . $tr_l;
      }
      $data[] = array('tr'=>$str);
  }
  echo $data1 = json_encode($data);
} // if
else {
  echo "no";
}



?>

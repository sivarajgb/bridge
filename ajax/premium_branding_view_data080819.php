<?php
error_reporting(E_ALL); ini_set('display_errors', 1);
include("../config.php");

$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$flag=$_SESSION['flag'];

$start_date = $_POST['start'];
$end_date = $_POST['end'];
$shop_id = $_POST['shop'];
$type = $_POST['type'];
$total = $_POST['total'];

if($type == 'completed'){
    $sql_counts = "SELECT ms.master_service,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1')) THEN 1 ELSE 0 END ) AS count FROM go_bumpr.user_booking_tb as g INNER JOIN b2b.b2b_booking_tbl as b ON g.booking_id=b.gb_booking_id  LEFT JOIN b2b.b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id LEFT JOIN go_bumpr.go_axle_service_price_tbl as ms ON (g.service_type=ms.service_type AND g.vehicle_type=ms.type) WHERE g.mec_id='$shop_id' AND g.axle_flag='1' AND b.b2b_swap_flag!='1'  AND s.b2b_acpt_flag='1' AND DATE(b.b2b_log) BETWEEN '$start_date' AND '$end_date' GROUP BY ms.master_service  ORDER BY count DESC";
}else{
    $sql_counts = "SELECT ms.master_service,count(b.b2b_booking_id) as count FROM go_bumpr.user_booking_tb as g INNER JOIN b2b.b2b_booking_tbl as b ON g.booking_id=b.gb_booking_id  LEFT JOIN b2b.b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id LEFT JOIN go_bumpr.go_axle_service_price_tbl as ms ON (g.service_type=ms.service_type AND g.vehicle_type=ms.type) WHERE g.mec_id='$shop_id' AND g.axle_flag='1' AND b.b2b_swap_flag!='1'  AND s.b2b_acpt_flag='1' AND DATE(b.b2b_log) BETWEEN '$start_date' AND '$end_date' GROUP BY ms.master_service ORDER BY count DESC";
}
$res_counts = mysqli_query($conn,$sql_counts);

if(mysqli_num_rows($res_counts)>0){
    ?>
    <div style="width:90%;margin-left:8px;">
    <table class="table table-bordered" style="border:1px solid #00BCD4;margin:15px;" >
    <thead style="background-color:#B2DFDB;align:center;display: table-header-group;">
    <th>ServiceType</th>
    <th>Count</th>
    <th>Percentage(%)</th>
    </thead>
    <tbody>
    <?php
    while($row_counts = mysqli_fetch_array($res_counts)){
        $service = $row_counts['master_service'];
        $count = $row_counts['count'];
        if($count == 0 ){
            continue;
        }
        $count_per = round(($count/$total)*100,1);
        ?>
        <tr>
        <td style="text-align:center;"><?php echo $service; ?></td>
        <td style="text-align:center;"><?php echo $count; ?></td>
        <td style="text-align:center;"><?php echo $count_per.' %'; ?></td>
        </tr>
        <?php
    }
    ?>
    </tbody>
    </table>
    </div>
    <?php
}
else {
  echo "no results found!!!";
}
?>

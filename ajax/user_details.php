<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id']))) {
	header('location:logout.php');
	die();
}
$today = date('Y-m-d');

$booking_id = base64_decode($_GET['bi']);
$type = base64_decode($_GET['t']);

if($booking_id == '' || $type == ''){
  header('location:somethingwentwrong.php');
	die();
}
?>

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
  <title>GoBumpr Bridge</title>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


<!-- auto complete -->
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/jquery-ui.structure.min.css">
<link rel="stylesheet" href="css/jquery-ui.theme.min.css">
<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>
<!-- date time picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="css/bootstrap-datetimepicker-standalone.css">

  <style>
  nav,ol{
	  background: #009688 !important;
	  font-size:18px;
	   margin-top: -4px;
  }
   .navbar-fixed-top {
    top: 0;
    border-width: 0 0 0px;
  }
  body{
	  background: #fff !important;
	  color:black;
  }
	<!-- auto complete -->
	@charset "utf-8";.ui-autocomplete{z-index:1000 !important;cursor:default;list-style:none;}
	.ui-widget{}
	.ui-autocomplete{overflow-y:auto;overflow-x:hidden;}
	.ui-menu{width:0px;display:none;}
	.ui-autocomplete > li{padding:10px;padding-left:10px;}
	ul{margin-bottom:0;}
	.ui-autocomplete > li.ui-state-focus{background-color:#DDD;}
	.ui-autocomplete .ui-front .ui-menu .ui-widget .ui-widget-content{border:0;}
	.ui-helper-hidden-accessible{display:none;}
	.gobumpr-icon{font-style:normal;font-weight:normal;speak:none;display:inline-block;text-decoration:inherit;text-align:center;font-variant:normal;text-transform:none;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
	.ui-widget{background-color:white;width:100%;}
	.ui-widget-content{padding-left:1px;display:block;width:20px;position:relative;line-height:12px;max-height:210px;border:.5px solid #DADADA;}
	.ui-widget{}
	.ui-autocomplete { position: absolute; cursor: default;}


<!-- vehicle type -->
.bike,
.car{
  cursor: pointer;
  user-select: none;
  -webkit-user-select: none;
  -webkit-touch-callout: none;
}
.bike > input,
.car > input{ /* HIDE ORG RADIO & CHECKBOX */
  visibility: hidden;
  position: absolute;
}
/* RADIO & CHECKBOX STYLES */
.bike > i,
.car > i{     /* DEFAULT <i> STYLE */
  display: inline-block;
  vertical-align: middle;
  width:  16px;
  height: 16px;
  border-radius: 50%;
  transition: 0.2s;
  box-shadow: inset 0 0 0 8px #fff;
  border: 1px solid gray;
  background: gray;
}

label > input{ /* HIDE RADIO */
  visibility: hidden; /* Makes input not-clickable */
  position: absolute; /* Remove input from document flow */
}
label > input + img{ /* IMAGE STYLES */
  cursor:pointer;
  border:2px solid transparent;
}
label > input:checked + img { /* (RADIO CHECKED) IMAGE STYLES */
   border-radius:12px;
   padding:5px;
   background-color:#ffa800;
  box-shadow: 0 0 3px 0 #394;
}
.borderless td, .borderless th {
    border: none !important;
}
	/* anchor tags */
  a{
	  text-decoration:none;
	  color:black;
  }
   a:hover{
	  text-decoration:none;
	  color:#4B436A;
  }
    .datepicker {
	  cursor:pointer;
  }
.datepicker:before {
content: '';
display: inline-block;
border-left: 7px solid transparent;
border-right: 7px solid transparent;
border-bottom: 7px solid #ccc;
border-bottom-color: transparent !important;
position: absolute;
top: -7px;
left: 190px;  // I made a change here 
}

.datepicker:after {
content: '';
display: inline-block;
border-left: 6px solid transparent;
border-right: 6px solid transparent;
border-top-color: transparent !important;
border-top: 6px solid #ffffff;
position: absolute;
bottom: -6px;
left: 191px;  // I made a change here 
}

#datepick > span:hover{cursor: pointer;}

 .floating-box {
	 display: inline-block;
	 margin: 22px;
 padding: 22px;
 width:203px;
 height:105px;
 box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  font-size: 17px;
}

/* vertical menu */
/* Mixin */
@mixin vertical-align($position: relative) {
  position: $position;
  top: 50%;
  -webkit-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}

.ver_menu {
  @include vertical-align();
}

select{
    color: red;
}
option{
  height: 25px;
}
option:hover {
  box-shadow: 0 0 10px 100px #ddd inset;
}
  </style>
</head>
<body>
<?php include_once("header.php"); ?>
 <div class="overlay" data-sidebar-overlay></div>

<div class="padding"></div>
  <?php
    // get user id  from user_booking_table
      $sql_user_bk = "SELECT user_id,user_veh_id,user_vech_no,booking_status FROM user_booking_tb WHERE booking_id='$booking_id' ";
      $res_user_bk = mysqli_query($conn,$sql_user_bk);
      $row_user_bk = mysqli_fetch_object($res_user_bk);

      $user_id = $row_user_bk->user_id;
      $veh_id=$row_user_bk->user_veh_id;
      $reg_no = $row_user_bk->user_vech_no;
      $status = $row_user_bk->booking_status;

  // get vehicle details from user_vehicle_table
      $sql_user_veh = "SELECT reg_no,brand,model,flag FROM user_vehicle_table WHERE id='$veh_id'";
      $res_user_veh = mysqli_query($conn,$sql_user_veh);
      $row_user_veh = mysqli_fetch_object($res_user_veh);

      $veh_number = $row_user_veh->reg_no;
      $veh_brand = $row_user_veh->brand;
      $veh_model = $row_user_veh->model;
      $veh_flag = $row_user_veh->flag;


  // get user details from user register table using user id
      $sql_user = "SELECT name,email_id,mobile_number,lat_lng,Locality_Home FROM user_register where reg_id='$user_id' ";
      $res_user = mysqli_query($conn,$sql_user);
      $row_user = mysqli_fetch_object($res_user);

      $user_name = $row_user->name;
      $user_mail = $row_user->email_id;
      $user_mobile = $row_user->mobile_number;
      $user_location = $row_user->lat_lng;
      if($user_location == ''){
        $user_location = $row_user->Locality_Home;
      }

  ?>
  <div  id="user" style=" margin-left:20px;border:2px solid #708090; border-radius:8px; width:380px;height:430px; padding:20px; margin-top:18px; float:left;">
    <table id="table1" class="table borderless">
       <tr><td><strong>User Id</strong></td><td><?php echo $user_id; ?></td></tr>
       <tr><td><strong>Name</strong></td><td><?php echo $user_name; ?></td></tr>
       <tr><td><strong>Phn No.</strong></td><td><?php echo $user_mobile; ?></td></tr>
       <tr><td><strong>E-mail</strong></td><td><?php echo $user_mail; ?></td></tr>
       <tr><td><strong>Veh No.</strong></td><?php if($veh_flag == '0'){ ?> <td style="color:red;"><?php echo $reg_no; ?></td> <?php } else{ ?> <td><?php echo $reg_no; ?></td> <?php } ?></tr>
       <tr><td><strong>Brand</strong></td><?php if($veh_flag == '0'){ ?> <td style="color:red;"><?php echo $veh_brand; ?>,<?php echo $veh_model; ?></td> <?php } else{ ?> <td><?php echo $veh_brand; ?>,<?php echo $veh_model; ?></td> <?php } ?></tr>
       <tr><td><strong>Current Location<strong></td><td><?php echo $user_location; ?></td></tr>
  </table>
<!-- Edit User -->
<div  style="float:left; margin-left:90px;">
   <div id="edting_user" style="display:inline-block; align-items:center;" >
       <!-- Trigger the modal with a button -->
       <button id="eu" type="button" class="btn btn-sm" data-toggle="modal" data-target="#myModal_user" style="background-color:#E0F2F1;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;Edit User</button>

       <!-- Modal -->
       <div class="modal fade" id="myModal_user" role="dialog" >
         <div class="modal-dialog" style="width:860px;">

          <!-- Modal content-->
          <div class="modal-content">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Edit User</h3>
             </div>
             <div class="modal-body">
								<?php
								$sql_edit_user ="SELECT name,mobile_number,email_id,City,Locality_Home,Locality_Work,source,campaign,Last_service_date,Next_service_date,Last_called_on,Follow_up_date,comments FROM user_register WHERE reg_id='$user_id'";
								$res_edit_user = mysqli_query($conn, $sql_edit_user);
								$row_edit_user = mysqli_fetch_object($res_edit_user);
								$u_name = $row_edit_user->name;
								$u_mobile = $row_edit_user->mobile_number;
								$u_mail  = $row_edit_user->email_id;
								$u_city  = $row_edit_user->City;
								$u_loc_home  = $row_edit_user->Locality_Home;
								$u_loc_work  = $row_edit_user->Locality_Work;
								$u_source = $row_edit_user->source;
								$u_campaign = $row_edit_user->campaign;
								$u_last_serviced  = $row_edit_user->Last_service_date;
								$u_next_serviced  = $row_edit_user->Next_service_date;
								$u_last_called  = $row_edit_user->Last_called_on;
								$u_followup  = $row_edit_user->Follow_up_date;
								$u_comments  = $row_edit_user->comments;
								?>
								<form id="edit_user" class="form" method="post" action="edit_user.php">
                  <div class="row">
                       <br>
                       <div class="col-xs-3 col-xs-offset-1 form-group">
                         <input class="form-control" type="text" id="mobile" name="mobile" placeholder="Mobile" readonly maxlength="10" value="<?php echo $u_mobile; ?>">
                       </div>

                      <div class="col-xs-3 form-group">
                          <input  class="form-control" type="text" id="user_name" name="user_name"  pattern="^[a-zA-Z0-9\s]+$" onchange="try{setCustomValidity('')}catch(e){}" placeholder="User Name" required  value="<?php echo $u_name; ?>">
                      </div>

                      <div class="col-xs-4 form-group">
                         <input type="email" class="form-control" id="email" name="email" placeholder="E-Mail"  value="<?php echo $u_mail; ?>">
                      </div>
                  </div>
                  <div class="row"></div>
                  <div class="row">
                      <div id="city_u" class="col-xs-4 col-xs-offset-1 form-group">
			                    <div class="ui-widget">
                             <select class="form-control" id="city" name="city" required>
                             <?php
                             if($u_city == "chennai" || $u_city == "Chennai" || $u_city == "CHENNAI"){
                               ?>
                               <option value="Chennai" selected>Chennai</option>
                               <option value="Bangalore">Bangalore</option>
                               <option value="Coimbatore">Coimbatore</option>
                               <?php
                             }
                             else if($u_city=="bangalore" || $u_city=="Bangalore" || $u_city=="BANGALORE"){
                               ?>
                                <option value="Chennai">Chennai</option>
                               <option value="Bangalore" selected>Bangalore</option>
                               <option value="Coimbatore">Coimbatore</option>
                               <?php
                             }
                              else if($u_city=="coimbatore" || $u_city=="Coimbatore" || $u_city=="COIMBATORE"){
                               ?>
                                <option value="Chennai">Chennai</option>
                               <option value="Bangalore">Bangalore</option>
                               <option value="Coimbatore" selected>Coimbatore</option>
                               <?php
                             }
                             else{
                               ?>
                               <option value="" selected>Select City</option>
                                <option value="Chennai" >Chennai</option>
                               <option value="Bangalore">Bangalore</option>
                               <option value="Coimbatore">Coimbatore</option>
                               <?php
                             }
                             ?>

                             </select>
			                    </div>
                      </div>
                      <div id="loc_home" class="col-xs-3 form-group">
                        <div class="ui-widget" id="loc_home">
        	                <input class="form-control autocomplete" id="location_home" type="text" name="location_home" placeholder="Home Locality" value="<?php echo $u_loc_home; ?>">
                       </div>
                      </div>

                      <div id="loc_work" class="col-xs-3 form-group">
                          <div class="ui-widget" id="loc_work">
        	                <input class="form-control autocomplete" id="location_work" type="text" name="location_work" placeholder="Work Locality" value="<?php echo $u_loc_work; ?>">
                       </div>
                      </div>
									</div>
                  <div class="row"></div>
                  <div class="row">
                    <div id="source_u" class="col-xs-5 col-xs-offset-1 form-group">
			                  <div class="ui-widget">
                           <select class="form-control" id="source" name="source">
                           <option value="<?php echo $u_source; ?>" selected><?php echo $u_source; ?></option>
                           <?php 
      $sql_sources = "SELECT user_source FROM user_source_tbl WHERE flag='0' AND user_source !='$u_source' ORDER BY  user_source ASC";
      $res_sources = mysqli_query($conn,$sql_sources);
      while($row_sources = mysqli_fetch_object($res_sources)){
        $source_name = $row_sources->user_source;
        ?>
        <option value="<?php echo $source_name; ?>"><?php echo $source_name; ?></option>
        <?php
      }
      ?>
                           </select>
			                  </div>
                    </div>

                   <div id="cam" class="col-xs-5 form-group">
			                <div class="ui-widget">
				                  <input class="form-control" id="campaign" type="text" name="campaign"  placeholder="Campaign"   value="<?php echo $u_campaign; ?>">
			                </div>
                   </div>
							    </div>
                  <div class="row"></div>
                  <div class="row">

                 <div class="col-xs-2 col-xs-offset-1 form-group">
                    <label> Last Serviced</label></div>
                    <div class="col-xs-3  form-group">
                    <?php
                    if($u_last_serviced == $today){
                      ?>
                      <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="last_service_date" name="last_service_date"  value="<?php echo date('d-m-Y',strtotime($u_last_serviced)); ?>">
                    <?php }
                    else{
                      ?>
                        <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="last_service_date" name="last_service_date"  value="<?php echo date('d-m-Y',strtotime($u_last_serviced)); ?>" readonly>
                    <?php } ?>
                   </div>
                   <div class="col-xs-2 form-group">
                       <label> Next Service On</label>
									 </div>
                   <div class="col-xs-3  form-group">
                      <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="next_service_date" name="next_service_date"  value="<?php echo date('d-m-Y',strtotime($u_next_serviced)); ?>">
                   </div>
                  </div>
                  <div class="row"></div>
                  <div class="row">
              <div class="col-xs-2 col-xs-offset-1 form-group">
                <label> Last Called On</label>
              </div>
              <div class="col-xs-3  form-group">
              <?php
              if($u_last_called == $today){
                ?>
                <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="last_called_on" name="last_called_on"  value="<?php echo date('d-m-Y',strtotime($u_last_called)); ?>">
             <?php  }
              else{
                ?>
                 <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="last_called_on" name="last_called_on"  value="<?php echo date('d-m-Y',strtotime($u_last_called)); ?>" readonly>
              <?php } ?>
              </div>
              <div class="col-xs-2 form-group">
                 <label> FollowUp Date</label>
							</div>
              <div class="col-xs-3  form-group">
                  <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="follow_up_date" name="follow_up_date"  value="<?php echo date('d-m-Y',strtotime($u_followup)); ?>">
              </div>
                  </div>
                  <div class="row"></div>
                  <div class="row">
           <div class="col-xs-10 col-xs-offset-1 form-group">
              <textarea class="form-control" maxlength="100" id="comments" name="comments" placeholder="Comments..." ><?php echo $u_comments; ?></textarea>
          </div>
        </div>
                  <div class="row"></div>
          <div class="row">
           <br>
           <div class="form-group" align="center">
            <input class="form-control" type="submit" id="edit_user_submit" name="edit_user_submit" value="Update" style=" width:90px; background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
          </div>
				</div>
                   <input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
									 <input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
								   <input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
                   <input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
                 </form>
</div> <!-- modal body -->
</div> <!-- modal content -->
</div>  <!-- modal dailog -->
</div>  <!-- modal -->

</div>
</div> <!-- edit user -->
</div>
<div style=" margin-top:19px; margin-left:45px; width:700px; max-height:420px; overflow-y:auto; float:left;">
  <table id="table2" class="table table-bordered table-responsive " style="border-collapse:collapse; ">
	<thead style="background-color: #D3D3D3;">
	 <th>No</th>
	 <th>VehicleNo</th>
	 <th>Activity</th>
	 <th>ServiceLogDate</th>
	 <th>Support</th>
	 <th>Service</th>
	 </thead>
	 <tbody>
	 <?php
	 $reg_array=array();


		$sql1=mysqli_query($conn,"SELECT * FROM user_booking_tb WHERE user_id='$user_id' ORDER by log DESC");
		while($row1=mysqli_fetch_object($sql1)){
			$reg_array[]=$row1;
		}
		$sql2=mysqli_query($conn,"SELECT * FROM user_register WHERE reg_id='$user_id' ORDER by log DESC");
		while($row2=mysqli_fetch_object($sql2)){
			$reg_array[]=$row2;
		}

		$sql3=mysqli_query($conn,"SELECT * FROM user_vehicle_table WHERE user_id='$user_id' ORDER by log DESC");
		while($row3=mysqli_fetch_object($sql3)){
			$reg_array[]=$row3;
		}
		$sql4=mysqli_query($conn,"SELECT * FROM user_activity_tbl WHERE user_id='$user_id' ORDER by log DESC");
		$status_count=mysqli_num_rows($sql4);
		while($row4=mysqli_fetch_object($sql4)){
			$reg_array[]=$row4;
		}
		$sql5=mysqli_query($conn,"SELECT *,Follow_up_date AS cmnt_followup_date FROM admin_comments_tbl WHERE user_id='$user_id' ORDER by log DESC");
		$status_comments=mysqli_num_rows($sql5);
		while($row5=mysqli_fetch_object($sql5)){
			$reg_array[]=$row5;
		}
		function do_compare($item1, $item2) {
	    $ts1 = strtotime($item1->log);
	    $ts2 = strtotime($item2->log);
	    return $ts2 - $ts1;
	     }
	usort($reg_array, 'do_compare');

		 $arr_count = count($reg_array);

		  $x=0;
		  $axle = false;
		 for($i=0;$i<$arr_count;$i++) {
					  $x=$x+1;
	 if($reg_array[$i]->axle_flag==1&&$axle==false)
	 {
		$axle = true;
		$axle_q = "SELECT g.go_booking_id,g.b2b_shop_id,g.sent_log,b.b2b_shop_name FROM goaxle_track g,b2b.b2b_mec_tbl b WHERE g.b2b_shop_id = b.b2b_shop_id AND g.go_booking_id = '".$reg_array[$i]->booking_id."'";
		$axle_res = mysqli_query($conn,$axle_q);
		$count = mysqli_num_rows($axle_res);
		if($count==0)
			continue;
		$axle_row = mysqli_fetch_object($axle_res);
		$activity = "GoAxled to ".$axle_row->b2b_shop_name." - ".$reg_array[$i]->booking_id;
		$service_type = $reg_array[$i]->service_type;
		if($axle_row->sent_log == '0000-00-00 00:00:00')
		$log_date=date("d-m-Y h:i a");
		else
		$log_date=date('d-m-Y h:i a',strtotime($axle_row->sent_log));
		 }
	 elseif($axle==true){
		 $axle= false;
			$activity =  'BOOKING -'.$reg_array[$i-1]->booking_id.' -'.$reg_array[$i-1]->shop_name ;
			$veh_no = $reg_array[$i-1]->user_vech_no ;
							 $service_type = $reg_array[$i-1]->service_type;
               $support= $reg_array[$i-1]->crm_update_id;
			   $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i-1]->log));
			   
					      	  }
	 elseif(isset($reg_array[$i]->booking_id)){
					      	 $activity =  'BOOKING -'.$reg_array[$i]->booking_id.' -'.$reg_array[$i]->shop_name ;
							 $veh_no = $reg_array[$i]->user_vech_no ;
							 $service_type = $reg_array[$i]->service_type;
               $support= $reg_array[$i]->crm_update_id;
			   $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
			  
					      	  }
	 elseif(isset($reg_array[$i]->reg_id)){

					      	$activity = 'USER REGISTER -'.$reg_array[$i]->reg_id.'-'.$reg_array[$i]->name.'-'.$reg_array[$i]->email_id.'-'.$reg_array[$i]->mobile_number;
							    $veh_no = '';
							    $service_type = '';
                  $support= $reg_array[$i]->crm_log_id;
				  $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));

					      	}
	 elseif(isset($reg_array[$i]->vehicle_id)){

					      	$activity = 'VEHICLE REGISTER -'.$reg_array[$i]->brand.'-'.$reg_array[$i]->model.'-'.$reg_array[$i]->type;
					      	$veh_no = $reg_array[$i]->reg_no;
							    $service_type = '';
                  $support= $reg_array[$i]->crm_log_id;
				  $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));

					      	}
	 elseif(isset($reg_array[$i]->user_activity)){
						      				if($status_count<=1){

						      					    $activity = 'User Activity-'.'Prospect to '.$reg_array[$i]->user_activity;
												        $veh_no = '';
							                  $service_type = '';
                                 $support= $reg_array[$i]->crm_log_id;
								 $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
                            }
						      					else
						      					{
						      						$activity = 'User Activity-'. $reg_array[$i]->user_activity;
													    $veh_no = '';
							                $service_type = '';
                              $support= $reg_array[$i]->crm_log_id;
							  $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
						      					}


					      	}
	 elseif(isset($reg_array[$i]->com_id)){
     $cmt = $reg_array[$i]->comments;
     $support= $reg_array[$i]->crm_log_id;
	 if($reg_array[$i]->cmnt_followup_date!='0000-00-00')
	 {
		$followup_on='-Follow Up On- '.date("d-M-Y",strtotime($reg_array[$i]->cmnt_followup_date));
	 }
	 else
	 {
		$followup_on="";
	 }
     if($cmt == ''){
       $cmt="No Comments!";
     }
	      $act_status = $reg_array[$i]->status;
        if($act_status == ""){
          $act_status = "FollowUp";
        }
        if($act_status == "Cancelled"){
          	$activity = $reg_array[$i]->category.'-REASON-'.$cmt.$followup_on;
			$log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
        }
        else if($act_status == "Reverted"){
          	$activity = $reg_array[$i]->category.$followup_on;
			$log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
        }
        else{
            $activity = $act_status.'-'.$reg_array[$i]->category.'-REASON-'.$cmt.$followup_on;
			$log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
        }
		$service_type = '';
    $veh_no = '';

		}
	  /*if(isset($reg_array[$i]->booking_id)){
			$bkid = $reg_array[$i]->booking_id;
      if($booking_id == $bkid){
      ?>
      <tr style="background-color:#B2DFDB;">
      <?php
      }
      else{
      ?>
      <tr>
      <?php
      }
    }*/
	//$veh_no = $activity;
	if (strlen(stristr($activity, 'BOOKING -'))>0) {
		
		$bkid=explode("-",$activity);
		$v = explode(" ",$bkid[1]);
		$bookid = $v[0];
		
		if($booking_id===$bookid)
		{
		?>
			<tr style="background-color:#B2DFDB;">
			<?php
		}
		else{
		?>
		  <tr>
		  <?php
		}
		}
		else{
		?>
		  <tr>
		  <?php
		}
	 ?>
	 <td><?php echo $x;?></td>
	 <td><?php echo $veh_no; ?></td>
	 <td><?php echo $activity ; ?></td>
	 <td><?php echo $log_date;?></td>
	 <td><?php
	//echo "SELECT crm_lod_id FROM admin_comments_tbl WHERE log='$x' ";
	  $sql_crm_name = mysqli_query($conn,"SELECT name FROM crm_admin WHERE crm_log_id = '$support' ");
	  $row_sup_name= mysqli_fetch_array($sql_crm_name);
	  echo $sup_name=$row_sup_name['name']; ?></td>
	 <td><?php echo $service_type; ?></td>
	 </tr>
	 <?php
		 }
		 ?>
	 </tbody>
 </table>
</div>



<!-- Add Vehicle -->
<!-- Trigger the modal with a button -->
<div id="av" style="position:fixed; margin-left:35%; bottom:20px;display:none;">
<button id="a1" type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_vehicle" style="background-color:#4CB192 ;"><i class="fa fa-car" aria-hidden="true"></i>&nbsp;&nbsp;Add Vehicle</button>
</div>

<!-- cancel booking -->
<div id="cb" style="position:fixed; margin-left:15%; bottom:20px;display:none;">
<button class="btn btn-md" id="cancel" data-toggle="modal" data-target="#myModal_cancel_booking" style="background-color:#FF7043;"><i class="fa fa-ban" aria-hidden="true"></i>&nbsp;&nbsp;Cancel Booking</button>
</div>
<!-- push others to bookings -->
		<div id="pbook" style="position:fixed; margin-left:55%; bottom:20px;display:none;">
    <button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_followup"  style="position:relative;background-color:#58da80;width:180px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Push To FollowUp</button>
<button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_add_booking"  style="position:relative;background-color:#39B8AC;width:180px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Push To Bookings</button>
     </div>
<!-- revert cancelled bookings -->
		<div id="revert" style="position:fixed; margin-left:55%; bottom:20px;display:none;">
    <a href="revert_cancel_booking.php?id=<?php echo base64_encode($booking_id); ?>&t=<?php echo base64_encode($type); ?>"><button type="button" class="btn btn-md" style="position:relative;background-color:#39B8AC;width:130px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i>&nbsp;Revert Back</button></a>
    </div>
<!-- push button -->
<div id="pb" style="position:fixed; margin-left:55%; bottom:20px;display:none;">

	<!-- vertical menu  leads-->
	<div style="display:none; position:relative;" id = "var_menu1">
		<div class="ver_menu" id="eb">
		 <button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_edit_booking"  style="position:relative;background-color:#39B8AC;width:130px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Edit Bookings</button>
	  </div>
		<div class="ver_menu" >
		<button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_followup"  style="position:relative;background-color:#D0D854;width:100px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;FollowUp</button>
	  </div>
	  <div class="ver_menu" >
		<button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_others" style="position:relative;background-color:#D88854;width:100px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Others</button>
	  </div>
	</div>
	<!-- vertical menu bookings-->
	<div style="display:none; position:relative;" id = "var_menu2">
		<div class="ver_menu" >
		<button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_followup" style="position:relative;background-color:#D0D854;width:100px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;FollowUp</button>
	  </div>
	  <div class="ver_menu" >
		<button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_others" style="position:relative;background-color:#D88854;width:100px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Others</button>
	  </div>
	</div>
	<!-- vertical menu followup-->
  <div style="display:none; position:relative;" id = "var_menu3">
    <div class="ver_menu" id="rebook">
    <button id="rb" type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_reschedule" style="position:relative;background-color:#4DD0E1;width:130px;margin:10px; margin-bottom:10px;"><i class="fa fa-recycle" aria-hidden="true"></i>&nbsp;&nbsp;Reschedule</button>
    </div>
    <div class="ver_menu" id="ebook">
    <button id="eb1" type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_edit_booking" style="position:relative;background-color:#E19A16;width:130px;margin:10px; margin-bottom:10px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;Edit Booking</button>
    </div>
    <div class="ver_menu" >
		 <button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_add_booking"  style="position:relative;background-color:#B5A5C3;width:130px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;New Booking</button>
	  </div>
		<div class="ver_menu" >
		<button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_rnr1"  style="position:relative;background-color:#39B8AC;width:100px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;RNR 1</button>
	  </div>
	  <div class="ver_menu" >
		<button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_others"  style="position:relative;background-color:#D88854;width:100px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Others</button>
	  </div>
	</div>
	<!-- vertical menu rnr1-->
	<div style="display:none; position:relative;" id = "var_menu4">
    <div class="ver_menu" id="rebook">
    <button id="rb" type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_reschedule" style="position:relative;background-color:#4DD0E1;width:130px;margin:10px; margin-bottom:10px;"><i class="fa fa-recycle" aria-hidden="true"></i>&nbsp;&nbsp;Reschedule</button>
    </div>
    <div class="ver_menu" id="ebook">
    <button id="eb1" type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_edit_booking" style="position:relative;background-color:#E19A16;width:130px;margin:10px; margin-bottom:10px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;Edit Booking</button>
    </div>
    <div class="ver_menu" >
		<button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_add_booking"  style="position:relative;background-color:#B5A5C3;width:130px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;New Booking</button>
	  </div>
		<div class="ver_menu" >
		<button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_rnr2"  style="position:relative;background-color:#D0D854;width:90px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;RNR 2</button>
	  </div>
	  <div class="ver_menu" >
		<button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_others"  style="position:relative;background-color:#D88854;width:90px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Others</button>
	  </div>
	</div>
	<!-- vertical menu rnr2-->
	<div style="display:none; position:relative;" id = "var_menu5">
    <div class="ver_menu" id="rebook">
    <button id="rb" type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_reschedule" style="position:relative;background-color:#4DD0E1;width:130px;margin:10px; margin-bottom:10px;"><i class="fa fa-recycle" aria-hidden="true"></i>&nbsp;&nbsp;Reschedule</button>
    </div>
    <div class="ver_menu" id="ebook">
    <button id="eb1" type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_edit_booking" style="position:relative;background-color:#E19A16;width:130px;margin:10px; margin-bottom:10px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;Edit Booking</button>
    </div>
    <div class="ver_menu" >
		<button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_add_booking"  style="position:relative;background-color:#B5A5C3;width:130px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;New Booking</button>
	  </div>
		 <div class="ver_menu" >
		<button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_others"  style="position:relative;background-color:#D88854;width:90px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Others</button>
	  </div>
	</div>



<button id="a3" type="button" class="btn btn-md push"  style="background-color:#47B862;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;Push User To</button>
</div>

<!-- jQuery library -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<!-- show 3 buttons -->

<script>
$(document).ready(function(){
		var t = "<?php echo $type; ?>" ;
		if(t == "o"){
			$("#av").hide();
			$("#pb").hide();
      $("#cb").hide();
      $("#pbook").show();
		}
    else{
			$("#av").show();
			$("#pb").show();
      $("#cb").show();
		}
    if(t == "c"){
			$("#av").hide();
			$("#pb").hide();
      $("#pbook").hide();
      $("#cb").hide();
      $("#revert").show();
		}
    var axle_flag = "<?php $sql_get_axle = mysqli_query($conn,"SELECT axle_flag FROM user_booking_tb WHERE booking_id='$booking_id'");
    $row_get_axle = mysqli_fetch_object($sql_get_axle);
    $axle_flag = $row_get_axle->axle_flag;
    echo $axle_flag; ?>";
    //console.log(axle_flag);
    if(axle_flag == 1 && t == "f"){
      $("#ebook").hide();
      $("#cb").hide();
    }
    if(axle_flag == 1 && t == "b"){
      $("#cb").hide();
    }
    if(t == "eb"){
      $("#eu").hide();
      $("#av").hide();
			$("#pb").hide();
      $("#cb").hide();
    }
    if(t == "unwanted"){
      $("#av").show();
			$("#pb").show();
      $("#cb").show();
      $("#eu").show();
      $("#revert").hide();
      $("#pbook").hide();
    }
});
</script>

<!-- push button -->
<script>
$(document).ready(function(){
	$(".push").click(function(){
		var t = "<?php echo $type; ?>" ;
		var s = "<?php echo $status; ?>" ;
		if(t == "l" || t == "unwanted"){
			$("#var_menu1").slideToggle("slow");
		}
		if(t == "b"){
			$("#var_menu2").slideToggle("slow");

		}
		if(t == "f"){
			if(s == "4")
			{
				$("#var_menu4").slideToggle("slow");
			}
			if(s == "5"){
				$("#var_menu5").slideToggle("slow");
			}
			if(s == "3"){
				$("#var_menu3").slideToggle("slow");
			}
		}
});
});
</script>
<!-- zoom buttons -->
<script>
$(document).ready(function() {
  var oldSize = parseFloat($("#a1").css('font-size'));
  var newSize = oldSize  * 1.2;
  $("#a1").hover(
    function() {
     $("#a1").animate({ fontSize: newSize}, 200);
    },
    function() {
    $("#a1").animate({ fontSize: oldSize}, 200);
   }
 );
});
</script>

<script>
$(document).ready(function() {
  var oldSize = parseFloat($("#a3").css('font-size'));
  var newSize = oldSize  * 1.2;
  $("#a3").hover(
    function() {
     $("#a3").animate({ fontSize: newSize}, 200);
    },
    function() {
    $("#a3").animate({ fontSize: oldSize}, 200);
   }
 );
});
</script>

<!-- add vehicle -->
<!-- Modal -->
<div class="modal fade" id="myModal_vehicle" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Add Vehicle (<?php echo $user_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="add_vehicle" class="form" method="post" action="add_vehicle.php" >
<div class="row">
  <div class="col-xs-6 col-xs-offset-3 form-group">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <label class="bike">
 <input id="veh" type="radio" name="veh" value="2w" />
 <img id="bike" src="images/bike.png">
  </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <label class="car">
  <input id="veh" type="radio" name="veh" value="4w" />
  <img id="car" src="images/car.png">
  </label>
  </div>
</div>
<div class="row"></div>
<div class="row">
<div id="b_m" class="col-xs-6 col-xs-offset-3 form-group">
			 <div class="ui-widget">
				 <input class="form-control autocomplete" id="BrandModelid" type="text" name="BrandModelid"  required placeholder="Select Model">
			 </div>
</div>
</div>

<div class="row"></div>
<div class="row">
<div class="col-xs-6 col-xs-offset-3 form-group">
<select class="form-control" id="fuel" name="fuel">
<option selected >Fuel Type </option>
<option data-imagesrc="images/bike.png" value="Diesel">Diesel</option>
<option data-imagesrc="images/car.png" value="Petrol">Petrol</option>
</select>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-6 col-xs-offset-3 form-group">
<input class="form-control" type="text" id="regno" name="regno"  data-mask-reverse="true" maxlength="13" placeholder="Vehicle No">
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-6 col-xs-offset-3 form-group">
<input class="form-control" type="text" id="year" name="year" placeholder="Year">
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-6 col-xs-offset-3 form-group">
<input class="form-control" type="number" id="km" name="km" placeholder="Km Driven">
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<br>
<input class="form-control" type="submit" id="vehicle_submit" name="vehicle_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
 <input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" />
 <input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" />
 <input type="hidden" id="type" name="type" value="<?php echo $type; ?>" />
 <input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" />
 </form>
</div>
</div>
</div></div>

<!-- add booking -->
<!-- Modal -->
<div class="modal fade" id="myModal_add_booking" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Add Booking(<?php echo $user_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="add_booking" class="form" method="post" action="add_booking.php">
<div class="row" align="center">
<div  id="veh_t" class="col-xs-6 col-xs-offset-3 form-group">

  <label class="bike">
  <input class="veh_b" id="veh_b" type="radio" name="veh_b" value="2w" checked/>
  <img id="veh_b" src="images/bike.png">
</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label class="car">
  <input class="veh_b" id="car" type="radio" name="veh_b" value="4w" />
  <img id="veh_b" src="images/car.png">
</label>
</div></div>

<div class="row"></div>

<div class="row">
<div id="b_m" class="col-xs-8 col-xs-offset-2 form-group">
<select class="form-control brandmodel" id="veh_no" name="veh_no" required>
<option selected value="">Vehicle</option>
 </select>
</div>
</div>

<div class="row"></div>

<div class="row">

<div class="col-xs-8 col-xs-offset-2 form-group" id="service">
<div class="ui-widget" id="serv">
        	<input class="form-control autocomplete servicetype" id="service_type" type="text" name="service_type" placeholder="Service Type" required>
  </div>
</div>
</div>

<div class="row"></div>

<div class="row">
<div class="col-xs-7 col-xs-offset-2 form-group" id="loc">
        <div class="ui-widget">
        	<input class="form-control autocomplete" id="location" type="text" name="location" placeholder="Start typing Location..." required>
        </div>
</div>
        <div><p class="btn btn-sm" id="go" style="margin-left:-8px;background-color:#B2EBF2;">Go</p></div>
</div> 

<div class="row"></div>

<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group" id="mec">

<select class="form-control  mechanic" id="mechanic" name="mechanic" required>
<option selected>Select Mechanic</option>
</select>
</div>
</div>


<div class="row"></div>
<div class="row">
<div class="col-xs-3 col-xs-offset-2 form-group">
<label> Service - Date</label></div>
<div class="col-xs-5  form-group">
<input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="service_date" name="service_date" required>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-3 col-xs-offset-2 form-group">
<label> Next Service Date</label></div>
<div class="col-xs-5  form-group">
<input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="next_service_date" name="next_service_date" required>
</div>
</div>
<div class="row"></div>

<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group" >
<textarea class="form-control" maxlength="100" id="description" name="description" placeholder="Description..." ></textarea>
</div>
</div>

<div class="row"></div>
<div class="row">
<div class="col-xs-4 col-xs-offset-2 form-group">
<label>PickUp</label>
  <input class="form-group" type="radio" id="pickup" name="pickup" value="1" >&nbsp;Yes&nbsp;</input>
  <input class="form-group" type="radio" id="pickup" name="pickup" value="0" checked>&nbsp;No</input>
</div>

<div class="col-xs-4  form-group">
<label>SMS</label>
<input class="form-group" type="radio" id="sms" name="sms" value="1" checked>&nbsp;Yes&nbsp;</input>
<input class="form-group" type="radio" id="sms" name="sms" value="0" >&nbsp;No</input></div>
</div>

<div class="row"></div>

<div class="row">
<div class="col-xs-2 col-xs-offset-2 form-group">
<label>Amount&nbsp;:&nbsp;&nbsp;</label>
</div>
<div class="col-xs-6 form-group">
<input class="form-control" type="number" id="amount" name="amount" placeholder="Amount">
</div>
</div>


<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<input class="form-control" type="submit" id="booking_submit" name="booking_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
</form>

</div>

</div>
</div>
</div>

<!-- edit booking -->
<!-- Modal -->
<div class="modal fade" id="myModal_edit_booking" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Edit Booking(<?php echo $booking_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="edit_booking" class="form" method="post" action="edit_booking.php">
  <?php
$sql_book = "SELECT vech_id,user_veh_id,vehicle_type,user_vech_no,service_type,mec_id,shop_name,service_description,service_date,pick_up,amt,followup_date,locality FROM user_booking_tb WHERE booking_id = '$booking_id'";
$res_book = mysqli_query($conn,$sql_book);
$row_book = mysqli_fetch_object($res_book);
$b_veh_id=$row_book->vech_id;
$b_user_veh_id = $row_book->user_veh_id;
$b_veh = $row_book->vehicle_type;
$b_veh_no = $row_book->user_vech_no;
$b_service_type = $row_book->service_type;
$b_mec_id = $row_book->mec_id;
$b_shop_name = $row_book->shop_name;
$b_desc = $row_book->service_description;
$b_service_date = $row_book->service_date;
$b_pickup = $row_book->pick_up;
$b_amount = $row_book->amt;
$next_service_date = $row_book->followup_date;
$b_locality = $row_book->locality;

// get brand model
$sql_model = "SELECT id,brand,model,reg_no FROM user_vehicle_table WHERE id='$b_veh_id'";
$res_model = mysqli_query($conn,$sql_model);
$row_model = mysqli_fetch_object($res_model);
$brand_id = $row_model->id;
$brandmodel = $row_model->brand." ".$row_model->model." ".$row_model->reg_no;

if($b_mec_id == "" || $b_mec_id==0){
  $shop_address = "";
}
else{
//get shop address
$sql_shop_loc = mysqli_query($conn,"SELECT address4 FROM admin_mechanic_table WHERE mec_id='$b_mec_id' ");
$row_shop_loc= mysqli_fetch_object($sql_shop_loc);
$shop_address = $row_shop_loc->address4;
}

    switch($b_service_type){
      case 'general_service' :
      if($b_veh =='2w'){
       $b_service_type = "General Service";
      }
      else{
       $b_service_type = "Car service and repair";
      }
      break;
      case 'break_down': $b_service_type = "Breakdown Assistance"; break;
      case 'tyre_puncher':$b_service_type = "Tyre Puncture";  break;
      case 'other_service':$b_service_type = "Repair Job"; break;
      case 'car_wash':
      if($b_veh == '2w'){
       $b_service_type = "water Wash";
      }
      else{
       $b_service_type = "Car wash exterior";
      }break;
      case 'engine_oil':$b_service_type = "Repair Job"; break;
      case 'free_service':$b_service_type = "General Service"; break;
      case 'car_wash_both':
      case 'completecarspa':$b_service_type = "Complete Car Spa"; break;
      case 'car_wash_ext':$b_service_type = "Car wash exterior"; break;
      case 'car_wash_int':$b_service_type = "Interior Detailing"; break;
      case 'Doorstep car spa':$b_service_type = "Doorstep Car Spa"; break;
      case 'Doorstep_car_wash':$b_service_type = "Doorstep Car Wash"; break;
      case 'free_diagnastic':
      case 'free_diagnostic':
      case 'diagnostics':
      if($b_veh == '2w'){
       $b_service_type = "Diagnostics/Check-up";
     }
     else{
      $b_service_type = "Vehicle Diagnostics";
     }
         break;
      case 'water_wash':$b_service_type = "water Wash"; break;
      case 'exteriorfoamwash':$b_service_type = "Car wash exterior"; break;
      case 'interiordetailing':$b_service_type = "Interior Detailing"; break;
      case 'rubbingpolish':$b_service_type = "Car Polish"; break;
      case 'underchassisrustcoating':$b_service_type = "Underchassis Rust Coating"; break;
      case 'headlamprestoration':$b_service_type = "Headlamp Restoration"; break;
      default:$b_service_type = $b_service_type;
      }

if($b_veh == "2w"){
  ?>
  <div class="row" align="center">
  <div class="col-xs-6 col-xs-offset-3 form-group">
  <label class="bike">
  <input id="veh_be" type="radio" name="veh_be" value="2w" checked/>
  <img id="veh_be" src="images/bike.png">
</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label class="car">
  <input id="car" type="radio" name="veh_be" value="4w" />
  <img id="veh_be" src="images/car.png">
</label>
</div></div>
<?php
}
else{
  ?>
<div class="row" align="center">
  <div class="col-xs-6 col-xs-offset-3 form-group">
  <label class="bike">
  <input id="veh_be" type="radio" name="veh_be" value="2w" />
  <img id="veh_be" src="images/bike.png">
</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label class="car">
  <input id="car" type="radio" name="veh_be" value="4w" checked/>
  <img id="veh_be" src="images/car.png">
</label>
</div></div>
<?php
}?>

<div class="row"></div>

<div class="row">
<div id="b_me" class="col-xs-8 col-xs-offset-2 form-group">
<select class="form-control" id="veh_noe" name="veh_noe">
<option selected value="<?php echo $brand_id; ?>"><?php echo $brandmodel; ?></option>
 </select>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group" id="service">
<div class="ui-widget">
        	<input class="form-control autocomplete" id="service_typee" type="text" name="service_typee" placeholder="Service Type" value="<?php echo $b_service_type; ?>" required>
</div>
</div>
</div>

<div class="row"></div>

<div class="row">
<div class="col-xs-7 col-xs-offset-2 form-group" id="loce">
        <div class="ui-widget">
        	<input class="form-control autocomplete" id="locatione" type="text" name="locatione" placeholder="Start typing Location..." value="<?php echo $b_locality; ?>" required>
        </div>
        
</div>
<div><p class="btn btn-sm" id="go" style="margin-left:-8px;background-color:#B2EBF2;">Go</p></div>
</div> 

<div class="row"></div>

<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group" id="mece">

<select class="form-control" id="mechanice" name="mechanice" required>
<?php if($b_shop_name == '' || $b_shop_name == '0'){ ?>
<option selected value="">Select Mechanic</option>
<?php } 
else{ ?>
<option selected value="<?php echo $b_mec_id; ?>"><?php echo $b_shop_name; ?></option>
<?php } ?>
</select>
</div>
</div>


<div class="row"></div>

<div class="row">
<div class="col-xs-3 col-xs-offset-2 form-group">
<label> Service - Date</label>
</div>
<div class="col-xs-5  form-group">
<input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="service_datee" name="service_datee" value="<?php echo date("d-m-Y",strtotime($b_service_date)); ?>" required>
</div>
</div>

<div class="row"></div>

<div class="row">
<div class="col-xs-3 col-xs-offset-2 form-group">
<label> Next Service Date</label>
</div>
<div class="col-xs-5  form-group">
<input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="next_service_datee" name="next_service_datee" value="<?php echo date("d-m-Y",strtotime($next_service_date)); ?>" required>
</div>
</div>
<div class="row"></div>

<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group" >
<textarea class="form-control" maxlength="100" id="descriptione" name="descriptione" placeholder="Description..." ><?php echo $b_desc; ?></textarea>
</div>
</div>

<div class="row"></div>

<div class="row">
<div class="col-xs-4 col-xs-offset-2 form-group">
<label>PickUp</label>
<?php
if($b_pickup == "yes"){
  ?>
  <input class="form-group" type="radio" id="pickupe" name="pickupe" value="1" checked>&nbsp;Yes&nbsp;</input>
  <input class="form-group" type="radio" id="pickupe" name="pickupe" value="0">&nbsp;No</input>
  <?php
}
else{
  ?>
  <input class="form-group" type="radio" id="pickupe" name="pickupe" value="1">&nbsp;Yes&nbsp;</input>
  <input class="form-group" type="radio" id="pickupe" name="pickupe" value="0" checked>&nbsp;No</input>
  <?php
} ?>
</div>

<div class="col-xs-4  form-group">
<label>SMS</label>
<input class="form-group" type="radio" id="smse" name="smse" value="1" checked>&nbsp;Yes&nbsp;</input>
<input class="form-group" type="radio" id="smse" name="smse" value="0" >&nbsp;No</input>
</div>
</div>

<div class="row"></div>

<div class="row">
<div class="col-xs-2 col-xs-offset-2 form-group">
<label>Amount&nbsp;:&nbsp;&nbsp;</label>
</div>
<div class="col-xs-6 form-group">
<input class="form-control" type="number" id="amounte" name="amounte" placeholder="Amount" value="<?php echo $b_amount; ?>">
</div>
</div>


<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<input class="form-control" type="submit" id="edit_booking_submit" name="edit_booking_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
</form>

</div>

</div>
</div>
</div>

<!-- add followup -->
<!-- Modal -->
<div class="modal fade" id="myModal_followup" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h3 class="modal-title">Add Follow Up (Id : <?php echo $user_id; ?>)</h3>
  </div>
  <div class="modal-body">
    <form id="add_followup" role="form" method="post" action="add_followup.php">
      <div class="row">
        <div class="col-xs-8 col-xs-offset-2 form-group">
          <br>
          <select class="form-control" id="followup_status" name="status" required>
          <option selected value="">Select Reason</option>
          <?php
          $sql="SELECT activity FROM admin_activity_tbl WHERE flag='0'";
          $query=mysqli_query($conn,$sql);

          while ($rows = mysqli_fetch_array($query)){ ?>
          <option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>
          <?php } ?>
          </select>
        </div>
      </div>
      <div class="row"></div>

      <div class="row">
        <div class="col-xs-8 col-xs-offset-2 form-group">
        <textarea class="form-control" maxlength="1000" id="comments" name="comments" style="min-height:100px;" placeholder="Comments..."></textarea>
        </div>
      </div>
      <div class="row"></div>
      <div class="row">
        <div class="col-xs-3 col-xs-offset-2 form-group">
        <label>&nbsp;FollowUp Date</label>
        </div>
<div class="col-xs-5">
<div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="follow_date"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
</div>
</div>
</div>

<div class="row">
<div class="col-xs-3 col-xs-offset-2 form-group">
 <label>&nbsp;Priority</label>
</div>
<div class="col-xs-5">
<div class="form-group">
 <input type="radio" name="priority" value="1" style="width:30px;">Low</input>
 <input type="radio" name="priority" value="2" style="width:30px;">Medium</input>
 <input type="radio" name="priority" value="3" style="width:30px;" checked>High</input>
</div>
</div>
</div>

      <div class="row"></div>
      <div class="row">
        <div class="col-xs-2 col-xs-offset-5 form-group">
          <br>
          <input class="form-control" type="submit" id="followup_submit" name="followup_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
        </div>
      </div>
      <input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
      <input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
      <input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
      <input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >

    </form>
  </div>
</div>
</div>
</div>

<!-- add RNR 1 -->
<!-- Modal -->
<div class="modal fade" id="myModal_rnr1" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Add RNR 1 (Id : <?php echo $user_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="add_rnr1" class="form" method="post" action="add_rnr1.php">
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<br>
<select class="form-control" id="rnr1_status" name="status" required>
<option selected value="">Select Reason</option>
<?php
$sql="SELECT activity FROM admin_activity_tbl WHERE flag='0'";
$query=mysqli_query($conn,$sql);

while ($rows = mysqli_fetch_array($query)){ ?>
<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>
<?php } ?>
</select>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<textarea class="form-control" maxlength="1000" id="comments" name="comments" style="min-height:100px;" placeholder="Comments..."></textarea>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-3 col-xs-offset-2 form-group">
<label>&nbsp;FollowUp Date</label>
</div>
<div class="col-xs-5  form-group">
<div class="form-group">
                <div class='input-group date' id='datetimepicker2'>
                    <input type='text' class="form-control" name="follow_date"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
</div></div>
</div>
<div class="row">
<div class="col-xs-3 col-xs-offset-2 form-group">
 <label>&nbsp;Priority</label>
</div>
<div class="col-xs-5">
<div class="form-group">
 <input type="radio" name="priority" value="1" style="width:30px;">Low</input>
 <input type="radio" name="priority" value="2" style="width:30px;">Medium</input>
 <input type="radio" name="priority" value="3" style="width:30px;" checked>High</input>
</div>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<br>
<input class="form-control" type="submit" id="rnr1_submit" name="rnr1_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >

</form>
</div>
</div>
</div>
</div>
<!-- add RNR 2 -->
<!-- Modal -->
<div class="modal fade" id="myModal_rnr2" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Add RNR 2 (Id : <?php echo $user_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="add_rnr2" class="form" method="post" action="add_rnr2.php">
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<br>
<select class="form-control" id="rnr2_status" name="status" required>
<option selected value="">Select Reason</option>
<?php
$sql="SELECT activity FROM admin_activity_tbl WHERE flag='0'";
$query=mysqli_query($conn,$sql);

while ($rows = mysqli_fetch_array($query)){ ?>
<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>
<?php } ?>
</select>
</div>
</div>
<div class="row"></div>

<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<textarea class="form-control" maxlength="1000" id="comments" name="comments" style="min-height:100px;" placeholder="Comments..."></textarea>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-3 col-xs-offset-2 form-group">
<label>&nbsp;FollowUp Date</label></div>
<div class="col-xs-5  form-group">
<div class="form-group">
                <div class='input-group date' id='datetimepicker3'>
                    <input type='text' class="form-control" name="follow_date"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
</div></div>
</div>
<div class="row">
<div class="col-xs-3 col-xs-offset-2 form-group">
 <label>&nbsp;Priority</label>
</div>
<div class="col-xs-5">
<div class="form-group">
 <input type="radio" name="priority" value="1" style="width:30px;">Low</input>
 <input type="radio" name="priority" value="2" style="width:30px;">Medium</input>
 <input type="radio" name="priority" value="3" style="width:30px;" checked>High</input>
</div>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<br>
<input class="form-control" type="submit" id="rnr2_submit" name="rnr2_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
</form>
</div>
</div>
</div>
</div>
<!-- move to  others -->
<!-- Modal -->
<div class="modal fade" id="myModal_others" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Move to Others (Id : <?php echo $user_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="add_others" class="form" method="post" action="add_others.php">
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<br>
<select class="form-control" id="others_status" name="status" required>
<option selected value="">Select Reason</option>
<?php
$sql="SELECT activity FROM admin_activity_tbl WHERE flag='2'";
$query=mysqli_query($conn,$sql);

while ($rows = mysqli_fetch_array($query)){ ?>
<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>
<?php } ?>
</select>
</div>
</div>
<div class="row"></div>

<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<textarea class="form-control" maxlength="1000" id="comments" name="comments" style="min-height:100px;" placeholder="Comments..."></textarea>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<br>
<input class="form-control" type="submit" id="others_submit" name="others_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
</form>
</div>
</div>
</div>
</div>
<!-- add RNR 1 -->
<!-- Modal -->
<div class="modal fade" id="myModal_reschedule" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Reschedule (Id : <?php echo $booking_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="reschedule_booking" class="form" method="post" action="reschedule_booking.php">
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<br>
<select class="form-control" id="reschedule_status" name="status" required>
<option selected value="">Select Reason</option>
<?php
$sql="SELECT activity FROM admin_activity_tbl WHERE flag='4'";
$query=mysqli_query($conn,$sql);

while ($rows = mysqli_fetch_array($query)){ ?>
<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>
<?php } ?>
</select>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<textarea class="form-control" maxlength="1000" id="comments" name="comments" style="min-height:100px;" placeholder="Comments..."></textarea>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-3 col-xs-offset-2 form-group">
<label>&nbsp;Rechedule To</label>
</div>
<div class="col-xs-5  form-group">
<div class="form-group">
                <div class='input-group date' id='datetimepicker4'>
                    <input type='text' class="form-control" name="reschedule_to"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
</div></div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<br>
<input class="form-control" type="submit" id="reschedule_submit" name="reschedule_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >

</form>
</div>
</div>
</div>
</div>

<!-- Cancel Bookings -->
<!-- Modal -->
<div class="modal fade" id="myModal_cancel_booking" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Cancel Booking (Id : <?php echo $booking_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="cancel_booking" class="form" method="post" action="cancel_booking.php">
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<br>
<select class="form-control" id="cancel_status" name="status" required>
<option selected value="">Select Reason</option>
<?php
$sql="SELECT activity FROM admin_activity_tbl WHERE flag='1'";
$query=mysqli_query($conn,$sql);

while ($rows = mysqli_fetch_array($query)){ ?>
<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>
<?php } ?>
</select>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<textarea class="form-control" maxlength="1000" id="cancel_comments" name="comments" style="min-height:100px;" placeholder="Comments..." required></textarea>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<br>
<input class="form-control" type="submit" id="cancel_submit" name="cancel_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
</form>
</div>
</div>
</div>
</div>

<!-- brand and model -->
 <script>
$(function() {
		    $( "#BrandModelid" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_brandmodel.php",
                data: {
                    term: request.term,
					extraParams:$('#veh:checked').val()
                },
                dataType: 'json'
            }).done(function(data) {
				//console.log(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#BrandModelid").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "9999999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script>
<!-- user location -->
<script>
$("#location_home").click(function(){
var c = $('#city').val();
  console.log(c);
  if(c==''){
    alert("Plese select City to get localityies!");
  }
  else{
$(function() {

			    $( "#location_home" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_user_locality.php",
                data: {
                    term: request.term,
          					city:$('#city').val()
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location_home").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

  }
});

</script>
<!-- locality work -->
<script>
$("#location_work").click(function(){
var c = $('#city').val();
  console.log(c);
  if(c==''){
    alert("Plese select City to get localityies!");
  }
  else{
$(function() {
			    $( "#location_work" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_work_locality.php",
                data: {
                    term: request.term,
          					city:$('#city').val()
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location_work").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});
  }
});
</script>

<!-- reg number -->
<!-- masking -->
<script type="text/javascript" src="js/inputmask.js"></script>
<script type="text/javascript" src="js/jquery.inputmask.js"></script>
  <script type="text/javascript" src="js/inputmask.extensions.js"></script>

<script>
Inputmask("A{2}/9{2}/A{2}/9{4}").mask($("#regno"));
</script>

<!-- select Vehicle Number  -->
<script>
$(document).ready(function($) {
 $(document).on('change', '[name="veh_b"]' , function(){
  	var selectvalue = $('[name="veh_b"]:checked').val();
	var selectuser = <?php echo $user_id; ?>;
	$("#veh_no").empty();
		 $("div.veh").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_veh.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selectvalue": selectvalue , "selectuser" : selectuser},
            success : function(data) {
				// alert(data);
                $('#veh_no').append(data);
                 $("#location").autocomplete('close').val('');
                 $("#service_type").autocomplete('close').val('');
                  $("#mechanic").empty();
                  $("#mechanic").append('<option value="">Select Mechanic</option>');

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

    });
});
</script>
<!-- select vehicle on page load -->
<script>
$(document).ready(function($) {
  	var selectvalue = $('[name="veh_b"]:checked').val();
	var selectuser = <?php echo $user_id; ?>;
  var veh_no = $('#veh_no').val();
	$("#veh_no").empty();
		 $("div.veh").show();
      // $("#location").autocomplete('close').val('');
      // $("#mechanic").empty();
      // $("#mechanic").append('<option value="">Select Mechanic</option>');
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_veh.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selectvalue": selectvalue , "selectuser" : selectuser, "veh_no": veh_no},
            success : function(data) {
				// alert(data);
                $('#veh_no').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

});
</script>

<!-- select Service type -->
<!-- select Service type -->
 <script>
$(function() {
			    $( "#service_type" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_service_type.php",
                data: {
                    term: request.term,
					//type : $('#veh_b:checked').val()
						type : $('#veh_b:checked').val(),
					vtype: $('#veh_no').val(),
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#service_type").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script>

<!-- select Mechanic - ->
<script>
$(document).ready(function($) {
 $(document).on('change', '[name="veh_b"]' , function(){
  	var selecttype = $('[name="veh_b"]:checked').val();
  //	var selectservice = $("#service_type").val();
//	var selecttype = $("#veh_b").val();
	$("#mechanic").empty();
		 $("div.mec").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_mechanic.php",  // create a new php page to handle ajax request
            type : "POST",
          //  data : {"selectservice": selectservice , "selecttype": selecttype},
            data : { "selecttype": selecttype },
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanic').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

    });
});
</script>-->
<!-- select Mechanic Based On Location -->
<script>
$(document).ready(function($) {
 $("#location").on("autocompletechange", function(){
  //	var selectservice = $("#service_type").val();
	var selecttype = $('[name="veh_b"]:checked').val();
	var selectloc = $("#location").val();
  var veh_no = $("#veh_no").val();
	$("#mechanic").empty();
		 $("div.mec").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_mechanic_loc.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc,"veh_no": veh_no },
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanic').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

    });
});
</script>
<!-- select Mechanic Based On Location on page load - ->
<script>
$(document).ready(function($) {
  //	var selectservice = $("#service_type").val();
	var selecttype = $('[name="veh_b"]:checked').val();
	var selectloc = $("#location").val();
	$("#mechanic").empty();
		 $("div.mec").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_mechanic_loc.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc },
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanic').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }
   });
});
</script>-->

<!-- select Mechanic - ->
<script>
$(document).ready(function($) {
  	var selecttype = $('[name="veh_b"]:checked').val();
  //	var selectservice = $("#service_type").val();
//	var selecttype = $("#veh_b").val();
	$("#mechanic").empty();
		 $("div.mec").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_mechanic.php",  // create a new php page to handle ajax request
            type : "POST",
          //  data : {"selectservice": selectservice , "selecttype": selecttype},
            data : { "selecttype": selecttype },
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanic').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

});
</script> -->

<!-- --------------------------------------- edit booking ------------------------------------------------------ -->
<!-- select Vehicle Number  -->
<script>
$(document).ready(function($) {
 $(document).on('change', '[name="veh_be"]' , function(){
  	var selectvalue = $('[name="veh_be"]:checked').val();
	var selectuser = <?php echo $user_id; ?>;
	$("#veh_noe").empty();
  
		 $("div.veh").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_veh.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selectvalue": selectvalue , "selectuser" : selectuser},
            success : function(data) {
				// alert(data);
                $('#veh_noe').append(data);
                $("#locatione").autocomplete('close').val('');
                $("#service_typee").autocomplete('close').val('');
                $("#mechanice").empty();
                $("#mechanice").append('<option value="">Select Mechanic</option>');

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

    });
});
</script>
<!-- select vehicle on page loaad -->
<script>
$(document).ready(function($) {
  	var selectvalue = $('[name="veh_be"]:checked').val();
	var selectuser = <?php echo $user_id; ?>;
	$("#veh_noe").empty();
		 $("div.veh").show();
      //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_veh.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selectvalue": selectvalue , "selectuser" : selectuser},
            success : function(data) {
				// alert(data);
                $('#veh_noe').append(data);
               // $("#locatione").autocomplete('close').val('');
               // $("#mechanice").empty();
               // $("#mechanice").append('<option valu="">Select Mechanic</option>');

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

});
</script>

<!-- select Service type -->
 <script>
$(function() {
			    $( "#service_typee" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_service_type.php",
                data: {
                    term: request.term,
					//type : $('#veh_b:checked').val()
						type : $('#veh_be:checked').val(),
					vtype: $('#veh_noe').val(),
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#service_typee").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script>
<!-- select Location of service center -->
 <script>
$(function() {
			    $( "#location" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_location.php",
                data: {
                    term: request.term,
					//type : $('#veh_b:checked').val()
					vtype: $('[name="veh_b"]:checked').val(),
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script> 

<!-- edit booking select Location of service center -->
 <script>
$(function() {
			    $( "#locatione" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_location.php",
                data: {
                    term: request.term,
					//type : $('#veh_b:checked').val()
					vtype: $('[name="veh_be"]:checked').val(),
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#locatione").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script> 

<!-- select Mechanic Based On Location -->
<script>
$(document).ready(function($) {
 $("#locatione").on("autocompletechange", function(){
  	//var selectservice = $("#service_typee").val();
	var selecttype = $('[name="veh_be"]:checked').val();
	var selectloc = $("#locatione").val();
  var veh_no = $("#veh_noe").val();
	$("#mechanice").empty();

		 $("div.mece").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_mechanic_loc.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc ,"veh_no": veh_no},
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanice').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

    });
});
</script>
<!-- select Mechanic Based On Location  on page load-->
<script>
$(document).ready(function($) {
 //$("#locatione").on("autocompletechange", function(){
  	//var selectservice = $("#service_typee").val();
	var selecttype = $('[name="veh_be"]:checked').val();
	var selectloc = $("#locatione").val();
  var veh_no = $("#veh_noe").val();
  if(selectloc != ""){
    $("#mechanice").empty();

		 $("div.mece").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_mechanic_loc.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc,"veh_no": veh_no },
            success : function(data) {
				console.log(data);
				//alert(data);
                $('#mechanice').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });
  }
});
</script>
<!-- select Mechanic Based On Location  on page load- ->
<script>
$(document).ready(function($) {
	var selecttype = $('[name="veh_be"]:checked').val();
	var selectloc = $("#locatione").val();
	$("#mechanice").empty();
		 $("div.mece").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_mechanic_loc.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc },
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanice').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }
    });
});
</script>-->

<!-- select Mechanic - ->
<script>
$(document).ready(function($) {
 $(document).on('change', '[name="veh_be"]' , function(){
  	var selecttype = $('[name="veh_be"]:checked').val();
  //	var selectservice = $("#service_type").val();
//	var selecttype = $("#veh_b").val();
	$("#mechanice").empty();
		 $("div.mece").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_mechanic.php",  // create a new php page to handle ajax request
            type : "POST",
          //  data : {"selectservice": selectservice , "selecttype": selecttype},
            data : { "selecttype": selecttype },
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanice').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

    });
});
</script>-->
<!-- select Mechanic - ->
<script>
$(document).ready(function($) {
  	var selecttype = $('[name="veh_be"]:checked').val();
  //	var selectservice = $("#service_type").val();
//	var selecttype = $("#veh_b").val();
	$("#mechanice").empty();
		 $("div.mece").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_mechanic.php",  // create a new php page to handle ajax request
            type : "POST",
          //  data : {"selectservice": selectservice , "selecttype": selecttype},
            data : { "selecttype": selecttype },
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanice').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

});
</script>-->
<!-- --------------------------------------- edit booking ----------------------------------------------- ->
<!-- validation -->
<script>
var userinput = document.getElementById('user_name');
userinput.oninvalid = function(event) {
	  document.getElementById("user_name").style.borderColor="#E42649";
    event.target.setCustomValidity('Only aplhabets and digits are allowed!');
}
</script>
<script>
 var date = new Date();
date.setDate(date.getDate());

$('.datepicker').datepicker({
   autoclose: true,
    startDate: date
});
if($('input.datepicker').val()){
    $('input.datepicker').datepicker('setDate', 'today');
 }
 </script>
<!-- date time picker -->
<script type="text/javascript">
    $(function () {
        var dateNow = new Date();
        $('#datetimepicker1').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            defaultDate:dateNow
        });
         $('#datetimepicker2').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            defaultDate:dateNow
        });
         $('#datetimepicker3').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            defaultDate:dateNow
        });
        $('#datetimepicker4').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            defaultDate:dateNow
        });
    });

</script>
 <!-- select veh before fetching models -->
 <script>
 $(document).ready(function(){
   $("#BrandModelid").click(function(){
     var veh = $('input[name=veh]:checked').val();
     //console.log(veh);
     if(veh == null){
       //console.log(veh);
       alert("Please select vehicle type to get vehicle models!");
     }
   });
 })
 </script>
 <script>
 $(document).ready(function(){
$('input[name=veh]').change(function(){
  $("#BrandModelid").val("");
});
 });
 </script>
 <!-- disable buttons on submit -->
 <script>
 $(document).ready(function(){
  $("#add_followup").submit(function(e){
    $('#followup_submit').attr('disabled','disabled');
    var option = document.getElementById('followup_status');
    option.oninvalid = function(event) {
        document.getElementById("followup_status").style.borderColor="#E42649";
        event.target.setCustomValidity('Please select a reason!');
        $('#followup_submit').removeAttr('disabled'); 
    }
  });
 });
 </script>
 <script>
 $(document).ready(function(){
  $("#add_rnr1").submit(function(e){
    $('#rnr1_submit').attr('disabled','disabled');
    var option = document.getElementById('rnr1_status');
    option.oninvalid = function(event) {
        document.getElementById("rnr1_status").style.borderColor="#E42649";
        event.target.setCustomValidity('Please select a reason!');
        $('#rnr1_submit').removeAttr('disabled'); 
    }
  });
 });
 </script>
  <script>
 $(document).ready(function(){
  $("#add_rnr2").submit(function(e){
    $('#rnr2_submit').attr('disabled','disabled');
    var option = document.getElementById('rnr2_status');
    option.oninvalid = function(event) {
        document.getElementById("rnr2_status").style.borderColor="#E42649";
        event.target.setCustomValidity('Please select a reason!');
        $('#rnr2_submit').removeAttr('disabled'); 
    }
  });
 });
 </script>
  <script>
 $(document).ready(function(){
  $("#add_others").submit(function(e){
    $('#others_submit').attr('disabled','disabled');
    var option = document.getElementById('others_status');
    userinput.oninvalid = function(event) {
        document.getElementById("others_status").style.borderColor="#E42649";
        event.target.setCustomValidity('Please select a reason!');
        $('#others_submit').removeAttr('disabled'); 
    }
  });
 });
 </script>
  <script>
 $(document).ready(function(){
  $("#cancel_booking").submit(function(e){
    $('#cancel_submit').attr('disabled','disabled');
    var option = document.getElementById('cancel_status');
    option.oninvalid = function(event) {
        document.getElementById("cancel_status").style.borderColor="#E42649";
        event.target.setCustomValidity('Please select a reason!');
        $('#cancel_submit').removeAttr('disabled'); 
    }
    var comt = document.getElementById('cancel_comments');
    comt.oninvalid = function(event) {
        document.getElementById("cancel_comments").style.borderColor="#E42649";
        event.target.setCustomValidity('Please explain the reason to cancel!');
        $('#cancel_submit').removeAttr('disabled'); 
    }
  });
 });
 </script>
 <!-- check if locality is empty -->
<script>
$(document).ready(function($) {
  $("#mechanic").click(function(){
    if($("#location").val()== ''){
      $("#mechanic").empty();
      $("#mechanic").append('<option value="">Select Mechanic</option>');
      alert("Please select a Location to get mechanics!!!");
    }
  });
});
</script>
<script>
$(document).ready(function($) {
  $("#mechanice").click(function(){
    if($("#locatione").val()== ''){
      $("#mechanice").empty();
      $("#mechanice").append('<option value="">Select Mechanic</option>');
      alert("Please select a Location to get mechanics!!!");
    }
  });
});
</script>
<script>
$(document).ready(function($) {
  $("#go").click(function(){
    if($("#location").val()== ''){
      alert("Oops no location has been selected!!!");
    }
  });
});
</script>


</body>
</html>

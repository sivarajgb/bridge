<?php
ini_set("precision", 3);
ini_set('max_execution_time', -1);
include("../config.php");
$conn = db_connect1();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$super_flag = $_SESSION['super_flag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$starttime = date('H:i:s',strtotime($_POST['starttime']));
$endtime =  date('H:i:s',strtotime($_POST['endtime']));
$bservice_type =  $_POST['bservice'];
$bvehicle =  $_POST['vehicle'];
$bmaster_service =  $_POST['master_service'];
$crm_person =  $_POST['person'];
$bsource =  $_POST['bsource'];
$city = $_POST['city'];
$target = $_POST['target'];


$_SESSION['crm_city'] = $city; 


$start = $startdate.' '.$starttime;
$end = $enddate.' '.$endtime;
?>
<?php

switch($target)
{
	case "#Service" : 
					$sql_service_all = "SELECT master_service,type FROM go_axle_service_price_tbl ORDER BY master_service ASC";
					$res_service_all = mysqli_query($conn,$sql_service_all) or die(mysqli_error($conn));
					while($row_service_all = mysqli_fetch_object($res_service_all)){
						$master_service = $row_service_all->master_service;
						$type = $row_service_all->type;

						$master_service_arr['all'][$master_service]['no_lsr_all'] = $master_service_arr['all'][$master_service]['no_bsr_all'] = $master_service_arr['all'][$master_service]['no_fsr_all'] = $master_service_arr['all'][$master_service]['no_csr_all'] = $master_service_arr['all'][$master_service]['no_osr_all'] = $master_service_arr['all'][$master_service]['no_isr_all'] = $master_service_arr['all'][$master_service]['no_dsr_all'] = $master_service_arr['all'][$master_service]['no_env_all'] = 0;

						$master_service_arr[$type][$master_service]['no_lsr_all'] = $master_service_arr[$type][$master_service]['no_bsr_all'] = $master_service_arr[$type][$master_service]['no_fsr_all'] = $master_service_arr[$type][$master_service]['no_csr_all'] = $master_service_arr[$type][$master_service]['no_osr_all'] = $master_service_arr[$type][$master_service]['no_isr_all'] = $master_service_arr[$type][$master_service]['no_dsr_all'] = $master_service_arr[$type][$master_service]['no_env_all'] = 0;
					}
					$no_lsr_all_total['all'] = 0;$no_lsr_all_total['2w'] = 0;$no_lsr_all_total['4w'] = 0;
					$no_bsr_all_total['all'] = 0;$no_bsr_all_total['2w'] = 0;$no_bsr_all_total['4w'] = 0;
					$no_fsr_all_total['all'] = 0;$no_fsr_all_total['2w'] = 0;$no_fsr_all_total['4w'] = 0;
					$no_csr_all_total['all'] = 0;$no_csr_all_total['2w'] = 0;$no_csr_all_total['4w'] = 0;
					$no_osr_all_total['all'] = 0;$no_osr_all_total['2w'] = 0;$no_osr_all_total['4w'] = 0;
					$no_isr_all_total['all'] = 0;$no_isr_all_total['2w'] = 0;$no_isr_all_total['4w'] = 0;
					$no_dsr_all_total['all'] = 0;$no_dsr_all_total['2w'] = 0;$no_dsr_all_total['4w'] = 0;
					$no_env_all_total['all'] = 0;$no_env_all_total['2w'] = 0;$no_env_all_total['4w'] = 0;
					$services_all = '';
					$sql_get_services_all = "SELECT service_type FROM go_axle_service_price_tbl";
					$res_get_services_all = mysqli_query($conn,$sql_get_services_all);
					while($row_get_services_all = mysqli_fetch_array($res_get_type_services_all)){
						if($services_all == ""){
							$services_all = "'".$row_get_services_all['service_type']."'";
						}
						else{
							$services_all = $services_all.",'".$row_get_services_all['service_type']."'";
						}
					}           						
					//Service counts
					//all
					$no_lsr_all = 0;$no_bsr_all = 0;$no_fsr_all = 0;$no_csr_all = 0;$no_osr_all = 0;$no_isr_all = 0;$no_dsr_all = 0;
					
					$cond_s_all ='';
					$cond_s_all = $cond_s_all.($city == 'all' ? "" : "AND b.city='$city'");
					$cond_s_all = $cond_s_all.($crm_person == 'all' ? "" : "AND b.crm_update_id='$crm_person'");
					$cond_s_all = $cond_s_all.($bservice_type == 'all' ? "" : "AND b.service_type='$bservice_type'");
					$cond_s_all = $cond_s_all.($bsource == 'all' ? "" : "AND b.source='$bsource'");
					$cond_s_all = $cond_s_all.($bvehicle == 'all' ? "" : "AND b.vehicle_type='$bvehicle'");
					$cond_s_all = $cond_s_all.($bmaster_service == 'all' ? "" : "AND gs.master_service IN ('$bmaster_service')");
					

					$sql_sr_all = $city == "Chennai" ? "SELECT distinct b.booking_id,b.vehicle_type,gs.type,b.service_type,gs.master_service,b.flag,b.booking_status,b.flag_fo,b.log,b.activity_status,b.flag_unwntd,bb.b2b_vehicle_at_garage,b.service_status FROM go_bumpr.user_booking_tb b left join go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type left join b2b.b2b_booking_tbl bb on b.booking_id=bb.gb_booking_id WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.log BETWEEN '$start' AND '$end' {$cond_s_all} and (ISNULL(b2b_swap_flag) or b2b_swap_flag!='1') group by b.booking_id": "SELECT distinct b.booking_id,b.vehicle_type,gs.type,b.service_type,gs.master_service,b.flag,b.booking_status,b.flag_fo,b.log,b.activity_status,b.flag_unwntd,bb.b2b_vehicle_at_garage,b.service_status FROM go_bumpr.user_booking_tb b left join go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type left join b2b.b2b_booking_tbl bb on b.booking_id=bb.gb_booking_id where b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.source != 'Sulekha Booking'  AND b.log BETWEEN '$start' AND '$end' {$cond_s_all} and (ISNULL(b2b_swap_flag) or b2b_swap_flag!='1') group by b.booking_id";
					//echo $sql_sr_all;
					$res_sr_all = mysqli_query($conn,$sql_sr_all);
					
					//$res_sr_all = mysqli_query($conn,$sql_sr_all);

					while($row_sr_all = mysqli_fetch_object($res_sr_all)){
						$master_service = $row_sr_all->master_service;
						$type = $row_sr_all->vehicle_type;
						$booking_flag_sra = $row_sr_all->flag;
						$status_sra = $row_sr_all->booking_status;
						$flag_fo_sra = $row_sr_all->flag_fo;
						$flag_unwntd_sra = $row_sr_all->flag_unwntd;
						$activity_status_sra = $row_sr_all->activity_status;
						$vehicle_at_garage=$row_sr_all->b2b_vehicle_at_garage;
						$service_status=$row_sr_all->service_status;

						$master_service_arr['all'][$master_service]['type'] = $type;
						if($flag_unwntd_sra == '1'){
							if($booking_flag_sra == '1' && $activity_status_sra == '26'){
								$master_service_arr['all'][$master_service]['no_dsr_all']=$master_service_arr['all'][$master_service]['no_dsr_all']+1;
								$master_service_arr[$type][$master_service]['no_dsr_all']=$master_service_arr[$type][$master_service]['no_dsr_all']+1;
								$no_dsr_all_total['all']=$no_dsr_all_total['all']+1;
								$no_dsr_all_total[$type]=$no_dsr_all_total[$type]+1;
							}
						}
						else{
							$master_service_arr['all'][$master_service]['no_lsr_all']=$master_service_arr['all'][$master_service]['no_lsr_all']+1;
							$master_service_arr[$type][$master_service]['no_lsr_all']=$master_service_arr[$type][$master_service]['no_lsr_all']+1;
							$no_lsr_all_total['all']=$no_lsr_all_total['all']+1;
							$no_lsr_all_total[$type]=$no_lsr_all_total[$type]+1;
							switch($status_sra){
								case '1':if($booking_flag_sra == '1'){
											$master_service_arr['all'][$master_service]['no_csr_all']=$master_service_arr['all'][$master_service]['no_csr_all']+1;
											$master_service_arr[$type][$master_service]['no_csr_all']=$master_service_arr[$type][$master_service]['no_csr_all']+1;
											$no_csr_all_total['all']=$no_csr_all_total['all']+1;
											$no_csr_all_total[$type]=$no_csr_all_total[$type]+1;
										}
										else{
											if($flag_fo_sra == '1'){
												$master_service_arr['all'][$master_service]['no_fsr_all']=$master_service_arr['all'][$master_service]['no_fsr_all']+1;
												$master_service_arr[$type][$master_service]['no_fsr_all']=$master_service_arr[$type][$master_service]['no_fsr_all']+1;
												$no_fsr_all_total['all']=$no_fsr_all_total['all']+1;
												$no_fsr_all_total[$type]=$no_fsr_all_total[$type]+1;
											}
											else{
												$master_service_arr['all'][$master_service]['no_isr_all']=$master_service_arr['all'][$master_service]['no_isr_all']+1;
												$master_service_arr[$type][$master_service]['no_isr_all']=$master_service_arr[$type][$master_service]['no_isr_all']+1;
												$no_isr_all_total['all']=$no_isr_all_total['all']+1;
												$no_isr_all_total[$type]=$no_isr_all_total[$type]+1;
											}
										}
										break;
								case '2':if($booking_flag_sra == '1'){
											$master_service_arr['all'][$master_service]['no_csr_all']=$master_service_arr['all'][$master_service]['no_csr_all']+1;
											$master_service_arr[$type][$master_service]['no_csr_all']=$master_service_arr[$type][$master_service]['no_csr_all']+1;
											$no_csr_all_total['all']= $no_csr_all_total['all']+1;
											$no_csr_all_total[$type]= $no_csr_all_total[$type]+1;
										}
										else{
											$master_service_arr['all'][$master_service]['no_bsr_all']=$master_service_arr['all'][$master_service]['no_bsr_all']+1;
											$master_service_arr[$type][$master_service]['no_bsr_all']=$master_service_arr[$type][$master_service]['no_bsr_all']+1;
											$no_bsr_all_total['all']=$no_bsr_all_total['all']+1;
											$no_bsr_all_total[$type]=$no_bsr_all_total[$type]+1;

											if(($vehicle_at_garage=='1') || ($service_status=="Completed") || ($service_status=="In Progress")){
												$master_service_arr['all'][$master_service]['no_env_all']=$master_service_arr['all'][$master_service]['no_env_all']+1;
												$master_service_arr[$type][$master_service]['no_env_all']=$master_service_arr[$type][$master_service]['no_env_all']+1;
												$no_env_all_total['all']=$no_env_all_total['all']+1;
												$no_env_all_total[$type]=$no_env_all_total[$type]+1;
											}
										}
										break;
								case '3':
								case '4':
								case '5':
								case '6':if($booking_flag_sra != '1'){
											$master_service_arr['all'][$master_service]['no_fsr_all']=$master_service_arr['all'][$master_service]['no_fsr_all']+1;
											$master_service_arr[$type][$master_service]['no_fsr_all']=$master_service_arr[$type][$master_service]['no_fsr_all']+1;
											$no_fsr_all_total['all']=$no_fsr_all_total['all']+1;
											$no_fsr_all_total[$type]=$no_fsr_all_total[$type]+1;
										}
										else{
											$master_service_arr['all'][$master_service]['no_csr_all']=$master_service_arr['all'][$master_service]['no_csr_all']+1;
											$master_service_arr[$type][$master_service]['no_csr_all']=$master_service_arr[$type][$master_service]['no_csr_all']+1;
											$no_csr_all_total['all']= $no_csr_all_total['all']+1;
											$no_csr_all_total[$type]= $no_csr_all_total[$type]+1;
										}
										break;
								case '0':if($booking_flag_sra != '1'){
											$master_service_arr['all'][$master_service]['no_osr_all']=$master_service_arr['all'][$master_service]['no_osr_all']+1;
											$master_service_arr[$type][$master_service]['no_osr_all']=$master_service_arr[$type][$master_service]['no_osr_all']+1;
											$no_osr_all_total['all']=$no_osr_all_total['all']+1;
											$no_osr_all_total[$type]=$no_osr_all_total[$type]+1;
										}
										else{
											$master_service_arr['all'][$master_service]['no_csr_all']=$master_service_arr['all'][$master_service]['no_csr_all']+1;
											$master_service_arr[$type][$master_service]['no_csr_all']=$master_service_arr[$type][$master_service]['no_csr_all']+1;
											$no_csr_all_total['all']=$no_csr_all_total['all']+1;
											$no_csr_all_total[$type]=$no_csr_all_total[$type]+1;
										}
										break;
							}
						}
						$master_service_arr['all']['total']['no_lsr_all_total'] = $no_lsr_all_total['all'];							
						$master_service_arr['all']['total']['no_bsr_all_total'] = $no_bsr_all_total['all'];							
						$master_service_arr['all']['total']['no_fsr_all_total'] = $no_fsr_all_total['all'];							
						$master_service_arr['all']['total']['no_csr_all_total'] = $no_csr_all_total['all'];							
						$master_service_arr['all']['total']['no_osr_all_total'] = $no_osr_all_total['all'];							
						$master_service_arr['all']['total']['no_isr_all_total'] = $no_isr_all_total['all'];							
						$master_service_arr['all']['total']['no_dsr_all_total'] = $no_dsr_all_total['all'];
						$master_service_arr['all']['total']['no_env_all_total'] = $no_env_all_total['all'];							
						
						$master_service_arr[$type]['total']['no_lsr_all_total'] = $no_lsr_all_total[$type];							
						$master_service_arr[$type]['total']['no_bsr_all_total'] = $no_bsr_all_total[$type];							
						$master_service_arr[$type]['total']['no_fsr_all_total'] = $no_fsr_all_total[$type];							
						$master_service_arr[$type]['total']['no_csr_all_total'] = $no_csr_all_total[$type];							
						$master_service_arr[$type]['total']['no_osr_all_total'] = $no_osr_all_total[$type];							
						$master_service_arr[$type]['total']['no_isr_all_total'] = $no_isr_all_total[$type];							
						$master_service_arr[$type]['total']['no_dsr_all_total'] = $no_dsr_all_total[$type];
						$master_service_arr[$type]['total']['no_env_all_total'] = $no_env_all_total[$type];							
					}
					
					$no_lsr_all_total=0;$no_bsr_all_total=0;$no_fsr_all_total=0;$no_csr_all_total=0;$no_osr_all_total=0;$no_isr_all_total=0;$no_dsr_all_total=0;$no_env_all_total=0;
					foreach(array_keys($master_service_arr['all']) as $key=>$master_service_all)
					{
						$type = $master_service_arr['all'][$master_service_all]['type'];
						$no_lsr_all = $master_service_arr['all'][$master_service_all]['no_lsr_all'];
						$no_bsr_all = $master_service_arr['all'][$master_service_all]['no_bsr_all'];
						$no_fsr_all = $master_service_arr['all'][$master_service_all]['no_fsr_all'];
						$no_csr_all = $master_service_arr['all'][$master_service_all]['no_csr_all'];
						$no_osr_all = $master_service_arr['all'][$master_service_all]['no_osr_all'];
						$no_isr_all = $master_service_arr['all'][$master_service_all]['no_isr_all'];
						$no_dsr_all = $master_service_arr['all'][$master_service_all]['no_dsr_all'];
						$no_env_all = $master_service_arr['all'][$master_service_all]['no_env_all'];
						if($master_service_all == 'total')
						{
							$no_lsr_all_total = $master_service_arr['all']['total']['no_lsr_all_total'];
							$no_bsr_all_total = $master_service_arr['all']['total']['no_bsr_all_total'];
							$no_fsr_all_total = $master_service_arr['all']['total']['no_fsr_all_total'];
							$no_csr_all_total = $master_service_arr['all']['total']['no_csr_all_total'];
							$no_osr_all_total = $master_service_arr['all']['total']['no_osr_all_total'];
							$no_isr_all_total = $master_service_arr['all']['total']['no_isr_all_total'];
							$no_dsr_all_total = $master_service_arr['all']['total']['no_dsr_all_total'];
							$no_env_all_total = $master_service_arr['all']['total']['no_env_all_total'];
						}
						$tr1 = '<tr>';
						if($no_lsr_all == 0){ 
							continue; 
						}
						else{ 
						$td1 = "<td>".$master_service_all."</td>";
						$td2 = "<td style='text-align:center;font-size:13px;'>".$type."</td>";
						$td3 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$type."' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalservice'>".$no_lsr_all."</button></td>";
						$td4 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$type."' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalservice'>".$no_bsr_all."(".number_format((($no_bsr_all/$no_lsr_all)*100),2)."%)</button></td>";
						$td10 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$type."' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='envconv' data-target='#myModalservice'>".$no_env_all."(".number_format((($no_env_all/$no_bsr_all)*100),2)."%)</button></td>";
						$td5 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$type."' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalservice'>".$no_fsr_all."(".number_format((($no_fsr_all/$no_lsr_all)*100),2)."%)</button></td>";
						$td6 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$type."' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalservice'>".$no_csr_all."(".number_format((($no_csr_all/$no_lsr_all)*100),2)."%)</button></td>";
						$td7 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$type."' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalservice'>".$no_osr_all."(".number_format((($no_osr_all/$no_lsr_all)*100),2)."%)</button></td>";
						$td8 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$type."' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalservice'>".$no_isr_all."(".number_format((($no_isr_all/$no_lsr_all)*100),2)."%)</button></td>";
						$td9 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$type."' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalservice'>".$no_dsr_all."</button></td>";
						}
						$tr1_l = '</tr>';
						$str = $tr1.$td1.$td2.$td3.$td4.$td10.$td5.$td6.$td7.$td8.$td9.$tr1_l;
						$data[] = array('tr'=>$str);
					}
					$tr1 = "<tr class='avoid-sortSer1' style='display:none'>";
					$td1 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);' colspan='2'>Total</td>";
					$td2 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='all' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalservice'>".$no_lsr_all_total."</button></td>";
					$td3 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='all' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalservice'>".$no_bsr_all_total."(".number_format((($no_bsr_all_total/$no_lsr_all_total)*100),2)."%)</button></td>";
					$td9 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='all' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='envconv' data-target='#myModalservice'>".$no_env_all_total."(".number_format((($no_env_all_total/$no_bsr_all_total)*100),2)."%)</button></td>";
					$td4 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='all' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalservice'>".$no_fsr_all_total."(".number_format((($no_fsr_all_total/$no_lsr_all_total)*100),2)."%)</button></td>";
					$td5 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='all' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalservice'>".$no_csr_all_total."(".number_format((($no_csr_all_total/$no_lsr_all_total)*100),2)."%)</button></td>";
					$td6 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='all' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalservice'>".$no_osr_all_total."(".number_format((($no_osr_all_total/$no_lsr_all_total)*100),2)."%)</button></td>";
					$td7 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='all' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalservice'>".$no_isr_all_total."(".number_format((($no_isr_all_total/$no_lsr_all_total)*100),2)."%)</button></td>";
					$td8 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='all' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalservice'>".$no_dsr_all_total."</button></td>";
					
					$str = $tr1.$td1.$td2.$td3.$td9.$td4.$td5.$td6.$td7.$td8.$tr1_l;
					$data[] = array('tr'=>$str);
					
					$result['table1'] = $data;

					$no_lsr_all_total = 0;$no_bsr_all_total = 0;$no_fsr_all_total = 0;$no_csr_all_total =0;$no_osr_all_total = 0;$no_isr_all_total = 0;$no_dsr_all_total = 0;$no_env_all_total = 0;

					foreach(array_keys($master_service_arr['2w']) as $key=>$master_service_all)
					{

						$no_lsr_all = $master_service_arr['2w'][$master_service_all]['no_lsr_all'];
						$no_bsr_all = $master_service_arr['2w'][$master_service_all]['no_bsr_all'];
						$no_fsr_all = $master_service_arr['2w'][$master_service_all]['no_fsr_all'];
						$no_csr_all = $master_service_arr['2w'][$master_service_all]['no_csr_all'];
						$no_osr_all = $master_service_arr['2w'][$master_service_all]['no_osr_all'];
						$no_isr_all = $master_service_arr['2w'][$master_service_all]['no_isr_all'];
						$no_dsr_all = $master_service_arr['2w'][$master_service_all]['no_dsr_all'];
						$no_env_all = $master_service_arr['2w'][$master_service_all]['no_env_all'];
						if($master_service_all == 'total')
						{
							$no_lsr_all_total = $master_service_arr['2w']['total']['no_lsr_all_total'];
							$no_bsr_all_total = $master_service_arr['2w']['total']['no_bsr_all_total'];
							$no_fsr_all_total = $master_service_arr['2w']['total']['no_fsr_all_total'];
							$no_csr_all_total = $master_service_arr['2w']['total']['no_csr_all_total'];
							$no_osr_all_total = $master_service_arr['2w']['total']['no_osr_all_total'];
							$no_isr_all_total = $master_service_arr['2w']['total']['no_isr_all_total'];
							$no_dsr_all_total = $master_service_arr['2w']['total']['no_dsr_all_total'];
							$no_env_all_total = $master_service_arr['2w']['total']['no_env_all_total'];
						}
						$tr1 = '<tr>';
						if($no_lsr_all == 0)
						{ 
							continue; 
						}
						else
						{ 
							$td1 = "<td>".$master_service_all."</td>";
							$td2 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalservice'>".$no_lsr_all."</button></td>";
							$td3 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalservice'>".$no_bsr_all."(".number_format((($no_bsr_all/$no_lsr_all)*100),2)."%)</button></td>";
							$td9 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='envconv' data-target='#myModalservice'>".$no_env_all."(".number_format((($no_env_all/$no_bsr_all)*100),2)."%)</button></td>";
							$td4 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalservice'>".$no_fsr_all."(".number_format((($no_fsr_all/$no_lsr_all)*100),2)."%)</button></td>";
							$td5 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalservice'>".$no_csr_all."(".number_format((($no_csr_all/$no_lsr_all)*100),2)."%)</button></td>";
							$td6 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalservice'>".$no_osr_all."(".number_format((($no_osr_all/$no_lsr_all)*100),2)."%)</button></td>";
							$td7 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalservice'>".$no_isr_all."(".number_format((($no_isr_all/$no_lsr_all)*100),2)."%)</button></td>";
							$td8 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalservice'>".$no_dsr_all."</button></td>";
						}
						$tr1_l = '</tr>';
						$str = $tr1.$td1.$td2.$td3.$td9.$td4.$td5.$td6.$td7.$td8.$tr1_l;
						$data1[] = array('tr'=>$str);
					}
					$tr2 = "<tr class='avoid-sortSer2' style='display:none'>";
					$td1 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>Total</td>";
					$td2 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='2w' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalservice'>".$no_lsr_all_total."</button></td>";
					$td3 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='2w' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalservice'>".$no_bsr_all_total."(".number_format((($no_bsr_all_total/$no_lsr_all_total)*100),2)."%)</button></td>";
					$td9 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='2w' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='envconv' data-target='#myModalservice'>".$no_env_all_total."(".number_format((($no_env_all_total/$no_bsr_all_total)*100),2)."%)</button></td>";
					$td4 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='2w' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalservice'>".$no_fsr_all_total."(".number_format((($no_fsr_all_total/$no_lsr_all_total)*100),2)."%)</button></td>";
					$td5 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='2w' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalservice'>".$no_csr_all_total."(".number_format((($no_csr_all_total/$no_lsr_all_total)*100),2)."%)</button></td>";
					$td6 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='2w' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalservice'>".$no_osr_all_total."(".number_format((($no_osr_all_total/$no_lsr_all_total)*100),2)."%)</button></td>";
					$td7 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='2w' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalservice'>".$no_isr_all_total."(".number_format((($no_isr_all_total/$no_lsr_all_total)*100),2)."%)</button></td>";
					$td8 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='2w' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalservice'>".$no_dsr_all_total."</button></td>";
					$tr_2="</tr>";
					$str = $tr2.$td1.$td2.$td3.$td9.$td4.$td5.$td6.$td7.$td8.$tr_2;
					$data1[] = array('tr'=>$str);
					
					$result['table2'] = $data1;
				$no_lsr_all_total = 0;$no_bsr_all_total = 0;$no_fsr_all_total = 0;$no_csr_all_total =0;$no_osr_all_total = 0;$no_isr_all_total = 0;$no_dsr_all_total = 0;$no_env_all_total = 0;
					foreach(array_keys($master_service_arr['4w']) as $key=>$master_service_all)
					{
						$no_lsr_all = $master_service_arr['4w'][$master_service_all]['no_lsr_all'];
						$no_bsr_all = $master_service_arr['4w'][$master_service_all]['no_bsr_all'];
						$no_fsr_all = $master_service_arr['4w'][$master_service_all]['no_fsr_all'];
						$no_csr_all = $master_service_arr['4w'][$master_service_all]['no_csr_all'];
						$no_osr_all = $master_service_arr['4w'][$master_service_all]['no_osr_all'];
						$no_isr_all = $master_service_arr['4w'][$master_service_all]['no_isr_all'];
						$no_dsr_all = $master_service_arr['4w'][$master_service_all]['no_dsr_all'];
						$no_env_all = $master_service_arr['4w'][$master_service_all]['no_env_all'];
						if($master_service_all == 'total')
						{
							$no_lsr_all_total = $master_service_arr['4w']['total']['no_lsr_all_total'];
							$no_bsr_all_total = $master_service_arr['4w']['total']['no_bsr_all_total'];
							$no_fsr_all_total = $master_service_arr['4w']['total']['no_fsr_all_total'];
							$no_csr_all_total = $master_service_arr['4w']['total']['no_csr_all_total'];
							$no_osr_all_total = $master_service_arr['4w']['total']['no_osr_all_total'];
							$no_isr_all_total = $master_service_arr['4w']['total']['no_isr_all_total'];
							$no_dsr_all_total = $master_service_arr['4w']['total']['no_dsr_all_total'];
							$no_env_all_total = $master_service_arr['4w']['total']['no_env_all_total'];
							// echo $no_lsr_all_total;
							// die;
							
						}
						$tr1 = '<tr>';
						if($no_lsr_all == 0)
						{ 
							continue; 
						}
						else
						{ 
							$td1 = "<td>".$master_service_all."</td>";
							$td2 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalservice'>".$no_lsr_all."</button></td>";
							$td3 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalservice'>".$no_bsr_all."(".number_format((($no_bsr_all/$no_lsr_all)*100),2)."%)</button></td>";
							$td9 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='envconv' data-target='#myModalservice'>".$no_env_all."(".number_format((($no_env_all/$no_bsr_all)*100),2)."%)</button></td>";
							$td4 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalservice'>".$no_fsr_all."(".number_format((($no_fsr_all/$no_lsr_all)*100),2)."%)</button></td>";
							$td5 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalservice'>".$no_csr_all."(".number_format((($no_csr_all/$no_lsr_all)*100),2)."%)</button></td>";
							$td6 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalservice'>".$no_osr_all."(".number_format((($no_osr_all/$no_lsr_all)*100),2)."%)</button></td>";
							$td7 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalservice'>".$no_isr_all."(".number_format((($no_isr_all/$no_lsr_all)*100),2)."%)</button></td>";
							$td8 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-service='".$master_service_all."' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalservice'>".$no_dsr_all."</button></td>";
						}
						$tr1_l = '</tr>';
						$str = $tr1.$td1.$td2.$td3.$td9.$td4.$td5.$td6.$td7.$td8.$tr1_l;
						$data2[] = array('tr'=>$str);
					}
					$tr3 = "<tr class='avoid-sortSer3' style='display:none'>";
					$td1 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>Total</td>";
					$td2 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='4w' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalservice'>".$no_lsr_all_total."</button></td>";
					$td3 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='4w' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalservice'>".$no_bsr_all_total."(".number_format((($no_bsr_all_total/$no_lsr_all_total)*100),2)."%)</button></td>";
					$td9 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='4w' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='envconv' data-target='#myModalservice'>".$no_env_all_total."(".number_format((($no_env_all_total/$no_bsr_all_total)*100),2)."%)</button></td>";
					$td4 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='4w' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalservice'>".$no_fsr_all_total."(".number_format((($no_fsr_all_total/$no_lsr_all_total)*100),2)."%)</button></td>";
					$td5 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='4w' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalservice'>".$no_csr_all_total."(".number_format((($no_csr_all_total/$no_lsr_all_total)*100),2)."%)</button></td>";
					$td6 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='4w' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalservice'>".$no_osr_all_total."(".number_format((($no_osr_all_total/$no_lsr_all_total)*100),2)."%)</button></td>";
					$td7 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='4w' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalservice'>".$no_isr_all_total."(".number_format((($no_isr_all_total/$no_lsr_all_total)*100),2)."%)</button></td>";
					$td8 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='4w' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalservice'>".$no_dsr_all_total."</button></td>";
					$tr_3 = '</tr>';
					$str = $tr3.$td1.$td2.$td3.$td9.$td4.$td5.$td6.$td7.$td8.$tr_3;
					$data2[] = array('tr'=>$str);
					
					$result['table3'] = $data2;
					$result['count'] = $master_service_arr['all']['total']['no_lsr_all_total'];
					
					echo json_encode($result);
					//echo $result;
					break;
	case "#Person" :
					$sql_person = "SELECT crm_log_id FROM crm_admin WHERE flag = 0 ORDER BY name ASC";
                    $res_person = mysqli_query($conn,$sql_person) or die(mysqli_error($conn));
                    while($row_person = mysqli_fetch_object($res_person)){
						$crm_id = $row_person->crm_log_id;
						$persons_arr[$crm_id]['no_lp'] = $persons_arr[$crm_id]['no_bp'] = $persons_arr[$crm_id]['no_fp'] = $persons_arr[$crm_id]['no_cp'] = $persons_arr[$crm_id]['no_op'] = $persons_arr[$crm_id]['no_ip'] = $persons_arr[$crm_id]['no_dp'] = $persons_arr[$crm_id]['no_envp'] = 0;
					}
                    // initialize total count
                    $persons_arr['total']['no_lp_total'] = $persons_arr['total']['no_bp_total'] = $persons_arr['total']['no_fp_total'] = $persons_arr['total']['no_cp_total'] = $persons_arr['total']['no_op_total'] = $persons_arr['total']['no_ip_total'] = $persons_arr['total']['no_dp_total'] = $persons_arr['total']['no_envp_total'] =0;
                    
					
                        $cond_p ='';
                        $cond_p = $cond_p.($city == 'all' ? "" : "AND b.city='$city'");
                        $cond_p = $cond_p.($crm_person == 'all' ? "" : "AND b.crm_update_id='$crm_person'");
                        $cond_p = $cond_p.($bservice_type == 'all' ? "" : "AND b.service_type='$bservice_type'");
                        $cond_p = $cond_p.($bsource == 'all' ? "" : "AND b.source='$bsource'");
                        $cond_p = $cond_p.($bvehicle == 'all' ? "" : "AND b.vehicle_type='$bvehicle'");
                        $cond_p = $cond_p.($bmaster_service == 'all' ? "" : "AND gs.master_service IN ('$bmaster_service')");


                        $sql_p = $city == "Chennai" ? "SELECT distinct b.booking_id,b.flag,b.booking_status,b.service_type,b.flag_fo,b.log,b.activity_status,b.vehicle_type,b.crm_update_id,crm.name,b.flag_unwntd,bb.b2b_vehicle_at_garage,b.service_status,gs.master_service FROM go_bumpr.user_booking_tb b left join go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type left join crm_admin crm on b.crm_update_id=crm.crm_log_id left join b2b.b2b_booking_tbl bb on b.booking_id=bb.gb_booking_id where b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings'  AND b.log BETWEEN '$start' AND '$end' {$cond_p} and (ISNULL(b2b_swap_flag) or b2b_swap_flag!='1') group by b.booking_id" : "SELECT distinct b.booking_id,b.flag,b.booking_status,b.service_type,b.flag_fo,b.log,b.activity_status,b.vehicle_type,b.crm_update_id,crm.name,b.flag_unwntd,bb.b2b_vehicle_at_garage,b.service_status,gs.master_service FROM go_bumpr.user_booking_tb b left join go_axle_service_price_tbl gs on b.service_type=gs.service_type left join crm_admin crm on b.crm_update_id=crm.crm_log_id left join b2b.b2b_booking_tbl bb on b.booking_id=bb.gb_booking_id where b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings'  AND b.source != 'Sulekha Booking'  AND b.log BETWEEN '$start' AND '$end' {$cond_p} and (ISNULL(b2b_swap_flag) or b2b_swap_flag!='1') group by b.booking_id";
                         //echo $sql_p;
						// $sql_p = $city == "all" ? "SELECT booking_id,flag,booking_status,flag_fo,log,activity_status,flag_unwntd FROM user_booking_tb WHERE crm_update_id='$crm_id' AND mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' " : "SELECT booking_id,flag,booking_status,flag_fo,log,activity_status,flag_unwntd FROM user_booking_tb WHERE crm_update_id='$crm_id' AND city='$city' AND mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' ";
                        $res_p = mysqli_query($conn,$sql_p);


                        /* $no_rows_p = mysqli_num_rows($res_p);
                        if($no_rows_p < 1){
                            continue;
                        } */

                        //Person counts
                        //$no_lp = 0;$no_bp = 0;$no_fp = 0;$no_cp = 0;$no_op = 0;$no_ip = 0;$no_dp=0;$no_env=0;

                        while($row_p = mysqli_fetch_object($res_p)){
                            $booking_flag_p = $row_p->flag;
                            $status_p = $row_p->booking_status;
                            $flag_fo_p = $row_p->flag_fo;
                            $flag_unwntd_p = $row_p->flag_unwntd;
                            $activity_status_p = $row_p->activity_status;
							$crm_id = $row_p->crm_update_id;
							$vehicle_at_garage=$row_p->b2b_vehicle_at_garage;
							$service_status=$row_p->service_status;

                            $persons_arr[$crm_id]['name'] = $row_p->name;
                            if($flag_unwntd_p == '1'){
                                if($booking_flag_p == '1' && $activity_status_p=='26'){
                                    $persons_arr[$crm_id]['no_dp']=$persons_arr[$crm_id]['no_dp']+1;
                                    $persons_arr['total']['no_dp_total']=$persons_arr['total']['no_dp_total']+1;
                                }
                            }
                            else{
                                $persons_arr[$crm_id]['no_lp']=$persons_arr[$crm_id]['no_lp']+1;
                                $persons_arr['total']['no_lp_total']=$persons_arr['total']['no_lp_total']+1;
    
                                switch($status_p){
                                    case '1':if($booking_flag_p == '1'){
                                                $persons_arr[$crm_id]['no_cp']=$persons_arr[$crm_id]['no_cp']+1;
                                                $persons_arr['total']['no_cp_total']=$persons_arr['total']['no_cp_total']+1;
                                             }
                                            else{
                                                if($flag_fo_p == '1'){
                                                    $persons_arr[$crm_id]['no_fp']=$persons_arr[$crm_id]['no_fp']+1;
                                                    $persons_arr['total']['no_fp_total']=$persons_arr['total']['no_fp_total']+1;
                                                }
                                                else{
                                                    $persons_arr[$crm_id]['no_ip']=$persons_arr[$crm_id]['no_ip']+1;
                                                    $persons_arr['total']['no_ip_total']=$persons_arr['total']['no_ip_total']+1;
                                                }
                                            }
                                            break;
                                    case '2':if($booking_flag_p == '1')
                                            {
                                                $persons_arr[$crm_id]['no_cp']=$persons_arr[$crm_id]['no_cp']+1;
                                                $persons_arr['total']['no_cp_total']=$persons_arr['total']['no_cp_total']+1;
                                            }
                                            else{
                                                $persons_arr[$crm_id]['no_bp']=$persons_arr[$crm_id]['no_bp']+1;
												$persons_arr['total']['no_bp_total']=$persons_arr['total']['no_bp_total']+1;
												if(($vehicle_at_garage=='1') || ($service_status=="Completed") || ($service_status=="In Progress")){
													
													$persons_arr[$crm_id]['no_envp']=$persons_arr[$crm_id]['no_envp']+1;
													$persons_arr['total']['no_envp_total']=$persons_arr['total']['no_envp_total']+1;
												}
                                            }
                                            break;
                                    case '3':
                                    case '4':
                                    case '5':
                                    case '6': if($booking_flag_p != '1'){
                                                $persons_arr[$crm_id]['no_fp']=$persons_arr[$crm_id]['no_fp']+1;
                                                $persons_arr['total']['no_fp_total']=$persons_arr['total']['no_fp_total']+1;
                                            }
                                            else{
                                                $persons_arr[$crm_id]['no_cp']=$persons_arr[$crm_id]['no_cp']+1;
                                                $persons_arr['total']['no_cp_total']=$persons_arr['total']['no_cp_total']+1;
                                            }
    
                                            break;
                                    case '0':if($booking_flag_p != '1'){
                                                $persons_arr[$crm_id]['no_op']=$persons_arr[$crm_id]['no_op']+1;
                                                $persons_arr['total']['no_op_total']=$persons_arr['total']['no_op_total']+1;
                                            }
                                            else{
                                                $persons_arr[$crm_id]['no_cp']=$persons_arr[$crm_id]['no_cp']+1;
                                                $persons_arr['total']['no_cp_total']=$persons_arr['total']['no_cp_total']+1;
                                            }
                                            break;
                                } // switch
                            } // else
                        } // while
					//print_r($persons_arr);
					
					foreach(array_keys($persons_arr) as $key=>$crm_id)
					{
						$crm_name = $persons_arr[$crm_id]['name'];
						if($crm_name != 'total')
						{
							$no_lp = $persons_arr[$crm_id]['no_lp'];
							$no_bp = $persons_arr[$crm_id]['no_bp'];
							$no_fp = $persons_arr[$crm_id]['no_fp'];
							$no_cp = $persons_arr[$crm_id]['no_cp'];
							$no_op = $persons_arr[$crm_id]['no_op'];
							$no_ip = $persons_arr[$crm_id]['no_ip'];
							$no_dp = $persons_arr[$crm_id]['no_dp'];
							$no_envp = $persons_arr[$crm_id]['no_envp'];
						}
						$tr1 = "<tr>";
						if($no_lp == 0){
							continue;
						}
						else{
							$td1 = "<td>".$crm_name."</td>";
							$td2 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$crm_id."' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalperson'>".$no_lp."</button></td>";
							$td3 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$crm_id."' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalperson'>".$no_bp." (".number_format((($no_bp/$no_lp)*100),2)."%)</button></td>";
							$td9 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$crm_id."' data-startdate='".$start."' data-enddate='".$end."' data-status='envconv' data-target='#myModalperson'>".$no_envp." (".number_format((($no_envp/$no_bp)*100),2)."%)</button></td>";
							$td4 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$crm_id."' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalperson'>".$no_fp." (".number_format((($no_fp/$no_lp)*100),2)."%)</button></td>";
							$td5 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$crm_id."' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalperson'>".$no_cp." (".number_format((($no_cp/$no_lp)*100),2)."%)</button></td>";
							$td6 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$crm_id."' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalperson'>".$no_op." (".number_format((($no_op/$no_lp)*100),2)."%)</button></td>";
							$td7 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$crm_id."' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalperson'>".$no_ip." (".number_format((($no_ip/$no_lp)*100),2)."%)</button></td>";
							$td8 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$crm_id."' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalperson'>".$no_dp."</button></td>";
						}
						$tr1_l = "</tr>";
						$str = $tr1.$td1.$td2.$td3.$td9.$td4.$td5.$td6.$td7.$td8.$tr1_l;
						$person_data[] = array('tr'=>$str);
					}
					$no_lp_total = $persons_arr['total']['no_lp_total'];
					$no_bp_total = $persons_arr['total']['no_bp_total'];
					$no_fp_total = $persons_arr['total']['no_fp_total'];
					$no_cp_total = $persons_arr['total']['no_cp_total'];
					$no_op_total = $persons_arr['total']['no_op_total'];
					$no_ip_total = $persons_arr['total']['no_ip_total'];
					$no_dp_total = $persons_arr['total']['no_dp_total'];
					$no_envp_total = $persons_arr['total']['no_envp_total'];

					$tr2="<tr class = 'avoid-sortperson' style='display:none'>";
					$td1 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>Total</td>";
					$td2 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalperson'>".$no_lp_total."</button></td>";
					$td3 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalperson'>".$no_bp_total."(".number_format((($no_bp_total/$no_lp_total)*100),2)."%)</button></td>";
					$td9 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='envconv' data-target='#myModalperson'>".$no_envp_total."(".number_format((($no_envp_total/$no_bp_total)*100),2)."%)</button></td>";
					$td4 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalperson'>".$no_fp_total."(".number_format((($no_fp_total/$no_lp_total)*100),2)."%)</button></td>";
					$td5 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalperson'>".$no_cp_total."(".number_format((($no_cp_total/$no_lp_total)*100),2)."%)</button></td>";
					$td6 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalperson'>".$no_op_total."(".number_format((($no_op_total/$no_lp_total)*100),2)."%)</button></td>";
					$td7 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalperson'>".$no_ip_total."(".number_format((($no_ip_total/$no_lp_total)*100),2)."%)</button></td>";
					$td8 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalperson'>".$no_dp_total."</button></td>";
					
					$str = $tr2.$td1.$td2.$td3.$td9.$td4.$td5.$td6.$td7.$td8.$tr1_l;
					$person_data[] = array('tr'=>$str);

					$result['person_data'] = $person_data;
					$result['count'] = $persons_arr['total']['no_lp_total'];
					echo json_encode($result);
					break; 
	case "#Source" :
				        $sql_source = "SELECT user_source FROM user_source_tbl WHERE flag='0' ORDER By user_source ASC";
                        $res_source = mysqli_query($conn,$sql_source) or die(mysqli_error($conn));
                        
						while($row_source = mysqli_fetch_object($res_source)){
							$source = $row_source->user_source;
							$sources_arr['all'][$source]['no_ls_all'] = $sources_arr['all'][$source]['no_bs_all'] = $sources_arr['all'][$source]['no_fs_all'] = $sources_arr['all'][$source]['no_cs_all'] = $sources_arr['all'][$source]['no_os_all'] = $sources_arr['all'][$source]['no_is_all'] = $sources_arr['all'][$source]['no_ds_all'] = $sources_arr['all'][$source]['no_envs_all'] = 0;
							$sources_arr['2w'][$source]['no_ls_all'] = $sources_arr['2w'][$source]['no_bs_all'] = $sources_arr['2w'][$source]['no_fs_all'] = $sources_arr['2w'][$source]['no_cs_all'] = $sources_arr['2w'][$source]['no_os_all'] = $sources_arr['2w'][$source]['no_is_all'] = $sources_arr['2w'][$source]['no_ds_all'] = $sources_arr['2w'][$source]['no_envs_all'] = 0;
							$sources_arr['4w'][$source]['no_ls_all'] = $sources_arr['4w'][$source]['no_bs_all'] = $sources_arr['4w'][$source]['no_fs_all'] = $sources_arr['4w'][$source]['no_cs_all'] = $sources_arr['4w'][$source]['no_os_all'] = $sources_arr['4w'][$source]['no_is_all'] = $sources_arr['4w'][$source]['no_ds_all'] = $sources_arr['4w'][$source]['no_envs_all'] = 0;
						}
						$no_ls_all_total['all'] = $no_ls_all_total['2w'] = $no_ls_all_total['4w'] = 0;
						$no_bs_all_total['all'] = $no_bs_all_total['2w'] = $no_bs_all_total['4w'] = 0;
						$no_fs_all_total['all'] = $no_fs_all_total['2w'] = $no_fs_all_total['4w'] = 0;
						$no_cs_all_total['all'] = $no_cs_all_total['2w'] = $no_cs_all_total['4w'] = 0;
						$no_os_all_total['all'] = $no_os_all_total['2w'] = $no_os_all_total['4w'] = 0;
						$no_is_all_total['all'] = $no_is_all_total['2w'] = $no_is_all_total['4w'] = 0;
						$no_ds_all_total['all'] = $no_ds_all_total['2w'] = $no_ds_all_total['4w'] = 0;
						$no_envs_all_total['all'] = $no_envs_all_total['2w'] = $no_envs_all_total['4w'] = 0;

                            //Source counts
                            //all
                            
                            $cond_so_all ='';
                            $cond_so_all = $cond_so_all.($city == 'all' ? "" : "AND b.city='$city'");
                            $cond_so_all = $cond_so_all.($crm_person == 'all' ? "" : "AND b.crm_update_id='$crm_person'");
                             $cond_so_all = $cond_so_all.($bservice_type == 'all' ? "" : "AND b.service_type='$bservice_type'");
                             $cond_so_all = $cond_so_all.($bsource == 'all' ? "" : "AND b.source='$bsource'");
							$cond_so_all = $cond_so_all.($bvehicle == 'all' ? "" : "AND b.vehicle_type='$bvehicle'");
							$cond_so_all = $cond_so_all.($bmaster_service == 'all' ? "" : "AND gs.master_service IN ('$bmaster_service')");

                            
                            $sql_s_all = $city == "Chennai" ? "SELECT DISTINCT b.booking_id,b.flag,b.booking_status,b.flag_fo,b.log,b.activity_status,b.source,b.vehicle_type,b.flag_unwntd,b.service_status,bb.b2b_vehicle_at_garage,gs.master_service FROM user_booking_tb as b left join go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type left join b2b.b2b_booking_tbl bb on b.booking_id=bb.gb_booking_id WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings'  AND b.log BETWEEN '$start' AND '$end' {$cond_so_all} and (ISNULL(b2b_swap_flag) or b2b_swap_flag!='1') group by b.booking_id" : "SELECT DISTINCT b.booking_id,b.flag,b.booking_status,b.flag_fo,b.log,b.activity_status,b.source,b.vehicle_type,b.flag_unwntd,bb.b2b_vehicle_at_garage,b.service_status,gs.master_service FROM user_booking_tb as b left join go_axle_service_price_tbl gs on b.service_type=gs.service_type left join b2b.b2b_booking_tbl bb on b.booking_id=bb.gb_booking_id WHERE b.mec_id NOT IN(400001,200018,200379,400974) AND b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884) AND b.service_type != 'IOCL Check-up' AND b.source !='Re-Engagement Bookings' AND b.source != 'Sulekha Booking'   AND b.log BETWEEN '$start' AND '$end' {$cond_so_all} and (ISNULL(b2b_swap_flag) or b2b_swap_flag!='1') group by b.booking_id";
							//	echo $sql_s_all;
                            //$sql_s_all = $city == "all" ? "SELECT booking_id,flag,booking_status,flag_fo,log,activity_status,flag_unwntd FROM user_booking_tb WHERE source='$source' AND mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' " : "SELECT booking_id,flag,booking_status,flag_fo,log,activity_status,flag_unwntd FROM user_booking_tb WHERE source='$source' AND city='$city' AND mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' ";
                            $res_s_all = mysqli_query($conn,$sql_s_all);

                            while($row_s_all = mysqli_fetch_object($res_s_all)){
                                $booking_flag_sa = $row_s_all->flag;
                                $status_sa = $row_s_all->booking_status;
                                $flag_fo_sa = $row_s_all->flag_fo;
                                $flag_unwntd_sa = $row_s_all->flag_unwntd;
								$activity_status_sa = $row_s_all->activity_status;
								$vehicle_at_garage=$row_s_all->b2b_vehicle_at_garage;
								$service_status=$row_s_all->service_status;

								if($row_s_all->source == 'bookNow')
								{
									$source = 'website';
								}
								else
								{
									$source = $row_s_all->source;
								}
								$type = $row_s_all->vehicle_type;

                                if($flag_unwntd_sa == '1'){
                                    if($booking_flag_sa == '1' && $activity_status_sa == '26'){
                                        $sources_arr['all'][$source]['no_ds_all']=$sources_arr['all'][$source]['no_ds_all']+1;
                                        $sources_arr[$type][$source]['no_ds_all']=$sources_arr[$type][$source]['no_ds_all']+1;
                                        $no_ds_all_total['all']=$no_ds_all_total['all']+1;
                                        $no_ds_all_total[$type]=$no_ds_all_total[$type]+1;
                                    }
                                }
                                else{
                                    $sources_arr['all'][$source]['no_ls_all']=$sources_arr['all'][$source]['no_ls_all']+1;
                                    $sources_arr[$type][$source]['no_ls_all']=$sources_arr[$type][$source]['no_ls_all']+1;
                                    $no_ls_all_total['all']=$no_ls_all_total['all']+1;
                                    $no_ls_all_total[$type]=$no_ls_all_total[$type]+1;
        
                                    switch($status_sa){
                                        case '1':if($booking_flag_sa == '1'){
                                                    $sources_arr['all'][$source]['no_cs_all']=$sources_arr['all'][$source]['no_cs_all']+1;
                                                    $sources_arr[$type][$source]['no_cs_all']=$sources_arr[$type][$source]['no_cs_all']+1;
                                                    $no_cs_all_total['all']=$no_cs_all_total['all']+1;
                                                    $no_cs_all_total[$type]=$no_cs_all_total[$type]+1;
                                                }
                                                else{
                                                    if($flag_fo_sa == '1'){
                                                        $sources_arr['all'][$source]['no_fs_all']=$sources_arr['all'][$source]['no_fs_all']+1;
                                                        $sources_arr[$type][$source]['no_fs_all']=$sources_arr[$type][$source]['no_fs_all']+1;
                                                        $no_fs_all_total['all']=$no_fs_all_total['all']+1;
                                                        $no_fs_all_total[$type]=$no_fs_all_total[$type]+1;
                                                    }
                                                    else{
                                                        $sources_arr['all'][$source]['no_is_all']=$sources_arr['all'][$source]['no_is_all']+1;
                                                        $sources_arr[$type][$source]['no_is_all']=$sources_arr[$type][$source]['no_is_all']+1;
                                                        $no_is_all_total['all']=$no_is_all_total['all']+1;
                                                        $no_is_all_total[$type]=$no_is_all_total[$type]+1;
                                                    }
                                                }
                                                break;
                                         case '2':if($booking_flag_sa == '1'){
                                                    $sources_arr['all'][$source]['no_cs_all;']=$sources_arr['all'][$source]['no_cs_all']+1;
                                                    $sources_arr[$type][$source]['no_cs_all;']=$sources_arr[$type][$source]['no_cs_all']+1;
                                                    $no_cs_all_total['all']=$no_cs_all_total['all']+1;
                                                    $no_cs_all_total[$type]=$no_cs_all_total[$type]+1;
                                                }
                                                else{
                                                    $sources_arr['all'][$source]['no_bs_all']=$sources_arr['all'][$source]['no_bs_all']+1;
                                                    $sources_arr[$type][$source]['no_bs_all']=$sources_arr[$type][$source]['no_bs_all']+1;
                                                    $no_bs_all_total['all']=$no_bs_all_total['all']+1;
													$no_bs_all_total[$type]=$no_bs_all_total[$type]+1;
													if(($vehicle_at_garage=='1') || ($service_status=="Completed") || ($service_status=="In Progress")){
														$sources_arr['all'][$source]['no_envs_all']=$sources_arr['all'][$source]['no_envs_all']+1;
                                                    	$sources_arr[$type][$source]['no_envs_all']=$sources_arr[$type][$source]['no_envs_all']+1;
                                                    	$no_envs_all_total['all']=$no_envs_all_total['all']+1;
														$no_envs_all_total[$type]=$no_envs_all_total[$type]+1;
													}
                                                }
                                                break;
                                        case '3':
                                        case '4':
                                        case '5':
                                        case '6':if($booking_flag_sa != '1'){
                                                    $sources_arr['all'][$source]['no_fs_all']=$sources_arr['all'][$source]['no_fs_all']+1;
                                                    $sources_arr[$type][$source]['no_fs_all']=$sources_arr[$type][$source]['no_fs_all']+1;
                                                    $no_fs_all_total['all']=$no_fs_all_total['all']+1;
                                                    $no_fs_all_total[$type]=$no_fs_all_total[$type]+1;
                                                }
                                                else{
                                                    $sources_arr['all'][$source]['no_cs_all']= $sources_arr['all'][$source]['no_cs_all']+1;
                                                    $sources_arr[$type][$source]['no_cs_all']= $sources_arr[$type][$source]['no_cs_all']+1;
                                                    $no_cs_all_total['all']=$no_cs_all_total['all']+1;
                                                    $no_cs_all_total[$type]=$no_cs_all_total[$type]+1;
                                                }
                                                break;
                                        case '0':if($booking_flag_sa != '1'){
                                                    $sources_arr['all'][$source]['no_os_all']=$sources_arr['all'][$source]['no_os_all']+1;
                                                    $sources_arr[$type][$source]['no_os_all']=$sources_arr[$type][$source]['no_os_all']+1;
                                                    $no_os_all_total['all']=$no_os_all_total['all']+1;
                                                    $no_os_all_total[$type]=$no_os_all_total[$type]+1;
                                                }
                                                else{
                                                    $sources_arr['all'][$source]['no_cs_all']=$sources_arr['all'][$source]['no_cs_all']+1;
                                                    $sources_arr[$type][$source]['no_cs_all']=$sources_arr[$type][$source]['no_cs_all']+1;
                                                    $no_cs_all_total['all']= $no_cs_all_total['all']+1;
                                                    $no_cs_all_total[$type]= $no_cs_all_total[$type]+1;
                                                }
                                                break;
                                    }
                                }
								
								$sources_arr['all']['total']['no_ls_all_total'] = $no_ls_all_total['all'];
								$sources_arr['all']['total']['no_bs_all_total'] = $no_bs_all_total['all'];
								$sources_arr['all']['total']['no_fs_all_total'] = $no_fs_all_total['all'];
								$sources_arr['all']['total']['no_cs_all_total'] = $no_cs_all_total['all'];
								$sources_arr['all']['total']['no_os_all_total'] = $no_os_all_total['all'];
								$sources_arr['all']['total']['no_is_all_total'] = $no_is_all_total['all'];
								$sources_arr['all']['total']['no_ds_all_total'] = $no_ds_all_total['all'];
								$sources_arr['all']['total']['no_envs_all_total'] = $no_envs_all_total['all'];
								
								$sources_arr[$type]['total']['no_ls_all_total'] = $no_ls_all_total[$type];
								$sources_arr[$type]['total']['no_bs_all_total'] = $no_bs_all_total[$type];
								$sources_arr[$type]['total']['no_fs_all_total'] = $no_fs_all_total[$type];
								$sources_arr[$type]['total']['no_cs_all_total'] = $no_cs_all_total[$type];
								$sources_arr[$type]['total']['no_os_all_total'] = $no_os_all_total[$type];
								$sources_arr[$type]['total']['no_is_all_total'] = $no_is_all_total[$type];
								$sources_arr[$type]['total']['no_ds_all_total'] = $no_ds_all_total[$type];
								$sources_arr[$type]['total']['no_envs_all_total'] = $no_envs_all_total[$type];
                           }
						   // print_r($sources_arr);
                           $no_ls_all_total=0;$no_bs_all_total=0;$no_fs_all_total=0;$no_cs_all_total=0;$no_os_all_total=0;$no_is_all_total=0;$no_ds_all_total=0;$no_envs_all_total=0;

						foreach(array_keys($sources_arr['all']) as $key=>$source)
						{
							$no_ls_all = $sources_arr['all'][$source]['no_ls_all'];
							$no_bs_all = $sources_arr['all'][$source]['no_bs_all'];
							$no_fs_all = $sources_arr['all'][$source]['no_fs_all'];
							$no_cs_all = $sources_arr['all'][$source]['no_cs_all'];
							$no_os_all = $sources_arr['all'][$source]['no_os_all'];
							$no_is_all = $sources_arr['all'][$source]['no_is_all'];
							$no_ds_all = $sources_arr['all'][$source]['no_ds_all'];
							$no_envs_all = $sources_arr['all'][$source]['no_envs_all'];
							if($source == 'total')
							{
								$no_ls_all_total = $sources_arr['all']['total']['no_ls_all_total'];
								$no_bs_all_total = $sources_arr['all']['total']['no_bs_all_total'];
								$no_fs_all_total = $sources_arr['all']['total']['no_fs_all_total'];
								$no_cs_all_total = $sources_arr['all']['total']['no_cs_all_total'];
								$no_os_all_total = $sources_arr['all']['total']['no_os_all_total'];
								$no_is_all_total = $sources_arr['all']['total']['no_is_all_total'];
								$no_ds_all_total = $sources_arr['all']['total']['no_ds_all_total'];
								$no_envs_all_total = $sources_arr['all']['total']['no_envs_all_total'];
							}
							$tr1 = "<tr>";
							if($no_ls_all == 0){  continue;  }
							else
							{ 
								$td1 = "<td>".$source."</td>";
								$td2 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalsource'>".$no_ls_all."</button></td>";
								$td3 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalsource'>".$no_bs_all."(".number_format((($no_bs_all/$no_ls_all)*100),2)."%)</button></td>";
								$td9 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='envconv' data-target='#myModalsource'>".$no_envs_all."(".number_format((($no_envs_all/$no_bs_all)*100),2)."%)</button></td>";
								$td4 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalsource'>".$no_fs_all."(".number_format((($no_fs_all/$no_ls_all)*100),2)."%)</button></td>";
								$td5 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalsource'>".$no_cs_all."(".number_format((($no_cs_all/$no_ls_all)*100),2)."%)</button></td>";
								$td6 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalsource'>".$no_os_all."(".number_format((($no_os_all/$no_ls_all)*100),2)."%)</button></td>";
								$td7 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalsource'>".$no_is_all."(".number_format((($no_is_all/$no_ls_all)*100),2)."%)</button></td>";
								$td8 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalsource'>".$no_ds_all."</button></td>";
							}
							$tr1_l = "</tr>";
							$str = $tr1.$td1.$td2.$td3.$td9.$td4.$td5.$td6.$td7.$td8.$tr1_l;
							$src_data1[] = array('tr'=>$str);
						}
						$tr2 = "<tr class='avoid-sortsrc1' style='display:none'>";
						$td1 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>Total</td>";
						$td2 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalsource'>".$no_ls_all_total."</button></td>";
						$td3 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalsource'>".$no_bs_all_total."(".number_format((($no_bs_all_total/$no_ls_all_total)*100),2)."%)</button></td>";
						$td9 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='envconv' data-target='#myModalsource'>".$no_envs_all_total."(".number_format((($no_envs_all_total/$no_bs_all_total)*100),2)."%)</button></td>";
						$td4 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalsource'>".$no_fs_all_total."(".number_format((($no_fs_all_total/$no_ls_all_total)*100),2)."%)</button></td>";
						$td5 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalsource'>".$no_cs_all_total."(".number_format((($no_cs_all_total/$no_ls_all_total)*100),2)."%)</button></td>";
						$td6 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalsource'>".$no_os_all_total."(".number_format((($no_os_all_total/$no_ls_all_total)*100),2)."%)</button></td>";
						$td7 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalsource'>".$no_is_all_total."(".number_format((($no_is_all_total/$no_ls_all_total)*100),2)."%)</button></td>";
						$td8 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='all' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalsource'>".$no_ds_all_total."</button></td>";
						$str = $tr2.$td1.$td2.$td3.$td9.$td4.$td5.$td6.$td7.$td8.$tr1_l;
						$src_data1[] = array('tr'=>$str);
						
						$result['src_table1'] = $src_data1;
							?>


						<?php
					$no_ls_all_total =0;$no_bs_all_total =0;$no_fs_all_total =0;$no_cs_all_total =0;$no_os_all_total =0;$no_is_all_total =0;$no_ds_all_total =0;$no_envs_all_total =0;
				        foreach(array_keys($sources_arr['2w']) as $key=>$source)
						{
							$no_ls_all = $sources_arr['2w'][$source]['no_ls_all'];
							$no_bs_all = $sources_arr['2w'][$source]['no_bs_all'];
							$no_fs_all = $sources_arr['2w'][$source]['no_fs_all'];
							$no_cs_all = $sources_arr['2w'][$source]['no_cs_all'];
							$no_os_all = $sources_arr['2w'][$source]['no_os_all'];
							$no_is_all = $sources_arr['2w'][$source]['no_is_all'];
							$no_ds_all = $sources_arr['2w'][$source]['no_ds_all'];
							$no_envs_all = $sources_arr['2w'][$source]['no_envs_all'];
							if($source == 'total')
							{
								$no_ls_all_total = $sources_arr['2w']['total']['no_ls_all_total'];
								$no_bs_all_total = $sources_arr['2w']['total']['no_bs_all_total'];
								$no_fs_all_total = $sources_arr['2w']['total']['no_fs_all_total'];
								$no_cs_all_total = $sources_arr['2w']['total']['no_cs_all_total'];
								$no_os_all_total = $sources_arr['2w']['total']['no_os_all_total'];
								$no_is_all_total = $sources_arr['2w']['total']['no_is_all_total'];
								$no_ds_all_total = $sources_arr['2w']['total']['no_ds_all_total'];
								$no_envs_all_total = $sources_arr['2w']['total']['no_envs_all_total'];
							}
							$tr1 = "<tr>";
							if($no_ls_all == 0){  continue;  }
							else
							{ 
								$td1 = "<td>".$source."</td>";
								$td2 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalsource'>".$no_ls_all."</button></td>";
								$td3 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalsource'>".$no_bs_all."(".number_format((($no_bs_all/$no_ls_all)*100),2)."%)</button></td>";
								$td9 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='envconv' data-target='#myModalsource'>".$no_envs_all."(".number_format((($no_envs_all/$no_bs_all)*100),2)."%)</button></td>";
								$td4 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalsource'>".$no_fs_all."(".number_format((($no_fs_all/$no_ls_all)*100),2)."%)</button></td>";
								$td5 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalsource'>".$no_cs_all."(".number_format((($no_cs_all/$no_ls_all)*100),2)."%)</button></td>";
								$td6 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalsource'>".$no_os_all."(".number_format((($no_os_all/$no_ls_all)*100),2)."%)</button></td>";
								$td7 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalsource'>".$no_is_all."(".number_format((($no_is_all/$no_ls_all)*100),2)."%)</button></td>";
								$td8 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalsource'>".$no_ds_all."</button></td>";
							}
							$tr1_l = "</tr>";
							$str = $tr1.$td1.$td2.$td3.$td9.$td4.$td5.$td6.$td7.$td8.$tr1_l;
							$src_data2[] = array('tr'=>$str);
						}
						$tr2 = "<tr class='avoid-sortsrc2' style='display:none'>";
						$td1 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>Total</td>";
						$td2 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalsource'>".$no_ls_all_total."</button></td>";
						$td3 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalsource'>".$no_bs_all_total."(".number_format((($no_bs_all_total/$no_ls_all_total)*100),2)."%)</button></td>";
						$td9 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='envconv' data-target='#myModalsource'>".$no_envs_all_total."(".number_format((($no_envs_all_total/$no_bs_all_total)*100),2)."%)</button></td>";
						$td4 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalsource'>".$no_fs_all_total."(".number_format((($no_fs_all_total/$no_ls_all_total)*100),2)."%)</button></td>";
						$td5 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalsource'>".$no_cs_all_total."(".number_format((($no_cs_all_total/$no_ls_all_total)*100),2)."%)</button></td>";
						$td6 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalsource'>".$no_os_all_total."(".number_format((($no_os_all_total/$no_ls_all_total)*100),2)."%)</button></td>";
						$td7 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalsource'>".$no_is_all_total."(".number_format((($no_is_all_total/$no_ls_all_total)*100),2)."%)</button></td>";
						$td8 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalsource'>".$no_ds_all_total."</button></td>";
						$str = $tr2.$td1.$td2.$td3.$td9.$td4.$td5.$td6.$td7.$td8.$tr1_l;
						$src_data2[] = array('tr'=>$str);
						
						$result['src_table2'] = $src_data2;?>
            

						<?php
					$no_ls_all_total =0;$no_bs_all_total =0;$no_fs_all_total =0;$no_cs_all_total =0;$no_os_all_total =0;$no_is_all_total =0;$no_ds_all_total =0;$no_envs_all_total =0;
				        foreach(array_keys($sources_arr['4w']) as $key=>$source)
						{
							$no_ls_all = $sources_arr['4w'][$source]['no_ls_all'];
							$no_bs_all = $sources_arr['4w'][$source]['no_bs_all'];
							$no_fs_all = $sources_arr['4w'][$source]['no_fs_all'];
							$no_cs_all = $sources_arr['4w'][$source]['no_cs_all'];
							$no_os_all = $sources_arr['4w'][$source]['no_os_all'];
							$no_is_all = $sources_arr['4w'][$source]['no_is_all'];
							$no_ds_all = $sources_arr['4w'][$source]['no_ds_all'];
							$no_envs_all = $sources_arr['4w'][$source]['no_envs_all'];
							if($source == 'total')
							{
								$no_ls_all_total = $sources_arr['4w']['total']['no_ls_all_total'];
								$no_bs_all_total = $sources_arr['4w']['total']['no_bs_all_total'];
								$no_fs_all_total = $sources_arr['4w']['total']['no_fs_all_total'];
								$no_cs_all_total = $sources_arr['4w']['total']['no_cs_all_total'];
								$no_os_all_total = $sources_arr['4w']['total']['no_os_all_total'];
								$no_is_all_total = $sources_arr['4w']['total']['no_is_all_total'];
								$no_ds_all_total = $sources_arr['4w']['total']['no_ds_all_total'];
								$no_envs_all_total = $sources_arr['4w']['total']['no_envs_all_total'];
							}
							$tr1 = "<tr>";
							if($no_ls_all == 0){  continue;  }
							else
							{ 
								$td1 = "<td>".$source."</td>";
								$td2 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalsource'>".$no_ls_all."</button></td>";
								$td3 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalsource'>".$no_bs_all."(".number_format((($no_bs_all/$no_ls_all)*100),2)."%)</button></td>";
								$td9 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='envconv' data-target='#myModalsource'>".$no_envs_all."(".number_format((($no_envs_all/$no_bs_all)*100),2)."%)</button></td>";
								$td4 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalsource'>".$no_fs_all."(".number_format((($no_fs_all/$no_ls_all)*100),2)."%)</button></td>";
								$td5 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalsource'>".$no_cs_all."(".number_format((($no_cs_all/$no_ls_all)*100),2)."%)</button></td>";
								$td6 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalsource'>".$no_os_all."(".number_format((($no_os_all/$no_ls_all)*100),2)."%)</button></td>";
								$td7 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalsource'>".$no_is_all."(".number_format((($no_is_all/$no_ls_all)*100),2)."%)</button></td>";
								$td8 = "<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-source='".$source."' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalsource'>".$no_ds_all."</button></td>";
							}
							$tr1_l = "</tr>";
							$str = $tr1.$td1.$td2.$td3.$td9.$td4.$td5.$td6.$td7.$td8.$tr1_l;
							$src_data3[] = array('tr'=>$str);
						}
						$tr2 = "<tr class='avoid-sortsrc3' style='display:none'>";
						$td1 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>Total</td>";
						$td2 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalsource'>".$no_ls_all_total."</button></td>";
						$td3 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalsource'>".$no_bs_all_total."(".number_format((($no_bs_all_total/$no_ls_all_total)*100),2)."%)</button></td>";
						$td9 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='envconv' data-target='#myModalsource'>".$no_envs_all_total."(".number_format((($no_envs_all_total/$no_bs_all_total)*100),2)."%)</button></td>";
						$td4 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalsource'>".$no_fs_all_total."(".number_format((($no_fs_all_total/$no_ls_all_total)*100),2)."%)</button></td>";
						$td5 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalsource'>".$no_cs_all_total."(".number_format((($no_cs_all_total/$no_ls_all_total)*100),2)."%)</button></td>";
						$td6 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalsource'>".$no_os_all_total."(".number_format((($no_os_all_total/$no_ls_all_total)*100),2)."%)</button></td>";
						$td7 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='idle' data-target='#myModalsource'>".$no_is_all_total."(".number_format((($no_is_all_total/$no_ls_all_total)*100),2)."%)</button></td>";
						$td8 = "<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalsource'>".$no_ds_all_total."</button></td>";
						$str = $tr2.$td1.$td2.$td3.$td9.$td4.$td5.$td6.$td7.$td8.$tr1_l;
						$src_data3[] = array('tr'=>$str);
						
						$result['src_table3'] = $src_data3;
						$result['count'] = $sources_arr['all']['total']['no_ls_all_total'];
						echo json_encode($result);
						//echo $result;
						//die;
						
						break;
						
	case "#NonConversion" : 
						//$tr1 = "<tr style='background-color:#E0F2F1;'><td colspan='3' style='font-size:18px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cancelled Bookings</td></tr>";
	$cond_nc ='';
	$cond_nc = $cond_nc.($city == 'all' ? "" : "AND b.city='$city'");
	$cond_nc = $cond_nc.($crm_person == 'all' ? "" : "AND b.crm_update_id='$crm_person'");
	$cond_nc = $cond_nc.($bservice_type == 'all' ? "" : "AND b.service_type='$bservice_type'");
	$cond_nc = $cond_nc.($bsource == 'all' ? "" : "AND b.source='$bsource'");
	$cond_nc = $cond_nc.($bvehicle == 'all' ? "" : "AND b.vehicle_type='$bvehicle'");
	$cond_nc = $cond_nc.($bmaster_service == 'all' ? "" : "AND gs.master_service IN ('$bmaster_service')");

	$sql_conv = "SELECT id,activity FROM admin_activity_tbl WHERE flag='1' ORDER BY activity ASC";
	$res_conv = mysqli_query($conn,$sql_conv) or die(mysqli_error($conn));
	
	while($row_conv = mysqli_fetch_object($res_conv)){
		$act_id = $row_conv->id;
		$activity = $row_conv->activity;
		
		
		$nc_cancelled_count_2w = 0 ;
		$nc_cancelled_count_4w = 0 ;

			$sql_get_type = $city == "Chennai" ? "SELECT distinct b.booking_id,b.vehicle_type,b.flag_duplicate,gs.master_service FROM user_booking_tb as b  left join b2b.b2b_booking_tbl bb on b.booking_id=bb.gb_booking_id left join go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type WHERE b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.mec_id NOT IN(400001,200018,200379,400974)and b.flag='1' and b.activity_status='$act_id' and b.log BETWEEN '$start' AND '$end' {$cond_nc} group by b.booking_id" : "SELECT distinct b.vehicle_type,b.flag_duplicate,gs.master_service FROM user_booking_tb as b left join go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type left join b2b.b2b_booking_tbl bb on b.booking_id=bb.gb_booking_id WHERE b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.source != 'Sulekha Booking'  AND b.mec_id NOT IN(400001,200018,200379,400974) and b.flag='1' and b.activity_status='$act_id' and b.log  BETWEEN '$start' AND '$end' {$cond_nc}";
				//echo $sql_get_type;
			$res_get_type = mysqli_query($conn,$sql_get_type);
			
			while($row_get_type = mysqli_fetch_object($res_get_type)){
			//print_r($row_get_type);
			$vehicle_type = $row_get_type->vehicle_type;
			$flag_duplicate = $row_get_type->flag_duplicate;
			$flag_cancel = $row_get_type->flag;

			if($act_id == '26')
			{
				if($vehicle_type == '2w'){
					$nc_cancelled_count_2w=$nc_cancelled_count_2w+1;
				}
				else if($vehicle_type == '4w'){
					$nc_cancelled_count_4w=$nc_cancelled_count_4w+1;				
				}
			}
			else
			{
				if($vehicle_type == '2w'){
					
					$nc_cancelled_count_2w=$nc_cancelled_count_2w+1;
				}
				else if($vehicle_type == '4w'){
					$nc_cancelled_count_4w=$nc_cancelled_count_4w+1;				
				}
			}
		}
	$tr2 = "<tr>";
	$td1 = "<td style='text-align:left;width:50%;word-break: break-word;'>".$activity."</td>";
	$td2 = "<td style='text-align:left;width:25%;word-break: break-word;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-activity='".$act_id."' data-ncv='1' data-startdate='".$start."' data-enddate='".$end."' data-target='#myModalnonconv'>".$nc_cancelled_count_2w."</button></td>";
	$td3 = "<td style='text-align:left;width:25%;word-break: break-word;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-activity='".$act_id."' data-ncv='1' data-startdate='".$start."' data-enddate='".$end."' data-target='#myModalnonconv'>".$nc_cancelled_count_4w."</button></td>";
	$tr2_l = "</tr>";

	$str1 = $tr2.$td1.$td2.$td3.$tr2_l;
	$cancel_data[]=$str1;
	//echo $str1;
	}						
						$sql_conv = "SELECT id,activity FROM admin_activity_tbl WHERE flag='2' ORDER BY activity ASC";
						$res_conv = mysqli_query($conn,$sql_conv) or die(mysqli_error($conn));
						while($row_conv = mysqli_fetch_object($res_conv)){
							$activity_id = $row_conv->id;
							$activity = $row_conv->activity;
							 
							$nc_others_count_2w = 0;
							$nc_others_count_4w = 0;  

								$sql_get_type = $city == "Chennai" ? "SELECT distinct b.booking_id,b.vehicle_type,gs.master_service FROM user_booking_tb as b left join b2b.b2b_booking_tbl bb on b.booking_id=bb.gb_booking_id left join go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type WHERE b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.mec_id NOT IN(400001,200018,200379,400974) and b.activity_status='$activity_id' and b.log BETWEEN '$start' AND '$end' {$cond_nc} group by b.booking_id" : "SELECT distinct b.vehicle_type,b.flag_duplicate,gs.master_service FROM user_booking_tb as b left join b2b.b2b_booking_tbl bb on b.booking_id=bb.gb_booking_id left join go_axle_service_price_tbl gs on b.service_type=gs.service_type and b.vehicle_type=gs.type WHERE b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.source != 'Sulekha Booking' AND  b.mec_id NOT IN(400001,200018,200379,400974)and b.activity_status='$activity_id' and b.log  BETWEEN '$start' AND '$end' {$cond_nc} group by b.booking_id";
								 //echo $sql_get_type;
								//$sql_get_type = $city == "all" ? "SELECT vehicle_type FROM user_booking_tb WHERE booking_id='$others_booking_id' AND mec_id NOT IN(400001,200018,200379,400974) " : "SELECT vehicle_type FROM user_booking_tb WHERE booking_id='$others_booking_id' AND city='$city' AND mec_id NOT IN(400001,200018,200379,400974) ";
								$res_get_type = mysqli_query($conn,$sql_get_type) or die($mysqli_error($conn));
								
								while($row_get_type = mysqli_fetch_object($res_get_type)){
								$vehicle_type = $row_get_type->vehicle_type;

								if($vehicle_type == '2w'){
									$nc_others_count_2w=$nc_others_count_2w+1;
								}
								else if($vehicle_type == '4w'){
									$nc_others_count_4w=$nc_others_count_4w+1;				
								}  
							}
							$tr4 = "<tr>";
							$td4 = "<td style='text-align:left;width:50%;word-break: break-word;'>".$activity."</td>";
							$td5 = "<td style='text-align:left;width:25%;word-break: break-word;'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='2w' data-activity='".$activity_id."' data-ncv='2' data-startdate='".$start."' data-enddate='".$end."' data-target='#myModalnonconv'>".$nc_others_count_2w."</button></td>";
							$td6 = "<td style='text-align:left;width:25%;word-break: break-word;'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='4w' data-activity='".$activity_id."' data-ncv='2' data-startdate='".$start."' data-enddate='".$end."' data-target='#myModalnonconv'>".$nc_others_count_4w."</button></td>";
							$tr4_l = "</tr>";
							
							$str2 = $tr4.$td4.$td5.$td6.$tr4_l;
							$other_data[]=$str2;
						}
						$nonconv_data['cancel'] = $cancel_data;
						$nonconv_data['other'] = $other_data;
						//$result['count'] = $persons_arr['all']['total']['no_lp_total'];						
						echo json_encode($nonconv_data);
						//echo $cancel_data;
						break;
}

?>
<?php
include('../config.php');
$conn = db_connect3();

$shop_id = $_POST['shop_id'];

$sql_garage = "SELECT b2b_cover_image,b2b_shop_name,b2b_owner_name,b2b_vehicle_type,b2b_authorized,b2b_address1,b2b_address1,b2b_address2,b2b_address3,b2b_address4,b2b_address5,b2b_pincode,b2b_mobile_number_1,b2b_mobile_number_2,b2b_mobile_number_3,b2b_mobile_number_4,b2b_mobile_number_5,b2b_email_id_1,b2b_shop_size,b2b_lead_usr,b2b_renw_date FROM b2b_mec_tbl WHERE b2b_shop_id = '$shop_id' ";
$res_garage = mysqli_query($conn,$sql_garage);

while($row_garage = mysqli_fetch_assoc($res_garage)){
    $image = $row_garage['b2b_cover_image'];
    $shop_name = $row_garage['b2b_shop_name'];
    $owner_name = $row_garage['b2b_owner_name'];
    $vehicle_type = $row_garage['b2b_vehicle_type'];
    $address1 = $row_garage['b2b_address1'];
    $address2 = $row_garage['b2b_address2'];
    $address3 = $row_garage['b2b_address3'];
    $address4 = $row_garage['b2b_address4'];
    $address5 = $row_garage['b2b_address5'];
    $pincode = $row_garage['b2b_pincode'];
    $mobile1 = $row_garage['b2b_mobile_number_1'];
    $mobile2 = $row_garage['b2b_mobile_number_2'];
    $mobile3 = $row_garage['b2b_mobile_number_3'];
    $mobile4 = $row_garage['b2b_mobile_number_4'];
    $mobile5 = $row_garage['b2b_mobile_number_5'];
    $email = $row_garage['b2b_email_id_1'];
    $shop_size = $row_garage['b2b_shop_size'];
    $lead_user = $row_garage['b2b_lead_usr'];
    $renewal_date = $row_garage['b2b_renw_date'];
    $authorized = $row_garage['b2b_authorized'];
}
$addressline1 ='';
$addressline2 ='';
$addressline3 ='';

if($address1 != ''){
    $addressline1 = $addressline1.$address1;
}
if($address2 != ''){
    $addressline1 = $addressline1.','.$address2;
}
if($address3 != ''){
    $addressline2 = $addressline2.$address3;
}
if($address4 != ''){
    $addressline2 = $addressline2.','.$address4;
}
if($address5 != ''){
    $addressline3 = $addressline3.$address5;
}
if($pincode != ''){
    $addressline3 = $addressline3.','.$pincode.'.';
}
$mobile_number ='';

if($mobile1 != ''){
    $mobile_number = $mobile_number.$mobile1;
}
if($mobile2 != ''){
    $mobile_number = $mobile_number.','.$mobile2;
}
if($mobile3 != ''){
    $mobile_number = $mobile_number.$mobile3;
}
if($mobile4 != ''){
    $mobile_number = $mobile_number.','.$mobile4;
}
if($mobile5 != ''){
    $mobile_number = $mobile_number.$mobile5;
}

$today_date = strtotime($today);
$renew_date = strtotime($renewal_date);

if($lead_user == '0'){
  if($today_date > $renew_date){
    $membership = "Premium"; 
  }
  else{
    $membership = "Basic";
  }
}
else{
  $membership = "Basic";
}

$sql_credits = "SELECT b2b_credits FROM b2b_credits_tbl WHERE b2b_shop_id='$shop_id'";
$res_credits = mysqli_query($conn,$sql_credits);
$row_credits = mysqli_fetch_array($res_credits);
$credits = $row_credits['b2b_credits'];

if($image !=''){ ?><img src="<?php echo $image; ?>" width="250" height="150" class="image" caption="<?php echo $shop_name; ?>" /><?php }
    else{ ?><img src="images/image_not_found.png" width="250" height="150" class="image" caption="<?php echo $shop_name; ?>" /><?php } ?>
<h4><?php echo $shop_name." (".$vehicle_type.") "; ?> <?php if($authorized == '1'){ ?><img src="images/authorized.png" alt="authorized" width="25" height="25" /><?php } ?></h4>
<table id="shopdetails" class="table borderless">
    <tr><td><strong>Shop Id</strong></td><td><?php echo $shop_id; ?></td></tr>
    <tr><td><strong>Owner</strong></td><td><?php echo $owner_name; ?></td></tr>
    <tr><td><strong>Address</strong></td><?php if($addressline1!= '' ){ ?><td><?php echo $addressline1; ?></td> <?php } ?></tr>
    <?php if($addressline2!= '' ){ ?><tr><td></td><td><?php echo $addressline2; ?></td></tr> <?php } ?>
    <?php if($addressline2!= '' ){ ?><tr><td></td><td><?php echo $addressline3; ?></td></tr> <?php } ?>
    <?php if($mobile_number != '' ){ ?><tr><td><strong>Mobile</strong></td><td><?php echo $mobile_number; ?></td></tr><?php } ?>
    <?php if($email !=''){ ?><tr><td><strong>E-Mail</strong></td><td><?php echo $email; ?></td></tr><?php } ?>
    <?php if($shop_size !=''){ ?><tr><td><strong>ShopSize</strong></td><td><?php echo $shop_size; ?></td></tr><?php } ?>
    <tr><td><strong>MemberShip</strong></td><td><?php echo $membership; ?></td></tr>
    <tr><td><strong>Renewal Date</strong></td><td><?php echo date('d M Y h.i A', strtotime($renewal_date)); ?></td></tr>
    <tr><td><strong>Credit Balance</strong></td><td><?php echo $credits; ?></td></tr>
</table>

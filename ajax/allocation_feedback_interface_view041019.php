<?php
//error_reporting(E_ALL);
//ini_set('display_errors',1);
include("../config.php");
$conn = db_connect1();
session_start();
$today=date('Y-m-d');

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$super_flag = $_SESSION['super_flag'];

$city = $_POST['city'];
$amt_column=strtolower($city).'_amt';
$_SESSION['crm_city'] = $city;
?>

 <div class="tab" style="margin-top:25px; margin-left:50px;">
  <button class="tablinks" onclick="openTab(event, 'Bikes')" id="biketab">Bikes</button>
  <button class="tablinks" onclick="openTab(event, 'Cars')" id="cartab">Cars</button>
</div>
<div id="Bikes" class="tabcontent" style="margin-left:50px;">
      <div align="center" id="table">
     <div style="margin-top:20px;overflow-y:auto;">
    <form action="../bridge/allocate_feedback_servicetypes.php?id=bike" method="post" id="bikes_form">

      <table class="table table-striped table-hover" >
      <thead style="background-color: #B2DFDB;">
      <th>Service Type</th>
     
      <th>Alloted To</th>
      </thead>
      <tbody>
      
      <?php

        $column = ucwords($city).'_feedback_id';
        
        if(ucwords($city) == 'Chennai')
    {
      $sql_service = "SELECT id,".$column.",service_type  FROM feedback_allocation WHERE vehicle_type='2w' AND bridge_flag='0'  AND allocation_flag = '0' ";        
    }
    else
    {
      $sql_service = "SELECT id,".$column.",service_type FROM feedback_allocation WHERE vehicle_type='2w' AND bridge_flag='0'  AND allocation_flag = '0' ";        
    }
   // echo $sql_service;
        $res_service = mysqli_query($conn,$sql_service);
        while($row_service = mysqli_fetch_array($res_service)){
                    
            $crm_id = $row_service[$column];

           // $crm_id = $row_service['crm_id'];
            $service_id=$row_service['id'];
            ?>
            <tr>
            <td><?php echo $service = $row_service['service_type']; ?></td>
            
            <td><input type="text" id="<?php echo $service_id; ?>" name="<?php echo $service_id; ?>"/></td>
            </tr>

         <?php   }
      ?>
      

      </tbody>
     <!--  <input type="submit" class="btn btn-md" id="bikes_submit" style="background-color:#009688;font-size:16px;color:#F3E5F5;position:fixed;top:50%;right:20px;"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Update</input>-->

      </table>
     <input type="submit" class="btn btn-md" id="bikes_submit" style="background-color:#009688;font-size:16px;color:#F3E5F5;position:fixed;top:50%;right:20px;" value="Update"> 

  </form>
      </div>
      </div>
</div>


<div id="Cars" class="tabcontent" style="margin-left:50px;">
          <div align="center" id="table">
        <div style="margin-top:20px;overflow-y:auto;">
    <form action="../bridge/allocate_feedback_servicetypes.php?id=car" method="post" id="cars_form">

        <table class="table table-striped table-hover" >
        <thead style="background-color: #B2DFDB;">
        <th>Service Type</th>
        
        <th>Alloted To</th>
        </thead>

        <tbody>
        <?php
        $column = ucwords($city).'_feedback_id';
        $sql_service = "SELECT id,".$column.",service_type FROM feedback_allocation WHERE vehicle_type='4w' AND bridge_flag='0' ";
        $res_service = mysqli_query($conn,$sql_service);
        while($row_service = mysqli_fetch_array($res_service)){
          
            $crm_id = $row_service[$column];

            //$crm_id = $row_service['crm_id'];
            $service_id=$row_service['id'];           

            ?>
            <tr>
            <td><?php echo $service = $row_service['service_type']; ?></td>
            
            <td><input type="text"  id="<?php echo $service_id; ?>" name="<?php echo $service_id; ?>"/></td>
            </tr>
         <?php   }
      ?>
        </tbody>

        </table>
        <input type="submit" class="btn btn-md" id="cars_submit" style="background-color:#009688;font-size:16px;color:#F3E5F5;position:fixed;top:50%;right:20px;" value="Update"> 
       <!-- <input type="submit" class="btn btn-md" id="cars_submit" style="background-color:#009688;font-size:16px;color:#F3E5F5;position:fixed;top:50%;right:20px;"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Update</input>-->

    </form>
        </div>
        </div>
</div>


<?php include("persons.php");//to Update the crm persons in json file 
?>
<script>
var persons = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('text'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  prefetch: 'persons.json'
});
persons.initialize();


</script>
<script>
 <?php 
 $column = ucwords($city).'_feedback_id';
  $sql = "SELECT id,".$column." FROM feedback_allocation";
  $res = mysqli_query($conn,$sql);
  while($row = mysqli_fetch_array($res)){
    $s_id = $row['id'];
    $s_crm_id = $row[$column];
    ?>
    var elt = $("#<?php echo $s_id; ?>");
  elt.tagsinput({
  itemValue: 'value',
  itemText: 'text',
  typeaheadjs: {
    name: 'persons',
    displayKey: 'text',
    source: persons.ttAdapter()
  }
});
    <?php
    if($s_crm_id != ''){
    $people = explode(",", $s_crm_id);
       foreach($people as $person)
      {
        if(!preg_match('/^crm[0-9]{3,}$/',$person)){
          continue;
        }
        $sql_crm = "SELECT name FROM crm_admin WHERE crm_log_id='$person' ";
        $res_crm = mysqli_query($conn,$sql_crm);
        $row_crm = mysqli_fetch_object($res_crm);
        $crm_name = $row_crm->name;
        ?> 
       // console.log('<?php echo $crm_name; ?>');    
         elt.tagsinput('add', { "value": "<?php echo $person; ?>" , "text": "<?php echo $crm_name; ?>" });
        <?php
      }
    }
  }
  ?>
</script>

<script>
$(".bikes_submit").click(function( event ){
 // console.log("bike clicked");
  event.preventDefault();
  var input = $("input").val();
 // alert("bikes");
  if(input==''){
    alert("please enter assign atleast one person");
  }
});
</script>

<script>
function openTab(evt, vehicle) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(vehicle).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>
 <script>
$(document).ready(function(){
  document.getElementById("biketab").click();
});
</script> 
<?php
//echo "id:";echo $_POST['id_post'];
if(isset($_POST['id_post'])){
     $tabs = $_POST['id_post'];
      if($tabs =='car'){
        ?>
        <script>
            $(document).ready(function(){
            document.getElementById("cartab").click();
            });
        </script>
<?php }
else if($tabs =='bike'){
        ?>
        <script>
            $(document).ready(function(){
            document.getElementById("biketab").click();
            });
        </script>
<?php }
     else if($tabs =='null'){
        ?>
        <script>
          $(document).ready(function(){
          document.getElementById("biketab").click();
        });
        </script>
    <?php }
}

?>
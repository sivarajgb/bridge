<?php
include("../config.php");
$conn = db_connect1();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];


$city = $_POST['city'];
$veh = $_POST['veh'];
$shop_status = $_POST['status'];

$_SESSION['crm_city'] = $city;
$no = 0;
?>


  <div id="div1" style="width:100%;float:left;" align="center">
    <table class="table table-striped table-bordered tablesorter table-hover results" id="table" style="width: 60%;font-size:12px;">
    <thead style="background-color:#B2DFDB;align:center;display: table-header-group;">
    <th>No. <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>ShopName <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Localities <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
	<th>Status <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
	
    </thead>
    <tbody id="tbody">

    <?php
	$cond = '';
	$cond = $cond.($veh == "all" ? "" : "AND type='$veh'");
	$cond = $cond.($shop_status == "active" ? "AND status='0'" : "AND status='1'");
    $sql_garage = "SELECT DISTINCT mec_id,shop_name,address4,status,exception_stage,DATE_ADD(exception_date, INTERVAL 5 DAY) as exception_date FROM admin_mechanic_table as m inner join b2b.b2b_mec_tbl as bm on m.axle_id = bm.b2b_shop_id WHERE address5 = '$city'  AND axle_id >1000 AND type!='' AND type!='both' $cond";
    $res_garage = mysqli_query($conn,$sql_garage);

      // $list_credits = array();

    while($row_garage = mysqli_fetch_object($res_garage)){
		$exception_stage = '';
        $shop_id = $row_garage->mec_id;
        $shop_name = $row_garage->shop_name;
        $locality = $row_garage->address4;
        $status = $row_garage->status;
		$exception_stage = $row_garage->exception_stage;
		$booking_ids = "";
		if($exception_stage >= -1 && $exception_stage <= 3)
		{
			$limit = $exception_stage;
			if($limit == -1)
			{
				$limit = 3;
			}
			$exception_query = "SELECT f.booking_id FROM user_booking_tb b LEFT JOIN feedback_track f ON f.booking_id = b.booking_id WHERE b.exception_flag = 1 and b.mec_id = '$shop_id' ORDER BY f.crm_update_time DESC LIMIT $limit";
			$res_exception = mysqli_query($conn,$exception_query);
			$count = 0;
			$booking_ids = "";
			while($row_exception = mysqli_fetch_object($res_exception))
			{
				if($count>0)
				{
					$booking_ids = $booking_ids.", ";
				}
				$booking_ids = $booking_ids.$row_exception->booking_id;
				$count = $count + 1;
			}
		}
        $exception_date = date('d-M-Y H:i',strtotime($row_garage->exception_date));
       	switch($exception_stage){
			//case '1': $icon = '<i style="padding-left:5px;font-size:25px;color:FFBA00" title="1st Warning!"  class="fa fa-exclamation"></i>';break;
			case '1': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exception1.svg">';break;
			case '2': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exception2.svg">';break;
			case '3': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exception3.svg">';break;
			case '-1': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="Blocked!" src="/images/exceptionlock.svg">';break;
			default:$icon = '';
		}
		
        $vehicle_type = 'all';
        $unused_credits = 100;
		$enc_shop_id = base64_encode($shop_id);

        ?>
        <tr>
            <td><?php echo $no = $no+1; ?></td>
			<?php
				if($exception_stage == -1)
				{
				?>
					<td><a style="cursor:pointer;" data-toggle="modal" data-target="#myModal<?php echo $shop_id;?>"><?php echo  $shop_name.$icon;?></a>
					<div id="myModal<?php echo $shop_id;?>" class="modal fade" role="dialog">
					  <div class="modal-dialog" style="top:20%;">

						<!-- Modal content-->
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Alert!</h4>
						  </div>
						  <div class="modal-body">
							<p>Inactive due to Improper Usage of Axle. Blocked till <?php echo $exception_date;?></p>
							<br>
							<p>Booking IDs: <?php echo $booking_ids;?></p>
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						  </div>
						</div>

					  </div>
					</div>
					</td>
				<?php
				}
				else
				{
				?>
					<td><a href="service_types.php?mid=<?php echo $enc_shop_id;?>"><?php echo  $shop_name.$icon;?></a></td>
				<?php
				}
			?>
			<td><?php echo  $locality;?></td>
			<?php if($status==0){ ?>
			<td><span class="label label-success">Active</span></td>
			<?php } else {?>
			<td><span class="label label-danger">Inactive</span></td>
			<?php }?>
        </tr>
    
        <?php
    }
    //$list_credits1 = $list_credits;
    
    ?>

    </tbody>
    </table>
  </div>
<!-- search bar  -->
<script>
$(document).ready(function() {
  $(".search").keyup(function () {
    var searchTerm = $(".search").val();
    var listItem = $('.results tbody').children('tr');
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

  $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });

  $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','false');
  });

  $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','true');
  });
  var jobCount = $('.results tbody tr[visible="true"]').length;
    $('.counter').text(jobCount + ' shops');

  if(jobCount == '0') {$('.no-result').show();}
    else {$('.no-result').hide();
	}
 });
});
</script>


<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#table").tablesorter( {sortList: [[0,0], [1,0]]} );
    }
);
</script>
<?php
// error_reporting(E_ALL); ini_set('display_errors', 1);
include("../config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();
$today=date('Y-m-d');

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enc_date = base64_encode($startdate);
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$vehicle = $_POST['vehicle_type'];
$source = $_POST['source_type'];
$master = $_POST['master'];
$city = $_POST['city'];
//print_r($_POST);

$_SESSION['crm_city'] = $city;
$enc_veh = base64_encode($vehicle);
if($vehicle=='all')
{
	$cond = " AND(b.b2b_vehicle_type = '2w' or b.b2b_vehicle_type = '4w')";
	$cond1 = " AND(g.vehicle_type = '2w' or g.vehicle_type = '4w')";
}
else
{
	$cond = " AND b.b2b_vehicle_type = '$vehicle'";
	$cond1 = " AND g.vehicle_type = '$vehicle'";
}
if($source=='all')
{
	$conds.= "";
}
else if($source=='core')
{
	$conds.= " AND  g.source!='Re-Engagement Bookings'";
}
else if($source=='re')
{
	$conds.= " AND g.source='Re-Engagement Bookings'";
}

$total_veh = 0;
$total_credits =0;
$total_completed_s=0;
$total_goaxle_rate =0;
$total_end_s=0;
$total_goaxled =0;
$total_leads =0;
$totalbookings=array();
?>
<div style="clear:both;">
<div align="center" id = "table" style="width:47%; margin-left:20px;margin-right:20px;float:left;">
<table id="example1" class="table table-striped table-bordered tablesorter table-hover results" style="max-width:95%;">
<thead style="background-color: #D3D3D3;">
<?php if($master == "all"){ ?>
    <th style="text-align:center;">ServiceType</th>
<?php } 
else{ ?>
<th style="text-align:center;">MasterServiceType</th>
<?php } ?>
<th style="text-align:center;">GoAxles <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Credits <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Completed <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">GoAxle Rate <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">End Conv. <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
</thead>
<tbody id="tbody">
<?php


$sql_service_all = "SELECT master_service,type FROM go_axle_service_price_tbl ORDER BY master_service ASC";
					$res_service_all = mysqli_query($conn1,$sql_service_all) or die(mysqli_error($conn1));
					while($row_service_all = mysqli_fetch_object($res_service_all)){
						$master_service = $row_service_all->master_service;
						$master_service_arr['all'][$master_service]['leads'] = $master_service_arr['all'][$master_service]['goaxled'] = 0;	
					}
					$services_all = '';
					$sql_get_services_all = "SELECT service_type FROM go_axle_service_price_tbl";
					$res_get_services_all = mysqli_query($conn1,$sql_get_services_all);
					while($row_get_services_all = mysqli_fetch_array($res_get_services_all)){
						$service_type = $row_get_services_all['service_type'];
						if($services_all == ""){
							$services_all = "'".$row_get_services_all['service_type']."'";
						}
						else{
							$services_all = $services_all.",'".$row_get_services_all['service_type']."'";
						}
						$service_type_arr['all'][$service_type]['leads'] = $service_type_arr['all'][$service_type]['goaxled'] = 0;
					}
					
					$cond_s_all ='';
					$cond_s_all = $cond_s_all.($city == 'all' ? "" : "AND g.city='$city'");
					
					$sql_sr_all = "SELECT DISTINCT g.booking_id,g.vehicle_type,gs.master_service,g.service_type,g.flag,g.booking_status,g.flag_fo,g.log,g.activity_status,g.flag_unwntd FROM user_booking_tb as g LEFT JOIN go_axle_service_price_tbl gs ON gs.service_type = g.service_type AND gs.type = g.vehicle_type WHERE g.service_type IN($services_all) AND g.mec_id NOT IN(400001,200018,200379,400974)  AND g.service_type NOT IN('GoBumpr Tyre Fest') AND DATE(g.log) BETWEEN '$startdate' AND '$enddate' {$cond_s_all}{$conds}";
					//echo $sql_sr_all;
					$res_sr_all = mysqli_query($conn1,$sql_sr_all);
					
					$res_sr_all = mysqli_query($conn1,$sql_sr_all);
					while($row_sr_all = mysqli_fetch_object($res_sr_all)){

						$master_service = $row_sr_all->master_service;
						$service_type = $row_sr_all->service_type;
						$type = $row_sr_all->vehicle_type;
						$booking_flag_sra = $row_sr_all->flag;
						$status_sra = $row_sr_all->booking_status;
						$flag_fo_sra = $row_sr_all->flag_fo;
						$flag_unwntd_sra = $row_sr_all->flag_unwntd;
						$activity_status_sra = $row_sr_all->activity_status;

						if($flag_unwntd_sra != '1'){

							$master_service_arr['all'][$master_service]['leads']=$master_service_arr['all'][$master_service]['leads']+1;
							$service_type_arr['all'][$service_type]['leads']=$service_type_arr['all'][$service_type]['leads']+1;
							if($status_sra == 2 && $booking_flag_sra != '1'){
											$master_service_arr['all'][$master_service]['goaxled']=$master_service_arr['all'][$master_service]['goaxled']+1;
											$service_type_arr['all'][$service_type]['goaxled']=$service_type_arr['all'][$service_type]['goaxled']+1;
										}
						}

					}


switch($master){
    case 'all':   
    // $sql_st = "SELECT DISTINCT b2b_service_type FROM b2b_booking_tbl WHERE b2b_vehicle_type='$vehicle' AND DATE(b2b_log) BETWEEN '$startdate' and '$enddate' ORDER BY b2b_service_type ASC";
    // $res_st = mysqli_query($conn2,$sql_st);
    // while($row_st = mysqli_fetch_object($res_st)){
    //     $service_type = $row_st->b2b_service_type;

        $sql_booking = $city == "all" ? "SELECT b.b2b_service_type,COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' AND s.b2b_acpt_flag='1' )) THEN 1 ELSE 0 END ) AS completed_count FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id=g.booking_id  WHERE s.b2b_acpt_flag='1' AND b.b2b_swap_flag!='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' {$cond}{$conds} AND b.b2b_shop_id NOT IN (1014,1035,1670,1673) GROUP BY b.b2b_service_type" : "SELECT b.b2b_service_type,COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' AND s.b2b_acpt_flag='1' )) THEN 1 ELSE 0 END ) AS completed_count FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id  WHERE s.b2b_acpt_flag='1' AND b.b2b_swap_flag!='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' {$cond} {$conds}AND b.b2b_shop_id NOT IN (1014,1035,1670,1673) AND g.city='$city' AND g.service_type NOT IN('GoBumpr Tyre Fest') GROUP BY b.b2b_service_type";
        $res_booking = mysqli_query($conn2,$sql_booking);
		//echo $sql_booking;die;
		while($row_booking = mysqli_fetch_object($res_booking)){
			 $service_type_arr['all'][$row_booking->b2b_service_type]['bookings']=$row_booking;
		}
		
		foreach($service_type_arr['all'] as $st){

		if(isset($st['bookings']->b2b_service_type)){
			$service_type = $st['bookings']->b2b_service_type;
			$no_of_vehicles = $st['bookings']->bookings;
			if(isset($st['goaxled'])){
			$no_of_goaxled = $st['goaxled'];
			}
			$no_of_credits = $st['bookings']->amount/100;
			if(isset($st['leads'])){
			$no_of_leads = $st['leads'];
			} 

			$no_of_completed_s = $st['bookings']->completed_count >0 ? $st['bookings']->completed_count : 0 ;
			
			if($no_of_vehicles == '0' && $no_of_credits == '0'){
				continue;
			}
			$end_conversion_s = $no_of_vehicles!= '0' ? round(($no_of_completed_s/$no_of_vehicles)*100,1) : '0%';
			if(!isset($st['leads'])){
			$goaxle_rate = '-';
			}
			else{
			$goaxle_rate = $no_of_leads!= '0' ? round(($no_of_goaxled/$no_of_leads)*100,1).' %' : '0%';
			}
			$total_veh = $total_veh+$no_of_vehicles;
			$total_credits = $total_credits+$no_of_credits;
			$total_completed_s = $total_completed_s+$no_of_completed_s;
			$total_goaxled = $total_goaxled+$no_of_goaxled;
			$total_leads = $total_leads+$no_of_leads;
			$total_end_s = $total_veh!= '0' ? round(($total_completed_s/$total_veh)*100,1) : '0';
			$total_goaxle_rate = $total_goaxled!= '0' ? round(($total_goaxled/$total_leads)*100,1) : '0';        
        ?>
        <tr>
            <td><?php echo $service_type ; ?></td>
            <td style="text-align:center;"><?php echo $no_of_vehicles ; ?></td>
            <td style="text-align:center;"><?php echo $no_of_credits ; ?></td>
            <td style="text-align:center;"><?php echo $no_of_completed_s ; ?></td>
            <?php
			switch(true){
                case ($goaxle_rate >= 60): ?><td style="text-align:center;background-color:#00ff4e70;"><?php echo $goaxle_rate  ; ?></td> <?php break;
                case ($goaxle_rate <60 && $goaxle_rate>= 40): ?><td style="text-align:center;background-color:#ffa800ad;"><?php echo $goaxle_rate ; ?></td> <?php break;
                case ($goaxle_rate <40 && $goaxle_rate>= 30): ?><td style="text-align:center;background-color:#a25506a6;"><?php echo $goaxle_rate ; ?></td> <?php break;
                case ($goaxle_rate < 30): ?><td style="text-align:center;background-color:#ff2f009c;"><?php echo $goaxle_rate ; ?></td> <?php break;
               default: ?><td style="text-align:center;"><?php echo $goaxle_rate ; ?></td> <?php
            }
			
            switch(true){
                case ($end_conversion_s >= 70): ?><td style="text-align:center;background-color:#00ff4e70;"><?php echo $end_conversion_s.' %' ; ?></td> <?php break;
                case ($end_conversion_s <70 && $end_conversion_s>= 60): ?><td style="text-align:center;background-color:#ffa800ad;"><?php echo $end_conversion_s.' %' ; ?></td> <?php break;
                case ($end_conversion_s <60 && $end_conversion_s>= 40): ?><td style="text-align:center;background-color:#a25506a6;"><?php echo $end_conversion_s.' %' ; ?></td> <?php break;
                case ($end_conversion_s < 40): ?><td style="text-align:center;background-color:#ff2f009c;"><?php echo $end_conversion_s.' %' ; ?></td> <?php break;
               default: ?><td style="text-align:center;"><?php echo $end_conversion_s.' %' ; ?></td> <?php
            }
			
            ?>        
        </tr>
        <?php
        }
		}
		
        break;
    case 'master': 
 //    $sql_st = $city == "all" ? "SELECT DISTINCT g.master_service FROM go_bumpr.go_axle_service_price_tbl as g INNER JOIN b2b.b2b_booking_tbl as b ON (g.service_type=b.b2b_service_type AND g.type=b.b2b_vehicle_type) WHERE {$cond} AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ORDER BY g.master_service ASC" : "SELECT DISTINCT g.master_service FROM go_bumpr.go_axle_service_price_tbl as g INNER JOIN b2b.b2b_booking_tbl as b ON (g.service_type=b.b2b_service_type AND g.type=b.b2b_vehicle_type) LEFT JOIN go_bumpr.user_booking_tb as gb ON b.gb_booking_id=gb.booking_id WHERE {$cond} AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND gb.city='$city' ORDER BY g.master_service ASC";
 //    $res_st = mysqli_query($conn1,$sql_st);
	// $services = '';
	// while($row_st = mysqli_fetch_array($res_st)){
            
 //        $master_service = $row_st['master_service'];

 //        $sql_services = $vehicle == "all" ? "SELECT distinct service_type FROM go_axle_service_price_tbl WHERE master_service='$master_service' AND (type='2w' or type='4w')" : "SELECT distinct service_type FROM go_axle_service_price_tbl WHERE master_service='$master_service' AND type='$vehicle'";
 //        $res_services = mysqli_query($conn1,$sql_services);
 //        while($row_services=mysqli_fetch_array($res_services)){
 //            if($services == ""){
 //                $services = "'".$row_services['service_type']."'";
 //            }
 //            else{
 //                $services = $services.",'".$row_services['service_type']."'";
 //            }
 //        }
	// }
 //        // echo $services;

 //        $service_type = $row_st->b2b_service_type;
    	$master_arr=array();
        $sql_master="SELECT service_type,type, master_service FROM go_bumpr.go_axle_service_price_tbl";
        $res_master = mysqli_query($conn1,$sql_master);
        while($row=mysqli_fetch_object($res_master)){
        	$master_arr[$row->type][$row->service_type]=$row->master_service;
        }
        $new_master_service_arr=array();
        if($vehicle=='all'){
	        $sql_booking = $city == "all" ? "SELECT b.b2b_service_type,COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' AND s.b2b_acpt_flag='1' )) THEN 1 ELSE 0 END ) AS completed_count FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id=g.booking_id  WHERE s.b2b_acpt_flag='1' AND b.b2b_swap_flag!='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_vehicle_type='2w' AND b.b2b_shop_id NOT IN (1014,1035,1670,1673) {$cond}{$conds} GROUP BY b.b2b_service_type" : "SELECT b.b2b_service_type,COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' AND s.b2b_acpt_flag='1' )) THEN 1 ELSE 0 END ) AS completed_count FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id  WHERE s.b2b_acpt_flag='1' AND b.b2b_swap_flag!='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_vehicle_type='2w' AND b.b2b_shop_id NOT IN (1014,1035,1670,1673) AND g.city='$city' {$cond}{$conds} AND g.service_type NOT IN('GoBumpr Tyre Fest')  GROUP BY b.b2b_service_type";
	        //echo $sql_booking;
	        $res_booking = mysqli_query($conn2,$sql_booking);
	        while($row=mysqli_fetch_object($res_booking)){
	        	$master_service=$master_arr['2w'][$row->b2b_service_type];
	        	if(!array_key_exists($master_service,$new_master_service_arr)){
	        		$new_master_service_arr[$master_service]=array();	
	        	}
	        	$new_master_service_arr[$master_service]['bookings']+=$row->bookings;
	        	$new_master_service_arr[$master_service]['amount']+=$row->amount;
	        	$new_master_service_arr[$master_service]['completed']+=$row->completed_count;
	        				// var_dump($row_booking);

	        }
	        $sql_booking = $city == "all" ? "SELECT b.b2b_service_type,COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' AND s.b2b_acpt_flag='1' )) THEN 1 ELSE 0 END ) AS completed_count FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id=g.booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_swap_flag!='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_vehicle_type='4w' AND b.b2b_shop_id NOT IN (1014,1035,1670,1673) AND g.service_type NOT IN('GoBumpr Tyre Fest') {$cond} {$conds}GROUP BY b.b2b_service_type" : "SELECT b.b2b_service_type,COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' AND s.b2b_acpt_flag='1' )) THEN 1 ELSE 0 END ) AS completed_count FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_swap_flag!='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_vehicle_type='4w' {$cond}{$conds} AND b.b2b_shop_id NOT IN (1014,1035,1670,1673) AND g.city='$city' AND g.service_type NOT IN('GoBumpr Tyre Fest') GROUP BY b.b2b_service_type";
	        //echo $sql_booking;
	        $res_booking = mysqli_query($conn2,$sql_booking);
	        while($row=mysqli_fetch_object($res_booking)){
	        	$master_service=$master_arr['4w'][$row->b2b_service_type];
	        	if(!array_key_exists($master_service,$new_master_service_arr)){
	        		$new_master_service_arr[$master_service]=array();	
	        	}
	        	$new_master_service_arr[$master_service]['bookings']+=$row->bookings;
	        	$new_master_service_arr[$master_service]['amount']+=$row->amount;
	        	$new_master_service_arr[$master_service]['completed']+=$row->completed_count;
	        				// var_dump($row_booking);

	        }
	    }else{
	    	$sql_booking = $city == "all" ? "SELECT b.b2b_service_type,COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' AND s.b2b_acpt_flag='1' )) THEN 1 ELSE 0 END ) AS completed_count FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id=g.booking_id  WHERE s.b2b_acpt_flag='1' AND b.b2b_swap_flag!='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' {$cond}{$conds} AND b.b2b_shop_id NOT IN (1014,1035,1670,1673) AND g.service_type NOT IN('GoBumpr Tyre feof(handle)st') GROUP BY b.b2b_service_type" : "SELECT b.b2b_service_type,COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' AND s.b2b_acpt_flag='1' )) THEN 1 ELSE 0 END ) AS completed_count FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id  WHERE s.b2b_acpt_flag='1' AND b.b2b_swap_flag!='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' {$cond}{$conds} AND b.b2b_shop_id NOT IN (1014,1035,1670,1673) AND g.city='$city' AND g.service_type NOT IN('GoBumpr Tyre Fest') GROUP BY b.b2b_service_type";
	    	//echo $sql_booking;
	        $res_booking = mysqli_query($conn2,$sql_booking);
	        while($row=mysqli_fetch_object($res_booking)){
	        	$master_service=$master_arr[$vehicle][$row->b2b_service_type];
	        	if(!array_key_exists($master_service,$new_master_service_arr)){
	        		$new_master_service_arr[$master_service]=array();	
	        	}
	        	$new_master_service_arr[$master_service]['bookings']+=$row->bookings;
	        	$new_master_service_arr[$master_service]['amount']+=$row->amount;
	        	$new_master_service_arr[$master_service]['completed']+=$row->completed_count;
	    	}
	    }	
		foreach ($new_master_service_arr as $key => $value) {
			$row_booking=(object)[];
			$row_booking->master_service=$key;
			$row_booking->bookings=$value["bookings"];
			$row_booking->amount=$value["amount"];
			$row_booking->completed_count=$value["completed"];
			$master_service_arr['all'][$row_booking->master_service]['bookings']=(object)$row_booking;

		}

		foreach($master_service_arr['all'] as $ms){

			if(isset($ms['bookings']->bookings)){
			$master_service = $ms['bookings']->master_service;
			$no_of_vehicles = $ms['bookings']->bookings;
			if(isset($ms['leads'])){
			$no_of_leads = $ms['leads'];
			}
			$no_of_goaxled= $ms['goaxled'];
			$no_of_credits = $ms['bookings']->amount/100;
			$no_of_completed_s = $ms['bookings']->completed_count >0 ? $ms['bookings']->completed_count : 0;

			if($no_of_vehicles == '0' && $no_of_credits == '0'){
				continue;
			} 
			if(!(isset($ms['leads']))){
				$goaxle_rate = '-';
			}
			else{
			$goaxle_rate = $no_of_goaxled!= '0' ? floor(($no_of_goaxled/$no_of_leads)*100).' %' : '0%';
			}
			$end_conversion_s = $no_of_vehicles!= '0' ? round(($no_of_completed_s/$no_of_vehicles)*100,1) : '0%';
			$total_veh = $total_veh+$no_of_vehicles;
			$total_credits = $total_credits+$no_of_credits;
			$total_completed_s = $total_completed_s+$no_of_completed_s;
			$total_leads = $total_leads+$no_of_leads;
			$total_goaxled = $total_goaxled+$no_of_goaxled;
			$total_end_s = $total_veh!= '0' ? round(($total_completed_s/$total_veh)*100,1) : '0';
			$total_goaxle_rate = $total_leads!= '0' ? floor(($total_goaxled/$total_leads)*100) : '0';
        ?>
        <tr>
            <td><?php echo $master_service ; ?></td>
            <td style="text-align:center;"><?php if($no_of_vehicles == ''){ echo '0'; } else { echo $no_of_vehicles ;} ?></td>
            <td style="text-align:center;"><?php echo $no_of_credits ; ?></td>
            <td style="text-align:center;"><?php echo $no_of_completed_s ; ?></td>
            <?php
			switch(true){
                case ($goaxle_rate >= 60): ?><td style="text-align:center;background-color:#00ff4e70;"><?php echo $goaxle_rate ; ?></td> <?php break;
                case ($goaxle_rate <60 && $goaxle_rate>= 40): ?><td style="text-align:center;background-color:#ffa800ad;"><?php echo $goaxle_rate; ?></td> <?php break;
                case ($goaxle_rate <40 && $goaxle_rate>= 30): ?><td style="text-align:center;background-color:#a25506a6;"><?php echo $goaxle_rate; ?></td> <?php break;
                case ($goaxle_rate < 30): ?><td style="text-align:center;background-color:#ff2f009c;"><?php echo $goaxle_rate; ?></td> <?php break;
               default: ?><td style="text-align:center;"><?php echo $goaxle_rate; ?></td>  <?php
            }
            switch(true){
                case ($end_conversion_s >= 70): ?><td style="text-align:center;background-color:#00ff4e70;"><?php echo $end_conversion_s.' %' ; ?></td> <?php break;
                case ($end_conversion_s <70 && $end_conversion_s>= 60): ?><td style="text-align:center;background-color:#ffa800ad;"><?php echo $end_conversion_s.' %' ; ?></td> <?php break;
                case ($end_conversion_s <60 && $end_conversion_s>= 40): ?><td style="text-align:center;background-color:#a25506a6;"><?php echo $end_conversion_s.' %' ; ?></td> <?php break;
                case ($end_conversion_s < 40): ?><td style="text-align:center;background-color:#ff2f009c;"><?php echo $end_conversion_s.' %' ; ?></td> <?php break;
               default: ?><td style="text-align:center;"><?php echo $end_conversion_s.' %' ; ?></td>  <?php
            }
            ?>              
        </tr>
        <?php
        }
		}
    break;
}
?>
</tbody>
<tbody class="avoid-sort">
<tr>
<td style="text-align:center;">Total</td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_veh; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_credits; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_completed_s; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_goaxle_rate.' %'; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_end_s.' %'; ?></td>
</tr>
</tbody>
</table>
</div>
<div style="width:47%; margin-left:20px;margin-right:20px;float:left;">
<table id="example2" class="table table-striped table-bordered tablesorter table-hover results" style="max-width:95%;">
<thead style="background-color: #D3D3D3;">
<th style="text-align:center;">Alloted To</th>
<th style="text-align:center;">GoAxles <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Credits <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Completed <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">GoaxleRate <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">End Conv. <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
</thead>
<tbody id="tbody">
<?php
 $total_veh_pr = 0;
 $total_credits_pr = 0;
 $total_completed = 0;
 $total_end = 0;
 $total_goaxle =0;
 $total_leads=0;
 $total_goaxled =0;
 $total_bookings = array();
/* $sql_pr = "SELECT DISTINCT crm_log_id,name FROM crm_admin WHERE flag =1 ORDER BY name ASC";
$res_pr = mysqli_query($conn1,$sql_pr); 

while($row_pr = mysqli_fetch_object($res_pr)){
    $crm_person_id = $row_pr->crm_log_id;
    $crm_person_name = $row_pr->name;
	$crm_arr[$crm_person_id] = $crm_person_name;
}	
print_r($crm_arr); */

	
	$sql_crmid = "SELECT crm_log_id FROM crm_admin WHERE flag = 0 ORDER BY name ASC";
	$res_crmid = mysqli_query($conn1,$sql_crmid) or die(mysqli_error($conn));
	while($row_crmid = mysqli_fetch_object($res_crmid)){
		$crm_id = $row_crmid->crm_log_id;
		$crm_arr[$crm_id]['leads'] = $crm_arr[$crm_id]['goaxled'] = 0;
	}
	$cond_p ='';
	$cond_p = $cond_p.($city == 'all' ? "" : " AND g.city='$city'");
	
	$sql_user_bookings = "SELECT DISTINCT g.booking_id,g.flag,g.booking_status,g.crm_update_id,g.flag_unwntd FROM user_booking_tb as g LEFT JOIN localities as l ON g.locality = l.localities  WHERE g.mec_id NOT IN(400001,200018,200379,400974) AND g.service_type NOT IN('GoBumpr Tyre Fest') AND DATE(g.log) BETWEEN '$startdate' AND '$enddate' {$cond_p} {$conds}";
	//echo $sql_user_bookings;
	$res_user_bookings = mysqli_query($conn1,$sql_user_bookings);

	while($row_user_bookings = mysqli_fetch_object($res_user_bookings)){
		$booking_flag = $row_user_bookings->flag;
		$status = $row_user_bookings->booking_status;
		$flag_unwntd = $row_user_bookings->flag_unwntd;
		$crm_id = $row_user_bookings->crm_update_id;

		if($flag_unwntd != '1'){
			if(isset($crm_arr[$crm_id]['leads'])){
			 $crm_arr[$crm_id]['leads']=$crm_arr[$crm_id]['leads']+1;
			}
			if($status == '2' && $booking_flag != '1')
			{
				if(isset($crm_arr[$crm_id]['goaxled'])){
					$crm_arr[$crm_id]['goaxled']=$crm_arr[$crm_id]['goaxled']+1; 
				}
			}												
		} 
	} 
	//$sql_booking_pr = $city == "all" ? "SELECT crm.name,crm.crm_log_id,COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' )) THEN 1 ELSE 0 END ) AS completed_count FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id LEFT JOIN go_bumpr.crm_admin as crm ON g.crm_update_id = crm.crm_log_id WHERE s.b2b_acpt_flag='1' AND b.b2b_swap_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.service_type NOT IN('GoBumpr Tyre Fest') AND {$cond1} GROUP BY g.crm_update_id" : "SELECT crm.name,crm.crm_log_id,COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' )) THEN 1 ELSE 0 END ) AS completed_count FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id 	LEFT JOIN go_bumpr.crm_admin as crm ON g.crm_update_id = crm.crm_log_id  WHERE s.b2b_acpt_flag='1' AND b.b2b_swap_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.service_type NOT IN('GoBumpr Tyre Fest') {$cond_p} {$cond1}{$conds} GROUP BY g.crm_update_id";
	
	$sql_booking_pr="SELECT crm.name,crm.crm_log_id,COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' )) THEN 1 ELSE 0 END ) AS completed_count FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id LEFT JOIN go_bumpr.crm_admin as crm ON g.crm_update_id = crm.crm_log_id WHERE s.b2b_acpt_flag='1' AND b.b2b_swap_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.service_type NOT IN('GoBumpr Tyre Fest')  {$cond1} {$cond_p} {$conds} GROUP BY g.crm_update_id";
    //echo $sql_booking_pr;
    $res_booking_pr = mysqli_query($conn1,$sql_booking_pr);
	while($row_booking_pr = mysqli_fetch_object($res_booking_pr))
	{
		$crm_arr[$row_booking_pr->crm_log_id]['bookings'] = $row_booking_pr;
	}

	foreach($crm_arr as $crm)
	{
		if(isset($crm['bookings']->completed_count)){
		$crm_person_id = $crm['bookings']->crm_log_id;
		$crm_person_name = $crm['bookings']->name;
		$no_of_vehicles_pr = $crm['bookings']->bookings;
		$no_of_goaxled = $crm['goaxled'];
		if(isset($crm['leads'])){
		$no_of_leads = $crm['leads'];
		}
		$no_of_credits_pr = $crm['bookings']->amount/100;
	
    if($no_of_vehicles_pr == '0' && $no_of_credits_pr == '0'){
        continue;
    }

    $no_of_completed = $crm['bookings']->completed_count > 0 ? $crm['bookings']->completed_count : 0 ;
    $no_of_vehicles_pr = $crm['bookings']->bookings > 0 ? $crm['bookings']->bookings : 0 ;

    $end_conversion = $no_of_vehicles_pr!= '0' ? round(($no_of_completed/$no_of_vehicles_pr)*100,1) : '0%';
	if(!isset($crm['leads'])){	
		$goaxle_rate = '-'; 
	}
	else{
		$goaxle_rate = $no_of_leads!= '0' ? floor(($no_of_goaxled/$no_of_leads)*100).' %' : '0%'; 
	}
    $total_goaxled = $total_goaxled+$no_of_goaxled;
	$total_leads = $total_leads+$no_of_leads;
    $total_veh_pr = $total_veh_pr+$no_of_vehicles_pr;
    $total_credits_pr = $total_credits_pr+$no_of_credits_pr;
    $total_completed = $total_completed + $no_of_completed;
    $total_end = $total_veh_pr!= '0' ? round(($total_completed/$total_veh_pr)*100,1) : '0'; 
    $total_goaxle = $total_goaxled!= '0' ? floor(($total_goaxled/$total_leads)*100) : '0'; 

?>
<tr>
    <td id="<?php echo $crm_person_id; ?>"><?php echo $crm_person_name ; ?></td>
    <td style="text-align:center;"><?php echo $no_of_vehicles_pr ; ?></td>
    <td style="text-align:center;"><?php echo $no_of_credits_pr ; ?></td>
    <td style="text-align:center;"><?php echo $no_of_completed ; ?></td>
    <?php
	switch(true){
        case ($goaxle_rate >= 60): ?><td style="text-align:center;background-color:#00ff4e70;"><?php echo $goaxle_rate ; ?></td> <?php break;
        case ($goaxle_rate <60 && $goaxle_rate>= 40): ?><td style="text-align:center;background-color:#ffa800ad;"><?php echo $goaxle_rate; ?></td> <?php break;
        case ($goaxle_rate <40 && $goaxle_rate>= 30): ?><td style="text-align:center;background-color:#a25506a6;"><?php echo $goaxle_rate; ?></td> <?php break;
        case ($goaxle_rate < 30): ?><td style="text-align:center;background-color:#ff2f009c;"><?php echo $goaxle_rate; ?></td> <?php break;
        default: ?><td style="text-align:center;"><?php echo $goaxle_rate; ?></td>  <?php
    }
    switch(true){
        case ($end_conversion >= 70): ?><td style="text-align:center;background-color:#00ff4e70;"><?php echo $end_conversion.' %' ; ?></td> <?php break;
        case ($end_conversion <70 && $end_conversion>= 60): ?><td style="text-align:center;background-color:#ffa800ad;"><?php echo $end_conversion.' %' ; ?></td> <?php break;
        case ($end_conversion <60 && $end_conversion>= 40): ?><td style="text-align:center;background-color:#a25506a6;"><?php echo $end_conversion.' %' ; ?></td> <?php break;
        case ($end_conversion < 40): ?><td style="text-align:center;background-color:#ff2f009c;"><?php echo $end_conversion.' %' ; ?></td> <?php break;
        default: ?><td style="text-align:center;"><?php echo $end_conversion.' %' ; ?></td>  <?php
    }
    ?> 
</tr>
<?php
}

}

$trophy_start = date('Y-m-01 00:00:00',strtotime($startdate)); 
$trophy_end = date('Y-m-t 12:59:59',strtotime($startdate));

$counter_trophy =0;

$sql_trophy = $city == "all" ? "SELECT COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,g.crm_update_id as crm_id FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id  WHERE s.b2b_acpt_flag='1' AND b.b2b_swap_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$trophy_start' and '$trophy_end' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.crm_update_id NOT IN ('crm003','crm036','crm018') AND {$cond1} GROUP BY g.crm_update_id ORDER BY bookings DESC,amount DESC LIMIT 3" : "SELECT COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,g.crm_update_id as crm_id FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_swap_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$trophy_start' and '$trophy_end' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.crm_update_id NOT IN ('crm003','crm036','crm018') {$cond1}{$conds} {$cond_p} GROUP BY g.crm_update_id ORDER BY bookings DESC,amount DESC LIMIT 3"; 
//echo $sql_trophy;
$res_trophy = mysqli_query($conn1,$sql_trophy);
while($row_trophy = mysqli_fetch_array($res_trophy)){
    $crm_id = $row_trophy['crm_id'];
    $sql_name = "SELECT name FROM crm_admin WHERE crm_log_id='$crm_id'";
    $res_name = mysqli_query($conn1,$sql_name);
    $row_name = mysqli_fetch_array($res_name);
    $name = $row_name = $row_name['name'];
    ?>
    <script>
    $(document).ready(function() {
        //first
        var id = '<?php echo $crm_id; ?>';
       // console.log(id);
        var counter = '<?php  echo $counter_trophy; ?>';
        switch(counter){
            case '0':$("#<?php echo $crm_id; ?>").append('&nbsp;<i class="fa fa-trophy" aria-hidden="true" style="font-size:19px;color:#D4AF37;"></i>');break;
            case '1':$("#<?php echo $crm_id; ?>").append('&nbsp;<i class="fa fa-trophy" aria-hidden="true" style="font-size:19px;color:#A9A9A9;"></i>');break;
            case '2':$("#<?php echo $crm_id; ?>").append('&nbsp;<i class="fa fa-trophy" aria-hidden="true" style="font-size:19px;color:#b87333;"></i>');break;
        }    
    });
    </script>
    <?php
    $counter_trophy = $counter_trophy+1;
    if($counter_trophy == '3'){
        break;
    }
}

$counter_shield =0;

$sql_shield = $city == "all" ? "SELECT COUNT(b.b2b_booking_id) as bookings,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' )) THEN 1 ELSE 0 END ) AS end_count ,g.crm_update_id as crm_id FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id  WHERE s.b2b_acpt_flag='1' AND b.b2b_swap_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$trophy_start' and '$trophy_end' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.crm_update_id NOT IN ('crm003','crm036','crm018') AND g.service_type NOT IN('GoBumpr Tyre Fest') AND {$cond1} GROUP BY g.crm_update_id HAVING bookings > 24 " : "SELECT COUNT(b.b2b_booking_id) as bookings,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' )) THEN 1 ELSE 0 END ) AS end_count ,g.crm_update_id as crm_id FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_swap_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$trophy_start' and '$trophy_end' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.crm_update_id NOT IN ('crm003','crm036','crm018') AND g.service_type NOT IN('GoBumpr Tyre Fest') {$cond1}{$conds} {$cond_p} GROUP BY g.crm_update_id HAVING bookings > 24 "; 
//echo $sql_shield;
$res_shield = mysqli_query($conn1,$sql_shield);
if(mysqli_num_rows($res_shield)>0){
    while($row_shield = mysqli_fetch_array($res_shield)){
        $crm_id = $row_shield['crm_id'];
        $bookings = $row_shield['bookings'];
        $completed = $row_shield['end_count'];
        $endconv = round(($completed/$bookings)*100,1);

        $list[] = array("crm_id"=>$crm_id, "endconv"=> $endconv);
    }
    foreach ($list as $key => $row) {
        $val[$key] = $row['endconv'];
    }
    
    // Sort the data with volume descending, edition ascending
    // Add $data as the last parameter, to sort by the common key
    array_multisort($val, SORT_DESC, $list);
    //arsort($list);
    //var_dump($list);

    foreach($list as $row){
        $crm_id = $row['crm_id'];
        ?>
        <script>
        $(document).ready(function() {
            //first
            var id = '<?php echo $crm_id; ?>';
            //console.log(id);
            var counter = '<?php  echo $counter_shield; ?>';
            //console.log(counter);
            switch(counter){
                case '0':$("#<?php echo $crm_id; ?>").append('&nbsp;<i class="fa fa-space-shuttle" aria-hidden="true" style="font-size:19px;color:#D4AF37;"></i>');break;
                case '1':$("#<?php echo $crm_id; ?>").append('&nbsp;<i class="fa fa-space-shuttle" aria-hidden="true" style="font-size:19px;color:#A9A9A9;"></i>');break;
                case '2':$("#<?php echo $crm_id; ?>").append('&nbsp;<i class="fa fa-space-shuttle" aria-hidden="true" style="font-size:19px;color:#b87333;"></i>');break;
            }    
        });
        </script>
        <?php
        $counter_shield = $counter_shield+1;
        if($counter_shield == '3'){
            break;
        }
    }

}


?>

</tbody>
<tbody class="avoid-sort">
<tr>
<td style="text-align:center;">Total</td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_veh_pr; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_credits_pr; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_completed; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_goaxle.' %'; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_end.' %'; ?></td>
</tr>
</tbody>
</table>
</div>
</div>
<div style="width:90%; margin-left:20px;margin-left:60px;clear:both;">
<table id="example3" class="table table-striped table-bordered tablesorter table-hover results" style="max-width:95%;">
<thead style="background-color: #D3D3D3;">
<th style="text-align:center;">Service Center</th>
<th style="text-align:center;">Model</th>
<th style="text-align:center;">Leads/Credits</th>
<th style="text-align:center;">Locality</th>
<?php if($vehicle!="2w") { ?>
<th style="text-align:center;">VehiclesSent <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<?php } else { ?>
<th style="text-align:center;">RE Sent <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">NRE Sent <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<?php } ?>
<th style="text-align:center;">Credits <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Accepted <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Rejected <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Idle <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Completed <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">End Conv. <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
</thead>
<tbody id="tbody">
<?php
$total_swapped =0;
/* $sql_garage = $city == "all" ? "SELECT DISTINCT m.b2b_shop_id,m.b2b_shop_name,m.b2b_address4,c.b2b_credits,c.b2b_partner_flag,c.b2b_flag,gm.exception_stage FROM b2b_mec_tbl as m INNER JOIN b2b_credits_tbl as c ON m.b2b_shop_id = c.b2b_shop_id LEFT JOIN go_bumpr.admin_mechanic_table as gm ON m.b2b_shop_id = gm.axle_id " : "SELECT DISTINCT m.b2b_shop_id,m.b2b_shop_name,m.b2b_address4,c.b2b_credits,c.b2b_partner_flag,c.b2b_flag,gm.exception_stage FROM b2b_mec_tbl as m INNER JOIN b2b_credits_tbl as c ON m.b2b_shop_id = c.b2b_shop_id LEFT JOIN go_bumpr.admin_mechanic_table as gm ON m.b2b_shop_id = gm.axle_id WHERE m.b2b_address5='$city'";
$res_garage = mysqli_query($conn2,$sql_garage);
echo $sql_garage;
while($row_garage = mysqli_fetch_object($res_garage)){
    $shop_id = $row_garage->b2b_shop_id;
    $shop_name = $row_garage->b2b_shop_name;
    $locality = $row_garage->b2b_address4;
    $credits_left = $row_garage->b2b_credits;
    $premium = $row_garage->b2b_partner_flag;
    $b2b_flag = $row_garage->b2b_flag;
    $exception_stage = $row_garage->exception_stage;
	switch($exception_stage){
		//case '1': $icon = '<i style="padding-left:5px;font-size:25px;color:FFBA00" title="1st Warning!"  class="fa fa-exclamation"></i>';break;
		case '1': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="1st Warning!" src="/images/exception1.svg">';break;
		case '2': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="2nd Warning!" src="/images/exception2.svg">';break;
		case '3': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="3rd Warning!" src="/images/exception3.svg">';break;
		case '-1': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="Blocked!" src="/images/exceptionlock.svg">';break;
		default:$icon = '';
	}
    $sql_booking_garage = "SELECT DISTINCT b.b2b_booking_id,b.b2b_credit_amt,s.b2b_acpt_flag,s.b2b_deny_flag,g.service_status,b.b2b_check_in_report,b.b2b_source FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id WHERE b.b2b_swap_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND b.b2b_shop_id='$shop_id' AND g.vehicle_type='$vehicle'";
    $res_booking_garage = mysqli_query($conn1,$sql_booking_garage);
    
    $no_of_vehicles_garage = 0;
    $no_of_credits_garage = 0;
    $accepted = 0;
    $rejected = 0;
    $idle = 0;
    $no_of_completed_garage = 0;
    $end_conversion_garage=0;



    while($row_booking_garage = mysqli_fetch_array($res_booking_garage)){
        
        $acpt = $row_booking_garage['b2b_acpt_flag'];
        $deny = $row_booking_garage['b2b_deny_flag'];
        $credit = $row_booking_garage['b2b_credit_amt'];
        $service_status = $row_booking_garage['service_status'];
        $credit = $credit/100;
        $check_in_report = $row_booking_garage['b2b_check_in_report'];
        $b2b_source = $row_booking_garage['b2b_source'];
        
    
        $no_of_vehicles_garage = $no_of_vehicles_garage+1;

             
        if($acpt == '1'){
            $accepted=$accepted+1;
            $no_of_credits_garage = $no_of_credits_garage+$credit;
            if(($service_status == 'Completed' || $service_status == 'In Progress') || ($check_in_report=='1' && $b2b_source=='lead_convert')){
                $no_of_completed_garage = $no_of_completed_garage+1;
            }
        }
        if($deny == '1'){
            $rejected=$rejected+1;
        }
        if($acpt == '0' && $deny == '0'){
            $idle=$idle+1;
        }      

    }

    if($no_of_vehicles_garage == '0' && $no_of_credits_garage == '0'){
        continue;
    } */

	$total_veh_garage = $total_credits_garage = $total_accepted = $total_rejected = $total_idle = $total_completed_garage = $total_end_garage = $total_re= 0;
	$sql_booking_garage = $city == "all" ? "SELECT DISTINCT m.b2b_shop_id,m.b2b_shop_name, m.b2b_vehicle_type, m.b2b_address4,c.b2b_credits,c.b2b_partner_flag,c.b2b_flag,c.b2b_leads,c.b2b_re_leads,c.b2b_non_re_leads,m.b2b_new_flg,COUNT(CASE WHEN b.b2b_swap_flag != 1 THEN 1 END) AS vehicles_sent, COUNT(CASE WHEN (b.b2b_swap_flag !=1 AND b.brand = 'Royal Enfield') THEN 1 END) AS re_sent , SUM(IF(s.b2b_acpt_flag = 1 AND b.b2b_swap_flag != 1, b.b2b_credit_amt,0)) AS credits,COUNT(CASE WHEN (s.b2b_acpt_flag = 1 AND b.b2b_swap_flag != 1) THEN 1 END) AS accepted,COUNT(CASE WHEN (s.b2b_deny_flag = 1 AND b.b2b_swap_flag != 1) THEN 1 END) AS rejected,COUNT(CASE WHEN (s.b2b_deny_flag = 1 AND b.b2b_swap_flag = 1) THEN 1 END) AS swapped,COUNT(CASE WHEN (s.b2b_deny_flag = 0 AND s.b2b_acpt_flag = 0 AND b.b2b_swap_flag != 1) THEN 1 END) AS idle,SUM(IF(s.b2b_acpt_flag = 1 AND b.b2b_swap_flag!=1 AND g.exception_revoke!=1 AND ((g.service_status = 'Completed' OR g.service_status = 'In Progress') OR (b.b2b_check_in_report = '1')), 1, 0)) AS end_count FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id LEFT JOIN b2b_mec_tbl as m ON m.b2b_shop_id = b.b2b_shop_id LEFT JOIN b2b_credits_tbl as c ON m.b2b_shop_id = c.b2b_shop_id WHERE b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.service_type NOT IN('GoBumpr Tyre Fest') {$cond1} GROUP BY m.b2b_shop_id":"SELECT DISTINCT m.b2b_shop_id,m.b2b_shop_name,m.b2b_vehicle_type,m.b2b_address4,c.b2b_credits,c.b2b_partner_flag,c.b2b_flag,c.b2b_leads,c.b2b_re_leads,c.b2b_non_re_leads,m.b2b_new_flg,COUNT(CASE WHEN b.b2b_swap_flag != 1 THEN 1 END) AS vehicles_sent, COUNT(CASE WHEN (b.b2b_swap_flag !=1 AND b.brand = 'Royal Enfield') THEN 1 END) AS re_sent , SUM(IF(s.b2b_acpt_flag = 1 AND b.b2b_swap_flag != 1, b.b2b_credit_amt,0)) AS credits,COUNT(CASE WHEN (s.b2b_acpt_flag = 1 AND b.b2b_swap_flag != 1) THEN 1 END) AS accepted,COUNT(CASE WHEN (s.b2b_deny_flag = 1 AND b.b2b_swap_flag != 1) THEN 1 END) AS rejected,COUNT(CASE WHEN (s.b2b_deny_flag = 1 AND b.b2b_swap_flag = 1) THEN 1 END) AS swapped,COUNT(CASE WHEN (s.b2b_deny_flag = 0 AND s.b2b_acpt_flag = 0 AND b.b2b_swap_flag != 1) THEN 1 END) AS idle,SUM(IF(s.b2b_acpt_flag = 1 AND b.b2b_swap_flag!=1 AND g.exception_revoke!=1 AND ((g.service_status = 'Completed' OR g.service_status = 'In Progress') OR (b.b2b_check_in_report = '1')), 1, 0)) AS end_count FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id LEFT JOIN b2b_mec_tbl as m ON m.b2b_shop_id = b.b2b_shop_id LEFT JOIN b2b_credits_tbl as c ON m.b2b_shop_id = c.b2b_shop_id WHERE b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.service_type NOT IN('GoBumpr Tyre Fest') {$cond1}{$conds} AND m.b2b_address5='$city' GROUP BY m.b2b_shop_id ";
    $res_booking_garage = mysqli_query($conn2,$sql_booking_garage);
	//echo $sql_booking_garage;
	while($row_booking_garage = mysqli_fetch_object($res_booking_garage)){
		$exception_stage = '';
		$shop_id = $row_booking_garage->b2b_shop_id;
		$shop_name = $row_booking_garage->b2b_shop_name;
		$locality = $row_booking_garage->b2b_address4;
		$credits_left = $row_booking_garage->b2b_credits;
		$premium = $row_booking_garage->b2b_partner_flag;
		$b2b_flag = $row_booking_garage->b2b_flag;
		$b2b_new_flg = $row_booking_garage->b2b_new_flg;
		$leads=$row_booking_garage->b2b_leads;
		$re_leads=$row_booking_garage->b2b_re_leads;
		$non_re_leads=$row_booking_garage->b2b_non_re_leads;
		$booking_ids = "";
		$shop_vehicle_type=$row_booking_garage->b2b_vehicle_type;
		
		$total_re_leads=$total_re_leads+$re_leades;
		$total_non_re_leads=$total_non_re_leads+$non_re_leades;
		// if($exception_stage >= -1 && $exception_stage <= 3)
		// {
			// $limit = $exception_stage;
			// if($limit == -1)
			// {
				// $limit = 3;
			// }
			// $exception_query = "SELECT f.booking_id FROM user_booking_tb b LEFT JOIN feedback_track f ON f.booking_id = b.booking_id WHERE b.exception_flag = 1 and b.mec_id = '$gbpr_mec_id' ORDER BY f.crm_update_time DESC LIMIT $limit";
			// $res_exception = mysqli_query($conn1,$exception_query);
			// $count = 0;
			// $booking_ids = "";
			// while($row_exception = mysqli_fetch_object($res_exception))
			// {
				// if($count>0)
				// {
					// $booking_ids = $booking_ids.", ";
				// }
				// $booking_ids = $booking_ids.$row_exception->booking_id;
				// $count = $count + 1;
			// }
		// }
		// switch($exception_stage){
			// case '1': $icon = '<i style="padding-left:5px;font-size:25px;color:FFBA00" title="1st Warning!"  class="fa fa-exclamation"></i>';break;
			// case '1': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exception1.svg">';break;
			// case '2': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exception2.svg">';break;
			// case '3': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exception3.svg">';break;
			// case '-1': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exceptionlock.svg">';break;
			// default:$icon = '';
		// }
		$no_of_vehicles_garage = $row_booking_garage->vehicles_sent;
		$no_of_re=$row_booking_garage->re_sent;
		$no_of_nre=$no_of_vehicles_garage-$no_of_re;
		$no_of_credits_garage = $row_booking_garage->credits;
		$no_of_credits_garage = $no_of_credits_garage/100;
		$accepted = $row_booking_garage->accepted;
		$rejected = $row_booking_garage->rejected;
        $swapped = $row_booking_garage->swapped;
		$idle = $row_booking_garage->idle;
		$no_of_completed_garage = $row_booking_garage->end_count;
		
		$total_re_leads=$total_re_leads+$re_leades;
		$total_non_re_leads=$total_non_re_leads+$non_re_leades;
		
		
	$end_conversion_garage = $accepted!= '0' ? floor(($no_of_completed_garage/$accepted)*100) : '0'; 
    $total_veh_garage = $total_veh_garage+$no_of_vehicles_garage;
    $total_re+=$no_of_re;
    $total_credits_garage = $total_credits_garage+$no_of_credits_garage;
	$total_re_leads=$row_booking_garage->b2b_re_leads;
	$total_nre_leads=$row_booking_garage->b2b_non_re_leads;
	$total_re=$total_re+$no_of_re;
	$total_nre=$total_nre+$no_of_nre;
    $total_accepted = $total_accepted+$accepted;
    $total_rejected=$total_rejected+$rejected;
    $total_swapped=$total_swapped+$swapped;
    $total_idle = $total_idle+$idle;
    $total_completed_garage = $total_completed_garage+$no_of_completed_garage;
    $total_end_garage = $total_accepted!= '0' ? floor(($total_completed_garage/$total_accepted)*100) : '0'; 


?>
<tr>
<td><?php echo $shop_name; if($premium == 2){ ?>&nbsp;&nbsp;<img src="images/authorized.png" style="width:28px;" title="Premium Garage"> <?php } if($premium == 1 && $b2b_flag == 1){ ?>&nbsp;&nbsp;<i class="fa fa-archive" aria-hidden="true" title="Old Premium partner" style="font-size: 18px;"></i> <?php } if($premium == 1 && $b2b_flag != 1){ ?>&nbsp;&nbsp;<i class="fa fa-leaf" aria-hidden="true" title="Upcoming Premium Prospect" style="font-size: 18px;color:#59bf4d;"></i> <?php } ?></td>
	
	<?php if($b2b_new_flg==1){ ?>
		<td style="text-align: center;"><?php echo "Leads" ; ?></td>
		<?php if($vehicle=="2w"){ ?>
		<td style="text-align:center;"> <?php echo "RE-".$total_re_leads."<br>NRE-".$total_nre_leads; ?></td>
		<?php
		}else{?>
			<td style="text-align:center;"> <?php echo $leads; ?></td>
		<?php }
		?>
	<?php }else{ if($premium==1){ ?>
		<td style="text-align: center;"><?php echo "Premium 1.0" ; ?></td>
		<?php } elseif($premium==2){ ?>
		<td style="text-align: center;"><?php echo "Premium 2.0" ; ?></td>
		<?php } else{ ?>
		<td style="text-align: center;"><?php echo "Precredit" ; ?></td>
		<?php } ?>
		<td style="text-align:center;"> <?php echo $credits_left ; ?>
	<?php } ?>

    <td><?php echo $locality ; ?></td>
    <?php if($vehicle!="2w"){ ?>
    <td style="text-align:center;"> <?php echo $no_of_vehicles_garage ; ?>
    	</td>
    <?php } else{ ?>
    <td style="text-align:center;"> <?php echo $no_of_re ; ?>
    	</td>
    <td style="text-align:center;"> <?php echo $no_of_nre ; ?>
    	</td>
    <?php } ?>
    	<!-- echo $no_of_vehicles_garage ; }else{ 
    		
    		echo $no_of_re." - ".$no_of_nre;
    		}?>  -->
    <td style="text-align:center;"><?php echo $no_of_credits_garage ; ?></td>
    <td style="text-align:center;"><?php echo $accepted ; ?></td>
    <td style="text-align:center;"><?php echo $rejected ; ?> [<?php echo $swapped ; ?>]</td>
    <td style="text-align:center;"><?php echo $idle ; ?></td>
    <td style="text-align:center;"><?php echo $no_of_completed_garage ; ?></td>
    <?php
    switch(true){
        case ($end_conversion_garage >= 70): ?><td style="text-align:center;background-color:#00ff4e70;"><?php echo $end_conversion_garage.' %' ; ?></td> <?php break;
        case ($end_conversion_garage <70 && $end_conversion_garage>= 60): ?><td style="text-align:center;background-color:#ffa800ad;"><?php echo $end_conversion_garage.' %' ; ?></td> <?php break;
        case ($end_conversion_garage <60 && $end_conversion_garage>= 40): ?><td style="text-align:center;background-color:#a25506a6;"><?php echo $end_conversion_garage.' %' ; ?></td> <?php break;
        case ($end_conversion_garage < 40): ?><td style="text-align:center;background-color:#ff2f009c;"><?php echo $end_conversion_garage.' %' ; ?></td> <?php break;
        default: ?><td style="text-align:center;"><?php echo $end_conversion_garage.' %' ; ?></td>  <?php
    }
    ?>
</tr>
<?php
}
?>
</tbody>
<tbody class="avoid-sort">
<tr>
<td colspan="4" style="text-align:center;">Total</td>
<?php if($vehicle=="2w"){ ?>
	<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_re; ?></td>
	<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_veh_garage-$total_re; ?></td>
<?php } else{ ?>
	<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_veh_garage; ?></td>
<?php } ?>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_credits_garage; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_accepted; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_rejected." [".$total_swapped."]"; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_idle; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_completed_garage; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_end_garage.' %'; ?></td>
</tr>
</tbody>
</table>
</div>

<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example1").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[3,3],[0,0]]} );
    }
);
</script>
<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example2").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[3,3],[0,0]]} );
    }
);
</script>
<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example3").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[4,4],[0,0]]} );
    }
);
</script>
<?php


?>
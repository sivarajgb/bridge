<?php

include("../config.php");
$conn = db_connect3();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));


$no = 0;
?>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
.w3-blue{
    color: #000!important;
    background-color: #B2DFDB!important;
	}
	.form-group{
		padding-bottom:8px;
		padding-right:8px;
	}
</style>
<div id="division" style="overflow-x:scroll;width:99%;" align="center">
  <div id="div1" >
  <div class="w3-container" style="padding-left: 3.5px;padding-right: 3.5px;">
    <table class="w3-responsive w3-table-all w3-card-4 tablesorter results" id="table" style="font-size:12px;">
    <thead style="background-color:#B2DFDB;align:left;display: table-header-group;">
    <tr  class = "w3-blue">
	<th>No. <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>ShopName <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Owner Name <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Mobile <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Email <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Request Type</th>
    <th>Demo</th>
    <th>Trial</th>
    <th>Status <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Requirements <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
	<th></th><th></th>
	</tr>
    </thead>
    <tbody id="tbody">

    <?php

    $sql_garage = "SELECT * FROM b2b_axle_req_tbl WHERE b2b_flag=0";
    $res_garage = mysqli_query($conn,$sql_garage);

      // $list_credits = array();

    while($row_garage = mysqli_fetch_object($res_garage)){
        $b2b_id = $row_garage->b2b_id;
        $shop_name = $row_garage->b2b_shop_name;
        $owner_name = $row_garage->b2b_reg_name;
        $mobile = $row_garage->b2b_shop_phone;
        $email = $row_garage->b2b_reg_email;
        $address = $row_garage->b2b_shop_address;
        $city = $row_garage->b2b_city;
        $country = $row_garage->b2b_country;
        $requirements = $row_garage->b2b_comments;
        $premium = ($row_garage->b2b_premimum_req==1?'Yes':'No');
        $signup = ($row_garage->b2b_signup_req==1?'Yes':'No');
        $demo = ($row_garage->b2b_demo_req==1?'Yes':'No');
        $demo_sent_date = $row_garage->b2b_demo_sent_date;
        $trial = ($row_garage->b2b_trial_req==1?'Yes':'No');;
        $trial_exp_date = $row_garage->b2b_trial_expiry_date;
        $followup_date = $row_garage->b2b_followup_date;
        $status = ($row_garage->b2b_lead_status == '0' ? "New Lead" : ($row_garage->b2b_lead_status == '1' ? "Hot Lead" : ($row_garage->b2b_lead_status == '2' ? "Warm Lead" :"Cold Lead")));
        $admin_comments = $row_garage->b2b_admin_comments;
		$request_type = ($premium=='Yes' ? "premium" : "signup");
		$demo_status = $row_garage->b2b_demo_status;
		$trail_status = $row_garage->b2b_trail_status;

        ?>
        <tr>
            <td><?php echo $no = $no+1; ?></td>
            <td><strong><?php echo  $shop_name;?></strong><?php //echo $address; ?><?php //echo $city.' ,'.$country; ?></td>
            <td><?php echo  $owner_name;?></td>
			<td><?php echo  $mobile;?></td>
            <td><?php echo $email; ?></td>
            <td><?php echo ($request_type == 'signup' ? "<i class='fa fa-handshake-o' aria-hidden='true' style='font-size:24px;color: green!important;' title='Requested Partnership'></i>" : "<img src='premium.png' style='width:28px;' title='Requested Premium Partnership'/>" );?></td>
            <td><?php echo ($demo == 'Yes' ? "<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Requested Demo'></i>" : "<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Demo - Not Requested'></i>" );?></td>
            <td><?php echo ($trial == 'Yes' ? "<i class='fa fa-check-circle' aria-hidden='true' style='font-size:24px;color: green!important;' title='Requested Trial'></i>" : "<i class='fa fa-times-circle' aria-hidden='true' style='font-size:24px;color:red!important;' title='Trial Not Requested'></i>" );?></td>
            <td><?php echo $status; ?></td>
            <td><?php echo $requirements; ?></td>
			<td><p class="modal_data" id=<?php echo $b2b_id;?> data-demo="<?php echo $demo_status;?>" data-trail="<?php echo $trail_status;?>" data-comments="<?php echo $admin_comments;?>" data-email="<?php echo $email;?>" data-b2bid="<?php echo $b2b_id;?>" data-followup_date="<?php echo $followup_date ?>" data-status="<?php echo $status ?>" title="Edit Status"><i class="fa fa-pencil" aria-hidden="true" style="cursor:pointer;font-size:24px;"></i></p></td>
			<td><p><i data-b2bid="<?php echo $b2b_id;?>" class="fa fa-trash" aria-hidden="true" style="cursor:pointer;font-size:24px;" title="Delete"></i></p></td>
        </tr>
    
        <?php
    } ?>
    </tbody>
    </table>
	</div>
  </div>
  </div>
<!-- search bar  -->
<script>
$(document).ready(function() {
  $(".search").keyup(function () {
    var searchTerm = $(".search").val();
    var listItem = $('.results tbody').children('tr');
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

  $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });

  $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','false');
  });

  $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','true');
  });
  var jobCount = $('.results tbody tr[visible="true"]').length;
    $('.counter').text(jobCount + ' shops');

  if(jobCount == '0') {$('.no-result').show();}
    else {$('.no-result').hide();
	}
 });
});
</script>


<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#table").tablesorter( {sortList: [[0,0], [1,0]]} );
    }
);
</script>
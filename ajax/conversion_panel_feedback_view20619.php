<?php

include("../config.php");
$conn = db_connect1();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$super_flag = $_SESSION['super_flag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$starttime = date('H:i:s',strtotime($_POST['starttime']));
$endtime =  date('H:i:s',strtotime($_POST['endtime']));
$city = $_POST['city'];
$target = $_POST['target'];


$_SESSION['crm_city'] = $city; 
//$veh_type=$_POST['vehicle_type'];

$start = $startdate.' '.$starttime;
$end = $enddate.' '.$endtime;
?>
<?php

switch($target)
{
	case "#Service" : 
	$vehicle_arr=array("all","2w","4w");
	$i=1;
	$cond_sr_v="";
	$cond_sr="";
	foreach($vehicle_arr as $vehicle_type){
	$cond_sr_v="";
	$cond_sr="";
	$cond_sr = $cond_sr.($city == 'all' ? "" : "AND b.city='$city'");
	$cond_sr_v=$cond_sr_v.($vehicle_type == 'all' ? "" : " AND b.vehicle_type='$vehicle_type'");
	$sql_service="SELECT b.service_type as master_service,COUNT(CASE WHEN f.reservice_flag = '1' AND f.feedback_status = '1' THEN 1 ELSE NULL END) AS Reservice,
    COUNT(case when f.flag='0' then 1 else NULL end) as Goaxle,
    COUNT(CASE WHEN (f.feedback_status='0' or f.feedback_status='1' or f.feedback_status = '2' OR f.feedback_status = '3' OR f.feedback_status = '4') and f.flag='0' THEN 1 ELSE NULL END) AS EndConversion,
    COUNT(CASE WHEN (f.feedback_status = '11' OR f.feedback_status = '12' OR f.feedback_status = '21' OR f.feedback_status = '22' OR f.feedback_status = '31' OR f.feedback_status = '41' OR f.feedback_status = '51') and f.flag='0' THEN 1 ELSE NULL END) AS FollowUp,
    COUNT(CASE WHEN (f.feedback_status = '-2' OR f.feedback_status = '-3') and f.flag='0' THEN 1 ELSE NULL END) AS Others,
    COUNT(CASE WHEN f.feedback_status = '5' AND f.flag = '0' THEN 1 ELSE NULL END) AS Cancel FROM
    go_bumpr.user_booking_tb b  LEFT JOIN feedback_track f ON f.booking_id = b.booking_id WHERE b.mec_id NOT IN (400001 , 200018, 200379, 400974)
        AND f.shop_id NOT IN (1014 , 1035, 1670) AND b.user_id NOT IN (21816 , 41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884)
		{$cond_sr_v} AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND f.log BETWEEN '$start' AND '$end' {$cond_sr}  GROUP BY b.service_type";
		//echo $sql_service;
		$sql_ser_res=mysqli_query($conn,$sql_service);
		$tr_sr="";
		$total_cancel=$total_endconv=$total_followup=$total_goaxle=$total_others=$total_reservice=0;
		while($sql_ser_row=mysqli_fetch_assoc($sql_ser_res)){
			if($vehicle_type=="all"){
				$total_goaxle_count=$total_goaxle_count+$sql_ser_row[Goaxle];
			}
			$total_goaxle=$total_goaxle+$sql_ser_row[Goaxle];
			$total_endconv=$total_endconv+$sql_ser_row[EndConversion];
			$total_followup=$total_followup+$sql_ser_row[FollowUp];
			$total_others=$total_others+$sql_ser_row[Others];
			$total_cancel=$total_cancel+$sql_ser_row[Cancel];
			$total_reservice=$total_reservice+$sql_ser_row[Reservice];

				$tr_sr.="<tr>
							<td>".$sql_ser_row[master_service]."</td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$sql_ser_row[master_service]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Goaxle' data-target='#myModalservice'>".$sql_ser_row[Goaxle]."</td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$sql_ser_row[master_service]."' data-startdate='".$start."' data-enddate='".$end."' data-status='EndConversion' data-target='#myModalservice'>".$sql_ser_row[EndConversion]."(".floor(($sql_ser_row[EndConversion]/$sql_ser_row[Goaxle])*100)."%)</td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$sql_ser_row[master_service]."' data-startdate='".$start."' data-enddate='".$end."' data-status='FollowUp' data-target='#myModalservice'>".$sql_ser_row[FollowUp]."(".floor(($sql_ser_row[FollowUp]/$sql_ser_row[Goaxle])*100)."%)</td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$sql_ser_row[master_service]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Others' data-target='#myModalservice'>".$sql_ser_row[Others]."(".floor(($sql_ser_row[Others]/$sql_ser_row[Goaxle])*100)."%)</td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$sql_ser_row[master_service]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Cancel' data-target='#myModalservice'>".$sql_ser_row[Cancel]."(".floor(($sql_ser_row[Cancel]/$sql_ser_row[Goaxle])*100)."%)</td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$sql_ser_row[master_service]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Reservice' data-target='#myModalservice'>".$sql_ser_row[Reservice]."</td>
						</tr>";
		}	
					$tr_sr.="<tr class='avoid-sortSer".$i."' style='display:none'>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>Total</td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='lead' data-target='#myModalservice'>".$total_goaxle."</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='booking' data-target='#myModalservice'>".$total_endconv."(".floor(@($total_endconv/$total_goaxle)*100)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='followup' data-target='#myModalservice'>".$total_followup."(".floor(@($total_followup/$total_goaxle)*100)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='cancelled' data-target='#myModalservice'>".$total_others."(".floor(@($total_others/$total_others)*100)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='others' data-target='#myModalservice'>".$total_cancel."(".floor(@($total_cancel/$total_goaxle)*100)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='duplicate' data-target='#myModalservice'>".$total_reservice."</button></td>
					</tr>";
					//$str = $tr3.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$tr_3;

					$data[$i][]= array('tr'=>$tr_sr);
					$table="table".$i;
					$result[$table] = $data[$i];
					$i++;
	}
					$result['count'] = $total_goaxle_count;
					//print_r($result);
					echo json_encode($result);
					break;
	case "#Person" :
	$cond_pr = $cond_sr.($city == 'all' ? "" : "AND b.city='$city'");
					$sql_person="SELECT ca.name,ca.crm_log_id,COUNT(CASE WHEN f.reservice_flag = '1' AND f.feedback_status = '1' THEN 1 ELSE NULL END) AS Reservice,
					COUNT(case when f.flag='0' then 1 else NULL end) as Goaxle,
					COUNT(CASE WHEN (f.feedback_status='0' or f.feedback_status='1' or f.feedback_status = '2' OR f.feedback_status = '3' OR f.feedback_status = '4') and f.flag='0' THEN 1 ELSE NULL END) AS EndConversion,
					COUNT(CASE WHEN (f.feedback_status = '11' OR f.feedback_status = '12' OR f.feedback_status = '21' OR f.feedback_status = '22' OR f.feedback_status = '31' OR f.feedback_status = '41' OR f.feedback_status = '51') and f.flag='0' THEN 1 ELSE NULL END) AS FollowUp,
					COUNT(CASE WHEN (f.feedback_status = '-2' OR f.feedback_status = '-3') and f.flag='0' THEN 1 ELSE NULL END) AS Others,
					COUNT(CASE WHEN f.feedback_status = '5' AND f.flag = '0' THEN 1 ELSE NULL END) AS Cancel FROM go_bumpr.user_booking_tb b 
					LEFT JOIN feedback_track f ON f.booking_id = b.booking_id join crm_admin ca on ca.crm_log_id=f.crm_log_id 
					WHERE b.mec_id NOT IN (400001 , 200018, 200379, 400974) AND f.shop_id NOT IN (1014 , 1035, 1670) AND b.user_id 
					NOT IN (21816 , 41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884) AND b.service_type != 'IOCL Check-up' 
					AND b.source != 'Re-Engagement Bookings' AND f.log BETWEEN '$start' AND '$end' {$cond_pr}  GROUP BY ca.name";
					$sql_per_res=mysqli_query($conn,$sql_person);

					$total_cancel=$total_endconv=$total_followup=$total_goaxle=$total_others=$total_reservice=0;

					while($sql_per_row=mysqli_fetch_assoc($sql_per_res)){

						$total_goaxle=$total_goaxle+$sql_per_row[Goaxle];
						$total_endconv=$total_endconv+$sql_per_row[EndConversion];
						$total_followup=$total_followup+$sql_per_row[FollowUp];
						$total_others=$total_others+$sql_per_row[Others];
						$total_cancel=$total_cancel+$sql_per_row[Cancel];
						$total_reservice=$total_reservice+$sql_per_row[Reservice];
						
							$tr_pr.="<tr>
										<td style='text-align:center;font-size:13px;'>".$sql_per_row[name]."</td>
										<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$sql_per_row[crm_log_id]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Goaxle' data-target='#myModalperson'>".$sql_per_row[Goaxle]."</button></td>
										<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$sql_per_row[crm_log_id]."' data-startdate='".$start."' data-enddate='".$end."' data-status='EndConversion' data-target='#myModalperson'>".$sql_per_row[EndConversion]."(".floor(($sql_per_row[EndConversion]/$sql_per_row[Goaxle])*100)."%)</button></td>
										<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$sql_per_row[crm_log_id]."' data-startdate='".$start."' data-enddate='".$end."' data-status='FollowUp' data-target='#myModalperson'>".$sql_per_row[FollowUp]."(".floor(($sql_per_row[FollowUp]/$sql_per_row[Goaxle])*100)."%)</button></td>
										<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$sql_per_row[crm_log_id]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Others' data-target='#myModalperson'>".$sql_per_row[Others]."(".floor(($sql_per_row[Others]/$sql_per_row[Goaxle])*100)."%)</button></td>
										<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$sql_per_row[crm_log_id]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Cancel' data-target='#myModalperson'>".$sql_per_row[Cancel]."(".floor(($sql_per_row[Cancel]/$sql_per_row[Goaxle])*100)."%)</button></td>
										<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$sql_per_row[crm_log_id]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Reservice' data-target='#myModalperson'>".$sql_per_row[Reservice]."</button></td>
									</tr>";
					}
					
					$tr_pr.="<tr class = 'avoid-sortperson' style='display:none'>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>Total</td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Goaxle' data-target='#myModalperson'>".$total_goaxle."</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='EndConversion' data-target='#myModalperson'>".$total_endconv."(".floor(($total_endconv/$total_goaxle)*100)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='FollowUp' data-target='#myModalperson'>".$total_followup."(".floor(($total_followup/$total_goaxle)*100)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Others' data-target='#myModalperson'>".$total_others."(".floor(($total_others/$total_goaxle)*100)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Cancel' data-target='#myModalperson'>".$total_cancel."(".floor(($total_cancel/$total_goaxle)*100)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Reservice' data-target='#myModalperson'>".$total_reservice."</button></td>
					</tr>";
					//$str = $tr2.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$tr1_l;
					$person_data[] = array('tr'=>$tr_pr);
					$result['person_data'] = $person_data;
					echo json_encode($result);
					break; 
	case "#Source" :
	$vehicle_arr=array("all","2w","4w");
	$i=1;
	$cond_src_v="";
	$cond_src="";
	foreach($vehicle_arr as $vehicle_type){
	$cond_src_v="";
	$cond_src="";
	$cond_src = $cond_src.($city == 'all' ? "" : "AND b.city='$city'");
	$cond_src_v=$cond_src_v.($vehicle_type == 'all' ? "" : " AND b.vehicle_type='$vehicle_type'");

	$sql_source="SELECT us.user_source,COUNT(CASE WHEN f.reservice_flag = '1' AND f.feedback_status = '1' THEN 1 ELSE NULL END) AS Reservice,
    COUNT(case when f.flag='0' then 1 else NULL end) as Goaxle,
    COUNT(CASE WHEN (f.feedback_status='0' or f.feedback_status='1' or f.feedback_status = '2' OR f.feedback_status = '3' OR f.feedback_status = '4') and f.flag='0' THEN 1 ELSE NULL END) AS EndConversion,
    COUNT(CASE WHEN (f.feedback_status = '11' OR f.feedback_status = '12' OR f.feedback_status = '21' OR f.feedback_status = '22' OR f.feedback_status = '31' OR f.feedback_status = '41' OR f.feedback_status = '51') and f.flag='0' THEN 1 ELSE NULL END) AS FollowUp,
    COUNT(CASE WHEN (f.feedback_status = '-2' OR f.feedback_status = '-3') and f.flag='0' THEN 1 ELSE NULL END) AS Others,
    COUNT(CASE WHEN f.feedback_status = '5' AND f.flag = '0' THEN 1 ELSE NULL END) AS Cancel FROM go_bumpr.user_booking_tb b 
	LEFT JOIN feedback_track f ON f.booking_id = b.booking_id 
	left join user_source_tbl us on us.user_source=b.source WHERE b.mec_id NOT IN (400001 , 200018, 200379, 400974) AND f.shop_id NOT IN (1014 , 1035, 1670) 
	AND b.user_id NOT IN (21816 , 41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884) {$cond_src_v} AND b.service_type != 'IOCL Check-up'
	AND b.source != 'Re-Engagement Bookings' AND f.log BETWEEN '$start' AND '$end' {$cond_src}  GROUP BY us.user_source";
		//echo $sql_service;
		$sql_src_res=mysqli_query($conn,$sql_source);
		$tr_src="";
		$total_cancel=$total_endconv=$total_followup=$total_goaxle=$total_others=$total_reservice=0;
		while($sql_scr_row=mysqli_fetch_assoc($sql_src_res)){
			if($vehicle_type=="all"){
				$total_goaxle_count=$total_goaxle_count+$sql_scr_row[Goaxle];
			}
			$total_goaxle=$total_goaxle+$sql_scr_row[Goaxle];
			$total_endconv=$total_endconv+$sql_scr_row[EndConversion];
			$total_followup=$total_followup+$sql_scr_row[FollowUp];
			$total_others=$total_others+$sql_scr_row[Others];
			$total_cancel=$total_cancel+$sql_scr_row[Cancel];
			$total_reservice=$total_reservice+$sql_scr_row[Reservice];
			
				if($sql_scr_row[user_source]==""){
					$source="Book-Now";
				}else{
					$source=$sql_scr_row[user_source];
				}
				$tr_src.="<tr>
							<td style='text-align:center;font-size:13px;'>".$source."</td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='".$sql_scr_row[user_source]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Goaxle' data-target='#myModalsource'>".$sql_scr_row[Goaxle]."</button></td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='".$sql_scr_row[user_source]."' data-startdate='".$start."' data-enddate='".$end."' data-status='EndConversion' data-target='#myModalsource'>".$sql_scr_row[EndConversion]."(".floor(@($sql_scr_row[EndConversion]/$sql_scr_row[Goaxle])*100)."%)</button></td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='".$sql_scr_row[user_source]."' data-startdate='".$start."' data-enddate='".$end."' data-status='FollowUp' data-target='#myModalsource'>".$sql_scr_row[FollowUp]."(".floor(@($sql_scr_row[EndConversion]/$sql_scr_row[Goaxle])*100)."%)</button></td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='".$sql_scr_row[user_source]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Others' data-target='#myModalsource'>".$sql_scr_row[Others]."(".floor(@($sql_scr_row[EndConversion]/$sql_scr_row[Goaxle])*100)."%)</button></td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='".$sql_scr_row[user_source]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Cancel' data-target='#myModalsource'>".$sql_scr_row[Cancel]."(".floor(@($sql_scr_row[EndConversion]/$sql_scr_row[Goaxle])*100)."%)</button></td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='".$sql_scr_row[user_source]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Reservice' data-target='#myModalsource'>".$sql_scr_row[Reservice]."</button></td>
						</tr>";
		}	
					$tr_src.="<tr class='avoid-sortsrc".$i."' style='display:none'>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>Total</td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Goaxle' data-target='#myModalsource'>".$total_goaxle."</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='EndConversion' data-target='#myModalsource'>".$total_endconv."(".floor(@($total_endconv/$total_goaxle)*100)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='FollowUp' data-target='#myModalsource'>".$total_followup."(".floor(@($total_followup/$total_goaxle)*100)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Others' data-target='#myModalsource'>".$total_others."(".floor(@($total_others/$total_others)*100)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Cancel' data-target='#myModalsource'>".$total_cancel."(".floor(@($total_cancel/$total_goaxle)*100)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Reservice' data-target='#myModalsource'>".$total_reservice."</button></td>
					</tr>";
					//$str = $tr3.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$tr_3;

					$src_data[$i][]= array('tr'=>$tr_src);
					$table="src_table".$i;
					$result[$table] = $src_data[$i];
					$i++;
	}
						echo json_encode($result);
						
						break;
						
	case "#NonConversion" : 
	$cond_nc ='';
	$cond_nc = $cond_nc.($city == 'all' ? "" : "AND b.city='$city'");

	$sql_conv = "SELECT id,activity FROM admin_activity_tbl WHERE flag='1' ORDER BY activity ASC";
	$res_conv = mysqli_query($conn,$sql_conv) or die(mysqli_error($conn));
	
	while($row_conv = mysqli_fetch_object($res_conv)){
		$act_id = $row_conv->id;
		$activity = $row_conv->activity;
		
		
		$nc_cancelled_count_2w = 0 ;
		$nc_cancelled_count_4w = 0 ;

			$sql_get_type= "SELECT f.booking_id,b.vehicle_type,f.reservice_flag FROM user_booking_tb as b join feebback_track f on f.booking_id=b.booking_id 
			WHERE b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884)
			 AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.mec_id NOT IN(400001,200018,200379,400974)and b.flag='1' 
			 and b.activity_status='$act_id' and b.log BETWEEN '$start' AND '$end' {$cond_nc} ";
			$res_get_type = mysqli_query($conn,$sql_get_type);
			
			while($row_get_type = mysqli_fetch_object($res_get_type)){
			//print_r($row_get_type);
			$vehicle_type = $row_get_type->vehicle_type;
			$flag_duplicate = $row_get_type->flag_duplicate;
			$flag_cancel = $row_get_type->flag;

			if($act_id == '26')
			{
				if($vehicle_type == '2w'){
					$nc_cancelled_count_2w=$nc_cancelled_count_2w+1;
				}
				else if($vehicle_type == '4w'){
					$nc_cancelled_count_4w=$nc_cancelled_count_4w+1;				
				}
			}
			else
			{
				if($vehicle_type == '2w'){
					
					$nc_cancelled_count_2w=$nc_cancelled_count_2w+1;
				}
				else if($vehicle_type == '4w'){
					$nc_cancelled_count_4w=$nc_cancelled_count_4w+1;				
				}
			}
		}
	$tr2 = "<tr>";
	$td1 = "<td style='text-align:left;width:50%;word-break: break-word;'>".$activity."</td>";
	$td2 = "<td style='text-align:left;width:25%;word-break: break-word;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='2w' data-activity='".$act_id."' data-startdate='".$start."' data-enddate='".$end."' data-target='#myModalnonconv'>".$nc_cancelled_count_2w."</button></td>";
	$td3 = "<td style='text-align:left;width:25%;word-break: break-word;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='4w' data-activity='".$act_id."' data-startdate='".$start."' data-enddate='".$end."' data-target='#myModalnonconv'>".$nc_cancelled_count_4w."</button></td>";
	$tr2_l = "</tr>";

	$str1 = $tr2.$td1.$td2.$td3.$tr2_l;
	$cancel_data[]=$str1;
	//echo $str1;
	}						
						$sql_conv = "SELECT id,activity FROM admin_activity_tbl WHERE flag='2' ORDER BY activity ASC";
						$res_conv = mysqli_query($conn,$sql_conv) or die(mysqli_error($conn));
						while($row_conv = mysqli_fetch_object($res_conv)){
							$activity_id = $row_conv->id;
							$activity = $row_conv->activity;
							 
							$nc_others_count_2w = 0;
							$nc_others_count_4w = 0;  

								$sql_get_type = $city == "Chennai" ? "SELECT b.booking_id,b.vehicle_type FROM user_booking_tb as b WHERE b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.mec_id NOT IN(400001,200018,200379,400974) and b.activity_status='$activity_id' and b.log BETWEEN '$start' AND '$end' {$cond_nc}" : "SELECT b.vehicle_type,b.flag_duplicate FROM user_booking_tb as b WHERE b.user_id NOT IN(21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783,226,252884) AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND b.mec_id NOT IN(400001,200018,200379,400974)and b.activity_status='$activity_id' and b.log  BETWEEN '$start' AND '$end' {$cond_nc}";
								//$sql_get_type = $city == "all" ? "SELECT vehicle_type FROM user_booking_tb WHERE booking_id='$others_booking_id' AND mec_id NOT IN(400001,200018,200379,400974) " : "SELECT vehicle_type FROM user_booking_tb WHERE booking_id='$others_booking_id' AND city='$city' AND mec_id NOT IN(400001,200018,200379,400974) ";
								$res_get_type = mysqli_query($conn,$sql_get_type) or die($mysqli_error($conn));
								
								while($row_get_type = mysqli_fetch_object($res_get_type)){
								$vehicle_type = $row_get_type->vehicle_type;

								if($vehicle_type == '2w'){
									$nc_others_count_2w=$nc_others_count_2w+1;
								}
								else if($vehicle_type == '4w'){
									$nc_others_count_4w=$nc_others_count_4w+1;				
								}  
							}
							$tr4 = "<tr>";
							$td4 = "<td style='text-align:left;width:50%;word-break: break-word;'>".$activity."</td>";
							$td5 = "<td style='text-align:left;width:25%;word-break: break-word;'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='2w' data-activity='".$activity_id."' data-startdate='".$start."' data-enddate='".$end."' data-target='#myModalnonconv'>".$nc_others_count_2w."</button></td>";
							$td6 = "<td style='text-align:left;width:25%;word-break: break-word;'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='4w' data-activity='".$activity_id."' data-startdate='".$start."' data-enddate='".$end."' data-target='#myModalnonconv'>".$nc_others_count_4w."</button></td>";
							$tr4_l = "</tr>";
							
							$str2 = $tr4.$td4.$td5.$td6.$tr4_l;
							$other_data[]=$str2;
						}
						$nonconv_data['cancel'] = $cancel_data;
						$nonconv_data['other'] = $other_data;						
						echo json_encode($nonconv_data);
						break;
}


?>
<?php
header('Content-Type: application/json');
include '../DBController.php';
$conn= new DB_connection();
$from_date=date('Y-m-d',strtotime($_GET['startdate']));
$to_date=date('Y-m-d',strtotime($_GET['enddate']));
$vehicle_type=$_GET['vehicle'];
$city=$_GET['city'];
//$from_date="2019-01-01";
//$to_date="2019-01-31";

 $result=$conn->goaxle_report($from_date,$to_date,$vehicle_type,$city);
 $i=1;
 $n=mysqli_num_rows($result);
 if($n!=0){
	while($row=mysqli_fetch_assoc($result)){
		$repeat_percent=($row[RepeatCount])/($row[GoaxelCount])*100;
		if($row[NewCount]==""){
			$new_count="0";
		}else{
			$new_count=$row[NewCount];
		}
		if($row[RepeatCount]==""){
			$rept_count="0";
		}else{
			$rept_count=$row[RepeatCount];
		}
		
		$total_goaxle=$total_goaxle+$row[GoaxelCount];
		$total_new=$total_new+$new_count;
		$total_repeat=$total_repeat+$rept_count;
		
		$html.="<tr>
					<td>".$i."</td>
					<td>".date('d M Y',strtotime($row[date1]))."</td>";
					if($vehicle_type!="all"){
					$html.="<td>".$row[vehicle_type1]."</td>";
					}
					$html.="<td>".$row[GoaxelCount]."</td>
					<td class='new_user_count'><a class='user_count' data-count_date='".$row[date1]."' data-vehicle_type='".$row[vehicle_type1]."' data-page='new_count' data-toggle='modal' data-target='#myModal_user_booking'>".$new_count."</a></td>
					<td class='rept_user_count'><a class='user_count' data-count_date='".$row[date1]."' data-vehicle_type='".$row[vehicle_type1]."' data-page='rept_count' data-toggle='modal' data-target='#myModal_user_booking'>".$rept_count."</a></td>
					<td>".number_format($repeat_percent,2)."</td>
				</tr>";
		$i++;
	}
	$total_rept_per=($total_repeat/$total_goaxle)*100;
	$html.="<tr class='total_row'>";
				if($vehicle_type=="all"){
					$html.="<td colspan=2>Total</td>";
					}else{
						$html.="<td colspan=3>Total</td>";
					}
	
				$html.="<td>".$total_goaxle."</td>
				<td>".$total_new."</td>
				<td>".$total_repeat."</td>
				<td>".number_format($total_rept_per,2)."(%)</td>
				</tr>";
 }else{
	 $html.="<tr><td colspan=7>No Records Found...!</td></tr>";
 }
	echo $data1 = json_encode($html);
	exit;
 ?>

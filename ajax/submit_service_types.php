<?php
include("../config.php");
$conn = db_connect1();
session_start();

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$flag = $_SESSION['flag'];
$mec_id = $_POST['mid'];
$employee_shop_id = $_POST['employee_shop_id'];
$log = date('Y-m-d H-i-s');

//print_r($_POST);
$activated = array();
$de_activated = array();
$done = 0;

$select_sql = "select * from admin_service_type where mec_id='$mec_id'";
$select_exe = mysqli_query($conn, $select_sql);
$num = mysqli_num_rows($select_exe);
if ($num <= 0) {
	$select_type = "select type from admin_mechanic_table where mec_id='$mec_id'";
	$type_exe = mysqli_query($conn, $select_type);
	$type_row = mysqli_fetch_assoc($type_exe);
	$type = $type_row['type'];
	
	$insert_sql = "INSERT INTO admin_service_type(mec_id,type,flag,log)VALUES('$mec_id','$type','0','$log')";
	//echo $insert_sql;die;
	$insert_exc = mysqli_query($conn, $insert_sql);
}

if (array_key_exists("arr", $_POST)) {
	$v = $_POST['arr'];
	$service_type = array();
	$status = array();
	foreach ($v as $_service_type => $_status) {
		$service_type[] = $_service_type;
		$status[] = $_status;
	}
	$sql = "UPDATE admin_service_type SET ";
	for ($i = 0; $i < sizeof($service_type); $i++) {
		$sql .= "$service_type[$i] = '$status[$i]' ";
		
		if ($i <= sizeof($service_type) - 2) {
			$sql .= ", ";
		}
		
		if ($status[$i] == '1') {
			$activated[] = $service_type[$i];
		} else {
			$de_activated[] = $service_type[$i];
		}
	}
	$sql .= " WHERE mec_id='$mec_id'";
//echo $sql;
	$res = mysqli_query($conn, $sql) or die(mysqli_error($conn));
	if ($res) {
		$done = 1;
	}
}

if (array_key_exists("shop_status", $_POST)) {
	$shop_status = $_POST['shop_status'];
	if ($shop_status != '') {
		$sql = "UPDATE admin_mechanic_table SET status = '$shop_status', updated_by = '$crm_log_id' WHERE mec_id='$mec_id'";
		$res = mysqli_query($conn, $sql) or die(mysqli_error($conn));
		if ($shop_status == '1') {
			//$array_push($activated,"garage_activated");}
			$de_activated[] = "garage_deactivated";
		} else {
			$activated[] = "garage_activated";
		}
	}
	if ($res) {
		$done = 1;
	}
	
}

if (array_key_exists("blacklist_status", $_POST)) {
	$blacklist_status = $_POST['blacklist_status'];
	
	if ($blacklist_status == 1) {
		$b2b_flag = -5;//inactive
		$leila = 1;
	} else if ($blacklist_status == 0) {
		$b2b_flag = 1;//active
		$leila = 0;
	}
	
	if ($blacklist_status != '') {
		$sql1 = "UPDATE b2b.b2b_mec_tbl SET leila = '$leila' WHERE gobumpr_id='$mec_id'";
		$res = mysqli_query($conn, $sql1) or die(mysqli_error($conn));
		
		$sql = "UPDATE b2b.b2b_employee_tbl SET b2b_flag = '$b2b_flag' WHERE b2b_shop_id='$employee_shop_id'";
		// echo $sql;exit;
		$res = mysqli_query($conn, $sql) or die(mysqli_error($conn));
		if ($blacklist_status == '1') {
			//$array_push($activated,"garage_activated");}
			$de_activated[] = "garage_blacklisted";
		} else {
			$activated[] = "garage_unblacklisted";
		}
	}
	if ($res) {
		$done = 1;
	}
	
}

// print_r($activated);

$activate = implode(";", $activated);
$de_activate = implode(";", $de_activated);
if ($activate or $de_activate != '') {
	$sql_log = "INSERT INTO st_toggle_history(mec_id,log,crm_id,activated,de_activated) VALUES ('$mec_id','$log','$crm_log_id','$activate','$de_activate')";
	
	$res = mysqli_query($conn, $sql_log) or die(mysqli_error($conn));
}

if ($done == 1) {
	echo "success";
}
?>

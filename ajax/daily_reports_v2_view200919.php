<?php
include("../config.php");
error_reporting(E_ALL); ini_set('display_errors', 1);
$conn = db_connect2();
$conn1 = db_connect1();
session_start();

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$master_service = $_GET['master_service'];
$service = $_GET['service'];
$person = $_GET['person'];
$source = $_GET['source'];
$today = date('Y-m-d');
$services = '';
//print_r($source);
$programatic = $_GET['programatic'];
$city = $_GET['city'];

$_SESSION['crm_city'] = $city;

$cond ='';

$cond = $cond.($person == 'all' ? "" : "AND g.crm_update_id='$person'");
$cond = $cond.($city == 'all' ? "" : "AND g.city='$city'");

if($source=='all')
{
	$cond.= "";
}
else if($source=='core')
{
	$cond.=" AND g.source!='Re-Engagement Bookings' and g.city='$city' ";
}
else if($source=='re')
{
	$cond.=" AND g.source='Re-Engagement Bookings' and g.city='$city' ";
}
if($master_service != "all" && $service == "all"){
    //echo " entered";
    $sql_serv = "SELECT service_type FROM go_axle_service_price_tbl WHERE bridge_flag='0' AND master_service='$master_service'";
    $res_serv = mysqli_query($conn1,$sql_serv);
    $services = '';
    while($row_serv = mysqli_fetch_array($res_serv)){
        if($services == ""){
            $services = $row_serv['service_type']."'";
        }
        else{
            $services = $services.",'".$row_serv['service_type']."'";
        }
    }
    $cond = $cond." AND g.service_type IN ('$services)";
} 

$cond = $cond.($service != 'all' ?  " AND g.service_type='$service'" : '' ) ;

$cond = $cond.($programatic == 'yes' ? "" : " AND g.crm_update_id NOT IN ('crm003','crm036','crm018')");

$start = strtotime($startdate);
$end = strtotime($enddate);
$noofdays = date('t',strtotime($start));

$str = '';
$total_working_days = 23;
switch($city){
    case 'Chennai' :$car_cpd = 111; //car credits per day
                    $bike_cpd = 85;  //bike credits per day
                    break;
    case 'Bangalore' :$car_cpd = 53; //car credits per day
                    $bike_cpd = 48;  //bike credits per day
                    break;
    case 'all' :$car_cpd = 164; //car credits per day
                    $bike_cpd = 133;  //bike credits per day
                    break;
    default : $car_cpd = 0; //car credits per day
                $bike_cpd = 0;  //bike credits per day
}
for ( $i = $start; $i <= $end; $i = $i + 86400 ) {
    $thisDate = date('d M Y', $i);
    $day = date('l', strtotime($thisDate));

    $date = date('Y-m-d', $i);
    $enc_date=base64_encode($date);
    $enc_2w = base64_encode('2w');
    $enc_4w = base64_encode('4w');
	$datesArr[] = $date;
}
$dates = join("','",$datesArr);
$sql_b2b = "SELECT DISTINCT b.gb_booking_id,b.b2b_vehicle_type,b.b2b_credit_amt,b.b2b_log,g.log,g.service_status,b.b2b_check_in_report,b.b2b_vehicle_at_garage,b.b2b_vehicle_ready FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id=g.booking_id  WHERE s.b2b_acpt_flag='1' AND b.b2b_swap_flag!='1' AND b.gb_booking_id!='0' AND (DATE(b.b2b_log) IN ('$dates')) AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.service_type NOT IN('GoBumpr Tyre Fest') {$cond}  ORDER BY g.log"; 
//echo $sql_b2b;
$res_b2b = mysqli_query($conn,$sql_b2b);

$sql_booking = "SELECT g.booking_id,g.vehicle_type,g.log FROM user_booking_tb as g WHERE (DATE(g.log) IN ('$dates')) AND g.mec_id NOT IN ('200018','200360','200393','400001','400974') AND g.user_id NOT IN(21816,41317,859,3132,20666,2792,128,19,7176,19470,1) AND g.service_type NOT IN('GoBumpr Tyre Fest') {$cond} AND g.flag_unwntd!='1'";  
//echo $sql_booking;  
$res_booking = mysqli_query($conn1,$sql_booking);

$dateData = [];
while($row_b2b = mysqli_fetch_object($res_b2b))
{
	$log = date('Y-m-d', strtotime($row_b2b->b2b_log));
	$veh_type = $row_b2b->b2b_vehicle_type;
	$dateData[$log]['veh_type'][] = $veh_type;
	$creditamount = $row_b2b->b2b_credit_amt;
	$dateData[$log]['creditamount'][] = $creditamount;
	$gb_booking_id = $row_b2b->gb_booking_id;
	$dateData[$log]['gb_booking_id'][] = $gb_booking_id;
	$service_status = $row_b2b->service_status;
	$dateData[$log]['service_status'][] = $service_status;
	$b2b_check_in_report = $row_b2b->b2b_check_in_report;
	$dateData[$log]['check_in_stage'][] = $b2b_check_in_report;
	$b2b_vehicle_at_garage = $row_b2b->b2b_vehicle_at_garage; 
	$dateData[$log]['at_garage_stage'][] = $b2b_vehicle_at_garage;
	$b2b_vehicle_ready = $row_b2b->b2b_vehicle_ready;
	$dateData[$log]['ready_stage'][] = $b2b_vehicle_ready;
	$log = $row_b2b->b2b_log;
}

$dateData2 = [];
while($row_booking = mysqli_fetch_object($res_booking))
{
	$log = date('Y-m-d', strtotime($row_booking->log));
	$vehicle_type = $row_booking->vehicle_type;
	$dateData2[$log]['vehicle_type'][] = $vehicle_type;
	$booking_id = $row_booking->booking_id;
	$dateData2[$log]['booking_id'][] = $booking_id;
}
$carsum =0 ;$carcount=0;$caravg=0;$carbookings = 0;$cartotalgoaxles = 0;$carconverted = 0;
$bikesum =0 ;$bikecount=0;$bikeavg=0;$bikebookings = 0;$biketotalgoaxles = 0;$bikeconverted = 0;
$totalgoaxles =0;$thismonthgoaxles = 0;
$dates_completed = 0;
for ( $i = $start; $i <= $end; $i = $i + 86400 ) {
	$dates_completed = $dates_completed + 1;
	$bike = 0;
    $car = 0;
    $total = 0 ;
	$carbookings = 0;$cargoaxles = 0;$carconverted = 0;
	$bikebookings = 0;$bikegoaxles = 0;$bikeconverted = 0;
	$totalbookings =0;$totalgoaxles =0;
	$thisDate = date('d M Y', $i);
    $day = date('l', strtotime($thisDate));

    $date = date('Y-m-d', $i);
	$enc_date=base64_encode($date);
    $enc_2w = base64_encode('2w');
    $enc_4w = base64_encode('4w');
	// print_r($dateData);
	$carcount = $carcount+1;$bikecount = $bikecount+1;
	if(count($dateData[$date]['veh_type'])>0)
	{
		for($k = 0;$k<count($dateData[$date]['veh_type']);$k++)
		{
			switch($dateData[$date]['veh_type'][$k]){
				case '2w': $bike = $bike+$dateData[$date]['creditamount'][$k]; 
						   $bikegoaxles = $bikegoaxles + 1;
						   if(($dateData[$date]['service_status'][$k] == 'Completed' || $dateData[$date]['service_status'][$k] == 'In Progress') || ($dateData[$date]['check_in_stage'][$k] == 1 || $dateData[$date]['at_garage_stage'][$k] == 1 || $dateData[$date]['ready_stage'][$k] == 1))
						   {
							   $bikeconverted = $bikeconverted + 1;
						   }
						   break;
				case '4w': $car = $car+$dateData[$date]['creditamount'][$k]; 
						   $cargoaxles = $cargoaxles + 1;
						   if(($dateData[$date]['service_status'][$k] == 'Completed' || $dateData[$date]['service_status'][$k] == 'In Progress') || ($dateData[$date]['check_in_stage'][$k] == 1 || $dateData[$date]['at_garage_stage'][$k] == 1 || $dateData[$date]['ready_stage'][$k] == 1))
						   {
							   $carconverted = $carconverted + 1;
						   }
						   break;
			}//switch
		}
		for($k = 0;$k<count($dateData2[$date]['vehicle_type']);$k++){
			switch($dateData2[$date]['vehicle_type'][$k]){
				case '2w': $bikebookings = $bikebookings + 1;
						   break;
				case '4w': $carbookings = $carbookings + 1;
						   break;
			}//switch
		}
	}
	// if($bikebookings < $bikegoaxles)
	// {
		// $bikebookings = $bikebookings + ($bikegoaxles - $bikebookings);
	// }
	// if($carbookings < $cargoaxles)
	// {
		// $carbookings = $carbookings + ($cargoaxles - $carbookings);
	// }
	$bikecredits = $bike/100;
    $carcredits = $car/100;
	$carsum=$carsum+$carcredits;
	$bikesum=$bikesum+$bikecredits;
    $totalcredits = $bikecredits+$carcredits;
	$totalgoaxles = count($dateData[$date]['gb_booking_id']);	
	$totalconverted = $bikeconverted+$carconverted;
	$bikeconverted_percentage = round((($bikeconverted/$bikegoaxles)*100),2);
	$carconverted_percentage = round((($carconverted/$cargoaxles)*100),2);
	$totalconverted_percentage = round((($totalconverted/$totalgoaxles)*100),2);
	$totalbookings = $bikebookings+$carbookings;
	$bikegoaxle_percentage = round((($bikegoaxles/$bikebookings)*100),2);
	$cargoaxle_percentage = round((($cargoaxles/$carbookings)*100),2);
	$totalgoaxle_percentage = round((($totalgoaxles/$totalbookings)*100),2);
	
	if(is_nan($bikeconverted_percentage))
	{
		$bikeconverted_percentage = 0;
	}
	if(is_nan($carconverted_percentage))
	{
		$carconverted_percentage = 0;
	}
	if(is_nan($totalconverted_percentage))
	{
		$totalconverted_percentage = 0;
	}
	if(is_nan($bikegoaxle_percentage))
	{
		$bikegoaxle_percentage = 0;
	}
	if(is_nan($cargoaxle_percentage))
	{
		$cargoaxle_percentage = 0;
	}
	if(is_nan($totalgoaxle_percentage))
	{
		$totalgoaxle_percentage = 0;
	}
	
    // if ($thisDate=='28 Apr 2018') {
    //     $carcredits=$carcredits+10;
    //     # code...
    // }
	
	$tr = '<tr>';
    $td1 = '<td style="text-align:center;">'.$thisDate.'</td>';
    $td2 = '<td style="text-align:center;">'.$day.'</td>';

    $total_cpd = $car_cpd+$bike_cpd;

    $car_120p = 120*($car_cpd/100);  // 120% of car credits
    $bike_120 = 120*($bike_cpd/100); // 120% of car credits
    $total_120 = $car_120p+$bike_120;
	
	$td3 = '<td style="text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.base64_encode(all).'&s='.base64_encode($source).'" >'.$totalgoaxles.'</a></td>';
	if($totalgoaxle_percentage > 110)
	{
		$td4 = '<td style="text-align:center;background-color:#7aa8ec;">'.$totalgoaxle_percentage.' %</td>';
	}
	elseif($totalgoaxle_percentage > 100 && $totalgoaxle_percentage < 110)
	{
		$td4 = '<td style="text-align:center;background-color:#7bde95;">'.$totalgoaxle_percentage.' %</td>';
	}
	elseif($totalgoaxle_percentage > 50)
	{
		$td4 = '<td style="text-align:center;background-color:#f3ae2b;">'.$totalgoaxle_percentage.' %</td>';
	}
	else
	{
		$td4 = '<td style="text-align:center;background-color:#e88c87;">'.$totalgoaxle_percentage.' %</td>';
	}
	$td5 = '<td style="text-align:center;">'.$totalconverted_percentage.' %</td>';
	$td6 = '<td style="text-align:center;">'.$totalcredits.'</td>';
	$tr_l = '</tr>';

    $str = $tr.$td1.$td2.$td3.$td5.$td6.$tr_l;
    // $str = $tr.$td1.$td2.$td3.$td4.$td5.$td6.$tr_l;
    $data1[] = array('tr'=>$str);
	
	
	$td2w_3 = '<td style="text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_2w.''&s='.base64_encode($source)." >'.$bikegoaxles.'</a></td>';
	if($bikegoaxle_percentage > 110)
	{
		$td2w_4 = '<td style="text-align:center;background-color:#7aa8ec;">'.$bikegoaxle_percentage.' %</td>';
	}
	elseif($bikegoaxle_percentage > 100 && $bikegoaxle_percentage < 110)
	{
		$td2w_4 = '<td style="text-align:center;background-color:#7bde95;">'.$bikegoaxle_percentage.' %</td>';
	}
	elseif($bikegoaxle_percentage > 50)
	{
		$td2w_4 = '<td style="text-align:center;background-color:#f3ae2b;">'.$bikegoaxle_percentage.' %</td>';
	}
	else
	{
		$td2w_4 = '<td style="text-align:center;background-color:#e88c87;">'.$bikegoaxle_percentage.' %</td>';
	}
	$td2w_5 = '<td style="text-align:center;">'.$bikeconverted_percentage.' %</td>';
	$td2w_6 = '<td style="text-align:center;">'.$bikecredits.'</td>';
	$str = $tr.$td1.$td2.$td2w_3.$td2w_5.$td2w_6.$tr_l;
	// $str = $tr.$td1.$td2.$td2w_3.$td2w_4.$td2w_5.$td2w_6.$tr_l;
    $data2w[] = array('tr'=>$str);
	
	
	$td4w_3 = '<td style="text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_4w.'&s='.base64_encode($source).'" >'.$cargoaxles.'</a></td>';
	if($cargoaxle_percentage > 110)
	{
		$td4w_4 = '<td style="text-align:center;background-color:#7aa8ec;">'.$cargoaxle_percentage.' %</td>';
	}
	elseif($cargoaxle_percentage > 100 && $cargoaxle_percentage < 110)
	{
		$td4w_4 = '<td style="text-align:center;background-color:#7bde95;">'.$cargoaxle_percentage.' %</td>';
	}
	elseif($cargoaxle_percentage > 50)
	{
		$td4w_4 = '<td style="text-align:center;background-color:#f3ae2b;">'.$cargoaxle_percentage.' %</td>';
	}
	else
	{
		$td4w_4 = '<td style="text-align:center;background-color:#e88c87;">'.$cargoaxle_percentage.' %</td>';
	}
	$td4w_5 = '<td style="text-align:center;">'.$carconverted_percentage.' %</td>';
	$td4w_6 = '<td style="text-align:center;">'.$carcredits.'</td>';
	$str = $tr.$td1.$td2.$td4w_3.$td4w_5.$td4w_6.$tr_l;
	// $str = $tr.$td1.$td2.$td4w_3.$td4w_4.$td4w_5.$td4w_6.$tr_l;
    $data4w[] = array('tr'=>$str);
	
	/*$td3 = '<td style="text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_4w.'" >'.$carcredits.'<br>[ '.$cargoaxles.' ]</a></td>';
    $td4 = '<td style="text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_2w.'" >'.$bikecredits.'<br>[ '.$bikegoaxles.' ]</a></td>'; 
    $td5 = '<td style="text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.base64_encode(all).'" >'.$totalcredits.'<br>[ '.$totalgoaxles.' ]</td>';*/
    /*$td6 = '<td style="text-align:center;">'.$carconverted_percentage.' %</td>';
	$td7 = '<td style="text-align:center;">'.$bikeconverted_percentage.' %</td>';
	if($totalconverted_percentage == 100)
	{
		$td8 = '<td style="text-align:center;background-color:#7bde95;">'.$totalconverted_percentage.' %</td>';
	}
	elseif($totalconverted_percentage > 60)
	{
		$td8 = '<td style="text-align:center;background-color:#f3ae2b;">'.$totalconverted_percentage.' %</td>';
	}
	else
	{
		$td8 = '<td style="text-align:center;background-color:#e88c87;">'.$totalconverted_percentage.' %</td>';
	}
	$td9 = '<td style="text-align:center;">'.$cargoaxle_percentage.' %</td>';
	$td10 = '<td style="text-align:center;">'.$bikegoaxle_percentage.' %</td>';
	if($totalgoaxle_percentage == 100)
	{
		$td11 = '<td style="text-align:center;background-color:#7bde95;">'.$totalgoaxle_percentage.' %</td>';
	}
	elseif($totalgoaxle_percentage > 50)
	{
		$td11 = '<td style="text-align:center;background-color:#f3ae2b;">'.$totalgoaxle_percentage.' %</td>';
	}
	else
	{
		$td11 = '<td style="text-align:center;background-color:#e88c87;">'.$totalgoaxle_percentage.' %</td>';
	}*/
	$cartotalgoaxles = $cartotalgoaxles + $cargoaxles;
	$biketotalgoaxles = $biketotalgoaxles + $bikegoaxles;
	$thismonthgoaxles = $cartotalgoaxles + $biketotalgoaxles;
}
$totalvehCount = $carcount + $bikecount;
$caravg = $carcount == 0 ? 0 : ceil($cartotalgoaxles/$dates_completed);
$bikeavg = $bikecount == 0 ? 0 : ceil($biketotalgoaxles/$dates_completed);
$totalavg = $totalvehCount == 0 ? 0 : ceil($thismonthgoaxles/$dates_completed);
$acheivedper_car = $car_cpd == 0 ? 0 : ceil(($cartotalgoaxles/($car_cpd*$total_working_days))*100);
$acheivedper_bike = $bike_cpd == 0 ? 0 : ceil(($biketotalgoaxles/($bike_cpd*$total_working_days))*100);
$acheivedper_total = $total_cpd == 0 ? 0 : ceil(($thismonthgoaxles/($total_cpd*$total_working_days))*100);
$carprocon = ceil($caravg*$total_working_days);
$bikeprocon = ceil($bikeavg*$total_working_days);
$cartarget = $car_cpd*$total_working_days;
$biketarget = $bike_cpd*$total_working_days;
$totaltarget = $cartarget + $biketarget;
$carachieved = $cartotalgoaxles." [".$acheivedper_car." %]";
$bikeachieved = $biketotalgoaxles." [".$acheivedper_bike." %]";
$totalachieved = $thismonthgoaxles." [".$acheivedper_total." %]";
$data2 = array('car_cpd'=>$car_cpd , 'bike_cpd'=>$bike_cpd , 'caravg'=>$caravg , 'bikeavg'=>$bikeavg , 'totalavg'=>$caravg+$bikeavg, 'carprocon'=>$carprocon , 'bikeprocon'=>$bikeprocon , 'totalprocon'=>$carprocon+$bikeprocon , 'cartarget'=>$cartarget , 'biketarget'=>$biketarget, 'totaltarget'=>$totaltarget , 'carachieved'=>$carachieved , 'bikeachieved'=> $bikeachieved , 'totalachieved'=> $totalachieved);

// Loop between timestamps, 24 hours at a time
/* for ( $i = $start; $i <= $end; $i = $i + 86400 ) {
    $thisDate = date('d M Y', $i);
    $day = date('l', strtotime($thisDate));

    $date = date('Y-m-d', $i);

    $enc_date=base64_encode($date);
    $enc_2w = base64_encode('2w');
    $enc_4w = base64_encode('4w');

    $sql_b2b = "SELECT DISTINCT b.gb_booking_id,b.b2b_vehicle_type,b.b2b_credit_amt,b.b2b_log FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id=g.booking_id LEFT JOIN go_bumpr.localities as l ON g.locality=l.localities WHERE s.b2b_acpt_flag='1' AND b.b2b_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log)='$date'  AND b.b2b_shop_id NOT IN (1014,1035,1670) {$cond}";    
    $res_b2b = mysqli_query($conn,$sql_b2b);
    //echo $sql_b2b;
    //initialize
    $bike = 0;
    $car = 0;
    $total = 0 ;

    while($row_b2b = mysqli_fetch_object($res_b2b)){
        $veh_type = $row_b2b->b2b_vehicle_type;
        $creditamount = $row_b2b->b2b_credit_amt;
        $gb_booking_id = $row_b2b->gb_booking_id;
        $log = $row_b2b->b2b_log;

        switch($veh_type){
            case '2w': $bike = $bike+$creditamount; break;
            case '4w': $car = $car+$creditamount; break;
        }//switch
    } // while

    $bikecredits = $bike/100;
    $carcredits = $car/100;
    $totalcredits = $bikecredits+$carcredits;
    if($date > $today){
        continue;
    }

    $tr = '<tr>';
    $td1 = '<td style="text-align:center;">'.$thisDate.'</td>';
    $td2 = '<td style="text-align:center;">'.$day.'</td>';

    $total_cpd = $car_cpd+$bike_cpd;

    $car_120p = 120*($car_cpd/100);  // 120% of car credits
    $bike_120 = 120*($bike_cpd/100); // 120% of car credits
    $total_120 = $car_120p+$bike_120;


    if($carcredits >= $car_120p){ 
        $td3 ='<td style="background-color:#7bde95;text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_4w.'" >'.$carcredits.'</a></td>';
    }
    else if($carcredits <$car_120p && $carcredits>= $car_cpd){ 
        $td3 = '<td style="background-color:#f3ae2b;text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_4w.'" >'.$carcredits.'</a></td>';
    }
    else{ 
        $td3 = '<td style="text-align:center;background-color:#e88c87;"><a href="daily_reports_servicetype.php?dt='.$enc_date.'&vt='.$enc_4w.'&t='.base64_encode(reports).'" >'.$carcredits.'</a></td>';
    }
    if($bikecredits >= $bike_120){ 
        $td4 = '<td style="background-color:#7bde95;text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_2w.'" >'.$bikecredits.'</a></td>';
    }
    else if($bikecredits <$bike_120 && $bikecredits >=$bike_cpd ){ 
        $td4 = '<td style="background-color:#f3ae2b;text-align:center;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_2w.'" >'.$bikecredits.'</a></td>'; 
    }
    else{ 
        $td4 = '<td style="text-align:center;background-color:#e88c87;"><a href="daily_reports_servicetype.php?t='.base64_encode(reports).'&dt='.$enc_date.'&vt='.$enc_2w.'" >'.$bikecredits.'</a></td>'; 
    }
    if($totalcredits >= $total_120){ 
        $td5 = '<td style="background-color:#7bde95;text-align:center;">'.$totalcredits.'</td>'; 
    }
    else if($totalcredits < $total_120 && $totalcredits >= $total_cpd){ 
        $td5 = '<td style="background-color:#f3ae2b;text-align:center;">'.$totalcredits.'</td>'; 
    }
    else{ 
        $td5 = '<td style="text-align:center;background-color:#e88c87;">'.$totalcredits.'</td>';
    }

    $tr_l = '</tr>';

    $str = $tr.$td1.$td2.$td3.$td4.$td5.$tr_l;
    $data[] = array('tr'=>$str);
} */

$rtn_data['data1'] = $data1;
$rtn_data['data2w'] = $data2w;
$rtn_data['data4w'] = $data4w;
$rtn_data['data2'] = $data2;
echo $rtn_data1 = json_encode($rtn_data);
?>

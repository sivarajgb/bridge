<?php
error_reporting(E_ALL);
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$flag = $_SESSION['flag'];


$startdate = date('Y-m-d', strtotime($_GET['startdate']));
$enddate = date('Y-m-d', strtotime($_GET['enddate']));
$person = $_GET['person'];
$source = $_GET['source'];
$vehicle = $_GET['vehicle'];
$service = $_GET['service'];
$city = $_GET['city'];
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

$col_name = $vehicle == '2w' ? 'l.bike_cluster' : 'l.car_cluster';

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;

$cond = '';

$cond = $cond . ($vehicle == 'all' ? "" : "AND b.vehicle_type='$vehicle'");
$cond = $cond . ($source == 'all' ? "" : "AND t.src='$source'");
$cond = $cond . ($service == 'all' ? "" : "AND b.service_type='$service'");
$cond = $cond . ($person == 'all' ? "" : "AND t.crm_alloct_id='$person'");
$cond = $cond . ($city == 'all' ? "" : "AND b.city='$city'");
$cond = $cond . ($cluster == 'all' ? "" : ($vehicle != 'all' ? "AND " . $col_name . "='$cluster'" : "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%" . $cluster . "%'"));
$ticket_admin = array('crm029', 'crm107', 'crm040', 'crm015', 'crm087');
if ($flag != 1 && !in_array($crm_log_id, $ticket_admin)) {
	$cond = "AND t.crm_alloct_id='$crm_log_id'";
}
if ($city == 'Chennai') {
	$sql_ticket = "SELECT DISTINCT t.ticket_id, t.booking_id,t.ticket_status,t.src,u.name AS user_name,u.mobile_number AS user_mobile,b.locality,v.brand,v.model,b.service_type,b.shop_name,b.axle_flag,t.update_log,t.issue_faced,t.closure_date,t.log,c.name AS crm_name FROM ticket_tb AS t LEFT JOIN user_booking_tb AS b ON b.booking_id= t.booking_id LEFT JOIN crm_admin AS c ON t.crm_alloct_id= c.crm_log_id LEFT JOIN user_register AS u ON t.user_id = u.reg_id LEFT JOIN user_vehicle_table AS v ON b.user_veh_id = v.id LEFT JOIN localities AS l ON b.locality = l.localities WHERE (DATE(t.update_log) BETWEEN '$startdate' and '$enddate' OR DATE(t.followup_date) BETWEEN '$startdate' and '$enddate') {$cond} ORDER BY t.update_log DESC";
} else {
	$sql_ticket = "SELECT DISTINCT t.ticket_id, t.booking_id,t.ticket_status,t.src,u.name AS user_name,u.mobile_number AS user_mobile,b.locality,v.brand,v.model,b.service_type,b.shop_name,b.axle_flag,t.update_log,t.issue_faced,t.closure_date,t.log,c.name AS crm_name FROM ticket_tb AS t LEFT JOIN user_booking_tb AS b ON b.booking_id= t.booking_id LEFT JOIN crm_admin as c ON t.crm_alloct_id=c.crm_log_id LEFT JOIN user_register as u ON t.user_id=u.reg_id LEFT JOIN user_vehicle_table as v ON b.user_veh_id=v.id WHERE DATE(t.update_log) BETWEEN '$startdate' and '$enddate' {$cond} ORDER BY t.update_log";
}
//echo $sql_ticket;
$res_ticket = mysqli_query($conn, $sql_ticket);
$no = 0;
$count = mysqli_num_rows($res_ticket);
if ($count > 0) {
	while ($row_ticket = mysqli_fetch_object($res_ticket)) {
		$ticket_id = $row_ticket->ticket_id;
		$booking_id = $row_ticket->booking_id;
		$ticket_status = $row_ticket->ticket_status;
		$user_name = $row_ticket->user_name;
		$user_mobile = $row_ticket->user_mobile;
		$locality = $row_ticket->locality;
		$brand = $row_ticket->brand;
		$model = $row_ticket->model;
		$service_type = $row_ticket->service_type;
		$ticket_src = $row_ticket->src;
		$shop_name = $row_ticket->shop_name;
		$axle_flag = $row_ticket->axle_flag;
		$update_log = $row_ticket->update_log;
		$issue_faced = $row_ticket->issue_faced;
		$closure_date = $row_ticket->closure_date;
		$log = $row_ticket->log;
		$crm_name = $row_ticket->crm_name;
		
		$ticket_status == 'Resolved' ? $tr = '<tr style="background-color:#aaf1b0;">' : $tr = '<tr>';
		$td1 = '<td>' . $ticket_id . '<a href="ticket_history.php?t=' . base64_encode($ticket_id) . '" target="_blank"><i class="fa fa-eye" style="padding-left:12px;" aria-hidden="true"></i></a></td>';
		$td2 = '<td>' . $booking_id . '</td>';
		$td3 = '<td>' . $ticket_status . '</td>';
		$td5 = '<td>' . $user_name . '</td>';
		if (!$_SESSION['eupraxia_flag']) {
			$td6 = '<td>' . $user_mobile . '</td>';
		}
		// $td7 = '<td>'.$locality.'</td>';
		$td7 = '';
		$td8 = '<td>' . $ticket_src . '</td>';
		$td9 = '<td>' . $service_type . '</td>';
		$td10 = '<td>' . $shop_name . '</td>';
		$td11 = '<td style="text-align:center;">';
		$axle_flag == 1 ? $td11 = $td11 . '<i class="fa fa-check-circle" aria-hidden="true" style="font-size:24px;color: green!important;" title="GoAxled!"></i></td>' : $td11 = $td11 . '<i class="fa fa-times-circle" aria-hidden="true" style="font-size:24px;color:red!important;" title="Not GoAxled!"></i></td>';
		$td13 = '<td>' . $issue_faced . '</td>';
		$td14 = '<td>' . $crm_name . '</td>';
		$td15 = '<td>' . date('d-m-Y', strtotime($update_log)) . '</td>';
		$td16 = '<td>';
		$closure_date == '0000-00-00 00:00:00' ? $td16 = $td16 . 'Not Updated</td>' : $td16 = $td16 . date('d-m-Y', strtotime($closure_date)) . '</td>';
		$td17 = '<td>' . date('d-m-Y', strtotime($log)) . '</td>';
		$tr_l = '</tr>';
		
		if ($_SESSION['eupraxia_flag']) {
			$str = $tr . $td1 . $td2 . $td3 . $td5 . $td7 . $td8 . $td9 . $td10 . $td11 . $td13 . $td14 . $td15 . $td16 . $td17 . $tr_l;
		} else {
			$str = $tr . $td1 . $td2 . $td3 . $td5 . $td6 . $td7 . $td8 . $td9 . $td10 . $td11 . $td13 . $td14 . $td15 . $td16 . $td17 . $tr_l;
		}
		$data[] = array('tr' => $str);
	}
	echo $data1 = json_encode($data);
} // if
else {
	echo "no";
}
?>
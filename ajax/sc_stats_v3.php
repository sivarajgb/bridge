<?php
//error_reporting(E_ALL); ini_set('display_errors', 1);
include("../config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();
$today=date('Y-m-d');

$crm_log_id = $_SESSION['crm_log_id'] ;

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$vehicle = $_POST['vehicle'];
$master = $_POST['master'];
$shop_id = $_POST['shop_id'];
$city = $_POST['city'];

$_SESSION['crm_city'] = $city;

$cond = '';
$cond = $city == 'all' ? $cond."" : $cond." AND b.city = '$city'";
$cond = $shop_id == 'all' ? $cond."" : $cond." AND b2b_b.b2b_shop_id = '$shop_id'";
$cond = $vehicle == 'all' ? $cond."" : $cond." AND b.vehicle_type = '$vehicle'";

$cond2 = '';
$cond2 = $vehicle == 'all' ? $cond2."" : $cond2." WHERE type = '$vehicle'";

$cond3 = '';
$cond3 = $vehicle == 'all' ? $cond3."" : $cond3." AND type = '$vehicle'";


switch($master){
    case 'all': 
				$sql_data = "SELECT b.service_type,
							 SUM(CASE WHEN b.final_bill_amt != '' THEN 1 ELSE 0 END) AS Total_Billed_Customer,
							 SUM(b.final_bill_amt) AS Total_Bill,
							 AVG(CASE WHEN b.rating != 0 AND ((b.service_status = 'Completed') OR (b2b_b.b2b_vehicle_ready = 1)) THEN b.rating END) AS Avg_Rating,
							 SUM(CASE WHEN ((b.service_status = 'Completed') OR (b2b_b.b2b_vehicle_ready = 1)) THEN 1 ELSE 0 END) AS Total_Completed,
							 COUNT(b2b_b.b2b_booking_id) AS Total_Booking,
							 COUNT(CASE WHEN b.rating != 0 AND b.rating !='' AND ((b.service_status = 'Completed') OR (b2b_b.b2b_vehicle_ready = 1)) THEN 1 END) as count_rating,
							 SUM(CASE WHEN b.rating != 0 AND b.rating !='' AND ((b.service_status = 'Completed') OR (b2b_b.b2b_vehicle_ready = 1)) THEN b.rating END) as sum_rating
							 
							 FROM user_booking_tb b LEFT JOIN b2b.b2b_booking_tbl b2b_b ON b2b_b.gb_booking_id = b.booking_id
							 WHERE b2b_b.b2b_swap_flag = '0'
							 {$cond}
							 AND DATE(b2b_b.b2b_log) BETWEEN '$startdate' AND '$enddate'
							 AND b2b_b.b2b_shop_id NOT IN ('1014','1035','1670')
							 GROUP BY b.service_type ORDER BY b.shop_name;";
				// echo $sql_data;die;
				$res_data = mysqli_query($conn1,$sql_data);
				while($row_data = mysqli_fetch_object($res_data))
				{
					$service_type = $row_data->service_type;
					$total_billed_customer = $row_data->Total_Billed_Customer;
					$total_bill = $row_data->Total_Bill;
					$avg_bill_value = $total_bill/$total_billed_customer;
					$avg_rating = $row_data->Avg_Rating;
					$count_rating = $row_data->count_rating;
					$sum_rating = $row_data->sum_rating;
					$total_completed = $row_data->Total_Completed;
					$total_bookings = $row_data->Total_Booking;
					$conversion_rate = round(($total_completed/$total_bookings)*100,2);
					
					if(is_nan($avg_bill_value))
					{
						$avg_bill_value = 0;
					}
					if($avg_rating == '' || $avg_rating == null)
					{
						$avg_rating = 0;
					}
					
					$tr = "<tr>";
					$td1 = "<td style='text-align:center;'>$service_type</td>";
					$td2 = "<td style='text-align:center;'>$total_billed_customer</td>";
					$td3 = "<td><i class='fa fa-inr' style='padding-right: 5px;'></i>".number_format($total_bill)."</td>";
					$td4 = "<td><i class='fa fa-inr' style='padding-right: 5px;'></i>".number_format(round($avg_bill_value,0))."</td>";
					$td5 = "<td style='text-align:center;'>$total_bookings</td>";
					$td6 = "<td style='text-align:center;'>$total_completed</td>";
					$td7 = "<td style='text-align:center;'>$conversion_rate %</td>";
					$td8 = "<td style='text-align:center;'>$count_rating</td>";
					$td9 = "<td style='text-align:center;'>".round($avg_rating,2)."</td>";
					$tr_l = "</tr>";
					
					$overall_total_billed_customer = $overall_total_billed_customer + $total_billed_customer;
					$overall_total_bill = $overall_total_bill + $total_bill;
					$overall_total_bookings = $overall_total_bookings + $total_bookings;
					$overall_total_completed = $overall_total_completed + $total_completed;
					$overall_count_rating = $overall_count_rating + $count_rating;
					$overall_rating = $overall_rating + $sum_rating;
					
					$str = $tr.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$td9.$tr_l;
					$data[] = $str;
				}
				break;
	case 'master': 
				// $sql_st = "SELECT DISTINCT master_service FROM go_axle_service_price_tbl {$cond2}";
				// $res_st = mysqli_query($conn1,$sql_st);
				// $services = '';
				// while($row_st = mysqli_fetch_array($res_st))
				// {
				// 	$master_service = $row_st['master_service'];
				// 	$sql_services = "SELECT service_type FROM go_axle_service_price_tbl WHERE master_service='$master_service' {$cond3}";
				// 	$res_services = mysqli_query($conn1,$sql_services);
				// 	while($row_services=mysqli_fetch_array($res_services))
				// 	{
				// 		if($services == "")
				// 		{
				// 			$services = "'".$row_services['service_type']."'";
				// 		}
				// 		else
				// 		{
				// 			$services = $services.",'".$row_services['service_type']."'";
				// 		}
				// 	}
				// }
				$cond_shop = $shop_id == 'all' ? "" :"AND b2b_b.b2b_shop_id = '$shop_id'";
				$master_arr=array();
		        $sql_master="SELECT service_type,type, master_service FROM go_bumpr.go_axle_service_price_tbl";
		        $res_master = mysqli_query($conn1,$sql_master);
		        while($row=mysqli_fetch_object($res_master)){
		        	$master_arr[$row->type][trim($row->service_type)]=$row->master_service;
		        }
		        // var_dump($master_arr);die;
		        if($vehicle=='all'){
			        $new_master_service_arr=array();
					$sql_data = "SELECT 
    b.service_type,
    SUM(CASE
        WHEN b.final_bill_amt != '' THEN 1
        ELSE 0
    END) AS Total_Billed_Customer,
    SUM(b.final_bill_amt) AS Total_Bill,
    AVG(CASE
        WHEN
            b.rating != 0
                AND ((b.service_status = 'Completed')
                OR (b2b_b.b2b_vehicle_ready = 1))
        THEN
            b.rating
    END) AS Avg_Rating,
    SUM(CASE
        WHEN
            ((b.service_status = 'Completed')
                OR (b2b_b.b2b_vehicle_ready = 1))
        THEN
            1
        ELSE 0
    END) AS Total_Completed,
    COUNT(b2b_b.b2b_booking_id) AS Total_Booking,
    COUNT(CASE
        WHEN
            b.rating != 0 AND b.rating != ''
                AND ((b.service_status = 'Completed')
                OR (b2b_b.b2b_vehicle_ready = 1))
        THEN
            1
    END) AS count_rating,
    SUM(CASE
        WHEN
            b.rating != 0 AND b.rating != ''
                AND ((b.service_status = 'Completed')
                OR (b2b_b.b2b_vehicle_ready = 1))
        THEN
            b.rating
    END) AS sum_rating
FROM
    go_bumpr.user_booking_tb b
        LEFT JOIN
    b2b.b2b_booking_tbl b2b_b ON b2b_b.gb_booking_id = b.booking_id
WHERE
    b2b_b.b2b_swap_flag = '0' AND b.vehicle_type='2w'
        AND b.city = '$city' {$cond_shop}
        
        AND DATE(b2b_b.b2b_log) BETWEEN '$startdate' AND '$enddate'
        AND b2b_b.b2b_shop_id NOT IN ('1014' , '1035', '1670')
GROUP BY b.service_type
ORDER BY b.shop_name;";
					// echo $sql_data;die;
					$res_data = mysqli_query($conn1,$sql_data);
					while($row=mysqli_fetch_object($res_data)){
			        	$master_service=$master_arr['2w'][$row->service_type];
			        	if(!array_key_exists($master_service,$new_master_service_arr)){
			        		$new_master_service_arr[$master_service]=array();	
			        	}
			        	$new_master_service_arr[$master_service]['Total_Billed_Customer']+=$row->Total_Billed_Customer;
			        	$new_master_service_arr[$master_service]['Total_Bill']+=$row->Total_Bill;
			        	$new_master_service_arr[$master_service]['Avg_Rating']+=$row->Avg_Rating;
			        	$new_master_service_arr[$master_service]['count']+=1;
			        	$new_master_service_arr[$master_service]['Total_Completed']+=$row->Total_Completed;

			        	$new_master_service_arr[$master_service]['Total_Booking']+=$row->Total_Booking;
			        	$new_master_service_arr[$master_service]['count_rating']+=$row->count_rating;
			        	$new_master_service_arr[$master_service]['sum_rating']+=$row->sum_rating;
			        	

		        	}
					$sql_data = "SELECT 
    b.service_type,
    SUM(CASE
        WHEN b.final_bill_amt != '' THEN 1
        ELSE 0
    END) AS Total_Billed_Customer,
    SUM(b.final_bill_amt) AS Total_Bill,
    AVG(CASE
        WHEN
            b.rating != 0
                AND ((b.service_status = 'Completed')
                OR (b2b_b.b2b_vehicle_ready = 1))
        THEN
            b.rating
    END) AS Avg_Rating,
    SUM(CASE
        WHEN
            ((b.service_status = 'Completed')
                OR (b2b_b.b2b_vehicle_ready = 1))
        THEN
            1
        ELSE 0
    END) AS Total_Completed,
    COUNT(b2b_b.b2b_booking_id) AS Total_Booking,
    COUNT(CASE
        WHEN
            b.rating != 0 AND b.rating != ''
                AND ((b.service_status = 'Completed')
                OR (b2b_b.b2b_vehicle_ready = 1))
        THEN
            1
    END) AS count_rating,
    SUM(CASE
        WHEN
            b.rating != 0 AND b.rating != ''
                AND ((b.service_status = 'Completed')
                OR (b2b_b.b2b_vehicle_ready = 1))
        THEN
            b.rating
    END) AS sum_rating
FROM
    go_bumpr.user_booking_tb b
        LEFT JOIN
    b2b.b2b_booking_tbl b2b_b ON b2b_b.gb_booking_id = b.booking_id
WHERE
    b2b_b.b2b_swap_flag = '0' AND b.vehicle_type='4w'
        AND b.city = '$city' {$cond_shop}
        AND DATE(b2b_b.b2b_log) BETWEEN '$startdate' AND '$enddate'
        AND b2b_b.b2b_shop_id NOT IN ('1014' , '1035', '1670')
GROUP BY b.service_type
ORDER BY b.shop_name;";
					// echo $sql_data;
					$res_data = mysqli_query($conn1,$sql_data);
					while($row=mysqli_fetch_object($res_data)){
			        	$master_service=$master_arr['4w'][$row->service_type];
			        	if($row->service_type=="Car Oil Service"){
			        		$master_service="Car Oil Service";
			        	}
			        	if(!array_key_exists($master_service,$new_master_service_arr)){
			        		$new_master_service_arr[$master_service]=array();	
			        	}
			        	$new_master_service_arr[$master_service]['Total_Billed_Customer']+=$row->Total_Billed_Customer;
			        	$new_master_service_arr[$master_service]['Total_Bill']+=$row->Total_Bill;
			        	$new_master_service_arr[$master_service]['Avg_Rating']+=$row->Avg_Rating;
			        	$new_master_service_arr[$master_service]['count']+=1;
			        	$new_master_service_arr[$master_service]['Total_Completed']+=$row->Total_Completed;
			        	
			        	$new_master_service_arr[$master_service]['Total_Booking']+=$row->Total_Booking;
			        	$new_master_service_arr[$master_service]['count_rating']+=$row->count_rating;
			        	$new_master_service_arr[$master_service]['sum_rating']+=$row->sum_rating;
		        	}
				}else{
					$new_master_service_arr=array();
					$sql_data = "SELECT b.service_type,
							 SUM(CASE WHEN b.final_bill_amt != '' THEN 1 ELSE 0 END) AS Total_Billed_Customer,
							 SUM(b.final_bill_amt) AS Total_Bill,
							 AVG(CASE WHEN b.rating != 0 AND ((b.service_status = 'Completed') OR (b2b_b.b2b_vehicle_ready = 1)) THEN b.rating END) AS Avg_Rating,
							 SUM(CASE WHEN ((b.service_status = 'Completed') OR (b2b_b.b2b_vehicle_ready = 1)) THEN 1 ELSE 0 END) AS Total_Completed,
							 COUNT(b2b_b.b2b_booking_id) AS Total_Booking,
							 COUNT(CASE WHEN b.rating != 0 AND b.rating !='' AND ((b.service_status = 'Completed') OR (b2b_b.b2b_vehicle_ready = 1)) THEN 1 END) as count_rating,
							 SUM(CASE WHEN b.rating != 0 AND b.rating !='' AND ((b.service_status = 'Completed') OR (b2b_b.b2b_vehicle_ready = 1)) THEN b.rating END) as sum_rating
							 
							 FROM user_booking_tb b LEFT JOIN b2b.b2b_booking_tbl b2b_b ON b2b_b.gb_booking_id = b.booking_id
							 WHERE b2b_b.b2b_swap_flag = '0'
							 {$cond}
							 AND DATE(b2b_b.b2b_log) BETWEEN '$startdate' AND '$enddate'
							 AND b2b_b.b2b_shop_id NOT IN ('1014','1035','1670')
							 GROUP BY b.service_type ORDER BY b.shop_name;";
					$res_data = mysqli_query($conn1,$sql_data);
					
					while($row=mysqli_fetch_object($res_data)){
			        	$master_service=$master_arr[$vehicle][$row->service_type];
			        	if(!array_key_exists($master_service,$new_master_service_arr)){
			        		$new_master_service_arr[$master_service]=array();	
			        	}
			        	$new_master_service_arr[$master_service]['Total_Billed_Customer']+=$row->Total_Billed_Customer;
			        	$new_master_service_arr[$master_service]['Total_Bill']+=$row->Total_Bill;
			        	$new_master_service_arr[$master_service]['Avg_Rating']+=$row->Avg_Rating;
			        	$new_master_service_arr[$master_service]['count']+=1;
			        	$new_master_service_arr[$master_service]['Total_Completed']+=$row->Total_Completed;
			        	
			        	$new_master_service_arr[$master_service]['Total_Booking']+=$row->Total_Booking;
			        	$new_master_service_arr[$master_service]['count_rating']+=$row->count_rating;
			        	$new_master_service_arr[$master_service]['sum_rating']+=$row->sum_rating;
			        	
		        	}
		        	

				}
				foreach ($new_master_service_arr as $key => $value) {
					$master_service_type = $key;
					$total_billed_customer = $value['Total_Billed_Customer'];
					$total_bill = $value['Total_Bill'];
					$avg_bill_value = $total_bill/$total_billed_customer;
					$avg_rating = $value['Avg_Rating'];
					$count_rating = $value['count_rating'];
					$sum_rating = $value['sum_rating'];
					$total_completed = $value['Total_Completed'];
					$total_bookings = $value['Total_Booking'];
					$conversion_rate = round(($total_completed/$total_bookings)*100,2);
					$count=$value['count'];
					if(is_nan($avg_bill_value))
					{
						$avg_bill_value = 0;
					}
					if($avg_rating == '' || $avg_rating == null)
					{
						$avg_rating = 0;
					}
					
					$tr = "<tr>";
					$td1 = "<td style='text-align:center;'>$master_service_type</td>";
					$td2 = "<td style='text-align:center;'>$total_billed_customer</td>";
					$td3 = "<td><i class='fa fa-inr' style='padding-right: 5px;'></i>".number_format($total_bill)."</td>";
					$td4 = "<td ><i class='fa fa-inr' style='padding-right: 5px;'></i>".number_format(round($avg_bill_value,0))."</td>";
					$td5 = "<td style='text-align:center;'>$total_bookings</td>";
					$td6 = "<td style='text-align:center;'>$total_completed</td>";
					$td7 = "<td style='text-align:center;'>$conversion_rate %</td>";
					$td8 = "<td style='text-align:center;'>$count_rating</td>";
					$td9 = "<td style='text-align:center;'>".round($sum_rating/$count_rating,2)."</td>";
					$tr_l = "</tr>";
					
					$overall_total_billed_customer = $overall_total_billed_customer + $total_billed_customer;
					$overall_total_bill = $overall_total_bill + $total_bill;
					$overall_total_bookings = $overall_total_bookings + $total_bookings;
					$overall_total_completed = $overall_total_completed + $total_completed;
					$overall_count_rating = $overall_count_rating + $count_rating;
					$overall_rating = $overall_rating + $sum_rating;
					
					$str = $tr.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$td9.$tr_l;
					$data[] = $str;
				}
				break;
}

$overall_avg_bill_value = $overall_total_bill / $overall_total_billed_customer;
$overall_conversion_rate = $overall_total_completed / $overall_total_bookings;
$overall_avg_rating = $overall_rating / $overall_count_rating;

$td1_1 = "<th style='text-align:center;background-color: #D3D3D3;'>Total</th>";
$td1_2 = "<th style='text-align:center;background-color: #D3D3D3;'>$overall_total_billed_customer</th>";
$td1_3 = "<th style='background-color: #D3D3D3;'><i class='fa fa-inr' style='padding-right: 5px;'></i>".number_format($overall_total_bill)."</th>";
$td1_4 = "<th style='background-color: #D3D3D3;'><i class='fa fa-inr' style='padding-right: 5px;'></i>".number_format(round($overall_avg_bill_value,0))."</th>";
$td1_5 = "<th style='text-align:center;background-color: #D3D3D3;'>$overall_total_bookings</th>";
$td1_6 = "<th style='text-align:center;background-color: #D3D3D3;'>$overall_total_completed</th>";
$td1_7 = "<th style='text-align:center;background-color: #D3D3D3;'>".round($overall_conversion_rate*100,2)." %</th>";
$td1_8 = "<th style='text-align:center;background-color: #D3D3D3;'>$overall_count_rating</th>";
$td1_9 = "<th style='text-align:center;background-color: #D3D3D3;'>".round($overall_avg_rating,2)."</th>";

$str = $tr.$td1_1.$td1_2.$td1_3.$td1_4.$td1_5.$td1_6.$td1_7.$td1_8.$td1_9.$tr_l;
$data[] = $str;

echo json_encode($data);
?>
<?php
include("../config.php");
$conn = db_connect3();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$city = $_POST['city'];
$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate = date('Y-m-d',strtotime($_POST['enddate']));

function calculate_median($arr) {
    $count = count($arr); //total numbers in array
	$middleval = floor(($count-1)/2); // find the middle value, or the lowest middle value
	if($count % 2) { // odd number, middle is the median
        $median = $arr[$middleval];
    } else { // even number, calculate avg of 2 medians
        $low = $arr[$middleval];
        $high = $arr[$middleval+1];
        $median = (($low+$high)/2);
    }
    return $median;
}

$sql_shops = "SELECT DISTINCT(p.b2b_shop_id),m.b2b_shop_name FROM b2b.b2b_partner_token p,b2b.b2b_mec_tbl m WHERE p.b2b_shop_id = m.b2b_shop_id AND p.b2b_shop_id NOT IN (1014,1035,1670) AND m.b2b_address5 = '$city'";
if($city=='all')
$sql_shops = "SELECT DISTINCT(p.b2b_shop_id),m.b2b_shop_name FROM b2b.b2b_partner_token p,b2b.b2b_mec_tbl m WHERE p.b2b_shop_id = m.b2b_shop_id AND p.b2b_shop_id NOT IN (1014,1035,1670)";
$res_shops = mysqli_query($conn,$sql_shops);
while($row_shops = mysqli_fetch_object($res_shops))
{
	$shop_id = $row_shops->b2b_shop_id;
	$shop_name = $row_shops->b2b_shop_name;
	$sql = "SELECT sum(s.b2b_acpt_flag LIKE '1') as accept, sum(s.b2b_deny_flag LIKE '1') as deny, count(s.b2b_booking_id) as total, (count(s.b2b_booking_id)-(sum(s.b2b_acpt_flag LIKE '1')+sum(s.b2b_deny_flag LIKE '1'))) as idle,AVG(TIMESTAMPDIFF(MINUTE,b.b2b_log,s.b2b_mod_log)) AS avg_action,AVG(TIMESTAMPDIFF(MINUTE,s.b2b_mod_log,b.b2b_contacted_log)) AS avg_contact,MIN(TIMESTAMPDIFF(MINUTE,b.b2b_log,s.b2b_mod_log)) AS min_action,MAX(TIMESTAMPDIFF(MINUTE,b.b2b_log,s.b2b_mod_log)) AS max_action,MIN(TIMESTAMPDIFF(MINUTE,s.b2b_mod_log,b.b2b_contacted_log)) AS min_contact,MAX(TIMESTAMPDIFF(MINUTE,s.b2b_mod_log,b.b2b_contacted_log)) AS max_contact,sum(b.b2b_check_in_report LIKE '1')/count(s.b2b_booking_id)*100 as check_in,sum(b.b2b_vehicle_at_garage LIKE '1')/count(s.b2b_booking_id)*100 as inspected,sum(b.b2b_vehicle_ready LIKE '1')/count(s.b2b_booking_id)*100 as completed FROM b2b.b2b_status s, b2b.b2b_booking_tbl b WHERE s.b2b_booking_id = b.b2b_booking_id AND DATE(s.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_shop_id = '$shop_id';";
	$res = mysqli_query($conn,$sql);
	$row = mysqli_fetch_object($res);
	//$avg_acptd = round($row->avg,0);
	$total_leads = $row->total;
	$accepted_leads = $row->accept;
	$rejected_leads = $row->deny;
	$idle_leads = $row->idle;
	$avg_action_time = round($row->avg_action,0);
	$min_action_time = $row->min_action;
	$max_action_time = $row->max_action;
	$avg_contact_time = round($row->avg_contact,0);
	$min_contact_time = $row->min_contact;
	$max_contact_time = $row->max_contact;
	$check_in = round($row->check_in,1);
	$inspected = round($row->inspected,1);
	$completed = round($row->completed,1);
	/* $sql_med = "SELECT TIMESTAMPDIFF(MINUTE,g.sent_log,b.b2b_mod_log) AS diff FROM go_bumpr.goaxle_track g,b2b.b2b_status b WHERE g.b2b_booking_id = b.b2b_booking_id AND g.b2b_shop_id= '$shop_id' AND TIMESTAMPDIFF(MINUTE,g.sent_log,b.b2b_mod_log) is not null AND DATE(g.sent_log) between '$startdate' and '$enddate' ORDER BY diff ASC; ";
	$res_med = mysqli_query($conn,$sql_med);
	$arr = array();
	while($row_med = mysqli_fetch_object($res_med))
	{
		$arr[] = $row_med->diff;
	}
	$med_acptd = calculate_median($arr);
	$sql1 = "SELECT AVG(TIMESTAMPDIFF(MINUTE,s.b2b_acpt_log,b.b2b_contacted_log)) AS avg,MIN(TIMESTAMPDIFF(MINUTE,s.b2b_acpt_log,b.b2b_contacted_log)) AS minimum,MAX(TIMESTAMPDIFF(MINUTE,s.b2b_acpt_log,b.b2b_contacted_log)) AS maximum FROM b2b.b2b_booking_tbl b, b2b.b2b_status s WHERE b.b2b_booking_id = s.b2b_booking_id AND b.b2b_shop_id= '$shop_id' AND b.b2b_contacted='1' AND DATE(b.b2b_log) between '$startdate' and '$enddate';";
	$res1 = mysqli_query($conn,$sql1);
	$row1 = mysqli_fetch_object($res1);
	$avg_contact = round($row1->avg,0);
	$min_contact = $row1->minimum;
	$max_contact = $row1->maximum;
	$sql1_med = "SELECT TIMESTAMPDIFF(MINUTE,s.b2b_acpt_log,b.b2b_contacted_log) AS diff FROM b2b.b2b_booking_tbl b,b2b.b2b_status s WHERE b.b2b_booking_id = s.b2b_booking_id AND b.b2b_shop_id= '$shop_id' AND TIMESTAMPDIFF(MINUTE,b.b2b_log,b.b2b_contacted_log) is not null AND DATE(b.b2b_log) between '$startdate' and '$enddate' ORDER BY diff ASC;";
	$res1_med = mysqli_query($conn,$sql1_med);
	$arr1 = array();
	while($row1_med = mysqli_fetch_object($res1_med))
	{
		$arr1[] = $row1_med->diff;
	}
	$med_contact = calculate_median($arr1); */
	?>
	<tr>
	<td style="text-align: center;"><strong><?php echo $shop_name;?></strong></td>
	<td style="text-align: center;"><?php echo $avg_action_time==""?0:$avg_action_time;?></td>
	<td style="text-align: center;"><?php echo $min_action_time==""?0:$min_action_time;?></td>
	<td style="text-align: center;"><?php echo $max_action_time==""?0:$max_action_time;?></td>
	<td style="text-align: center;"><?php echo $min_action_time==""?0:$min_action_time;?></td>
	<td style="text-align: center;"><?php echo $avg_contact_time==""?0:$avg_contact_time;?></td>
	<td style="text-align: center;"><?php echo $min_contact_time==""?0:$min_contact_time;?></td>
	<td style="text-align: center;"><?php echo $max_contact_time==""?0:$max_contact_time;?></td>
	<td style="text-align: center;"><?php echo $min_contact_time==""?0:$min_contact_time;?></td>
	<td style="text-align: center;"><?php echo $total_leads==""?0:$total_leads;?></td>
	<td style="text-align: center;"><?php echo $accepted_leads==""?0:$accepted_leads;?></td>
	<td style="text-align: center;"><?php echo $rejected_leads==""?0:$rejected_leads;?></td>
	<td style="text-align: center;"><?php echo $idle_leads==""?0:$idle_leads;?></td>
	<td style="text-align: center;"><?php echo $check_in;?>%</td>
	<td style="text-align: center;"><?php echo $inspected;?>%</td>
	<td style="text-align: center;"><?php echo $completed;?>%</td>
	</tr>
<?php
}
/*echo $sql1;
echo $sql1_med;
echo $sql;
print_r($arr);
print_r($arr1);*/
?>
<?php
include("../config.php");
$conn = db_connect1();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$city = $_GET['city'];
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;

function run_query($conn, $query){
	$query_result = mysqli_query($conn,$query);
	$query_rows = mysqli_fetch_object($query_result);
	return $query_object = $query_rows->count;
}

function run_query1($conn, $query){
	$count_no_leads = 0;
	$count_no_bookings = 0;
	$count_no_cancelled = 0;
	$count_no_others = 0;
	$count_no_unwanted = 0;
	$query_result = mysqli_query($conn,$query);
	while($query_rows = mysqli_fetch_object($query_result))
	{
		$booking_id = $query_rows->booking_id;
		$booking_status = $query_rows->booking_status;
		$override_flag = $query_rows->override_flag;
		$flag = $query_rows->flag;
		$flag_unwntd = $query_rows->flag_unwntd;
		$o_status = $query_rows->status;
		if($booking_status == 2 && $flag != 1 && $flag_unwntd == 0)
		{
			$count_no_bookings = $count_no_bookings + 1;
		}
		else if($booking_status == 1 && $flag != 1 && $flag_unwntd == 0 && $override_flag!='1' && (is_null($o_status) || $o_status !='2'))
		{
			$count_no_leads = $count_no_leads + 1;
		}
		else if($flag == 1)
		{
			$count_no_cancelled = $count_no_cancelled + 1;
		}
		else if($booking_status == 0 && $flag != 1)
		{
			$count_no_others = $count_no_others + 1;
		}
	}
	$rtn['count_no_bookings'] = $count_no_bookings;
	$rtn['count_no_leads'] = $count_no_leads;
	$rtn['count_no_cancelled'] = $count_no_cancelled;
	$rtn['count_no_others'] = $count_no_others;
	return $rtn;
}

$cond ='';
 
$cond = $cond.($city == 'all' ? "" : "AND b.city='$city'");
$cond = $cond.($cluster == 'all' ? "" : "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%' " );

$select_sql = "SELECT count(DISTINCT b.booking_id) as count FROM user_booking_tb as b LEFT JOIN localities as l ON b.locality=l.localities WHERE b.crm_update_id = '$crm_log_id' {$cond}" ;
$select_sql1 =  "SELECT DISTINCT b.booking_id,b.booking_status,b.flag,b.flag_unwntd,b.override_flag,o.status FROM user_booking_tb as b LEFT JOIN override_tbl as o on b.booking_id=o.booking_id LEFT JOIN localities as l ON b.locality = l.localities WHERE b.booking_id!='' AND b.crm_update_id = '$crm_log_id' {$cond}";

if($crm_log_id=='crm012')
{
  $select_sql = "SELECT count(DISTINCT b.booking_id) as count FROM user_booking_tb as b LEFT JOIN localities as l ON b.locality=l.localities WHERE (b.crm_update_id = '$crm_log_id' OR b.crm_update_id IN ('crm012','crm055','crm041')) {$cond}";
  $select_sql1 =  "SELECT DISTINCT b.booking_id,b.booking_status,b.flag,b.flag_unwntd ,b.override_flag,o.status FROM user_booking_tb as b LEFT JOIN override_tbl as o on b.booking_id=o.booking_id  LEFT JOIN localities as l ON b.locality = l.localities WHERE b.booking_id!='' AND (b.crm_update_id = '$crm_log_id' OR b.crm_update_id IN ('crm012','crm055','crm041')) {$cond}";
}
//echo "{$select_sql} AND b.booking_status='1' AND b.flag!='1' AND b.flag_unwntd = '0' AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ";
// leads count
$no_leads_arr = run_query1($conn, "{$select_sql1} AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
$no_leads = $no_leads_arr['count_no_leads'];
// bookings count
//$no_bookings = run_query1($conn, "{$select_sql1} AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
$no_bookings = $no_leads_arr['count_no_bookings'];

// followups count
$no_followups = run_query($conn, "{$select_sql} AND b.booking_status IN(3,4,5,6) AND b.flag!='1' AND DATE(b.followup_date) BETWEEN '$startdate' AND '$enddate'");

// cancelled count
//$no_cancelled = run_query1($conn, "{$select_sql1} AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
$no_cancelled = $no_leads_arr['count_no_cancelled'];

//others count
//$no_others = run_query1($conn, "{$select_sql1} AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' ");
$no_others = $no_leads_arr['count_no_others'];

// overall count
// $no_overall = run_query($conn, "{$select_sql} AND b.flag_unwntd = '0' AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate'");
$sql_override = "SELECT count(DISTINCT o.booking_id) as count FROM override_tbl as o LEFT JOIN user_booking_tb as b on b.booking_id=o.booking_id LEFT JOIN localities as l ON b.locality = l.localities BETWEEN '$startdate' AND '$enddate' WHERE o.booking_id!='' {$cond} AND o.status='0' AND DATE(o.log) BETWEEN '$startdate' AND '$enddate' ";
$res_override = mysqli_query($conn,$sql_override);
$count = mysqli_num_rows($res_override);
if($count >0){
while($row_override = mysqli_fetch_object($res_override)){
	$override_count = $row_override->count;
}
}
else {
	$override_count = "0";
}
//echo $data = json_encode(array('leadsCount'=>$no_leads, 'bookingsCount'=>$no_bookings, 'followupsCount'=>$no_followups, 'cancelledCount'=>$no_cancelled, 'othersCount'=>$no_others, 'overallCount'=>$no_overall));
echo $data = json_encode(array('leadsCount'=>$no_leads,  'followupsCount'=>$no_followups, 'cancelledCount'=>$no_cancelled, 'othersCount'=>$no_others,'OverrideCount'=>$override_count));

?>

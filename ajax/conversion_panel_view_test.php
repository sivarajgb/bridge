<?php
include("../config.php");
$conn = db_connect1();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$super_flag = $_SESSION['super_flag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$starttime = date('H:i:s',strtotime($_POST['starttime']));
$endtime =  date('H:i:s',strtotime($_POST['endtime']));
$city = $_POST['city'];

$_SESSION['crm_city'] = $city;

$start = $startdate.' '.$starttime;
$end = $enddate.' '.$endtime;
?>

<div class="tab" style="margin-top:70px; margin-left:50px;">
  <button class="tablinks" onclick="openTab(event, 'Person')" id="persontab">Person</button>
  <button class="tablinks" onclick="openTab(event, 'Service')" id="servicetab">Service</button>
  <button class="tablinks" onclick="openTab(event, 'Source')" id="sourcetab">Source</button>
  <button class="tablinks" onclick="openTab(event, 'NonConversion')" id="nonconversiontab">NonConversion</button>
  <div style="float:right;width:200px; padding:9px;font-size:17px;" title="Total Count">
  <label>Total : </label>&nbsp;&nbsp;&nbsp;<span id="count"></span>
</div>
</div>
<script>
$(document).ready(function(){
  document.getElementById("persontab").click();
});
</script>
<div id="Person" class="tabcontent" style="margin-left:50px;">
 <?php
        if($super_flag == '1'){ ?>
        <button id="person_excel" class="btn btn-md" style="margin-top:10px;margin-left:900px;float-left;background-color:#00695C;color:#fafafa;"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;&nbsp;Export</button>
        <?php } ?>
  <div align="center">
	<div style="margin-top:20px;max-width:85%;overflow-y:auto;">
		<table id="person_table" class="table table-striped table-hover" >
			<thead style="background-color: #B2DFDB;">
				<th></th>
				<th>Leads</th>
				<th>GoAxled</th>
				<th>Followups</th>
				<th>Cancelled</th>
				<th>Others</th>				
				<th>Idle</th>
                <th>Duplicate</th>
			</thead>
			<tbody>
				<?php
					$sql_person = "SELECT crm_log_id,name FROM crm_admin ORDER BY name ASC";
                    $res_person = mysqli_query($conn,$sql_person) or die(mysqli_error($conn));
                    
                    // initialize total count
                    $no_lp_total = 0;$no_bp_total = 0;$no_fp_total = 0;$no_cp_total = 0;$no_op_total = 0;$no_ip_total = 0;$no_dp_total=0;
                    
					while($row_person = mysqli_fetch_object($res_person)){
						$crm_id = $row_person->crm_log_id;
                        $crm_name=$row_person->name;
                        
                       $sql_p = $city == "all" ? "SELECT booking_id,flag,booking_status,flag_fo,log,activity_status,flag_unwntd FROM user_booking_tb WHERE crm_update_id='$crm_id' AND mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' " : "SELECT booking_id,flag,booking_status,flag_fo,log,activity_status,flag_unwntd FROM user_booking_tb WHERE crm_update_id='$crm_id' AND city='$city' AND mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' ";
                        $res_p = mysqli_query($conn,$sql_p);


                        $no_rows_p = mysqli_num_rows($res_p);
                        if($no_rows_p < 1){
                            continue;
                        }

                        //Person counts
                        $no_lp = 0;$no_bp = 0;$no_fp = 0;$no_cp = 0;$no_op = 0;$no_ip = 0;$no_dp=0;

                        while($row_p = mysqli_fetch_object($res_p)){
                            $booking_flag_p = $row_p->flag;
                            $status_p = $row_p->booking_status;
                            $flag_fo_p = $row_p->flag_fo;
                            $flag_unwntd_p = $row_p->flag_unwntd;
                            $activity_status_p = $row_p->activity_status;

                            if($flag_unwntd_p == '1'){
                                if($booking_flag_p == '1' && $activity_status_p=='26'){
                                    $no_dp=$no_dp+1;
                                    $no_dp_total=$no_dp_total+1;
                                }
                            }
                            else{
                                $no_lp=$no_lp+1;
                                $no_lp_total=$no_lp_total+1;
    
                                switch($status_p){
                                    case '1':if($booking_flag_p == '1'){
                                                $no_cp=$no_cp+1;
                                                $no_cp_total=$no_cp_total+1;
                                             }
                                            else{
                                                if($flag_fo_p == '1'){
                                                    $no_fp=$no_fp+1;
                                                    $no_fp_total=$no_fp_total+1;
                                                }
                                                else{
                                                    $no_ip=$no_ip+1;
                                                    $no_ip_total=$no_ip_total+1;
                                                }
                                            }
                                            break;
                                    case '2':if($booking_flag_p == '1')
                                            {
                                                $no_cp=$no_cp+1;
                                                $no_cp_total=$no_cp_total+1;
                                            }
                                            else{
                                                $no_bp=$no_bp+1;
                                                $no_bp_total=$no_bp_total+1;
                                            }
                                            break;
                                    case '3':
                                    case '4':
                                    case '5':
                                    case '6': if($booking_flag_p != '1'){
                                                $no_fp=$no_fp+1;
                                                $no_fp_total=$no_fp_total+1;
                                            }
                                            else{
                                                $no_cp=$no_cp+1;
                                                $no_cp_total=$no_cp_total+1;
                                            }
    
                                            break;
                                    case '0':if($booking_flag_p != '1'){
                                                $no_op=$no_op+1;
                                                $no_op_total=$no_op_total+1;
                                            }
                                            else{
                                                $no_cp=$no_cp+1;
                                                $no_cp_total=$no_cp_total+1;
                                            }
                                            break;
                                } // switch
                            } // else
                        } // while
					?>
					<tr>
						
                        <?php if($no_lp == 0){ 
                             continue; 
                             }
                        else{ ?>
                        <td><?php echo $crm_name; ?></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-crmlogid="<?php echo $crm_id; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'lead';?>" data-target="#myModalperson"><?php echo $no_lp;?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-crmlogid="<?php echo $crm_id; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'booking';?>" data-target="#myModalperson"><?php echo $no_bp." (".floor(($no_bp/$no_lp)*100)."%)";?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-crmlogid="<?php echo $crm_id; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'followup';?>" data-target="#myModalperson"><?php echo $no_fp." (".floor(($no_fp/$no_lp)*100)."%)";?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-crmlogid="<?php echo $crm_id; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'cancelled';?>" data-target="#myModalperson"><?php echo $no_cp." (".floor(($no_cp/$no_lp)*100)."%)";?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-crmlogid="<?php echo $crm_id; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'others';?>" data-target="#myModalperson"><?php echo $no_op." (".floor(($no_op/$no_lp)*100)."%)";?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-crmlogid="<?php echo $crm_id; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'idle';?>" data-target="#myModalperson"><?php echo $no_ip." (".floor(($no_ip/$no_lp)*100)."%)";?></button></td>
                        <td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-crmlogid="<?php echo $crm_id; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'duplicate';?>" data-target="#myModalperson"><?php echo $no_dp;?></button></td>
                        <?php } ?>
					</tr>

					<?php   }
				?>
                <tr>
                <td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);">Total</td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-crmlogid="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'lead';?>" data-target="#myModalperson"><?php echo $no_lp_total; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-crmlogid="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'booking';?>" data-target="#myModalperson"><?php echo $no_bp_total."(".floor(($no_bp_total/$no_lp_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-crmlogid="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'followup';?>" data-target="#myModalperson"><?php echo $no_fp_total."(".floor(($no_fp_total/$no_lp_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-crmlogid="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'cancelled';?>" data-target="#myModalperson"><?php echo $no_cp_total."(".floor(($no_cp_total/$no_lp_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-crmlogid="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'others';?>" data-target="#myModalperson"><?php echo $no_op_total."(".floor(($no_op_total/$no_lp_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-crmlogid="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'idle';?>" data-target="#myModalperson"><?php echo $no_ip_total."(".floor(($no_ip_total/$no_lp_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-crmlogid="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'duplicate';?>" data-target="#myModalperson"><?php echo $no_dp_total; ?></button></td>
                </tr>
			</tbody>
		</table>
	</div>
  </div>
</div>

<div id="Service" class="tabcontent" style="margin-left:50px;max-width:99%;" align="center">
	<div class="tab2" style="margin-top:10px; margin-left:10px;max-width:99%;">
			<button class="tablinks2" onclick="openTab2(event, 'all_service')" id="alltab_service"> All </button>
			<button class="tablinks2" onclick="openTab2(event, '2w_service')" id="2wtab_service"> 2W </button>
			<button class="tablinks2" onclick="openTab2(event, '4w_service')" id="4wtab_service"> 4W </button>
	</div>
	<!-- ----------- all------------------- -->
		<div id="all_service" class="tabcontent2" style="margin-left:10px;max-width:99%;">
		<?php
        if($super_flag == '1'){ ?>
        <button id="service_excel_all" class="btn btn-md" style="margin-top:10px;margin-left:700px;float-left;background-color:#00695C;color:#fafafa;"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;&nbsp;Export</button>
        <?php } ?>
		<div align="center">
	   <div style="margin-top:10px;max-width:95%;overflow-y:auto;">
       <table id="service_table_all"  class="table table-striped table-hover" >
            <thead style="background-color: #B2DFDB;">
				<th></th>
				<th>Type</th>
				<th>Leads</th>
				<th>GoAxled</th>
				<th>Followups</th>
				<th>Cancelled</th>
				<th>Others</th>				
				<th>Idle</th>
                <th>Duplicate</th>
            </thead>
            <tbody>
				<?php
					$sql_service_all = "SELECT DISTINCT master_service,type FROM go_axle_service_price_tbl ORDER BY master_service ASC";
                    $res_service_all = mysqli_query($conn,$sql_service_all) or die(mysqli_error($conn));
                    
                    $no_lsr_all_total = 0;$no_bsr_all_total = 0;$no_fsr_all_total = 0;$no_csr_all_total = 0;$no_osr_all_total = 0;$no_isr_all_total = 0;$no_dsr_all_total = 0;
                    
					while($row_service_all = mysqli_fetch_object($res_service_all)){
                        $master_service_all = $row_service_all->master_service;
                        $veh_type = $row_service_all->type;

                        $sql_get_services_all = "SELECT service_type FROM go_axle_service_price_tbl WHERE master_service='$master_service_all' AND type='$veh_type'";
                        $res_get_services_all = mysqli_query($conn,$sql_get_services_all);

                        $services_all = '';

                        while($row_get_services_all = mysqli_fetch_array($res_get_services_all)){
                            if($services_all == ""){
                                $services_all = "'".$row_get_services_all['service_type']."'";
                            }
                            else{
                                $services_all = $services_all.",'".$row_get_services_all['service_type']."'";
                            }
                        }             
                        
                        //Service counts
                        //all
                        $no_lsr_all = 0;$no_bsr_all = 0;$no_fsr_all = 0;$no_csr_all = 0;$no_osr_all = 0;$no_isr_all = 0;$no_dsr_all = 0;
                        
                        $sql_sr_all = $city == "all" ? "SELECT booking_id,flag,booking_status,flag_fo,log,activity_status,flag_unwntd FROM user_booking_tb WHERE service_type IN($services_all) AND vehicle_type='$veh_type' AND mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' " : "SELECT booking_id,flag,booking_status,flag_fo,log,activity_status,flag_unwntd FROM user_booking_tb WHERE service_type IN($services_all) AND vehicle_type='$veh_type' AND city='$city' AND mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' ";
                        $res_sr_all = mysqli_query($conn,$sql_sr_all);

                        while($row_sr_all = mysqli_fetch_object($res_sr_all)){
                            $booking_flag_sra = $row_sr_all->flag;
                            $status_sra = $row_sr_all->booking_status;
                            $flag_fo_sra = $row_sr_all->flag_fo;
                            $flag_unwntd_sra = $row_sr_all->flag_unwntd;
                            $activity_status_sra = $row_sr_all->activity_status;

                            if($flag_unwntd_sra == '1'){
                                if($booking_flag_sra == '1' && $activity_status_sra == '26'){
                                    $no_dsr_all=$no_dsr_all+1;
                                    $no_dsr_all_total=$no_dsr_all_total+1;
                                }
                            }
                            else{
                                $no_lsr_all=$no_lsr_all+1;
                                $no_lsr_all_total=$no_lsr_all_total+1;
                                switch($status_sra){
                                    case '1':if($booking_flag_sra == '1'){
                                                $no_csr_all=$no_csr_all+1;
                                                $no_csr_all_total=$no_csr_all_total+1;
                                            }
                                            else{
                                                if($flag_fo_sra == '1'){
                                                    $no_fsr_all=$no_fsr_all+1;
                                                    $no_fsr_all_total=$no_fsr_all_total+1;
                                                }
                                                else{
                                                    $no_isr_all=$no_isr_all+1;
                                                    $no_isr_all_total=$no_isr_all_total+1;
                                                }
                                            }
                                            break;
                                    case '2':if($booking_flag_sra == '1'){
                                                $no_csr_all=$no_csr_all+1;
                                                $no_csr_all_total= $no_csr_all_total+1;
                                            }
                                            else{
                                                $no_bsr_all=$no_bsr_all+1;
                                                $no_bsr_all_total=$no_bsr_all_total+1;
                                            }
                                            break;
                                    case '3':
                                    case '4':
                                    case '5':
                                    case '6':if($booking_flag_sra != '1'){
                                                $no_fsr_all=$no_fsr_all+1;
                                                $no_fsr_all_total=$no_fsr_all_total+1;
                                            }
                                            else{
                                                $no_csr_all=$no_csr_all+1;
                                                $no_csr_all_total= $no_csr_all_total+1;
                                            }
                                            break;
                                    case '0':if($booking_flag_sra != '1'){
                                                $no_osr_all=$no_osr_all+1;
                                                $no_osr_all_total=$no_osr_all_total+1;
                                            }
                                            else{
                                                $no_csr_all=$no_csr_all+1;
                                                $no_csr_all_total=$no_csr_all_total+1;
                                            }
                                            break;
                                    }

                            }
                                                        
                            
                        }

						?>
						<tr>
						
						
                        <?php if($no_lsr_all == 0){ 
                             continue; 
                             }
                        else{ ?>
                        <td><?php echo $master_service_all; ?></td>
                        <td style="font-size:13px;"><?php echo $veh_type; ?></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-service="<?php echo $master_service_all; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'lead';?>" data-target="#myModalservice"><?php echo $no_lsr_all?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-service="<?php echo $master_service_all; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'booking';?>" data-target="#myModalservice"><?php echo $no_bsr_all."(".floor(($no_bsr_all/$no_lsr_all)*100)."%)";?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-service="<?php echo $master_service_all; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'followup';?>" data-target="#myModalservice"><?php echo $no_fsr_all."(".floor(($no_fsr_all/$no_lsr_all)*100)."%)";?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-service="<?php echo $master_service_all; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'cnacelled';?>" data-target="#myModalservice"><?php echo $no_csr_all."(".floor(($no_csr_all/$no_lsr_all)*100)."%)";?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-service="<?php echo $master_service_all; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'others';?>" data-target="#myModalservice"><?php echo $no_osr_all."(".floor(($no_osr_all/$no_lsr_all)*100)."%)";?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-service="<?php echo $master_service_all; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'idle';?>" data-target="#myModalservice"><?php echo $no_isr_all."(".floor(($no_isr_all/$no_lsr_all)*100)."%)";?></button></td>
                        <td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-service="<?php echo $master_service_all; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'duplicate';?>" data-target="#myModalservice"><?php echo $no_dsr_all;?></button></td>
                        <?php } ?>
						</tr>
					<?php   }
				?>
                <tr>
                <td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);" colspan="2">Total</td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'lead';?>" data-target="#myModalservice"><?php echo $no_lsr_all_total; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'booking';?>" data-target="#myModalservice"><?php echo $no_bsr_all_total."(".floor(($no_bsr_all_total/$no_lsr_all_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'followup';?>" data-target="#myModalservice"><?php echo $no_fsr_all_total."(".floor(($no_fsr_all_total/$no_lsr_all_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'cancelled';?>" data-target="#myModalservice"><?php echo $no_csr_all_total."(".floor(($no_csr_all_total/$no_lsr_all_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'others';?>" data-target="#myModalservice"><?php echo $no_osr_all_total."(".floor(($no_osr_all_total/$no_lsr_all_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'idle';?>" data-target="#myModalservice"><?php echo $no_isr_all_total."(".floor(($no_isr_all_total/$no_lsr_all_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'duplicate';?>" data-target="#myModalservice"><?php echo $no_dsr_all_total; ?></button></td>
                </tr>
            </tbody>
        </table>
			</div>
		</div>
		</div>
<!-- ----------- all------------------- -->
	<!-- ----------- 2 wheelers------------------- -->
		<div id="2w_service" class="tabcontent2" style="margin-left:10px;max-width:99%;">
		<?php
        if($super_flag == '1'){ ?>
        <button id="service_excel_2w" class="btn btn-md" style="margin-top:10px;margin-left:700px;float-left;background-color:#00695C;color:#fafafa;"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;&nbsp;Export</button>
        <?php } ?>
		<div align="center">
			<div style="margin-top:10px;max-width:95%;overflow-y:auto;">
       <table id="service_table_2w"  class="table table-striped table-hover" >
            <thead style="background-color: #B2DFDB;">
				<th></th>
				<th>Leads</th>
				<th>GoAxled</th>
				<th>Followups</th>
				<th>Cancelled</th>
				<th>Others</th>				
				<th>Idle</th>
                <th>Duplicate</th>
            </thead>
            <tbody>
				<?php
					$sql_service_2w = "SELECT DISTINCT master_service FROM go_axle_service_price_tbl WHERE type='2w' ORDER BY master_service ASC";
                    $res_service_2w = mysqli_query($conn,$sql_service_2w) or die(mysqli_error($conn));
                    
                    $no_lsr_2w_total = 0;$no_bsr_2w_total = 0;$no_fsr_2w_total = 0;$no_csr_2w_total = 0;$no_osr_2w_total = 0;$no_isr_2w_total = 0;$no_dsr_2w_total = 0;

					while($row_service_2w = mysqli_fetch_object($res_service_2w)){
                        $master_service_2w = $row_service_2w->master_service;
                        
                        $sql_get_services_2w = "SELECT service_type FROM go_axle_service_price_tbl WHERE master_service='$master_service_2w' AND type='2w'";
                        $res_get_services_2w = mysqli_query($conn,$sql_get_services_2w);

                        $services_2w = '';

                        while($row_get_services_2w = mysqli_fetch_array($res_get_services_2w)){
                            $services_2w = $services_2w == "" ? "'".$row_get_services_2w['service_type']."'" : $services_2w.",'".$row_get_services_2w['service_type']."'";
                        }             

                        //2w
                        $no_lsr_2w = 0;$no_bsr_2w = 0;$no_fsr_2w = 0;$no_csr_2w = 0;$no_osr_2w = 0;$no_isr_2w = 0;$no_dsr_2w = 0;

                        $sql_sr_2w = $city == "all" ? "SELECT booking_id,flag,booking_status,flag_fo,log,activity_status,flag_unwntd FROM user_booking_tb WHERE service_type IN($services_2w) AND vehicle_type='2w' AND mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' " : "SELECT booking_id,flag,booking_status,flag_fo,log,activity_status,flag_unwntd FROM user_booking_tb WHERE service_type IN($services_2w) AND vehicle_type='2w' AND city='$city' AND mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' ";
                        $res_sr_2w = mysqli_query($conn,$sql_sr_2w);

                        while($row_sr_2w = mysqli_fetch_object($res_sr_2w)){
                            $booking_flag_sr2 = $row_sr_2w->flag;
                            $status_sr2 = $row_sr_2w->booking_status;
                            $flag_fo_sr2 = $row_sr_2w->flag_fo;
                            $flag_unwntd_sr2 = $row_sr_2w->flag_unwntd;
                            $activity_status_sr2 = $row_sr_2w->activity_status;

                            if($flag_unwntd_sr2 == '1'){
                                if($booking_flag_sr2 == '1' && $activity_status_sr2 == '26'){
                                    $no_dsr_2w=$no_dsr_2w+1;
                                    $no_dsr_2w_total=$no_dsr_2w_total+1;
                                }
                            }
                            else{
                                $no_lsr_2w= $no_lsr_2w+1;
                                $no_lsr_2w_total=$no_lsr_2w_total+1;
    
                                switch($status_sr2){
                                    case '1':if($booking_flag_sr2 == '1'){
                                                $no_csr_2w=$no_csr_2w+1;
                                                $no_csr_2w_total=$no_csr_2w_total+1;
                                             }
                                            else{
                                                if($flag_fo_sr2 == '1'){
                                                    $no_fsr_2w=$no_fsr_2w+1;
                                                    $no_fsr_2w_total=$no_fsr_2w_total+1;
                                                }
                                                else{
                                                    $no_isr_2w=$no_isr_2w+1;
                                                    $no_isr_2w_total=$no_isr_2w_total+1;
                                                }
                                            }
                                            break;
                                    case '2':if($booking_flag_sr2 == '1'){
                                                  $no_csr_all=$no_csr_all+1;
                                                  $no_csr_all_total=$no_csr_all_total+1;
                                             }
                                             else{
                                                $no_bsr_2w=$no_bsr_2w+1;
                                                $no_bsr_2w_total= $no_bsr_2w_total+1;
                                             }
                                            break;
                                    case '3':
                                    case '4':
                                    case '5':
                                    case '6':if($booking_flag_sr2 != '1'){
                                                $no_fsr_2w=$no_fsr_2w+1;
                                                $no_fsr_2w_total=$no_fsr_2w_total+1;
                                            }
                                            else{
                                                $no_csr_2w=$no_csr_2w+1;
                                                $no_csr_2w_total=$no_csr_2w_total+1;
                                            }
                                            break;
                                    case '0':if($booking_flag_sr2 != '1'){
                                                $no_osr_2w=$no_osr_2w+1;
                                                $no_osr_2w_total=$no_osr_2w_total+1;
                                            }
                                            else{
                                                $no_csr_2w=$no_csr_2w+1;
                                                $no_csr_2w_total= $no_csr_2w_total+1;
                                            }
                                            break;
                                }
                            }
                        }

						?>
						<tr>
						
                        <?php if($no_lsr_2w == 0){ 
                             continue;
                             }
                        else{ ?>
                        <td><?php echo $master_service_2w; ?></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-service="<?php echo $master_service_2w; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'lead';?>" data-target="#myModalservice"><?php echo $no_lsr_2w?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-service="<?php echo $master_service_2w; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'booking';?>" data-target="#myModalservice"><?php echo $no_bsr_2w."(".floor(($no_bsr_2w/$no_lsr_2w)*100)."%)";?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-service="<?php echo $master_service_2w; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'followup';?>" data-target="#myModalservice"><?php echo $no_fsr_2w."(".floor(($no_fsr_2w/$no_lsr_2w)*100)."%)";?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-service="<?php echo $master_service_2w; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'cancelled';?>" data-target="#myModalservice"><?php echo $no_csr_2w."(".floor(($no_csr_2w/$no_lsr_2w)*100)."%)";?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-service="<?php echo $master_service_2w; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'others';?>" data-target="#myModalservice"><?php echo $no_osr_2w."(".floor(($no_osr_2w/$no_lsr_2w)*100)."%)";?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-service="<?php echo $master_service_2w; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'idle';?>" data-target="#myModalservice"><?php echo $no_isr_2w."(".floor(($no_isr_2w/$no_lsr_2w)*100)."%)";?></button></td>
                        <td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-service="<?php echo $master_service_2w; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'duplicate';?>" data-target="#myModalservice"><?php echo $no_dsr_2w;?></button></td>
                        <?php } ?>
						</tr>
					<?php   }
				?>
                <tr>
                <td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);">Total</td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'lead';?>" data-target="#myModalservice"><?php echo $no_lsr_2w_total; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'booking';?>" data-target="#myModalservice"><?php echo $no_bsr_2w_total."(".floor(($no_bsr_2w_total/$no_lsr_2w_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'followup';?>" data-target="#myModalservice"><?php echo $no_fsr_2w_total."(".floor(($no_fsr_2w_total/$no_lsr_2w_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'cancelled';?>" data-target="#myModalservice"><?php echo $no_csr_2w_total."(".floor(($no_csr_2w_total/$no_lsr_2w_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'others';?>" data-target="#myModalservice"><?php echo $no_osr_2w_total."(".floor(($no_osr_2w_total/$no_lsr_2w_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'idle';?>" data-target="#myModalservice"><?php echo $no_isr_2w_total."(".floor(($no_isr_2w_total/$no_lsr_2w_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'duplicate';?>" data-target="#myModalservice"><?php echo $no_dsr_2w_total; ?></button></td>
                </tr>

            </tbody>
        </table>
			</div>
		</div>
		</div>
<!-- ----------- 2w------------------- -->
	<!-- ----------- 4 wheelers------------------- -->
		<div id="4w_service" class="tabcontent2" style="margin-left:10px;max-width:99%;">
		<?php
        if($super_flag == '1'){ ?>
        <button id="service_excel_4w" class="btn btn-md" style="margin-top:10px;margin-left:700px;float-left;background-color:#00695C;color:#fafafa;"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;&nbsp;Export</button>
        <?php } ?>
		<div align="center">
			<div style="margin-top:10px;max-width:95%;overflow-y:auto;">
       <table id="service_table_4w"  class="table table-striped table-hover" >
            <thead style="background-color: #B2DFDB;">
				<th></th>
				<th>Leads</th>
				<th>GoAxled</th>
				<th>Followups</th>
				<th>Cancelled</th>
				<th>Others</th>				
				<th>Idle</th>
                <th>Duplicate</th>
            </thead>
            <tbody>
				<?php
					$sql_service_4w = "SELECT DISTINCT master_service FROM go_axle_service_price_tbl WHERE type='4w' ORDER BY master_service ASC";
                    $res_service_4w = mysqli_query($conn,$sql_service_4w) or die(mysqli_error($conn));
                    
                    $no_lsr_4w_total = 0;$no_bsr_4w_total = 0;$no_fsr_4w_total = 0;$no_csr_4w_total = 0;$no_osr_4w_total = 0;$no_isr_4w_total = 0;$no_dsr_4w_total = 0;
                    
					while($row_service_4w = mysqli_fetch_object($res_service_4w)){
                        $master_service_4w = $row_service_4w->master_service;
                        //echo $master_service_4w;
                        
                        $sql_get_services_4w = "SELECT service_type FROM go_axle_service_price_tbl WHERE master_service='$master_service_4w' AND type='4w'";
                        $res_get_services_4w = mysqli_query($conn,$sql_get_services_4w);

                        $services_4w = '';

                        while($row_get_services_4w = mysqli_fetch_array($res_get_services_4w)){
                            $services_4w = $services_4w == "" ? "'".$row_get_services_4w['service_type']."'" : $services_4w.",'".$row_get_services_4w['service_type']."'";
                        }      

                        //4w
                        $no_lsr_4w = 0;$no_bsr_4w = 0;$no_fsr_4w = 0;$no_csr_4w = 0;$no_osr_4w = 0;$no_isr_4w = 0;$no_dsr_4w = 0;
                        
                        $sql_sr_4w = $city == "all" ? "SELECT booking_id,flag,booking_status,flag_fo,log,activity_status,flag_unwntd FROM user_booking_tb WHERE service_type IN($services_4w) AND vehicle_type='4w' AND mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' " : "SELECT booking_id,flag,booking_status,flag_fo,log,activity_status,flag_unwntd FROM user_booking_tb WHERE service_type IN($services_4w) AND vehicle_type='4w' AND city='$city' AND mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' ";
                        $res_sr_4w = mysqli_query($conn,$sql_sr_4w);

                        while($row_sr_4w = mysqli_fetch_object($res_sr_4w)){
                            $booking_flag_sr4 = $row_sr_4w->flag;
                            $status_sr4 = $row_sr_4w->booking_status;
                            $flag_fo_sr4 = $row_sr_4w->flag_fo;
                            $flag_unwntd_sr4 = $row_sr_4w->flag_unwntd;
                            $activity_status_sr4 = $row_sr_4w->activity_status;

                            if($flag_unwntd_sr4 == '1'){
                                if($booking_flag_sr4 == '1' && $activity_status_sr4 == '26'){
                                    $no_dsr_4w=$no_dsr_4w+1;
                                    $no_dsr_4w_total=$no_dsr_4w_total+1;
                                }
                            }
                            else{
                                $no_lsr_4w=$no_lsr_4w+1;
                                $no_lsr_4w_total=$no_lsr_4w_total+1;
    
                                switch($status_sr4){
                                    case '1':if($booking_flag_sr4 == '1'){
                                                $no_csr_4w=$no_csr_4w+1;
                                                $no_csr_4w_total= $no_csr_4w_total+1;
                                            }
                                            else{
                                                if($flag_fo_sr4 == '1'){
                                                    $no_fsr_4w=$no_fsr_4w+1;
                                                    $no_fsr_4w_total=$no_fsr_4w_total+1;
                                                }
                                                else{
                                                    $no_isr_4w=$no_isr_4w+1;
                                                    $no_isr_4w_total=$no_isr_4w_total+1;
                                                }
                                            }
                                            break;
                                    case '2':if($booking_flag_sr4 == '1'){
                                                $no_csr_4w=$no_csr_4w+1;
                                                $no_csr_4w_total=$no_csr_4w_total+1;
                                             }
                                            else{
                                                $no_bsr_4w= $no_bsr_4w+1;
                                                $no_bsr_4w_total=$no_bsr_4w_total+1;
                                            }
                                            break;
                                    case '3':
                                    case '4':
                                    case '5':
                                    case '6':if($booking_flag_sr4 != '1'){
                                                $no_fsr_4w=$no_fsr_4w+1;
                                                $no_fsr_4w_total=$no_fsr_4w_total+1;
                                            }
                                            else{
                                                $no_csr_4w=$no_csr_4w+1;
                                                $no_csr_4w_total=$no_csr_4w_total+1;
                                            }
                                            break;
                                    case '0':if($booking_flag_sr4 != '1'){
                                                $no_osr_4w=$no_osr_4w+1;
                                                $no_osr_4w_total=$no_osr_4w_total+1;
                                            }
                                            else{
                                                $no_csr_4w=$no_csr_4w+1;
                                                $no_csr_4w_total=$no_csr_4w_total+1;
                                            }
                                            break;
                                }
                            }
                        }

						?>
						<tr>
						
                        <?php if($no_lsr_4w == 0){ 
                             continue; 
                             }
                        else{ ?>
                        <td><?php echo $master_service_4w; ?></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-service="<?php echo $master_service_4w; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'lead';?>" data-target="#myModalservice"><?php echo $no_lsr_4w,"(100%)";?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-service="<?php echo $master_service_4w; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'booking';?>" data-target="#myModalservice"><?php echo $no_bsr_4w."(".floor(($no_bsr_4w/$no_lsr_4w)*100)."%)";?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-service="<?php echo $master_service_4w; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'followup';?>" data-target="#myModalservice"><?php echo $no_fsr_4w."(".floor(($no_fsr_4w/$no_lsr_4w)*100)."%)";?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-service="<?php echo $master_service_4w; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'cancelled';?>" data-target="#myModalservice"><?php echo $no_csr_4w."(".floor(($no_csr_4w/$no_lsr_4w)*100)."%)";?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-service="<?php echo $master_service_4w; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'others';?>" data-target="#myModalservice"><?php echo $no_osr_4w."(".floor(($no_osr_4w/$no_lsr_4w)*100)."%)";?></button></td>
						<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-service="<?php echo $master_service_4w; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'idle';?>" data-target="#myModalservice"><?php echo $no_isr_4w."(".floor(($no_isr_4w/$no_lsr_4w)*100)."%)";?></button></td>
                        <td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-service="<?php echo $master_service_4w; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'duplicate';?>" data-target="#myModalservice"><?php echo $no_dsr_4w;?></button></td>
                        <?php } ?>
						</tr>
					<?php   }
				?>
                <tr>
                <td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);">Total</td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'lead';?>" data-target="#myModalservice"><?php echo $no_lsr_4w_total; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'booking';?>" data-target="#myModalservice"><?php echo $no_bsr_4w_total."(".floor(($no_bsr_4w_total/$no_lsr_4w_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'followup';?>" data-target="#myModalservice"><?php echo $no_fsr_4w_total."(".floor(($no_fsr_4w_total/$no_lsr_4w_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'cancelled';?>" data-target="#myModalservice"><?php echo $no_csr_4w_total."(".floor(($no_csr_4w_total/$no_lsr_4w_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'others';?>" data-target="#myModalservice"><?php echo $no_osr_4w_total."(".floor(($no_osr_4w_total/$no_lsr_4w_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'idle';?>" data-target="#myModalservice"><?php echo $no_isr_4w_total."(".floor(($no_isr_4w_total/$no_lsr_4w_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-service="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'duplicate';?>" data-target="#myModalservice"><?php echo $no_dsr_4w_total; ?></button></td>
                </tr>

            </tbody>
        </table>
			</div>
		</div>
		</div>
<!-- ----------- 4 wheelers------------------- -->
<script>
function openTab2(evt, vehicle2) {
    // Declare all variables
    var i, tabcontent2, tablinks2;

    // Get all elements with class="tabcontent" and hide them
    tabcontent2 = document.getElementsByClassName("tabcontent2");
    for (i = 0; i < tabcontent2.length; i++) {
        tabcontent2[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks2 = document.getElementsByClassName("tablinks2");
    for (i = 0; i < tablinks2.length; i++) {
        tablinks2[i].className = tablinks2[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(vehicle2).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>

<script>
$(document).ready(function(){
  document.getElementById("alltab_service").click();
});
</script>
<script>
 $(function() {
    var startDate = "<?php echo date('d-m-Y',strtotime($start)); ?>";
    var endDate = "<?php echo date('d-m-Y',strtotime($end)); ?>";
    $("#service_excel_all").click(function(){
        $("#service_table_all").table2excel({
           // exclude: ".noExl",
            //name: "Excel Document Name",
            filename:  "Conversion(ServiceView) from " + startDate + " to " + endDate
        }); 
    });
   // $("#service_table_all").dataTable();
  });
</script>
<script>
 $(function() {
    var startDate = "<?php echo date('d-m-Y',strtotime($start)); ?>";
    var endDate = "<?php echo date('d-m-Y',strtotime($end)); ?>";
    $("#service_excel_2w").click(function(){
        $("#service_table_2w").table2excel({
           // exclude: ".noExl",
            //name: "Excel Document Name",
            filename:  "Conversion(ServiceView-2w) from " + startDate + " to " + endDate
        }); 
    });
    //$("#service_table_2w").dataTable();
  });
</script>
<script>
 $(function() {
    var startDate = "<?php echo date('d-m-Y',strtotime($start)); ?>";
    var endDate = "<?php echo date('d-m-Y',strtotime($end)); ?>";
    $("#service_excel_4w").click(function(){
        $("#service_table_4w").table2excel({
           // exclude: ".noExl",
            //name: "Excel Document Name",
            filename:  "Conversion(ServiceView-4w) from " + startDate + " to " + endDate
        }); 
    });
   // $("#service_table_4w").dataTable();
  });
</script>
</div>

<div id="Source" class="tabcontent" style="margin-left:50px;max-width:99%;" align="center">
		<div class="tab1" style="margin-top:10px; margin-left:10px;">
			<button class="tablinks1" onclick="openTab1(event, 'all_source')" id="alltab_source"> All </button>
			<button class="tablinks1" onclick="openTab1(event, '2w_source')" id="2wtab_source"> 2W </button>
			<button class="tablinks1" onclick="openTab1(event, '4w_source')" id="4wtab_source"> 4W </button>
		</div>
<!-- ----------- all------------------- -->
		<div id="all_source" class="tabcontent1" style="margin-left:10px;">
		<?php
        if($super_flag == '1'){ ?>
        <button id="source_excel_all" class="btn btn-md" style="margin-top:10px;margin-left:700px;float-left;background-color:#00695C;color:#fafafa;"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;&nbsp;Export</button>
        <?php } ?>
		<div align="center">
			<div style="margin-top:10px;max-width:95%;overflow-y:auto;">
			<table id="source_table_all" class="table table-striped table-hover" >
					<thead style="background-color: #B2DFDB;">
						<th></th>
						<th>Leads</th>
						<th>GoAxled</th>
						<th>Followups</th>
						<th>Cancelled</th>
						<th>Others</th>				
						<th>Idle</th>
                        <th>Duplicate</th>
					</thead>
					<tbody>
						<?php
				        $sql_source = "SELECT user_source FROM user_source_tbl WHERE flag='0' ORDER By user_source ASC";
                        $res_source = mysqli_query($conn,$sql_source) or die(mysqli_error($conn));
                        
                        $no_ls_all_total = 0;$no_bs_all_total = 0;$no_fs_all_total = 0;$no_cs_all_total = 0;$no_os_all_total = 0;$no_is_all_total = 0;$no_ds_all_total = 0;
                        
						while($row_source = mysqli_fetch_object($res_source)){
							$source = $row_source->user_source;

                            //Source counts
                            //all
                            $no_ls_all = 0;$no_bs_all = 0;$no_fs_all = 0;$no_cs_all = 0;$no_os_all = 0;$no_is_all = 0;$no_ds_all = 0;
                           
                            $sql_s_all = $city == "all" ? "SELECT booking_id,flag,booking_status,flag_fo,log,activity_status,flag_unwntd FROM user_booking_tb WHERE source='$source' AND mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' " : "SELECT booking_id,flag,booking_status,flag_fo,log,activity_status,flag_unwntd FROM user_booking_tb WHERE source='$source' AND city='$city' AND mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' ";
                            $res_s_all = mysqli_query($conn,$sql_s_all);

                            while($row_s_all = mysqli_fetch_object($res_s_all)){
                                $booking_flag_sa = $row_s_all->flag;
                                $status_sa = $row_s_all->booking_status;
                                $flag_fo_sa = $row_s_all->flag_fo;
                                $flag_unwntd_sa = $row_s_all->flag_unwntd;
                                $activity_status_sa = $row_s_all->activity_status;

                                if($flag_unwntd_sa == '1'){
                                    if($booking_flag_sa == '1' && $activity_status_sa == '26'){
                                        $no_ds_all=$no_ds_all+1;
                                        $no_ds_all_total=$no_ds_all_total+1;
                                    }
                                }
                                else{
                                    $no_ls_all=$no_ls_all+1;
                                    $no_ls_all_total=$no_ls_all_total+1;
        
                                    switch($status_sa){
                                        case '1':if($booking_flag_sa == '1'){
                                                    $no_cs_all=$no_cs_all+1;
                                                    $no_cs_all_total=$no_cs_all_total+1;
                                                }
                                                else{
                                                    if($flag_fo_sa == '1'){
                                                        $no_fs_all=$no_fs_all+1;
                                                        $no_fs_all_total=$no_fs_all_total+1;
                                                    }
                                                    else{
                                                        $no_is_all=$no_is_all+1;
                                                        $no_is_all_total=$no_is_all_total+1;
                                                    }
                                                }
                                                break;
                                         case '2':if($booking_flag_sa == '1'){
                                                    $no_cs_all=$no_cs_all+1;
                                                    $no_cs_all_total=$no_cs_all_total+1;
                                                }
                                                else{
                                                    $no_bs_all=$no_bs_all+1;
                                                    $no_bs_all_total=$no_bs_all_total+1;
                                                }
                                                break;
                                        case '3':
                                        case '4':
                                        case '5':
                                        case '6':if($booking_flag_sa != '1'){
                                                    $no_fs_all=$no_fs_all+1;
                                                    $no_fs_all_total=$no_fs_all_total+1;
                                                }
                                                else{
                                                    $no_cs_all= $no_cs_all+1;
                                                    $no_cs_all_total=$no_cs_all_total+1;
                                                }
                                                break;
                                        case '0':if($booking_flag_sa != '1'){
                                                    $no_os_all=$no_os_all+1;
                                                    $no_os_all_total=$no_os_all_total+1;
                                                }
                                                else{
                                                    $no_cs_all=$no_cs_all+1;
                                                    $no_cs_all_total= $no_cs_all_total+1;
                                                }
                                                break;
                                    }
                                }
                           }

						?>
						<tr>
							
                            <?php if($no_ls_all == 0){  continue;  }
                            else{ ?>
                            <td><?php echo $source; ?></td>
							<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'lead';?>" data-target="#myModalsource"><?php  echo $no_ls_all; ?></button></td>
							<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'booking';?>" data-target="#myModalsource"><?php  echo $no_bs_all."(".floor(($no_bs_all/$no_ls_all)*100)."%)"; ?></button></td>
							<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'followup';?>" data-target="#myModalsource"><?php  echo $no_fs_all."(".floor(($no_fs_all/$no_ls_all)*100)."%)"; ?></button></td>
							<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'cancelled';?>" data-target="#myModalsource"><?php  echo $no_cs_all."(".floor(($no_cs_all/$no_ls_all)*100)."%)"; ?></button></td>
							<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'others';?>" data-target="#myModalsource"><?php  echo $no_os_all."(".floor(($no_os_all/$no_ls_all)*100)."%)"; ?></button></td>
							<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'idle';?>" data-target="#myModalsource"><?php  echo $no_is_all."(".floor(($no_is_all/$no_ls_all)*100)."%)"; ?></button></td>
                            <td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'duplicate';?>" data-target="#myModalsource"><?php  echo $no_ds_all; ?></button></td>
                            <?php } ?>
						</tr>
						<?php   }
						?>
                <tr>
                <td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);">Total</td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'lead';?>" data-target="#myModalsource"><?php echo $no_ls_all_total; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'booking';?>" data-target="#myModalsource"><?php echo $no_bs_all_total."(".floor(($no_bs_all_total/$no_ls_all_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'followup';?>" data-target="#myModalsource"><?php echo $no_fs_all_total."(".floor(($no_fs_all_total/$no_ls_all_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'cancelled';?>" data-target="#myModalsource"><?php echo $no_cs_all_total."(".floor(($no_cs_all_total/$no_ls_all_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'others';?>" data-target="#myModalsource"><?php echo $no_os_all_total."(".floor(($no_os_all_total/$no_ls_all_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'idle';?>" data-target="#myModalsource"><?php echo $no_is_all_total."(".floor(($no_is_all_total/$no_ls_all_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="all" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'duplicate';?>" data-target="#myModalsource"><?php echo $no_ds_all_total; ?></button></td>
                </tr>

					</tbody>
				</table>
			</div>
		</div>
		</div>
<!-- ----------- all------------------- -->
<!-- ----------- 2wheelers------------------- -->
		<div id="2w_source" class="tabcontent1" style="margin-left:10px;">
		<?php
        if($super_flag == '1'){ ?>
        <button id="source_excel_2w" class="btn btn-md" style="margin-top:10px;margin-left:700px;float-left;background-color:#00695C;color:#fafafa;"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;&nbsp;Export</button>
        <?php } ?>
		<div align="center">
			<div style="margin-top:10px;max-width:95%;overflow-y:auto;">
			<table id="source_table_2w" class="table table-striped table-hover" >
					<thead style="background-color: #B2DFDB;">
						<th></th>
						<th>Leads</th>
						<th>GoAxled</th>
						<th>Followups</th>
						<th>Cancelled</th>
						<th>Others</th>				
						<th>Idle</th>
                        <th>Duplicate</th>
					</thead>
					<tbody>
						<?php
				        $sql_source = "SELECT user_source FROM user_source_tbl WHERE flag='0' ORDER By user_source ASC";
                        $res_source = mysqli_query($conn,$sql_source) or die(mysqli_error($conn));
                        
                        $no_ls_2w_total = 0;$no_bs_2w_total = 0;$no_fs_2w_total = 0;$no_cs_2w_total = 0;$no_os_2w_total = 0;$no_is_2w_total = 0;$no_ds_2w_total = 0;
                        
						while($row_source = mysqli_fetch_object($res_source)){
							$source = $row_source->user_source;
                            
                            //2w
                            $no_ls_2w = 0;$no_bs_2w = 0;$no_fs_2w = 0;$no_cs_2w = 0;$no_os_2w = 0;$no_is_2w = 0;$no_ds_2w = 0;
                            
                            $sql_s_2w = $city == "all" ? "SELECT booking_id,flag,booking_status,flag_fo,log,activity_status,flag_unwntd FROM user_booking_tb WHERE source='$source' AND vehicle_type='2w' AND mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' " : "SELECT booking_id,flag,booking_status,flag_fo,log,activity_status,flag_unwntd FROM user_booking_tb WHERE source='$source' AND vehicle_type='2w' AND city='$city' AND mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' ";
                            $res_s_2w = mysqli_query($conn,$sql_s_2w);

                            while($row_s_2w = mysqli_fetch_object($res_s_2w)){
                                $booking_flag_s2 = $row_s_2w->flag;
                                $status_s2 = $row_s_2w->booking_status;
                                $flag_fo_s2 = $row_s_2w->flag_fo;
                                $flag_unwntd_s2 = $row_s_2w->flag_unwntd;
                                $activity_status_s2 = $row_s_2w->activity_status;

                                if($flag_unwntd_s2 == '1'){
                                    if($booking_flag_s2 == '1' && $activity_status_s2 =='26'){
                                        $no_ds_2w=$no_ds_2w+1;
                                        $no_ds_2w_total=$no_ds_2w_total+1;
                                    }
                                }
                                else{
                                    $no_ls_2w= $no_ls_2w+1;
                                    $no_ls_2w_total=$no_ls_2w_total+1;
                                    
                                    switch($status_s2){
                                        case '1':if($booking_flag_s2 == '1'){
                                                       $no_cs_2w=$no_cs_2w+1;
                                                       $no_cs_2w_total=$no_cs_2w_total+1;
                                                    }
                                                else{
                                                    if($flag_fo_s2 == '1'){
                                                        $no_fs_2w=$no_fs_2w+1;
                                                        $no_fs_2w_total=$no_fs_2w_total+1;
                                                    }
                                                    else{
                                                        $no_is_2w=$no_is_2w+1;
                                                        $no_is_2w_total=$no_is_2w_total+1;
                                                    }
                                                }
                                                break;
                                        case '2':if($booking_flag_s2 == '1'){
                                                    $no_cs_2w=$no_cs_2w+1;
                                                    $no_cs_2w_total=$no_cs_2w_total+1;
                                                 }
                                                else{
                                                    $no_bs_2w=$no_bs_2w+1;
                                                    $no_bs_2w_total=$no_bs_2w_total+1;
                                                }
                                                break;
                                        case '3':
                                        case '4':
                                        case '5':
                                        case '6':if($booking_flag_s2 != '1'){
                                                    $no_fs_2w=$no_fs_2w+1;
                                                    $no_fs_2w_total=$no_fs_2w_total+1;
                                                }
                                                else{
                                                    $no_cs_2w=$no_cs_2w+1;
                                                    $no_cs_2w_total=$no_cs_2w_total+1;
                                                }
                                                break;
                                        case '0':if($booking_flag_s2 != '1'){
                                                    $no_os_2w=$no_os_2w+1;
                                                    $no_os_2w_total=$no_os_2w_total+1;
                                                }
                                                else{
                                                    $no_cs_2w=$no_cs_2w+1;
                                                    $no_cs_2w_total=$no_cs_2w_total+1;
                                                }
                                                break;
                                    }
                                }

                            }
						?>
                        <tr>
                            
                            <?php if($no_ls_2w == 0){  continue;  }
                            else{ ?>
                            <td><?php echo $source; ?></td>
                            <td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'lead';?>" data-target="#myModalsource"><?php echo $no_ls_2w;?></button></td>
                            <td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'booking';?>" data-target="#myModalsource"><?php echo $no_bs_2w."(".floor(($no_bs_2w/$no_ls_2w)*100)."%)"; ?></button></td>
                            <td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'followup';?>" data-target="#myModalsource"><?php echo $no_fs_2w."(".floor(($no_fs_2w/$no_ls_2w)*100)."%)"; ?></button></td>
                            <td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'cancelled';?>" data-target="#myModalsource"><?php echo $no_cs_2w."(".floor(($no_cs_2w/$no_ls_2w)*100)."%)"; ?></button></td>
                            <td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'others';?>" data-target="#myModalsource"><?php echo $no_os_2w."(".floor(($no_os_2w/$no_ls_2w)*100)."%)"; ?></button></td>
                            <td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'idle';?>" data-target="#myModalsource"><?php echo $no_is_2w."(".floor(($no_is_2w/$no_ls_2w)*100)."%)"; ?></button></td>
                            <td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'duplicate';?>" data-target="#myModalsource"><?php echo $no_ds_2w; ?></button></td>

                            <?php } ?>
                        </tr>
                    <?php   
                }
                ?>
                <tr>
                    <td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);">Total</td>
                    <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'lead';?>" data-target="#myModalsource"><?php echo $no_ls_2w_total; ?></button></td>
                    <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'booking';?>" data-target="#myModalsource"><?php echo $no_bs_2w_total."(".floor(($no_bs_2w_total/$no_ls_2w_total)*100)."%)"; ?></button></td>
                    <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'followup';?>" data-target="#myModalsource"><?php echo $no_fs_2w_total."(".floor(($no_fs_2w_total/$no_ls_2w_total)*100)."%)"; ?></button></td>
                    <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'cancelled';?>" data-target="#myModalsource"><?php echo $no_cs_2w_total."(".floor(($no_cs_2w_total/$no_ls_2w_total)*100)."%)"; ?></button></td>
                    <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'others';?>" data-target="#myModalsource"><?php echo $no_os_2w_total."(".floor(($no_os_2w_total/$no_ls_2w_total)*100)."%)"; ?></button></td>
                    <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'idle';?>" data-target="#myModalsource"><?php echo $no_is_2w_total."(".floor(($no_is_2w_total/$no_ls_2w_total)*100)."%)"; ?></button></td>
                    <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'duplicate';?>" data-target="#myModalsource"><?php echo $no_ds_2w_total; ?></button></td>
                </tr>
            </tbody>				
        </table>
	</div>
   </div>
</div>
<!-- ----------- 2wheelers------------------- -->
<!-- ----------- 4wheelers------------------- -->
		<div id="4w_source" class="tabcontent1" style="margin-left:10px;">
		<?php
        if($super_flag == '1'){ ?>
        <button id="source_excel_4w" class="btn btn-md" style="margin-top:10px;margin-left:700px;float-left;background-color:#00695C;color:#fafafa;"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;&nbsp;Export</button>
        <?php } ?>
		<div align="center">
			<div style="margin-top:10px;max-width:95%;overflow-y:auto;">
			<table id="source_table_4w" class="table table-striped table-hover" >
					<thead style="background-color: #B2DFDB;">
						<th></th>
						<th>Leads</th>
						<th>GoAxled</th>
						<th>Followups</th>
						<th>Cancelled</th>
						<th>Others</th>				
						<th>Idle</th>
                        <th>Duplicate</th>
					</thead>
					<tbody>
						<?php
				        $sql_source = "SELECT user_source FROM user_source_tbl WHERE flag='0' ORDER By user_source ASC";
                        $res_source = mysqli_query($conn,$sql_source) or die(mysqli_error($conn));
                        
                        $no_ls_4w_total = 0;$no_bs_4w_total = 0;$no_fs_4w_total = 0;$no_cs_4w_total = 0;$no_os_4w_total = 0;$no_is_4w_total = 0;$no_ds_4w_total = 0;
                        
						while($row_source = mysqli_fetch_object($res_source)){
							$source = $row_source->user_source;

                            //4w
                            $no_ls_4w = 0;$no_bs_4w = 0;$no_fs_4w = 0;$no_cs_4w = 0;$no_os_4w = 0;$no_is_4w = 0;$no_ds_4w = 0;
                            
                            $sql_s_4w = $city == "all" ? "SELECT booking_id,flag,booking_status,flag_fo,log,activity_status,flag_unwntd FROM user_booking_tb WHERE source='$source' AND vehicle_type='4w' AND mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' " :  "SELECT booking_id,flag,booking_status,flag_fo,log,activity_status,flag_unwntd FROM user_booking_tb WHERE source='$source' AND vehicle_type='4w' AND city='$city' AND mec_id NOT IN(400001,200018,200379,400974) AND log BETWEEN '$start' AND '$end' ";
                            $res_s_4w = mysqli_query($conn,$sql_s_4w);

                            while($row_s_4w = mysqli_fetch_object($res_s_4w)){
                                $booking_flag_s4 = $row_s_4w->flag;
                                $status_s4 = $row_s_4w->booking_status;
                                $flag_fo_s4 = $row_s_4w->flag_fo;
                                $activity_status_s4 = $row_s_4w->activity_status;
                                $flag_unwntd_s4 = $row_s_4w->flag_unwntd;

                                if($flag_unwntd_s4 == '1'){
                                    if($booking_flag_s4 == '1' && $activity_status_s4 =='26'){
                                        $no_ds_4w=$no_ds_4w+1;
                                        $no_ds_4w_total= $no_ds_4w_total+1;
                                    }
                                }
                                else{
                                    $no_ls_4w=$no_ls_4w+1;
                                    $no_ls_4w_total=$no_ls_4w_total+1;
        
                                    switch($status_s4){
                                            case '1':if($booking_flag_s4 == '1'){
                                                          $no_cs_4w=$no_cs_4w+1;
                                                          $no_cs_4w_total=$no_cs_4w_total+1;
                                                     }
                                                    else{
                                                        if($flag_fo_s4 == '1'){
                                                            $no_fs_4w=$no_fs_4w+1 ;
                                                            $no_fs_4w_total=$no_fs_4w_total+1;
                                                        }
                                                        else{
                                                            $no_is_4w=$no_is_4w+1 ;
                                                            $no_is_4w_total=$no_is_4w_total+1 ;
                                                        }
                                                    }
                                                    break;
                                            case '2':if($booking_flag_s4 == '1'){
                                                         $no_cs_4w=$no_cs_4w+1;
                                                         $no_cs_4w_total=$no_cs_4w_total+1;
                                                        }
                                                        else{
                                                            $no_bs_4w=$no_bs_4w+1;
                                                            $no_bs_4w_total=$no_bs_4w_total+1;
                                                        }
                                                    break;
                                            case '3':
                                            case '4':
                                            case '5':
                                            case '6':if($booking_flag_s4 != '1'){
                                                        $no_fs_4w=$no_fs_4w+1;
                                                        $no_fs_4w_total=$no_fs_4w_total+1;
                                                    }
                                                    else{
                                                        $no_cs_4w=$no_cs_4w+1;
                                                        $no_cs_4w_total=$no_cs_4w_total+1;
                                                    }
                                                    break;
                                            case '0':if($booking_flag_s4 != '1'){
                                                        $no_os_4w=$no_os_4w+1;
                                                        $no_os_4w_total=$no_os_4w_total+1;
                                                    }
                                                    else{
                                                        $no_cs_4w=$no_cs_4w+1;
                                                        $no_cs_4w_total=$no_cs_4w_total+1;
                                                    }
                                                    break;
                                        }
                                }
                           }
							?>
								<tr>
                                    <?php if($no_ls_4w == 0){  continue;  }
                                    else{ ?>
                                    <td><?php echo $source; ?></td>
									<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'lead';?>" data-target="#myModalsource"><?php echo $no_ls_4w;?></button></td>
									<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'booking';?>" data-target="#myModalsource"><?php echo $no_bs_4w."(".floor(($no_bs_4w/$no_ls_4w)*100)."%)"; ?></button></td>
									<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'followup';?>" data-target="#myModalsource"><?php echo $no_fs_4w."(".floor(($no_fs_4w/$no_ls_4w)*100)."%)"; ?></button></td>
									<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'cancelled';?>" data-target="#myModalsource"><?php echo $no_cs_4w."(".floor(($no_cs_4w/$no_ls_4w)*100)."%)"; ?></button></td>
									<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'others';?>" data-target="#myModalsource"><?php echo $no_os_4w."(".floor(($no_os_4w/$no_ls_4w)*100)."%)"; ?></button></td>
									<td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'idle';?>" data-target="#myModalsource"><?php echo $no_is_4w."(".floor(($no_is_4w/$no_ls_4w)*100)."%)"; ?></button></td>
                                    <td style="font-size:13px;"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-source="<?php echo $source; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'duplicate';?>" data-target="#myModalsource"><?php echo $no_ds_4w; ?></button></td>
                                    <?php } ?>
								</tr>
							<?php   }
						?>
                <tr>
                <td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);">Total</td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'lead';?>" data-target="#myModalsource"><?php echo $no_ls_4w_total; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'booking';?>" data-target="#myModalsource"><?php echo $no_bs_4w_total."(".floor(($no_bs_4w_total/$no_ls_4w_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'followup';?>" data-target="#myModalsource"><?php echo $no_fs_4w_total."(".floor(($no_fs_4w_total/$no_ls_4w_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'cancelled';?>" data-target="#myModalsource"><?php echo $no_cs_4w_total."(".floor(($no_cs_4w_total/$no_ls_4w_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'others';?>" data-target="#myModalsource"><?php echo $no_os_4w_total."(".floor(($no_os_4w_total/$no_ls_4w_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'idle';?>" data-target="#myModalsource"><?php echo $no_is_4w_total."(".floor(($no_is_4w_total/$no_ls_4w_total)*100)."%)"; ?></button></td>
                <td style="text-align:left;background-color:rgba(84, 156, 156, 0.57);"><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-source="all" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-status="<?php echo 'duplicate';?>" data-target="#myModalsource"><?php echo $no_ds_4w_total; ?></button></td>
                </tr>

					</tbody>
				</table>
			</div>
		</div>
		</div>
<!-- ----------- 4wheelers------------------- -->
<script>
function openTab1(evt, vehicle) {
    // Declare all variables
    var i, tabcontent1, tablinks1;

    // Get all elements with class="tabcontent" and hide them
    tabcontent1 = document.getElementsByClassName("tabcontent1");
    for (i = 0; i < tabcontent1.length; i++) {
        tabcontent1[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks1 = document.getElementsByClassName("tablinks1");
    for (i = 0; i < tablinks1.length; i++) {
        tablinks1[i].className = tablinks1[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(vehicle).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>

<script>
$(document).ready(function(){
  document.getElementById("alltab_source").click();
});
</script>
<script>
 $(function() {
    var startDate = "<?php echo date('d-m-Y',strtotime($start)); ?>";
    var endDate = "<?php echo date('d-m-Y',strtotime($end)); ?>";
    $("#source_excel_all").click(function(){
        $("#source_table_all").table2excel({
           // exclude: ".noExl",
            //name: "Excel Document Name",
            filename:  "Conversion(SourceView) from " + startDate + " to " + endDate
        }); 
    });
   // $("#source_table_all").dataTable();
  });
</script>
<script>
 $(function() {
    var startDate = "<?php echo date('d-m-Y',strtotime($start)); ?>";
    var endDate = "<?php echo date('d-m-Y',strtotime($end)); ?>";
    $("#source_excel_2w").click(function(){
        $("#source_table_2w").table2excel({
           // exclude: ".noExl",
            //name: "Excel Document Name",
            filename:  "Conversion(SourceView-2w) from " + startDate + " to " + endDate
        }); 
    });
    //$("#source_table_2w").dataTable();
  });
</script>
<script>
 $(function() {
    var startDate = "<?php echo date('d-m-Y',strtotime($start)); ?>";
    var endDate = "<?php echo date('d-m-Y',strtotime($end)); ?>";
    $("#source_excel_4w").click(function(){
        $("#source_table_4w").table2excel({
           // exclude: ".noExl",
            //name: "Excel Document Name",
            filename:  "Conversion(SourceView-4w) from " + startDate + " to " + endDate
        }); 
    });
    //$("#source_table_4w").dataTable();
  });
</script>

</div>

<div id="NonConversion" class="tabcontent" style="margin-left:50px;">
  <?php
    if($super_flag == '1'){ ?>
    <button id="nonconversion_excel" class="btn btn-md" style="margin-top:10px;margin-left:900px;float-left;background-color:#00695C;color:#fafafa;"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;&nbsp;Export</button>
    <?php } ?>
  <div align="center">
    <div style="margin-top:20px;max-width:95%;overflow-y:auto;">
       <table id="nonconversion_table" class="table table-striped table-hover" >
            <thead style="background-color: #B2DFDB;">
				<th></th>
				<th>2Wheelers</th>
				<th>4Wheelers</th>
            </thead>
            <tbody>
			   <tr style="background-color:#E0F2F1;"><td colspan="3" style="font-size:18px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cancelled Bookings</td></tr>
				<?php
					$sql_conv = "SELECT id,activity FROM admin_activity_tbl WHERE flag='1' ORDER BY activity ASC";
					$res_conv = mysqli_query($conn,$sql_conv) or die(mysqli_error($conn));

					while($row_conv = mysqli_fetch_object($res_conv)){
                        $act_id = $row_conv->id;
                        $activity = $row_conv->activity;

                        $sql_non_conv_c = "SELECT DISTINCT com_id,book_id FROM admin_comments_tbl WHERE activity_status='$act_id' AND log BETWEEN '$start' AND '$end'";
                        $res_non_conv_c = mysqli_query($conn,$sql_non_conv_c) or die(mysqli_error($conn));

                        $nc_cancelled_count_2w = 0 ;
                        $nc_cancelled_count_4w = 0 ;
                        while($row_non_conv_c = mysqli_fetch_object($res_non_conv_c)){
                            $cancelled_booking_id = $row_non_conv_c->book_id;

                            $sql_get_type = $city == "all" ? "SELECT vehicle_type FROM user_booking_tb WHERE booking_id='$cancelled_booking_id' AND mec_id NOT IN(400001,200018,200379,400974) " : "SELECT vehicle_type FROM user_booking_tb WHERE booking_id='$cancelled_booking_id' AND city='$city' AND mec_id NOT IN(400001,200018,200379,400974) ";
                            $res_get_type = mysqli_query($conn,$sql_get_type);
                            
                            $row_get_type = mysqli_fetch_object($res_get_type);
                            $vehicle_type = $row_get_type->vehicle_type;

                            if($vehicle_type == '2w'){
                                $nc_cancelled_count_2w=$nc_cancelled_count_2w+1;
                            }
                            else if($vehicle_type == '4w'){
                                $nc_cancelled_count_4w=$nc_cancelled_count_4w+1;				
                            }
                        }
                        ?>
						<tr>
						<td><?php echo $activity; ?></td>
						<td><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-activity="<?php echo $act_id; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-target="#myModalnonconv"><?php echo $nc_cancelled_count_2w; ?></button></td>
						<td><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-activity="<?php echo $act_id; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-target="#myModalnonconv"><?php echo $nc_cancelled_count_4w; ?></button></td>
						</tr>
					<?php   }
				?>
			<tr style="background-color:#E0F2F1;"><td colspan="3" style="font-size:18px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other Bookings</td></tr>  
				<?php
					$sql_conv = "SELECT id,activity FROM admin_activity_tbl WHERE flag='2' ORDER BY activity ASC";
					$res_conv = mysqli_query($conn,$sql_conv) or die(mysqli_error($conn));
					while($row_conv = mysqli_fetch_object($res_conv)){
					$activity_id = $row_conv->id;
					$activity = $row_conv->activity;

					$sql_non_conv_o = "SELECT DISTINCT com_id,book_id FROM admin_comments_tbl WHERE activity_status='$activity_id' AND log BETWEEN '$start' AND '$end'";
					$res_non_conv_o = mysqli_query($conn,$sql_non_conv_o) or die(mysqli_error($conn));
					 
                    $nc_others_count_2w = 0;
				    $nc_others_count_4w = 0;  
					while($row_non_conv_o = mysqli_fetch_object($res_non_conv_o)){
                        $others_booking_id = $row_non_conv_o->book_id;

                        $sql_get_type = $city == "all" ? "SELECT vehicle_type FROM user_booking_tb WHERE booking_id='$others_booking_id' AND mec_id NOT IN(400001,200018,200379,400974) " : "SELECT vehicle_type FROM user_booking_tb WHERE booking_id='$others_booking_id' AND city='$city' AND mec_id NOT IN(400001,200018,200379,400974) ";
                        $res_get_type = mysqli_query($conn,$sql_get_type) or die($mysqli_error($conn));
                        
                        $row_get_type = mysqli_fetch_object($res_get_type);
                        $vehicle_type = $row_get_type->vehicle_type;

                        if($vehicle_type == '2w'){
                            $nc_others_count_2w=$nc_others_count_2w+1;
                        }
                        else if($vehicle_type == '4w'){
                            $nc_others_count_4w=$nc_others_count_4w+1;				
                        }  
                    }
					
				?>
				<tr>
					<td><?php echo $activity; ?></td>
					<td><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="2w" data-activity="<?php echo $activity_id; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-target="#myModalnonconv"><?php echo $nc_others_count_2w; ?></button></td>
					<td><button type="button" style="background-color:transparent;" class="btn"  data-toggle="modal" data-vehicle="4w" data-activity="<?php echo $activity_id; ?>" data-startdate="<?php echo $start; ?>" data-enddate="<?php echo $end; ?>" data-target="#myModalnonconv"><?php echo $nc_others_count_4w; ?></button></td>
				</tr>
				<?php   }
				?>
            </tbody>
        </table>
    </div>
  </div>
</div>


<script>
 $(function() {
    var startDate = "<?php echo date('d-m-Y',strtotime($start)); ?>";
    var endDate = "<?php echo date('d-m-Y',strtotime($end)); ?>";
    $("#person_excel").click(function(){
        $("#person_table").table2excel({
           // exclude: ".noExl",
            //name: "Excel Document Name",
            filename:  "Conversion(PersonView) from " + startDate + " to " + endDate
        }); 
     });
   // $("#person_table").dataTable();
  });
</script>
<script>
 $(function() {
    var startDate = "<?php echo date('d-m-Y',strtotime($start)); ?>";
    var endDate = "<?php echo date('d-m-Y',strtotime($end)); ?>";
    $("#nonconversion_excel").click(function(){
        $("#nonconversion_table").table2excel({
           // exclude: ".noExl",
            //name: "Excel Document Name",
            filename:  "NonConversion from " + startDate + " to " + endDate
        }); 
    });
    //$("#nonconversion_table").dataTable();
  });
</script>

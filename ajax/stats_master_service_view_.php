<?php
include("../config.php");
//error_reporting(E_ALL); ini_set('display_errors', 1);
$conn = db_connect2();
session_start();


$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$vehicle = $_GET['vehicle'];
$city = $_GET['city'];
$cluster = $_GET['cluster'];

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;

$today = date('Y-m-d');

$start = strtotime($startdate);
$end = strtotime($enddate);

$total_total = 0;

$cond = '' ; 
$cond = $cond.($vehicle == 'all' ? "" : "AND g.vehicle_type='$vehicle'");
$cond = $cond.($city == 'all' ? "" : "AND g.city='$city'");
$cond = $cond.($cluster == 'all' ? "" : "AND (case when g.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'");

$vehcond = '' ; 
$vehcond = $vehcond.($vehicle == 'all' ? "" : "AND g.vehicle_type='$vehicle'");
$vehcond = $vehcond.($city == 'all' ? "" : "AND g.city='$city'");
$vehcond = $vehcond.($cluster == 'all' ? "" : "AND (case when g.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'");

$sql_ms = "SELECT DISTINCT ms.short_name,COUNT(g.booking_id) as count FROM go_bumpr.go_axle_service_price_tbl as ms LEFT JOIN go_bumpr.user_booking_tb as g ON (ms.service_type=g.service_type AND ms.type=g.vehicle_type) LEFT JOIN go_bumpr.localities as l ON g.locality=l.localities WHERE DATE(g.log) BETWEEN '$startdate' AND '$enddate' AND g.mec_id NOT IN (400001,200018,200379,400974) {$vehcond} GROUP BY ms.short_name ORDER BY COUNT(g.booking_id) DESC";
$res_ms = mysqli_query($conn,$sql_ms);

if(mysqli_num_rows($res_ms)>=1){
?>
    <div id="division" style="float:left; margin-top:10px;margin-left:35px;width:65%;clear:both;">
    <div id="div1" style="width:89%;float:left;">
    <table class="table table-striped table-bordered tablesorter table-hover results" id="table">
    <thead style="background-color:#B2DFDB;align:center;display: table-header-group;">
    <th style="text-align:center;min-width:120px;">Date </th>
    <th style="text-align:center;">Day </th>
    
    <?php

    while($row_ms = mysqli_fetch_object($res_ms)){
        if($row_ms->count < 0 ){
            continue;
        }
        $total_total = $total_total + $row_ms->count;
        $master_services[] = array("service" => $row_ms->short_name, "count" => $row_ms->count); 
    ?>
    <th style="text-align:center;"><?php echo $row_ms->short_name; ?></th>
    <?php  } ?>
    <th style="text-align:center;background-color:#73bdbd;">Total </th>
    </thead>
    <tbody id="tbody">

    <?php
    //var_dump($master_services);  
    // Loop between timestamps, 24 hours at a time
    for ( $i = $start; $i <= $end; $i = $i + 86400 ) {
        $thisDate = date('d M Y', $i);
        $day = date('l', strtotime($thisDate));

        $date = date('Y-m-d', $i);
        $total = 0;

        if($date > $today){
            continue;
        }
        ?>
        <tr>
        <td style="text-align:center;"><?php echo $thisDate; ?></td>
        <td style="text-align:center;"><?php echo $day;?></td>
        <?php
        foreach($master_services as $row){
            $ms = $row['service'];
            $sql_b = "SELECT count(g.booking_id) as count FROM go_bumpr.user_booking_tb as g INNER JOIN go_bumpr.go_axle_service_price_tbl as ms ON ( g.service_type=ms.service_type AND g.vehicle_type=ms.type ) LEFT JOIN go_bumpr.localities as l ON g.locality=l.localities WHERE ms.short_name='$ms' AND DATE(g.log)='$date' AND g.mec_id NOT IN (400001,200018,200379,400974) {$cond}";    
            $res_b = mysqli_query($conn,$sql_b);
            $row_b = mysqli_fetch_array($res_b);
            $total = $total + $row_b['count'];
            ?>
            <td style="text-align:center;"><?php echo $row_b['count'];?></td>
            <?php
        }
        ?>
            <td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total;?></td>    
        </tr>
    <?php        
    }
    ?>

    </tbody>
    <tbody class="avoid-sort">
    <tr>
    <td style="text-align:center;background-color:#73bdbd;" colspan="2">Total</td>
    <?php
        foreach($master_services as $row){
            ?>
            <td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $row['count'];?></td>
            <?php
        }
    ?>
    <td style="text-align:center;background-color:#73bdbd;"><?php echo $total_total;?></td>
    </tr>
    </tbody>
    </table>
    </div>
    </div>
    <!-- table sorter -->
    <script>
    $(document).ready(function()
        {
            $("#table").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[0,0],[1,1]]} );
        }
    );
    </script>
<?php
}
else{
    echo "<div style='margin-top:140px;' align='center' ><h3>No Results Found!</h3></div>";
}
?>



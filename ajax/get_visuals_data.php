<?php
include("../config.php");
$conn = db_connect3();
$conn1 = db_connect1();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$shop_id = $_POST['shop_id'];
$startdate = date('Y-m-d',strtotime($_POST['startDate']));
$enddate = date('Y-m-d',strtotime($_POST['endDate']));
$master_service = $_POST['master_service'];
$service = $_POST['service'];
$city = $_POST['city'];

$_SESSION['crm_city'] = $city;


class feedbacks {
  private $shop_id;
  private $master_service;
  private $service;
  private $city;
  
  function __construct($shop_id,$master_service,$service,$city){
    $this->shop_id = $shop_id;
    $this->master_service = $master_service;
    $this->service = $service;
    $this->city = $city;
	
	if($this->master_service != "" && $this->service == ""){
		$conn1 = db_connect1();
		$sql_serv = "SELECT service_type FROM go_axle_service_price_tbl WHERE bridge_flag='0' AND master_service='$this->master_service'";
		$res_serv = mysqli_query($conn1,$sql_serv);
		$this->services = '';
		while($row_serv = mysqli_fetch_array($res_serv)){
			if($this->services == ""){
				$this->services = $row_serv['service_type']."'";
			}
			else{
				$this->services = $this->services.",'".$row_serv['service_type']."'";
			}
		}
	}
  }
  function sh0_s0_c0(){
	$master = $this->master_service;
	if($master == ""){
		return "";
	}
	else{
		return "AND b.service_type IN('$this->services)";
	}
  } 
  function sh0_s0_c1(){
	$master = $this->master_service;
	if($master == ""){
		return "AND b.city='$this->city'";
	}
	else{
		return "AND b.city='$this->city' AND b.service_type IN('$this->services)";
	}
  } 
  function sh0_s1_c0(){
	return "AND b.service_type IN('$this->service')";
  } 
  function sh0_s1_c1(){
	return "AND b.service_type IN('$this->service') AND b.city='$this->city'";
  }  
  function sh1_s0_c0(){
	$master = $this->master_service;
	if($master == ""){
		return "AND b2b_b.b2b_shop_id = '$this->shop_id'";
	}
	else{
		return "AND b2b_b.b2b_shop_id = '$this->shop_id' AND b.service_type IN('$this->services)";
	}
  } 
  function sh1_s0_c1(){
	$master = $this->master_service;
	if($master == ""){
		return "AND b2b_b.b2b_shop_id = '$this->shop_id' AND b.city='$this->city'";
	}
	else{
		return "AND b2b_b.b2b_shop_id = '$this->shop_id' AND b.city='$this->city' AND b.service_type IN('$this->services)";
	}
  }  
  function sh1_s1_c0(){
	return "AND b2b_b.b2b_shop_id = '$this->shop_id' AND b.service_type IN('$this->service')";
  } 
  function sh1_s1_c1(){
	return "AND b2b_b.b2b_shop_id = '$this->shop_id' AND b.service_type IN('$this->service') AND b.city='$this->city'";
  }
}

$master_service_fun = $master_service=='all' ? "" : $master_service;
$service_type_fun = $service=='all' ? "" : $service;
$city_fun = $city=='all' ? "" : $city;

$feedbacks_obj = new feedbacks($shop_id,$master_service_fun,$service_type_fun,$city_fun);

$shop_id_val = $shop_id != "" ? "1" : "0";
$service_val = $service=='all' ? "0" : "1";
$city_val = $city == 'all' ? "0" : "1";

$cond = $feedbacks_obj->{"sh{$shop_id_val}_s{$service_val}_c{$city_val}"}();

/*$sql = "SELECT r.rating_id,r.rating,r.feedback,r.axle_booking_id,r.booking_id,r.source,r.service_type,m.axle_id,m.address5,r.log,r.medium,m.mec_id FROM go_bumpr.user_rating_tbl r,go_bumpr.admin_mechanic_table m WHERE r.status = '0' AND m.mec_id = r.mec_id AND DATE(r.log) BETWEEN '$startdate' and '$enddate' {$cond}";
$res = mysqli_query($conn,$sql);

$rating_arr = array();
$count = mysqli_num_rows($res);
$ratingReview = $ratingOnly = 0;
if($count>0)
{
	while($row= mysqli_fetch_object($res))
	{
		$rating = $row->rating;
		$feedback = $row->feedback;
		if($rating!=""&&$feedback!="")
		{
			$ratingReview++;
		}
		if($rating!=""&&$feedback=="")
		{
			$ratingOnly++;
		}
		$rating_arr[] = $row->rating;
	}
	$avg_rating = array_sum($rating_arr)/count($rating_arr);
	$review['avg_rating'] = round($avg_rating,1);
	$review['ratingReview'] = $ratingReview;
	$review['ratingOnly'] = $ratingOnly;
}
else
{*/
	$review['avg_rating'] = 0;
	$review['ratingReview'] = 0;
	$review['ratingOnly'] = 0;
//}
$sql = "SELECT count(*) as feedback_team,rating.medium FROM go_bumpr.goaxle_track g JOIN go_bumpr.user_booking_tb b ON g.go_booking_id = b.booking_id JOIN go_bumpr.user_register r ON b.user_id = r.reg_id JOIN b2b.b2b_booking_tbl b2b_b ON g.b2b_booking_id = b2b_b.b2b_booking_id JOIN b2b.b2b_credits_tbl b2b_c ON b2b_b.b2b_shop_id = b2b_c.b2b_shop_id LEFT JOIN go_bumpr.user_rating_tbl rating ON g.b2b_booking_id = rating.axle_booking_id WHERE g.status = 'sent' AND b.feedback_status IN (0,1) AND DATE(g.sent_log) BETWEEN '$startdate' and '$enddate' AND b2b_b.b2b_shop_id NOT IN (1014,1035,1670) {$cond} AND b.service_status='completed' group by rating.medium";
$res = mysqli_query($conn,$sql);
$no = 0;
if(mysqli_num_rows($res) >=1){
	$total=mysqli_num_rows($res);
	while($row = mysqli_fetch_object($res)){
	$feedback_team = $row->feedback_team;
	$medium = $row->medium;
	$completed[$medium] = $feedback_team;
	}
}
else {
  //echo $sql_booking;
  $completed['axle'] = 0;
  //$completed['rtt'] = 0;

}
$sql1 = "SELECT b.service_type,AVG(b.final_bill_amt) AS avg FROM go_bumpr.goaxle_track g JOIN go_bumpr.user_booking_tb b ON g.go_booking_id = b.booking_id JOIN go_bumpr.user_register r ON b.user_id = r.reg_id JOIN b2b.b2b_booking_tbl b2b_b ON g.b2b_booking_id = b2b_b.b2b_booking_id JOIN b2b.b2b_credits_tbl b2b_c ON b2b_b.b2b_shop_id = b2b_c.b2b_shop_id LEFT JOIN go_bumpr.user_rating_tbl rating ON g.b2b_booking_id = rating.axle_booking_id WHERE g.status = 'sent' AND b.feedback_status IN (0,1) AND DATE(g.sent_log) BETWEEN '$startdate' and '$enddate' AND b2b_b.b2b_shop_id NOT IN (1014,1035,1670) {$cond} GROUP BY b.service_type ORDER BY avg DESC LIMIT 5;";
$res1 = mysqli_query($conn,$sql1);
$count1 = mysqli_num_rows($res1);
$billing_arr = array();
if($count1>0)
{
	while($row1= mysqli_fetch_object($res1))
	{
		$billing_arr['service_type'][] = $row1->service_type;
		$billing_arr['billing_avg'][] = round($row1->avg,2);
	}
}
else
{
	$billing_arr['billing_avg'] = 0;
}


$sql2 = "SELECT rating.rating,count(*) as count FROM go_bumpr.goaxle_track g JOIN go_bumpr.user_booking_tb b ON g.go_booking_id = b.booking_id JOIN go_bumpr.user_register r ON b.user_id = r.reg_id JOIN b2b.b2b_booking_tbl b2b_b ON g.b2b_booking_id = b2b_b.b2b_booking_id JOIN b2b.b2b_credits_tbl b2b_c ON b2b_b.b2b_shop_id = b2b_c.b2b_shop_id LEFT JOIN go_bumpr.user_rating_tbl rating ON g.b2b_booking_id = rating.axle_booking_id WHERE g.status = 'sent' AND b.feedback_status IN (0,1) AND DATE(g.sent_log) BETWEEN '$startdate' and '$enddate' AND b2b_b.b2b_shop_id NOT IN (1014,1035,1670) {$cond} group by rating.rating;";
$res2 = mysqli_query($conn,$sql2);
$count2 = mysqli_num_rows($res2);
if($count2>0)
{
while($row2 = mysqli_fetch_object($res2))
{
	$rating = (float)$row2->rating;
	$rating = round($rating,0);
	$ratingData['rate'][$rating] = $ratingData['rate'][$rating] + $row2->count;
	if($ratingData['rate'][$rating] == undefined)
	{
		$ratingData['rate'][$rating] = 0;
	}
}
}
else
{
	$ratingData['rate'][0] = 0;
}

$sql3 = "SELECT b.service_type, b.service_status FROM go_bumpr.goaxle_track g JOIN go_bumpr.user_booking_tb b ON g.go_booking_id = b.booking_id JOIN go_bumpr.user_register r ON b.user_id = r.reg_id JOIN b2b.b2b_booking_tbl b2b_b ON g.b2b_booking_id = b2b_b.b2b_booking_id JOIN b2b.b2b_credits_tbl b2b_c ON b2b_b.b2b_shop_id = b2b_c.b2b_shop_id LEFT JOIN go_bumpr.user_rating_tbl rating ON g.b2b_booking_id = rating.axle_booking_id WHERE g.status = 'sent' AND DATE(g.sent_log) BETWEEN '$startdate' and '$enddate' AND b2b_b.b2b_shop_id NOT IN (1014,1035,1670) {$cond}";
$res3 = mysqli_query($conn,$sql3);
$count3 = mysqli_num_rows($res3);
if($count3>0)
{
	while($row3 = mysqli_fetch_object($res3))
	{
		$service_type = $row3->service_type;
		$service_status = $row3->service_status;
		if($service_status == 'Completed' || $service_status == 'In Progress')
		{
			$converted[$service_type] = $converted[$service_type] + 1;
		}
		$conv_total[$service_type] = $conv_total[$service_type] + 1;
		$conv_rate[$service_type] = round(($converted[$service_type]/$conv_total[$service_type])*100,2);
	}
}
else
{
	$conv_rate['service_type'] = 0;
}
$result[] = $review;
$result[] = $billing_arr;
$result[] = $ratingData;
$result[] = $completed;
$result[] = $conv_rate;
//$result[] = $total;
echo json_encode($result);
?>	
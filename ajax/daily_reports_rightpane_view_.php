<?php
include("../config.php");
error_reporting(E_ALL); ini_set('display_errors', 1);
$conn = db_connect2();
session_start();

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$master_service = $_GET['master_service'];
$service = $_GET['service'];
$person = $_GET['person'];
$today = date('Y-m-d');
$services = '';

$programatic = $_GET['programatic'];
$city = $_GET['city'];
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;

$cond ='';

$cond = $cond.($person == 'all' ? "" : "AND g.crm_update_id='$person'");
$cond = $cond.($city == 'all' ? "" : "AND g.city='$city'");
$cond = $cond.($cluster == 'all' ? "" :  "AND (case when g.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'");

if($master_service != "all" && $service == "all"){
    //echo " entered";
    $conn1 = db_connect1();
    $sql_serv = "SELECT service_type FROM go_axle_service_price_tbl WHERE bridge_flag='0' AND master_service='$this->master_service'";
    $res_serv = mysqli_query($conn1,$sql_serv);
    $services = '';
    while($row_serv = mysqli_fetch_array($res_serv)){
        if($services == ""){
            $services = $row_serv['service_type']."'";
        }
        else{
            $services = $services.",'".$row_serv['service_type']."'";
        }
    }
    $cond = $cond." AND g.service_type IN ('$services)";
} 

$cond = $cond.($service != 'all' ?  " AND g.service_type='$service'" : '' ) ;

$cond = $cond.($programatic == 'yes' ? "" : " AND g.crm_update_id NOT IN ('crm003','crm036')");

$start = strtotime($startdate);
$end = strtotime($enddate);
$noofdays = date('t',strtotime($start));

switch($city){
    case 'Chennai' :$car_cpd = 263.5; //car credits per day
                    $bike_cpd = 75;  //bike credits per day
                    break;
    case 'Bangalore' :$car_cpd = 50; //car credits per day
                    $bike_cpd = 0;  //bike credits per day
                    break;
    case 'all' :$car_cpd = 313.5; //car credits per day
                    $bike_cpd = 75;  //bike credits per day
                    break;
    default : $car_cpd = 0; //car credits per day
                $bike_cpd = 0;  //bike credits per day
}

$start = strtotime($startdate);
$end = strtotime($enddate);
$noofdays = date('t',strtotime($start));
// Loop between timestamps, 24 hours at a time
for ( $i = $start; $i <= $end; $i = $i + 86400 ) {
    $thisDate = date('d M Y', $i);
    $day = date('l', strtotime($thisDate));

    $date = date('Y-m-d', $i);
    
    $sql_b2b = "SELECT DISTINCT b.gb_booking_id,b.b2b_vehicle_type,b.b2b_credit_amt,b.b2b_log FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id=g.booking_id LEFT JOIN go_bumpr.localities as l ON g.locality=l.localities WHERE s.b2b_acpt_flag='1' AND b.b2b_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log)='$date'  AND b.b2b_shop_id NOT IN (1014,1035,1670) {$cond}";    
    $res_b2b = mysqli_query($conn,$sql_b2b);
    //echo $sql_b2b;
    //initialize
    $bike = 0;
    $car = 0;
    $total = 0 ;

    while($row_b2b = mysqli_fetch_object($res_b2b)){
        $veh_type = $row_b2b->b2b_vehicle_type;
        $creditamount = $row_b2b->b2b_credit_amt;
        $gb_booking_id = $row_b2b->gb_booking_id;
        $log = $row_b2b->b2b_log;

        switch($veh_type){
            case '2w': $bike = $bike+$creditamount; break;
            case '4w': $car = $car+$creditamount; break;
        }//switch
    } // while

    $bikecredits = $bike/100;
    $carcredits = $car/100;
    $totalcredits = $bikecredits+$carcredits;
    if($date > $today){
        continue;
    }
    $list_credits[] = array("date" => $date, "bikecredits" => $bikecredits,"carcredits" =>$carcredits , "totalcredits" => $totalcredits);
}// for
$carsum =0 ;$carcount=0;$caravg=0;
//get non zero car credits and calculate avg
foreach($list_credits as $key=>$row){
    if($row['carcredits'] != 0){
        $carsum=$carsum+$row['carcredits'];
        $carcount = $carcount+1;
    }
    else{
        continue;
    }
}
$caravg = $carcount == 0 ? 0 : ceil($carsum/$carcount);

$bikesum =0 ;$bikecount=0;$bikeavg=0;
//get non zero bike credits and calculate avg
foreach($list_credits as $key=>$row){
    if($row['bikecredits'] != 0){
        $bikesum=$bikesum+$row['bikecredits'];
        $bikecount = $bikecount+1;
    }
    else{
        continue;
    }
}
$bikeavg = $bikecount == 0 ? 0 : ceil($bikesum/$bikecount);
//get non zero total credits
$achieved_car=0;$acheivedper_car =0;
foreach($list_credits as $key=>$row){
        $achieved_car=$achieved_car+$row['carcredits'];
}
$acheivedper_car = ceil(($achieved_car/($car_cpd*24))*100);
$achieved_bike=0;$acheivedper_bike =0;
foreach($list_credits as $key=>$row){
        $achieved_bike=$achieved_bike+$row['bikecredits'];
}
$acheivedper_bike = $bike_cpd == 0 ? 0 : ceil(($achieved_bike/($bike_cpd*24))*100);
//calculated projected consumpsion credits= average credits*no of days in that month
$carprocon = ceil($caravg*$noofdays);
$bikeprocon = ceil($bikeavg*$noofdays);

$cartarget = $car_cpd*24;
$biketarget = $bike_cpd*24;
$carachieved = $achieved_car." [".$acheivedper_car." %]";
$bikeachieved = $achieved_bike." [".$acheivedper_bike." %]";

echo $data = json_encode(array('car_cpd'=>$car_cpd , 'bike_cpd'=>$bike_cpd , 'caravg'=>$caravg , 'bikeavg'=>$bikeavg , 'totalavg'=>$caravg+$bikeavg, 'carprocon'=>$carprocon , 'bikeprocon'=>$bikeprocon , 'totalprocon'=>$carprocon+$bikeprocon , 'cartarget'=>$cartarget , 'biketarget'=>$biketarget , 'carachieved'=>$carachieved , 'bikeachieved'=> $bikeachieved));
?>

<?php
include("../config.php");
//error_reporting(E_ALL); ini_set('display_errors', 1);

$conn = db_connect1();
session_start();

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$today = date('Y-m-d');
$city = $_POST['city'];
$_SESSION['crm_city'] = $city;
$vehicle=$_POST['vehicle_type'];

$cond ='';
$cond1 ='';

$cond = $cond.($city == 'all' ? "" : "AND b.city ='$city'");
$cond1= ($vehicle=='all')?"" : "AND b.vehicle_type = '$vehicle'";
?>

<div align="center" id = "table" style="margin-left:20px;margin-right:20px;">
<table id="example1" class="table table-striped table-bordered tablesorter table-hover results">
<thead style="background-color:#B2DFDB;">

<th style="text-align:center;">Service Center</th>
<th style="text-align:center;">Locality</th>
<th style="text-align:center;">Business Model</th>

<th style="text-align:center;">Total Swapped Goaxles <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Total Credits Consumed <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Total Leads Coonsumed <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Total Swapping Revenue <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
</thead>
<tbody id="tbody">

<?php 
$total_goaxle=0;
$total_credits=0;
$total_leads=0;
$total_revenue=0;

$sql_goaxle="SELECT
	    DISTINCT bb.b2b_booking_id,
	    bb.gb_booking_id AS booking_id,
	    bb.b2b_shop_id,
	    bb.b2b_log,
	    m.b2b_address4,
	    m.b2b_shop_name,
	    b.service_type,
	    b.vehicle_type,
	    bb.brand,
		g.model AS business_model,
	    g.lead_price,
	    g.re_lead_price,
	    g.nre_lead_price,
	    g.amount,
	    g.premium_tenure,
	    g.start_date,
	    g.end_date,
	    bb.b2b_credit_amt,
	    bb.b2b_check_in_report,
	    b.service_status,
	    m.b2b_new_flg,
	    c.b2b_leads,
	    c.b2b_credits
	FROM
	    b2b.b2b_booking_tbl bb
	        LEFT JOIN
	    user_booking_tb b ON bb.gb_booking_id = b.booking_id
	        LEFT JOIN
		garage_model_history g ON g.b2b_shop_id = bb.b2b_shop_id
	        AND DATE(bb.b2b_log) >= DATE(g.start_date)
	        AND g.id=(select id from go_bumpr.garage_model_history where b2b_shop_id=bb.b2b_shop_id AND DATE(bb.b2b_log) >= DATE(start_date) order by start_date desc limit 1) 
	        LEFT JOIN
		b2b.b2b_status s ON s.b2b_booking_id = bb.b2b_booking_id
		    LEFT JOIN
		b2b.b2b_mec_tbl m ON m.b2b_shop_id=bb.b2b_shop_id
			LEFT JOIN
		b2b.b2b_credits_tbl c ON c.b2b_shop_id=bb.b2b_shop_id
	WHERE
	    (DATE(bb.b2b_log) BETWEEN '$startdate' and '$enddate')
	        AND bb.b2b_shop_id NOT IN (1014 , 1035, 1670, 1673)
	        AND b.user_id NOT IN (21816 , 41317,
	        859,
	        3132,
	        20666,
	        56511,
	        2792,
	        128,
	        19,
	        7176,
	        19470,
	        1,
	        951,
	        103699,
	        113453,
	        108783)
	        AND b.service_type NOT IN ('GoBumpr Tyre Fest')
	        {$cond1}
	        {$cond}
	        AND bb.b2b_swap_flag = 1
	        AND s.b2b_acpt_flag = 1 ORDER BY bb.b2b_booking_id;";
$res_goaxle = mysqli_query($conn,$sql_goaxle);

while($row_goaxle = mysqli_fetch_object($res_goaxle))
{
	$veh_type=$row_goaxle->vehicle_type;
	$brand=$row_goaxle->brand;
	$price=$row_goaxle->price;
	$business_model = trim($row_goaxle->business_model);
	$revenue=0;
	$credit_amt=$row_goaxle->b2b_credit_amt;
	$lead_price = $row_goaxle->lead_price;
	$re_lead_price = $row_goaxle->re_lead_price;
	$nre_lead_price = $row_goaxle->nre_lead_price;
	// $exception_log=$row_goaxle->exception_log;
	// echo $exception_log;
	$leads=$row_goaxle->b2b_leads;
	$credits=$row_goaxle->b2b_credits;
	$locality = $row_goaxle->b2b_address4;
	$shop_name=$row_goaxle->b2b_shop_name;
	$service_type=$row_goaxle->service_type;
	$service_status=$row_goaxle->service_status;
	$check_in_report=$row_goaxle->b2b_check_in_report;
	$premium=$row_goaxle->b2b_partner_flag;
	$new_flag=$row_goaxle->b2b_new_flg;
	$shop_id=$row_goaxle->b2b_shop_id;
	$shopData[$shop_id]['model']=$business_model;
	$shopData[$shop_id]['lead_price']=$lead_price;
	$shopData[$shop_id]['re_lead_price']=$re_lead_price;
	$shopData[$shop_id]['nre_lead_price']=$nre_lead_price;
	$shopData[$shop_id]['type']=$veh_type;

	if(!array_key_exists('credits_consumed',$shopData[$shop_id])){
		$shopData[$shop_id]['credits_consumed']=0;
	}
	if(!array_key_exists('leads_consumed',$shopData[$shop_id])){
		$shopData[$shop_id]['leads_consumed']=0;
	}
	if($new_flag==1){
		
		$account="L".$leads;
	}else{
		$account=$credits;
	}
	if($business_model=="Pre Credit"){
	 	$credits = $row_goaxle->b2b_credit_amt;
		$revenue = $credits;
		$shopData[$shop_id]['credits_consumed']+=($credit_amt/100);
	}else if($business_model=="Premium 1.0"){
		$amount = $row_goaxle->amount;
		$premium_tenure = $row_goaxle->premium_tenure;
		$shopData[$shop_id]['credits_consumed']+=($credit_amt/100);
		$revenue=round(($amount/$premium_tenure)/25);
	}else if($business_model=="Premium 2.0"){
		if($veh_type=="4w"){
			$revenue = $lead_price;
		}else{
			if($brand=="Royal Enfield"){
				$revenue=$lead_price;
			}else{
				$revenue=$nre_lead_price;
			}
		}
		$shopData[$shop_id]['leads_consumed']+=1;

	}else if($business_model=="Leads 3.0"){
					
		if($veh_type=="4w"){
			$revenue = $lead_price;
		}else{
			if($brand=="Royal Enfield"){
				$revenue=($re_lead_price!=0)?$re_lead_price:$lead_price;
			}else{
				$revenue=($nre_lead_price!=0)?$nre_lead_price:$lead_price;
			}
		}
		$shopData[$shop_id]['leads_consumed']+=1;		
	}else{
		$credits = $row_goaxle->b2b_credit_amt;
		$revenue = $credits;
		$shopData[$shop_id]['model']="Pre Credit";
		$shopData[$shop_id]['credits_consumed']+=($credit_amt/100);

	}
	$shopData[$shop_id]['revenue']+=round($revenue,2);
	
	$shopData[$shop_id]['account']=$account;
	$shopData[$shop_id]['shop_name']=$shop_name;
	$shopData[$shop_id]['locality']=$locality;	
	$shopData[$shop_id]['goaxle']+=1;
	// var_dump($dateData);die;
}
foreach ($shopData as $key => $value) { 
	if($value['goaxle']>0){ 
		$total_goaxle+=$value['goaxle'];
		$total_credits+=$value['credits_consumed'];
		$total_leads+=$value['leads_consumed'];
		$total_revenue+=$value['revenue'];			
	?>
	<tr>
        <td><?php echo $value['shop_name']." (".$value['account'].")" ; ?></td>
        <td><?php echo $value['locality'] ; ?></td>
        <td><?php echo $value['model'] ; ?></td>
        <td style="text-align:center;"><a id="swap" href="" data-shop="<?php echo $key;?>"><?php echo $value['goaxle'] ; ?></a></td>
        <td style="text-align:center;"><?php echo $value['credits_consumed'] ; ?></td>
        <td style="text-align:center;"><?php echo $value['leads_consumed'] ; ?></td>
        <td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" ></i> <?php echo $value['revenue'] ; ?></td>
    </tr>
<?php } } ?>	
</tbody>
<tbody class="avoid-sort">
<tr>
<td style="text-align:center;" colspan="3">Total</td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_goaxle; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_credits; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_leads; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><i class="fa fa-inr" aria-hidden="true" > <?php echo $total_revenue; ?></td>
</tr>
</tbody>
</table>
</div>


<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example1").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[3,3],[0,0]]} );
    }
);
</script>
<?php
include("../config.php");
$conn = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate = date('Y-m-d',strtotime($_POST['enddate']));
$veh_type = $_POST['veh'];
$city = $_POST['city'];
$loc = $_POST['loc'];
// $city = "chennai";

$_SESSION['crm_city'] = $city;


//0-all && 1-particular
class shops {
  private $veh;
  private $loc;

  function __construct($veh, $loc){
    $this->veh = $veh;
    $this->loc = $loc;
  }
  function v0_l0(){
    return "";
  }
  function v0_l1(){
    return "AND m.b2b_address4 ='$this->loc'";
  }
  function v1_l0(){
    return "AND m.b2b_vehicle_type = '$this->veh'";
  }
  function v1_l1(){
    return "AND m.b2b_vehicle_type = '$this->veh' AND m.b2b_address4 ='$this->loc'";
  }
}

$shops_obj = new shops($veh_type, $loc);

$veh_val = $veh_type=="all" ? "0" : "1";
$loc_val = $loc=="" ? "0" : "1";

$cond = $shops_obj->{"v{$veh_val}_l{$loc_val}"}();

$sql_shops = "SELECT DISTINCT em.b2b_shop_id,m.b2b_shop_id,m.b2b_shop_name,sum(em.credits_deducted) as credits_deducted,sum(case when am.premium='1' then 1 else 0 end) as vehicles_deducted FROM b2b_mec_tbl as m RIGHT JOIN go_bumpr.exception_mechanism_track as em ON em.b2b_shop_id=m.b2b_shop_id RIGHT JOIN go_bumpr.admin_mechanic_table as am on am.mec_id=em.mec_id WHERE m.b2b_address5 ='$city' AND date(em.exception_log) BETWEEN '$startdate' and '$enddate' AND em.flag=0 {$cond} group by em.b2b_shop_id ORDER BY m.b2b_shop_name ASC ";
// $sql_shops = "SELECT DISTINCT em.b2b_shop_id,em.shop_name,c.b2b_credits,gm.exception_stage FROM gobumpr_test.exception_mechanism_track as em INNER JOIN b2b_mec_tbl as m INNER JOIN b2b_credits_tbl as c ON em.b2b_shop_id = c.b2b_shop_id LEFT JOIN gobumpr_test.admin_mechanic_table as gm ON em.b2b_shop_id = gm.axle_id WHERE m.b2b_cred_model=1 AND m.b2b_address5 ='chennai' {$cond} ORDER BY em.shop_name ASC ";
// echo $sql_shops;
$res_shops = mysqli_query($conn,$sql_shops);

$no = 0;

$count = mysqli_num_rows($res_shops);
if($count >0){
while($row_shops = mysqli_fetch_object($res_shops)){
$shop_id = $row_shops->b2b_shop_id;
$shop_name = $row_shops->b2b_shop_name;
$credits = $row_shops->credits_deducted;
$vehicles = $row_shops->vehicles_deducted;
$credits_lossed=$credits/100;
// switch($exception_stage){
//  //case '1': $icon = '<i style="padding-left:5px;font-size:25px;color:FFBA00" title="1st Warning!"  class="fa fa-exclamation"></i>';break;
//  case '1': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="1st Warning!" src="/images/exception1.svg">';break;
//  case '2': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="2nd Warning!" src="/images/exception2.svg">';break;
//  case '3': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="3rd Warning!" src="/images/exception3.svg">';break;
//  case '-1': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="Blocked!" src="/images/exceptionlock.svg">';break;
//  default:$icon = '';
// }
  ?>
  <tr>
    <td><?php echo $no=$no+1 ; ?></td>
    <?php
    // $str='id';
    // $str_encode=base64_encode($str);
   $id_encode=base64_encode($shop_id);
   ?>
     <td><div class="rad"><input type="radio" name="radio_shop" id="<?php echo $shop_id;?>" onclick="get_count('<?php echo $id_encode; ?>');" value="<?php echo $shop_id; ?>"/><label for="<?php echo $shop_id;?>"><?php echo $shop_name; ?></label></div></td>
    <td><?php echo $credits_lossed. ' '.'['.$vehicles.']'; ?></td>
  </tr>
  <?php 
}
} // if
else {
  echo "<h3 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h3>";
}
 ?>

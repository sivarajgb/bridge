<?php
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$starttime = date('H:i:s',strtotime($_GET['startTime']));
$endtime =  date('H:i:s',strtotime($_GET['endTime']));
$start = $startdate.' '.$starttime;
$end = $enddate.' '.$endtime;
// $startdate = date('Y-m-d',strtotime($_GET['startdate']));
// $enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$city = $_GET['city'];

$_SESSION['crm_city'] = $city;

$cond1 ='';
$cond1 = $cond1.($city == 'all' ? "" : "AND city='$city'");

$sql1 = "SELECT crm_log_id,name FROM crm_admin WHERE crm_flag = 1 and flag = 0 {$cond1} ORDER BY crm_log_id;";
$res1 = mysqli_query($conn,$sql1);
$crm_list = "('";
$i = 0;
$no_of_rows = mysqli_num_rows($res1);
while($row1 = mysqli_fetch_object($res1))
{
	$i++;
	$crm_list = $crm_list.$row1->crm_log_id."'";
	if($i != $no_of_rows)
	{
		$crm_list = $crm_list.",'";
	}
	$crms_arr[$row1->crm_log_id]['name'] = $row1->name;
	$crms_arr[$row1->crm_log_id]['goaxles'] = 0;
	$crms_arr[$row1->crm_log_id]['converted'] = 0;
	$crms_arr[$row1->crm_log_id]['unique'] = 0;
	$crms_arr[$row1->crm_log_id]['total'] = 0;
}
$crm_list = $crm_list.")";
// print_r($crms_arr);

$sql2 = "SELECT touchpoints.crm_log_id,touchpoints.unique_count,touchpoints.total_count FROM (SELECT crm_log_id,count(DISTINCT user_id) as unique_count,count(user_id) as total_count FROM admin_comments_tbl where crm_log_id in $crm_list and log BETWEEN '$start' AND '$end' GROUP BY crm_log_id) as touchpoints;";
$res2 = mysqli_query($conn,$sql2);
while($row2 = mysqli_fetch_object($res2))
{
	$crms_arr[$row2->crm_log_id]['unique'] = $row2->unique_count;
	$crms_arr[$row2->crm_log_id]['total'] = $row2->total_count;
}
// print_r($crms_arr);

$sql_booking = "SELECT b.booking_id,b.booking_status,b.service_status,b.crm_update_id,b2b_b.b2b_check_in_report,b2b_b.b2b_vehicle_at_garage,b2b_b.b2b_vehicle_ready FROM user_booking_tb b LEFT JOIN b2b.b2b_booking_tbl b2b_b ON b.booking_id = b2b_b.gb_booking_id WHERE b.booking_status = '2' AND b.crm_update_time BETWEEN '$start' AND '$end' AND b2b_b.b2b_swap_flag!=1;";
$res_booking = mysqli_query($conn,$sql_booking);
while($row_booking = mysqli_fetch_object($res_booking))
{
	$service_status = $row_booking->service_status;
	$check_in_stage = $row_booking->b2b_check_in_report;
	$at_garage_stage = $row_booking->b2b_vehicle_at_garage;
	$ready_stage = $row_booking->b2b_vehicle_ready;
	
	if(!in_array($row_booking->crm_update_id,array_keys($crms_arr)))
	{
		continue;
	}
	
	$crms_arr[$row_booking->crm_update_id]['goaxles'] = $crms_arr[$row_booking->crm_update_id]['goaxles'] + 1;
	if(($service_status == 'Completed' || $service_status == 'In Progress') || ($check_in_stage == 1 || $at_garage_stage == 1 || $ready_stage == 1))
	{
		$crms_arr[$row_booking->crm_update_id]['converted'] = $crms_arr[$row_booking->crm_update_id]['converted'] + 1;
	}
}
// print_r($crms_arr);
foreach($crms_arr as $crm)
{
	// if($crm['unique']==0)
	// {
		// continue;
	// }
	
	if($crm['unique'] < $crm['goaxles'])
	{
		$crm['unique'] = $crm['unique'] + ($crm['goaxles'] - $crm['unique']);
	}
	
	$tr1 = "<tr>";
	$td1 = "<td>".$crm['name']."</td>";
	$td2 = "<td>".$crm['unique']."</td>";
	$td3 = "<td>".$crm['total']."</td>";
	$td4 = "<td>".$crm['goaxles']."</td>";
	$td5 = "<td>".$crm['converted']."</td>";
	$goaxle_rate = round((($crm['goaxles']/$crm['unique'])*100),2);
	if(is_nan($goaxle_rate))
	{
		$goaxle_rate = 0;
	}
	$td6 = "<td>".$goaxle_rate." %</td>";
	$conversion_rate = round((($crm['converted']/$crm['goaxles'])*100),2);
	if(is_nan($conversion_rate))
	{
		$conversion_rate = 0;
	}
	$td7 = "<td>".$conversion_rate." %</td>";
	$tr1_l = "</tr>";
	
	$str = $tr1.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$tr1_l;
	$data1[] = $str;
}

$data['tbl_data'] = $data1;

echo json_encode($data);
?>
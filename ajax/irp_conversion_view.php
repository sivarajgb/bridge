<?php
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];

$city = $_GET['city'];

$sql_crm_list = "SELECT * FROM crm_admin WHERE  flag = 0 AND (city='$city' OR city='all')  ORDER BY irp_rewards DESC";
// $sql_crm_list = "SELECT * FROM crm_admin ORDER BY irp_rewards DESC";
$res_crm_list = mysqli_query($conn,$sql_crm_list);
while($row_crm_list = mysqli_fetch_object($res_crm_list))
{
	$crms_arr[$row_crm_list->crm_log_id]['name'] = $row_crm_list->name;
	$crms_arr[$row_crm_list->crm_log_id]['goaxles'] = 0;
	$crms_arr[$row_crm_list->crm_log_id]['converted'] = 0;
	$crms_arr[$row_crm_list->crm_log_id]['nonconverted'] = 0;
	$crms_arr[$row_crm_list->crm_log_id]['rewards'] = 0;
	$crms_arr[$row_crm_list->crm_log_id]['rewards_earned'] = $row_crm_list->irp_rewards;
}

//print_r($crms_arr);
$sql_booking = "SELECT b.booking_id,b.booking_status,b.service_status,b.crm_update_id,b2b_b.b2b_credit_amt,b2b_b.b2b_check_in_report,b2b_b.b2b_vehicle_at_garage,b2b_b.b2b_vehicle_ready FROM user_booking_tb b LEFT JOIN b2b.b2b_booking_tbl b2b_b ON b.booking_id = b2b_b.gb_booking_id WHERE b.booking_status = '2' AND b.irp_flag = '1' AND b2b_b.b2b_swap_flag!=1;";
$res_booking = mysqli_query($conn,$sql_booking);
while($row_booking = mysqli_fetch_object($res_booking))
{
	$service_status = $row_booking->service_status;
	$check_in_stage = $row_booking->b2b_check_in_report;
	$at_garage_stage = $row_booking->b2b_vehicle_at_garage;
	$ready_stage = $row_booking->b2b_vehicle_ready;
	$credit_amt = $row_booking->b2b_credit_amt;
	
	if(!in_array($row_booking->crm_update_id,array_keys($crms_arr)))
	{
		continue;
	}
	
	$crms_arr[$row_booking->crm_update_id]['goaxles'] = $crms_arr[$row_booking->crm_update_id]['goaxles'] + 1;
	if(($service_status == 'Completed' || $service_status == 'In Progress') || ($check_in_stage == 1 || $at_garage_stage == 1 || $ready_stage == 1))
	{
		$crms_arr[$row_booking->crm_update_id]['converted'] = $crms_arr[$row_booking->crm_update_id]['converted'] + 1;
		$crms_arr[$row_booking->crm_update_id]['rewards'] = $crms_arr[$row_booking->crm_update_id]['rewards'] + ($credit_amt * (.25));
	}
	$crms_arr[$row_booking->crm_update_id]['nonconverted'] = $crms_arr[$row_booking->crm_update_id]['goaxles'] - $crms_arr[$row_booking->crm_update_id]['converted'];
	
}
foreach(array_keys($crms_arr) as $crm)
{

	if($$crms_arr[$crm]['rewards_earned'] != $crms_arr[$crm]['rewards'])
	{
		$update = "UPDATE crm_admin SET irp_rewards='".$crms_arr[$crm]['rewards']."' WHERE crm_log_id = '$crm'";
		$res_update = mysqli_query($conn,$update) or die(mysqli_error($conn));
	}
}
$sql_crm_list = "SELECT * FROM crm_admin WHERE  flag = 0 AND  (city='$city' OR city='all')  ORDER BY irp_rewards DESC";
// $sql_crm_list = "SELECT * FROM crm_admin ORDER BY irp_rewards DESC";
$res_crm_list = mysqli_query($conn,$sql_crm_list);
while($row_crm_list = mysqli_fetch_object($res_crm_list))
{
	$crms_arr[$row_crm_list->crm_log_id]['rewards'] = $row_crm_list->irp_rewards;
	$crms_arr[$row_crm_list->crm_log_id]['rewards_claimed'] = $row_crm_list->irp_rewards_claimed;
	$data['earned'] = $crms_arr[$crm_log_id]['rewards'];
	$data['claimed'] = $crms_arr[$crm_log_id]['rewards_claimed'];
}
$no=1;
foreach($crms_arr as $crm)
{
	$tr1 = "<tr>";
	$td1 = "<td style='text-align:left;vertical-align:inherit;'><p style='width:133px;margin:0px;'>";
	$td1 = $td1.$crm['name'];
	if($no==1&&$crm['rewards']!=0){ $td1 = $td1."<img src='../images/1st.png' style='padding-left: 8px;width:40px;'></img>"; }
	if($no==2&&$crm['rewards']!=0){ $td1 = $td1."<img src='../images/2nd.png' style='padding-left: 8px;width:40px;'></img>"; }
	if($no==3&&$crm['rewards']!=0){ $td1 = $td1."<img src='../images/3rd.png' style='padding-left: 8px;width:40px;'></img>"; }
	$td1 = $td1."</p></td>";
	$td2 = "<td style='text-align:center;vertical-align:inherit;'>".$crm['goaxles']."</td>";
	$td3 = "<td style='text-align:center;vertical-align:inherit;'>".$crm['converted']."</td>";
	$td4 = "<td style='text-align:center;vertical-align:inherit;'>".$crm['nonconverted']."</td>";
	$td5 = "<td style='text-align:center;vertical-align:inherit;'><i class='fa fa-inr' aria-hidden='true' style='padding-right: 2px;padding-left: 1px;'></i>".$crm['rewards']."</td>";
	$tr1_l = "</tr>";
	
	$str = $tr1.$td1.$td2.$td3.$td4.$td5.$tr1_l;
	$data1[] = $str;
	$no = $no+1;
}

$data['tbl_data'] = $data1;

echo json_encode($data);
?>

<?php
//error_reporting(E_ALL); ini_set('display_errors', 1);
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$flag=$_SESSION['flag'];

$table = $_GET['table'];
$vehicle = $_GET['vehicle'];
$city = $_GET['city'];
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

$col_name = $vehicle == '2w' ? 'l.bike_cluster' : 'l.car_cluster';

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;
$today = date('Y-m-d');
$last_two_weeks = date('y-m-d', strtotime('-13 days'));
$begin = new DateTime($last_two_weeks);
$end   = new DateTime($today);
$datesArr = [];
for($i = $begin; $i <= $end; $i->modify('+1 day'))
{
	$date = $i->format("Y-m-d");
	$datesArr[] = $date;
	$dateData[$date] = [];
}
$dates = join("','",$datesArr);

$cond ='';

$cond = $cond.($vehicle == 'all' ? "" : "AND m.b2b_vehicle_type='$vehicle'");
$cond = $cond.($city == 'all' ? "" : "AND m.b2b_address5='$city'");
$cond = $cond.($cluster == 'all' ? "" : ($vehicle != 'all' ? "AND ".$col_name."='$cluster'" : "AND (case when m.b2b_vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'"));

$sql_mec = "SELECT DISTINCT gm.mec_id,m.b2b_shop_id,m.b2b_shop_name,m.b2b_subscription_date,m.b2b_renw_date,m.b2b_promised_leads,gm.exception_stage FROM b2b.b2b_mec_tbl as m INNER JOIN go_bumpr.admin_mechanic_table as gm ON m.b2b_shop_id=gm.axle_id INNER JOIN b2b.b2b_credits_tbl as c ON m.b2b_shop_id=c.b2b_shop_id LEFT JOIN go_bumpr.localities as l ON m.b2b_address4 = l.localities WHERE c.b2b_partner_flag='2' {$cond} ORDER BY m.b2b_shop_name ASC";
$res_mec = mysqli_query($conn2,$sql_mec) or die(mysqli_error($conn2)); 

$count = mysqli_num_rows($res_mec); 
if($count >0){
    /*switch($table){
        case 'table1': */
                while($row_mec = mysqli_fetch_object($res_mec)){
                    $shop_id = $row_mec->mec_id;
                    $shop_name = $row_mec->b2b_shop_name;
                    $start_date = $row_mec->b2b_subscription_date;
                    $end_date = $row_mec->b2b_renw_date;
                    $promised = $row_mec->b2b_promised_leads;
                    $exception_stage = $row_mec->exception_stage;
                    $booking_ids = "";
					if($exception_stage >= -1 && $exception_stage <= 3)
					{
						$limit = $exception_stage;
						if($limit == -1)
						{
							$limit = 3;
						}
						$exception_query = "SELECT f.booking_id FROM user_booking_tb b LEFT JOIN feedback_track f ON f.booking_id = b.booking_id WHERE b.exception_flag = 1 and b.mec_id = '$shop_id' ORDER BY f.crm_update_time DESC LIMIT $limit";
						$res_exception = mysqli_query($conn,$exception_query);
						$count = 0;
						$booking_ids = "";
						while($row_exception = mysqli_fetch_object($res_exception))
						{
							if($count>0)
							{
								$booking_ids = $booking_ids.", ";
							}
							$booking_ids = $booking_ids.$row_exception->booking_id;
							$count = $count + 1;
						}
					}
					$exception_date = date('d-M-Y H:i',strtotime($row_garage->exception_date));
                    $sql_counts = "SELECT em.exception_completed,em.leads_sent,sum(b.b2b_credit_amt) as total_credits,count(b.b2b_booking_id) as sent_count,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1')) THEN 1 ELSE 0 END ) AS completed_count FROM user_booking_tb as g INNER JOIN b2b.b2b_booking_tbl as b ON g.booking_id=b.gb_booking_id  LEFT JOIN b2b.b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id LEFT JOIN (SELECT sum(vehicles_completed) as exception_completed,sum(leads_sent) as leads_sent,mec_id FROM exception_mechanism_track WHERE mec_id = '$shop_id' and flag=0) em ON em.mec_id='$shop_id' WHERE g.mec_id='$shop_id' AND g.axle_flag='1' AND b.b2b_swap_flag!='1'  AND s.b2b_acpt_flag='1' AND DATE(b.b2b_log) BETWEEN '$start_date' AND '$end_date' ";
					$res_counts = mysqli_query($conn,$sql_counts);

                    $row_counts = mysqli_fetch_object($res_counts);

                    $sent = $row_counts->sent_count;
                    $completed = $row_counts->completed_count;
                    $exception_completed = $row_counts->exception_completed;
                    $exception_leads = $row_counts->leads_sent;
                    $total_credits = $row_counts->total_credits;

                    $r_date = new DateTime($today);
                    $r_date1 = new DateTime($start_date);
                    $r_date2 = new DateTime($end_date);

                    $days_remaining = $r_date2->diff($r_date)->format("%a");
                    $days_passed = $r_date->diff($r_date1)->format("%a");

                    $completed_per = $promised !='0' ? round(($completed/$promised)*100,1) : '0' ;
                    $current = $days_passed != '0' ? round($completed/$days_passed,1) : '0' ;
                    $req = $days_remaining != '0' ? round(($promised-$completed)/$days_remaining,1) : '0' ;
                    $end = $sent!= '0' ? round(($completed/$sent)*100,1) : '0'; 


                    $tr = '<tr>';
                    switch($exception_stage){
                        //case '1': $icon = '<i style="padding-left:5px;font-size:25px;color:FFBA00" title="1st Warning!"  class="fa fa-exclamation"></i>';break;
						case '1': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exception1.svg">';break;
						case '2': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exception2.svg">';break;
						case '3': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exception3.svg">';break;
						case '-1': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exceptionlock.svg">';break;
                        default:$icon = '';
                    }
                    $td1 = '<td>'.$shop_name.$icon.'</td>';
                    $td2 = '<td style="text-align:center;">'.date('d M Y',strtotime($start_date)).'</td>';
                    $td3 = '<td style="text-align:center;">'.date('d M Y',strtotime($end_date)).'</td>';
                    $td4 = '<td style="text-align:center;">'.$promised.'</td>';
                    
                    $td5 = '<td style="text-align:center;"><button type="button" style="background-color:transparent;text-decoration:underline;font-size:16px;padding:0px;" class="btn" data-start="'.$start_date.'" data-end="'.$end_date.'" data-shop="'.$shop_id.'" data-type="completed" data-total="'.$completed.'" data-toggle="modal" data-target="#myModalcompleted">'.$completed.'</button></td>';
                    $td6 = '<td style="text-align:center;">'.($exception_completed + $exception_leads).'</td>';
                    $td6 = $td6.'<td style="text-align:center;">'.$completed_per.' %</td>';
                    $td7 = '<td style="text-align:center;">'.$days_remaining.'</td>';
                    $td8 = '<td style="text-align:center;">'.$current.'</td>';
                    $td9 = '<td style="text-align:center;">'.$req.'</td>';
                    $td10 = '<td style="text-align:center;"><button type="button" style="background-color:transparent;text-decoration:underline;font-size:16px;padding:0px;" class="btn" data-start="'.$start_date.'" data-end="'.$end_date.'" data-shop="'.$shop_id.'" data-type="sent" data-total="'.$sent.'" data-toggle="modal" data-target="#myModalsent">'.$sent.'</button></td>';
                	
                    switch(true){
                        case ($end >= 70): $td11 = '<td style="text-align:center;background-color:#00ff4e70;">'.$end.' %</td>'; break;
                        case ($end <70 && $end>= 60): $td11 = '<td style="text-align:center;background-color:#ffa800ad;">'.$end.' %</td>'; break;
                        case ($end <60 && $end>= 40): $td11 = '<td style="text-align:center;background-color:#a25506a6;">'.$end.' %</td>'; break;
                        case ($end < 40): $td11 = '<td style="text-align:center;background-color:#ff2f009c;">'.$end.' %</td>'; break;
                        default: $td11 = '<td style="text-align:center;">'.$end.'%</td>';
                    }
                    
					$cpcom = $total_credits/$completed;
					$td12 = '<td style="padding-left: 3.75%;"><i class="fa fa-inr" style="padding-right: 5px;"></i>'.round($cpcom,2).'</td>';
                    $tr_l ='</tr>';
                    
                    $str = $tr.$td.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$td9.$td10.$td11.$td12.$tr_l;
                    // $str = $tr.$td.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$td9.$td10.$td11.$tr_l;
                    $data[] = array('tr'=>$str);
                //}
        /*break;
        case 'table2':*/
                //while($row_mec = mysqli_fetch_object($res_mec)){
					
					//----------------
                    
					$shop_id = $row_mec->mec_id;
                    $shop_name = $row_mec->b2b_shop_name;
                    $exception_stage = $row_mec->exception_stage;
                    $tr = '';
                    $tr = $tr.'<tr>';
                    switch($exception_stage){
                        //case '1': $icon = '<i style="padding-left:5px;font-size:25px;color:FFBA00" title="1st Warning!"  class="fa fa-exclamation"></i>';break;
						case '1': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exception1.svg">';break;
						case '2': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exception2.svg">';break;
						case '3': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exception3.svg">';break;
						case '-1': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exceptionlock.svg">';break;
                        default:$icon = '';
                    }
                    $tr = $tr.'<td rowspan="2" style="vertical-align:middle;">'.$shop_name.$icon.'</td>';
                    $tr = $tr.'<td>GoAxles</td>';
					$sql_counts = "SELECT b.b2b_booking_id,b.b2b_check_in_report,g.service_status,b.b2b_source,b.b2b_log FROM go_bumpr.user_booking_tb AS g INNER JOIN b2b.b2b_booking_tbl AS b ON g.booking_id = b.gb_booking_id LEFT JOIN b2b.b2b_status AS s ON b.b2b_booking_id = s.b2b_booking_id WHERE g.mec_id = '$shop_id' AND g.axle_flag = '1' AND b.b2b_swap_flag != '1' AND s.b2b_acpt_flag = '1' AND (DATE(b.b2b_log) IN ('$dates')) ORDER BY b.b2b_log;";
					$res_counts = mysqli_query($conn,$sql_counts);
					$dateData = [];
					while($row_counts = mysqli_fetch_object($res_counts))
					{
						$b2b_log = date('Y-m-d', strtotime($row_counts->b2b_log));
						$b2b_booking_id = $row_counts->b2b_booking_id;
						$dateData[$b2b_log]['b2b_booking_id'][] = $b2b_booking_id;
						$b2b_check_in_report = $row_counts->b2b_check_in_report;
						$dateData[$b2b_log]['b2b_check_in_report'][] = $b2b_check_in_report;
						$service_status = $row_counts->service_status;
						$dateData[$b2b_log]['service_status'][] = $service_status;
						$b2b_source = $row_counts->b2b_source;
						$dateData[$b2b_log]['b2b_source'][] = $b2b_source;
					}
					$begin = new DateTime($last_two_weeks);
					$end   = new DateTime($today);
					for($i = $begin; $i <= $end; $i->modify('+1 day'))
					{
						$date = $i->format("Y-m-d");
						$sent = count($dateData[$date]['b2b_booking_id']);
						$tr = $tr.'<td style="text-align:center;">'.$sent.'</td>';
                    }
                    $tr =$tr.'</tr>';
                    //error_reporting(E_ALL); ini_set('display_errors', 1);
                    $tr = $tr.'<tr><td>Completed</td>';
                    $begin = new DateTime($last_two_weeks);
                    $end   = new DateTime($today);
                    for($j = $begin; $j <= $end; $j->modify('+1 day')){
						
						
                        $date = $j->format("Y-m-d");
                        $dateData[$date]['completed'] = 0;
						$sent = count($dateData[$date]['b2b_booking_id']);
						if(count($dateData[$date]['b2b_booking_id'])>0)
						{
							for($k = 0;$k<count($dateData[$date]['b2b_booking_id']);$k++)
							{
								if(($dateData[$date]['service_status'][$k] == 'Completed' || $dateData[$date]['service_status'][$k] == 'In Progress') || ($dateData[$date]['b2b_check_in_report'][$k] == 1 && $dateData[$date]['b2b_source'][$k] == 'lead_convert'))
								{
									$dateData[$date]['completed'] = $dateData[$date]['completed'] + 1;
								}
							}		
						}
						$completed = $dateData[$date]['completed'];

                        $endc = $sent!= '0' ? round(($completed/$sent)*100,1) : '0';
                        switch(true){
                            case ($endc >= 70): $tr = $tr.'<td style="text-align:center;text-decoration: none;border-top:3px solid green;">'.$completed.'</td>'; break;
                            case ($endc <70 && $endc>= 60): $tr = $tr.'<td style="text-align:center;text-decoration: none;border-top:3px solid orange;">'.$completed.'</td>'; break;
                            case ($endc <60 && $endc>= 40): $tr = $tr.'<td style="text-align:center;text-decoration: none;border-top:3px solid brown;">'.$completed.'</td>'; break;
                            case ($endc < 40): $tr = $tr.'<td style="text-align:center;text-decoration: none;border-top:3px solid red;">'.$completed.'</td>'; break;
                            default: $tr = $tr.'<td style="text-align:center;">'.$completed.'</td>';
                        }

                        //$tr = $tr.'<td style="text-align:center;">'.$completed.'</td>';
                    }
                    $tr =$tr.'</tr>';
					
					//$tr = "ok";
					//-------
                    
					$data1[] = array('tr'=>$tr);
                }
        /*break;
    }*/
	$res_data["data1"] = $data;
	$res_data["data2"] = $data1;
    echo $result = json_encode($res_data);
} // if
else {
    echo "no";
}

 ?>

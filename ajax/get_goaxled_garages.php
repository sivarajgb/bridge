<?php
include("../config.php");
$conn = db_connect1();
session_start();
$crm_log_id = $_SESSION['crm_log_id'] ;
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$city = $_GET['city'];
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];
?>

<option value="all" selected>All Garages</option>
<?php

$sql_garage = $cluster != 'all' ? ($flag == '1' ? "SELECT DISTINCT b.mec_id,b.shop_name FROM user_booking_tb as b LEFT JOIN localities as l ON b.locality=l.localities WHERE (l.car_cluster = '$cluster' || l.bike_cluster = '$cluster' ) AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' AND b.city='$city' AND b.axle_flag='1' ORDER BY  b.shop_name ASC" : "SELECT DISTINCT b.mec_id,b.shop_name FROM user_booking_tb as b LEFT JOIN localities as l ON b.locality=l.localities WHERE (l.car_cluster = '$cluster' || l.bike_cluster = '$cluster' ) AND DATE(b.crm_update_time) BETWEEN '$startdate' AND '$enddate' AND b.city='$city' AND b.axle_flag='1' AND b.crm_update_id='$crm_log_id' ORDER BY  b.shop_name ASC") : ($flag == '1' ? "SELECT DISTINCT mec_id,shop_name FROM user_booking_tb WHERE DATE(crm_update_time) BETWEEN '$startdate' AND '$enddate' AND city='$city' AND axle_flag='1' ORDER BY  shop_name ASC" : "SELECT DISTINCT mec_id,shop_name FROM user_booking_tb WHERE DATE(crm_update_time) BETWEEN '$startdate' AND '$enddate' AND city='$city' AND axle_flag='1' AND crm_update_id='$crm_log_id' ORDER BY  shop_name ASC") ;

//echo $sql_garage;
    $res_garage = mysqli_query($conn,$sql_garage);
    while($row_garage = mysqli_fetch_object($res_garage)){
        $garage_id = $row_garage->mec_id;
        $garage_name = $row_garage->shop_name;
    ?>
        <option value="<?php echo $garage_id; ?>"><?php echo $garage_name; ?></option>
        <?php
    }
?>
<?php
error_reporting(E_ALL); ini_set('display_errors', 0);
ini_set('max_execution_time', -1);
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$flag=$_SESSION['flag'];

$table = $_GET['table'];
$vehicle = $_GET['vehicle'];
$city = $_GET['city'];
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

$col_name = $vehicle == '2w' ? 'l.bike_cluster' : 'l.car_cluster';

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;
$today = date('Y-m-d');
$last_two_weeks = date('y-m-d', strtotime('-13 days'));
$begin = new DateTime($last_two_weeks);
// $till_date   = new DateTime($today);
$datesArr = [];
for($i = $begin; $i <= $end; $i->modify('+1 day'))
{
    $date = $i->format("Y-m-d");
    $datesArr[] = $date;
    $dateData[$date] = [];
}
$dates = join("','",$datesArr);
$data.='<thead>
        <th style="text-align:center;vertical-align: middle;">Garage name</th>
      <th style="text-align:center;vertical-align: middle;">Vehicle type</th>
      <th style="text-align:center;vertical-align: middle;">Premium Model</th>
      <th style="text-align:center;vertical-align: middle;">Start Date</th>
      <th style="text-align:center;vertical-align: middle;">Promise</th>
      <th style="text-align:center;vertical-align: middle;">Lead Price</th>
      <th style="text-align:center;vertical-align: middle;">Goaxles send</th>
      <th style="text-align:center;vertical-align: middle;">Total EBM count</th>
      <th style="text-align:center;vertical-align: middle;">Total EBM Lead deducted </th>';
      if($vehicle=="2w"){
        $data.='<th style="text-align:center;vertical-align: middle;">Remaing RE Lead </th>
        <th style="text-align:center;vertical-align: middle;">Remaing NRE Lead </th>';
      }else{
        $data.='<th style="text-align:center;">Remaing Lead </th>';
      }
      $data.='<th style="text-align:center;vertical-align: middle;">Completed</th>
              <th style="text-align:center;vertical-align: middle;">End Convertion</th>
              <th style="text-align:center;">End Convertion Rate</th>
              <th style="text-align:center;vertical-align: middle;">ROI </th>
       </thead><tbody>';
$cond ='';
$cond = $cond.($vehicle == 'all' ? "" : "AND m.b2b_vehicle_type='$vehicle'");
$cond = $cond.($city == 'all' ? "" : "AND m.b2b_address5='$city'");
$sql_mec ="SELECT DISTINCT m.b2b_shop_id,m.b2b_shop_name,m.b2b_subscription_date,m.b2b_renw_date,m.b2b_promised_leads,gm.exception_stage,m.b2b_vehicle_type,c.b2b_credits,c.b2b_leads,c.b2b_re_leads,c.b2b_non_re_leads FROM b2b.b2b_mec_tbl as m JOIN go_bumpr.admin_mechanic_table as gm ON m.b2b_shop_id=gm.axle_id JOIN b2b.b2b_credits_tbl as c ON m.b2b_shop_id=c.b2b_shop_id  WHERE c.b2b_partner_flag='2' and gm.premium='1' AND gm.premium2='1' {$cond} ORDER BY m.b2b_shop_name ASC 
";
//echo $sql_mec;

$res_mec = mysqli_query($conn2,$sql_mec) or die(mysqli_error($conn2)); 

$count = mysqli_num_rows($res_mec); 
if($count >0){
    //$shop_id='';
    while($row_mec = mysqli_fetch_object($res_mec)){
        $end_rate=0;
                    $shop_id = $row_mec->b2b_shop_id;
                    $shop_name = $row_mec->b2b_shop_name;
                    $start_date = $row_mec->b2b_subscription_date;
                    $promised = $row_mec->b2b_promised_leads;
                    $exception_completed = $row_counts->exception_completed;
                    $exception_leads = $row_counts->leads_sent;
                    $vehical_type = $row_mec->b2b_vehicle_type;
                    $city = $row_mec->b2b_address5;
                    $Total_EBM = $row_mec->b2b_credits;
                    $b2b_re_leads=$row_mec->b2b_re_leads;
                    $b2b_nre_leads=$row_mec->b2b_non_re_leads;
                    $b2b_leads=$row_mec->b2b_leads;
                    
                    
$sql_count="SELECT 
COUNT(CASE WHEN s.b2b_acpt_flag = 1 THEN 1 END) as Goaxle,
count(case when ub.booking_status='2' and axle_flag='1' and bb.b2b_swap_flag!=1 and (ub.service_status='Completed' or ub.service_status='In Progress' or bb.b2b_vehicle_at_garage='1') then ub.booking_id else NULL end) 
  as Endconversion,
  SUM(CASE WHEN  CAST(ub.final_bill_amt AS UNSIGNED) > CAST( bb.b2b_bill_amount AS UNSIGNED) THEN ub.final_bill_amt ELSE bb.b2b_bill_amount END) AS ROI,
count(ub.service_status = 'Completed') AS completed_count,
count(bb.b2b_booking_id) as sent_count
from go_bumpr.user_booking_tb as ub 
join b2b.b2b_booking_tbl as bb 
on 
ub.booking_id=bb.gb_booking_id
left join go_bumpr.exception_mechanism_track as em 
on
ub.booking_id=em.booking_id
join b2b.b2b_mec_tbl  as mt 
on 
bb.b2b_shop_id=mt.b2b_shop_id
LEFT JOIN b2b.b2b_status s ON s.b2b_booking_id=bb.b2b_booking_id
where mt.b2b_shop_id='$shop_id' and 
bb.b2b_log  between '$start_date' and '$today' 
group by mt.b2b_shop_id";

 //echo $sql_count;die;
$sql_exception ="SELECT count(exception_log) as Total_EBM_count,sum(leads_sent)  as EBM from exception_mechanism_track where b2b_shop_id='$shop_id' AND DATE(exception_log) BETWEEN '$start_date' AND '$today'  AND flag=0  ORDER BY exception_log DESC";

//echo $sql_exception;
$res_exception = mysqli_query($conn,$sql_exception);

$no = 0;

$count = mysqli_num_rows($res_exception);
if($count >0){
    while($row_exception = mysqli_fetch_object($res_exception))
{
    $tebm_count=$row_exception->Total_EBM_count;
    $lead_deduct_count=$row_exception->EBM;
}
} 

   $sql_lead="select * FROM go_bumpr.garage_model_history where b2b_shop_id='$shop_id' and model='Premium 2.0' order by start_date desc limit 1 ";
    $res_lead=mysqli_query($conn,$sql_lead);
    $row_lead=mysqli_fetch_object($res_lead);
    $b2b_shop_id=$row_lead->b2b_shop_id;
    $leads=$row_lead->leads;
    $nre_leads=$row_lead->nre_leads;
    $re_leads=$row_lead->re_leads;
    
    $sqli_mec=mysqli_query($conn2,"select * from b2b.b2b_mec_tbl where b2b_shop_id='$b2b_shop_id'");
    $sqli_fetch=mysqli_fetch_object($sqli_mec);
 //echo $sql_lead;
$wheeler=$sqli_fetch->b2b_vehicle_type;
if($wheeler=='4w')
{
    $lead_price=$row_lead->lead_price;
}
elseif($wheeler=='2w')
{
  if ($nre_leads != '0')
   {
      $lead_price=$row_lead->nre_lead_price;
  } 
  else 
  {
      $lead_price=$row_lead->re_lead_price;
  }
  
}
//echo $lead_price;
    $res_go=mysqli_query($conn,$sql_count);
    //print_r($res_go);
    $row_go=mysqli_fetch_object($res_go);
    $sent = $row_counts->sent_count;
    $gcout = $row_go->Goaxle;
    $end_conv = $row_go->Endconversion;
    $ebm = $row_go->EBM;
    $ebm_count = $row_go->Total_EBM_count;
    $completed = $row_go->completed_count;
    $exception_completed = $row_counts->exception_completed;
    $exception_leads = $row_counts->leads_sent;

    $end_rate= number_format((($row_go->Endconversion/$row_go->Goaxle)*100),2);
    $end = $sent!= '0' ? round(($completed/$sent)*100,1) : '0'; 
    $total_ebm_count = number_format( $tebm_count*2);
    $roi = $row_go->ROI;
    if ($lead_deduct_count!=''){
        $newlead_deduct_count=$lead_deduct_count;
    }
    else{
        $newlead_deduct_count=0;


    }
    
    //echo $lead_price;
    
    //echo $gcout;
                    $data.='<tr>
                    <td style="text-align:center;">'.$shop_name.'</td>
                    <td style="text-align:center;">'.$vehical_type.'</td>
                    <td style="text-align:center;">Premium 2.0</td>
                    <td style="text-align:center;">'.date('d M Y',strtotime($start_date)).'</td>
                    <td style="text-align:center;">'.$promised.'</td>
                    <td style="text-align:center;"><i class="fa fa-rupee"></i>'.$lead_price.'</td>
                    <td style="text-align:center;">
                    <button type="button" style="background-color:transparent;text-decoration:underline;font-size:16px;padding:0px;" class="btn" data-start="'.$start_date.'" data-end="'.$today.'" data-shop="'.$shop_id.'" data-type="sent" data-total="'.$gcout.'" data-toggle="modal" data-target="#myModalsent">'.$gcout.'</button></td>

                    <td style="text-align:center;">'.$tebm_count.'</td>
                    <td style="text-align:center;">'.$total_ebm_count.'</td>';
                    if($vehicle=="2w"){
                    $data.='
                    <td style="text-align:center;">'.$b2b_re_leads.'</td>
                    <td style="text-align:center;">'.$b2b_nre_leads.'</td>';
                  }
                  else
                  {
                    $data.='<td style="text-align:center;">'.$b2b_leads.'</td>';

                  }
                   $data.='<td style="text-align:center;">'.$completed.'</td>
                          <td style="text-align:center;">'.$end_conv.'</td>';
                          if($end_rate>=70) 
                            {
                               $data.='<td style="text-align:center; background-color:green;">'.$end_rate.'%</td>';     
                            }
                            else if($end_rate <70 && $end_rate>= 60)
                            {
                                $data.='<td style="text-align:center; background-color:orange;">'.$end_rate.'%</td>';        
                            }
                            else if($end_rate <60 && $end_rate>= 40)
                            { 
                                $data.='<td style="text-align:center; background-color:brown;">'.$end_rate.'%</td>';        
                            }            
                            else if($end_rate < 40)
                            {
                                $data.='<td style="text-align:center; background-color:red">'.$end_rate.'%</td>';
                            }
                            else 
                            {
                                $data.='<td style="text-align:center;">'.$end_rate.'%</td>';        
                            }

                          
                           $data.='<td style="text-align:center;"nowrap><i class="fa fa-rupee"></i>'.$roi.'</td>';
} 

$data.='</tbody>';
// if
echo json_encode($data);

}
else {
    echo json_encode("no"); 
}

 ?>




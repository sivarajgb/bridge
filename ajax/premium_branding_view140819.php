<?php
error_reporting(E_ALL); ini_set('display_errors', 0);
ini_set('max_execution_time', -1);
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$flag=$_SESSION['flag'];

$table = $_GET['table'];
$vehicle = $_GET['vehicle'];
$city = $_GET['city'];
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

$col_name = $vehicle == '2w' ? 'l.bike_cluster' : 'l.car_cluster';

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;
$today = date('Y-m-d');
$last_two_weeks = date('y-m-d', strtotime('-13 days'));
$begin = new DateTime($last_two_weeks);
$end   = new DateTime($today);
$datesArr = [];
for($i = $begin; $i <= $end; $i->modify('+1 day'))
{
    $date = $i->format("Y-m-d");
    $datesArr[] = $date;
    $dateData[$date] = [];
}
$dates = join("','",$datesArr);

$cond ='';
$cond = $cond.($vehicle == 'all' ? "" : "AND m.b2b_vehicle_type='$vehicle'");
$cond = $cond.($city == 'all' ? "" : "AND m.b2b_address5='$city'");
$sql_mec ="SELECT DISTINCT m.b2b_shop_id,m.b2b_shop_name,m.b2b_subscription_date,m.b2b_renw_date,m.b2b_promised_leads,gm.exception_stage,m.b2b_vehicle_type,c.b2b_credits,c.b2b_leads,c.b2b_re_leads FROM b2b.b2b_mec_tbl as m JOIN go_bumpr.admin_mechanic_table as gm ON m.b2b_shop_id=gm.axle_id JOIN b2b.b2b_credits_tbl as c ON m.b2b_shop_id=c.b2b_shop_id  WHERE c.b2b_partner_flag='2' and gm.premium='1' AND gm.premium2='1' {$cond} ORDER BY m.b2b_shop_name ASC 
";
//echo $sql_mec;

$res_mec = mysqli_query($conn2,$sql_mec) or die(mysqli_error($conn2)); 

$count = mysqli_num_rows($res_mec); 
if($count >0){
    //$shop_id='';
    while($row_mec = mysqli_fetch_object($res_mec)){
        $end_rate=0;
                    $shop_id = $row_mec->b2b_shop_id;
                    $shop_name = $row_mec->b2b_shop_name;
                    $start_date = $row_mec->b2b_subscription_date;
                    $end_date = $row_mec->b2b_renw_date;
                    $promised = $row_mec->b2b_promised_leads;
                    $exception_completed = $row_counts->exception_completed;
                    $exception_leads = $row_counts->leads_sent;
                    $vehical_type = $row_mec->b2b_vehicle_type;
                    $city = $row_mec->b2b_address5;
                    $Total_EBM = $row_credits->b2b_credits;
                    

                    $sql_count="SELECT count(case when b.booking_status='2' and axle_flag='1' and bb.b2b_swap_flag!=1 then b.booking_id else NULL end) as Goaxle,
count(case when b.booking_status='2' and axle_flag='1' and bb.b2b_swap_flag!=1 and (b.service_status='Completed' or b.service_status='In Progress' or bb.b2b_vehicle_at_garage='1') then b.booking_id else NULL end) as Endconversion,
sum(case when mt.b2b_new_flg='1' then em.leads_sent else em.credits_deducted end ) as EBM,
sum(bb.b2b_bill_amount) as ROI
from go_bumpr.user_booking_tb as b join b2b.b2b_booking_tbl as bb on b.booking_id=bb.gb_booking_id
left join go_bumpr.exception_mechanism_track as em on b.booking_id=em.booking_id
join b2b.b2b_mec_tbl  as mt on bb.b2b_shop_id=mt.b2b_shop_id where mt.b2b_shop_id='$shop_id' and bb.b2b_swap_flag!='1' and bb.b2b_log between '$start_date' and '$end_date' group by mt.b2b_shop_id";
 //echo $sql_count;
    $res_go=mysqli_query($conn,$sql_count);
    //print_r($res_go);
    $row_go=mysqli_fetch_object($res_go);
    // print_r($row_go);
    $gcout = $row_go->Goaxle;
    $end = $row_go->Endconversion;
    $ebm = $row_go->EBM;
    $end_rate= number_format((($row_go->Endconversion/$row_go->Goaxle)*100),2);
    $roi = $row_go->ROI;
    if ($ebm!=''){
        $new=$ebm;
    }
    else{
        $new=0;
    }
    //echo $end;
    
    //echo $gcout;
                    $data.='<tr>
                    <td>'.$shop_name.'</td>
                    <td>'.$vehical_type.'</td>
                    <td style="text-align:center;">'.date('d M Y',strtotime($start_date)).'</td>
                    <td style="text-align:center;">'.date('d M Y',strtotime($end_date)).'</td>
                    <td style="text-align:center; background-color:#ffa800">'.$promised.'</td>
                    <td style="text-align:center; background-color:#6FA3DE">
                    <button type="button" style="background-color:transparent;text-decoration:underline;font-size:16px;padding:0px;" class="btn" data-start="'.$start_date.'" data-end="'.$end_date.'" data-shop="'.$shop_id.'" data-type="sent" data-total="'.$gcout.'" data-toggle="modal" data-target="#myModalsent">'.$gcout.'</button></td>
                    <td style="text-align:center; background-color:#ff80aa">'.$end.'</td>
                    <td style="text-align:center; background-color:#5cd65c">'.$end_rate.'</td>
                    <td style="text-align:center; background-color:#FA8072">'.$new.'</td>
                    <td style="text-align:center; background-color:#00ff00">'.$roi.'</td>

                    </tr>';
                    
    
} // if
echo json_encode($data);

}
else {
    echo json_encode("no"); 
}

 ?>


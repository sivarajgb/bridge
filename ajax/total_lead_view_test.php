<?php

include("../config.php");
$conn = db_connect3();
$conn1 = db_connect1();
session_start();
$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];

$veh_type = $_POST['veh'];
$city = $_POST['city'];
$loc = $_POST['loc'];
$status=$_POST['status'];

$_SESSION['crm_city'] = $city;
error_reporting(0);
//ini_set('display_errors', 1);

//0-all && 1-particular
class shops {
  private $veh;
  private $loc;

  function __construct($veh, $loc){
    $this->veh = $veh;
    $this->loc = $loc;
  }
  function v0_l0(){
    return "";
  }
  function v0_l1(){
    return "AND m.b2b_address4 ='$this->loc'";
  }
  function v1_l0(){
    return "AND m.b2b_vehicle_type = '$this->veh'";
  }
  function v1_l1(){
    return "AND m.b2b_vehicle_type = '$this->veh' AND m.b2b_address4 ='$this->loc'";
  }
}
$shops_obj = new shops($veh_type, $loc);
$i=0;
$veh_val = $veh_type=="all" ? "0" : "1";
$loc_val = $loc=="" ? "0" : "1";

$cond = $shops_obj->{"v{$veh_val}_l{$loc_val}"}();

// if($status=="active"){
//   $cond1="AND g.end_date = '0000-00-00'";
// }
// else if ($status=="inactive") {
//   $cond1 = "AND g.end_date != '0000-00-00'";
// }
// else{
//   $cond1 ="";
// }

$sql_shops="SELECT m.b2b_shop_id,m.b2b_shop_name,m.b2b_vehicle_type,g.start_date,g.end_date,g.leads,g.re_leads,g.nre_leads FROM b2b_mec_tbl AS m JOIN go_bumpr.garage_model_history AS g ON m.b2b_shop_id = g.b2b_shop_id WHERE m.b2b_address5 = '$city' AND g.model = 'Leads 3.0' {$cond} order by b2b_shop_name,start_date";
// $sql_shops="SELECT m.b2b_shop_id,m.b2b_shop_name,m.b2b_vehicle_type,g.start_date,g.end_date,g.leads,g.re_leads,g.nre_leads FROM b2b_mec_tbl AS m JOIN go_bumpr.garage_model_history AS g ON m.b2b_shop_id = g.b2b_shop_id WHERE m.b2b_address5 = '$city' AND g.model = 'Leads 3.0' {$cond} {$cond1} order by b2b_shop_name,start_date";
$res_garage = mysqli_query($conn,$sql_shops);
$count = mysqli_num_rows($res_garage);
if($count >0){
$garage_shops="SELECT b2b_shop_id,start_date,end_date,leads,re_leads,nre_leads FROM go_bumpr.garage_model_history where model='Leads 3.0' order by b2b_shop_id,start_date";
$res_garages = mysqli_query($conn,$garage_shops);
while($row_shop = mysqli_fetch_object($res_garages)){
  $shop_ids[]=$row_shop->b2b_shop_id;
  $start_dates[]=$row_shop->start_date;
  $end_dates[]=$row_shop->end_date;
  $prom_leads[]=$row_shop->leads;
  $prom_re_leads[]=$row_shop->re_leads;
  $prom_nre_leads[]=$row_shop->nre_leads;
}
// $sql_leads_accepted = "SELECT b.b2b_booking_id, b.b2b_shop_id, DATE(b.b2b_log) as date,b.brand,g.exception_flag as exceptions FROM b2b_booking_tbl AS b JOIN b2b_status AS s ON b.b2b_booking_id = s.b2b_booking_id JOIN go_bumpr.user_booking_tb AS g ON b.gb_booking_id = g.booking_id WHERE b.b2b_swap_flag != '1' AND s.b2b_acpt_flag = '1' AND s.b2b_deny_flag = '0'";
$sql_leads_accepted = "SELECT b.b2b_booking_id, b.b2b_shop_id,b.brand,DATE(b.b2b_log) as date FROM b2b_booking_tbl AS b JOIN b2b_status AS s ON b.b2b_booking_id = s.b2b_booking_id WHERE b.b2b_swap_flag != '1' AND s.b2b_acpt_flag = '1' AND s.b2b_deny_flag = '0'";
    $res_leads_accepted = mysqli_query($conn,$sql_leads_accepted);
    while($row_leads_accepted = mysqli_fetch_object($res_leads_accepted)){
        $accept_booking_id[]=$row_leads_accepted->b2b_booking_id;
        $accept_shop_id[]=$row_leads_accepted->b2b_shop_id;
        $accept_log[]=$row_leads_accepted->date;
        $brand[]=$row_leads_accepted->brand;
        // $exceptions[]=$row_leads_accepted->exceptions;
        // $exceptions[]=0;
    }
$sql_exceptions="SELECT a.b2b_shop_id,a.credits_deducted,b.brand,DATE(a.exception_log) AS date FROM exception_mechanism_track AS a JOIN b2b.b2b_booking_tbl AS b ON a.booking_id = b.gb_booking_id group by b.gb_booking_id";
$res_exceptions=mysqli_query($conn1,$sql_exceptions);
while ($row_exceptions=mysqli_fetch_object($res_exceptions)) {
  $exc_shop_id[]=$row_exceptions->b2b_shop_id;
  $exc_brand[]=$row_exceptions->brand;
  $exc_log[]=$row_exceptions->date;
}

$sql_leads_swapped = "SELECT b.b2b_booking_id,b.b2b_shop_id, DATE(b.b2b_log) as date,b.brand FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_swap_flag='1' AND s.b2b_acpt_flag='1' AND s.b2b_deny_flag='0'";
$res_leads_swapped = mysqli_query($conn,$sql_leads_swapped);
while($row_leads_swapped = mysqli_fetch_object($res_leads_swapped)){
  $swap_booking_id[]=$row_leads_swapped->b2b_booking_id;
  $swap_shop_id[]=$row_leads_swapped->b2b_shop_id;
  $swap_log[]=$row_leads_swapped->date;
  $swap_brand[]=$row_leads_swapped->brand;
}

$sql_credits="SELECT * FROM b2b.b2b_credits_tbl";
$res_credits = mysqli_query($conn,$sql_credits);
while($row_credits = mysqli_fetch_object($res_credits)){
  $b2b_credits_shop_id[]=$row_credits->b2b_shop_id;
  $b2b_credits_leads[]=$row_credits->b2b_leads;
}

$no = 0;
$prev_leads=0;
$prev_leads_re=0;
$prev_leads_nre=0;
$prev_shop_id=1062;

while($row_shops = mysqli_fetch_object($res_garage)){
    $shop_id = $row_shops->b2b_shop_id;
    $shop_name = $row_shops->b2b_shop_name;
    $start_date=$row_shops->start_date;
    $end_date=$row_shops->end_date;
    $end_date=$row_shops->end_date;
    $promised_leads=$row_shops->leads;
    $promised_re_leads=$row_shops->re_leads;
    $promised_nre_leads=$row_shops->nre_leads;
    $b2b_vehicle_type=$row_shops->b2b_vehicle_type;

    //accept_leads
    $accept_count=0;
    $accept_count_re=0;
    $accept_count_nre=0;
    for($i=0;$i<sizeof($accept_booking_id);$i++){
      if($accept_shop_id[$i]==$shop_id){
        if($end_date=="0000-00-00"){
          if($accept_log[$i]>=$start_date){
            $accept_count+=1;
            if($brand[$i]=="Royal Enfield"){
                $accept_count_re+=1;
              }
            else{
                $accept_count_nre+=1;
            }
          }  
        }
        else{
          if($accept_log[$i]>=$start_date&&$accept_log[$i]<=$end_date){
            $accept_count+=1;
            if($brand[$i]=="Royal Enfield"){
                $accept_count_re+=1;
              }
            else{
                $accept_count_nre+=1;
            }
          }
        }
      }
    }

    //swap_leads
    $swap_count=0;
    $swap_count_re=0;
    $swap_count_nre=0;
    for($i=0;$i<sizeof($swap_booking_id);$i++){
      if($swap_shop_id[$i]==$shop_id){
        if($end_date=="0000-00-00"){
          if($swap_log[$i]>=$start_date){
            $swap_count+=1;
            if($swap_brand[$i]=="Royal Enfield"){
                $swap_count_re+=1;
              }
            else{
                $swap_count_nre+=1;
            }
          }  
        }
        else{
          if($swap_log[$i]>=$start_date&&$swap_log[$i]<=$end_date){
            $swap_count+=1;
            if($swap_brand[$i]=="Royal Enfield"){
                $swap_count_re+=1;
              }
            else{
                $swap_count_nre+=1;
            }
          }
        }
      }
    }

    $swap = $swap_count_re+$swap_count_nre;

      //excep_leads
      $exc_count=0;
      $exc_count_re=0;
      $exc_count_nre=0;
      for($i=0;$i<sizeof($exc_shop_id);$i++){
        if($exc_shop_id[$i]==$shop_id){
          if($end_date=="0000-00-00"){
            if(strtotime($exc_log[$i])>=strtotime($start_date)){
              $exc_count+=1;
              if($exc_brand[$i]=="Royal Enfield"){
                $exc_count_re+=1;
              }
              else{
                $exc_count_nre+=1;
              }
            }  
          }
          else{
            if(strtotime($exc_log[$i])>=strtotime($start_date)&&strtotime($exc_log[$i])<=strtotime($end_date)){
              $exc_count+=1;
              if($exc_brand[$i]=="Royal Enfield"){
                $exc_count_re+=1;
              }
              else{
                $exc_count_nre+=1;
              }
            }
          }
        }
      }

      $ebm_count=$exc_count_re+$exc_count_nre;

    /////carry_leads
    for($i=0;$i<sizeof($shop_ids);$i++){
      if($shop_ids[$i]==$shop_id && $start_dates[$i]==$start_date){
        $j=$i-1;
        if(isset($shop_ids[$j])){
          if($shop_ids[$i]==$shop_ids[$j]){
            $carry_start_date=$start_dates[$j];
            $carry_end_date=$end_dates[$j];
            $carry_prom_leads=$prom_leads[$j];
            $carry_re_prom_leads=$prom_re_leads[$j];
            $carry_nre_prom_leads=$prom_nre_leads[$j];
            $value=1;
          }
          else{
            $value=0;
          }
        }
        else {
          $value=0;
        }
      }
    }
    if($value==1){
      ///carry_dates_exception
      $carry_exc_count=0;
      $carry_exc_count_re=0;
      $carry_exc_count_nre=0;
      for($i=0;$i<sizeof($exc_shop_id);$i++){
        if($exc_shop_id[$i]==$shop_id){
          if($carry_end_date=="0000-00-00"){
            if(strtotime($exc_log[$i])>=strtotime($carry_start_date)){
              $carry_exc_count+=1;
              if($exc_brand[$i]=="Royal Enfield"){
                $carry_exc_count_re+=1;
              }
              else{
                $carry_exc_count_nre+=1;
              }
            }  
          }
          else{
            if(strtotime($exc_log[$i])>=strtotime($carry_start_date)&&strtotime($exc_log[$i])<=strtotime($carry_end_date)){
              $carry_exc_count+=1;
              if($exc_brand[$i]=="Royal Enfield"){
                $carry_exc_count_re+=1;
              }
              else{
                $carry_exc_count_nre+=1;
              }
            }
          }
        }
      }

      ///carry_lead_sent
      $carry_accept_count=0;
      $carry_accept_count_re=0;
      $carry_accept_count_nre=0;
      for($i=0;$i<sizeof($accept_booking_id);$i++){
        if($accept_shop_id[$i]==$shop_id){
          if($carry_end_date=="0000-00-00"){
            if($accept_log[$i]>=$carry_start_date){
              $carry_accept_count+=1;
              if($brand[$i]=="Royal Enfield"){
                $carry_accept_count_re+=1;
              }
              else{
                $carry_accept_count_nre+=1;
              }              
            }  
          }
          else{
            if(($accept_log[$i]>=$carry_start_date)&&($accept_log[$i])<=$carry_end_date){
              $carry_accept_count+=1;
              if($brand[$i]=="Royal Enfield"){
                $carry_accept_count_re+=1;
              }
              else{
                $carry_accept_count_nre+=1;
              }
            }
          }
        }
      }
      //carry_swap_leads
      $carry_swap_count=0;
      $carry_swap_count_re=0;
      $carry_swap_count_nre=0;
      for($i=0;$i<sizeof($swap_booking_id);$i++){
        if($swap_shop_id[$i]==$shop_id){
          if($carry_end_date=="0000-00-00"){
            if($swap_log[$i]>=$carry_start_date){
              $carry_swap_count+=1;
              if($swap_brand[$i]=="Royal Enfield"){
                  $carry_swap_count_re+=1;
                }
              else{
                  $carry_swap_count_nre+=1;
              }
            }  
          }
          else{
            if($swap_log[$i]>=$carry_start_date&&$swap_log[$i]<=$carry_end_date){
              $carry_swap_count+=1;
              if($swap_brand[$i]=="Royal Enfield"){
                  $carry_swap_count_re+=1;
                }
              else{
                  $carry_swap_count_nre+=1;
              }
            }
          }
        }
      }

    if($prev_shop_id!=$shop_id){
        $prev_leads=0;
        $prev_leads_re=0;
        $prev_leads_nre=0;
      }
      // if($b2b_vehicle_type=="2w"){
      //     $carry_prom_leads=$carry_leads_re+$carry_leads_nre;
      // }
      $carry_leads=$carry_prom_leads-$carry_exc_count-$carry_accept_count-$carry_swap_count+$prev_leads;
      $carry_leads_re=$carry_re_prom_leads-$carry_exc_count_re-$carry_accept_count_re-$carry_swap_count_re+$prev_leads_re;
      $carry_leads_nre=$carry_nre_prom_leads-$carry_exc_count_nre-$carry_accept_count_nre-$carry_swap_count_nre+$prev_leads_nre;
      $leads=$prev_leads;
      $leads_re=$prev_leads_re;
      $leads_nre=$prev_leads_nre;
      $prev_leads=$carry_leads;
      $prev_leads_re=$carry_leads_re;
      $prev_leads_nre=$carry_leads_nre;
      $prev_shop_id=$shop_id;
    }
    else{
      $carry_leads=0;
      $carry_leads_re=0;
      $carry_leads_nre=0;
    }
    // if($b2b_vehicle_type=="2w"){
    //   $promised_leads=$promised_re_leads+$promised_nre_leads;
    // }
    $total_leads=$promised_leads+$carry_leads;
    $total_leads_re=$promised_re_leads+$carry_leads_re;
    $total_leads_nre=$promised_nre_leads+$carry_leads_nre;

    $consumed_leads=$accept_count+$exc_count+$swap_count;
    $consumed_leads_re=$accept_count_re+$exc_count_re+$swap_count_re;
    $consumed_leads_nre=$accept_count_nre+$exc_count_nre+$swap_count_nre;

    $remain_leads=$total_leads-$consumed_leads;
    $remain_leads_re=$total_leads_re-$consumed_leads_re;
    $remain_leads_nre=$total_leads_nre-$consumed_leads_nre;

    
?>
<?php if($status=="active") { if($end_date=="0000-00-00"){ ?>
  <?php if($veh_type!="2w"){ ?>
        <tr>
          <td><?php echo $no=$no+1 ; ?></td>
          <td title="<?php echo $shop_id; ?>"><?php echo $shop_name; ?></td>
          <td><?php echo date("d-m-Y", strtotime($start_date)); ?></td>
          <td><?php if($end_date!="0000-00-00") {echo date("d-m-Y", strtotime($end_date));;}else{echo "-";} ?></td>

          <td><?php 
            if($end_date!="0000-00-00"){
              if($status=="both"){
                echo "<p style='color:red;'>Inactive</p>";
              }
              else{
                echo "Inactive";
              }
            }else{
              if($status=="both"){
                echo "<p style='color:green;'>Active</p>";
              }
              else{
                echo "Active";
              }
            } 
          ?></td>

          <td><?php echo $promised_leads; ?></td> 
          <td <?php if($carry_leads<0){echo "style='background-color:#ff2929'";}?>><?php echo $carry_leads; 
           // if($carry_leads!=0){
           // echo"$carry_leads = $carry_prom_leads - $carry_accept_count - $carry_swap_count - $carry_exc_count - ($leads)";} else{echo 0;}?></td>
          <td <?php if($total_leads<0){echo "style='background-color:#ff2929'";}?>><?php echo $total_leads; ?></td>
          <td <?php if($consumed_leads<0){echo "style='background-color:#ff2929'";}?>><?php 
          echo $consumed_leads;
             // echo $consumed_leads."= $accept_count + $exc_count + $swap_count"; ?></td>
             <td><?php echo '<a href= "#" style="color:red;" class="ebm" data-target="#ebm" data-toggle="modal" data-startdate="'.$start_date.'" data-enddate="'.$end_date.'" data-shopid="'.$shop_id.'">'.$ebm_count.'</a>'; ?></td>
          <td><?php echo '<a href= "#" style="color:blue;" class="swap" data-target="#swap" data-toggle="modal" data-startdate="'.$start_date.'" data-enddate="'.$end_date.'" data-swapshopid="'.$shop_id.'" >'.$swap.'</a>'; ?></td>
          <td <?php if($remain_leads<0){echo "style='background-color:#ff2929'";}?>><?php echo $remain_leads; ?></td>
           
          <!-- <td><?php for($i=0;$i<sizeof($b2b_credits_shop_id);$i++){if($b2b_credits_shop_id[$i]==$shop_id){echo $b2b_credits_leads[$i];}} ?></td> -->
        </tr>
      <?php } else { ?>
        <tr>
          <td><?php echo $no=$no+1 ; ?></td>
          <td title="<?php echo $shop_id; ?>"><?php echo $shop_name; ?></td>
          <td><?php echo date("d-m-Y", strtotime($start_date)); ?></td>
          <td><?php if($end_date!="0000-00-00") {echo date("d-m-Y", strtotime($end_date));;}else{echo "-";} ?></td>
          <td><?php 
              if($end_date!="0000-00-00"){
              if($status=="both"){
                echo "<p style='color:red;'>Inactive</p>";
              }
              else{
                echo "Inactive";
              }
            }else{
              if($status=="both"){
                echo "<p style='color:green;'>Active</p>";
              }
              else{
                echo "Active";
              }
            }
           ?></td>
          <td <?php if($promised_re_leads<0){echo "style='background-color:#ff2929'";}?>><?php echo $promised_re_leads; ?></td> 
          <td <?php if($promised_nre_leads<0){echo "style='background-color:#ff2929'";}?>><?php echo $promised_nre_leads; ?></td>
          <td <?php if($carry_leads_re<0){echo "style='background-color:#ff2929'";}?>>
            <?php echo $carry_leads_re; //echo "$carry_leads_re = $carry_re_prom_leads - $carry_accept_count_re - $carry_swap_count_re - $carry_exc_count_re - $leads_re"; ?>
          </td>
          <td <?php if($carry_leads_nre<0){echo "style='background-color:#ff2929'";}?>>
            <?php echo $carry_leads_nre; //echo "$carry_leads_nre = $carry_nre_prom_leads - $carry_accept_count_nre -- $carry_swap_count_re - $carry_exc_count_nre - $leads_nre"; ?>
          </td>
          <td <?php if($total_leads_re<0){echo "style='background-color:#ff2929'";}?>><?php echo $total_leads_re; ?></td>
          <td <?php if($total_leads_nre<0){echo "style='background-color:#ff2929'";}?>><?php echo $total_leads_nre; ?></td>
          <td <?php if($consumed_leads_re<0){echo "style='background-color:#ff2929'";}?>><?php echo $consumed_leads_re; ?></td>
          <td <?php if($consumed_leads_nre<0){echo "style='background-color:#ff2929'";}?>><?php echo $consumed_leads_nre; ?></td>
          <td><?php echo '<a href= "#" style="color:red;" class="ebm" data-target="#ebm" data-toggle="modal" data-startdate="'.$start_date.'" data-enddate="'.$end_date.'" data-shopid="'.$shop_id.'">'.$ebm_count.'</a>'; ?></td>
          <td><?php echo '<a href= "#" style="color:blue;" class="swap" data-target="#swap" data-toggle="modal" data-startdate="'.$start_date.'" data-enddate="'.$end_date.'" data-swapshopid="'.$shop_id.'" >'.$swap.'</a>'; ?></td>
          <td <?php if($remain_leads_re<0){echo "style='background-color:#ff2929'";}?>><?php echo $remain_leads_re; ?></td>
          
          <td <?php if($remain_leads_nre<0){echo "style='background-color:#ff2929'";}?>><?php echo $remain_leads_nre; ?></td>
          </tr>
        <?php } ?> <?php } } //if status ?>

        <?php if($status=="inactive") { if($end_date!="0000-00-00"){ ?>
  <?php if($veh_type!="2w"){ ?>
        <tr>
          <td><?php echo $no=$no+1 ; ?></td>
          <td title="<?php echo $shop_id; ?>"><?php echo $shop_name; ?></td>
          <td><?php echo date("d-m-Y", strtotime($start_date)); ?></td>
          <td><?php if($end_date!="0000-00-00") {echo date("d-m-Y", strtotime($end_date));;}else{echo "-";} ?></td>

          <td><?php 
            if($end_date!="0000-00-00"){
              if($status=="both"){
                echo "<p style='color:red;'>Inactive</p>";
              }
              else{
                echo "Inactive";
              }
            }else{
              if($status=="both"){
                echo "<p style='color:green;'>Active</p>";
              }
              else{
                echo "Active";
              }
            } 
          ?></td>

          <td><?php echo $promised_leads; ?></td> 
          <td <?php if($carry_leads<0){echo "style='background-color:#ff2929'";}?>><?php echo $carry_leads; 
           // if($carry_leads!=0){
           // echo"$carry_leads = $carry_prom_leads - $carry_accept_count - $carry_swap_count - $carry_exc_count - ($leads)";} else{echo 0;}?></td>
          <td <?php if($total_leads<0){echo "style='background-color:#ff2929'";}?>><?php echo $total_leads; ?></td>
          <td <?php if($consumed_leads<0){echo "style='background-color:#ff2929'";}?>><?php 
          echo $consumed_leads;
             // echo $consumed_leads."= $accept_count + $exc_count + $swap_count"; ?></td>
             <td><?php echo '<a href= "#" style="color:red;" class="ebm" data-target="#ebm" data-toggle="modal" data-startdate="'.$start_date.'" data-enddate="'.$end_date.'" data-shopid="'.$shop_id.'">'.$ebm_count.'</a>'; ?></td>
          <td><?php echo '<a href= "#" style="color:blue;" class="swap" data-target="#swap" data-toggle="modal" data-startdate="'.$start_date.'" data-enddate="'.$end_date.'" data-swapshopid="'.$shop_id.'" >'.$swap.'</a>'; ?></td>
          <td <?php if($remain_leads<0){echo "style='background-color:#ff2929'";}?>><?php echo $remain_leads; ?></td>


          <!-- <td><?php for($i=0;$i<sizeof($b2b_credits_shop_id);$i++){if($b2b_credits_shop_id[$i]==$shop_id){echo $b2b_credits_leads[$i];}} ?></td> -->
        </tr>
      <?php } else { ?>
        <tr>
          <td><?php echo $no=$no+1 ; ?></td>
          <td title="<?php echo $shop_id; ?>"><?php echo $shop_name; ?></td>
          <td><?php echo date("d-m-Y", strtotime($start_date)); ?></td>
          <td><?php if($end_date!="0000-00-00") {echo date("d-m-Y", strtotime($end_date));;}else{echo "-";} ?></td>
          <td><?php 
              if($end_date!="0000-00-00"){
              if($status=="both"){
                echo "<p style='color:red;'>Inactive</p>";
              }
              else{
                echo "Inactive";
              }
            }else{
              if($status=="both"){
                echo "<p style='color:green;'>Active</p>";
              }
              else{
                echo "Active";
              }
            }
           ?></td>
          <td <?php if($promised_re_leads<0){echo "style='background-color:#ff2929'";}?>><?php echo $promised_re_leads; ?></td> 
          <td <?php if($promised_nre_leads<0){echo "style='background-color:#ff2929'";}?>><?php echo $promised_nre_leads; ?></td>
          <td <?php if($carry_leads_re<0){echo "style='background-color:#ff2929'";}?>>
            <?php echo $carry_leads_re; //echo "$carry_leads_re = $carry_re_prom_leads - $carry_accept_count_re - $carry_swap_count_re - $carry_exc_count_re - $leads_re"; ?>
          </td>
          <td <?php if($carry_leads_nre<0){echo "style='background-color:#ff2929'";}?>>
            <?php echo $carry_leads_nre; //echo "$carry_leads_nre = $carry_nre_prom_leads - $carry_accept_count_nre -- $carry_swap_count_re - $carry_exc_count_nre - $leads_nre"; ?>
          </td>
          <td <?php if($total_leads_re<0){echo "style='background-color:#ff2929'";}?>><?php echo $total_leads_re; ?></td>
          <td <?php if($total_leads_nre<0){echo "style='background-color:#ff2929'";}?>><?php echo $total_leads_nre; ?></td>
          <td <?php if($consumed_leads_re<0){echo "style='background-color:#ff2929'";}?>><?php echo $consumed_leads_re; ?></td>
          <td <?php if($consumed_leads_nre<0){echo "style='background-color:#ff2929'";}?>><?php echo $consumed_leads_nre; ?></td>
          <td><?php echo '<a href= "#" style="color:red;" class="ebm" data-target="#ebm" data-toggle="modal" data-startdate="'.$start_date.'" data-enddate="'.$end_date.'" data-shopid="'.$shop_id.'">'.$ebm_count.'</a>'; ?></td>
         <td><?php echo '<a href= "#" style="color:blue;" class="swap" data-target="#swap" data-toggle="modal" data-startdate="'.$start_date.'" data-enddate="'.$end_date.'" data-swapshopid="'.$shop_id.'" >'.$swap.'</a>'; ?></td>
          <td <?php if($remain_leads_re<0){echo "style='background-color:#ff2929'";}?>><?php echo $remain_leads_re; ?></td>
          
          <td <?php if($remain_leads_nre<0){echo "style='background-color:#ff2929'";}?>><?php echo $remain_leads_nre; ?></td>
          </tr>
        <?php } ?> <?php } } //if status ?>
        <?php if($status=="both") { if($end_date!=""){ ?>
  <?php if($veh_type!="2w"){ ?>
        <tr>
          <td><?php echo $no=$no+1 ; ?></td>
          <td title="<?php echo $shop_id; ?>"><?php echo $shop_name; ?></td>
          <td><?php echo date("d-m-Y", strtotime($start_date)); ?></td>
          <td><?php if($end_date!="0000-00-00") {echo date("d-m-Y", strtotime($end_date));;}else{echo "-";} ?></td>

          <td><?php 
            if($end_date!="0000-00-00"){
              if($status=="both"){
                echo "<p style='color:red;'>Inactive</p>";
              }
              else{
                echo "Inactive";
              }
            }else{
              if($status=="both"){
                echo "<p style='color:green;'>Active</p>";
              }
              else{
                echo "Active";
              }
            } 
          ?></td>

          <td><?php echo $promised_leads; ?></td> 
          <td <?php if($carry_leads<0){echo "style='background-color:#ff2929'";}?>><?php echo $carry_leads; 
           // if($carry_leads!=0){
           // echo"$carry_leads = $carry_prom_leads - $carry_accept_count - $carry_swap_count - $carry_exc_count - ($leads)";} else{echo 0;}?></td>
          <td <?php if($total_leads<0){echo "style='background-color:#ff2929'";}?>><?php echo $total_leads; ?></td>
          <td <?php if($consumed_leads<0){echo "style='background-color:#ff2929'";}?>><?php 
          echo $consumed_leads;
             // echo $consumed_leads."= $accept_count + $exc_count + $swap_count"; ?></td>
             <td><?php echo '<a href= "#" style="color:red;" class="ebm" data-target="#ebm" data-toggle="modal" data-startdate="'.$start_date.'" data-enddate="'.$end_date.'" data-shopid="'.$shop_id.'">'.$ebm_count.'</a>'; ?></td>
          <td><?php echo '<a href= "#" style="color:blue;" class="swap" data-target="#swap" data-toggle="modal" data-startdate="'.$start_date.'" data-enddate="'.$end_date.'" data-swapshopid="'.$shop_id.'" >'.$swap.'</a>'; ?></td>
          <td <?php if($remain_leads<0){echo "style='background-color:#ff2929'";}?>><?php echo $remain_leads; ?></td>

          <!-- <td><?php for($i=0;$i<sizeof($b2b_credits_shop_id);$i++){if($b2b_credits_shop_id[$i]==$shop_id){echo $b2b_credits_leads[$i];}} ?></td> -->
        </tr>
      <?php } else { ?>
        <tr>
          <td><?php echo $no=$no+1 ; ?></td>
          <td title="<?php echo $shop_id; ?>"><?php echo $shop_name; ?></td>
          <td><?php echo date("d-m-Y", strtotime($start_date)); ?></td>
          <td><?php if($end_date!="0000-00-00") {echo date("d-m-Y", strtotime($end_date));;}else{echo "-";} ?></td>
          <td><?php 
              if($end_date!="0000-00-00"){
              if($status=="both"){
                echo "<p style='color:red;'>Inactive</p>";
              }
              else{
                echo "Inactive";
              }
            }else{
              if($status=="both"){
                echo "<p style='color:green;'>Active</p>";
              }
              else{
                echo "Active";
              }
            }
           ?></td>
          <td <?php if($promised_re_leads<0){echo "style='background-color:#ff2929'";}?>><?php echo $promised_re_leads; ?></td> 
          <td <?php if($promised_nre_leads<0){echo "style='background-color:#ff2929'";}?>><?php echo $promised_nre_leads; ?></td>
          <td <?php if($carry_leads_re<0){echo "style='background-color:#ff2929'";}?>>
            <?php echo $carry_leads_re; //echo "$carry_leads_re = $carry_re_prom_leads - $carry_accept_count_re - $carry_swap_count_re - $carry_exc_count_re - $leads_re"; ?>
          </td>
          <td <?php if($carry_leads_nre<0){echo "style='background-color:#ff2929'";}?>>
            <?php echo $carry_leads_nre; //echo "$carry_leads_nre = $carry_nre_prom_leads - $carry_accept_count_nre -- $carry_swap_count_re - $carry_exc_count_nre - $leads_nre"; ?>
          </td>
          <td <?php if($total_leads_re<0){echo "style='background-color:#ff2929'";}?>><?php echo $total_leads_re; ?></td>
          <td <?php if($total_leads_nre<0){echo "style='background-color:#ff2929'";}?>><?php echo $total_leads_nre; ?></td>
          <td <?php if($consumed_leads_re<0){echo "style='background-color:#ff2929'";}?>><?php echo $consumed_leads_re; ?></td>
          <td <?php if($consumed_leads_nre<0){echo "style='background-color:#ff2929'";}?>><?php echo $consumed_leads_nre; ?></td>
           <td><?php echo '<a href= "#" style="color:red;" class="ebm" data-target="#ebm" data-toggle="modal" data-startdate="'.$start_date.'" data-enddate="'.$end_date.'" data-shopid="'.$shop_id.'">'.$ebm_count.'</a>'; ?></td>
          <td><?php echo '<a href= "#" style="color:blue;" class="swap" data-target="#swap" data-toggle="modal" data-startdate="'.$start_date.'" data-enddate="'.$end_date.'" data-swapshopid="'.$shop_id.'" >'.$swap.'</a>'; ?></td>
          <td <?php if($remain_leads_re<0){echo "style='background-color:#ff2929'";}?>><?php echo $remain_leads_re; ?></td>
         
          <td <?php if($remain_leads_nre<0){echo "style='background-color:#ff2929'";}?>><?php echo $remain_leads_nre; ?></td>
          
          </tr>
        <?php } ?> <?php } } //if status ?>


        <!-- ////////////// -->
        <?php }
        } else {
           echo "<h3 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h3>;";
        }
         ?> 

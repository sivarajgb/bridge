<?php
include("../config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();
$today=date('Y-m-d');

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

 $startdate = date('Y-m-d',strtotime($_POST['startdate']));
 $enddate =  date('Y-m-d',strtotime($_POST['enddate']));
 $vehicle = $_POST['vehicle_type'];
 $city = $_POST['city'];

 $_SESSION['crm_city'] = $city;


//0-all && 1-particular
class bookings {
  private $vehicle;
  private $city;

  function __construct($vehicle,$city){
    $this->vehicle = $vehicle;
    $this->city = $city;
  }
  function v0_c0(){
    return "";
  } 
  function v0_c1(){
    return "AND g.city='$this->city'";
  } 
  function v1_c0(){
    return "AND g.vehicle_type='$this->vehicle'";
  }
  function v1_c1(){
    return "AND g.vehicle_type='$this->vehicle' AND g.city='$this->city'";
  }
}

$bookings_obj = new bookings($vehicle,$city);

$vehicle_val = $vehicle=='all' ? "0" : "1";
$city_val = $city=='all' ? "0" : "1";
$cond = $bookings_obj->{"v{$vehicle_val}_c{$city_val}"}();

$sql_booking = "SELECT b.b2b_booking_id,b.gb_booking_id,b.b2b_shop_id,b.b2b_customer_name,b.b2b_cust_phone,b.brand,b.model,b.b2b_service_type,b.b2b_service_date,b.b2b_log,g.shop_name,g.vehicle_type,g.locality,g.service_status,g.final_bill_amt,g.rating,g.feedback,g.get_rltime_updte,g.followup_date FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id WHERE b.b2b_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND g.service_status='Completed' AND b.b2b_shop_id NOT IN (1014,1035,1670) {$cond} ORDER BY b.b2b_log ASC";
$res_booking = mysqli_query($conn2,$sql_booking);

$no = 0;

$count = mysqli_num_rows($res_booking);
if($count >=1){
?>
<div align="center" id = "table" style="max-width:95%; margin-top:110px;margin-left:10px;margin-right:10px;">
<table id="example" class="table table-striped table-bordered tablesorter table-hover results" style="max-width:95%;">
 <thead style="background-color: #D3D3D3;">
  <th>No</th>
  <th>TimeStamp</th>
  <th>BookingId</th>
  <th>CustomerName</th>
  <th>Mobile</th>
  <th>ShopName</th>
  <th>Vehicle</th>
  <th>ServiceType</th>
  <th>Amount</th>
  <th>Rating</th>
  <th>Feedback</th>
  <th>RTT</th>
  <th>Locality</th>
  <th>ServiceDate</th>
  </thead>
  <tbody id="tbody">

<?php

while($row_booking = mysqli_fetch_object($res_booking)){
$b2b_booking_id = $row_booking->b2b_booking_id;
$booking_id = $row_booking->gb_booking_id;
$shop_id = $row_booking->b2b_shop_id;
$user_name = $row_booking->b2b_customer_name;
$user_mobile = $row_booking->b2b_cust_phone;
$brand = $row_booking->brand;
$model = $row_booking->model;
$service_type = $row_booking->b2b_service_type;
$service_date = $row_booking->b2b_service_date;
$log = $row_booking->b2b_log;
$shop_name=$row_booking->shop_name;
$vehicle_type = $row_booking->vehicle_type;
$locality = $row_booking->locality;
$service_status = $row_booking->service_status;
$final_bill = $row_booking->final_bill_amt;
$rating = $row_booking->rating;
$feedback = $row_booking->feedback;
$realtime_updates = $row_booking->get_rltime_updte;
$next_service_date = $row_booking->followup_date;

?>

  <tr>
    <td><?php echo $no=$no+1 ; ?></td>
    <td><?php echo date('d M Y h.i A', strtotime($log)); ?></td>
    <td><p style="float:left;padding:10px;"><?php echo $booking_id; ?></p></td>
    <td><?php echo $user_name; ?></td>
    <td><?php echo $user_mobile; ?></td>
    <td><?php echo $shop_name; ?></td>
    <td><?php echo $brand." ".$model; ?></td>
    <td><?php echo $service_type; ?></td>
    <td><?php if($final_bill != '' || $final_bill != '0'){  echo $final_bill; } ?></td>
    <td><?php if($rating != '0'){  echo $rating; } ?></td>
    <td><?php if($feedback != ''){  echo $feedback; } ?></td>
    <td><?php if($realtime_updates == 1){ echo "Yes"; } else{ echo "No"; } ?></td>
    <td><?php echo $locality; ?></td>
    <td><?php if($service_date == "0000-00-00"){ echo "Not Updated Yet"; } else { echo date('d M Y', strtotime($service_date)); } ?></td>
  </tr>
  <?php
}
?>
<script>
var date = new Date();
date.setDate(date.getDate());

$('.datepicker').datepicker({
   autoclose: true,
    startDate: date
});
$('input.datepicker').datepicker('setDate', 'today');
</script>

  </tbody>
  </table>
</div>
</div>
<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example").tablesorter( {sortList: [[0,0], [1,0]]} );
    }
);
</script>
<?php
} // if
else {
  //echo $sql_booking;
  ?>
  <div align="center" style="margin-top:140px;">
  <h2>No Results Found !!! </h2>
  </div>
  <?php
}
 ?>

<?php
include("../config.php");
//error_reporting(E_ALL); 
//ini_set('display_errors', 1);

$conn = db_connect1();
session_start();

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$master_service = $_POST['master_service'];
$service_type = $_POST['service_type'];
$vehicle = $_POST['vehicle'];
$booking_status = $_POST['status'];
$city = $_POST['city'];

$_SESSION['crm_city'] = $city;

$services = '';

class overall {
    private $vehicle;
    private $master_service;
    private $service_type;
    private $booking_status;
    private $services;
    private $city;

    function __construct( $vehicle,$master_service,$service_type,$booking_status,$city){
        $this->vehicle = $vehicle;
        $this->master_service = $master_service;
        $this->service_type = $service_type;
        $this->status = $booking_status;
        $this->city = $city;
        //$this->services = $services;

        if($this->master_service != "" && $this->service_type == ""){
            $conn = db_connect1();
            $sql_serv = "SELECT service_type FROM go_axle_service_price_tbl WHERE bridge_flag='0' AND master_service='$this->master_service'";
            $res_serv = mysqli_query($conn,$sql_serv);
            $this->services = '';
            while($row_serv = mysqli_fetch_array($res_serv)){
                if($this->services == ""){
                    $this->services = $row_serv['service_type']."'";
                }
                else{
                    $this->services = $this->services.",'".$row_serv['service_type']."'";
                }
            }
        }
    }

    function v0_s0_st0_c0() {
      $master = $this->master_service;
      if($master == ""){
          return "";
      }
      else{
          return "AND service_type IN('$this->services)";
      }
    }
    function v0_s0_st0_c1() {
        $master = $this->master_service;
        if($master == ""){
            return "AND city='$this->city'";
        }
        else{
            return "AND service_type IN('$this->services) AND city='$this->city'";
        }
      }
    function v0_s0_st1_c0() {
      $ss = $this->status;
      $master = $this->master_service;
      if($master == ""){
        if($ss == 'c'){
            return "AND flag='1'";
          }
          else{
            return "AND booking_status='$ss' AND flag!='1'";
          }
      }
      else{
        if($ss == 'c'){
            return "AND flag='1' AND service_type IN('$this->services')";
          }
          else{
            return "AND booking_status='$ss' AND flag!='1' AND service_type IN('$this->services')";
          }
      }
      
    }
    function v0_s0_st1_c1() {
        $ss = $this->status;
        $master = $this->master_service;
        if($master == ""){
          if($ss == 'c'){
              return "AND flag='1'  AND city='$this->city'";
            }
            else{
              return "AND booking_status='$ss' AND flag!='1'  AND city='$this->city'";
            }
        }
        else{
          if($ss == 'c'){
              return "AND flag='1' AND service_type IN('$this->services')  AND city='$this->city'";
            }
            else{
              return "AND booking_status='$ss' AND flag!='1' AND service_type IN('$this->services')  AND city='$this->city'";
            }
        }
        
      }
    function v0_s1_st0_c0() {
        return "AND service_type='$this->service_type' ";
    }
    function v0_s1_st0_c1() {
        return "AND service_type='$this->service_type' AND city='$this->city'";
    }
    function v0_s1_st1_c0() {
        $ss = $this->status;
        if($ss == 'c'){
          return "AND service_type='$this->service_type' AND flag='1'";
        }
        else{
          return "AND service_type='$this->service_type' AND booking_status='$ss' AND flag!='1' ";
        }    
    }
    function v0_s1_st1_c1() {
        $ss = $this->status;
        if($ss == 'c'){
          return "AND service_type='$this->service_type' AND flag='1' AND city='$this->city'";
        }
        else{
          return "AND service_type='$this->service_type' AND booking_status='$ss' AND flag!='1' AND city='$this->city'";
        }    
    }
    function v1_s0_st0_c0() {
        $master = $this->master_service;
        if($master == ""){
            return "AND vehicle_type='$this->vehicle' ";
        }
        else{
            return "AND vehicle_type='$this->vehicle' AND service_type IN('$this->services')";
        }
    }
    function v1_s0_st0_c1() {
        $master = $this->master_service;
        if($master == ""){
            return "AND vehicle_type='$this->vehicle' AND city='$this->city'";
        }
        else{
            return "AND vehicle_type='$this->vehicle' AND service_type IN('$this->services') AND city='$this->city'";
        }
    }
    function v1_s0_st1_c0() {
        $ss = $this->status;
        $master = $this->master_service;
        if($master == ""){
            if($ss == 'c'){
                return "AND vehicle_type='$this->vehicle' AND flag='1'";
              }
              else{
                return "AND vehicle_type='$this->vehicle' AND booking_status='$ss' AND flag!='1'";
              } 
        }
        else{
            if($ss == 'c'){
                return "AND vehicle_type='$this->vehicle' AND flag='1' AND service_type IN('$this->services')";
              }
              else{
                return "AND vehicle_type='$this->vehicle' AND booking_status='$ss' AND flag!='1' AND service_type IN('$this->services')";
              } 
        }
    }
    function v1_s0_st1_c1() {
        $ss = $this->status;
        $master = $this->master_service;
        if($master == ""){
            if($ss == 'c'){
                return "AND vehicle_type='$this->vehicle' AND flag='1' AND city='$this->city'";
              }
              else{
                return "AND vehicle_type='$this->vehicle' AND booking_status='$ss' AND flag!='1' AND city='$this->city'";
              } 
        }
        else{
            if($ss == 'c'){
                return "AND vehicle_type='$this->vehicle' AND flag='1' AND service_type IN('$this->services') AND city='$this->city'";
              }
              else{
                return "AND vehicle_type='$this->vehicle' AND booking_status='$ss' AND flag!='1' AND service_type IN('$this->services') AND city='$this->city'";
              } 
        }
    }
    function v1_s1_st0_c0() {
        return "AND vehicle_type='$this->vehicle' AND service_type='$this->service_type'";
    }
    function v1_s1_st0_c1() {
        return "AND vehicle_type='$this->vehicle' AND service_type='$this->service_type' AND city='$this->city'";
    }
    function v1_s1_st1_c0() {
        $ss = $this->status;
        if($ss == 'c'){
          return "AND vehicle_type='$this->vehicle' AND service_type='$this->service_type' AND flag='1'";
        }
        else{
          return "AND vehicle_type='$this->vehicle' AND service_type='$this->service_type' AND booking_status='$ss' AND flag!='1'";
        }    
    }
    function v1_s1_st1_c1() {
        $ss = $this->status;
        if($ss == 'c'){
          return "AND vehicle_type='$this->vehicle' AND service_type='$this->service_type' AND flag='1' AND city='$this->city'";
        }
        else{
          return "AND vehicle_type='$this->vehicle' AND service_type='$this->service_type' AND booking_status='$ss' AND flag!='1' AND city='$this->city'";
        }    
    }
}

$vehicle_fun = $vehicle=='all' ? "" : $vehicle;
$master_service_fun = $master_service=='all' ? "" : $master_service;
$service_type_fun = $service_type=='all' ? "" : $service_type;
$booking_status_fun = $booking_status=='all'  ? "" : $booking_status;
$city_fun = $city=='all'  ? "" : $city;

$overall_obj = new overall( $vehicle_fun, $master_service_fun, $service_type_fun, $booking_status_fun, $city_fun);


$vehicle_val = $vehicle=='all' ? "0" : "1";
$master_service_val = $master_service=='all' ? "0" : "1";
$service_type_val = $service_type=='all' ? "0" : "1";
$booking_status_val = $booking_status=='all' ? "0" : "1";
$city_val = $city=='all' ? "0" : "1";

$cond = $overall_obj->{"v{$vehicle_val}_s{$service_type_val}_st{$booking_status_val}_c{$city_val}"}();

$sql_booking = "SELECT booking_id,user_id,mec_id,locality,flag_unwntd,crm_update_time FROM user_booking_tb WHERE booking_id!='' {$cond} AND DATE(log) BETWEEN '$startdate' AND '$enddate' ";

$res_booking = mysqli_query($conn,$sql_booking);

if($res_booking){
    
$no = 0;

$count = mysqli_num_rows($res_booking);
if($count >=1){

while($row_booking = mysqli_fetch_object($res_booking)){
$booking_id = $row_booking->booking_id;
$user_id = $row_booking->user_id;
$locality = $row_booking->locality;

if($locality == ""){
    $sql_user_loc = "SELECT Locality_Home FROM user_register WHERE reg_id='$user_id'";
    $res_user_loc = mysqli_query($conn,$sql_user_loc);
    $row_user_loc = mysqli_fetch_array($res_user_loc);
    $locality_u = $row_user_loc['Locality_Home'];
    if($locality_u == ''){
        $locality_u = "Central";
    }
    else{
        $locality = $locality_u;
    }  
}

if($locality == "chennai" || $locality == "Chennai"){
    $locality="Central";
}

$sql_latlng = "SELECT lat,lng FROM localities WHERE localities='$locality'";
$res_latlng = mysqli_query($conn,$sql_latlng);
$row_latlng = mysqli_fetch_array($res_latlng);
$lat = $row_latlng['lat'];
$lng = $row_latlng['lng'];
$localities[] = array("latt"=>$lat,"lngg"=>$lng);

}
// var_dump($localities);
?>
<?php
//foreach in array create a string like below
$data='';
    foreach ($localities as $row) {
         $lat= floatval(($row['latt']));
         $lng= floatval(($row['lngg']));
        
        if($data == ""){
            $data = '{lat: '.$lat.',lng: '.$lng.'}';
        }
        else{
            $data = $data.',{lat: '.$lat.',lng: '.$lng.'}'; 
        }
    }
//print_r($data);
if($city == "all"){
    $prepAddr = "chennai"; 
}
else{
    $prepAddr = $city;    
}
$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?key=AIzaSyBEFvPWpawLCHyQQQR_4Qx4kKeyojpDw7I&address='.$prepAddr.'&sensor=false');
$output= json_decode($geocode);
$latitude = $output->results[0]->geometry->location->lat;
$longitude = $output->results[0]->geometry->location->lng;

?>
<script>
$(document).ready(function(){
    initMap();
});
</script>
<script>
      function initMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: {lat: <?php echo $latitude ?>, lng: <?php echo $longitude ?>}
        });

        // Create an array of alphabetical characters used to label the markers.
        var labels = '1';

        // Add some markers to the map.
        // Note: The code uses the JavaScript Array.prototype.map() method to
        // create an array of markers based on a given "locations" array.
        // The map() method here has nothing to do with the Google Maps API.
        var markers = locations.map(function(location, i) {
          return new google.maps.Marker({
            position: location,
            label: labels[i % labels.length]
          });
        });

        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
      }
      var locations= <?php echo "[".$data."]"; ?>;

</script>

 <div id="map-container" style="width: 800px; height: 500px;"><div id="map" style="width: 785px; height: 485px;"></div>
</div>
<?php
} // if

else {
  //echo $sql_booking;
  echo "<div style='margin-top:300px;'><h3> No Results Found!!!</h3></div>";
}
}
else{
    echo "<div style='margin-top:300px;'><h3> Please change the filters and try!!!</h3></div>";
}
 ?>



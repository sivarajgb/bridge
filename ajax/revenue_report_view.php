<?php
include("../config.php");
//error_reporting(E_ALL); ini_set('display_errors', 1);
$conn = db_connect1();
session_start();

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$today = date('Y-m-d');
$city = $_GET['city'];
$_SESSION['crm_city'] = $city;

$cond ='';
$cond1 ='';

$cond = $cond.($city == 'all' ? "" : "AND b.city ='$city'");
$cond1 = $cond1.($city == 'all' ? "" : "AND m.address5 ='$city'");

$start = strtotime($startdate);
$end = strtotime($enddate);
$noofdays = date('t',($start));
$str = '';

$dateData2 = [];
for ( $i = $start; $i <= $end; $i = $i + 86400 ) {
    $thisDate = date('d M Y', $i);
    $day = date('l', strtotime($thisDate));

    $date = date('Y-m-d', $i);
	$dateData2[$date]['revenue'] = 0;
	$datesArr[] = $date;
}
$selectedDateRangeCount = count($datesArr);
$dates = join("','",$datesArr);
$sql_goaxle = "SELECT DISTINCT bb.b2b_booking_id,bb.gb_booking_id as booking_id,bb.b2b_log,s.price,b.service_type,b.vehicle_type FROM b2b.b2b_booking_tbl bb LEFT JOIN user_booking_tb b ON bb.gb_booking_id = b.booking_id LEFT JOIN go_axle_service_price_tbl s ON s.service_type = b.service_type AND s.type=b.vehicle_type WHERE (DATE(bb.b2b_log) IN ('$dates')) AND bb.b2b_shop_id NOT IN (1014,1035,1670,1673) AND  b.user_id NOT IN (21816,41317,859,3132,20666,56511,2792,128,19,7176,19470,1,951,103699,113453,108783) AND b.service_type NOT IN('GoBumpr Tyre Fest') {$cond} AND bb.b2b_swap_flag!=1 and bb.b2b_flag!=1;";
$res_goaxle = mysqli_query($conn,$sql_goaxle);
$dateData1 = [];
while($row_goaxle = mysqli_fetch_object($res_goaxle))
{
	$log = date('Y-m-d', strtotime($row_goaxle->b2b_log));
	$veh_type = $row_goaxle->vehicle_type;
	$dateData1[$log]['veh_type'][] = $veh_type;
	$price = $row_goaxle->price;
	$dateData1[$log]['price'][] = $price;
	// $log = $row_goaxle->log;
}
// print_r($dateData1);

$sql_revenue = "SELECT g.model as business_model,g.lead_price,g.amount,g.premium_tenure,g.start_date,g.end_date,bb.* FROM b2b.b2b_booking_tbl bb LEFT JOIN garage_model_history g ON g.b2b_shop_id = bb.b2b_shop_id AND date(bb.b2b_log) >= date(g.start_date) AND date(bb.b2b_log) <= date(g.end_date) LEFT JOIN b2b.b2b_status s ON s.b2b_booking_id = bb.b2b_booking_id LEFT JOIN user_booking_tb b ON b.booking_id = bb.gb_booking_id WHERE DATE(bb.b2b_log) between '$startdate' and  '$enddate' AND bb.b2b_flag != 1 AND bb.b2b_swap_flag !=1 AND s.b2b_acpt_flag = 1 {$cond};";
$res_revenue = mysqli_query($conn,$sql_revenue);
$revenue = 0;
while($row_revenue = mysqli_fetch_object($res_revenue))
{
	$log = date('Y-m-d', strtotime($row_revenue->b2b_log));
	$business_model = $row_revenue->business_model;
	$revenue = 0;
	switch($business_model)
	{
		case "PreCredit" : 	$credits = $row_revenue->b2b_credit_amt;
							$revenue = $credits;
							break;
		case "Premium 1.0":	$amount = $row_revenue->amount;
							$premium_tenure = $row_revenue->premium_tenure;
							// premium calculation is done in later part of code. can ignore this.
							break;
		case "Premium 2.0" :$lead_price = $row_revenue->lead_price;
							$revenue = $lead_price;
							break;
		case "Leads 3.0" :	$lead_price = $row_revenue->lead_price;
							$revenue = $lead_price;
							break;
		// default :			$credits = $row_revenue->b2b_credit_amt;
		// 					$revenue = $credits;
		// 					break;
	}
	$dateData2[$log]['revenue'] = $dateData2[$log]['revenue'] + $revenue;
}
// print_r($dateData2);

$sql_premium_revenue = "SELECT g.* FROM garage_model_history g LEFT JOIN admin_mechanic_table m ON m.axle_id = g.b2b_shop_id WHERE g.start_date >= '$startdate' and g.end_date <= '$enddate' {$cond1} HAVING g.model = 'Premium 1.0'";
$res_premium_revenue = mysqli_query($conn,$sql_premium_revenue);
while($row_premium_revenue = mysqli_fetch_object($res_premium_revenue))
{
	$shop_id = $row_premium_revenue->b2b_shop_id;
	$amount = $row_premium_revenue->amount;
	$premium_tenure = $row_premium_revenue->premium_tenure;
	$start_date = $row_premium_revenue->start_date;
	$end_date = $row_premium_revenue->end_date;
	$premium_shops[$shop_id]['shop_id'] = $shop_id;
	$premium_shops[$shop_id]['amount'] = $amount;
	$premium_shops[$shop_id]['premium_tenure'] = $premium_tenure;
	$premium_shops[$shop_id]['start_date'] = $start_date;
	$premium_shops[$shop_id]['end_date'] = $end_date;
}
// print_r($premium_shops);
foreach($premium_shops as $shops)
{
	// print_r($shops);
	$amount = $shops['amount'];
	$premium_tenure = $shops['premium_tenure'];
	$monthly = $amount / $premium_tenure;
	$per_day = $monthly / 25; //working days.
	
	$premium_start_date = $shops['start_date'];
	$premium_end_date = $shops['end_date'];
	$start = strtotime($premium_start_date);
	$end = strtotime($premium_end_date);
	for ( $i = $start; $i <= $end; $i = $i + 86400 ) {
		$thisDate = date('d M Y', $i);
		$day = date('l', strtotime($thisDate));
		if($day != 'Sunday')
		{
			$date = date('Y-m-d', $i);
			$dateData2[$date]['revenue'] = $dateData2[$date]['revenue'] + $per_day;
		}
	}

}

for ( $i = $start; $i <= $end; $i = $i + 86400 ) {
	$car_bookings =0 ;$car_goaxles =0 ;$car_total_price =0 ;$car_goaxles_price =0 ;$car_final_bill_amt=0;$car_billed=0;$car_unbilled=0;$car_unbilled_gmv=0;
	$bike_bookings =0 ;$bike_goaxles =0 ;$bike_total_price =0 ;$bike_goaxles_price =0 ;$bike_final_bill_amt=0;$bike_billed=0;$bike_unbilled=0;$bike_unbilled_gmv=0;
	
	$thisDate = date('d M Y', $i);
    $day = date('l', strtotime($thisDate));

    $date = date('Y-m-d', $i);
	$enc_date=base64_encode($date);
	if(count($dateData1[$date]['veh_type'])>0)
	{
		for($k = 0;$k<count($dateData1[$date]['veh_type']);$k++)
		{
			switch($dateData1[$date]['veh_type'][$k]){
				case '2w': 	$bike_goaxles = $bike_goaxles+1;
							$bike_goaxles_price = $bike_goaxles_price + $dateData1[$date]['price'][$k]; 
							break;
				case '4w':	$car_goaxles = $car_goaxles+1;
							$car_goaxles_price = $car_goaxles_price + $dateData1[$date]['price'][$k];
							break;
			}//switch
		}		
	}
	
    $total_goaxles = $bike_goaxles+$car_goaxles;
    $total_goaxles_price = $bike_goaxles_price+$car_goaxles_price;
		
	$tr = '<tr>';
    $td1 = '<td style="text-align:center;">'.$thisDate.'</td>';
    $td2 = '<td style="text-align:center;">'.$day.'</td>';
	$td3 = '<td style="text-align:center;"><a href="revenue_report_details.php?dt='.$enc_date.'&vt='.base64_encode(all).'" >'.$total_goaxles.'</a></td>';
	$td4 = '<td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" title="Total GoAxle GMV"></i> '.$total_goaxles_price.'</td>';
	$td5 = '<td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" title="Total Revenue"></i> '.$dateData2[$date]['revenue'].'</td>';
	$tr_l = '</tr>';

    $str = '<tr>'.$tr.$td1.$td2.$td3.$td4.$td5.$tr_l.'</tr>';
    $data1[] = array('row'=>$str);
}

$rtn_data['data1'] = $data1;
echo $rtn_data1 = json_encode($rtn_data);
?>
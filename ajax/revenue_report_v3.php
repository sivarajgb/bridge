<?php
include("../config.php");
//error_reporting(E_ALL); ini_set('display_errors', 1);

$conn = db_connect1();
session_start();

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$today = date('Y-m-d');
$city = $_GET['city'];
$_SESSION['crm_city'] = $city;

$cond ='';
$cond1 ='';

$cond = $cond.($city == 'all' ? "" : "AND b.city ='$city'");
$cond1 = $cond1.($city == 'all' ? "" : "AND m.address5 ='$city'");

$start = strtotime($startdate);
$end = strtotime($enddate);
$noofdays = date('t',($start));
$str = '';

$dateData = [];
for ( $i = $start; $i <= $end; $i = $i + 86400 ) {
    $date = date('Y-m-d', $i);
    $day=date('l', strtotime($date));
	$dateData[$date]['revenue'] = 0;
	$dateData[$date]['day'] = $day;
	$dateData[$date]['GMV'] = 0;
	$dateData[$date]['goaxle'] = 0;
	$dateData[$date]['precredit']['2w']=$dateData[$date]['precredit']['4w']=$dateData[$date]['premium']['2w']=$dateData[$date]['premium']['4w']=$dateData[$date]['leads']['2w']=$dateData[$date]['leads']['4w']=$dateData[$date]['ebm']['premium']=$dateData[$date]['ebm']['leads']=$dateData[$date]['ebm']['precredit']=0;

	$datesArr[] = $date;
}
$selectedDateRangeCount = count($datesArr);
$dates = join("','",$datesArr);

$sql_goaxle="SELECT
    DISTINCT bb.b2b_booking_id,
    bb.gb_booking_id AS booking_id,
    bb.b2b_shop_id,
    bb.b2b_log,
    a.price,
    b.service_type,
    b.vehicle_type,
    bb.brand,
	g.model AS business_model,
    g.lead_price,
    g.re_lead_price,
    g.nre_lead_price,
    g.amount,
    g.premium_tenure,
    g.start_date,
    g.end_date,
    bb.b2b_credit_amt,
    bb.b2b_swap_flag

FROM
    b2b.b2b_booking_tbl bb
        LEFT JOIN
    user_booking_tb b ON bb.gb_booking_id = b.booking_id
        LEFT JOIN
    go_axle_service_price_tbl a ON a.service_type = b.service_type
        AND a.type = b.vehicle_type AND a.amt=bb.b2b_credit_amt
        AND a.price=(SELECT MAX(price) FROM go_bumpr.go_axle_service_price_tbl WHERE service_type=b.service_type AND type=b.vehicle_type AND amt = bb.b2b_credit_amt)
        LEFT JOIN
	garage_model_history g ON g.b2b_shop_id = bb.b2b_shop_id
        AND DATE(bb.b2b_log) >= DATE(g.start_date)
        AND g.id=(select id from go_bumpr.garage_model_history where b2b_shop_id=bb.b2b_shop_id AND DATE(bb.b2b_log) >= DATE(start_date) order by start_date desc limit 1) 
        LEFT JOIN
	b2b.b2b_status s ON s.b2b_booking_id = bb.b2b_booking_id

WHERE
    (DATE(bb.b2b_log) IN ('$dates'))
        AND bb.b2b_shop_id NOT IN (1014 , 1035, 1670, 1673)
        AND b.user_id NOT IN (21816 , 41317,
        859,
        3132,
        20666,
        56511,
        2792,
        128,
        19,
        7176,
        19470,
        1,
        951,
        103699,
        113453,
        108783)
        AND b.service_type NOT IN ('GoBumpr Tyre Fest')
        {$cond}
        
        
        AND s.b2b_acpt_flag = 1";
$res_goaxle = mysqli_query($conn,$sql_goaxle);
// echo $sql_goaxle;die; 
while($row_goaxle = mysqli_fetch_object($res_goaxle))
{
	$log = date('Y-m-d', strtotime($row_goaxle->b2b_log));
	// echo $log;
	
	$model_flag=0;
	$model_start=$row_goaxle->start_date;
	$model_end=$row_goaxle->end_date;
	$veh_type=$row_goaxle->vehicle_type;
	$brand=$row_goaxle->brand;
	$price=$row_goaxle->price;
	$business_model = trim($row_goaxle->business_model);
	$revenue=0;
	$credit_amt=$row_goaxle->b2b_credit_amt;
	$lead_price = $row_goaxle->lead_price;
	$re_lead_price = $row_goaxle->re_lead_price;
	$nre_lead_price = $row_goaxle->nre_lead_price;
	$swap_flag=$row_goaxle->b2b_swap_flag;
	// $exception_log=$row_goaxle->exception_log;
	// echo $exception_log;

	if($business_model=="Pre Credit"){
	 	$credits = $row_goaxle->b2b_credit_amt;
		$revenue = $credits;
		$model_flag=0;
	}else if($business_model=="Premium 1.0"){
		$amount = $row_goaxle->amount;
		$premium_tenure = $row_goaxle->premium_tenure;
		$model_flag=1;
		$revenue=round(($amount/$premium_tenure)/(25));
	}else if($business_model=="Premium 2.0"){
		if($veh_type=="4w"){
			$revenue = $lead_price;
		}else{
			if($brand=="Royal Enfield"){
				$revenue=$lead_price;
			}else{
				$revenue=$nre_lead_price;
			}
		}
		$model_flag=2;
	}else if($business_model=="Leads 3.0"){
					
		if($veh_type=="4w"){
			$revenue = $lead_price;
		}else{
			if($brand=="Royal Enfield"){
				$revenue=($re_lead_price!=0)?$re_lead_price:$lead_price;
			}else{
				$revenue=($nre_lead_price!=0)?$nre_lead_price:$lead_price;
			}
		}
		$model_flag=3;
	}else{
		$credits = $row_goaxle->b2b_credit_amt;
		$revenue = $credits;
		$model_flag=0;
	}
							
	if($swap_flag!=1){
	// echo array_key_exists($log,$dateData);die;
	// echo $revenue;echo "\n";
		switch($model_flag){
			case 0 : $dateData[$log]['precredit'][$veh_type]+=$revenue;break;
			case 1 : $dateData[$log]['premium'][$veh_type]+=$revenue;break;
			case 2 : $dateData[$log]['premium'][$veh_type]+=$revenue;break;
			case 3 : $dateData[$log]['leads'][$veh_type]+=$revenue;break;

		}
		$dateData[$log]['revenue']+=round($revenue,2);

		$dateData[$log]['GMV']+=$price;
		$dateData[$log]['goaxle']+=1;
	// var_dump($dateData);die;
	}else{
		switch($model_flag){
			case 0 : $swapData[$log]['precredit'][$veh_type]+=$revenue;break;
			case 1 : $swapData[$log]['premium'][$veh_type]+=$revenue;break;
			case 2 : $swapData[$log]['premium'][$veh_type]+=$revenue;break;
			case 3 : $swapData[$log]['leads'][$veh_type]+=$revenue;break;

		}
		$swapData[$log]['revenue']+=round($revenue,2);

		$swapData[$log]['GMV']+=$price;
		$swapData[$log]['goaxle']+=1;

	}
}
$ebm_query="SELECT 
	DISTINCT e.booking_id,
	e.b2b_shop_id,
    b.service_type,
    b.vehicle_type,
    bb.brand,
	g.model AS business_model,
    g.lead_price,
    g.re_lead_price,
    g.nre_lead_price,
    g.amount,
    g.premium_tenure,
    g.start_date,
    g.end_date,
    bb.b2b_credit_amt,
    e.exception_log
FROM
    exception_mechanism_track e 
    LEFT JOIN
    b2b.b2b_booking_tbl bb ON bb.gb_booking_id=e.booking_id AND bb.b2b_shop_id=e.b2b_shop_id
    LEFT JOIN
    user_booking_tb b ON e.booking_id = b.booking_id
    LEFT JOIN 
    garage_model_history g ON e.b2b_shop_id = g.b2b_shop_id
        AND DATE(e.exception_log) >= DATE(g.start_date)
        AND g.id=(select id from go_bumpr.garage_model_history where b2b_shop_id=e.b2b_shop_id AND DATE(e.exception_log) >= DATE(start_date) order by start_date desc limit 1) 
WHERE
    (DATE(e.exception_log) IN ('$dates')) AND e.flag=0 {$cond}";
// echo $ebm_query;die;
$ebm_result=mysqli_query($conn,$ebm_query);
while($ebm_row=mysqli_fetch_object($ebm_result)){
	$log = date('Y-m-d', strtotime($ebm_row->exception_log));
	// var_dump($ebm_row);
	$model_flag=0;
	$model_start=$ebm_row->start_date;
	$model_end=$ebm_row->end_date;
	$veh_type=$ebm_row->vehicle_type;
	$brand=$ebm_row->brand;
	$re_lead_price = $row_goaxle->re_lead_price;
	$business_model = trim($ebm_row->business_model);
	$revenue=0;
	$credit_amt=$ebm_row->b2b_credit_amt;
	$lead_price = $ebm_row->lead_price;
	// $re_lead_price = $ebm_row->re_lead_price;
	$nre_lead_price = $ebm_row->nre_lead_price;
	$exception_log=$ebm_row->exception_log;
	switch($business_model)
	{
		case "Pre Credit" : 	$credits = $ebm_row->b2b_credit_amt;
							$revenue = $credits;
							$model_flag=0;
							break;
		case "Premium 1.0":	$amount = $ebm_row->amount;
							$premium_tenure = $ebm_row->premium_tenure;
							$model_flag=1;
							$revenue=round(($amount/$premium_tenure)/(25));
							break;
		case "Premium 2.0" :if($veh_type=="4w"){
								$revenue = $lead_price;
							}else{
								if($brand=="Royal Enfield"){
									$revenue=$lead_price;
								}else{
									$revenue=$nre_lead_price;
								}
							}
							$revenue*=2;
							$model_flag=2;
							break;
		case "Leads 3.0" :					
							if($veh_type=="4w"){
								$revenue = $lead_price;
							}else{
								if($brand=="Royal Enfield"){
									$revenue=($re_lead_price!=0)?$re_lead_price:$lead_price;
								}else{
									$revenue=($nre_lead_price!=0)?$nre_lead_price:$lead_price;
								}
							}
							$model_flag=3;
							break;
		default:			
							$credits = $ebm_row->b2b_credit_amt;
							$revenue = $credits;
							$model_flag=0;

	}

	
	switch($model_flag){
		case 0 : $dateData[$log]['ebm']['precredit']+=$revenue;break;
		case 1 : $dateData[$log]['ebm']['premium']+=$revenue;break;
		case 2 : $dateData[$log]['ebm']['premium']+=$revenue;break;
		case 3 : $dateData[$log]['ebm']['leads']+=$revenue;break;

	}
	$dateData[$log]['revenue']+=round($revenue,2);
	
}
$data[0]=$dateData;
$data[1]=$swapData;
echo json_encode($data);
?>
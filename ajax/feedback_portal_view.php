<?php
include("../config.php");
$conn = db_connect2();
$conn1 = db_connect1();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$sql_log = "SELECT feedback_admin FROM crm_admin WHERE crm_log_id='$crm_log_id'";
$res_log = mysqli_query($conn1,$sql_log);
while($row_log = mysqli_fetch_object($res_log))
{
	$feedback_admin = $row_log->feedback_admin;
}

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$today = date('Y-m-d');

$city = $_POST['city'];

$_SESSION['crm_city'] = $city;


?>
<div id="division" style="float:left; margin-top:10px;margin-left:35px;width:65%;">
<div id="div1" style="width:89%;float:left;">
<table class="table table-striped table-bordered tablesorter table-hover results" id="table">
<thead style="background-color:#B2DFDB;align:center;display: table-header-group;">
<th style="text-align:center;">Date <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Day </th>
<th style="text-align:center;">Total <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Yet to Service <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">In Progress <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Completed <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Idle <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
</thead>
<tbody id="tbody">

<?php

$start = strtotime($startdate);
$end = strtotime($enddate);
$noofdays = date('t',strtotime($start));
// Loop between timestamps, 24 hours at a time
for ( $i = $start; $i <= $end; $i = $i + 86400 ) {
    $thisDate = date('d M Y', $i);
    $day = date('l', strtotime($thisDate));

    $date = date('Y-m-d', $i);

    $enc_date=base64_encode($date);

if($city == "all"){
    $sql_b2b = "SELECT b.gb_booking_id,b.b2b_log,g.service_status FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id=g.booking_id WHERE b.b2b_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log)='$date' AND b.b2b_shop_id NOT IN (1014,1035,1670) ";    
}
else{
    $sql_b2b = "SELECT b.gb_booking_id,b.b2b_log,g.service_status FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id=g.booking_id WHERE b.b2b_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log)='$date' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.city='$city'";    
}
    $res_b2b = mysqli_query($conn,$sql_b2b);

    //initialize
    $yettoservice = 0;
    $inprogress = 0;
    $completed = 0;
    $idle = 0;
    $total = 0 ;

    while($row_b2b = mysqli_fetch_object($res_b2b)){
        $gb_booking_id = $row_b2b->gb_booking_id;
        $status = $row_b2b->service_status;
        $log = $row_b2b->b2b_log;

        $total = $total+1;

        switch($status){
            case 'Yet to Service his Vehicle':$yettoservice = $yettoservice+1;break;
            case 'Completed':$completed=$completed+1;break;
            case 'In Progress':$inprogress=$inprogress+1;break;
            default : $idle=$idle+1;
        }//switch
    } // while

if($date > $today){
    continue;
}
    $list[] = array("date" => $date, "yettoservice" => $yettoservice,"inprogress" =>$inprogress , "completed" => $completed , "idle" => $idle, "total" => $total);

    if($total == 0){
        ?>
        <tr>
    <td style="text-align:center;"><?php echo $thisDate; ?></td>
    <td style="text-align:center;"><?php echo $day;?></td>
    <td style="text-align:center;"><?php echo "0"; ?></td>
    <td style="text-align:center;"><?php echo "0"; ?></td>
    <td style="text-align:center;"><?php echo "0"; ?></td>
    <td style="text-align:center;"><?php echo "0"; ?></td>
    <td style="text-align:center;"><?php echo "0"; ?></td>
</tr>
        <?php
    }
    else{
    $ytsper = floor(($yettoservice/$total)*100);
    $ipper = floor(($inprogress/$total)*100);
    $cmpper = floor(($completed/$total)*100);
    $idlesper = floor(($idle/$total)*100);
?>
    <tr>
    <td style="text-align:center;"><?php echo $thisDate; ?></td>
    <td style="text-align:center;"><?php echo $day;?></td>
    <td style="text-align:center;"><?php echo $total; ?></td>
    <td style="text-align:center;"><a href="feedback_yet_to_service.php?t=<?php echo base64_encode(feedback); ?>&dt=<?php echo $enc_date; ?>"><?php echo $yettoservice."(".$ytsper."%)"; ?></a></td>
    <td style="text-align:center;"><a href="feedback_in_progress.php?t=<?php echo base64_encode(feedback); ?>&dt=<?php echo $enc_date; ?>"><?php echo $inprogress."(".$ipper."%)"; ?></a></td>
    <td style="text-align:center;"><a href="feedback_completed.php?t=<?php echo base64_encode(feedback); ?>&dt=<?php echo $enc_date; ?>"><?php echo $completed."(".$cmpper."%)"; ?></a></td>
    <td style="text-align:center;"><a href="feedback_idle.php?t=<?php echo base64_encode(feedback); ?>&dt=<?php echo $enc_date; ?>"><?php echo $idle."(".$idlesper."%)"; ?></a></td>
</tr>
<?php
    }
}
//initialize
$yettoservice_sum =0 ;$yettoservice_count=0;$yettoservice_avg=0;
//get non zero "yet to service" and calculate avg
foreach($list as $key=>$row){
    if($row['yettoservice'] != 0){
        $yettoservice_sum=$yettoservice_sum+$row['yettoservice'];
        $yettoservice_count = $yettoservice_count+1;
    }
    else{
        continue;
    }
}
if($yettoservice_count != 0 ){
    $yettoservice_avg = ceil($yettoservice_sum/$yettoservice_count);    
}
else{
    $yettoservice_avg = 0; 
}
//initialize
$inprogress_sum =0 ;$inprogress_count=0;$inprogress_avg=0;
//get non zero "in progress" and calculate avg
foreach($list as $key=>$row){
    if($row['inprogress'] != 0){
        $inprogress_sum=$inprogress_sum+$row['inprogress'];
        $inprogress_count = $inprogress_count+1;
    }
    else{
        continue;
    }
}
if($inprogress_count !=0){
    $inprogress_avg = ceil($inprogress_sum/$inprogress_count);    
}
else{
    $inprogress_avg = 0;
}

//initialize
$completed_sum =0 ;$completed_count=0;$completed_avg=0;
//get non zero "completed" and calculate avg
foreach($list as $key=>$row){
    if($row['completed'] != 0){
        $completed_sum=$completed_sum+$row['completed'];
        $completed_count = $completed_count+1;
    }
    else{
        continue;
    }
}
if($completed_count !=0){
    $completed_avg = ceil($completed_sum/$completed_count);    
}
else{
    $completed_avg = 0;
}

//initialize
$idle_sum =0 ;$idle_count=0;$idle_avg=0;
//get non zero "idle" and calculate avg
foreach($list as $key=>$row){
    if($row['idle'] != 0){
        $idle_sum=$idle_sum+$row['idle'];
        $idle_count = $idle_count+1;
    }
    else{
        continue;
    }
}
if($idle_count !=0){
    $idle_avg = ceil($idle_sum/$idle_count);    
}
else{
    $idle_avg = 0;
}

?>

</tbody>
</table>
</div>
<div id="rightpane" style="position:fixed;right:30px;width:30%;">
<div style="float:left;width:90%;">
<table class="table table-bordered" style="border:1px solid #00BCD4;margin:15px;padding:15px;overflow:auto;" >
<thead>
<th colspan="2" style="border:1px solid #B2EBF2;text-align:center;background-color:#b2ebf270;" >Average</th>
<th style="border:1px solid #B2EBF2;text-align:center;background-color:#b2ebf270;" >Total</th>
</thead>
<tbody>
<tr>
<td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;">Yet to Service</td>
<td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;"><?php echo $yettoservice_avg; ?></td>
<td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;"><?php echo $yettoservice_sum; ?></td>
</tr>
<tr>
<td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;">In Progress</td>
<td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;"><?php echo $inprogress_avg; ?></td>
<td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;"><?php echo $inprogress_sum; ?></td>
</tr>
<tr>
<td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;">Completed</td>
<td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;"><?php echo $completed_avg; ?></td>
<td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;"><?php echo $completed_sum; ?></td>
</tr>
<tr>
<td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;">Idle</td>
<td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;"><?php echo $idle_avg; ?></td>
<td style="border:1px solid #B2EBF2;text-align:center;font-size:13px;"><?php echo $idle_sum; ?></td>
</tr>

</tbody>
</table>

<?php
if($feedback_admin == '1')
{
	$_SESSION['feedback_admin'] = '1';
?>
	<div>
	<a href="feedback_moderation.php"> <button style="background: #B2EBF2;float: left;margin-left: 29%;margin-top: 20%;" class="btn btn-lg"><b>Moderate Feedback</b></button></a>
	</div>
<?php
}?>
</div>
<!--<h1>Pie Chart</h1>
<div id="chartContainer"></div>

<?php
    $dataPoints = array(
	array("y" => 72.48, "legendText" => "Google", "label" => "Google"),
	array("y" => 10.39, "legendText" => "Bing", "label" => "Bing"),
	array("y" => 7.78, "legendText" => "Yahoo!", "label" => "Yahoo!"),
	array("y" => 7.14, "legendText" => "Baidu", "label" => "Baidu"),
	array("y" => 0.22, "legendText" => "Ask", "label" => "Ask"),
	array("y" => 0.15, "legendText" => "AOL", "label" => "AOL"),
	array("y" => 1.84, "legendText" => "Others", "label" => "Others")
    );
?>

<script type="text/javascript">
    $(function () {
        var chart = new CanvasJS.Chart("chartContainer", {
            title: {
                text: "Desktop Search Engine Market Share, Jul-2016"
            },
            animationEnabled: true,
            legend: {
                verticalAlign: "center",
                horizontalAlign: "left",
                fontSize: 20,
                fontFamily: "Helvetica"
            },
            theme: "theme2",
            data: [
            {
                type: "pie",
                indexLabelFontFamily: "Garamond",
                indexLabelFontSize: 20,
                indexLabel: "{label} {y}%",
                startAngle: -20,
                showInLegend: true,
                toolTipContent: "{legendText} {y}%",
                dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
            }
            ]
        });
        chart.render();
    });
</script>-->
<!--<script>
var dt = new google.visualization.DataTable({
    cols: [{id: 'task', label: 'Task', type: 'string'},
           {id: 'hours', label: 'Hours per Day', type: 'number'}],
    rows: [{c:[{v: 'Work'}, {v: 11}]},
           {c:[{v: 'Eat'}, {v: 2}]},
           {c:[{v: 'Commute'}, {v: 2}]},
           {c:[{v: 'Watch TV'}, {v:2}]},
           {c:[{v: 'Sleep'}, {v:7, f:'7.000'}]}]
    }, 0.6);
var data = new google.visualization.DataTable();
data.addColumn('string', 'Task');
data.addColumn('number', 'Hours per Day');
data.addRows([
  ['Work', 11],
  ['Eat', 2],
  ['Commute', 2],
  ['Watch TV', 2],
  ['Sleep', {v:7, f:'7.000'}]
]);
</script>-->
</div>
<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#table").tablesorter( {sortList: [[0,0], [1,0]]} );
    }
);
</script>
<?php
ini_set("precision", 3);
include("../config.php");
$conn = db_connect1();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$super_flag = $_SESSION['super_flag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$starttime = date('H:i:s',strtotime($_POST['starttime']));
$endtime =  date('H:i:s',strtotime($_POST['endtime']));
$city = $_POST['city'];
$target = $_POST['target'];
$rating = $_POST['rating'];

$_SESSION['crm_city'] = $city; 
//$veh_type=$_POST['vehicle_type'];

$start = $startdate.' '.$starttime;
$end = $enddate.' '.$endtime;
?>
<?php

switch($target)
{
	case "#Service" : 
	$vehicle_arr=array("all","2w","4w");
	$i=1;
	$cond_sr_v="";
	$cond_sr="";
	foreach($vehicle_arr as $vehicle_type){
	$cond_sr_v="";
	$cond_sr="";
	$cond_sr = $cond_sr.($city == 'all' ? "" : "AND b.city='$city'");
	$cond_sr_v=$cond_sr_v.($vehicle_type == 'all' ? "" : " AND b.vehicle_type='$vehicle_type'");
	$sql_service="SELECT b.service_type as master_service,
	 COUNT(CASE WHEN f.reservice_flag = '1' AND f.feedback_status = '1' THEN 1 ELSE NULL END) AS Reservice,
	 COUNT(case when f.flag='0' then 1 else NULL end) as Goaxle, 
	 COUNT(CASE WHEN (f.feedback_status = '3' or f.service_status='In Progress') and f.flag='0' THEN 1 ELSE NULL END) AS InProgress,
	 COUNT(CASE WHEN (f.feedback_status='1' or f.service_status='Completed') and f.flag='0' THEN 1 ELSE NULL END) AS Completed, 
	 COUNT(CASE WHEN (f.feedback_status = '2' or f.feedback_status = '11' OR f.feedback_status = '12' OR f.feedback_status = '21' OR f.feedback_status = '22' OR f.feedback_status = '31' OR f.feedback_status = '41' OR f.feedback_status = '51') and f.flag='0' and f.service_status!='In Progress' THEN 1 ELSE NULL END) AS FollowUp, 
	 COUNT(CASE WHEN (f.feedback_status = '-2' OR f.feedback_status = '-3' OR f.feedback_status = '4') and f.flag='0' and f.service_status!='In Progress' THEN 1 ELSE NULL END) AS Others,
	 COUNT(CASE WHEN f.feedback_status = '5' AND f.flag = '0' THEN 1 ELSE NULL END) AS Cancel,
	 COUNT(CASE WHEN f.feedback_status = '0' AND f.flag = '0' THEN 1 ELSE NULL END) AS Idle
	 FROM go_bumpr.user_booking_tb b  LEFT JOIN feedback_track f ON f.booking_id = b.booking_id WHERE b.mec_id NOT IN (400001 , 200018, 200379, 400974)
        AND f.shop_id NOT IN (1014 , 1035, 1670) AND b.user_id NOT IN (21816 , 41317)
		{$cond_sr_v} AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND f.log BETWEEN '$start' AND '$end' {$cond_sr}  GROUP BY b.service_type";
		//echo $sql_service;
		$sql_ser_res=mysqli_query($conn,$sql_service);
		$tr_sr="";
		$total_cancel=$total_endconv=$total_inprogress=$total_completed=$total_followup=$total_goaxle=$total_others=$total_idle=$total_reservice=0;
		while($sql_ser_row=mysqli_fetch_assoc($sql_ser_res)){
			if($vehicle_type=="all"){
				$total_goaxle_count=$total_goaxle_count+$sql_ser_row[Goaxle];
			}
			$total_goaxle=$total_goaxle+$sql_ser_row[Goaxle];
			$total_inprogress=$total_inprogress+$sql_ser_row[InProgress];
			$total_completed=$total_completed+$sql_ser_row[Completed];
			$total_followup=$total_followup+$sql_ser_row[FollowUp];
			$total_others=$total_others+$sql_ser_row[Others];
			$total_cancel=$total_cancel+$sql_ser_row[Cancel];
			$total_idle=$total_idle+$sql_ser_row[Idle];
			$total_reservice=$total_reservice+$sql_ser_row[Reservice];

				$tr_sr.="<tr>
							<td>".$sql_ser_row[master_service]."</td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$sql_ser_row[master_service]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Goaxle' data-target='#myModalservice'>".$sql_ser_row[Goaxle]."</td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$sql_ser_row[master_service]."' data-startdate='".$start."' data-enddate='".$end."' data-status='InProgress' data-target='#myModalservice'>".$sql_ser_row[InProgress]."(".number_format((($sql_ser_row[InProgress]/$sql_ser_row[Goaxle])*100),2)."%)</td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$sql_ser_row[master_service]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Completed' data-target='#myModalservice'>".$sql_ser_row[Completed]."(".number_format((($sql_ser_row[Completed]/$sql_ser_row[Goaxle])*100),2)."%)</td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$sql_ser_row[master_service]."' data-startdate='".$start."' data-enddate='".$end."' data-status='FollowUp' data-target='#myModalservice'>".$sql_ser_row[FollowUp]."(".number_format((($sql_ser_row[FollowUp]/$sql_ser_row[Goaxle])*100),2)."%)</td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$sql_ser_row[master_service]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Others' data-target='#myModalservice'>".$sql_ser_row[Others]."(".number_format((($sql_ser_row[Others]/$sql_ser_row[Goaxle])*100),2)."%)</td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$sql_ser_row[master_service]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Idle' data-target='#myModalservice'>".$sql_ser_row[Idle]."(".number_format((($sql_ser_row[Idle]/$sql_ser_row[Goaxle])*100),2)."%)</td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$sql_ser_row[master_service]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Cancel' data-target='#myModalservice'>".$sql_ser_row[Cancel]."(".number_format((($sql_ser_row[Cancel]/$sql_ser_row[Goaxle])*100),2)."%)</td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='".$sql_ser_row[master_service]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Reservice' data-target='#myModalservice'>".$sql_ser_row[Reservice]."</td>
						</tr>";
		}	
					$tr_sr.="<tr class='avoid-sortSer".$i."' style='display:none'>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>Total</td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Goaxle' data-target='#myModalservice'>".$total_goaxle."</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='InProgress' data-target='#myModalservice'>".$total_inprogress."(".number_format((($total_inprogress/$total_goaxle)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Completed' data-target='#myModalservice'>".$total_completed."(".number_format((($total_completed/$total_goaxle)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='FollowUp' data-target='#myModalservice'>".$total_followup."(".number_format((($total_followup/$total_goaxle)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Others' data-target='#myModalservice'>".$total_others."(".number_format((($total_others/$total_goaxle)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Idle' data-target='#myModalservice'>".$total_idle."(".number_format((($total_idle/$total_goaxle)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Cancel' data-target='#myModalservice'>".$total_cancel."(".number_format((($total_cancel/$total_goaxle)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-service='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Reservice' data-target='#myModalservice'>".$total_reservice."</button></td>
					</tr>";
					//$str = $tr3.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$tr_3;

					$data[$i][]= array('tr'=>$tr_sr);
					$table="table".$i;
					$result[$table] = $data[$i];
					$i++;
	}
					$result['count'] = $total_goaxle_count;
					//print_r($result);
					echo json_encode($result);
					break;
	case "#Person" :
	//$vehicle_arr=array("all","2w","4w");
	//foreach($vehicle_arr as $vehicle_type){
	//$cond_pr =$cond_sr.($vehicle_type == 'all' ? "" : " AND b.vehicle_type='$vehicle_type'");
	$cond_pr = $cond_sr.($city == 'all' ? "" : "AND b.city='$city'");
					$sql_person="SELECT ca.name,ca.crm_log_id,
					COUNT(CASE WHEN f.reservice_flag = '1' AND f.feedback_status = '1' THEN 1 ELSE NULL END) AS Reservice,
					COUNT(case when f.flag='0' then 1 else NULL end) as Goaxle, 
					COUNT(CASE WHEN (f.feedback_status = '3' or f.service_status='In Progress') and f.flag='0' THEN 1 ELSE NULL END) AS InProgress,
				 COUNT(CASE WHEN (f.feedback_status='1' or f.service_status='Completed') and f.flag='0' THEN 1 ELSE NULL END) AS Completed, 
					COUNT(CASE WHEN (f.feedback_status = '2' or f.feedback_status = '11' OR f.feedback_status = '12' OR f.feedback_status = '21' OR f.feedback_status = '22' OR f.feedback_status = '31' OR f.feedback_status = '41' OR f.feedback_status = '51') and f.flag='0' and f.service_status!='In Progress' THEN 1 ELSE NULL END) AS FollowUp, 
					COUNT(CASE WHEN (f.feedback_status = '-2' OR f.feedback_status = '-3' OR f.feedback_status = '4') and f.flag='0' and f.service_status!='In Progress' THEN 1 ELSE NULL END) AS Others,
					COUNT(CASE WHEN f.feedback_status = '5' AND f.flag = '0' THEN 1 ELSE NULL END) AS Cancel,
					COUNT(CASE WHEN f.feedback_status = '0' AND f.flag = '0' THEN 1 ELSE NULL END) AS Idle
					FROM go_bumpr.user_booking_tb b 
					LEFT JOIN feedback_track f ON f.booking_id = b.booking_id left join crm_admin ca on ca.crm_log_id=f.crm_log_id 
					WHERE b.mec_id NOT IN (400001 , 200018, 200379, 400974) AND f.shop_id NOT IN (1014 , 1035, 1670) AND b.user_id 
					NOT IN (21816 , 41317) AND b.service_type != 'IOCL Check-up' 
					AND b.source != 'Re-Engagement Bookings' AND f.log BETWEEN '$start' AND '$end' {$cond_pr}  GROUP BY ca.name";
					$sql_per_res=mysqli_query($conn,$sql_person);

					$total_cancel=$total_inprogress=$total_completed=$total_endconv=$total_followup=$total_goaxle=$total_others=$total_idle=$total_reservice=0;

					while($sql_per_row=mysqli_fetch_assoc($sql_per_res)){
						//if($vehicle_type=="all"){
				$total_goaxle_count=$total_goaxle_count+$sql_per_row[Goaxle];
			//}
						$total_goaxle=$total_goaxle+$sql_per_row[Goaxle];
						$total_inprogress=$total_inprogress+$sql_per_row[InProgress];
			            $total_completed=$total_completed+$sql_per_row[Completed];
						$total_followup=$total_followup+$sql_per_row[FollowUp];
						$total_others=$total_others+$sql_per_row[Others];
						$total_idle=$total_idle+$sql_per_row[Idle];
						$total_cancel=$total_cancel+$sql_per_row[Cancel];
						$total_reservice=$total_reservice+$sql_per_row[Reservice];
						
							$tr_pr.="<tr>
										<td style='text-align:center;font-size:13px;'>".$sql_per_row[name]."</td>
										<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$sql_per_row[crm_log_id]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Goaxle' data-target='#myModalperson'>".$sql_per_row[Goaxle]."</button></td>
										<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$sql_per_row[crm_log_id]."' data-startdate='".$start."' data-enddate='".$end."' data-status='InProgress' data-target='#myModalperson'>".$sql_per_row[InProgress]."(".number_format((($sql_per_row[InProgress]/$sql_per_row[Goaxle])*100),2)."%)</button></td>
										<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$sql_per_row[crm_log_id]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Completed' data-target='#myModalperson'>".$sql_per_row[Completed]."(".number_format((($sql_per_row[Completed]/$sql_per_row[Goaxle])*100),2)."%)</button></td>
										<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$sql_per_row[crm_log_id]."' data-startdate='".$start."' data-enddate='".$end."' data-status='FollowUp' data-target='#myModalperson'>".$sql_per_row[FollowUp]."(".number_format((($sql_per_row[FollowUp]/$sql_per_row[Goaxle])*100),2)."%)</button></td>
										<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$sql_per_row[crm_log_id]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Others' data-target='#myModalperson'>".$sql_per_row[Others]."(".number_format((($sql_per_row[Others]/$sql_per_row[Goaxle])*100),2)."%)</button></td>
										<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$sql_per_row[crm_log_id]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Idle' data-target='#myModalperson'>".$sql_per_row[Idle]."(".number_format((($sql_per_row[Idle]/$sql_per_row[Goaxle])*100),2)."%)</button></td>
										<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$sql_per_row[crm_log_id]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Cancel' data-target='#myModalperson'>".$sql_per_row[Cancel]."(".number_format((($sql_per_row[Cancel]/$sql_per_row[Goaxle])*100),2)."%)</button></td>
										<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='".$sql_per_row[crm_log_id]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Reservice' data-target='#myModalperson'>".$sql_per_row[Reservice]."</button></td>
									</tr>";
					}
					
					$tr_pr.="<tr class = 'avoid-sortperson' style='display:none'>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>Total</td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Goaxle' data-target='#myModalperson'>".$total_goaxle."</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='InProgress' data-target='#myModalperson'>".$total_inprogress."(".number_format((($total_inprogress/$total_goaxle)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Completed' data-target='#myModalperson'>".$total_completed."(".number_format((($total_completed/$total_goaxle)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='FollowUp' data-target='#myModalperson'>".$total_followup."(".number_format((($total_followup/$total_goaxle)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Others' data-target='#myModalperson'>".$total_others."(".number_format((($total_others/$total_goaxle)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Idle' data-target='#myModalperson'>".$total_idle."(".number_format((($total_idle/$total_goaxle)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Cancel' data-target='#myModalperson'>".$total_cancel."(".number_format((($total_cancel/$total_goaxle)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-crmlogid='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Reservice' data-target='#myModalperson'>".$total_reservice."</button></td>
					</tr>";
					//$str = $tr2.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$tr1_l;
					$person_data[] = array('tr'=>$tr_pr);
					$result['person_data'] = $person_data;
				//}
					$result['count'] = $total_goaxle_count;
					echo json_encode($result);
					break; 
	case "#Rtt" :
	//$vehicle_arr=array("all","2w","4w");
	//foreach($vehicle_arr as $vehicle_type){
	//$cond_pr =$cond_sr.($vehicle_type == 'all' ? "" : " AND b.vehicle_type='$vehicle_type'");
	$cond_pr = $cond_pr.($city == 'all' ? "" : "AND b.city='$city'");
	$cond_rating = $cond_rating.($rating == 'all' ? "" : "AND ur.rating='$rating'");
	
	$sql_rtt="SELECT b.vehicle_type,
	
	 COUNT(CASE when f.flag='0' then 1 else NULL end) as Goaxle,
     COUNT(CASE WHEN bb.b2b_check_in_report = 0 and bb.b2b_vehicle_at_garage = 0 AND bb.b2b_vehicle_ready = 0 AND f.flag='0' THEN 1 ELSE NULL END) AS NoRTT, 
	 COUNT(CASE WHEN bb.b2b_check_in_report = 1 and bb.b2b_vehicle_at_garage != 1 AND bb.b2b_vehicle_ready != 1 AND f.flag='0' THEN 1 ELSE NULL END) AS CheckIns, 
	 COUNT(CASE WHEN bb.b2b_check_in_report = 1 AND bb.b2b_vehicle_at_garage = 1 AND bb.b2b_vehicle_ready = 1 AND f.flag='0' THEN 1 ELSE NULL END) AS AllStages
	 FROM go_bumpr.user_booking_tb b  
     LEFT JOIN feedback_track f ON f.booking_id = b.booking_id 
     LEFT JOIN b2b.b2b_booking_tbl bb on f.b2b_booking_id=bb.b2b_booking_id
     WHERE b.mec_id NOT IN (400001 , 200018, 200379, 400974)
     AND f.shop_id NOT IN (1014 , 1035, 1670) AND b.user_id NOT IN (21816,41317)
	{$cond_pr}
	AND b.service_type != 'IOCL Check-up' AND b.source != 'Re-Engagement Bookings' AND f.log BETWEEN '$start' AND '$end'  GROUP BY b.vehicle_type";
					
					//echo $sql_rtt;
					$sql_per_res=mysqli_query($conn,$sql_rtt);

					$total_goaxle=$total_checkins=$total_allstages=$total_no_rtt=$total_others=$total_idle=$total_reservice=0;
					$rating_grouped="";
					

					while($sql_per_row=mysqli_fetch_assoc($sql_per_res)){
						//if($vehicle_type=="all"){
				$total_goaxle_count=$total_goaxle_count+$sql_per_row[Goaxle];
			//}
						//print_r($sql_per_row);
						$total_goaxle=$total_goaxle+$sql_per_row[Goaxle];
						$total_checkins=$total_checkins+$sql_per_row[CheckIns];
						$total_allstages=$total_allstages+$sql_per_row[AllStages];
						$total_no_rtt=$total_no_rtt+$sql_per_row[NoRTT];
						/* $total_others=$total_others+$sql_per_row[Others];
						$total_idle=$total_idle+$sql_per_row[Idle];
						$total_cancel=$total_cancel+$sql_per_row[Cancel];
						$total_reservice=$total_reservice+$sql_per_row[Reservice]; */
						
						
						
							$tr_pr.="<tr>
										<td style='text-align:center;font-size:13px;'>".$sql_per_row[vehicle_type]."</td>
										<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-rating='".$sql_per_row[vehicle_type]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Goaxle' data-target='#myModalrtt'>".$sql_per_row[Goaxle]."</button></td>
										<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-rating='".$sql_per_row[vehicle_type]."' data-startdate='".$start."' data-enddate='".$end."' data-status='NoRTT' data-target='#myModalrtt'>".$sql_per_row[NoRTT]."</button></td>
										<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-rating='".$sql_per_row[vehicle_type]."' data-startdate='".$start."' data-enddate='".$end."' data-status='CheckIns' data-target='#myModalrtt'>".$sql_per_row[CheckIns]."</button></td>
										<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-rating='".$sql_per_row[vehicle_type]."' data-startdate='".$start."' data-enddate='".$end."' data-status='AllStages' data-target='#myModalrtt'>".$sql_per_row[AllStages]."</button></td>
										
									</tr>";
					}
					
					//$count = count($sql_per_row[Goaxle]);
					
					$tr_pr.="<tr class = 'avoid-sortrtt' style='display:none'>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>Total</td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-rating='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Goaxle' data-target='#myModalrtt'>".$total_goaxle."</button></td>  
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-rating='all' data-startdate='".$start."' data-enddate='".$end."' data-status='NoRTT' data-target='#myModalrtt'>".$total_no_rtt."</button></td>  
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-rating='all' data-startdate='".$start."' data-enddate='".$end."' data-status='CheckIns' data-target='#myModalrtt'>".$total_checkins."</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-rating='all' data-startdate='".$start."' data-enddate='".$end."' data-status='AllStages' data-target='#myModalrtt'>".$total_allstages."</button></td>
					</tr>";
					//$str = $tr2.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$tr1_l;
					$rtt_data[] = array('tr'=>$tr_pr);
					$result['rtt_data'] = $rtt_data;
					
				//}
					
					
					$result['count'] = $total_goaxle_count;
					echo json_encode($result);
					break; 
	case "#Source" :
	$vehicle_arr=array("all","2w","4w");
	$i=1;
	$cond_src_v="";
	$cond_src="";
	foreach($vehicle_arr as $vehicle_type){
	$cond_src_v="";
	$cond_src="";
	$cond_src = $cond_src.($city == 'all' ? "" : "AND b.city='$city'");
	$cond_src_v=$cond_src_v.($vehicle_type == 'all' ? "" : " AND b.vehicle_type='$vehicle_type'");

	$sql_source="SELECT us.user_source,
	COUNT(CASE WHEN f.reservice_flag = '1' AND f.feedback_status = '1' THEN 1 ELSE NULL END) AS Reservice,
	COUNT(case when f.flag='0' then 1 else NULL end) as Goaxle, 
	COUNT(CASE WHEN (f.feedback_status = '3' or f.service_status='In Progress') and f.flag='0' THEN 1 ELSE NULL END) AS InProgress,
	 COUNT(CASE WHEN (f.feedback_status='1' or f.service_status='Completed') and f.flag='0' THEN 1 ELSE NULL END) AS Completed, 
	COUNT(CASE WHEN (f.feedback_status = '2' or f.feedback_status = '11' OR f.feedback_status = '12' OR f.feedback_status = '21' OR f.feedback_status = '22' OR f.feedback_status = '31' OR f.feedback_status = '41' OR f.feedback_status = '51') and f.flag='0' and f.service_status!='In Progress' THEN 1 ELSE NULL END) AS FollowUp, 
	COUNT(CASE WHEN (f.feedback_status = '-2' OR f.feedback_status = '-3' OR f.feedback_status = '4') and f.flag='0' and f.service_status!='In Progress' THEN 1 ELSE NULL END) AS Others,
	COUNT(CASE WHEN f.feedback_status = '5' AND f.flag = '0' THEN 1 ELSE NULL END) AS Cancel,
	COUNT(CASE WHEN f.feedback_status = '0' AND f.flag = '0' THEN 1 ELSE NULL END) AS Idle
	FROM go_bumpr.user_booking_tb b 
	LEFT JOIN feedback_track f ON f.booking_id = b.booking_id 
	left join user_source_tbl us on us.user_source=b.source WHERE b.mec_id NOT IN (400001 , 200018, 200379, 400974) AND f.shop_id NOT IN (1014 , 1035, 1670) 
	AND b.user_id NOT IN (21816 , 41317) {$cond_src_v} AND b.service_type != 'IOCL Check-up'
	AND b.source != 'Re-Engagement Bookings' AND f.log BETWEEN '$start' AND '$end' {$cond_src}  GROUP BY us.user_source";
		//echo $sql_service;
		$sql_src_res=mysqli_query($conn,$sql_source);
		$tr_src="";
		$total_cancel=$total_endconv=$total_inprogress=$total_completed=$total_followup=$total_goaxle=$total_others=$total_idle=$total_reservice=0;
		while($sql_scr_row=mysqli_fetch_assoc($sql_src_res)){
			if($vehicle_type=="all"){
				$total_goaxle_count=$total_goaxle_count+$sql_scr_row[Goaxle];
			}
			$total_goaxle=$total_goaxle+$sql_scr_row[Goaxle];
			$total_inprogress=$total_inprogress+$sql_scr_row[InProgress];
			$total_completed=$total_completed+$sql_scr_row[Completed];
			$total_followup=$total_followup+$sql_scr_row[FollowUp];
			$total_others=$total_others+$sql_scr_row[Others];
			$total_cancel=$total_cancel+$sql_scr_row[Cancel];
			$total_idle=$total_idle+$sql_scr_row[Idle];
			$total_reservice=$total_reservice+$sql_scr_row[Reservice];
			
				if($sql_scr_row[user_source]==""){
					$source="Book-Now";
				}else{
					$source=$sql_scr_row[user_source];
				}
				$tr_src.="<tr>
							<td style='text-align:center;font-size:13px;'>".$source."</td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='".$sql_scr_row[user_source]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Goaxle' data-target='#myModalsource'>".$sql_scr_row[Goaxle]."</button></td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='".$sql_scr_row[user_source]."' data-startdate='".$start."' data-enddate='".$end."' data-status='InProgress' data-target='#myModalsource'>".$sql_scr_row[InProgress]."(".number_format((($sql_scr_row[InProgress]/$sql_scr_row[Goaxle])*100),2)."%)</button></td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='".$sql_scr_row[user_source]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Completed' data-target='#myModalsource'>".$sql_scr_row[Completed]."(".number_format((($sql_scr_row[Completed]/$sql_scr_row[Goaxle])*100),2)."%)</button></td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='".$sql_scr_row[user_source]."' data-startdate='".$start."' data-enddate='".$end."' data-status='FollowUp' data-target='#myModalsource'>".$sql_scr_row[FollowUp]."(".number_format((($sql_scr_row[FollowUp]/$sql_scr_row[Goaxle])*100),2)."%)</button></td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='".$sql_scr_row[user_source]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Others' data-target='#myModalsource'>".$sql_scr_row[Others]."(".number_format((($sql_scr_row[Others]/$sql_scr_row[Goaxle])*100),2)."%)</button></td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='".$sql_scr_row[user_source]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Idle' data-target='#myModalsource'>".$sql_scr_row[Idle]."(".number_format((($sql_scr_row[Idle]/$sql_scr_row[Goaxle])*100),2)."%)</button></td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='".$sql_scr_row[user_source]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Cancel' data-target='#myModalsource'>".$sql_scr_row[Cancel]."(".number_format((($sql_scr_row[Cancel]/$sql_scr_row[Goaxle])*100),2)."%)</button></td>
							<td style='text-align:center;font-size:13px;'><button type='button' style='background-color:transparent;' class='btn'  data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='".$sql_scr_row[user_source]."' data-startdate='".$start."' data-enddate='".$end."' data-status='Reservice' data-target='#myModalsource'>".$sql_scr_row[Reservice]."</button></td>
						</tr>";
		}	
					$tr_src.="<tr class='avoid-sortsrc".$i."' style='display:none'>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'>Total</td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Goaxle' data-target='#myModalsource'>".$total_goaxle."</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='InProgress' data-target='#myModalsource'>".$total_inprogress."(".number_format((($total_inprogress/$total_goaxle)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Completed' data-target='#myModalsource'>".$total_completed."(".number_format((($total_completed/$total_goaxle)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='FollowUp' data-target='#myModalsource'>".$total_followup."(".number_format((($total_followup/$total_goaxle)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Others' data-target='#myModalsource'>".$total_others."(".number_format((($total_others/$total_goaxle)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Idle' data-target='#myModalsource'>".$total_idle."(".number_format((($total_idle/$total_goaxle)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Cancel' data-target='#myModalsource'>".$total_cancel."(".number_format((($total_cancel/$total_goaxle)*100),2)."%)</button></td>
					<td style='text-align:center;background-color:rgba(84, 156, 156, 0.57);'><button type='button' style='background-color:transparent;' class='btn' data-toggle='modal' data-vehicle='".$vehicle_type."' data-source='all' data-startdate='".$start."' data-enddate='".$end."' data-status='Reservice' data-target='#myModalsource'>".$total_reservice."</button></td>
					</tr>";
					//$str = $tr3.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$tr_3;

					$src_data[$i][]= array('tr'=>$tr_src);
					$table="src_table".$i;
					$result[$table] = $src_data[$i];
					$i++;
	}
					$result['count'] = $total_goaxle_count;
						echo json_encode($result);
						
						break;
						
}


?>
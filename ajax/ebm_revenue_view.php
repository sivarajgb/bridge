<?php
// error_reporting(E_ALL); ini_set('display_errors', 1);
include("../config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();
$today=date('Y-m-d');

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enc_date = base64_encode($startdate);
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$vehicle = $_POST['vehicle_type'];
$city = $_POST['city'];

$_SESSION['crm_city'] = $city;

$enc_veh = base64_encode($vehicle);

$cond= ($vehicle=='all')?"" : "AND b.vehicle_type = '$vehicle'";
$cond1= $city == 'all' ? "" : "AND b.city ='$city'";
?>

<div align="center" id = "table" style="margin-left:20px;margin-right:20px;">
<table id="example1" class="table table-striped table-bordered tablesorter table-hover results">
<thead style="background-color:#B2DFDB;">

<th style="text-align:center;">Service Center</th>
<th style="text-align:center;">Locality</th>
<th style="text-align:center;">Business Model</th>

<th style="text-align:center;">Total Exceptions <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Total Credits Deducted <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Total Leads Deducted <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Revenue <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
</thead>
<tbody id="tbody">

<?php 
	$total_exceptions=0;
	$total_credits_deducted=0;
	$total_leads_deducted=0;
	$total_revenue=0;

	$sql_goaxle="SELECT 
	DISTINCT e.booking_id,
	e.b2b_shop_id,
	e.shop_name,
    b.service_type,
    b.vehicle_type,
    bb.brand,
	g.model AS business_model,
    g.lead_price,
    g.re_lead_price,
    g.nre_lead_price,
    g.amount,
    g.premium_tenure,
    g.start_date,
    g.end_date,
    bb.b2b_credit_amt,
    e.exception_log,
    c.b2b_credits,
    c.b2b_leads,
    c.b2b_partner_flag,
    m.b2b_new_flg,
    m.b2b_address4
FROM
    exception_mechanism_track e 
    LEFT JOIN
    b2b.b2b_booking_tbl bb ON bb.gb_booking_id=e.booking_id AND bb.b2b_shop_id=e.b2b_shop_id
    LEFT JOIN
    user_booking_tb b ON e.booking_id = b.booking_id
    LEFT JOIN 
    garage_model_history g ON e.b2b_shop_id = g.b2b_shop_id
        AND DATE(e.exception_log) >= DATE(g.start_date)
        AND g.id=(select id from go_bumpr.garage_model_history where b2b_shop_id=e.b2b_shop_id AND DATE(e.exception_log) >= DATE(start_date) order by start_date desc limit 1) 
    LEFT JOIN
		b2b.b2b_mec_tbl m ON m.b2b_shop_id=e.b2b_shop_id
	LEFT JOIN
		b2b.b2b_credits_tbl c ON c.b2b_shop_id=e.b2b_shop_id
WHERE
    (DATE(e.exception_log) BETWEEN '$startdate' and '$enddate') AND e.flag=0 {$cond} {$cond1} ORDER BY e.exception_log";
	$res_goaxle = mysqli_query($conn1,$sql_goaxle);
	// echo $sql_goaxle;
	while($row_goaxle = mysqli_fetch_object($res_goaxle))
	{
			
		$model_flag=0;
		$model_start=$row_goaxle->start_date;
		$model_end=$row_goaxle->end_date;
		$veh_type=$row_goaxle->vehicle_type;
		$brand=$row_goaxle->brand;
		
		$business_model = trim($row_goaxle->business_model);
		$revenue=0;
		$credit_amt=$row_goaxle->b2b_credit_amt;
		$lead_price = $row_goaxle->lead_price;
		$re_lead_price = $row_goaxle->re_lead_price;
		$nre_lead_price = $row_goaxle->nre_lead_price;
		// $exception_log=$row_goaxle->exception_log;
		// echo $exception_log;
		$locality = $row_goaxle->b2b_address4;
		$shop_name=$row_goaxle->shop_name;
		$service_type=$row_goaxle->service_type;
		$leads=$row_goaxle->b2b_leads;
		$credits=$row_goaxle->b2b_credits;

		// $check_in_report=$row_goaxle->b2b_check_in_report;
		$premium=$row_goaxle->b2b_partner_flag;
		$new_flag=$row_goaxle->b2b_new_flg;
		$shop_id=$row_goaxle->b2b_shop_id;
		
		$shopData[$shop_id]['model']=$business_model;
		$shopData[$shop_id]['lead_price']=$lead_price;
		$shopData[$shop_id]['re_lead_price']=$re_lead_price;
		$shopData[$shop_id]['nre_lead_price']=$nre_lead_price;
		$shopData[$shop_id]['type']=$veh_type;
		$shopData[$shop_id]['locality']=$locality;
		$shopData[$shop_id]['shop_name']=$shop_name;
		if(!array_key_exists('credits_deducted',$shopData[$shop_id])){
			$shopData[$shop_id]['credits_deducted']=0;
		}
		if(!array_key_exists('leads_deducted',$shopData[$shop_id])){
			$shopData[$shop_id]['leads_deducted']=0;
		}
		if($new_flag==1){
			$account="L".$leads;
		}else{
			$account=$credits;
		}
		$shopData[$shop_id]['account']=$account;

		if($business_model=="Pre Credit"){
		 	$credits = $row_goaxle->b2b_credit_amt;
		 	$shopData[$shop_id]['credits_deducted']+=($credits/100);
			$revenue = $credits;
			
		}else if($business_model=="Premium 1.0"){
			$amount = $row_goaxle->amount;
			$premium_tenure = $row_goaxle->premium_tenure;
			
			$revenue=round(($amount/$premium_tenure)/25);
			$shopData[$shop_id]['credits_deducted']+=($credit_amt/100);
		}else if($business_model=="Premium 2.0"){
			if($veh_type=="4w"){
				$revenue = 2*$lead_price;
			}else{
				if($brand=="Royal Enfield"){
					$revenue=2*$lead_price;
				}else{
					$revenue=2*$nre_lead_price;
				}
			}
			
			$shopData[$shop_id]['leads_deducted']+=2;
		}else if($business_model=="Leads 3.0"){
						
			if($veh_type=="4w"){
				$revenue = $lead_price;
			}else{
				if($brand=="Royal Enfield"){
					$revenue=($re_lead_price!=0)?$re_lead_price:$lead_price;
				}else{
					$revenue=($nre_lead_price!=0)?$nre_lead_price:$lead_price;
				}
			}
			$shopData[$shop_id]['leads_deducted']+=1;
		}else{
			$credits = $row_goaxle->b2b_credit_amt;
			$revenue = $credits;
		 	$shopData[$shop_id]['credits_deducted']+=($credits/100);
			$shopData[$shop_id]['model']="Pre Credit";
		}
		
		$shopData[$shop_id]['revenue']+=round($revenue,2);
		$shopData[$shop_id]['exceptions']+=1;
		
		
	}
	foreach ($shopData as $key => $value) {
		 
			$total_exceptions+=$value['exceptions'];
			$total_credits_deducted+=$value['credits_deducted'];
			$total_leads_deducted+=$value['leads_deducted'];
			$total_revenue+=$value['revenue'];			
	?>
		<tr>
            <td><?php echo $value['shop_name'].' ('.$value['account'].')' ; ?></td>
            <td><?php echo $value['locality'] ; ?></td>
            <td><?php echo $value['model'] ; ?></td>
            <td style="text-align:center;"><a id="exception" href="" data-shop="<?php echo $key; ?>"><?php echo $value['exceptions'] ; ?></a></td>
            <td style="text-align:center;"><?php echo $value['credits_deducted'] ; ?></td>
            <td style="text-align:center;"><?php echo $value['leads_deducted'] ; ?></td>
            <td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" > <?php echo $value['revenue'] ; ?></td>
        </tr>
	<?php  }
?>	
</tbody>
<tbody class="avoid-sort">
<tr>
<td style="text-align:center;" colspan="3">Total</td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_exceptions; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_credits_deducted; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_leads_deducted; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><i class="fa fa-inr" aria-hidden="true" > <?php echo $total_revenue; ?></td>
</tr>
</tbody>
</table>
</div>


<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example1").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[3,3],[0,0]]} );
    }
);
</script>
<!-- table sorter -->

<?php


?>
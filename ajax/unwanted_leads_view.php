<?php
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$src_crm = array('crm016','crm018','crm064','crm036','crm033','crm017');
$src_column = 0;
if(in_array($crm_log_id,$src_crm))
{
	$src_column = 1;
}

$startdate = date('Y-m-d',strtotime($_GET['startdate']));
$enddate =  date('Y-m-d',strtotime($_GET['enddate']));
$source = $_GET['source'];
$city = $_GET['city'];
$cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];

$_SESSION['crm_city'] = $city;
$_SESSION['crm_cluster'] = $cluster;

$cond ='';

$cond = $cond.($source == 'all' ? "" : "AND b.source='$source'");
$cond = $cond.($city == 'all' ? "" : "AND b.city='$city'");
$cond = $cond.($cluster == 'all' ? "" : "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'");

$sql_booking = "SELECT DISTINCT b.booking_id,b.user_id,b.mec_id,b.crm_update_time,b.source,b.vehicle_type,b.user_veh_id,b.service_type,b.service_date,b.shop_name,b.axle_flag,b.locality,b.utm_source,b.priority,b.flag_fo,b.crm_update_id,c.name as crm_name,u.name as user_name,u.mobile_number as user_mobile,u.lat_lng as user_latlng,u.Locality_Home as user_locality,u.user_level,v.brand,v.model FROM user_booking_tb as b LEFT JOIN crm_admin as c ON b.crm_update_id=c.crm_log_id LEFT JOIN user_register as u ON b.user_id=u.reg_id LEFT JOIN user_vehicle_table as v ON b.user_veh_id=v.id LEFT JOIN localities as l ON b.locality=l.localities WHERE b.booking_status='1' AND b.flag!='1' AND b.flag_unwntd = '1' AND DATE(b.crm_update_time) BETWEEN '$startdate' and '$enddate' {$cond} ORDER BY b.crm_update_time DESC";
$res_booking = mysqli_query($conn,$sql_booking);
 
$no = 0;

$count = mysqli_num_rows($res_booking);
if($count >0){
while($row_booking = mysqli_fetch_object($res_booking)){
$booking_id = $row_booking->booking_id;
$user_id = $row_booking->user_id;
$mec_id = $row_booking->mec_id;
$log = $row_booking->crm_update_time;
$bsource = $row_booking->source;
$veh_type = $row_booking->vehicle_type;
$veh_id = $row_booking->user_veh_id;
$service_type = $row_booking->service_type;
$service_date = $row_booking->service_date;
$shop_name = $row_booking->shop_name;
$axle_flag = $row_booking->axle_flag;
$locality = $row_booking->locality;
$utm_source = $row_booking->utm_source;
$priority = $row_booking->priority;
$flag_fo = $row_booking->flag_fo;
$alloted_to_id = $row_booking->crm_update_id;
$alloted_to = $row_booking->crm_name;
$user_name = $row_booking->user_name;
$user_mobile = $row_booking->user_mobile;
$address = $row_booking->user_lat_lng;
$home = $row_booking->user_locality;
$user_level = $row_booking->user_level;
$veh_brand = $row_booking->brand;
$veh_model = $row_booking->model;


// go axle status
$sql_axle = mysqli_query($conn2,"SELECT max(b.b2b_booking_id) as b2b_booking_id,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b LEFT JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.gb_booking_id='$booking_id'");
$row_axle= mysqli_fetch_object($sql_axle);
$bid=$row_axle->b2b_booking_id;
$accept = $row_axle->b2b_acpt_flag;
$deny = $row_axle->b2b_deny_flag;

if($alloted_to_id =='' || $alloted_to_id==' ' || $alloted_to_id=='0'){
  $tr = '<tr style="background-color:#EF5350 !important;">';
}
else{
  $tr = '<tr>';
}

$td1 = '<td>]'.$booking_id.'</td>';

$td2 = '<td>';
switch($service_type){
      case 'general_service' :
      if($veh_type =='2w'){
        $td2 = $td2."General Service";
      }
      else{
        $td2 = $td2."Car service and repair";
      }
      break;
      case 'break_down':  $td2 = $td2."Breakdown Assistance"; break;
      case 'tyre_puncher': $td2 = $td2."Tyre Puncture";  break;
      case 'other_service': $td2 = $td2."Repair Job"; break;
      case 'car_wash':
      if($veh_type == '2w'){
        $td2 = $td2."water Wash";
      }
      else{
        $td2 = $td2."Car wash exterior";
      }break;
      case 'engine_oil': $td2 = $td2."Repair Job"; break;
      case 'free_service': $td2 = $td2."General Service"; break;
      case 'car_wash_both':
      case 'completecarspa': $td2 = $td2."Complete Car Spa"; break;
      case 'car_wash_ext': $td2 = $td2."Car wash exterior"; break;
      case 'car_wash_int': $td2 = $td2."Interior Detailing"; break;
      case 'Doorstep car spa': $td2 = $td2."Doorstep Car Spa"; break;
      case 'Doorstep_car_wash': $td2 = $td2."Doorstep Car Wash"; break;
      case 'free_diagnastic':
      case 'free_diagnostic':
      case 'diagnostics':
      if($veh_type == '2w'){
        $td2 = $td2."Diagnostics/Check-up";
     }
     else{
       $td2 = $td2."Vehicle Diagnostics";
     }
         break;
      case 'water_wash': $td2 = $td2."water Wash"; break;
      case 'exteriorfoamwash': $td2 = $td2."Car wash exterior"; break;
      case 'interiordetailing': $td2 = $td2."Interior Detailing"; break;
      case 'rubbingpolish': $td2 = $td2."Car Polish"; break;
      case 'underchassisrustcoating': $td2 = $td2."Underchassis Rust Coating"; break;
      case 'headlamprestoration': $td2 = $td2."Headlamp Restoration"; break;
      default: $td2 = $td2.$service_type;
}  
$td2 = $td2.'</td>';

$td3 = '<td><a href="user_details.php?t='.base64_encode("unwanted").'&v58i4='.base64_encode($veh_id).'&bi='.base64_encode($booking_id).'&tt='.base64_encode("u").'"><i class="fa fa-eye" aria-hidden="true"></i></td>';
$td4 = '<td>'.$user_name;
switch($user_level){
  case '5': $td4 = $td4.'<p style="font-size:16px;color:green;padding:5px;" title="Level 5 User"><i class="fa fa-angellist" aria-hidden="true"></i></p>';
  break;
  case '4': $td4 = $td4.'<p style="font-size:16px;color:red;padding:5px;" title="Level 4 User"><i class="fa fa-bug" aria-hidden="true"></i></p>';
  break;
  case '3': $td4 = $td4.'<p style="font-size:16px;color:#26269e;padding:5px;" title="Level 3 User"><i class="fa fa-handshake-o" aria-hidden="true"></i></p>';
  break;
  case '2': $td4 = $td4.'<p style="font-size:16px;color:brown;padding:5px;" title="Level 2 User"><i class="fa fa-anchor" aria-hidden="true"></i></p>';
  break;
  case '1': $td4 = $td4.'<p style="font-size:16px;color:orange;padding:5px;" title="Level 1 User"><i class="fa fa-tripadvisor" aria-hidden="true"></i></p>';
  break;
  default:
}
$td4 = $td4.'</td>';
$td5 = '<td>'.$user_mobile.'</td>';
$td6 = '<td>';
if($bsource == "Hub Booking"){ 
  if($locality!=''){ 
    $td6 = $td6.$locality; 
  } 
  else if($home == ''){ 
    $td6 = $td6.$address; 
  } 
  else { 
    $td6 = $td6.$home; 
  } 
} 
else if($bsource == "GoBumpr App" || $bsource == "App"){ 
  $td6 = $td6.$address; 
} 
else{  
  $td6 = $td6.$locality; 
}
$td6 = $td6.'</td>';

$td7 = '<td>'.$veh_brand." ".$veh_model.'</td>';
$td8 = '<td>'.$shop_name.'</td>';
$td9 = '<td>';
if($service_date == "0000-00-00"){ 
  $td9 = $td9."Not Updated Yet"; 
} else {  $td9 = $td9.date('d M Y', strtotime($service_date)); }
$td9 = $td9.'</td>';
if($src_column==1){
		 $td9 = $td9."<td>$bsource</td><td>";
		 if($utm_source == ''){ $td9 = $td9."NA";  } else{ $td9 = $td9.$utm_source; }
		 $td9 = $td9."</td>";
}

$td10 = '<td>'.date('d M Y h.i A', strtotime($log)).'</td>';
$tr_l ='</tr>';
  
$str = $tr.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$td9.$td10.$tr_l;
$data[] = array('tr'=>$str);
}
echo $data1 = json_encode($data);
} // if
else {
  echo "no";
}
 ?>

<?php
// error_reporting(E_ALL); ini_set('display_errors', 1);
include("../config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();
$today=date('Y-m-d');

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));

$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$vehicle = $_POST['vehicle_type'];
$city = $_POST['city'];

$_SESSION['crm_city'] = $city;
$count=1;
$total_revenue=0;
$total_gmv=0;
$cond1= ($vehicle=='all')?"" : "AND b.vehicle_type = '$vehicle'";
$cond2= $city == 'all' ? "" : "AND b.city ='$city'";
?>

<div id = "table" style="margin-left:20px;margin-right:20px;">
<table id="example1" class="table table-striped table-bordered tablesorter table-hover results">
<thead style="background-color:#B2DFDB;">
<th style="text-align:center;">NO</th>
<th style="text-align:center;">Booking ID</th>
<th style="text-align:center;">Customer ID</th>
<th style="text-align:center;">Customer Name</th>
<th style="text-align:center;">Vehicle Type</th>
<th style="text-align:center;">Service Type</th>
<th style="text-align:center;">Service Center</th>
<th style="text-align:center;">City</th>
<th style="text-align:center;">Business Model</th>
<th style="text-align:center;">Status</th>
<th style="text-align:center;">Goaxle Log</th>
<th style="text-align:center;">Booking Log</th>
<th style="text-align:center;">Exception Log</th>
<th style="text-align:center;">Actual GMV</th>
<th style="text-align:center;">Revenue<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
</thead>
<tbody id="tbody">

<?php 
	

	$sql_goaxle="SELECT
    DISTINCT bb.b2b_booking_id,
	b.user_id,
    bb.gb_booking_id AS booking_id,
    bb.b2b_shop_id,
    bb.b2b_log AS goaxle_log,    
    b.service_type,
    b.vehicle_type,
    bb.brand,
	g.model AS business_model,
    g.lead_price,
    g.re_lead_price,
    g.nre_lead_price,
    g.amount,
    g.premium_tenure,
    g.start_date,
    g.end_date,
    b.log AS booking_log,
    bb.b2b_credit_amt,
    bb.b2b_swap_flag,
    bb.b2b_customer_name,
    b.shop_name,
    b.city,
	b.final_bill_amt as final_amt,
	a.price,
	bb.b2b_bill_amount as b2b_amt
FROM
    b2b.b2b_booking_tbl bb
        LEFT JOIN
    user_booking_tb b ON bb.gb_booking_id = b.booking_id
        LEFT JOIN
	garage_model_history g ON g.b2b_shop_id = bb.b2b_shop_id
        AND DATE(bb.b2b_log) >= DATE(g.start_date)
        AND g.id=(select id from go_bumpr.garage_model_history where b2b_shop_id=bb.b2b_shop_id AND DATE(bb.b2b_log) >= DATE(start_date) order by start_date desc limit 1) 
        LEFT JOIN
	b2b.b2b_status s ON s.b2b_booking_id = bb.b2b_booking_id
		LEFT JOIN
    go_axle_service_price_tbl a ON a.service_type = b.service_type
        AND a.type = b.vehicle_type AND a.amt=bb.b2b_credit_amt
        AND a.price=(SELECT MAX(price) FROM go_bumpr.go_axle_service_price_tbl WHERE service_type=b.service_type AND type=b.vehicle_type AND amt = bb.b2b_credit_amt)

WHERE
    (DATE(bb.b2b_log) BETWEEN '$startdate' AND '$enddate')
        AND bb.b2b_shop_id NOT IN (1014 , 1035, 1670, 1673)
        AND b.user_id NOT IN (21816 , 41317,
        859,
        3132,
        20666,
        56511,
        2792,
        128,
        19,
        7176,
        19470,
        1,
        951,
        103699,
        113453,
        108783)
        AND b.service_type NOT IN ('GoBumpr Tyre Fest')
        {$cond1} {$cond2}      
        
        AND s.b2b_acpt_flag = 1";
	$res_goaxle = mysqli_query($conn1,$sql_goaxle);
	// echo $sql_goaxle;die;
	while($row_goaxle = mysqli_fetch_object($res_goaxle))
	{
		
		
		$veh_type=$row_goaxle->vehicle_type;
		$brand=$row_goaxle->brand;
		$booking_id=$row_goaxle->booking_id;
		$booking_log=$row_goaxle->booking_log;
		$goaxle_log=$row_goaxle->goaxle_log;		
		$business_model = trim($row_goaxle->business_model);
		$revenue=0;
		$credit_amt=$row_goaxle->b2b_credit_amt;
		$lead_price = $row_goaxle->lead_price;
		$re_lead_price = $row_goaxle->re_lead_price;
		$nre_lead_price = $row_goaxle->nre_lead_price;
		// $exception_log=$row_goaxle->exception_log;
		// echo $exception_log;
		$locality = $row_goaxle->b2b_address4;
		$shop_name=$row_goaxle->shop_name;
		$service_type=$row_goaxle->service_type;		
		$s_city=$row_goaxle->city;
		$shop_id=$row_goaxle->b2b_shop_id;
		$customer=$row_goaxle->b2b_customer_name;
		$shop_name=$row_goaxle->shop_name;
		$swap_flag=$row_goaxle->b2b_swap_flag;
		$bill=($row_goaxle->final_amt=="")?$row_goaxle->b2b_amt:$row_goaxle->final_amt;
		if($swap_flag!=1){
			$status="Lead";
			if($bill!=""){
				$gmv=$bill;
			}else{
				$gmv=$row_goaxle->price;
			}
		}else{
			$status="Swapped";
			$gmv=0;
		}
		if($business_model=="Pre Credit"){
		 	$credits = $row_goaxle->b2b_credit_amt;
		 	
			$revenue = $credits;
			
		}else if($business_model=="Premium 1.0"){
			$amount = $row_goaxle->amount;
			$premium_tenure = $row_goaxle->premium_tenure;
			
			$revenue=round(($amount/$premium_tenure)/25);
		}else if($business_model=="Premium 2.0"){
			if($veh_type=="4w"){
				$revenue = $lead_price;
			}else{
				if($brand=="Royal Enfield"){
					$revenue=$lead_price;
				}else{
					$revenue=$nre_lead_price;
				}
			}
			
			
		}else if($business_model=="Leads 3.0"){
						
			if($veh_type=="4w"){
				$revenue = $lead_price;
			}else{
				if($brand=="Royal Enfield"){
					$revenue=($re_lead_price!=0)?$re_lead_price:$lead_price;
				}else{
					$revenue=($nre_lead_price!=0)?$nre_lead_price:$lead_price;
				}
			}
			
		}else{
			$credits = $row_goaxle->b2b_credit_amt;
			$revenue = $credits;
		 	
		}	
		$total_revenue+=$revenue;
		$total_gmv+=$gmv;
	?>
		<tr>
            <td><?php echo $count++ ; ?></td>
            <td><?php echo $booking_id; ?></td>
			<td><?php echo $row_goaxle->user_id; ?></td>
            <td><?php echo $customer; ?></td>
            <td><?php echo $veh_type; ?></td>
            <td><?php echo $service_type; ?></td>
            <td><?php echo $shop_name; ?></td>
            <td><?php echo $s_city; ?></td>            
            <td><?php echo ($business_model=="")?"Pre Credit":$business_model; ?></td>
            <td><?php echo $status; ?></td>
            <td><?php echo $goaxle_log; ?></td>
            <td><?php echo $booking_log; ?></td>
            <td><?php echo ""; ?></td>
			<td><i class="fa fa-inr" aria-hidden="true" > <?php echo $gmv; ?></td>
            <td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" > <?php echo $revenue ; ?></td>
        </tr>
	<?php  }

	$ebm_query="SELECT 
		DISTINCT e.booking_id as booking_id,
		b.user_id,
		e.b2b_shop_id,
	    b.service_type,
	    b.vehicle_type,
	    bb.brand,
		g.model AS business_model,
	    g.lead_price,
	    g.re_lead_price,
	    g.nre_lead_price,
	    g.amount,
	    g.premium_tenure,
	    g.start_date,
	    g.end_date,
	    bb.b2b_credit_amt,
	    e.exception_log,
	    b.shop_name,
	    bb.b2b_log AS goaxle_log,
		b.log AS booking_log,
		bb.b2b_customer_name,
		b.city
	FROM
	    exception_mechanism_track e 
	    LEFT JOIN
	    b2b.b2b_booking_tbl bb ON bb.gb_booking_id=e.booking_id AND bb.b2b_shop_id=e.b2b_shop_id
	    LEFT JOIN
	    user_booking_tb b ON e.booking_id = b.booking_id
	    LEFT JOIN 
	    garage_model_history g ON e.b2b_shop_id = g.b2b_shop_id
	        AND DATE(e.exception_log) >= DATE(g.start_date)
	        AND g.id=(select id from go_bumpr.garage_model_history where b2b_shop_id=e.b2b_shop_id AND DATE(e.exception_log) >= DATE(start_date) order by start_date desc limit 1) 
	WHERE
	    (DATE(e.exception_log) BETWEEN '$startdate' AND '$enddate') AND e.flag=0 {$cond1} {$cond2} GROUP BY booking_id";
	    // echo $ebm_query;die;
	$res=mysqli_query($conn1,$ebm_query);
	while($ebm_row=mysqli_fetch_object($res)){
		$veh_type=$ebm_row->vehicle_type;
		$brand=$ebm_row->brand;
		$booking_id=$ebm_row->booking_id;
		$booking_log=$ebm_row->booking_log;
		$goaxle_log=$ebm_row->goaxle_log;
		$exception_log=$ebm_row->exception_log;
		$business_model = trim($ebm_row->business_model);
		$revenue=0;
		$credit_amt=$ebm_row->b2b_credit_amt;
		$lead_price = $ebm_row->lead_price;
		$re_lead_price = $ebm_row->re_lead_price;
		$nre_lead_price = $ebm_row->nre_lead_price;
		$shop_name=$ebm_row->shop_name;
		$service_type=$ebm_row->service_type;		
		$s_city=$ebm_row->city;
		$shop_id=$ebm_row->b2b_shop_id;
		$customer=$ebm_row->b2b_customer_name;
		$shop_name=$ebm_row->shop_name;
		$status="Exception";
		if($business_model=="Pre Credit"){
		 	$credits = $ebm_row->b2b_credit_amt;		 	
			$revenue = $credits;			
		}else if($business_model=="Premium 1.0"){
			$amount = $ebm_row->amount;
			$premium_tenure = $ebm_row->premium_tenure;			
			$revenue=round(($amount/$premium_tenure)/25);
		}else if($business_model=="Premium 2.0"){
			if($veh_type=="4w"){
				$revenue = 2*$lead_price;
			}else{
				if($brand=="Royal Enfield"){
					$revenue=2*$lead_price;
				}else{
					$revenue=2*$nre_lead_price;
				}
			}
			
			
		}else if($business_model=="Leads 3.0"){
						
			if($veh_type=="4w"){
				$revenue = $lead_price;
			}else{
				if($brand=="Royal Enfield"){
					$revenue=($re_lead_price!=0)?$re_lead_price:$lead_price;
				}else{
					$revenue=($nre_lead_price!=0)?$nre_lead_price:$lead_price;
				}
			}
			
		}else{
			$credits = $ebm_row->b2b_credit_amt;
			$revenue = $credits;
		 	
		}	
		$total_revenue+=$revenue;
	?>
		<tr>
            <td><?php echo $count++ ; ?></td>
            <td><?php echo $booking_id; ?></td>
			<td><?php echo $ebm_row->user_id; ?></td>
            <td><?php echo $customer; ?></td>
            <td><?php echo $veh_type; ?></td>
            <td><?php echo $service_type; ?></td>
            <td><?php echo $shop_name; ?></td>
            <td><?php echo $s_city; ?></td>            
            <td><?php echo ($business_model=="")?"Pre Credit":$business_model; ?></td>
            <td><?php echo $status; ?></td>
            <td><?php echo $goaxle_log; ?></td>
            <td><?php echo $booking_log; ?></td>
            <td><?php echo $exception_log; ?></td>
            <td><i class="fa fa-inr" aria-hidden="true" > <?php echo 0; ?></td>
            <td style="text-align:center;"><i class="fa fa-inr" aria-hidden="true" > <?php echo $revenue ; ?></td>
        </tr>
	<?php  }
?>	
</tbody>
<tbody class="avoid-sort">
<tr>
<td style="text-align:center;" colspan="13">Total</td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><i class="fa fa-inr" aria-hidden="true" > <?php echo $total_gmv; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><i class="fa fa-inr" aria-hidden="true" > <?php echo $total_revenue; ?></td>
</tr>
</tbody>
</table>
</div>


<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example1").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[0,0]]} );
		wb = XLSX.utils.table_to_book(document.getElementById('example1'), {sheet:"Sheet JS",dateNF:"YYYY-MM-DD hh:mm:ss"});
		if(!wb['!cols']) wb['!cols'] = [];
		wb['!cols'][10] = { wch: 19};
		wb['!cols'][11] = { wch: 19};
		wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});
    }
);
</script>
<!-- table sorter -->

<?php


?>
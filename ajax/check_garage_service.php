<?php
include("../config.php");

$conn = db_connect1();
$conn2 = db_connect2();
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$flag = $_SESSION['flag'];
$super_flag = $_SESSION['super_flag'];

$src_crm = array('crm016', 'crm017', 'crm064', 'crm036', 'crm033', 'crm034', 'crm029');
$src_column = 0;
if (in_array($crm_log_id, $src_crm)) {
	$src_column = 1;
}

$crm_city = $_SESSION['cc'];

$garage_id = $_POST['garage_id'];
$service_type = $_POST['service_type'];

//check if garage or pickup location is empty
if (!$garage_id || !$service_type) return false;


//Get Pickup Location Lat, Lng
$query1 = "select st.service_type_column from go_bumpr.go_axle_service_price_tbl as st where st.service_type= '$service_type'";
$query_result1 = mysqli_query($conn, $query1);

$available_services = [];
while ($row = mysqli_fetch_object($query_result1)) {
	array_push($available_services, $row->service_type_column);
}
// print_r($available_services);

$selectArray = [];

foreach ($available_services as $available_service) {
	$query2 = "SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = 'go_bumpr' AND TABLE_NAME ='admin_service_type' AND COLUMN_NAME = '$available_service'";
	$query_result2 = mysqli_query($conn, $query2);
	
	if ($query_result2->num_rows) {
		array_push($selectArray, $available_service);
	}
}
if (count($selectArray) > 0) {
	
	$select = implode($selectArray, ',');
	
	$where = implode($selectArray, ' = 1 or ');;
	
	$query3 = "select $select from go_bumpr.admin_service_type where mec_id = $garage_id and ({$where})";
	$query_result3 = mysqli_query($conn, $query3);
	$row3 = mysqli_fetch_row($query_result3);
	
	if ($row3) {
		echo json_encode(['success' => true, 'service' => $service_type . ' Available']);
	} else {
		echo json_encode(['success' => true, 'service' => $service_type . ' Not Available']);
	}
}


// $row = mysqli_fetch_row($query_result2);
// 	print_r($query2);
// $query1 = "select  from go_bumpr.admin_service_type as st where st.mec_id= '$garage_id'";
// $query_result1 = mysqli_query($conn, $query1);


<?php
include("../config.php");
$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

 $startdate = date('Y-m-d',strtotime($_POST['startdate']));
 $enddate =  date('Y-m-d',strtotime($_POST['enddate']));
 $service_type = $_POST['service_type'];
 $vehicle = $_POST['vehicle'];
 $booking_status = $_POST['status'];

class overall {
    private $vehicle;
    private $service_type;
    private $booking_status;

    function __construct($person, $vehicle, $service_type,$booking_status){
        $this->vehicle = $vehicle;
        $this->service_type = $service_type;
        $this->status = $booking_status;
    }

    function v0_s0_st0() {
      return "";
    }
    function v0_s0_st1() {
      $ss = $this->status;
      if($ss == 'c'){
        return "AND flag='1'";
      }
      else{
        return "AND booking_status='$ss' AND flag!='1'";
      }
    }
    function v0_s1_st0() {
        return "AND service_type='$this->service_type'";
    }
    function v0_s1_st1() {
        $ss = $this->status;
        if($ss == 'c'){
          return "AND service_type='$this->service_type' AND flag='1'";
        }
        else{
          return "AND service_type='$this->service_type' AND booking_status='$ss' AND flag!='1' ";
        }    
    }
    function v1_s0_st0() {
        return "AND vehicle_type='$this->vehicle' ";
    }
    function v1_s0_st1() {
        $ss = $this->status;
        if($ss == 'c'){
          return "AND vehicle_type='$this->vehicle' AND flag='1'";
        }
        else{
          return "AND vehicle_type='$this->vehicle' AND booking_status='$ss' AND flag!='1'";
        }    
    }
    function v1_s1_st0() {
        return "AND vehicle_type='$this->vehicle' AND service_type='$this->service_type'";
    }
    function v1_s1_st1() {
        $ss = $this->status;
        if($ss == 'c'){
          return "AND vehicle_type='$this->vehicle' AND service_type='$this->service_type' AND flag='1'";
        }
        else{
          return "AND vehicle_type='$this->vehicle' AND service_type='$this->service_type' AND booking_status='$ss' AND flag!='1'";
        }    
    }
}

$vehicle_fun = $vehicle=='all' ? "" : $vehicle;
$service_type_fun = $service_type=='all' ? "" : $service_type;
$booking_status_fun = $booking_status=='all'  ? "" : $booking_status;


$overall_obj = new overall($person_fun, $vehicle_fun, $service_type_fun, $booking_status_fun);


$vehicle_val = $vehicle=='all' ? "0" : "1";
$service_type_val = $service_type=='all' ? "0" : "1";
$booking_status_val = $booking_status=='all' ? "0" : "1";


$cond = $overall_obj->{"v{$vehicle_val}_p{$person_val}_s{$service_type_val}_st{$booking_status_val}"}();

$sql_booking = "SELECT booking_id,user_id,mec_id,service_type,vehicle_type,user_veh_id,service_date,shop_name,axle_flag,crm_update_id,locality,utm_source,booking_status,priority,flag_fo,flag,flag_unwntd,case flag_fo when '1' then crm_update_time when '0' then log end as log_calc FROM user_booking_tb HAVING DATE(log_calc) BETWEEN '$startdate' and '$enddate' {$cond} ORDER BY log DESC";

$res_booking = mysqli_query($conn,$sql_booking);

$no = 0;

$count = mysqli_num_rows($res_booking);
if($count >=1){

while($row_booking = mysqli_fetch_object($res_booking)){
$booking_id = $row_booking->booking_id;
$user_id = $row_booking->user_id;
$mec_id = $row_booking->mec_id;
$log = $row_booking->log_calc;
$bsource = $row_booking->source;
$veh_type = $row_booking->vehicle_type;
$veh_id = $row_booking->user_veh_id;
$service_type = $row_booking->service_type;
$service_date = $row_booking->service_date;
$shop_name = $row_booking->shop_name;
$axle_flag = $row_booking->axle_flag;
$alloted_to_id = $row_booking->crm_update_id;
$locality = $row_booking->locality;
$status = $row_booking->booking_status;
$utm_source = $row_booking->utm_source;
$priority = $row_booking->priority;
$flag_fo = $row_booking->flag_fo;
$bflag = $row_booking->flag;
$unwantedflag = $row_booking->flag_unwntd;


   // get crm perdon name from crm admin table  
    $sql_alloted_person = "SELECT crm_log_id,name FROM crm_admin WHERE crm_log_id='$alloted_to_id' ";
    $query_alloted_person = mysqli_query($conn,$sql_alloted_person);
    $row_alloted_person = mysqli_fetch_object($query_alloted_person);
$alloted_to = $row_alloted_person->name;

// get user details from user register table using the user id from booking table  
$sql_user = "SELECT name,mobile_number,lat_lng,Locality_Home FROM user_register WHERE reg_id = '$user_id' ";
$res_user = mysqli_query($conn,$sql_user);
$row_user = mysqli_fetch_object($res_user);

$user_name = $row_user->name;
$user_mobile = $row_user->mobile_number;
$address = $row_user->lat_lng;
$home = $row_user->Locality_Home;

    if($veh_id!='' || $veh_id!='0'){
        $veh_query = "SELECT brand, model FROM user_vehicle_table WHERE id = '$veh_id'";
        $veh_res = mysqli_query($conn, $veh_query);
        $veh_row = mysqli_fetch_object($veh_res);
        $veh_brand = $veh_row->brand;
        $veh_model = $veh_row->model;
    }

// go axle status
$sql_axle = mysqli_query($conn2,"SELECT max(b2b_booking_id) as b2b_booking_id  FROM b2b_booking_tbl WHERE gb_booking_id='$booking_id'");
$row_axle= mysqli_fetch_object($sql_axle);
$bid=$row_axle->b2b_booking_id;

$sql_b2b_status = mysqli_query($conn2,"SELECT b2b_acpt_flag,b2b_deny_flag,b2b_acpt_log FROM b2b_status WHERE b2b_booking_id='$bid'");
$row_b2b_status = mysqli_fetch_object($sql_b2b_status);
$accept = $row_b2b_status->b2b_acpt_flag;
$deny = $row_b2b_status->b2b_deny_flag;

?>

  <tr>
    <td><?php echo $no=$no+1 ; ?></td>
    <td><p style="float:left;padding:10px;"><?php echo $booking_id; ?></p>
        <?php
        if($flag_fo == '1'){
          ?>
    <p class="p" style="font-size:16px;color:#D81B60;float:left;padding:5px;" title="Resheduled Booking!"><i class="fa fa-recycle" aria-hidden="true"></i></p>
    <?php
    switch($priority){
          case '1': ?>
    <p style="font-size:16px;color:#00ACC1;float:left;padding:5px;" title="Low Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>
    <?php   break;
          case '2':?>
    <p style="font-size:16px;color:#FFB300;float:left;padding:5px;" title="Medium Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>
    <?php   break;
          case '3':?>
    <p style="font-size:16px;color:#E53935;float:left;padding:5px;" title="High Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>
    <?php   break;
          default:?>
    <p style="font-size:16px;color:#757575;float:left;padding:5px;" title="Priority Not available!"><i class="fa fa-eercast" aria-hidden="true"></i></p>
    <?php
        }
        }
        ?>
    </td>
    <td><?php
        if($bflag == '1'){
          echo "Cancelled";
        }
        else if($unwantedflag == '1'){
          echo "UnWanted";
        }
        else{
          switch($status){
          case '1':echo "Lead"; break;
          case '2':echo "Booking"; break;
          case '3':echo "Followup"; break;
          case '4':echo "RNR1"; break;
          case '5':echo "RNR2"; break;
          default:echo "Other";
          }
        } 
    ?>
    </td>   
    <td><div class="row"><?php
    if($axle_flag!='1'){
        ?>
    <p style="background-color:#FFA800;padding:7px;width:30px;border-radius:18px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>
     <?php
     }
    else if($accept == 1 && $deny == 0 ){ ?>
    <p style="background-color:#6FA3DE;padding:7px;width:30px;border-radius:18px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>
    <?php }
    else if($accept==0 && $deny==1){ ?>
    <p style="background-color:#D25F34;padding:7px;width:30px;border-radius:18px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>
    <?php }
    else if($accept==0 && $deny==0){ ?>
    <p style="background-color:#69ED85;padding:7px;width:30px;border-radius:18px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>
    <?php } ?></div></td>

    <td><?php
    switch($service_type){
      case 'general_service' :
      if($veh_type =='2w'){
        echo "General Service";
      }
      else{
        echo "Car service and repair";
      }
      break;
      case 'break_down':  echo "Breakdown Assistance"; break;
      case 'tyre_puncher': echo "Tyre Puncture";  break;
      case 'other_service': echo "Repair Job"; break;
      case 'car_wash':
      if($veh_type == '2w'){
        echo "water Wash";
      }
      else{
        echo "Car wash exterior";
      }break;
      case 'engine_oil': echo "Repair Job"; break;
      case 'free_service': echo "General Service"; break;
      case 'car_wash_both':
      case 'completecarspa': echo "Complete Car Spa"; break;
      case 'car_wash_ext': echo "Car wash exterior"; break;
      case 'car_wash_int': echo "Interior Detailing"; break;
      case 'Doorstep car spa': echo "Doorstep Car Spa"; break;
      case 'Doorstep_car_wash': echo "Doorstep Car Wash"; break;
      case 'free_diagnastic':
      case 'free_diagnostic':
      case 'diagnostics':
      if($veh_type == '2w'){
        echo "Diagnostics/Check-up";
     }
     else{
       echo "Vehicle Diagnostics";
     }
         break;
      case 'water_wash': echo "water Wash"; break;
      case 'exteriorfoamwash': echo "Car wash exterior"; break;
      case 'interiordetailing': echo "Interior Detailing"; break;
      case 'rubbingpolish': echo "Car Polish"; break;
      case 'underchassisrustcoating': echo "Underchassis Rust Coating"; break;
      case 'headlamprestoration': echo "Headlamp Restoration"; break;
      default: echo $service_type;
      }  ?>
     </td>
     <td><?php echo $user_name; ?></td>
     <td><?php echo $user_mobile; ?></td>
     <td><?php if($bsource == "Hub Booking"){ if($locality!=''){ echo $locality; } else if($home == ''){ echo $address; } else { echo $home; } } else if($bsource == "GoBumpr App" || $bsource == "App"){ echo $address; } else{  echo $locality; } ?></td>
     <td><?php echo $veh_brand." ".$veh_model; ?></td>
     <td><?php echo $shop_name; ?></td>
     <td><?php if($service_date == "0000-00-00"){ echo "Not Updated Yet"; } else { echo date('d M Y', strtotime($service_date)); } ?></td>
     <td><?php echo $alloted_to; ?></td>
     <td><?php echo $bsource; ?></td>
    <td><?php if($utm_source == ''){ echo "NA";  } else{ echo $utm_source; } ?></td>
    <td><?php echo date('d M Y h.i A', strtotime($log)); ?></td>
  </tr>
  <?php
}
} // if

else {
  //echo $sql_booking;
  echo "no";
}
 ?>

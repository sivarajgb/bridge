<?php

include("../config.php");
$conn = db_connect3();
$conn1 = db_connect1();
session_start();
$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];

$veh_type = $_POST['veh'];
$city = $_POST['city'];
$loc = $_POST['loc'];

$_SESSION['crm_city'] = $city;
error_reporting(E_ALL);
//ini_set('display_errors', 1);

//0-all && 1-particular
class shops {
  private $veh;
  private $loc;

  function __construct($veh, $loc){
    $this->veh = $veh;
    $this->loc = $loc;
  }
  function v0_l0(){
    return "";
  }
  function v0_l1(){
    return "AND m.b2b_address4 ='$this->loc'";
  }
  function v1_l0(){
    return "AND m.b2b_vehicle_type = '$this->veh'";
  }
  function v1_l1(){
    return "AND m.b2b_vehicle_type = '$this->veh' AND m.b2b_address4 ='$this->loc'";
  }
}
$shops_obj = new shops($veh_type, $loc);
$i=0;
$veh_val = $veh_type=="all" ? "0" : "1";
$loc_val = $loc=="" ? "0" : "1";

$cond = $shops_obj->{"v{$veh_val}_l{$loc_val}"}();

$sql_shops="SELECT m.b2b_shop_id,m.b2b_shop_name,m.gobumpr_id,c.b2b_leads,g.start_date,g.leads,g.re_leads,g.nre_leads,c.b2b_re_leads,c.b2b_non_re_leads FROM b2b_mec_tbl AS m INNER JOIN go_bumpr.garage_model_history AS g ON m.b2b_shop_id = g.b2b_shop_id Inner JOIN b2b_credits_tbl as c ON m.b2b_shop_id = c.b2b_shop_id WHERE m.b2b_address5 = '$city' AND g.end_date = '0000-00-00' AND g.model = 'Leads 3.0' {$cond} ";
$res_shops = mysqli_query($conn,$sql_shops);
$count = mysqli_num_rows($res_shops);
if($count >0){

  $sql_counts="SELECT b.b2b_shop_id,DATE(b.b2b_log)as b2b_log,COUNT(*) AS vehicles_sent,SUM(g.exception_flag) AS exception_completed,SUM(CASE WHEN (b.brand = 'Royal Enfield') THEN 1 ELSE 0 END) AS re_sent,SUM(IF(g.exception_revoke != 1 AND ((g.service_status = 'Completed' OR g.service_status = 'In Progress') OR (b.b2b_check_in_report = '1')),1,0)) AS completed_count FROM b2b_booking_tbl AS b INNER JOIN b2b_status AS s ON b.b2b_booking_id = s.b2b_booking_id INNER JOIN go_bumpr.user_booking_tb AS g ON b.gb_booking_id = g.booking_id WHERE s.b2b_acpt_flag = '1' AND b.b2b_swap_flag != '1' group by b.b2b_booking_id";
    $res_counts = mysqli_query($conn,$sql_counts);
    while($row_counts = mysqli_fetch_object($res_counts)){
      $b2b_shop_id[]=$row_counts->b2b_shop_id;
      $b2b_log[]=$row_counts->b2b_log;
      $vehicles_sent[]=$row_counts->vehicles_sent;
      $completed_count[]=$row_counts->completed_count;
      $re_sent[]=$row_counts->re_sent;
      $exception[]=$row_counts->exception_completed;
    }
$no = 0;


  while($row_shops = mysqli_fetch_object($res_shops)){
    $shop_id = $row_shops->b2b_shop_id;
    $shop_name = $row_shops->b2b_shop_name;
    $gbpr_mec_id = $row_shops->gobumpr_id;
    $leads = $row_shops->b2b_leads;
    $start_date=$row_shops->start_date;
    $promised_leads=$row_shops->leads;
    $promised_re_leads=$row_shops->re_leads;
    $promised_nre_leads=$row_shops->nre_leads;
	$remaning_re_leads=$row_shops->b2b_re_leads;
	$remaning_nre_leads=$row_shops->b2b_non_re_leads;

      $count_leads_sent=0;
      $completed=0;
      $re_count_leads_sent=0;
      $exception_completed=0;
      for($i=0;$i<sizeof($b2b_shop_id);$i++) {
          if($b2b_shop_id[$i]==$shop_id && strtotime($b2b_log[$i]) >= strtotime($start_date)){
            $count_leads_sent+=1;
            $exception_completed+=$exception[$i];
            $re_count_leads_sent+=$re_sent[$i];
            $completed+=$completed_count[$i];
          }
      }
      $nre_count_leads_sent=$count_leads_sent-$re_count_leads_sent;    
    
        //$delta=$promised_leads-$count_leads_sent;
        $re_delta=$promised_re_leads-$re_count_leads_sent;
        $nre_delta=$promised_nre_leads-$nre_count_leads_sent;
        $delta=$re_delta+$nre_delta;
        $end_rate = $count_leads_sent!= '0' ? round(($completed/$count_leads_sent)*100,1) : '0';
        $x=$re_count_leads_sent+$nre_count_leads_sent;
        $end_rate_2w = $x!= '0' ? round(($completed/$x)*100,1) : '0';
      
      $red='style="background-color:#ff2323"';
      if($veh_type=="2w"){ ?>
        <tr>
          <td><?php echo $no=$no+1 ; ?></td>
           <td title="<?php echo $shop_name; ?>"><?php echo $shop_name; echo "(RE:".$remaning_re_leads."/ NRE:".$remaning_nre_leads.")"; ?></td>
          <!-- <td><?php //echo $credits; ?></td> -->
          <td><?php echo $start_date; ?></td>
          <td><?php if($promised_re_leads!=""){echo $promised_re_leads;}else{echo "0";} ?></td>
          <td><?php if($promised_nre_leads!=""){echo $promised_nre_leads;}else{echo "0";} ?></td>
          <td><?php 
            if($promised_re_leads=="0"&&$promised_nre_leads=="0"){
              echo $promised_leads;
            }else{
              echo $promised_re_leads+$promised_nre_leads;
            } 
          ?></td>
          <td><?php echo $re_count_leads_sent; ?></td>
          <td><?php echo $nre_count_leads_sent; ?></td>
          <td><?php if($completed!=""){ echo $completed;}else{echo "0";} ?></td>
          <td><?php echo $exception_completed; ?></td>
          <td <?php switch(true){
                      case ($end_rate_2w >= 70): echo "style='background-color:#00ff4e70'"; break;
                      case ($end_rate_2w <70 && $end_rate_2w>= 60): echo "style='background-color:#ffa800ad'"; break;
                      case ($end_rate_2w <60 && $end_rate_2w>= 40): echo "style='background-color:#a25506a6'"; break;
                      case ($end_rate_2w < 40): echo "style='background-color:#ff2f009c'"; break;
                            default: echo "";
                    }
              ?> >
          <?php echo $end_rate_2w."%"; ?></td>
           <?php
            if($promised_re_leads==0&&$promised_nre_leads==0&&$re_count_leads_sent==0&&$nre_count_leads_sent!=0){
              $re_delta=0;
              $nre_delta=$promised_leads-$nre_count_leads_sent;
              $delta=$nre_delta;
            }
            if($promised_re_leads==0&&$promised_nre_leads==0&&$re_count_leads_sent!=0&&$nre_count_leads_sent==0){
              $nre_delta=0;
              $re_delta=$promised_leads-$re_count_leads_sent;
              $delta=$re_delta;
            }
            if($promised_re_leads==0&&$promised_nre_leads==0&&$re_count_leads_sent!=0&&$nre_count_leads_sent!=0){
              $re_delta=0;
              $nre_delta=$promised_leads-$re_count_leads_sent-$nre_count_leads_sent;
              $delta=$nre_delta;
            }
            if($promised_re_leads==0&&$promised_nre_leads==0&&$re_count_leads_sent==0&&$nre_count_leads_sent==0){
              $delta=$promised_leads-$count_leads_sent;
            }

          ?>
          <td <?php if($re_delta<'0'){echo $red;} ?> >
            <?php echo $re_delta; ?>
          </td>
          <td <?php if($nre_delta<'0'){echo $red;} ?> >
            <?php echo $nre_delta; ?>
          </td>
          <td <?php if($delta<'0'){echo $red;} ?> >
            <?php echo $delta; ?>
          </td>
        </tr>
        <?php }else{
        $delta=$promised_leads-$count_leads_sent; ?>  
        <tr>
          <td><?php echo $no=$no+1 ; ?></td>
          <td title="<?php echo $shop_name; ?>"><?php echo $shop_name;echo " (L".$leads.")"; ?></td>
          <td><?php echo $start_date; ?></td>
          <td><?php if($promised_leads!=""){echo $promised_leads;}else{echo "0";} ?></td>
          <td><?php echo $count_leads_sent; ?></td>
          <td><?php if($completed!=""){ echo $completed;}else{echo "0";} ?></td> 
          <td><?php echo $exception_completed; ?></td>
          <td <?php switch(true){
                      case ($end_rate >= 70): echo "style='background-color:#00ff4e70'"; break;
                      case ($end_rate <70 && $end_rate>= 60): echo "style='background-color:#ffa800ad'"; break;
                      case ($end_rate <60 && $end_rate>= 40): echo "style='background-color:#a25506a6'"; break;
                      case ($end_rate < 40): echo "style='background-color:#ff2f009c'"; break;
                            default: echo "";
                    }
              ?> >
            <?php echo $end_rate."%"; ?>
          </td>
          <td <?php if($delta<'0'){echo $red;} ?> >
            <?php echo $delta; ?>
          </td>
        </tr>
        <?php  }  
      }////
  } // if
else {
  echo "<h3 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h3>";
}
 ?>
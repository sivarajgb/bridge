<?php
ini_set('max_execution_time', -1);
// error_reporting(E_ALL ^ E_NOTICE);
include("../config.php");
$conn = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];

$shop_id = $_POST['shop_id'];
$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate = date('Y-m-d',strtotime($_POST['enddate']));
?>

<!------      Leads send popup        ---------->
<!-- Modal -->
<div class="modal fade" id="myModal_leads_sent" role="dialog" >
	<div class="modal-dialog" style="width:860px;">

	 <!-- Modal content-->
	 <div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				 <h3 class="modal-title">Leads Sent</h3>
			</div>
			<div class="modal-body" style="max-width:93%; margin-top:6px;margin-left:29px; height:450px !important;overflow:auto !important;">
				<table class="table table-bordered table-hover">
					<thead style="background-color: #D3D3D3;">
						<th>No.</th>
						<th>Customer Name</th>
						<th>Mobile</th>
						<th>Brand</th>
						<th>Model</th>
						<th>Service Type</th>
						<th>Service Date</th>
						<th>Pick Up</th>
						<th>Status</th>
					</thead>
					<tbody id="leadssent">
        <?php  
        $sql_leadssent = "SELECT b.b2b_customer_name,b.b2b_cust_phone,b.brand,b.model,b.b2b_service_type,b.b2b_service_date,b.b2b_pick_up,b.b2b_flag,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND b.b2b_source !='' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
                     $res_leadssent = mysqli_query($conn,$sql_leadssent);
                     $n_leadssent = 0;
                    while($row_leadssent = mysqli_fetch_object($res_leadssent)){
                      ?>
                      <tr>
                        <td><?php echo $n_leadssent=$n_leadssent+1; ?> </td>
                        <td><?php echo $cus_name_leadssent = $row_leadssent->b2b_customer_name; ?></td>
                        <td><?php echo $cus_phn_leadssent = $row_leadssent->b2b_cust_phone; ?></td>
                        <td><?php echo $cus_brand_leadssent = $row_leadssent->brand; ?></td>
                        <td><?php echo $cus_model_leadssent = $row_leadssent->model; ?></td>
                        <td><?php echo $cus_service_leadssent = $row_leadssent->b2b_service_type; ?></td>
                        <td><?php echo $cus_service_date_leadssent = $row_leadssent->b2b_service_date; ?></td>
                        <td><?php  $cus_pickup_leadssent = $row_leadssent->b2b_pick_up;
                                  if($cus_pickup_leadssent == 1){
                                    echo "Yes";
                                  }
                                  else{
                                    echo "NO";
                                  }
                                  ?>
                        </td>
                        <td><?php
                                $b2b_flag = $row_leadssent->b2b_flag;
                                if($b2b_flag == '1'){
                                  echo "Cancelled";
                                }
                                else{
                                  $acpt_leadssent = $row_leadssent->b2b_acpt_flag;
                                  $deny_leadssent = $row_leadssent->b2b_deny_flag;
                                  if($acpt_leadssent == 0 && $deny_leadssent == 0){
                                    echo "No Action";
                                  }
                                  if($acpt_leadssent == 1 && $deny_leadssent == 0){
                                    echo "Accepted";
                                  }
                                  if($acpt_leadssent == 0 && $deny_leadssent == 1){
                                    echo "Denied";
                                  }
                                }

                         ?>
                       </td>
                      </tr>
                      <?php
                    };
                    ?>
					</tbody>
				</table>
</div> <!-- modal body -->
</div> <!-- modal content -->
</div>  <!-- modal dailog -->
</div>  <!-- modal -->

<!------      Leads accepted popup        ---------->
<!-- Modal -->
<div class="modal fade" id="myModal_leads_accepted" role="dialog" >
	<div class="modal-dialog" style="width:860px;">

	 <!-- Modal content-->
	 <div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				 <h3 class="modal-title">Leads Accepted</h3>
			</div>
			<div class="modal-body" style="max-width:93%; margin-top:6px;margin-left:29px; height:450px !important;overflow:auto !important;">
				<table class="table table-bordered table-hover">
					<thead style="background-color: #D3D3D3;">
						<th>No.</th>
						<th>Customer Name</th>
						<th>Mobile</th>
						<th>Brand</th>
						<th>Model</th>
						<th>Service Type</th>
						<th>Service Date</th>
						<th>Pick Up</th>
					</thead>
					<tbody id="leadsaccepted">
          <?php
          $sql_leadsaccepted = "SELECT b.b2b_customer_name,b.b2b_cust_phone,b.brand,b.model,b.b2b_service_type,b.b2b_service_date,b.b2b_pick_up,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_swap_flag!='1' AND b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1' AND s.b2b_deny_flag='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate'";
                         $res_leadsaccepted = mysqli_query($conn,$sql_leadsaccepted);
                         $n_leadsaccepted = 0;
                        while($row_leadsaccepted = mysqli_fetch_object($res_leadsaccepted)){
                          ?>
                          <tr>
                            <td><?php echo $n_leadsaccepted=$n_leadsaccepted+1; ?> </td>
                            <td><?php echo $cus_name_leadsaccepted = $row_leadsaccepted->b2b_customer_name; ?></td>
                            <td><?php echo $cus_phn_leadsaccepted = $row_leadsaccepted->b2b_cust_phone; ?></td>
                            <td><?php echo $cus_brand_leadsaccepted = $row_leadsaccepted->brand; ?></td>
                            <td><?php echo $cus_model_leadsaccepted = $row_leadsaccepted->model; ?></td>
                            <td><?php echo $cus_service_leadsaccepted = $row_leadsaccepted->b2b_service_type; ?></td>
                            <td><?php echo $cus_service_date_leadsaccepted = $row_leadsaccepted->b2b_service_date; ?></td>
                            <td><?php  $cus_pickup_leadsaccepted = $row_leadsaccepted->b2b_pick_up;
                                      if($cus_pickup_leadsaccepted == 1){
                                        echo "Yes";
                                      }
                                      else{
                                        echo "NO";
                                      }
                                      ?>
                            </td>
                          </tr>
                          <?php
                        };
          ?>
					</tbody>
				</table>
</div> <!-- modal body -->
</div> <!-- modal content -->
</div>  <!-- modal dailog -->
</div>  <!-- modal -->

<!------      Swaped Leads popup        ---------->
<!-- Modal -->
<div class="modal fade" id="myModal_swapped_leads" role="dialog" >
	<div class="modal-dialog" style="width:860px;">

	 <!-- Modal content-->
	 <div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				 <h3 class="modal-title">Swapped Leads</h3>
			</div>
			<div class="modal-body" style="max-width:93%; margin-top:6px;margin-left:29px; height:450px !important;overflow:auto !important;">
				<table class="table table-bordered table-hover">
					<thead style="background-color: #D3D3D3;">
						<th>No.</th>
						<th>Customer Name</th>
						<th>Mobile</th>
						<th>Brand</th>
						<th>Model</th>
						<th>Service Type</th>
						<th>Service Date</th>
						<th>Pick Up</th>
					</thead>
					<tbody id="swappedleads">
          <?php
          $sql_swapped_leads = "SELECT b.b2b_customer_name,b.b2b_cust_phone,b.brand,b.model,b.b2b_service_type,b.b2b_service_date,b.b2b_pick_up,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND b.b2b_swap_flag='1' AND s.b2b_acpt_flag='1' AND s.b2b_deny_flag='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate'";
                         $res_swapped_leads = mysqli_query($conn,$sql_swapped_leads);
                         $n_swapped_leads = 0;
                        while($row_swapped_leads = mysqli_fetch_object($res_swapped_leads)){
                          ?>
                          <tr>
                            <td><?php echo $n_swapped_leads=$n_swapped_leads+1; ?> </td>
                            <td><?php echo $cus_name_swapped_leads = $row_swapped_leads->b2b_customer_name; ?></td>
                            <td><?php echo $cus_phn_swapped_leads = $row_swapped_leads->b2b_cust_phone; ?></td>
                            <td><?php echo $cus_brand_swapped_leads = $row_swapped_leads->brand; ?></td>
                            <td><?php echo $cus_model_swapped_leads = $row_swapped_leads->model; ?></td>
                            <td><?php echo $cus_service_swapped_leads = $row_swapped_leads->b2b_service_type; ?></td>
                            <td><?php echo $cus_service_date_swapped_leads = $row_swapped_leads->b2b_service_date; ?></td>
                            <td><?php  $cus_pickup_swapped_leads = $row_swapped_leads->b2b_pick_up;
                                      if($cus_pickup_swapped_leads == 1){
                                        echo "Yes";
                                      }
                                      else{
                                        echo "NO";
                                      }
                                      ?>
                            </td>
                          </tr>
                          <?php
                        };
          ?>
					</tbody>
				</table>
</div> <!-- modal body -->
</div> <!-- modal content -->
</div>  <!-- modal dailog -->
</div>  <!-- modal -->

<!------      checkins reported popup        ---------->
<!-- Modal -->
<div class="modal fade" id="myModal_checkins_reported" role="dialog" >
	<div class="modal-dialog" style="width:860px;">

	 <!-- Modal content-->
	 <div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				 <h3 class="modal-title">CheckIns Reported</h3>
			</div>
			<div class="modal-body" style="max-width:93%; margin-top:6px;margin-left:29px; height:450px !important;overflow:auto !important;">
				<table class="table table-bordered table-hover">
					<thead style="background-color: #D3D3D3;">
						<th>No.</th>
						<th>Customer Name</th>
						<th>Mobile</th>
						<th>Brand</th>
						<th>Model</th>
						<th>Service Type</th>
						<th>Service Date</th>
						<th>Pick Up</th>
					</thead>
					<tbody id="checkinsreported">
          <?php
          $sql_checkinsreported = "SELECT b.b2b_customer_name,b.b2b_cust_phone,b.brand,b.model,b.b2b_service_type,b.b2b_service_date,b.b2b_pick_up,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1'  AND b.b2b_check_in_report='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
                            $res_checkinsreported = mysqli_query($conn,$sql_checkinsreported);
                            $n_checkinsreported = 0;
                           while($row_checkinsreported = mysqli_fetch_object($res_checkinsreported)){
                             ?>
                             <tr>
                               <td><?php echo $n_checkinsreported=$n_checkinsreported+1; ?> </td>
                               <td><?php echo $cus_name_checkinsreported = $row_checkinsreported->b2b_customer_name; ?></td>
                               <td><?php echo $cus_phn_checkinsreported = $row_checkinsreported->b2b_cust_phone; ?></td>
                               <td><?php echo $cus_brand_checkinsreported = $row_checkinsreported->brand; ?></td>
                               <td><?php echo $cus_model_checkinsreported = $row_checkinsreported->model; ?></td>
                               <td><?php echo $cus_service_checkinsreported = $row_checkinsreported->b2b_service_type; ?></td>
                               <td><?php echo $cus_service_date_checkinsreported = $row_checkinsreported->b2b_service_date; ?></td>
                               <td><?php  $cus_pickup_checkinsreported = $row_checkinsreported->b2b_pick_up;
                                         if($cus_pickup_checkinsreported == 1){
                                           echo "Yes";
                                         }
                                         else{
                                           echo "NO";
                                         }
                                         ?>
                               </td>
                             </tr>
                             <?php
                           };
                           ?>
					</tbody>
				</table>
</div> <!-- modal body -->
</div> <!-- modal content -->
</div>  <!-- modal dailog -->
</div>  <!-- modal -->
<!------      Inspections Completed popup        ---------->
<!-- Modal -->
<div class="modal fade" id="myModal_inspections_completed" role="dialog" >
	<div class="modal-dialog" style="width:860px;">

	 <!-- Modal content-->
	 <div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				 <h3 class="modal-title">Inspections Completed</h3>
			</div>
			<div class="modal-body" style="max-width:93%; margin-top:6px;margin-left:29px; height:450px !important;overflow:auto !important;">
				<table class="table table-bordered table-hover">
					<thead style="background-color: #D3D3D3;">
						<th>No.</th>
						<th>Customer Name</th>
						<th>Mobile</th>
						<th>Brand</th>
						<th>Model</th>
						<th>Service Type</th>
						<th>Service Date</th>
						<th>Pick Up</th>
					</thead>
					<tbody id="inspectionscompleted">
          <?php
          $sql_inspectionscompleted = "SELECT b.b2b_customer_name,b.b2b_cust_phone,b.brand,b.model,b.b2b_service_type,b.b2b_service_date,b.b2b_pick_up,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1'  AND b.b2b_vehicle_at_garage='1' AND b.b2b_service_under_progress='0' AND b.b2b_vehicle_ready='0' AND b.b2b_vehicle_delivered='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
                                $res_inspectionscompleted = mysqli_query($conn,$sql_inspectionscompleted);
                                $n_inspectionscompleted = 0;
                               while($row_inspectionscompleted = mysqli_fetch_object($res_inspectionscompleted)){
                                 ?>
                                 <tr>
                                   <td><?php echo $n_inspectionscompleted=$n_inspectionscompleted+1; ?> </td>
                                   <td><?php echo $cus_name_inspectionscompleted = $row_inspectionscompleted->b2b_customer_name; ?></td>
                                   <td><?php echo $cus_phn_inspectionscompleted = $row_inspectionscompleted->b2b_cust_phone; ?></td>
                                   <td><?php echo $cus_brand_inspectionscompleted = $row_inspectionscompleted->brand; ?></td>
                                   <td><?php echo $cus_model_inspectionscompleted = $row_inspectionscompleted->model; ?></td>
                                   <td><?php echo $cus_service_inspectionscompleted = $row_inspectionscompleted->b2b_service_type; ?></td>
                                   <td><?php echo $cus_service_date_inspectionscompleted = $row_inspectionscompleted->b2b_service_date; ?></td>
                                   <td><?php  $cus_pickup_inspectionscompleted = $row_inspectionscompleted->b2b_pick_up;
                                             if($cus_pickup_inspectionscompleted == 1){
                                               echo "Yes";
                                             }
                                             else{
                                               echo "NO";
                                             }
                                             ?>
                                   </td>
                                 </tr>
                                 <?php
                               };
                               ?>
					</tbody>
				</table>
</div> <!-- modal body -->
</div> <!-- modal content -->
</div>  <!-- modal dailog -->
</div>  <!-- modal -->
<!------      Services under progress popup        ---------->
<!-- Modal -->
<div class="modal fade" id="myModal_services_under_progress" role="dialog" >
	<div class="modal-dialog" style="width:860px;">

	 <!-- Modal content-->
	 <div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				 <h3 class="modal-title">Services Under Progress</h3>
			</div>
			<div class="modal-body" style="max-width:93%; margin-top:6px;margin-left:29px; height:450px !important;overflow:auto !important;">
				<table class="table table-bordered table-hover">
					<thead style="background-color: #D3D3D3;">
						<th>No.</th>
						<th>Customer Name</th>
						<th>Mobile</th>
						<th>Brand</th>
						<th>Model</th>
						<th>Service Type</th>
						<th>Service Date</th>
						<th>Pick Up</th>
					</thead>
					<tbody id="servicesunderprogress">
          <?php
          $sql_servicesunderprogress = "SELECT b.b2b_customer_name,b.b2b_cust_phone,b.brand,b.model,b.b2b_service_type,b.b2b_service_date,b.b2b_pick_up,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1'  AND b.b2b_service_under_progress='1' AND b.b2b_vehicle_ready='0' AND b.b2b_vehicle_delivered='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
                                  $res_servicesunderprogress = mysqli_query($conn,$sql_servicesunderprogress);
                                  $n_servicesunderprogress = 0;
                                 while($row_servicesunderprogress = mysqli_fetch_object($res_servicesunderprogress)){
                                   ?>
                                   <tr>
                                     <td><?php echo $n_servicesunderprogress=$n_servicesunderprogress+1; ?> </td>
                                     <td><?php echo $cus_name_servicesunderprogress = $row_servicesunderprogress->b2b_customer_name; ?></td>
                                     <td><?php echo $cus_phn_servicesunderprogress = $row_servicesunderprogress->b2b_cust_phone; ?></td>
                                     <td><?php echo $cus_brand_servicesunderprogress = $row_servicesunderprogress->brand; ?></td>
                                     <td><?php echo $cus_model_servicesunderprogress = $row_servicesunderprogress->model; ?></td>
                                     <td><?php echo $cus_service_servicesunderprogress = $row_servicesunderprogress->b2b_service_type; ?></td>
                                     <td><?php echo $cus_service_date_servicesunderprogress = $row_servicesunderprogress->b2b_service_date; ?></td>
                                     <td><?php  $cus_pickup_servicesunderprogress = $row_servicesunderprogress->b2b_pick_up;
                                               if($cus_pickup_servicesunderprogress == 1){
                                                 echo "Yes";
                                               }
                                               else{
                                                 echo "NO";
                                               }
                                               ?>
                                     </td>
                                   </tr>
                                   <?php
                                 };
          ?>
					</tbody>
				</table>
</div> <!-- modal body -->
</div> <!-- modal content -->
</div>  <!-- modal dailog -->
</div>  <!-- modal -->
<!------      vehicles ready popup        ---------->
<!-- Modal -->
<div class="modal fade" id="myModal_vehicles_ready" role="dialog" >
	<div class="modal-dialog" style="width:860px;">

	 <!-- Modal content-->
	 <div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				 <h3 class="modal-title">Vehicles Ready</h3>
			</div>
			<div class="modal-body" style="max-width:93%; margin-top:6px;margin-left:29px; height:450px !important;overflow:auto !important;">
				<table class="table table-bordered table-hover">
					<thead style="background-color: #D3D3D3;">
						<th>No.</th>
						<th>Customer Name</th>
						<th>Mobile</th>
						<th>Brand</th>
						<th>Model</th>
						<th>Service Type</th>
						<th>Service Date</th>
						<th>Pick Up</th>
					</thead>
					<tbody id="vehiclesready">
          <?php
          $sql_vehiclesready = "SELECT b.b2b_customer_name,b.b2b_cust_phone,b.brand,b.model,b.b2b_service_type,b.b2b_service_date,b.b2b_pick_up,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1'  AND b.b2b_vehicle_ready='1' AND b.b2b_vehicle_delivered='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
                         $res_vehiclesready = mysqli_query($conn,$sql_vehiclesready);
                         $n_vehiclesready = 0;
                        while($row_vehiclesready = mysqli_fetch_object($res_vehiclesready)){
                          ?>
                          <tr>
                            <td><?php echo $n_vehiclesready=$n_vehiclesready+1; ?> </td>
                            <td><?php echo $cus_name_vehiclesready = $row_vehiclesready->b2b_customer_name; ?></td>
                            <td><?php echo $cus_phn_vehiclesready = $row_vehiclesready->b2b_cust_phone; ?></td>
                            <td><?php echo $cus_brand_vehiclesready = $row_vehiclesready->brand; ?></td>
                            <td><?php echo $cus_model_vehiclesready = $row_vehiclesready->model; ?></td>
                            <td><?php echo $cus_service_vehiclesready = $row_vehiclesready->b2b_service_type; ?></td>
                            <td><?php echo $cus_service_date_vehiclesready = $row_vehiclesready->b2b_service_date; ?></td>
                            <td><?php  $cus_pickup_vehiclesready = $row_vehiclesready->b2b_pick_up;
                                      if($cus_pickup_vehiclesready == 1){
                                        echo "Yes";
                                      }
                                      else{
                                        echo "NO";
                                      }
                                      ?>
                            </td>
                          </tr>
                          <?php
                        };
                        ?>
					</tbody>
				</table>
</div> <!-- modal body -->
</div> <!-- modal content -->
</div>  <!-- modal dailog -->
</div>  <!-- modal -->
<!------     vehicles delivered  popup        ---------->
<!-- Modal -->
<div class="modal fade" id="myModal_vehicles_delivered" role="dialog" >
	<div class="modal-dialog" style="width:890px;">

	 <!-- Modal content-->
	 <div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				 <h3 class="modal-title">Vehicles Delivered</h3>
			</div>
			<div class="modal-body" style="max-width:93%; margin-top:6px;margin-left:29px; height:450px !important;overflow:auto !important;">
				<table class="table table-bordered table-hover">
					<thead style="background-color: #D3D3D3;">
						<th>No.</th>
						<th>Customer Name</th>
						<th>Mobile</th>
						<th>Brand</th>
						<th>Model</th>
						<th>Service Type</th>
						<th>Service Date</th>
						<th>Pick Up</th>
						<th>Bill</th>
					</thead>
					<tbody id="vehiclesdelivered">
          <?php
          $sql_vehiclesdelivered = "SELECT b.b2b_customer_name,b.b2b_cust_phone,b.brand,b.model,b.b2b_service_type,b.b2b_service_date,b.b2b_pick_up,b.b2b_bill_amount,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1'  AND b.b2b_vehicle_delivered='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
                             $res_vehiclesdelivered = mysqli_query($conn,$sql_vehiclesdelivered);
                             $n_vehiclesdelivered = 0;
                            while($row_vehiclesdelivered = mysqli_fetch_object($res_vehiclesdelivered)){
                              ?>
                              <tr>
                                <td><?php echo $n_vehiclesdelivered=$n_vehiclesdelivered+1; ?> </td>
                                <td><?php echo $cus_name_vehiclesdelivered = $row_vehiclesdelivered->b2b_customer_name; ?></td>
                                <td><?php echo $cus_phn_vehiclesdelivered = $row_vehiclesdelivered->b2b_cust_phone; ?></td>
                                <td><?php echo $cus_brand_vehiclesdelivered = $row_vehiclesdelivered->brand; ?></td>
                                <td><?php echo $cus_model_vehiclesdelivered = $row_vehiclesdelivered->model; ?></td>
                                <td><?php echo $cus_service_vehiclesdelivered = $row_vehiclesdelivered->b2b_service_type; ?></td>
                                <td><?php echo $cus_service_date_vehiclesdelivered = $row_vehiclesdelivered->b2b_service_date; ?></td>
                                <td><?php  $cus_pickup_vehiclesdelivered = $row_vehiclesdelivered->b2b_pick_up;
                                          if($cus_pickup_vehiclesdelivered == 1){
                                            echo "Yes";
                                          }
                                          else{
                                            echo "NO";
                                          }
                                          ?>
                                </td>
                                <td><?php echo $bill_vehiclesdelivered=$row_vehiclesdelivered->b2b_bill_amount; ?> </td>
                              </tr>
                              <?php
                            };
                            ?>
					</tbody>
				</table>
</div> <!-- modal body -->
</div> <!-- modal content -->
</div>  <!-- modal dailog -->
</div>  <!-- modal -->

<!------     total bill  popup        ---------->
<!-- Modal -->
<div class="modal fade" id="myModal_total_bill" role="dialog" >
	<div class="modal-dialog" style="width:890px;">

	 <!-- Modal content-->
	 <div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				 <h3 class="modal-title">Total Bill</h3>
			</div>
			<div class="modal-body" style="max-width:93%; margin-top:6px;margin-left:29px; height:450px !important;overflow:auto !important;">
				<table class="table table-bordered table-hover">
					<thead style="background-color: #D3D3D3;">
						<th>No.</th>
						<th>Customer Name</th>
						<th>Mobile</th>
						<th>Brand</th>
						<th>Model</th>
						<th>Service Type</th>
						<th>Service Date</th>
						<th>Pick Up</th>
						<th>Bill</th>
					</thead>
					<tbody id="total_bill">
          <?php
          $sql_total_bill = "SELECT b.b2b_customer_name,b.b2b_cust_phone,b.brand,b.model,b.b2b_service_type,b.b2b_service_date,b.b2b_pick_up,b.b2b_bill_amount,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1'  AND b.b2b_vehicle_ready='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ";
                   $res_total_bill = mysqli_query($conn,$sql_total_bill);
                   $n_total_bill = 0;
                            while($row_total_bill = mysqli_fetch_object($res_total_bill)){
                              ?>
                              <tr>
                                <td><?php echo $n_total_bill=$n_total_bill+1; ?> </td>
                                <td><?php echo $cus_name_total_bill = $row_total_bill->b2b_customer_name; ?></td>
                                <td><?php echo $cus_phn_total_bill = $row_total_bill->b2b_cust_phone; ?></td>
                                <td><?php echo $cus_brand_total_bill = $row_total_bill->brand; ?></td>
                                <td><?php echo $cus_model_total_bill = $row_total_bill->model; ?></td>
                                <td><?php echo $cus_service_total_bill = $row_total_bill->b2b_service_type; ?></td>
                                <td><?php echo $cus_service_date_total_bill = $row_total_bill->b2b_service_date; ?></td>
                                <td><?php  $cus_pickup_total_bill = $row_total_bill->b2b_pick_up;
                                          if($cus_pickup_total_bill == 1){
                                            echo "Yes";
                                          }
                                          else{
                                            echo "NO";
                                          }
                                          ?>
                                </td>
                                <td><?php echo $bill_total_bill = $row_total_bill->b2b_bill_amount; ?> </td>
                              </tr>
                              <?php
                            };
                            ?>
					</tbody>
				</table>
</div> <!-- modal body -->
</div> <!-- modal content -->
</div>  <!-- modal dailog -->
</div>  <!-- modal -->

<!------     avg bill  popup        ---------->
<!-- Modal -->
<div class="modal fade" id="myModal_avg_bill" role="dialog" >
	<div class="modal-dialog" style="width:890px;">

	 <!-- Modal content-->
	 <div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				 <h3 class="modal-title">Avg Bill</h3>
			</div>
			<div class="modal-body" style="max-width:93%; margin-top:6px;margin-left:29px; height:450px !important;overflow:auto !important;">
				<table class="table table-bordered table-hover">
					<thead style="background-color: #D3D3D3;">
						<th>No.</th>
						<th>Customer Name</th>
						<th>Mobile</th>
						<th>Brand</th>
						<th>Model</th>
						<th>Service Type</th>
						<th>Service Date</th>
						<th>Pick Up</th>
						<th>Bill</th>
					</thead>
					<tbody id="avg_bill">
          <?php
          $sql_avg_bill = "SELECT b.b2b_customer_name,b.b2b_cust_phone,b.brand,b.model,b.b2b_service_type,b.b2b_service_date,b.b2b_pick_up,b.b2b_bill_amount,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1'  AND b.b2b_vehicle_ready='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate'";
                   $res_avg_bill = mysqli_query($conn,$sql_avg_bill);
                    $n_avg_bill = 0;
                            while($row_avg_bill = mysqli_fetch_object($res_avg_bill)){
                              ?>
                              <tr>
                                <td><?php echo $n_avg_bill = $n_avg_bill+1; ?> </td>
                                <td><?php echo $cus_name_avg_bill = $row_avg_bill->b2b_customer_name; ?></td>
                                <td><?php echo $cus_phn_avg_bill = $row_avg_bill->b2b_cust_phone; ?></td>
                                <td><?php echo $cus_brand_avg_bill = $row_avg_bill->brand; ?></td>
                                <td><?php echo $cus_model_avg_bill = $row_avg_bill->model; ?></td>
                                <td><?php echo $cus_service_avg_bill = $row_avg_bill->b2b_service_type; ?></td>
                                <td><?php echo $cus_service_date_avg_bill = $row_avg_bill->b2b_service_date; ?></td>
                                <td><?php  $cus_pickup_avg_bill = $row_avg_bill->b2b_pick_up;
                                          if($cus_pickup_avg_bill == 1){
                                            echo "Yes";
                                          }
                                          else{
                                            echo "NO";
                                          }
                                          ?>
                                </td>
                                <td><?php echo $bill_avg_bill = $row_avg_bill->b2b_bill_amount; ?> </td>
                              </tr>
                              <?php
                            };
          ?>
					</tbody>
				</table>
</div> <!-- modal body -->
</div> <!-- modal content -->
</div>  <!-- modal dailog -->
</div>  <!-- modal -->

<!------     rating  popup        ---------->
<!-- Modal -->
<div class="modal fade" id="myModal_rating" role="dialog" >
	<div class="modal-dialog" style="width:890px;">

	 <!-- Modal content-->
	 <div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				 <h3 class="modal-title">Rating</h3>
			</div>
			<div class="modal-body" style="max-width:93%; margin-top:6px;margin-left:29px; height:450px !important;overflow:auto !important;">
				<table class="table table-bordered table-hover">
					<thead style="background-color: #D3D3D3;">
						<th>No.</th>
						<th>Customer Name</th>
						<th>Mobile</th>
						<th>Brand</th>
						<th>Model</th>
						<th>Service Type</th>
						<th>Service Date</th>
						<th>Pick Up</th>
						<th>Rating</th>
					</thead>
					<tbody id="ratn">
          <?php
          $sql_ratings = "SELECT b.b2b_customer_name,b.b2b_cust_phone,b.brand,b.model,b.b2b_service_type,b.b2b_service_date,b.b2b_pick_up,b.b2b_Rating,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1' AND b.b2b_Rating !='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate'";
                   $res_ratings = mysqli_query($conn,$sql_ratings);
                   $n_ratings = 0;
                            while($row_ratings = mysqli_fetch_object($res_ratings)){
                              ?>
                              <tr>
                                <td><?php echo $n_ratings=$n_ratings+1; ?> </td>
                                <td><?php echo $cus_name_ratings = $row_ratings->b2b_customer_name; ?></td>
                                <td><?php echo $cus_phn_ratings = $row_ratings->b2b_cust_phone; ?></td>
                                <td><?php echo $cus_brand_ratings = $row_ratings->brand; ?></td>
                                <td><?php echo $cus_model_ratings = $row_ratings->model; ?></td>
                                <td><?php echo $cus_service_ratings = $row_ratings->b2b_service_type; ?></td>
                                <td><?php echo $cus_service_date_ratings = $row_ratings->b2b_service_date; ?></td>
                                <td><?php  $cus_pickup_ratings = $row_ratings->b2b_pick_up;
                                          if($cus_pickup_ratings == 1){
                                            echo "Yes";
                                          }
                                          else{
                                            echo "NO";
                                          }
                                          ?>
                                </td>
                                <td><?php echo $rat_ratings=$row_ratings->b2b_Rating; ?> </td>
                              </tr>
                              <?php
                            };
          ?>
					</tbody>
				</table>
</div> <!-- modal body -->
</div> <!-- modal content -->
</div>  <!-- modal dailog -->
</div>  <!-- modal -->

<!------     reviews  popup        ---------->
<!-- Modal -->
<div class="modal fade" id="myModal_reviews" role="dialog" >
	<div class="modal-dialog" style="width:890px;">

	 <!-- Modal content-->
	 <div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				 <h3 class="modal-title">Reviews</h3>
			</div>
			<div class="modal-body" style="max-width:93%; margin-top:6px;margin-left:29px; height:450px !important;overflow:auto !important;">
				<table class="table table-bordered table-hover">
					<thead style="background-color: #D3D3D3;">
						<th>No.</th>
						<th>Customer Name</th>
						<th>Mobile</th>
						<th>Brand</th>
						<th>Model</th>
						<th>Service Type</th>
						<th>Service Date</th>
						<th>Pick Up</th>
						<th>Review</th>
					</thead>
					<tbody id="rew">
          <?php
          $sql_reviews = "SELECT b.b2b_customer_name,b.b2b_cust_phone,b.brand,b.model,b.b2b_service_type,b.b2b_service_date,b.b2b_pick_up,b.b2b_Feedback,s.b2b_acpt_flag,s.b2b_deny_flag FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_shop_id='$shop_id' AND s.b2b_acpt_flag='1' AND b.b2b_Feedback !='' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate'";
          $res_reviews = mysqli_query($conn,$sql_reviews);
          $n_reviews = 0;
          while($row_reviews = mysqli_fetch_object($res_reviews)){
           ?>
           <tr>
           <td><?php echo $n_reviews=$n_reviews+1; ?> </td>
           <td><?php echo $cus_name_reviews = $row_reviews->b2b_customer_name; ?></td>
           <td><?php echo $cus_phn_reviews = $row_reviews->b2b_cust_phone; ?></td>
           <td><?php echo $cus_brand_reviews = $row_reviews->brand; ?></td>
            <td><?php echo $cus_model_reviews = $row_reviews->model; ?></td>
            <td><?php echo $cus_service_reviews = $row_reviews->b2b_service_type; ?></td>
            <td><?php echo $cus_service_date_reviews = $row_reviews->b2b_service_date; ?></td>
             <td><?php  $cus_pickup_reviews = $row_reviews->b2b_pick_up;
                  if($cus_pickup_reviews == 1){
                        echo "Yes";
                   }
                   else{
                        echo "NO";
                    }
                   ?>
             </td>
              <td><?php echo $rew_reviews = $row_reviews->b2b_Feedback; ?> </td>
              </tr>
               <?php
               };
          ?>
					</tbody>
				</table>
</div> <!-- modal body -->
</div> <!-- modal content -->
</div>  <!-- modal dailog -->
</div>  <!-- modal -->


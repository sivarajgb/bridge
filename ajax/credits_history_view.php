<?php
include("../config.php");
$conn = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$life = $_POST['life'];
$city = $_POST['city'];

$_SESSION['crm_city'] = $city;

$no = 0;
if($life == "yes"){
  ?>
  <h4 style="width:70%;float:left;margin-left:50px;"><i class="fa fa-history" aria-hidden="true" style="color:#26A69A;"></i>&nbsp;&nbsp;LifeTime View</h4>
  <?php
}
?>
<div id="division">
  <div id="div1" style="width:70%;float:left;margin-left:40px;">
    <table class="table table-striped table-bordered tablesorter table-hover results" id="table" style="font-size:12px;">
    <thead style="background-color:#B2DFDB;align:center;display: table-header-group;">
    <th>No. <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>ShopName <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Type <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <!-- <th>TotalCredits <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th> -->
    <th>Credit Balance <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Lead Balance <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Goaxles <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Accepted <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    </thead>
    <tbody id="tbody">

    <?php

    $sql_garage = "SELECT DISTINCT m.b2b_shop_id,m.b2b_shop_name,m.b2b_vehicle_type,c.b2b_credits,c.b2b_leads,c.b2b_partner_flag,c.b2b_flag FROM b2b_mec_tbl as m INNER JOIN b2b_credits_tbl as c ON m.b2b_shop_id = c.b2b_shop_id WHERE m.b2b_cred_model=1  AND m.b2b_address5='$city' and m.leila != 1 ORDER BY m.b2b_shop_name ASC ";
    $res_garage = mysqli_query($conn,$sql_garage);

      // $list_credits = array();
    if($life=="no"){
        $date="AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate'";
    }
    else{
        $date="";
    }
    $sql_leads_sent = "SELECT count(b.b2b_booking_id) as count,b.b2b_shop_id FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_source !='' $date group by b.b2b_shop_id";
        $res_leads_sent = mysqli_query($conn,$sql_leads_sent);
        while($row_leads_sent = mysqli_fetch_object($res_leads_sent)){
            $count_leads_sent[]=$row_leads_sent->count;
            $lead_sent_id[]=$row_leads_sent->b2b_shop_id;
        }

    $sql_leads_accepted = "SELECT count(b.b2b_booking_id) as count,b.b2b_shop_id FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE b.b2b_swap_flag!='1' AND s.b2b_acpt_flag='1' AND s.b2b_deny_flag='0' $date group by b.b2b_shop_id";
        $res_leads_accepted = mysqli_query($conn,$sql_leads_accepted);
        while($row_leads_accepted = mysqli_fetch_object($res_leads_accepted)){
            $count_leads_accepted[]=$row_leads_accepted->count;
            $lead_accepted_id[]=$row_leads_accepted->b2b_shop_id;
        }
    $sql_used_credits = "SELECT b.b2b_credit_amt as total,b.b2b_shop_id FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id=s.b2b_booking_id WHERE s.b2b_acpt_flag='1' $date";
        $res_used_credits = mysqli_query($conn,$sql_used_credits);
        while($row_used_credits = mysqli_fetch_object($res_used_credits)){
            $amount[] = $row_used_credits->total;
            $used_credits_id[]=$row_used_credits->b2b_shop_id;
        }

    while($row_garage = mysqli_fetch_object($res_garage)){
        $shop_id = $row_garage->b2b_shop_id;
        $shop_name = $row_garage->b2b_shop_name;
        $vehicle_type = $row_garage->b2b_vehicle_type;
        $unused_credits = $row_garage->b2b_credits;
        $leads = $row_garage->b2b_leads;
        $premium = $row_garage->b2b_partner_flag;
        $b2b_flag = $row_garage->b2b_flag;

        $enc_shop_id = base64_encode($shop_id);

        $amount_got=0;
        if(isset($used_credits_id)){
            for($i=0;$i<sizeof($used_credits_id);$i++){
                if($used_credits_id[$i]==$shop_id){
                    $amount_got+=$amount[$i];
                }
            }
        }
        $used_credits = round($amount_got/100,1);

        $list_credits[] = array("id" => $shop_id, "shop_name" => $shop_name,"leads" =>$leads , "unused" => $unused_credits);
        

        $total_credits = $used_credits + $unused_credits;

        $goaxle_count=0;
        if(isset($lead_sent_id)){
            for($i=0;$i<sizeof($lead_sent_id);$i++){
                if($lead_sent_id[$i]==$shop_id){
                    $goaxle_count=$count_leads_sent[$i];
                }
            }
        }

        $accept_count=0;
        if(isset($lead_accepted_id)){
            for($i=0;$i<sizeof($lead_accepted_id);$i++){
                if($lead_accepted_id[$i]==$shop_id){
                    $accept_count=$count_leads_accepted[$i];
                }
            }
        }
        
        ?>
        <tr>
            <td><?php echo $no = $no+1; ?></td>
            <td><a href="shop_credits_history.php?si=<?php echo $enc_shop_id; ?>&t=<?php echo base64_encode('credits'); ?>" ><?php echo  $shop_name; if($premium == 2){ ?>&nbsp;&nbsp;<img src="images/authorized.png" style="width:28px;" title="Premium Garage"> <?php } if($premium == 1 && $b2b_flag == 1){ ?>&nbsp;&nbsp;<i class="fa fa-archive" aria-hidden="true" title="Old Premium partner" style="font-size: 18px;"></i> <?php }if($premium == 1 && $b2b_flag != 1){ ?>&nbsp;&nbsp;<i class="fa fa-leaf" aria-hidden="true" title="Upcoming Premium Prospect" style="font-size: 18px;color:#59bf4d;"></i> <?php } ?></a></td>
            <td><?php echo  $vehicle_type;?></td>
            <!-- <td><?php echo $total_credits; ?></td> -->
            <!-- <td class="price"><?php echo $used_credits; ?></td> -->
            <td><?php echo $unused_credits; ?></td>
            <td><?php echo $leads; ?></td>
            <td><?php echo $goaxle_count; ?></td>
            <td><?php echo $accept_count; ?></td>
        </tr>
    
        <?php
    }
    //$list_credits1 = $list_credits;
    foreach ($list_credits as $key => $row) {
        $id[$key] = $row['id'];
        $value[$key] = $row['leads'];
    }

    array_multisort($value, SORT_DESC, $id , SORT_ASC , $list_credits);
    $usedcredits_sorted = array_values($list_credits);

    foreach ($list_credits as $key => $row) {
        $id[$key] = $row['id'];
        $value[$key] = $row['unused'];
    }

    array_multisort($value, SORT_DESC, $id , SORT_ASC , $list_credits);
    $unusedcredits_sorted = array_values($list_credits);
    ?>

    </tbody>
    </table>
  </div>
  </div>
  

<div>
    <div style="width:14%;float:left;position:absolute;top:130;right:120;"><h4><b>Leads</b></h4></div>
    <div id="div2" style="width:14%;float:left;position:absolute;top:160;right:120;">
    <div class="floating-box">
    <p><i class="fa fa-star" aria-hidden="true" style="font-size:24px;color:#D4AF37;text-shadow: 2px 2px #80CBC4;"></i>&nbsp;&nbsp;<span style="font-size:15px;"><?php echo $usedcredits_sorted[0]['shop_name'];echo " - ".$usedcredits_sorted[0]['leads']; ?></span></p>
    </div>
    </div>
    <div id="div3" style="width:14%;float:left;position:absolute;top:200;right:120;">
    <div class="floating-box">
    <p><i class="fa fa-star" aria-hidden="true" style="font-size:24px;color:#A9A9A9;text-shadow: 2px 2px #80CBC4;"></i>&nbsp;&nbsp;<span style="font-size:15px;"><?php echo $usedcredits_sorted[1]['shop_name'];echo " - ".$usedcredits_sorted[1]['leads']; ?></span></p>
    </div>
    </div>
    <div id="div4" style="width:14%;float:left;position:absolute;top:240;right:120;">
    <div class="floating-box">
    <p><i class="fa fa-star" aria-hidden="true" style="font-size:24px;color:#b87333;text-shadow: 2px 2px #80CBC4;"></i>&nbsp;&nbsp;<span style="font-size:15px;"><?php echo $usedcredits_sorted[2]['shop_name'];echo " - ".$usedcredits_sorted[2]['leads']; ?></span></p>
    </div>
    </div>
    <div style="width:14%;float:left;position:absolute;top:290;right:120;"><h4><b>Credits</b></h4></div>
    <div id="div5" style="width:14%;float:left;position:absolute;top:320;right:120;">
    <div class="floating-box">
    <p><i class="fa fa-battery-full" aria-hidden="true" style="font-size:17px;color:#EF5350;text-shadow: 2px 2px #80CBC4;"></i>&nbsp;&nbsp;<span style="font-size:15px;"><?php echo $unusedcredits_sorted[0]['shop_name'];echo " - ".$unusedcredits_sorted[0]['unused']; ?></span></p>
    </div>
    </div>
    <div id="div6" style="width:14%;float:left;position:absolute;top:360;right:120;">
    <div class="floating-box">
    <p><i class="fa fa-battery-three-quarters" aria-hidden="true" style="font-size:17px;color:#EF5350;text-shadow: 2px 2px #80CBC4;"></i>&nbsp;&nbsp;<span style="font-size:15px;"><?php echo $unusedcredits_sorted[1]['shop_name'];echo " - ".$unusedcredits_sorted[1]['unused']; ?></span></p>
    </div>
    </div>
    <div id="div7" style="width:14%;float:left;position:absolute;top:400;right:120;">
    <div class="floating-box">
    <p><i class="fa fa-battery-half" aria-hidden="true" style="font-size:17px;color:#EF5350;text-shadow: 2px 2px #80CBC4;"></i>&nbsp;&nbsp;<span style="font-size:15px;"><?php echo $unusedcredits_sorted[2]['shop_name'];echo " - ".$unusedcredits_sorted[2]['unused']; ?></span></p>
    </div>
    </div>
</div>

<!-- search bar  -->
<script>
$(document).ready(function() {
  $(".search").keyup(function () {
    var searchTerm = $(".search").val();
    var listItem = $('.results tbody').children('tr');
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

  $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });

  $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','false');
  });

  $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','true');
  });
  var jobCount = $('.results tbody tr[visible="true"]').length;
    $('.counter').text(jobCount + ' shops');

  if(jobCount == '0') {$('.no-result').show();}
    else {$('.no-result').hide();
    }
 });
});
</script>


<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#table").tablesorter( {sortList: [[0,0], [1,0]]} );
    }
);
</script>
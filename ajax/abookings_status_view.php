<?php
  include("../config.php");
  $conn = db_connect1();
  $conn2 = db_connect2();
  session_start();
  $today=date('Y-m-d');

  $crm_log_id = $_SESSION['crm_log_id'] ;
  $crm_name = $_SESSION['crm_name'];
  $flag=$_SESSION['flag'];
  $super_flag = $_SESSION['super_flag'];
  $src_crm = array('crm016','crm018','crm064','crm036','crm033');
  $src_column = 0;
  if(in_array($crm_log_id,$src_crm))
  {
    $src_column = 1;
  }

  $startdate = date('Y-m-d',strtotime($_GET['startdate']));
  $enddate =  date('Y-m-d',strtotime($_GET['enddate']));
  $person = $_GET['person'];
  $source = $_GET['source'];
  $vehicle = $_GET['vehicle'];
  $city = $_GET['city'];
  $cluster = $city != 'Chennai' ? 'all' : $_GET['cluster'];
  $garage = $_GET['garage'];
  $status = $_GET['status'];

  $_SESSION['crm_city'] = $city;
  $_SESSION['crm_cluster'] = $cluster;

  $col_name = $vehicle == '2w' ? 'l.bike_cluster' : 'l.car_cluster';
 $cond ='';

 $cond = $cond.($vehicle == 'all' ? "" : "AND b.vehicle_type='$vehicle'");
 $cond = $cond.($source == 'all' ? "" : "AND b.source='$source'");
 $cond = $cond.($person == 'all' ? "" : "AND b.crm_update_id='$person'");
 $cond = $cond.($city == 'all' ? "" : "AND b.city='$city'");
 $cond = $cond.($cluster == 'all' ? "" : ($vehicle != 'all' ? "AND ".$col_name."='$cluster'" : "AND (case when b.vehicle_type = '2w' then l.bike_cluster else l.car_cluster end) like '%".$cluster."%'"));
 $cond = $cond.($garage == 'all' ? "" : "AND b.mec_id='$garage'");

  $sql_booking = "SELECT DISTINCT b.booking_id,b.user_id,b.mec_id,b.crm_update_time,b.source,b.vehicle_type,b.user_veh_id,b.service_type,b.service_date,b.shop_name,b.axle_flag,b.crm_update_id,b.locality,b.utm_source,b.priority,b.flag_fo,b.user_pmt_flg,c.name as crm_name,u.name as user_name,u.mobile_number as user_mobile,u.lat_lng as user_lat_lng,u.Locality_Home as user_locality,u.user_level,v.brand,v.model,s.b2b_acpt_flag,s.b2b_deny_flag FROM user_booking_tb as b LEFT JOIN crm_admin as c ON b.crm_update_id=c.crm_log_id LEFT JOIN user_register as u ON b.user_id=u.reg_id LEFT JOIN user_vehicle_table as v ON b.user_veh_id=v.id LEFT JOIN b2b.b2b_booking_tbl as bb ON b.booking_id=bb.gb_booking_id  LEFT JOIN b2b.b2b_status as s ON bb.b2b_booking_id=s.b2b_booking_id WHERE  bb.gb_booking_id!='0' AND bb.b2b_flag!='1' AND s.b2b_acpt_flag='1' AND b.booking_status='2' AND b.flag!='1' AND DATE(b.crm_update_time) BETWEEN '$startdate' and '$enddate' {$cond} ORDER BY b.crm_update_time DESC";
  //echo $sql_booking;
  $res_booking = mysqli_query($conn,$sql_booking);

  $no = 0;

$count = mysqli_num_rows($res_booking);
if($count >=1){

while($row_booking = mysqli_fetch_object($res_booking)){
$booking_id = $row_booking->booking_id;
$user_id = $row_booking->user_id;
$mec_id = $row_booking->mec_id;
$log = $row_booking->crm_update_time;
$bsource = $row_booking->source;
$veh_type = $row_booking->vehicle_type;
$veh_id = $row_booking->user_veh_id;
$service_type = $row_booking->service_type;
$service_date = $row_booking->service_date;
$shop_name = $row_booking->shop_name;
$axle_flag = $row_booking->axle_flag;
$alloted_to_id = $row_booking->crm_update_id;
$locality = $row_booking->locality;
$utm_source = $row_booking->utm_source;
$priority = $row_booking->priority;
$flag_fo = $row_booking->flag_fo;
$payment_flag = $row_booking->user_pmt_flg;
$alloted_to = $row_booking->name;
$user_name = $row_booking->user_name;
$user_mobile = $row_booking->user_mobile;
$address = $row_booking->user_lat_lng;
$home = $row_booking->user_locality;
$user_level = $row_booking->user_level;
$veh_brand = $row_booking->brand;
$veh_model = $row_booking->model;
$accept = $row_booking->b2b_acpt_flag;
$deny = $row_booking->b2b_deny_flag;


switch($status){
    case 'senttoaxle': if(($accept == 1 && $deny == 0) || ($accept == 0 && $deny == 1)){ continue 2; } break;
    case 'acceptedlead': if(($accept == 0 && $deny == 0) || ($accept == 0 && $deny == 1)){ continue 2; } break;
    case 'rejectedlead': if(($accept == 0 && $deny == 0) || ($accept == 1 && $deny == 0)){ continue 2; } break;
}

$tr = '<tr>';

$td1 = '<td><p style="float:left;padding:10px;">'.$booking_id.'</p>';
if($flag_fo == '1'){
  $td1 = $td1.'<p class="p" style="font-size:16px;color:#D81B60;float:left;padding:5px;" title="Resheduled Booking!"><i class="fa fa-recycle" aria-hidden="true"></i></p>';
  switch($priority){
    case '1': $td1 = $td1.'<p style="font-size:16px;color:#00ACC1;float:left;padding:5px;" title="Low Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';break;
    case '2': $td1 = $td1.'<p style="font-size:16px;color:#FFB300;float:left;padding:5px;" title="Medium Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';break;
    case '3': $td1 = $td1.'<p style="font-size:16px;color:#E53935;float:left;padding:5px;" title="High Priority!"><i class="fa fa-eercast" aria-hidden="true"></i></p>';break;
  }
}
if($payment_flag == '1'){
  $td1 = $td1.'<p style="font-size:16px;color:#1c57af;float:left;padding:5px;" title="Paid!"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></p>';                    
}
$td1 = $td1.'</td>';

$td2 = '<td>';
$td2 = $td2.'<div class="row">';
if($axle_flag!='1'){
  if($mec_id == '' || $mec_id == 0 || $mec_id == 1 || $service_type == '' || $service_date == '' || $veh_id == '0' ){
    $td2 = $td2.'<a href="user_details.php?t='.base64_encode("b").'&v58i4='.base64_encode($veh_id).'&bi='.base64_encode($booking_id).'" style="background-color:#ffa800;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
  }
  else{
    $td2 = $td2.'<a href="go_axle.php?bi='.$booking_id.'&t=b" style="background-color:#ffa800;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
  }
}
else if($accept == 1 && $deny == 0 ){ 
  $td2 = $td2.'<p style="background-color:#6FA3DE;padding:7px;width:30px;border-radius:18px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
}
else if($accept==0 && $deny==1){ 
  $td2 = $td2.'<p style="background-color:#D25F34;padding:7px;width:30px;border-radius:18px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
  if($super_flag == '1'){ 
    $td2 = $td2.'&nbsp;<button class="btn btn-link" data-toggle="modal" data-target="#confirm'.$booking_id.'" style="margin:0;padding:0;"><i class="fa fa-reply" aria-hidden="true" style="color:#40C4FF;"></i></button>';
  }
}
else if($accept==0 && $deny==0){ 
  $td2 = $td2.'<p style="background-color:#69ED85;padding:7px;width:30px;border-radius:18px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
  if($super_flag == '1'){ 
    $td2 = $td2.'&nbsp;<button  class="btn btn-link" data-toggle="modal" data-target="#confirm'.$booking_id.'" style="margin:0;padding:0;"><i class="fa fa-reply" aria-hidden="true" style="color:#40C4FF;"></i></button>';
  } 
}
$td2 = $td2.'<!-- Modal -->
<div class="modal fade" id="confirm'.$booking_id.'" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Undo GoAxle ( '.$booking_id.' )</h3>
      </div>
      <div class="modal-body" align="center" >
        <h4>Are you sure you want to Undo the status of this booking ? </h4>
        <div class="modal-footer" style="border-top:10px solid #fff;">
          <button type="button" class="btn" data-dismiss="modal" style="background-color:rgba(255, 82, 0, 0.67);">No</button>
          <a clas="btn btn-md" href="undo_axle.php?t='.base64_encode("b").'&bi='.base64_encode($booking_id).'" ><button type="button" class="btn" style="background-color:#69ED85;">Yes</button></a>
        </div>  
      </div>
    </div>
  </div>
</div>
</td>';

$td3 ='<td>';

switch($service_type){
      case 'general_service' :if($veh_type =='2w'){
          $td3 = $td3."General Service";
        }
        else{
          $td3 = $td3."Car service and repair";
        }
      break;
      case 'break_down':   $td3 = $td3."Breakdown Assistance"; break;
      case 'tyre_puncher':  $td3 = $td3."Tyre Puncture";  break;
      case 'other_service':  $td3 = $td3."Repair Job"; break;
      case 'car_wash':
      if($veh_type == '2w'){
         $td3 = $td3."water Wash";
      }
      else{
         $td3 = $td3."Car wash exterior";
      }break;
      case 'engine_oil':  $td3 = $td3."Repair Job"; break;
      case 'free_service':  $td3 = $td3."General Service"; break;
      case 'car_wash_both':
      case 'completecarspa':  $td3 = $td3."Complete Car Spa"; break;
      case 'car_wash_ext':  $td3 = $td3."Car wash exterior"; break;
      case 'car_wash_int':  $td3 = $td3."Interior Detailing"; break;
      case 'Doorstep car spa':  $td3 = $td3."Doorstep Car Spa"; break;
      case 'Doorstep_car_wash':  $td3 = $td3."Doorstep Car Wash"; break;
      case 'free_diagnastic':
      case 'free_diagnostic':
      case 'diagnostics':
      if($veh_type == '2w'){
         $td3 = $td3."Diagnostics/Check-up";
     }
     else{
        $td3 = $td3."Vehicle Diagnostics";
     }
         break;
      case 'water_wash':  $td3 = $td3."water Wash"; break;
      case 'exteriorfoamwash':  $td3 = $td3."Car wash exterior"; break;
      case 'interiordetailing':  $td3 = $td3."Interior Detailing"; break;
      case 'rubbingpolish':  $td3 = $td3."Car Polish"; break;
      case 'underchassisrustcoating':  $td3 = $td3."Underchassis Rust Coating"; break;
      case 'headlamprestoration':  $td3 = $td3."Headlamp Restoration"; break;
      default:  $td3 = $td3.$service_type;
}  
$td3 = $td3.'</td>';

$td4 = '<td><a href="user_details.php?t='.base64_encode("b").'&v58i4='.base64_encode($veh_id).'&bi='.base64_encode($booking_id).'"><i class="fa fa-eye" aria-hidden="true"></i></td>';    
$td5 = '<td>'.$user_name;
switch($user_level){
  case '5': $td5 = $td5.'<p style="font-size:16px;color:green;padding:5px;" title="Level 5 User"><i class="fa fa-angellist" aria-hidden="true"></i></p>';
  break;
  case '4': $td5 = $td5.'<p style="font-size:16px;color:red;padding:5px;" title="Level 4 User"><i class="fa fa-bug" aria-hidden="true"></i></p>';
  break;
  case '3': $td5 = $td5.'<p style="font-size:16px;color:#26269e;padding:5px;" title="Level 3 User"><i class="fa fa-handshake-o" aria-hidden="true"></i></p>';
  break;
  case '2': $td5 = $td5.'<p style="font-size:16px;color:brown;padding:5px;" title="Level 2 User"><i class="fa fa-anchor" aria-hidden="true"></i></p>';
  break;
  case '1': $td5 = $td5.'<p style="font-size:16px;color:orange;padding:5px;" title="Level 1 User"><i class="fa fa-tripadvisor" aria-hidden="true"></i></p>';
  break;
  default:
}
$td5 = $td5.'</td>';
$td6 = '<td>'.$user_mobile.'</td>';
$td7 = '<td>';
  if($bsource == "Hub Booking"){ 
    if($locality!=''){ 
      $td7 = $td7.$locality; 
    } 
    else if($home == ''){ 
      $td7 = $td7.$address; 
    } 
    else { 
      $td7 = $td7.$home; 
    } 
  } 
  else if($bsource == "GoBumpr App" || $bsource == "App"){ 
    $td7 = $td7.$locality; 
  } 
  else{  
    $td7 = $td7.$locality; 
  }
$td7 = $td7.'</td>';
     
$td8 = '<td>'.$veh_brand." ".$veh_model.'</td>';
$td9 = '<td>'.$shop_name.'</td>';
$td10 = '<td>';
if($service_date == "0000-00-00"){ 
  $td10 = $td10."Not Updated Yet"; 
} 
else { 
  $td10 = $td10.date('d M Y', strtotime($service_date)); 
} 
$td10 = $td10.'</td>';
     
$td11 = '<td>'.$alloted_to.'</td>';
if($src_column==1)
{
	$td11 =$td11."<td>$bsource</td><td>";
	if($utm_source == ''){ 
    $td11 =$td11."NA";  
  } 
  else{ 
    $td11 =$td11.$utm_source; 
  }
  $td11 =$td11."</td>";
}

$td12 ='<td>'.date('d M Y h.i A', strtotime($log)).'</td>';

$tr_l ='</tr>';
  
$str = $tr.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$td9.$td10.$td11.$td12.$tr_l;
$data[] = array('tr'=>$str);
}
echo $data1 = json_encode($data);
} // if
else {
  //echo $sql_booking;
  echo "no";
}
 ?>

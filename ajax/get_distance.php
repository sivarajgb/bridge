<?php
include("../config.php");

$conn = db_connect1();
$conn2 = db_connect2();
session_start();

$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$flag = $_SESSION['flag'];
$super_flag = $_SESSION['super_flag'];

$src_crm = array('crm016', 'crm017', 'crm064', 'crm036', 'crm033', 'crm034', 'crm029');
$src_column = 0;
if (in_array($crm_log_id, $src_crm)) {
	$src_column = 1;
}

$crm_city = $_SESSION['cc'];

$garage_id = $_POST['garage_id'];
$pickup_location = $_POST['pickup_location'];

//check if garage or pickup location is empty
if (!$garage_id || !$pickup_location) return false;

//Get Pickup Location Lat, Lng
$query1 = "select gl.lat, gl.lng from go_bumpr.localities as gl where localities = '$pickup_location' limit 1";
$query_result1 = mysqli_query($conn, $query1);
$row1 = mysqli_fetch_object($query_result1);

$pickup_lat = $row1->lat;
$pickup_lng = $row1->lng;

//Get Shop Lat, Lng
$query2 = "select gam.lat, gam.lng from go_bumpr.admin_mechanic_table as gam where mec_id = '$garage_id' limit 1";
$query_result2 = mysqli_query($conn, $query2);
$row2 = mysqli_fetch_object($query_result2);

$shop_lat = $row2->lat;
$shop_lng = $row2->lng;

//Calculate Distance
$query_distance = "select 6371*ACOS(COS(RADIANS('$pickup_lat'))*COS(RADIANS('$shop_lat'))*COS(RADIANS('$shop_lng')-RADIANS('$pickup_lng'))+SIN(RADIANS('$pickup_lat'))*SIN(RADIANS('$shop_lat'))) as distance";
$query_result3 = mysqli_query($conn, $query_distance);
$row3 = mysqli_fetch_object($query_result3);

echo json_encode(['success' => true, 'distance' => number_format((float)$row3->distance, 0, '.', '') . 'Km']);
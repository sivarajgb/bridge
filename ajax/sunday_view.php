<?php
include("../config.php");
$conn = db_connect2();
$conn1 = db_connect1();
session_start();
$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];

$veh_type = $_POST['veh'];
$city = $_POST['city'];
$loc = $_POST['loc'];
$sunday = $_POST['sunday'];

$_SESSION['crm_city'] = $city;


//0-all && 1-particular
class shops {
  private $veh;
  private $loc;

  function __construct($veh, $loc){
    $this->veh = $veh;
    $this->loc = $loc;
  }
  function v0_l0(){
    return "";
  }
  function v0_l1(){
    return "AND m.b2b_address4 ='$this->loc'";
  }
  function v1_l0(){
    return "AND m.b2b_vehicle_type = '$this->veh'";
  }
  function v1_l1(){
    return "AND m.b2b_vehicle_type = '$this->veh' AND m.b2b_address4 ='$this->loc'";
  }
}

$shops_obj = new shops($veh_type, $loc);

$veh_val = $veh_type=="all" ? "0" : "1";
$loc_val = $loc=="" ? "0" : "1";

$cond = $shops_obj->{"v{$veh_val}_l{$loc_val}"}();
$cond = $cond.($sunday == 'all' ? "AND gm.sunday_toggle='0'" : "AND gm.sunday_toggle='1'");

$sql_shops = "SELECT DISTINCT m.b2b_shop_id,m.b2b_shop_name,m.b2b_new_flg,gm.address4,gm.exception_stage,gm.mec_id as gbpr_mec_id,gm.pickup_available,gm.door_step FROM b2b_mec_tbl as m LEFT JOIN go_bumpr.admin_mechanic_table as gm ON m.b2b_shop_id = gm.axle_id WHERE m.b2b_cred_model=1 AND m.b2b_vehicle_type!='tyres' AND ((gm.status=0) OR (gm.exception_stage=-1)) AND m.b2b_address5 ='$city' {$cond} GROUP BY m.b2b_shop_id ORDER BY m.b2b_shop_name ASC  ";
 //echo $sql_shops;die;
$res_shops = mysqli_query($conn,$sql_shops);

$no = 0;

$count = mysqli_num_rows($res_shops);
if($count >0){
while($row_shops = mysqli_fetch_object($res_shops)){
$shop_id = $row_shops->b2b_shop_id;
$shop_name = $row_shops->b2b_shop_name;
$credits = $row_shops->b2b_credits;
$exception_stage = $row_shops->exception_stage;
$gbpr_mec_id = $row_shops->gbpr_mec_id;
$location = $row_shops->address4;
$b2b_new_flg= $row_shops->b2b_new_flg;
$b2b_leads= $row_shops->b2b_leads;
$re_leads= $row_shops->b2b_re_leads;
$nre_leads= $row_shops->b2b_non_re_leads;
$pickup = $row_shops->pickup_available;
$doorstep = $row_shops->door_step;
if ($b2b_new_flg==1) {
  // if($veh_type=="2w"){
  //   $credits=0;
  // }else{
  //   $credits="L".$b2b_leads;
  // }

}

switch($exception_stage){
  //case '1': $icon = '<i style="padding-left:5px;font-size:25px;color:FFBA00" title="1st Warning!"  class="fa fa-exclamation"></i>';break;
  case '1': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exception1.svg">';break;
  case '2': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exception2.svg">';break;
  case '3': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exception3.svg">';break;
  case '-1': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="'.$booking_ids.'" src="/images/exceptionlock.svg">';break;
  default:$icon = '';
}

if($doorstep==1 && ($pickup ==1 || $pickup == 0)){
  $mode = "Doorstep";
}else if($doorstep==0 && $pickup ==1 ){
  $mode = "Pickup only";
}else if($doorstep==0 && $pickup ==0 ){
  $mode = "None";
}
  ?>
    <tr>
      <td><?php echo $no=$no+1 ; ?></td>
      <td style="display:none;"><?php echo $gbpr_mec_id;?></td>
       <td><div class="rad"><label for="<?php echo $shop_id;?>"><?php echo $shop_name.$icon; ?></label></div></td>
       <td><?php echo $location; ?></td>
       
      <td><?php echo $mode; ?></td>
      <td style="text-align:center;">
          <button style="background: rgb(223, 117, 20);color:white;border:none;border-radius:5px;" type="button" class="use-address"><i style="font-size:20px" class="fa fa-check"></i></button>
          <button style="background: rgb(202, 60, 60);color:white;border:none;border-radius:5px;" type="button" class="remove-address"><i style="font-size:20px" class="fa fa-close"></i></button>
        </td>
    </tr>
  <?php 
}
} // if
else {
  echo "<h3 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h3>";
}
 ?>

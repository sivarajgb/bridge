<?php
include("../config.php");
$conn = db_connect2();
$conn1 = db_connect1();
session_start();

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];


$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$today = date('Y-m-d');

$city = $_POST['city'];

$_SESSION['crm_city'] = $city;


?>
<style>
.textarea {
    height: 2em;
    width: 100%;
    padding: 3px;
    transition: all 0.5s ease;
}

.textarea:focus {
    height: 15em;
}
</style>
<div id="division" style="margin-top:10px;margin-left:35px;">
<div id="div1" style="margin-left:-15px;">
<table class="table table-striped table-bordered tablesorter table-hover results" id="table">
<thead style="background-color:#B2DFDB;align:center;display: table-header-group;">
<th style="text-align:center;">S.No <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Booking ID<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Name </th>
<th style="text-align:center;">Mobile No.<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Shop Name<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Service Type<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Rating <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Feedback <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">User Comments <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Status <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
</thead>
<tbody id="tbody">

<?php

$start = strtotime($startdate);
$end = strtotime($enddate);
$noofdays = date('t',strtotime($start));
$no = 0;
// Loop between timestamps, 24 hours at a time
for ( $i = $start; $i <= $end; $i = $i + 86400 ) {
    $thisDate = date('d M Y', $i);
    $day = date('l', strtotime($thisDate));

    $date = date('Y-m-d', $i);

    $enc_date=base64_encode($date);

if($city == "all"){
    $sql_b2b = "SELECT g.rating_id,g.booking_id,g.rating,b.feedback as comments,g.feedback,g.name,g.user_id,r.mobile_number,b.shop_name,b.service_type,g.brand,g.model,g.mec_id,g.axle_booking_id FROM go_bumpr.user_rating_tbl as g LEFT JOIN go_bumpr.user_register as r ON r.reg_id = g.user_id LEFT JOIN go_bumpr.user_booking_tb as b ON g.booking_id = b.booking_id WHERE g.status='1' AND g.reject_flag!='1' AND DATE(g.log)='$date';";    
}
else{
    $sql_b2b = "SELECT g.rating_id,g.booking_id,g.rating,b.feedback as comments,g.feedback,g.name,g.user_id,r.mobile_number,b.shop_name,b.service_type,g.brand,g.model,g.mec_id,g.axle_booking_id,g.status,g.reject_flag FROM go_bumpr.user_rating_tbl as g  LEFT JOIN go_bumpr.user_register as r ON r.reg_id = g.user_id LEFT JOIN go_bumpr.user_booking_tb as b ON g.booking_id = b.booking_id WHERE DATE(g.log)='$date' AND b.city = '$city';";    
}
    $res_b2b = mysqli_query($conn,$sql_b2b);


    while($row_b2b = mysqli_fetch_object($res_b2b)){
        $rating_id = $row_b2b->rating_id;
        $booking_id = $row_b2b->booking_id;
        $rating = $row_b2b->rating;
        $feedback = $row_b2b->feedback;
        $comments = $row_b2b->comments;		
        $name = $row_b2b->name;
        $mobile_number = $row_b2b->mobile_number;
        $shop_name = $row_b2b->shop_name;
        $service_type = $row_b2b->service_type;
        $brand = $row_b2b->brand;
        $model = $row_b2b->model;
        $mec_id = $row_b2b->mec_id;
        $b2b_booking_id = $row_b2b->axle_booking_id;
        $status = $row_b2b->status;
        $reject_flag = $row_b2b->reject_flag;
		
		
		


        ?>
        <tr>
    <td style="text-align:center;"><?php echo $no = $no+1; ?></td>
    <td style="text-align:center;"><?php echo $booking_id; ?></td>
    <td style="text-align:center;"><?php echo $name;?></td>
    <td style="text-align:center;"><?php echo $mobile_number; ?></td>
    <td style="text-align:center;"><?php echo $shop_name; ?></td>
    <td style="text-align:center;"><?php echo $service_type; ?></td>
    <td style="text-align:center;"><div id="rateYo<?php echo $rating_id; ?>" data-toggle="tooltip" data-placement="right" title="<?php echo $rating; ?> "></div></td>
    <td style="text-align:center;"><?php echo $feedback; ?></td>
    <td style="text-align:center;"><?php echo $comments; ?></td>    
	<td style="text-align:center;">
	<?php if($status==0&&$reject_flag==0)
	{
		echo "Approved";
	}
	elseif($status==1&&$reject_flag==0){
		?>
	<i data-toggle="modal" data-target="#approveModal<?php echo $rating_id; ?>" class='fa fa-pencil' aria-hidden='true' style='cursor:pointer;float:left;font-size:24px;color: green!important;' title='Approve'></i>
	<i data-toggle="modal" data-target="#rejectModal<?php echo $rating_id; ?>" class='fa fa-trash' aria-hidden='true' style='float:right;cursor:pointer;font-size:24px;color: red!important;' title='Reject'></i>
	<?php
	}elseif($status=1&&$reject_flag==1){
		echo "Rejected"; 
	}?>
	</td>
    </tr>
	<?php
	$sql_o_rat_gb = "SELECT avg(rating) as rating FROM go_bumpr.user_rating_tbl WHERE mec_id='$mec_id' AND status!='1' AND rating!='0'";
	$res_o_rat_gb = mysqli_query($conn1,$sql_o_rat_gb) or die(mysqli_error($conn1));

	$row_o_rat = mysqli_fetch_object($res_o_rat_gb);
	$overall_rating_gb = $row_o_rat->rating;
	$avg_rating = round($overall_rating_gb,2);
	?>
<div id="approveModal<?php echo $rating_id; ?>" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
	 <form action="approve_feedback.php" method="post" id="form<?php echo $rating_id;?>">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Approve feedback ( <?php echo $booking_id; ?> )</h4>
      </div>
      <div class="modal-body">
		<div class="row">
		<div class="col-lg-7">
		<div class="row">
		<div class="col-lg-12">
		<strong style="font-size:16px;">Booking ID  </strong><span style="float:right;"><?php echo $booking_id; ?></span><br><br>
		<strong style="font-size:16px;">Name  </strong><span style="float:right;"><?php echo $name; ?></span><br><br>
		<strong style="font-size:16px;">Mobile Number  </strong><span style="float:right;"><?php echo $mobile_number; ?></span><br><br>
		<?php 
		if($brand != '' && $model != '')
		{
			echo "<strong style='font-size:16px;'>Brand Model  </strong><span style='float:right;'>$brand $model</span><br><br>";		
		}
		?>
		<strong style="font-size:16px;">Service Type  </strong><span style="float:right;"><?php echo $service_type; ?></span><br><br>
		<strong style="font-size:16px;">Service Partner  </strong><span style="float:right;"><?php echo $shop_name; ?></span><br><br>
		</div>
		</div>
		</div>
		<div class="col-lg-1" style="border-right: 1px #aaa solid;height: 260px;padding-left: 0px;padding-right: 0px;width: 11px;" >
		</div>
		<div class="col-lg-4" style="width:230px;">
		<div id="rate<?php echo $rating_id; ?>" style="margin-top: 20px;margin-bottom: 17px;"></div>
        <label style= "margin-top: 50px;">Feedback</label><i id="edit<?php echo $rating_id; ?>" class='fa fa-pencil' aria-hidden='true' style='cursor:pointer;padding-left:15px;' title='Edit'></i>
		<textarea class = "textarea" style="resize:none;" name="approved_feedback<?php echo $rating_id; ?>" id="approved_feedback<?php echo $rating_id; ?>" class="form-control" rows="2" cols="40" placeholder="Feedback..." readonly disabled><?php echo $feedback;?></textarea>
		</div>
		</div>
      </div>
      <div class="modal-footer">
		<input type="hidden" name="bi<?php echo $rating_id; ?>" id="bi<?php echo $rating_id; ?>" value="<?php echo $booking_id;?>">
		<input type="hidden" name="ri<?php echo $rating_id; ?>" id="ri<?php echo $rating_id; ?>" value="<?php echo $rating_id;?>">
		<input type="hidden" name="mi<?php echo $rating_id; ?>" id="mi<?php echo $rating_id; ?>" value="<?php echo $mec_id;?>">
		<input type="hidden" name="b2b_booking_id<?php echo $rating_id; ?>" id="b2b_booking_id<?php echo $rating_id; ?>" value="<?php echo $b2b_booking_id;?>">
		<input type="hidden" name="action<?php echo $rating_id; ?>" id="action<?php echo $rating_id; ?>" value="approve">
		<div style="width: 63%;">
		<i id="submit<?php echo $rating_id;?>" class="fa fa-check-circle" aria-hidden="true" style="cursor:pointer;font-size: 40px;color: green!important;padding-right: 20px;" title="Approve!"></i>
		<i id="RejectSubmit<?php echo $rating_id;?>" class="fa fa-times-circle" aria-hidden="true" style="cursor:pointer;font-size: 40px;color:red!important;padding-left: 20px;" title="Reject!"></i></div>
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
      </div>
	 </form>
    </div>

  </div>
</div>

<div id="rejectModal<?php echo $rating_id; ?>" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reject feedback ( <?php echo $booking_id; ?> )</h4>
      </div>
      <div class="modal-body">
       <div class="row">
		<div class="col-lg-7">
		<div class="row">
		<div class="col-lg-12">
		<strong style="font-size:16px;">Booking ID  </strong><span style="float:right;"><?php echo $booking_id; ?></span><br><br>
		<strong style="font-size:16px;">Name  </strong><span style="float:right;"><?php echo $name; ?></span><br><br>
		<strong style="font-size:16px;">Mobile Number  </strong><span style="float:right;"><?php echo $mobile_number; ?></span><br><br>
		<?php 
		if($brand != '' && $model != '')
		{
			echo "<strong style='font-size:16px;'>Brand Model  </strong><span style='float:right;'>$brand $model</span><br><br>";		
		}
		?>
		<strong style="font-size:16px;">Service Type  </strong><span style="float:right;"><?php echo $service_type; ?></span><br><br>
		<strong style="font-size:16px;">Service Partner  </strong><span style="float:right;"><?php echo $shop_name; ?></span><br><br>
		</div>
		</div>
		</div>
		<div class="col-lg-1" style="border-right: 1px #aaa solid;height: 260px;padding-left: 0px;padding-right: 0px;width: 11px;" >
		</div>
		<div class="col-lg-4" style="width:230px;">
		<div id="rateR<?php echo $rating_id; ?>" style="margin-top: 20px;margin-bottom: 17px;"></div>
        <textarea style="margin-top: 50px;resize:none;" name="rejected_feedback<?php echo $rating_id; ?>" id="rejected_feedback<?php echo $rating_id; ?>" class="form-control" rows="5" cols="40" placeholder="Feedback..." readonly><?php echo $feedback;?></textarea>
		</div>
		</div>
      </div>
      <div class="modal-footer">
		<input type="hidden" name="actionR<?php echo $rating_id; ?>" id="actionR<?php echo $rating_id; ?>" value="reject">
		<input type="hidden" name="riR<?php echo $rating_id; ?>" id="riR<?php echo $rating_id; ?>" value="<?php echo $rating_id;?>">
		<div style="width: 54%;"><i id="submitR<?php echo $rating_id;?>" class="fa fa-trash" aria-hidden="true" style="pointer:cursor;font-size: 40px;color: red!important;" title="Reject!"></i></div>
        
      </div>
    </div>

  </div>
</div>

	<script>
	$( "#edit<?php echo $rating_id;?>").click(function(){
		$("#approved_feedback<?php echo $rating_id;?>").prop("disabled", false);
		$("#approved_feedback<?php echo $rating_id;?>").prop("readonly", false);
		$("#approved_feedback<?php echo $rating_id;?>").focus();
	});
	$( "#submit<?php echo $rating_id;?>").click(function() {
		var feedback = $("#approved_feedback<?php echo $rating_id; ?>").val();
		var bi = $("#bi<?php echo $rating_id; ?>").val();
		var ri = $("#ri<?php echo $rating_id; ?>").val();
		var mi = $("#mi<?php echo $rating_id; ?>").val();
		var action = $("#action<?php echo $rating_id; ?>").val();
		var b2b_booking_id = $("#b2b_booking_id<?php echo $rating_id; ?>").val();
	$.ajax({
	    url : "approve_feedback.php",  // create a new php page to handle ajax request
	    type : "POST",
		data : {'feedback':feedback,'bi':bi,'ri':ri,'mi':mi,'b2b_booking_id':b2b_booking_id,'action':action},
		success : function(data) {
			location.href = "https://bridge.gobumpr.com/newbridge/feedback_moderation.php";
		}
	});
	});
	$( "#submitR<?php echo $rating_id;?>").click(function() {
		var ri = $("#riR<?php echo $rating_id; ?>").val();
		var action = $("#actionR<?php echo $rating_id; ?>").val();
	$.ajax({
	    url : "approve_feedback.php",  // create a new php page to handle ajax request
	    type : "POST",
		data : {'ri':ri,'action':action},
		success : function(data) {
			location.reload();
		}
	});
	});
	$( "#RejectSubmit<?php echo $rating_id;?>").click(function() {
		var ri = $("#riR<?php echo $rating_id; ?>").val();
		var action = $("#actionR<?php echo $rating_id; ?>").val();
	$.ajax({
	    url : "approve_feedback.php",  // create a new php page to handle ajax request
	    type : "POST",
		data : {'ri':ri,'action':action},
		success : function(data) {
			location.reload();
		}
	});
	});
$(function () {
  ratings= <?php echo $rating; ?>;
  avg_rating= <?php echo $avg_rating; ?>;
 if(ratings>0&&ratings<1)
  {
	  ratedfills = "#c60714";
  }
  if(ratings==1)
  {
	  ratedfills = "#cd1c26";
  }  
  if(ratings>1&&ratings<2)
  {
	  ratedfills = "#DE1D0F";
  } 
  if(ratings==2)
  {
	  ratedfills = "#ff7800";
  }
  if(ratings>2&&ratings<3)
  {
	  ratedfills = "#FFBA00";
  } 
  if(ratings==3)
  {
	  ratedfills = "#cdd614";
  }
  if(ratings>3&&ratings<4)
  {
	  ratedfills = "#9ACD32";
  }  
  if(ratings==4)
  {
	  ratedfills = "#5BA829";
  } 
  if(ratings>4&&ratings<5)
  {
	  ratedfills = "#3F7E00";
  }
  if(ratings==5)
  {
	  ratedfills = "#305D02";
  }	
  $("#rateYo<?php echo $rating_id; ?>").rateYo({
    rating: ratings,
    readOnly: true,
	starWidth: "20px",
	normalFill: "#ddd",
    ratedFill: ratedfills
  });
  $("#rate<?php echo $rating_id; ?>").rateYo({
	rating: ratings,
    readOnly: true,
	starWidth: "36px",
	normalFill: "#ddd",
    ratedFill: ratedfills
 });
$("#rateR<?php echo $rating_id; ?>").rateYo({
	rating: ratings,
    readOnly: true,
	starWidth: "36px",
	normalFill: "#ddd",
    ratedFill: ratedfills
 });
});
</script>

		
		<?php
		    } // while
}?>
</tbody>
</table>
</div>
<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#table").tablesorter( {sortList: [[0,0], [1,0]]} );
    }
);
</script>
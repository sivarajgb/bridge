<?php
//error_reporting(E_ALL); ini_set('display_errors', 1);
include("../config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();
$today=date('Y-m-d');

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enc_date = base64_encode($startdate);
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$vehicle = $_POST['vehicle_type'];
$master = $_POST['master'];
$city = $_POST['city'];

$_SESSION['crm_city'] = $city;

$enc_veh = base64_encode($vehicle);


$total_veh = 0;
$total_credits =0;

?>
<div style="clear:both;">
<div align="center" id = "table" style="width:47%; margin-left:20px;margin-right:20px;float:left;">
<table id="example1" class="table table-striped table-bordered tablesorter table-hover results" style="max-width:95%;">
<thead style="background-color: #D3D3D3;">
<?php if($master == "all"){ ?>
    <th style="text-align:center;">ServiceType</th>
<?php } 
else{ ?>
<th style="text-align:center;">MasterServiceType</th>
<?php } ?>
<th style="text-align:center;">GoAxles <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Credits <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Completed <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">End Conv. <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
</thead>
<tbody id="tbody">
<?php

switch($master){
    case 'all':   
    $sql_st = "SELECT DISTINCT b2b_service_type FROM b2b_booking_tbl WHERE b2b_vehicle_type='$vehicle' AND DATE(b2b_log) BETWEEN '$startdate' and '$enddate' ORDER BY b2b_service_type ASC";
    $res_st = mysqli_query($conn2,$sql_st);
    while($row_st = mysqli_fetch_object($res_st)){
        $service_type = $row_st->b2b_service_type;

        $sql_booking = $city == "all" ? "SELECT COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' AND s.b2b_acpt_flag='1' AND b.b2b_source='lead_convert')) THEN 1 ELSE 0 END ) AS completed_count FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id=g.booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_flag!='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_service_type='$service_type' AND b.b2b_vehicle_type='$vehicle' AND b.b2b_shop_id NOT IN (1014,1035,1670)" : "SELECT COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' AND s.b2b_acpt_flag='1' AND b.b2b_source='lead_convert')) THEN 1 ELSE 0 END ) AS completed_count FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_flag!='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_service_type='$service_type' AND b.b2b_vehicle_type='$vehicle' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.city='$city'";
        $res_booking = mysqli_query($conn2,$sql_booking);

        $row_booking = mysqli_fetch_array($res_booking);
        $no_of_vehicles = $row_booking['bookings'];
        $no_of_credits = $row_booking['amount']/100;
        $no_of_completed_s = $row_booking['completed_count'] >0 ? $row_booking['completed_count'] : 0 ;

        if($no_of_vehicles == '0' && $no_of_credits == '0'){
            continue;
        }
        
        $end_conversion_s = $no_of_vehicles!= '0' ? round(($no_of_completed_s/$no_of_vehicles)*100,1) : '0';
    

        $total_veh = $total_veh+$no_of_vehicles;
        $total_credits = $total_credits+$no_of_credits;
        $total_completed_s = $total_completed_s+$no_of_completed_s;
        $total_end_s = $total_veh!= '0' ? round(($total_completed_s/$total_veh)*100,1) : '0';
    
        
        ?>
        <tr>
            <td><?php echo $service_type ; ?></td>
            <td style="text-align:center;"><?php echo $no_of_vehicles ; ?></td>
            <td style="text-align:center;"><?php echo $no_of_credits ; ?></td>
            <td style="text-align:center;"><?php echo $no_of_completed_s ; ?></td>
            <?php
            switch(true){
                case ($end_conversion_s >= 70): ?><td style="text-align:center;background-color:#00ff4e70;"><?php echo $end_conversion_s.' %' ; ?></td> <?php break;
                case ($end_conversion_s <70 && $end_conversion_s>= 60): ?><td style="text-align:center;background-color:#ffa800ad;"><?php echo $end_conversion_s.' %' ; ?></td> <?php break;
                case ($end_conversion_s <60 && $end_conversion_s>= 40): ?><td style="text-align:center;background-color:#a25506a6;"><?php echo $end_conversion_s.' %' ; ?></td> <?php break;
                case ($end_conversion_s < 40): ?><td style="text-align:center;background-color:#ff2f009c;"><?php echo $end_conversion_s.' %' ; ?></td> <?php break;
               default: ?><td style="text-align:center;"><?php echo $end_conversion_s.' %' ; ?></td> <?php
            }
            ?>        
        </tr>
        <?php
        }
        break;
    case 'master': 
    $sql_st = $city == "all" ? "SELECT DISTINCT g.master_service FROM go_bumpr.go_axle_service_price_tbl as g INNER JOIN b2b.b2b_booking_tbl as b ON (g.service_type=b.b2b_service_type AND g.type=b.b2b_vehicle_type) WHERE b.b2b_vehicle_type='$vehicle' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' ORDER BY g.master_service ASC" : "SELECT DISTINCT g.master_service FROM go_bumpr.go_axle_service_price_tbl as g INNER JOIN b2b.b2b_booking_tbl as b ON (g.service_type=b.b2b_service_type AND g.type=b.b2b_vehicle_type) LEFT JOIN go_bumpr.user_booking_tb as gb ON b.gb_booking_id=gb.booking_id WHERE b.b2b_vehicle_type='$vehicle' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND gb.city='$city' ORDER BY g.master_service ASC";
    $res_st = mysqli_query($conn1,$sql_st);   
    while($row_st = mysqli_fetch_array($res_st)){
            
        $master_service = $row_st['master_service'];

        $sql_services = "SELECT service_type FROM go_axle_service_price_tbl WHERE master_service='$master_service' AND type='$vehicle'";
        $res_services = mysqli_query($conn1,$sql_services);
        $services = '';
        while($row_services=mysqli_fetch_array($res_services)){
            if($services == ""){
                $services = "'".$row_services['service_type']."'";
            }
            else{
                $services = $services.",'".$row_services['service_type']."'";
            }
        }
        //echo $services;

        //$service_type = $row_st->b2b_service_type;

        $sql_booking = $city == "all" ? $sql_booking = "SELECT COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' AND s.b2b_acpt_flag='1' AND b.b2b_source='lead_convert')) THEN 1 ELSE 0 END ) AS completed_count FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id=g.booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_flag!='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_service_type IN($services) AND b.b2b_vehicle_type='$vehicle' AND b.b2b_shop_id NOT IN (1014,1035,1670)" : "SELECT COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' AND s.b2b_acpt_flag='1' AND b.b2b_source='lead_convert')) THEN 1 ELSE 0 END ) AS completed_count FROM b2b_booking_tbl as b INNER JOIN b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id=g.booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_flag!='1' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_service_type IN($services) AND b.b2b_vehicle_type='$vehicle' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.city='$city'";
        $res_booking = mysqli_query($conn2,$sql_booking);

        $row_booking = mysqli_fetch_array($res_booking);
        $no_of_vehicles = $row_booking['bookings'];
        $no_of_credits = $row_booking['amount']/100;
        $no_of_completed_s = $row_booking['completed_count'] >0 ? $row_booking['completed_count'] : 0;

        if($no_of_vehicles == '0' && $no_of_credits == '0'){
            continue;
        }  
        $end_conversion_s = $no_of_vehicles!= '0' ? round(($no_of_completed_s/$no_of_vehicles)*100,1) : '0';
        
        $total_veh = $total_veh+$no_of_vehicles;
        $total_credits = $total_credits+$no_of_credits;
        $total_completed_s = $total_completed_s+$no_of_completed_s;
        $total_end_s = $total_veh!= '0' ? round(($total_completed_s/$total_veh)*100,1) : '0';
        
        ?>
        <tr>
            <td><?php echo $master_service ; ?></td>
            <td style="text-align:center;"><?php if($no_of_vehicles == ''){ echo '0'; } else { echo $no_of_vehicles ;} ?></td>
            <td style="text-align:center;"><?php echo $no_of_credits ; ?></td>
            <td style="text-align:center;"><?php echo $no_of_completed_s ; ?></td>
            <?php
            switch(true){
                case ($end_conversion_s >= 70): ?><td style="text-align:center;background-color:#00ff4e70;"><?php echo $end_conversion_s.' %' ; ?></td> <?php break;
                case ($end_conversion_s <70 && $end_conversion_s>= 60): ?><td style="text-align:center;background-color:#ffa800ad;"><?php echo $end_conversion_s.' %' ; ?></td> <?php break;
                case ($end_conversion_s <60 && $end_conversion_s>= 40): ?><td style="text-align:center;background-color:#a25506a6;"><?php echo $end_conversion_s.' %' ; ?></td> <?php break;
                case ($end_conversion_s < 40): ?><td style="text-align:center;background-color:#ff2f009c;"><?php echo $end_conversion_s.' %' ; ?></td> <?php break;
               default: ?><td style="text-align:center;"><?php echo $end_conversion_s.' %' ; ?></td>  <?php
            }
            ?>              
        </tr>
        <?php
        }
    break;
}
?>
</tbody>
<tbody class="avoid-sort">
<tr>
<td style="text-align:center;">Total</td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_veh; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_credits; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_completed_s; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_end_s.' %'; ?></td>
</tr>
</tbody>
</table>
</div>
<div style="width:47%; margin-left:20px;margin-right:20px;float:left;">
<table id="example2" class="table table-striped table-bordered tablesorter table-hover results" style="max-width:95%;">
<thead style="background-color: #D3D3D3;">
<th style="text-align:center;">Alloted To</th>
<th style="text-align:center;">GoAxles <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Credits <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Completed <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">End Conv. <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
</thead>
<tbody id="tbody">
<?php
 $total_veh_pr = 0;
 $total_credits_pr = 0;
 $total_completed = 0;
 $total_end = 0;
$sql_pr = "SELECT DISTINCT crm_log_id,name FROM crm_admin ORDER BY name ASC";
$res_pr = mysqli_query($conn1,$sql_pr); 

while($row_pr = mysqli_fetch_object($res_pr)){
    $crm_person_id = $row_pr->crm_log_id;
    $crm_person_name = $row_pr->name;

    $sql_booking_pr = $city == "all" ? "SELECT COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' AND b.b2b_source='lead_convert')) THEN 1 ELSE 0 END ) AS completed_count FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.crm_update_id='$crm_person_id' AND g.vehicle_type='$vehicle'" : "SELECT COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' AND b.b2b_source='lead_convert')) THEN 1 ELSE 0 END ) AS completed_count FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.city='$city' AND g.crm_update_id='$crm_person_id' AND g.vehicle_type='$vehicle'";
    $res_booking_pr = mysqli_query($conn1,$sql_booking_pr);

    $row_booking_pr = mysqli_fetch_array($res_booking_pr);
    $no_of_vehicles_pr = $row_booking_pr['bookings'];
    $no_of_credits_pr = $row_booking_pr['amount']/100;

    if($no_of_vehicles_pr == '0' && $no_of_credits_pr == '0'){
        continue;
    }

    $no_of_completed = $row_booking_pr['completed_count'] > 0 ? $row_booking_pr['completed_count'] : 0 ;

    $end_conversion = $no_of_vehicles_pr!= '0' ? round(($no_of_completed/$no_of_vehicles_pr)*100,1) : '0'; 
    
    $total_veh_pr = $total_veh_pr+$no_of_vehicles_pr;
    $total_credits_pr = $total_credits_pr+$no_of_credits_pr;
    $total_completed = $total_completed + $no_of_completed;
    $total_end = $total_veh_pr!= '0' ? round(($total_completed/$total_veh_pr)*100,1) : '0'; 

?>
<tr>
    <td id="<?php echo $crm_person_id; ?>"><?php echo $crm_person_name ; ?></td>
    <td style="text-align:center;"><?php echo $no_of_vehicles_pr ; ?></td>
    <td style="text-align:center;"><?php echo $no_of_credits_pr ; ?></td>
    <td style="text-align:center;"><?php echo $no_of_completed ; ?></td>
    <?php
    switch(true){
        case ($end_conversion >= 70): ?><td style="text-align:center;background-color:#00ff4e70;"><?php echo $end_conversion.' %' ; ?></td> <?php break;
        case ($end_conversion <70 && $end_conversion>= 60): ?><td style="text-align:center;background-color:#ffa800ad;"><?php echo $end_conversion.' %' ; ?></td> <?php break;
        case ($end_conversion <60 && $end_conversion>= 40): ?><td style="text-align:center;background-color:#a25506a6;"><?php echo $end_conversion.' %' ; ?></td> <?php break;
        case ($end_conversion < 40): ?><td style="text-align:center;background-color:#ff2f009c;"><?php echo $end_conversion.' %' ; ?></td> <?php break;
        default: ?><td style="text-align:center;"><?php echo $end_conversion.' %' ; ?></td>  <?php
    }
    ?> 
</tr>
<?php
}

$trophy_start = date('Y-m-01 00:00:00',strtotime($startdate)); 
$trophy_end = date('Y-m-t 12:59:59',strtotime($startdate));

$counter_trophy =0;

$sql_trophy = $city == "all" ? "SELECT COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,g.crm_update_id as crm_id FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$trophy_start' and '$trophy_end' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.crm_update_id NOT IN ('crm003','crm036') AND g.vehicle_type='$vehicle' GROUP BY g.crm_update_id ORDER BY bookings DESC,amount DESC" : "SELECT COUNT(b.b2b_booking_id) as bookings,SUM(b.b2b_credit_amt) as amount,g.crm_update_id as crm_id FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$trophy_start' and '$trophy_end' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.crm_update_id NOT IN ('crm003','crm036') AND g.vehicle_type='$vehicle' AND g.city='$city' GROUP BY g.crm_update_id ORDER BY bookings DESC,amount DESC"; 
$res_trophy = mysqli_query($conn1,$sql_trophy);

while($row_trophy = mysqli_fetch_array($res_trophy)){
    $crm_id = $row_trophy['crm_id'];
    $sql_name = "SELECT name FROM crm_admin WHERE crm_log_id='$crm_id'";
    $res_name = mysqli_query($conn1,$sql_name);
    $row_name = mysqli_fetch_array($res_name);
    $name = $row_name = $row_name['name'];
    ?>
    <script>
    $(document).ready(function() {
        //first
        var id = '<?php echo $crm_id; ?>';
       // console.log(id);
        var counter = '<?php  echo $counter_trophy; ?>';
        switch(counter){
            case '0':$("#<?php echo $crm_id; ?>").append('&nbsp;<i class="fa fa-trophy" aria-hidden="true" style="font-size:19px;color:#D4AF37;"></i>');break;
            case '1':$("#<?php echo $crm_id; ?>").append('&nbsp;<i class="fa fa-trophy" aria-hidden="true" style="font-size:19px;color:#A9A9A9;"></i>');break;
            case '2':$("#<?php echo $crm_id; ?>").append('&nbsp;<i class="fa fa-trophy" aria-hidden="true" style="font-size:19px;color:#b87333;"></i>');break;
        }    
    });
    </script>
    <?php
    $counter_trophy = $counter_trophy+1;
    if($counter_trophy == '3'){
        break;
    }
}

$counter_shield =0;

$sql_shield = $city == "all" ? "SELECT COUNT(b.b2b_booking_id) as bookings,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' AND b.b2b_source='lead_convert')) THEN 1 ELSE 0 END ) AS end_count ,g.crm_update_id as crm_id FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$trophy_start' and '$trophy_end' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.crm_update_id NOT IN ('crm003','crm036') AND g.vehicle_type='$vehicle' GROUP BY g.crm_update_id HAVING bookings > 24 " : "SELECT COUNT(b.b2b_booking_id) as bookings,SUM(CASE WHEN ((g.service_status='Completed' OR g.service_status='In Progress') OR (b.b2b_check_in_report='1' AND b.b2b_source='lead_convert')) THEN 1 ELSE 0 END ) AS end_count ,g.crm_update_id as crm_id FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id WHERE s.b2b_acpt_flag='1' AND b.b2b_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$trophy_start' and '$trophy_end' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND g.crm_update_id NOT IN ('crm003','crm036') AND g.vehicle_type='$vehicle' AND g.city='$city' GROUP BY g.crm_update_id HAVING bookings > 24 "; 
$res_shield = mysqli_query($conn1,$sql_shield);

if(mysqli_num_rows($res_shield)>0){
    while($row_shield = mysqli_fetch_array($res_shield)){
        $crm_id = $row_shield['crm_id'];
        $bookings = $row_shield['bookings'];
        $completed = $row_shield['end_count'];
        $endconv = round(($completed/$bookings)*100,1);

        $list[] = array("crm_id"=>$crm_id, "endconv"=> $endconv);
    }
    foreach ($list as $key => $row) {
        $val[$key] = $row['endconv'];
    }
    
    // Sort the data with volume descending, edition ascending
    // Add $data as the last parameter, to sort by the common key
    array_multisort($val, SORT_DESC, $list);
    //arsort($list);
    //var_dump($list);

    foreach($list as $row){
        $crm_id = $row['crm_id'];
        ?>
        <script>
        $(document).ready(function() {
            //first
            var id = '<?php echo $crm_id; ?>';
            //console.log(id);
            var counter = '<?php  echo $counter_shield; ?>';
            //console.log(counter);
            switch(counter){
                case '0':$("#<?php echo $crm_id; ?>").append('&nbsp;<i class="fa fa-space-shuttle" aria-hidden="true" style="font-size:19px;color:#D4AF37;"></i>');break;
                case '1':$("#<?php echo $crm_id; ?>").append('&nbsp;<i class="fa fa-space-shuttle" aria-hidden="true" style="font-size:19px;color:#A9A9A9;"></i>');break;
                case '2':$("#<?php echo $crm_id; ?>").append('&nbsp;<i class="fa fa-space-shuttle" aria-hidden="true" style="font-size:19px;color:#b87333;"></i>');break;
            }    
        });
        </script>
        <?php
        $counter_shield = $counter_shield+1;
        if($counter_shield == '3'){
            break;
        }
    }

}


?>

</tbody>
<tbody class="avoid-sort">
<tr>
<td style="text-align:center;">Total</td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_veh_pr; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_credits_pr; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_completed; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_end.' %'; ?></td>
</tr>
</tbody>
</table>
</div>
</div>
<div style="width:90%; margin-left:20px;margin-left:60px;clear:both;">
<table id="example3" class="table table-striped table-bordered tablesorter table-hover results" style="max-width:95%;">
<thead style="background-color: #D3D3D3;">
<th style="text-align:center;">Service Center</th>
<th style="text-align:center;">Locality</th>
<th style="text-align:center;">VehiclesSent <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Credits <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Accepted <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Rejected <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Idle <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Completed <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">End Conv. <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
</thead>
<tbody id="tbody">
<?php

$sql_garage = $city == "all" ? "SELECT DISTINCT m.b2b_shop_id,m.b2b_shop_name,m.b2b_address4,c.b2b_credits,c.b2b_partner_flag,c.b2b_flag,gm.exception_stage FROM b2b_mec_tbl as m INNER JOIN b2b_credits_tbl as c ON m.b2b_shop_id = c.b2b_shop_id LEFT JOIN go_bumpr.admin_mechanic_table as gm ON m.b2b_shop_id = gm.axle_id " : "SELECT DISTINCT m.b2b_shop_id,m.b2b_shop_name,m.b2b_address4,c.b2b_credits,c.b2b_partner_flag,c.b2b_flag,gm.exception_stage FROM b2b_mec_tbl as m INNER JOIN b2b_credits_tbl as c ON m.b2b_shop_id = c.b2b_shop_id LEFT JOIN go_bumpr.admin_mechanic_table as gm ON m.b2b_shop_id = gm.axle_id WHERE m.b2b_address5='$city'";
$res_garage = mysqli_query($conn2,$sql_garage);

while($row_garage = mysqli_fetch_object($res_garage)){
    $shop_id = $row_garage->b2b_shop_id;
    $shop_name = $row_garage->b2b_shop_name;
    $locality = $row_garage->b2b_address4;
    $credits_left = $row_garage->b2b_credits;
    $premium = $row_garage->b2b_partner_flag;
    $b2b_flag = $row_garage->b2b_flag;
    $exception_stage = $row_garage->exception_stage;
	switch($exception_stage){
		//case '1': $icon = '<i style="padding-left:5px;font-size:25px;color:FFBA00" title="1st Warning!"  class="fa fa-exclamation"></i>';break;
		case '1': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="1st Warning!" src="/images/exception1.svg">';break;
		case '2': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="2nd Warning!" src="/images/exception2.svg">';break;
		case '3': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="3rd Warning!" src="/images/exception3.svg">';break;
		case '-1': $icon = '<img style="width: 25px;height: 25px;margin-left: 10px;margin-bottom: 5px;" title="Blocked!" src="/images/exceptionlock.svg">';break;
		default:$icon = '';
	}
    $sql_booking_garage = "SELECT DISTINCT b.b2b_booking_id,b.b2b_credit_amt,s.b2b_acpt_flag,s.b2b_deny_flag,g.service_status,b.b2b_check_in_report,b.b2b_source FROM b2b.b2b_booking_tbl as b INNER JOIN b2b.b2b_status as s ON b.b2b_booking_id = s.b2b_booking_id LEFT JOIN go_bumpr.user_booking_tb as g ON b.gb_booking_id = g.booking_id WHERE b.b2b_flag!='1' AND b.gb_booking_id!='0' AND DATE(b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b.b2b_shop_id NOT IN (1014,1035,1670) AND b.b2b_shop_id='$shop_id' AND g.vehicle_type='$vehicle'";
    $res_booking_garage = mysqli_query($conn1,$sql_booking_garage);
    
    $no_of_vehicles_garage = 0;
    $no_of_credits_garage = 0;
    $accepted = 0;
    $rejected = 0;
    $idle = 0;
    $no_of_completed_garage = 0;
    $end_conversion_garage=0;



    while($row_booking_garage = mysqli_fetch_array($res_booking_garage)){
        
        $acpt = $row_booking_garage['b2b_acpt_flag'];
        $deny = $row_booking_garage['b2b_deny_flag'];
        $credit = $row_booking_garage['b2b_credit_amt'];
        $service_status = $row_booking_garage['service_status'];
        $credit = $credit/100;
        $check_in_report = $row_booking_garage['b2b_check_in_report'];
        $b2b_source = $row_booking_garage['b2b_source'];
        
    
        $no_of_vehicles_garage = $no_of_vehicles_garage+1;

             
        if($acpt == '1'){
            $accepted=$accepted+1;
            $no_of_credits_garage = $no_of_credits_garage+$credit;
            if(($service_status == 'Completed' || $service_status == 'In Progress') || ($check_in_report=='1' && $b2b_source=='lead_convert')){
                $no_of_completed_garage = $no_of_completed_garage+1;
            }
        }
        if($deny == '1'){
            $rejected=$rejected+1;
        }
        if($acpt == '0' && $deny == '0'){
            $idle=$idle+1;
        }      

    }

    if($no_of_vehicles_garage == '0' && $no_of_credits_garage == '0'){
        continue;
    }
    $end_conversion_garage = $accepted!= '0' ? round(($no_of_completed_garage/$accepted)*100,1) : '0'; 
    $total_veh_garage = $total_veh_garage+$no_of_vehicles_garage;
    $total_credits_garage = $total_credits_garage+$no_of_credits_garage;
    $total_accepted = $total_accepted+$accepted;
    $total_rejected=$total_rejected+$rejected;
    $total_idle = $total_idle+$idle;
    $total_completed_garage = $total_completed_garage+$no_of_completed_garage;
    $total_end_garage = $total_accepted!= '0' ? round(($total_completed_garage/$total_accepted)*100,1) : '0'; 


?>
<tr>
<td><?php echo $shop_name." ($credits_left)".$icon ; if($premium == 2){ ?>&nbsp;&nbsp;<img src="images/authorized.png" style="width:28px;" title="Premium Garage"> <?php } if($premium == 1 && $b2b_flag == 1){ ?>&nbsp;&nbsp;<i class="fa fa-archive" aria-hidden="true" title="Old Premium partner" style="font-size: 18px;"></i> <?php } if($premium == 1 && $b2b_flag != 1){ ?>&nbsp;&nbsp;<i class="fa fa-leaf" aria-hidden="true" title="Upcoming Premium Prospect" style="font-size: 18px;color:#59bf4d;"></i> <?php } ?></td>
    <td><?php echo $locality ; ?></td>
    <td style="text-align:center;"><?php echo $no_of_vehicles_garage ; ?></td>
    <td style="text-align:center;"><?php echo $no_of_credits_garage ; ?></td>
    <td style="text-align:center;"><?php echo $accepted ; ?></td>
    <td style="text-align:center;"><?php echo $rejected ; ?></td>
    <td style="text-align:center;"><?php echo $idle ; ?></td>
    <td style="text-align:center;"><?php echo $no_of_completed_garage ; ?></td>
    <?php
    switch(true){
        case ($end_conversion_garage >= 70): ?><td style="text-align:center;background-color:#00ff4e70;"><?php echo $end_conversion_garage.' %' ; ?></td> <?php break;
        case ($end_conversion_garage <70 && $end_conversion_garage>= 60): ?><td style="text-align:center;background-color:#ffa800ad;"><?php echo $end_conversion_garage.' %' ; ?></td> <?php break;
        case ($end_conversion_garage <60 && $end_conversion_garage>= 40): ?><td style="text-align:center;background-color:#a25506a6;"><?php echo $end_conversion_garage.' %' ; ?></td> <?php break;
        case ($end_conversion_garage < 40): ?><td style="text-align:center;background-color:#ff2f009c;"><?php echo $end_conversion_garage.' %' ; ?></td> <?php break;
        default: ?><td style="text-align:center;"><?php echo $end_conversion_garage.' %' ; ?></td>  <?php
    }
    ?>
</tr>
<?php
}
?>
</tbody>
<tbody class="avoid-sort">
<tr>
<td colspan="2" style="text-align:center;">Total</td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_veh_garage; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_credits_garage; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_accepted; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_rejected; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_idle; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_completed_garage; ?></td>
<td style="text-align:center;background-color:rgba(84, 156, 156, 0.57);"><?php echo $total_end_garage.' %'; ?></td>
</tr>
</tbody>
</table>
</div>

<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example1").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[3,3],[0,0]]} );
    }
);
</script>
<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example2").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[3,3],[0,0]]} );
    }
);
</script>
<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example3").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[4,4],[0,0]]} );
    }
);
</script>
<?php


?>
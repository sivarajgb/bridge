<?php
session_start();
include("../config.php");
$conn2 = db_connect2();
$shop_id=mysqli_real_escape_string($conn2,$_POST["shop_id"]);
$credits=mysqli_real_escape_string($conn2,$_POST["credits"]);
$amt=mysqli_real_escape_string($conn2,$_POST["amt"]);
$credit_amt=mysqli_real_escape_string($conn2,$_POST["credit_amt"]);
$leads_pack=mysqli_real_escape_string($conn2,$_POST["leads_pack"]);
$leads=mysqli_real_escape_string($conn2,$_POST["leads"]);
$purpose=mysqli_real_escape_string($conn2,$_POST["purpose"]);
$instaid=mysqli_real_escape_string($conn2,$_POST["instaid"]);
$receipt=mysqli_real_escape_string($conn2,$_POST["receipt"]);
$pay_mode=mysqli_real_escape_string($conn2,$_POST["mode"]);
$chequeno=mysqli_real_escape_string($conn2,$_POST["chequeno"]);
$chequedate=$_POST["chequedate"];
$bank=mysqli_real_escape_string($conn2,$_POST["bank"]);
$neft=mysqli_real_escape_string($conn2,$_POST["neft"]);
// $ref=mysqli_real_escape_string($conn2,$_POST["ref"]);
$paytm=mysqli_real_escape_string($conn2,$_POST["paytm"]);
$upi=mysqli_real_escape_string($conn2,$_POST["upi"]);
$discount=mysqli_real_escape_string($conn2,$_POST["discount"]);
$crm_log_id=$_SESSION["crm_log_id"];
$premium=$_POST["premium"];
$today = date('Y-m-d H:i:s');
if($chequedate==""){
	$chequedate='0000-00-00';
}
$purpose= ($purpose=="credit")?"Credits Recharge":"Leads Recharge";
$sql_shop = "SELECT b2b_shop_name,b2b_mobile_number_1 FROM b2b_mec_tbl WHERE leila != 1 and b2b_shop_id='$shop_id'";
$shop_result=mysqli_query($conn2,$sql_shop);
$shop_obj=mysqli_fetch_object($shop_result);
$cus_name=$shop_obj->b2b_shop_name;
$cus_no=$shop_obj->b2b_mobile_number_1;

$prev_credit_query="SELECT b2b_credits FROM b2b_credits_tbl WHERE b2b_shop_id='$shop_id'";
$prev_credit_result=mysqli_query($conn2,$prev_credit_query);
$prev_credit_object=mysqli_fetch_object($prev_credit_result);
$prev_credit=$prev_credit_object->b2b_credits;

$chequedate= date("Y-m-d", strtotime($chequedate));
if($pay_mode=="Paytm"){
	$ref=$paytm;
}else{
	$ref=$upi;
}
$error_flag=$_POST["error_flag"];
$sql_ins = "INSERT INTO b2b_credits_payment (b2b_shop_id,b2b_cus_name,b2b_cus_no,b2b_credits,b2b_amt,b2b_credit_amt,b2b_purpose,instmj_pmt_id,b2b_receipt_no,b2b_cheque_no,b2b_cheque_date,b2b_cheque_bank,b2b_neft_trn_id,b2b_other_ref,b2b_crm_update_id,b2b_comments,b2b_approve_flag,b2b_log,b2b_crm_log_id,b2b_payment_mode,b2b_prev_crdt_blnc,b2b_leads,b2b_leads_pack,premium,discount,sales_approve_flag) VALUES ('$shop_id','$cus_name','$cus_no','$credits','$amt','$credit_amt','$purpose','$instaid','$receipt','$chequeno','$chequedate','$bank','$neft','$ref','$crm_log_id','','1','$today','$crm_log_id','$pay_mode','$prev_credit','$leads','$leads_pack','$premium','$discount','$error_flag')";

if (!mysqli_query($conn2,$sql_ins)){
  echo("Error description: " . mysqli_error($conn2));
}
$query="SELECT b2b_id FROM b2b_credits_payment WHERE b2b_shop_id='$shop_id' ORDER BY b2b_id DESC LIMIT 1;";
$res=mysqli_query($conn2,$query);
$data=mysqli_fetch_array($res);
echo $data[0];
?>


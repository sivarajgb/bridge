<?php  

error_reporting(-1);
ini_set('display_errors', 'On');
set_error_handler("var_dump");
use PHPMailer\PHPMailer\PHPMailer;

require_once('../PHPMail/src/PHPMailer.php');
require_once('../PHPMail/src/SMTP.php');
require_once('../PHPMail/src/Exception.php');

$shop_id=$_POST['shop_id'];
$service_center=$_POST['service_center'];
$mobile=$_POST['mobile'];
$address=$_POST['address'];
$locality=explode(",",$address);
$total_amt=$_POST['total_amt'];
$discount=$_POST['discount'];
$discount_amt=$_POST['discount_amt'];
$mode=$_POST['mode'];
$tax=$_POST['tax'];
$lead_cat=$_POST['lead_cat'];
$act_lead=$_POST['act_lead'];
$leads=$_POST['leads'];
$act_credit=$_POST['act_credit'];
$credits=$_POST['credits'];
$actual_lead_amt=$_POST['actual_lead_amt'];
$actual_credit_amt=$_POST['actual_credit_amt'];
$instaid=$_POST['instaid'];
$receipt=$_POST['receipt'];
$chequeno=$_POST['chequeno'];
$chequedate=$_POST['chequedate'];
$bank=$_POST['bank'];
$neft=$_POST['neft'];
$error_flag=$_POST['error_flag'];
$paytm=$_POST['paytm'];
$ref=$_POST['upi'];
$veh=$_POST['veh'];
$mobile=$_POST['mobile'];
$pay_id=$_POST['pay_id'];
if($mode=="Paytm"){
  $ref=$paytm;
}
$actual_lead_price=$_POST['actual_lead_price'];
//SMTP Settings
$mail = new PHPMailer();
$mail->IsSMTP();
$mail->SMTPAuth   = true;
$mail->SMTPSecure = "TLS";
$mail->Host       = "smtp.pepipost.com";
$mail->Username   = "letsgobumpr";
$mail->Password   = "GoBumpr25000!";
$mail->SetFrom('Admin@gobumpr.com', 'Admin');
$mail->Subject = "Need Approval to recharge";
$mail->Port = 587;  
$mail->SMTPDebug = 2;
$mail->isHTML(true);

$to_mail = 'nandhu@gobumpr.com';

$body_start='<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <style type="text/css">
  td, th {
    padding:0.5em 1em;
    font-size:1.2em;
  }
  table, tr , td{
    border-collapse:collapse;
  }
  th{
    background-color:#FF8C00;
    color:white;
    height:60px;
  }
  button, a{
    cursor:pointer !important;
  }

  </style>

</head>
<body>

      <table style="margin:auto;width:50vw">
      <tr>        
        <th colspan="2" style="text-align:center;">Recharge Request Waiting for Approval</td>
      </tr>';
  if($error_flag==1){
    $body_start.='<tr><td colspan="2" style="color:black"><br>This particular recharge exceeds the floor price/discount mentioned. Requires your approval before proceeding to recharge this account.<br><br></td></tr>';
  }
    if($locality[3]!=" "){
      $body_service='
          <tr>
            <td style="color:#808080">Service Center</td>
            <td>'.$service_center.' ('.$locality[3].')</td>
           </tr>
          <tr>
            <td style="color:#808080">Vehicle Type</td>
            <td>'.$veh.'</td>
           </tr>
           <tr>
          <td style="color:#808080">Mobile Number</td>
            <td >'.$mobile.'</td>
           </tr>
          ';
  
  }else{
    $body_service='
          <tr>
          <td style="color:#808080">Service Center</td>
            <td >'.$service_center.'</td>
           </tr>
           <tr>
            <td style="color:#808080">Vehicle Type</td>
            <td >'.$veh.'</td>
           </tr>
           <tr>
          <td style="color:#808080">Mobile Number</td>
            <td>'.$mobile.'</td>
           </tr>
          ';  
  }
    $body_pay='
      
        <tr  style="padding-left: 0px;color: #FF8C00">
          <td>Payment Details</td>
        </tr>';
    $body_pay_lead_cat='<tr>         
            <td style="color:#808080">Leads Package</td>
            <td>'.$lead_cat.'</td>
          
        </tr>';
    $body_pay_mode='<tr>
          <td style="color:#808080">Payment Mode</td>
          <td>'.$mode.'</td>
        </tr>';
    $body_pay_cash='<tr>
            <td style="color:#808080">Receipt No</td>
            <td>'.$receipt.'</td>
          </tr>';
    $body_pay_insta='<tr>
            <td style="color:#808080">Instamojo ID</td>
            <td >'.$instaid.'</td>
          </tr>';
    $body_pay_neft='
          <tr>
            <td style="color:#808080">Transaction ID</td>
            <td>'.$neft.'</td>
          </tr>';
    $body_pay_others='<tr>
            <td style="color:#808080">Transaction ID</td>
            <td>'.$ref.'</td>
          </tr>';
    $body_pay_cheque='<tr>
          <td style="color:#808080">Cheque No</td>
          <td>'.$chequeno.'</td>
        </tr>
        <tr>
          <td  style="color:#808080">Cheque Date</td>
          <td  id="inv-cheque-date">'.$chequedate.'</td>
        </tr>
        <tr>
          <td style="color:#808080">Bank</td>
          <td >'.$bank.'</td>
        </tr>';
    $body_pay_inv='<tr>
        <td  style="color: #FF8C00">
          Invoice Details
        </td></tr>';
    $body_pay_inv_lead='<tr>
          <td style="color:#808080">'.$act_lead.' </td>
          <td class=" col-sm-6 col-xs-6" id="inv-actual-lead-amt">'.$actual_lead_amt.'</td>
        </tr><tr>
          <td style="color:#808080"> Lead Price </td>
          <td class=" col-sm-6 col-xs-6" id="inv-actual-lead-amt">Rs.'.$actual_lead_price.'</td>
        </tr>';
    $body_pay_inv_credit='<tr>
          <td style="color:#808080">'.$act_credit.' </td>
          <td>'.$actual_credit_amt.'</td>
        </tr>';
      if($discount_amt!=0){
        $body_pay_inv2='<tr>
              <td style="color:#808080"> GST @18%</td>
              <td>'.$tax.'</td>
            </tr>
            <tr>
              <td style="color:#808080">'.$discount.'</td>
              <td>'.$discount_amt.'</td>
            </tr>
            ';
      }else{
        $body_pay_inv2='<tr>
              <td style="color:#808080"> GST @18%</td>
              <td>'.$tax.'</td>
            </tr>';
      }
    $body_pay_total_lead='<tr><td id="inv-total" style="color:#808080">Total amount for '.$leads.' leads</td>';
    $body_pay_total_credit='<tr><td style="color:#808080">Total amount for '.$credits.' credits</td>';
    $body_pay_inv3='<td>'.$total_amt.'</td>
        </tr>';
    $body_pay_btn='<tr><td colspan="2" style="text-align:center"><a href="https://bridge.gobumpr.com/receipts-sheet-v2/recharge_sales_approve.php?id='.base64_encode($pay_id).'"><button style="background-color:#ff8c00;border:none;outline:none;width:200px;height:40px;cursor:pointer;">APPROVE</button></a></td></tr></table>
</body>
</html>';
if($leads==0){
  $body=$body_start.$body_service.$body_pay;
  switch($mode){
    case 'Cash':
      $body.=($body_pay_mode.$body_pay_cash);
      break;
    case 'Cheque':
      $body.=($body_pay_mode.$body_pay_cheque);
      break;
    case 'Paytm':
      $body.=($body_pay_mode.$body_pay_others);
      break;
    case 'UPI':
      $body.=($body_pay_mode.$body_pay_others);
      break;
    case 'Instamojo':
      $body.=($body_pay_mode.$body_pay_insta);
      break;
    case 'NEFT':
      $body.=($body_pay_mode.$body_pay_neft);
      break;
  }
  $body.=($body_pay_inv.$body_pay_inv_credit.$body_pay_inv2.$body_pay_total_credit.$body_pay_inv3.$body_pay_btn);

}else{
  $body=$body_start.$body_service.$body_pay.$body_pay_lead_cat;
  switch($mode){
    case 'Cash':
      $body.=($body_pay_mode.$body_pay_cash);
      break;
    case 'Cheque':
      $body.=($body_pay_mode.$body_pay_cheque);
      break;
    case 'Paytm':
      $body.=($body_pay_mode.$body_pay_others);
      break;
    case 'UPI':
      $body.=($body_pay_mode.$body_pay_others);
      break;
    case 'Instamojo':
      $body.=($body_pay_mode.$body_pay_insta);
      break;
    case 'NEFT':
      $body.=($body_pay_mode.$body_pay_neft);
      break;
  }
  $body.=($body_pay_inv.$body_pay_inv_lead.$body_pay_inv2.$body_pay_total_lead.$body_pay_inv3.$body_pay_btn);
}
$body = preg_replace("[\\\]",'',$body);


$mail->MsgHTML($body);
// echo $body;die;
$mail->AddAddress($to_mail,"Partner");
// $mail->AddCC('kishore.ram@gobumpr.com');
$mail->AddCC('mukund.srivathsan@gobumpr.com');

if (!$mail->Send()) {
    echo "Mailer Error: ".$mail->ErrorInfo;
} else {
    echo "Message sent!";
}
?>
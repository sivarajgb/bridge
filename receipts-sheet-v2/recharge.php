
<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id'])) || $flag!="1") {
  
   if($_SESSION['crm_log_id']!='crm093' && $_SESSION['crm_log_id']!='crm135' && $_SESSION['crm_log_id']!='crm264' && $_SESSION['crm_log_id']!='crm225' ){

  header('location:logout.php');
  die();
}
}
?>

<!DOCTYPE html>
<html>
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>GoBumpr Bridge</title>

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.css">

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

   

  
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
  <!-- table sorter -->
  
  <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>

<style>

/*home page blocks */
.floating-box {
 display: inline-block;
 margin: 2px;
 padding-top: 12px;
 padding-left: 12px;
 width:300px;
}
.navbar-fixed-top{
  z-index:100 !important;
}
.upper-div{
  z-index:999999 !important;
}
#range > span:hover{cursor: pointer;}
 /* table */
#tbody{
   font-size:15px !important;
  border:1.5px solid #c4b8b8 !important;
  
}
thead:hover{
  cursor:pointer;
}

.results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px;
  color:#9E9E9E;
}
#tbody, tbody tr {
    -webkit-animation: opacity 5s ease-in-out;
    animation: opacity 5s ease-in-out;
}

body{
  padding : 0px;
  font-size: 1.5em;
}


#paymentform{
  /*border : 1px solid grey;*/
  padding : 2em;
  
}
#invoice .row{
  margin-bottom: 0.3em;
  margin-top:0.3em;
}



#proceedbtndiv{
  text-align: center;
}
#paymentform .row{
  margin-top: 1em;
}

@media screen and (min-width: 769px){


  #paymentform{
    width:50vw;
  }
  #invoice{
    width:40vw;
  }
  #list-service-center{
    width:30em;
  } 
  #success-div{
    width:25em;
    margin-left:auto;
    margin-right:auto;
    margin-top:15%;
  }

  #form{
    left: 20%;
    right:20%;
    position: absolute;
    top: 40%;
  }
  #paymentform{
  position: absolute;
  top: 20vh;
  left: 25%;
  padding-top: 1em;
  }
  #invoice{
  position: absolute;
  top: 20vh;
  left: 30%;
  }
}
@media screen and (max-width: 768px){
  #form, #paymentform, #invoice{
    padding: 2em;
    margin-top: 2em;
    margin-bottom: 0;
    min-height: 80vh;
  }
  #list-service-center{
    width:100%;
  }
  #form{
    margin-top:40%;
  } 
}
#vehicle-type, #list-service-center{
/*      appearance: none;
  -webkit-appearance: none;
  -moz-appearance: none;*/
  border:none; 


  border-radius: 0%;
  box-shadow: 1px 1px 1px 1px gray;

}




#nextbtn{
  border:none;
}
.fas{
  color:#808080;
}
  body{
    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif;
  }

  #shop-details input{
    background-color: transparent;
    border:none;
    border-bottom: 1px solid #D3D3D3 !important;
/*      box-shadow: 0 1px 0 0 #2bbbad !important
*/    border-radius:0;
    outline:none;
    box-shadow: none;
    
    padding-bottom: 1em;
  }
  #shop-details .input-group-addon{
    background-color: transparent;
    border:none;
    font-size:30px;
  }
/*  #shop-details .input-group .form-control:last-child{

      border-radius:0;    
  }*/
  #address{
    background-color: transparent;
    border:none;
    border-bottom: 1px solid #D3D3D3 !important;
/*      box-shadow: 0 1px 0 0 #2bbbad !important
*/    border-radius:0;
    outline:none;
    box-shadow: none;


  }
  #proceedbtn{

    border:none;
    height:30px;
    outline:none;
    box-shadow: none;
    color: white;
  }
  @media screen and (min-width: 769px){
    #shop-details{
      width:40vw;
      margin-top: 2em;

    }
    #address{
      height: 50px;
    }
  }

  #shop-details .form-inline div{
    margin-top:1em;
  }
  #img-container {
      position: relative;
      text-align: center;
      color: white;
  }

/* Bottom left text */
  .bottom-left {
      position: absolute;
      bottom: 0.4em;
      left: 1.33em;
      background-color: rgba(105, 105, 105,0.8);
      width:14.7em; 
  }
  input[type="text"] {
    cursor:  text;
  }
  #paymentform label{
    color:black;
    font-weight: none;
  }
  #stepper-ul li {
    width: 2em;
    height: 2em;
    text-align: center;
    line-height: 2em;
    border-radius: 1em;
    background: #ffa800;
    margin: 0 5vw;
    display: inline-block;
    color: white;
    position: relative;
    cursor: pointer;
    user-select: none;
    -moz-user-select: -moz-none;

  }

  #stepper-ul li::before{
    content: '';
    position: absolute;
    top: .9em;
    left: -11vw;
    width: 11vw;
    height: .2em;
    background: #ffa800;
    z-index: -1;
  }


  #stepper-ul li:first-child::before {
    display: none;
  }

  .active-step {
    background: #ffa800;
  }
  #stepper-ul .active-step ~ li {
      background: #D3D3D3;
  }
  #stepper-ul .active-step ~ li::before {
      background: #D3D3D3;
  }
  .ui-widget{}
  .ui-autocomplete{overflow-y:auto;overflow-x:hidden;}
  .ui-menu{width:0px;display:none;}
  .ui-autocomplete > li{padding:10px;padding-left:10px;}
  ul{margin-bottom:0;}
  .ui-autocomplete > li.ui-state-focus{background-color:#DDD;}
  .ui-autocomplete .ui-front .ui-menu .ui-widget .ui-widget-content{border:0;}
  .ui-helper-hidden-accessible{display:none;}
  .ui-widget{background-color:white;width:100%;}
  .ui-widget-content{padding-left:1px;display:block;width:20px;position:relative;line-height:12px;max-height:210px;border:.5px solid #DADADA;}
  .ui-widget{}
  .ui-autocomplete { position: absolute; cursor: default;}
</style>

</head>
<body id="body">
<?php include_once("header.php"); ?>
<div id="stepper" style="text-align: center;margin-top: 5em;">
  <ul id="stepper-ul" style="padding-left: 0px">
    <li id="select-step" class="active-step">1</li>
    <li id="shop-step">2</li>
    <li id="pay-step">3</li>
    <li id="inv-step" >4</li>
  </ul>  
  </div>
<div class="container-fluid" id="form" style="padding: 2em;text-align: center;" hidden>
  <form class="form-inline">
    <div class="form-group">
<!--       <label for="vehicle-type">Vehicle Type</label>
 -->  <select id="vehicle-type" class="form-control selectpicker">
        <option>2w</option>
        <option>4w</option>
        <option>tyres</option>
      </select>
    </div>
<!--     <div class="form-group">
      <input list="service-center" placeholder="Select garage" id="list-service-center" class="form-control" autocomplete="off">
      <datalist id="service-center">
        
      </datalist>

    </div> -->
    <div class="form-group">
      <div class="ui-widget">
        <input class="form-control autocomplete" id="list-service-center" type="text" name="list-service-center" required placeholder="Select Garage">
      </div>
      <input id="service-center-id" hidden>
    </div>
  </form>  
  <div style="text-align:center">
    <button class="btn btn-success" id="nextbtn" style="left: 30%">Next</button>
  </div>
</div>

 
<!-- end of step 1 -->

<div class="container" id="shop-details" style="box-shadow: 1px 1px 1px 1px gray;padding:2em;border-radius: 1%" hidden>
  <form class="form-inline">
    <div class="col-sm-12" id="img-container">
      <img class="img-thumbnail img-fluid" src="images/loading.gif" id="shop-cover-img" onerror="this.onerror=null;this.src='images/image_not_found.png';">
      <div class="bottom-left col-sm-12"><h4 id="shop-name"></h4></div>
    </div>
    <div class="col-sm-6">
        <div class="input-group">
            <span id="shop-id" hidden></span>
            <span id="vehicle-type-final" hidden></span>
            <span class="input-group-addon"><i class="fas fa-user"></i></span>
          <input id="owner-name" class="form-control" name="" placeholder=""  disabled style="cursor:default;">
        </div>
      </div>
      <div class="col-sm-6">
        <div class="input-group">
            <span class="input-group-addon">&nbsp;<i class="fas fa-mobile-alt "></i></span>
          <input id="mobile-number" class="form-control" name="" placeholder=""  disabled style="cursor:default;">
      </div>      
      </div>
<!-- 
      <div class="col-sm-6">
        <div class="input-group">
        
            <span class="input-group-addon"><i class="fas fa-building"></i></span>
          <input id="shop-size" class="form-control" name="" placeholder="" disabled style="cursor:default;">
        </div>
      </div> -->
      <div class="col-sm-6">
        <div class="input-group">
            <span class="input-group-addon"><i class="fas fa-car " id="vehicle-icon"></i></span>
          <input id="membership" class="form-control" name="" placeholder=""  disabled style="cursor:default;">
        </div>      
      </div>
      <div class="col-sm-6">
        <div class="input-group">
          <span class="input-group-addon"><img id="gst-icon" src="images/pan.png" height="40" width="40"></span>
          <input id="gst" class="form-control" name="" placeholder=""  disabled style="cursor:default;">
        </div>      
      </div>
      <div class="col-sm-12" style="width:100%">
        <div class="input-group" >
          <span class="input-group-addon"><i class="fas fa-map-marker-alt "></i></span>
          <textarea disabled id="address" class="form-control" cols="100" style="cursor:default;"></textarea>
      </div>          
        
      </div>  

      <div class="col-sm-12" style="text-align:center;margin-top:2em;">
        <button id="proceedbtn" class="btn btn-success">Proceed to Recharge&nbsp;&nbsp;&nbsp;&nbsp;<i class="fas fa-chevron-circle-right " style="color:white;"></i></button>
      </div>  
  </form>    
</div>








<div class="container" id="paymentform" style="box-shadow: 1px 1px 1px 1px gray;padding:1.5em 2em;border-radius: 1%" hidden>
  <div class="alert alert-danger" id="discount-error" hidden style="text-align: center">
         Discount is greater than threshold. Subject to approval from Head of Sales.
  </div>
  <div class="alert alert-danger" id="lead-error" hidden style="text-align: center">
         Lead price is less than threshold. Subject to approval from Head of Sales.
  </div>
  <form class="form-horizontal" id="payment-form-element">
    <div class="form-group">
      <div class="col-sm-12" style="text-align: center">          
        <h3 class="form-control-static" id="service-center-name"></h3>
      </div>
    </div>
    <div class="form-group">
      
      <div class="col-sm-offset-4 col-sm-3 col-xs-6">
        <input type="radio" name="memtype" value="credit" id="credit-radio" checked> <label for="credit-radio" style="font-weight: bold;color:black;">Pre-Credit</label>
      </div>
      <div class=" col-sm-3 col-xs-6"> 
        <input type="radio" name="memtype" value="leads" id="leads-radio"> <label for="leads-radio" style="font-weight: bold;color:black;">Leads</label>
      </div>
    </div>
    <div class="form-group" id="premium-toggle-div" hidden>
      <label class="control-label col-sm-4 col-xs-6" for="">Premium</label>       
      <div class="col-sm-8 col-xs-6">
        <input type="checkbox" id="premium-toggle" data-size="small" data-on-color="success" data-off-color="danger" data-handle-width="6px" data-label-width="5" >
      </div>
    </div>
    <div class="form-group" id="lead-cat-div" hidden>
      <label class="control-label col-sm-4" for="lead-cat">Select Category</label>
      <div class="col-sm-8">          
        <select id="lead-cat" class="form-control" autocomplete="off">

        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-4 col-xs-6" for="amount"> Total Amount Paid<br>(Inclusive of tax)</label>
      <div class="col-sm-3 col-xs-6">

        <input type="text" class="form-control" id="amount" placeholder="Amount" autocomplete="off">
      </div>
      <label class="control-label col-sm-3 col-xs-6" for="discount" id="discount-label">Discount (%)</label>
      <div class="col-sm-2 col-xs-6"> 

        <input type="text" class="form-control" id="discount" autocomplete="off" value=0>
   <!--      <span class="input-group-addon"><i class="fas fa-percentage"></i></span> -->
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-4 col-xs-6" for="actual_amt">Amount</label>
      <div class="col-sm-3 col-xs-6">          
        <p class="form-control-static" id="actual-amt">Rs.0</p>
      </div>
      <label class="control-label col-sm-3 col-xs-6" for="tax-amt">Tax</label>
      <div class="col-sm-2 col-xs-6">          
        <p class="form-control-static" id="tax-amt">Rs.0</p>
      </div>
    </div>
    <div class="form-group" id="credit-mod">
        <label class="control-label col-sm-4 col-xs-6">Total Credits</label>
        <div class="col-sm-2 col-xs-6">          
          <p class="form-control-static" id="credits">0</p>
        </div>      
    </div>
    <div class="form-group" id="lead-mod" hidden>       
        <label class="control-label col-sm-4 col-xs-6">Price Per Lead</label>
        <div class="col-sm-2 col-xs-6">          
          <p class="form-control-static" id="lead-price">0</p>
        </div>               
    </div>

    <div class="form-group">
      <label class="control-label col-sm-4" for="payment-opt">Select Payment Method</label>
      <div class="col-sm-8">          
        <select id="payment-opt" class="form-control" autocomplete="off">
          <option selected>Cash</option>
          <option>Cheque</option>
          <option>NEFT</option>
          <option>Instamojo</option>
          <option>Paytm</option>
          <option>UPI</option>
        </select>
      </div>
    </div>
    <div class="form-group" id="cash">
      <label class="control-label col-sm-4" for="receipt" >Receipt No</label>
      <div class="col-sm-8">          
        <input type="text" class="form-control" id="receipt" placeholder="Enter the receipt number" autocomplete="off" required>
      </div>
    </div>
    <div class="form-group" id="NEFT" hidden>
      <label class="control-label col-sm-4" for="receipt">Transaction ID</label>
      <div class="col-sm-8">          
        <input type="text" class="form-control" id="trans-id" placeholder="Enter the transaction ID" val=" " autocomplete="off" >
      </div>
    </div>
<!--     <div class="form-group" id="others" hidden>
      <label class="control-label col-sm-4" for="reference">Reference</label>
      <div class="col-sm-8">          
        <input type="text" class="form-control" id="reference" placeholder="Enter the reference" val=" ">
      </div>
    </div> -->
    <div class="form-group" id="instamojo" hidden>
      <label class="control-label col-sm-4" for="instamojo-id">Instamojo ID</label>
      <div class="col-sm-8">          
        <input type="text" class="form-control" id="instamojo-id" placeholder="Enter the Instamojo ID" val=" " autocomplete="off" >
      </div>
    </div>
    <div class="form-group" id="paytm" hidden>
      <label class="control-label col-sm-4" for="paytm-id">Transaction ID</label>
      <div class="col-sm-8">          
        <input type="text" class="form-control" id="paytm-id" placeholder="Enter the transaction ID" val=" " autocomplete="off" >
      </div>
    </div>
    <div class="form-group" id="upi" hidden>
      <label class="control-label col-sm-4" for="upi-id">Transaction ID</label>
      <div class="col-sm-8">          
        <input type="text" class="form-control" id="upi-id" placeholder="Enter the transaction ID" val=" " autocomplete="off" >
      </div>
    </div>
    <div id="cheque" hidden>
    <div class="form-group">
      <label class="control-label col-sm-4" for="chequeno">Cheque No</label>
      <div class="col-sm-8">          
        <input type="text" class="form-control" id="chequeno" placeholder="Enter the cheque number" val=" " autocomplete="off" >
      </div>
    </div>
    
    <div class="form-group">
      <label class="control-label col-sm-4" for="bank">Bank Name</label>
      <div class="col-sm-8">          
        <input type="text" class="form-control" id="bank" placeholder="Enter the bank name" val=" " autocomplete="off" >
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-4" for="chequedate">Cheque Placement Date</label>
      <div class="col-sm-8">          
        <input type="text" class="form-control datepicker" data-date-format='dd-mm-yy' id="chequedate" placeholder="dd-mm-yy">
      </div>
    </div>

    </div>
  </form>
    <div class="row" id="proceedbtndiv">
    <div class="col-xs-12"> 
      <div class="alert alert-danger" id="error-box" hidden>
         
      </div>
      <button class="btn btn-success" id="invoice-btn">Confirm Recharge</button>
      <br>
    </div>
    </div>

</div>

<!-- step 3 ends -->

<div class="container-fluid" id="invoice"  style="border: 1px solid grey;padding-top: 1em;padding-bottom: 1em;box-shadow: 2px 2px 5px grey;" hidden>
  <div class="row" >
    
    <div class="col-sm-12" style="text-align: center;">
      <img src="images/invoice.png" class="img-fluid" width="100" height="100"><br>
      <span>Your request for recharge has been submitted. Please verify the details before proceeding with recharge.</span>
    </div>
  </div>
  <div class="row" >
    <div class="col-sm-1">
    </div>
    <div class="col-sm-11">
      <div class="row" style="padding-left: 0px;color: #FF8C00">
        <div class="col-sm-12">Service Center Details</div>
      </div>
      <div class="row">
        <div class=" col-sm-6 col-xs-6" style="color:#808080">Service Center</div>
        <div class="col-sm-6 col-xs-6" id="inv-service-center"></div>
        <div class=" col-sm-6 col-xs-6" style="color:#808080" id="inv-location-label">Address</div>
        <div class="col-sm-6 col-xs-6" id="inv-location"></div>
        <div class=" col-sm-6 col-xs-6" style="color:#808080">Mobile</div>
        <div class="col-sm-6 col-xs-6" id="inv-mobile"></div>
      </div>
    </div>
  </div>
  <div class="row" style="border-top:1px solid grey !important">
    <div class="col-sm-1">
    </div>
    <div class="col-sm-11"  >
      <div class="row" style="padding-left: 0px;color: #FF8C00">
        <div class="col-sm-12">Payment Details</div>
      </div>
      <div class="row" id="inv-category-div" hidden>
    <!--     <div class="col-sm-3 col-xs-5">Purpose</div>
        <div class="col-sm-3 col-xs-7" id="inv-purpose"></div> -->
        
          <div class=" col-sm-6 col-xs-6" style="color:#808080">Leads Package</div>
          <div class="col-sm-6 col-xs-6" id="inv-category"></div>
        
      </div>
      <div class="row">
        <div class="col-sm-6 col-xs-6" style="color:#808080">Payment Mode</div>
        <div class="col-sm-6 col-xs-6" id="inv-pay-mode"></div>
      </div>
      <div class="row">
        <div id="inv-pay-receipt"  hidden>
          <div class=" col-sm-6 col-xs-6" style="color:#808080">Receipt No</div>
          <div class="col-sm-6 col-xs-6" id="inv-receipt"></div>
        </div>
        <div id="inv-pay-instamojo"  hidden>
          <div class=" col-sm-6 col-xs-6" style="color:#808080">Instamojo ID</div>
          <div class="col-sm-6 col-xs-6" id="inv-instamojo"></div>
        </div>
        <div id="inv-pay-paytm"  hidden>
          <div class=" col-sm-6 col-xs-6" style="color:#808080">Transaction ID</div>
          <div class="col-sm-6 col-xs-6" id="inv-paytm"></div>
        </div>
        <div id="inv-pay-upi"  hidden>
          <div class=" col-sm-6 col-xs-6" style="color:#808080">Transaction ID</div>
          <div class="col-sm-6 col-xs-6" id="inv-upi"></div>
        </div>
        <div id="inv-pay-neft"  hidden>
          <div class=" col-sm-6 col-xs-6" style="color:#808080">Transaction ID</div>
          <div class="col-sm-6 col-xs-6" id="inv-trans"></div>
        </div>
<!--         <div id="inv-pay-others"  hidden >
          <div class=" col-sm-6 col-xs-6" style="color:#808080">Reference</div>
          <div class="col-sm-6 col-xs-6" id="inv-ref"></div>
        </div> -->
      </div>
      <div class="row" id="inv-pay-cheque-no" hidden >
        <div class="col-sm-6 col-xs-6" style="color:#808080">Cheque No</div>
        <div class="col-sm-6 col-xs-6" id="inv-cheque-no"></div>
      </div>
      <div class="row" id="inv-pay-bank" hidden >
        <div class="col-sm-6 col-xs-6" style="color:#808080">Cheque Placement Date</div>
        <div class="col-sm-6 col-xs-6" id="inv-cheque-date"></div>
      </div>
      <div class="row" id="inv-pay-cheque-date" hidden >
        <div class="col-sm-6 col-xs-6" style="color:#808080">Bank</div>
        <div class="col-sm-6 col-xs-6" id="inv-bank"></div>
      </div>
    </div>
  </div>
  <div class="row" style="border-top:1px solid grey !important">
    <div class="col-sm-1">
    </div>
    <div class="col-sm-11">
      <div class="row" style="padding-left: 0px;color: #FF8C00">
        <div class="col-sm-12">Invoice Details</div>
      </div>
      <div class="row" id="inv-leads-div" hidden>
        <div class="col-sm-6 col-xs-6" id="inv-actual-leads" style="color:#808080"></div>
        <div class=" col-sm-6 col-xs-6" id="inv-actual-lead-amt"></div>
      </div>
      <div class="row" id="inv-credits-div" hidden>
        <div class="col-sm-6 col-xs-6" id="inv-actual-credits" style="color:#808080"></div>
        <div class="col-sm-6 col-xs-6" id="inv-actual-credit-amt"></div>
      </div>      
      <div class="row">
        <div class=" col-sm-6 col-xs-6" style="color:#808080"> GST @18%</div>
        <div class="col-sm-6 col-xs-6" id="inv-tax"></div>
      </div>
      <div class="row" id="inv-discount-div" hidden>
        <div class="col-sm-6 col-xs-6" id="inv-discount" style="color:#808080"></div>
        <div class="col-sm-6 col-xs-6" id="inv-discount-amt"></div>
      </div>
      <div class="row">
        <div class=" col-sm-6 col-xs-6" id="inv-total" style="color:#808080"></div>
        <div class="col-sm-6 col-xs-6" id="inv-amount-paid"></div>
      </div>
    </div>

  </div>
  <div style="text-align:center; margin-top: 1em">
    <button class="btn btn-success" id="mailbtn">Recharge</button>
  </div>
</div>

<!-- step 4 ends -->

<div id="success-div" class="container-fluid" style="text-align: center;box-shadow: 2px 2px 5px grey;" hidden>
      <br>
      <img src="https://www.shareicon.net/data/512x512/2015/09/24/106380_add_512x512.png" class="img-fluid" width="100" height="100">
      <br>
      <span style="color:green">Thank you for requesting recharge for addition of leads/credits . It'll be approved shortly by the finance team.</span>
      <br><br>

</div>

<!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<!-- jQuery library -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>
 <!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript">
  $('#chequedate').datepicker({
   format: "dd-mm-yyyy",
   todayBtn: "linked",
           
  });
</script>

<script type="text/javascript">
  $("#premium-toggle").bootstrapSwitch();
</script>
<script>
  $(function() {
    $( "#list-service-center" ).autocomplete({
      source: function(request, response) {        
        $.ajax({
          url: "ajax/get_service_center.php",
          data: {
            term: request.term,
            vehicle_type:$('#vehicle-type').val(),
            city:$("#city").val()
          },
          dataType: 'json'
        }).done(function(data) {
        // console.log(data);
          if (data!=null) {
              response($.map(data, function(item) {
                  return item;
              }));
          }else {
            response($.map(data, function(item) {
              return "No Results";
            }));
          }
        });
      },
      select: function(event, ui) {
        event.preventDefault();
        $("#service-center-id").val(ui.item.value);
        $("#list-service-center").val(ui.item.label);
        // display(ui.item.value);
        // return false;
      },
      appendTo : $("#list-service-center").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
          if (!ui.content.length) {
              var noResult = { value:"",label:"No results found" };
              ui.content.push(noResult);
          } else {
              $("#message").empty();
          }
        },
        open: function(event, ui) {
          $(".ui-autocomplete").css("position", "absolute");
          $(".ui-autocomplete").css("z-index", "9999999");
        },
        change: function (event, ui) {
          if (!(ui.item)) event.target.value = "";
        }
        }).focus(function() {
       $(this).autocomplete("search");
    });
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#form").show();
    $("#city").show();
    error_flag=0;
    // find_service_center();
    // function find_service_center(){
    //   $("#loading").show();
    //   $("#service-center").empty();
    //   vehicle_type=$('#vehicle-type').val();
    //   city=$("#city").val();
    //   $.ajax({
    //     url : "ajax/find_service_center.php",
    //     type : "GET",
        
    //     data : {"vehicle":vehicle_type,"city":city},
        
    //     success : function(data){
          
    //       $.each(JSON.parse(data),function(key,value){
    //         $("#service-center").append("<option value='"+value['service_center']+' , '+value['location']+"' id="+value['id']+">");

    //       });
    //       $("#loading").hide();
    //       $("#form").show();
    //     },
    //     error : function(){
    //       alert("Error");
    //     }
    //   });
    // }
    $("#vehicle-type").change(function(){
      $("#list-service-center").val('');
    });
    $("#city").change(function(){
      $("#list-service-center").val('');      
    });
    $("#nextbtn").click(function(event){
      event.preventDefault();

      // val=$("#list-service-center").val();
      // obj=$("#service-center").find("option[value='"+val+"']"); 
      // if(obj!=null && obj.length>0){
        // opt = $('option[value="'+$("#list-service-center").val()+'"]');
        shop_id=$("#service-center-id").val();
        $("#form").hide();
        $(window).scrollTop(0);
        // $("#city").hide();
        $("#loading").show();
        $.ajax({
          url : "ajax/display_shop.php",
          type: "POST",
          data :{"id":shop_id},
          success : function(data){
            
            data=JSON.parse(data);
            
            $("#shop-cover-img").attr('src',data['b2b_cover_image']);
            $("#shop-name").text(data['b2b_shop_name']+", "+data['b2b_address5']);
            $("#owner-name").val(data['b2b_owner_name']);
            $("#mobile-number").val(data['b2b_mobile_number_1']);
            $("#address").text(data['b2b_address1']+', '+data['b2b_address2']+', '+data['b2b_address3']+', '+data['b2b_address4']);
            // $("#shop-size").val(data['b2b_shop_size']);
            $("#membership").val(data['b2b_vehicle_type']);
            $("#gst").val(data['gst_pan']);
            if(data['gst_pan']!=null){
              console.log(data['gst_pan'].length >10);
              if(data['gst_pan'].length >10 ){
                $("#gst-icon").attr('src','images/gst.png');
              }
            }
            $("#shop-id").text(data['b2b_shop_id']);
            $("#vehicle-type-final").text(data['b2b_vehicle_type']);
            if(data['b2b_vehicle_type']=="2w"){
              $("#vehicle-icon").removeClass('fa-car');
              $("#vehicle-icon").addClass('fa-motorcycle');

            }
            $("#loading").hide();
            $("#select-step").removeClass("active-step");
            $("#shop-step").addClass("active-step");
            if(present_step!=-1){
              // $( "#stepper-ul li" ).eq(present_step).css( "height", "2em" );
              // $( "#stepper-ul li" ).eq(present_step).css( "width", "2em" );
              // $( "#stepper-ul li" ).eq(present_step).css( "border", "none" );
            }
            present_step=-1;
            $("#shop-details").show();

          },
          error : function(){

          }
        });
      // }else{
      //   $("#loading").hide();

      //   alert("Invalid Input");
      // }
    });
    $("#proceedbtn").click(function(event){
      event.preventDefault();
      $("#shop-details").hide();
      $(window).scrollTop(0);
      $("#loading").show();
      $("#service-center-name").text($("#shop-name").text());

      if($("#vehicle-type-final").text()=="4w"){
        $("#lead-cat").empty();
        $("#lead-cat").append("<option selected>Spa Only</option><option>Service Only</option><option>Dent Only</option><option>Dent + Spa</option><option>Spa + Service</option><option>Dent + Service</option><option>All Services</option>");
      }else{
        $("#lead-cat").empty();
        $("#lead-cat").append("<option>Royal Enfield & Sports</option><option>Non Royal Enfield</option>");
      }
      leadsorcredits();
      $("#loading").hide();
      $("#shop-step").removeClass("active-step");
      $("#pay-step").addClass("active-step");
      if(present_step!=-1){
        // $( "#stepper-ul li" ).eq(present_step).css( "height", "2em" );
        // $( "#stepper-ul li" ).eq(present_step).css( "width", "2em" );
        // $( "#stepper-ul li" ).eq(present_step).css( "border", "none" );
      }

      present_step=-1;
      $("#payment-form-element").trigger('reset');
      $("#amount").val('');
      $("#actual-amt").text("Rs.0");
      $("#tax-amt").text("Rs.0");
      $("#credits").text(0);
      // $("#leads").text(0);
      $("#discount").val('');
      $("#paymentform").show();

    });
    $("#payment-opt").change(function(){
      mode=$("#payment-opt").val();
      if(mode=="Cash"){
        $("#cash").show();
        $("#instamojo").hide();
        $("#cheque").hide();
        $("#NEFT").hide();
        $("#paytm").hide();
        $("#upi").hide();
      }
      else if(mode=="Cheque"){
        $("#cash").hide();
        $("#instamojo").hide();
        $("#cheque").show();
        $("#NEFT").hide(); 
        $("#paytm").hide();
        $("#upi").hide();        
      }
      else if(mode=="NEFT"){
        $("#cash").hide();
        $("#instamojo").hide();
        $("#cheque").hide();
        $("#NEFT").show(); 
        $("#paytm").hide();
        $("#upi").hide();        
      }
      else if(mode=="Instamojo"){
        $("#cash").hide();
        $("#instamojo").show();
        $("#cheque").hide();
        $("#NEFT").hide();
        $("#paytm").hide();
        $("#upi").hide();         
      }
      else if(mode=="Paytm"){
        $("#cash").hide();
        $("#instamojo").hide();
        $("#cheque").hide();
        $("#NEFT").hide(); 
        $("#paytm").show();
        $("#upi").hide();        
      }
      else{
        $("#cash").hide();
        $("#instamojo").hide();
        $("#cheque").hide();
        $("#NEFT").hide(); 
        $("#paytm").hide();
        $("#upi").show(); 
      }
    });

    $("input[name='memtype']").change(function(){
      $("#amount").val('');
      $("#actual-amt").text("Rs.0");
      $("#tax-amt").text("Rs.0");
      $("#discount").val('');
      $("#instamojo-id").val('');
      $("#receipt").val('');
      $("#chequeno").val('');
      $("#chequedate").val('');
      $("#bank").val('');
      $("#trans-id").val('');
      $("#paytm-id").val('');
      $("#upi-id").val('');
      $("#discount-error").hide();
      $("#lead-error").hide();
      changeoffer();
      leadsorcredits();
    });
    function leadsorcredits(){
      if($("input[name='memtype']:checked").val()=="leads"){
        $("#credit-mod").hide();
        $("#premium-toggle-div").show();
        $("#lead-cat-div").show();
        $("#lead-mod").show();
        $("#discount-label").text("Total Leads");        
      }
      else{
        $("#lead-cat-div").hide();
        $("#lead-mod").hide();
        $("#premium-toggle-div").hide();
        $("#discount-label").text("Discount (%)");
        $("#credit-mod").show();
      }
    }
    $("#amount").keyup(function(){
      val=Number($("#amount").val());
      actual_amt=Number(val)/1.18;
      $("#actual-amt").text("Rs."+Math.round(actual_amt));
      $("#tax-amt").text("Rs."+Math.round(val-actual_amt));
      changeoffer();
    });
    $("#discount").keyup(function(){
      changeoffer();
    });
    $("#payment-opt").change(function(){
      $("#instamojo-id").val('');
      $("#receipt").val('');
      $("#chequeno").val('');
      $("#chequedate").val('');
      $("#bank").val('');
      $("#trans-id").val('');
      $("#paytm-id").val('');
      $("#upi-id").val('');
    });
    $("#discount").blur(function(){
      changeoffer();
      if ($("input[name='memtype']:checked").val()!="leads"){
        if(Number($("#discount").val())>10){
          error_flag=1;
          $("#discount-error").show();
        }else{
          error_flag=0;
          $("#discount-error").hide();
        }
      }else{
        
        premium_status=$('#premium-toggle').bootstrapSwitch('state');  
        lead_cat=$("#lead-cat").val();
        city=$("#city").val();
        $.ajax({
          url :'ajax/lead_package.php',
          type :'POST',
          data :{'lead_cat':lead_cat,'status':premium_status,'city':city},
          success : function(data){
            lead_floor_val=Number(data);   
            if(Number($("#lead-price").text().substr(3))<lead_floor_val && $("#discount").val().length>0){
              error_flag=1;
              $("#lead-error").show();
            }else{
              error_flag=0;
              $("#lead-error").hide();
            }         
          },
          error : function(){

            alert("error");
          }
        });
        // console.log(lead_floor_val);

      }
      
    });
    $("#lead-cat").change(function(){
      changeoffer();
    })
    $('#premium-toggle').on('switchChange.bootstrapSwitch', function(event, state) {
      changeoffer();
    });
    function changeoffer(){
      val=Number($("#actual-amt").text().substr(3));
      discount=Number($("#discount").val());
      if ($("input[name='memtype']:checked").val()!="leads") {
        // $("#leads").text(0); 
        credits=Math.round((val*(100+discount)/100)/100);
        discount_amt=Math.ceil(discount*val/100);
        actual_credit=Math.round(val/100);
        $("#credits").text(Math.round(credits));
      }
      else{        
        $("#credits").text(0);
        premium_status=$('#premium-toggle').bootstrapSwitch('state');  
        lead_cat=$("#lead-cat").val();
        city=$("#city").val();
        discount_amt=0;
        leads=discount; 
        if(Boolean(leads)){
          $("#lead-price").text("Rs."+Math.round(val/leads));
        }else{
          $("#lead-price").text("Rs.0");
        }
        // discount_amt=Math.round(discount*lead_val);     
        // actual_lead=Math.round(val/lead_val);    
        // $("#leads").text(leads.toFixed(0));
        
        $.ajax({
          url :'ajax/lead_package.php',
          type :'POST',
          data :{'lead_cat':lead_cat,'status':premium_status,'city':city},
          success : function(data){
            lead_floor_val=Number(data); 
            if(Number($("#lead-price").text().substr(3))<lead_floor_val && $("#discount").val().length>0){
              error_flag=1;
              $("#lead-error").show();
            }else{
              error_flag=0;
              $("#lead-error").hide();
            }                   
          },
          error : function(){
            alert("error");
          }
        });
        // console.log(lead_floor_val);


      }
    }
    $("#invoice-btn").click(function(event){
      event.preventDefault();
      
      mode=$("#payment-opt").val();
      // alert($('#premium-toggle').bootstrapSwitch('state'));
      if($("#amount").val()==""){
        $("#error-box").text("Please enter the Amount paid");
        $("#error-box").show();
        $("#paymentform").effect('shake'); 

      }else if(mode=="Cash" && $("#receipt").val()==""){
        $("#error-box").hide();
        $("#error-box").text("Please enter the Receipt No");
        $("#error-box").show();
        $("#paymentform").effect('shake'); 

      }else if(mode=="Cheque" && ($("#chequeno").val()=="" || $("#chequedate").val()=="" || $("#bank").val()=="")) {
        $("#error-box").hide();
        $("#error-box").text("Please enter the cheque details");
        $("#error-box").show();
        $("#paymentform").effect('shake'); 

      }else if(mode=="NEFT" && $("#trans-id").val()==""){
        $("#error-box").hide();
        $("#error-box").text("Please enter the Transaction ID");
        $("#error-box").show();  
        $("#paymentform").effect('shake'); 
     
      }else if(mode=="Instamojo" && $("#instamojo-id").val()==""){
        $("#error-box").hide();
        $("#error-box").text("Please enter the Instamojo ID");
        $("#error-box").show(); 
        $("#paymentform").effect('shake'); 
      }else if(mode=="Paytm" && $("#paytm-id").val()==""){
        $("#error-box").hide();
        $("#error-box").text("Please enter the Transaction ID");
        $("#error-box").show();
        $("#paymentform").effect('shake'); 
      }else if(mode=="UPI" && $("#upi-id").val()==""){
        $("#error-box").hide();
        $("#error-box").text("Please enter the Transaction ID");
        $("#error-box").show();
        $("#paymentform").effect('shake'); 
      }else{
        $("#error-box").hide();
        $("#paymentform").hide();
        $(window).scrollTop(0);
        $("#loading").show();
        shop_id=Number($("#shop-id").text());
        credits=Number($("#credits").text());
        amt=$("#amount").val();

        credit_amt=credits*100;
        premium=Number($('#premium-toggle').bootstrapSwitch('state')); 
        purpose=$("input[name='memtype']:checked").val();
        
        if(purpose=="leads"){
          leads=Number($("#discount").val());
          leads_pack=$("#lead-cat").val();
          discount=0;
        }else{
          leads=0;
          leads_pack="";
          discount=Number($("#discount").val());
        }
        mode=$("#payment-opt").val();
        
        instaid=$("#instamojo-id").val();
        receipt=$("#receipt").val();
        chequeno=$("#chequeno").val();
        chequedate=$("#chequedate").val();
        bank=$("#bank").val();
        neft=$("#trans-id").val();
        // ref=$("#reference").val();
        paytm=$("#paytm-id").val();
        upi=$("#upi-id").val();
        $.ajax({
          url : "ajax/recharge_view.php",
          type : "POST",
          data : {"shop_id":shop_id,"credits":credits,"amt":amt,"credit_amt":credit_amt,"leads":leads,"purpose":purpose,"leads_pack":leads_pack,"instaid":instaid,"receipt":receipt,"chequeno":chequeno,"chequedate":chequedate,"bank":bank,"neft":neft,"mode":mode,'instaid':instaid,'receipt':receipt,'chequeno':chequeno,'chequedate':chequedate,'bank':bank,'neft':neft,'discount_amt':discount_amt,'upi':upi,'paytm':paytm,'premium':premium,'discount':discount,'error_flag':error_flag},
          success : function(data){
            pay_id=data;
            $("#loading").hide();
            $("#inv-service-center").text($("#shop-name").text());
            $("#inv-mobile").text($("#mobile-number").val());
            // $("#inv-owner").text($("#owner-name").text());
            if($("#address").text().length<8){
              $("#inv-location").text($("#address").text())
              $("#inv-location-label").hide();
              $("#inv-location").hide();
            }else{
            $("#inv-location").text($("#address").text());
            }
            // $("#inv-purpose").text(purpose+" Recharge");
            $("#inv-amount-paid").text("Rs."+$("#amount").val());
            $("#inv-amount").text($("#actual-amt").text());
            $("#inv-pay-mode").text(mode);
            $("#inv-tax").text($("#tax-amt").text());

            if(purpose=="leads"){
              $("#inv-category-div").show();
              $("#inv-category").text(leads_pack);
              $("#inv-actual-leads").text($("#discount").val()+" leads");             
              $("#inv-actual-lead-amt").text($("#actual-amt").text())
              // $("#inv-discount").text("Discount - "+$("#discount").val()+" Leads");
              $("#inv-total").text("Total Amount for "+leads+" leads");
              $("#inv-leads-div").show();
            }else{
              $("#inv-actual-credits").text(actual_credit+" credits");
              $("#inv-discount").text("Discount - "+$("#discount").val()+" %");
              if($("#discount").val()!="0"){
                $("#inv-discount-div").show();              
                $("#inv-discount-amt").text("Rs."+discount_amt);
              }
              $("#inv-actual-credit-amt").text($("#actual-amt").text());
              $("#inv-total").text("Total Amount for "+credits+" credits");
              $("#inv-credits-div").show();
            }
            switch (mode){
              case "Cash" : 
                $("#inv-receipt").text(receipt);
                $("#inv-pay-receipt").show();
                break;
              case "Cheque" :
                $("#inv-cheque-date").text(chequedate);
                $("#inv-cheque-no").text(chequeno);
                $("#inv-bank").text(bank);
                $("#inv-pay-cheque-no").show();
                $("#inv-pay-cheque-date").show();
                $("#inv-pay-bank").show();
                break;
              case "Paytm" :
                $("#inv-paytm").text(paytm); 
                $("#inv-pay-paytm").show();
                break;
              case "Instamojo" :
                $("#inv-instamojo").text(instaid); 
                $("#inv-pay-instamojo").show();
                break;
              case "NEFT" :
                $("#inv-trans").text(neft);
                $("#inv-pay-neft").show();
                break;
              case "UPI" :
                $("#inv-upi").text(upi); 
                $("#inv-pay-upi").show();
                break;
            }
            $("#pay-step").removeClass("active-step");
            $("#inv-step").addClass("active-step");
            // if(present_step!=-1){
            //   // $( "#stepper-ul li" ).eq(present_step).css( "height", "2em" );
            //   // $( "#stepper-ul li" ).eq(present_step).css( "width", "2em" );
            //   // $( "#stepper-ul li" ).eq(present_step).css( "border", "none" );
            // }

            present_step=-1;

            $("#invoice").show();
          },
          error : function(){

          }
        });
      }
    });
    $("#mailbtn").click(function(){
      $("#invoice").hide();
      $(window).scrollTop(0);
      $("#stepper").hide();
      $("#loading").show();
      service_center=$("#inv-service-center").text();
      mobile=$("#inv-mobile").text();
      address=$("#inv-location").text();
      total_amt=$("#inv-amount-paid").text();
      discount=$("#inv-discount").text();
      mode=$("#inv-pay-mode").text();
      veh=$("#membership").val();
      mobile=$("#mobile-number").val();
      if(purpose=="leads"){
        discount_amt=0; 
      }else{
        discount_amt=$("#inv-discount-amt").text();
      }
      lead_price=Number($("#lead-price").text().substr(3));
      tax=$("#inv-tax").text();
      lead_cat=$("#inv-category").text();
      act_lead=$("#inv-actual-leads").text();             
      act_lead_amt=$("#inv-actual-lead-amt").text();  
      act_credit=$("#inv-actual-credits").text();
      act_credit_amt=$("#inv-actual-credit-amt").text();
      if(error_flag==1){
        $.ajax({
          url : "ajax/recharge_mail.php",
          type : "POST",
          data :{shop_id:shop_id,service_center:service_center,mobile:mobile,address:address,total_amt:total_amt,discount:discount,mode:mode,tax:tax,lead_cat:lead_cat,act_lead:act_lead,leads:leads,act_credit:act_credit,credits:credits,actual_lead_amt:act_lead_amt,actual_credit_amt:act_credit_amt,discount_amt:discount_amt,receipt:receipt,instaid:instaid,chequeno:chequeno,chequedate:chequedate,bank:bank,neft:neft,paytm:paytm,upi:upi,error_flag:error_flag,veh:veh,mobile:mobile,pay_id:pay_id,actual_lead_price:lead_price},
          success : function(){
            $("#loading").hide();

            $("#success-div").show();
            window.setTimeout(function(){window.location.reload()}, 5000);
          },
          error : function(){
            alert("error");
          }

        });
      }else{
        $.ajax({
          url : "ajax/finance_recharge_mail.php",
          type : "POST",
          data :{id:pay_id},
          success : function(){
            $("#loading").hide();

            $("#success-div").show();
            window.setTimeout(function(){window.location.reload()}, 5000);
          },
          error : function(){
            alert("error");
          }

        });

      }
        
    });
      
  });
  $(document).ready(function(){
    present_step=-1;
    $("#stepper-ul li").click(function(){
      li_index=$(this).index();
      active_index=$("li.active-step").index();
      if(li_index <=active_index ){

        if(present_step==-1){
          switch (active_index){
            case 0:
              $("#form").hide();break;
            case 1:
              $("#shop-details").hide();break;
            case 2:
              $("#paymentform").hide();break;
            case 3:
              $("#invoice").hide();break;
          }
        }else{
          // $( "#stepper-ul li" ).eq(present_step).css( "height", "2em" );
          // $( "#stepper-ul li" ).eq(present_step).css( "width", "2em" );
          // $( "#stepper-ul li" ).eq(present_step).css( "border", "none" );

          switch (present_step){
            case 0:
              $("#form").hide();break;
            case 1:
              $("#shop-details").hide();break;
            case 2:
              $("#paymentform").hide();break;
            case 3:
              $("#invoice").hide();break;
          }
        }
        present_step=li_index;
        // $( "#stepper-ul li" ).eq(li_index).css( "height", "2.5em" );
        // $( "#stepper-ul li" ).eq(li_index).css( "width", "2.5em" );
        // $( "#stepper-ul li" ).eq(li_index).css( "border", "2px dashed #FFA800" );
        switch (li_index){
          case 0:
            $("#form").show();break;
          case 1:
            $("#shop-details").show();break;
          case 2:
            $("#paymentform").show();break;
          case 3:
            $("#invoice").show();break;
        }
      }
    });

  });

  </script>






</body>
</html>


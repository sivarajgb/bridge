<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
date_default_timezone_set('Asia/Kolkata');
$today = date('Y-m-d H:i:s');
include("config.php");

$conn = db_connect2();
$conn1 = db_connect1();
if(!$conn){
  echo "Database Connection Error!";
}
else{
 $shop_id = base64_decode($_GET['id']);
 $payment_id = base64_decode($_GET['pay_id']);

  $sql_fetch = "SELECT b2b_amt,b2b_credits,b2b_leads,b2b_credit_amt,b2b_approve_flag,b2b_cus_name,premium,b2b_leads_pack FROM b2b_credits_payment WHERE b2b_id = {$payment_id}";
  
  $res_fetch = mysqli_query($conn,$sql_fetch);
  $row_fetch = mysqli_fetch_array($res_fetch);
  // var_dump($row_fetch);
  $credits = $row_fetch['b2b_credits'];
  $leads=$row_fetch['b2b_leads'];
  $leads_pack=$row_fetch['b2b_leads_pack'];
  $credit_amt = $row_fetch['b2b_credit_amt'];
  $approve_flag = $row_fetch['b2b_approve_flag'];
  $shop_name = $row_fetch['b2b_cus_name'];
  $amt=round($row_fetch['b2b_amt']/1.18);
  $premium=$row_fetch['premium'];
  $re_leads=0;
  $nre_leads=0;
  $re_lead_price=0;
  $nre_lead_price=0;
  if($leads==0){
    $lead_price=0;
    $model="Pre Credit";
    $amount=$credit_amt;
  }else{
    $lead_price=round($amt/$leads);
    if($premium==1){
      $model="Premium 2.0";
    }else{
      $model="Leads 3.0";
    }

    if($model!="Pre Credit"){
      if($leads_pack=="Royal Enfield & Sports"){
        $re_lead_price=$lead_price;
        $re_leads=$leads;
      }else if($leads_pack=="Non Royal Enfield"){
        $nre_lead_price=$lead_price;
        $nre_leads=$leads;
      }
    }
    $amount=$amt;
  }
}
 ?>
 <!DOCTYPE html>
 <html lang="en">
 <head>
 <meta charset="utf-8">
 <title>GoBumpr Bridge</title>
 <style>
 body{
   align-content: center;
   background-color: #fafafa;
   font-family: sans-serif;
   color: #000;
   font-style: normal;
 }

 </style>
 </head>
 <body>
<div align="center" style="margin-top:50px;">
  <?php if($approve_flag != '1'){  ?>

    <?php if($leads==0){ ?>
    <p style="font-size:37px; color:#000;"><?php echo $credits; ?> Credits have been added to "<?php echo strtoupper($shop_name); ?>" !</p>
  <?php }else { ?>
    <p style="font-size:37px; color:#000;"><?php echo $leads; ?> Leads have been added to "<?php echo strtoupper($shop_name); ?>" !</p>
  
  <?php } } else{ 
      $sql_veh_type="SELECT b2b_vehicle_type FROM b2b_mec_tbl WHERE b2b_shop_id='$shop_id';";
      $res_veh_type=mysqli_query($conn,$sql_veh_type);
      $row_veh_type=mysqli_fetch_array($res_veh_type);
      $veh=$row_veh_type['b2b_vehicle_type'];
      //get credits
      $sql_get_credits = "SELECT b2b_credits,b2b_leads,b2b_re_leads,b2b_non_re_leads,b2b_cash_remaining FROM b2b_credits_tbl WHERE b2b_shop_id='$shop_id'";
      $res_get_credits = mysqli_query($conn,$sql_get_credits);
      $row_get_credits = mysqli_fetch_object($res_get_credits);

      $avl_credits = $row_get_credits->b2b_credits;
      $avl_leads=$row_get_credits->b2b_leads;
      $avl_re_leads=$row_get_credits->b2b_re_leads;
      $avl_nre_leads=$row_get_credits->b2b_non_re_leads;
      $avl_amount = $row_get_credits->b2b_cash_remaining;
      //add credits
      $total_leads=$avl_leads+$leads;
      $total_credits = $avl_credits+$credits;
      $total_amount = $avl_amount+$credit_amt;
      $total_re_leads=$avl_re_leads+$re_leads;
      $total_nre_leads=$avl_nre_leads+$nre_leads;
      if($veh=="4w"){        
        $sql_updt_credits = "UPDATE b2b_credits_tbl SET b2b_credits='$total_credits',b2b_leads='$total_leads',b2b_cash_remaining='$total_amount',b2b_mod_log='$today' WHERE b2b_shop_id='$shop_id' ;";
        $res_updt_credits = mysqli_query($conn,$sql_updt_credits);
      }else{

        $sql_updt_credits = "UPDATE b2b_credits_tbl SET b2b_credits='$total_credits',b2b_re_leads='$total_re_leads',b2b_non_re_leads='$total_nre_leads',b2b_cash_remaining='$total_amount',b2b_mod_log='$today',b2b_leads='$total_leads' WHERE b2b_shop_id='$shop_id' ;";
        // echo $sql_updt_credits;die;
        
        $res_updt_credits = mysqli_query($conn,$sql_updt_credits);
      }
      if($model=="Pre Credit"){
        $updt_query="UPDATE b2b_mec_tbl SET b2b_cred_model='1',b2b_new_flg='0' WHERE b2b_shop_id='$shop_id';";
      }else{
        $updt_query="UPDATE b2b_mec_tbl SET b2b_cred_model='1',b2b_new_flg='1' WHERE b2b_shop_id='$shop_id';";
      }
      // echo $updt_query;
      $res_updt_flg=mysqli_query($conn,$updt_query);

      //remove flag
      $sql_updt_payment = "UPDATE b2b_credits_payment SET b2b_approve_flag='0',b2b_mod_log='$today',b2b_pmt_scs_flg='1',b2b_pmt_scs_date='$today' WHERE b2b_id = '$payment_id'";
      $res_updt_payment = mysqli_query($conn,$sql_updt_payment);

      $updt_flag=0;

      $sql_select_model="SELECT id,model,re_lead_price,re_leads,nre_lead_price,nre_leads,amount,leads,lead_price FROM garage_model_history WHERE b2b_shop_id='$shop_id' ORDER BY start_date DESC LIMIT 1;";
      // echo $sql_select_model;
      $res_select_model=mysqli_query($conn1,$sql_select_model);
      if(mysqli_num_rows($res_select_model)>0){
        $row_select_model=mysqli_fetch_array($res_select_model);
        // var_dump($row_select_model);
        // echo $leads_pack;
        $model_id=$row_select_model['id'];
        $model_val=$row_select_model['model'];
        $db_re_lead_price=$row_select_model['re_lead_price'];
        $db_nre_lead_price=$row_select_model['nre_lead_price'];
        $db_re_leads=$row_select_model['re_leads'];
        $db_nre_leads=$row_select_model['nre_leads'];
        $db_amt=$row_select_model['amount'];
        $db_leads=$row_select_model['leads'];
        $db_lead_price=$row_select_model['lead_price'];
        if($model=="Premium 2.0"){
          if($leads_pack=="Royal Enfield & Sports"){
            if($db_leads=="0"){
              $updt_flag=1;
            }
          }else if($leads_pack=="Non Royal Enfield"){
            if($db_nre_leads=="0"){
              $updt_flag=1;
            }
          }
        }else if($model=="Leads 3.0"){
          if($leads_pack=="Royal Enfield & Sports"){
            if($db_re_leads=="0"){
              $updt_flag=1;
            }
          }else if($leads_pack=="Non Royal Enfield"){
            if($db_nre_leads=="0"){
              $updt_flag=1;
            }
          }
        }
      }else{
        $model_id="";
        $model_val="";
      }
      // echo $updt_flag;
      $today1=date('Y-m-d');
      $yesterday=date('Y-m-d',strtotime("-1 days"));
      if($model==$model_val && $veh=='2w' && $model!='Pre Credit' && $updt_flag==1){
        $new_nre_leads=$nre_leads+$db_nre_leads;
        $new_nre_lead_price=($nre_lead_price==0)?$db_nre_lead_price:$nre_lead_price;
        $new_amt=$amount+$db_amt;
        if($model=="Leads 3.0"){
          $new_re_lead_price=($re_lead_price==0)?$db_re_lead_price:$re_lead_price;
          $new_re_leads=$re_leads+$db_re_leads;
          $sql_updt_model="UPDATE garage_model_history SET re_leads='$new_re_leads',nre_leads='$new_nre_leads',re_lead_price='$new_re_lead_price',nre_lead_price='$new_nre_lead_price',amount='$new_amt' WHERE id='$model_id';";
          $res_updt_model=mysqli_query($conn1,$sql_updt_model);
        }else{
          $new_re_lead_price=($re_lead_price==0)?$db_lead_price:$re_lead_price;
          $new_re_leads=$re_leads+$db_leads;
          $sql_updt_model="UPDATE garage_model_history SET leads='$new_re_leads',nre_leads='$new_nre_leads',lead_price='$new_re_lead_price',nre_lead_price='$new_nre_lead_price',amount='$new_amt' WHERE id='$model_id';";
          // echo $sql_updt_model;
          $res_updt_model=mysqli_query($conn1,$sql_updt_model);
        }
      }else{
        if($model_id!=""){
          $sql_updt_model="UPDATE garage_model_history SET end_date='$yesterday' WHERE id='$model_id';";
          $res_updt_model=mysqli_query($conn1,$sql_updt_model);
        }
        if($veh=='2w' && $model!='Pre Credit'){
          if($model=="Leads 3.0"){
            $sql_ins_model="INSERT INTO garage_model_history (b2b_shop_id,shop_name,start_date,end_date,model,credits,amount,nre_lead_price,nre_leads,re_lead_price,re_leads,flag,premium_tenure,leads,lead_price) VALUES ('$shop_id','$shop_name','$today1','0000-00-00','$model','$credits','$amount','$nre_lead_price','$nre_leads','$re_lead_price','$re_leads',0,0,0,0);";
          }else if($model=="Premium 2.0"){
            $sql_ins_model="INSERT INTO garage_model_history (b2b_shop_id,shop_name,start_date,end_date,model,credits,leads,amount,lead_price,nre_lead_price,nre_leads,flag,premium_tenure,re_lead_price,re_leads) VALUES ('$shop_id','$shop_name','$today1','0000-00-00','$model','$credits','$re_leads','$amount','$re_lead_price','$nre_lead_price','$nre_leads',0,0,0,0);";
          }
        }else{
            $sql_ins_model="INSERT INTO garage_model_history (b2b_shop_id,shop_name,start_date,end_date,model,credits,leads,amount,lead_price,nre_lead_price,nre_leads,re_lead_price,re_leads,flag,premium_tenure) VALUES ('$shop_id','$shop_name','$today1','0000-00-00','$model','$credits','$leads','$amount','$lead_price','$nre_lead_price','$nre_leads','$re_lead_price','$re_leads',0,0);";
        }
        // echo $sql_ins_model;
        $res_ins_model=mysqli_query($conn1,$sql_ins_model);
      }

  ?>
  <?php if($leads==0){ ?>
    <p style="font-size:37px; color:#000;"><?php echo $credits; ?> Credits have been added to "<?php echo strtoupper($shop_name); ?>" !</p>
  <?php }else { ?>
    <p style="font-size:37px; color:#000;"><?php echo $leads; ?> Leads have been added to "<?php echo strtoupper($shop_name); ?>" !</p>
  <?php } } ?>
</div>

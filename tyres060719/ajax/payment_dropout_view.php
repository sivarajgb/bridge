<?php
include("../config.php");
$conn = db_connect1();
session_start();
date_default_timezone_set('Asia/Kolkata');
// $crm_log_id = $_SESSION['crm_log_id'] ;
// $crm_name = $_SESSION['crm_name'];
// $flag=$_SESSION['flag'];
$today = date("Y-m-d");
$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));

// $city = $_POST['city'];  
// $_SESSION['crm_city'] = $city;
$no = 0;
?>


  <div id="div1" style="width:100%;float:left;" align="center">
    <table class="table table-striped table-bordered tablesorter table-hover results" id="table" style="width: 80%;font-size:12px;">
    <thead style="background-color:#B2DFDB;align:center;display: table-header-group;">
    <th>No. <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Mobile Number <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Vehicle<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Tyre Brand<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Tyre Count<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Localities<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
    <th>Log<i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>

    </thead>
    <tbody id="tbody">

    <?php
  $cond = '';
  // $cond = $cond.($veh == "all" ? "" : "AND m.type='$veh'");
  // $cond = $cond.($shop_status == "active" ? "AND b2b_m.b2b_avail='1'" : "AND b2b_m.b2b_avail='0'");
    $sql_utm = "SELECT tt.id,tb.temp_id,tt.mobile_number,tt.vehicle,tt.tyre_brand,tt.tyres_count,tt.locality,tt.log,tt.utm_campaign from go_bumpr.temp_tyres_booking as tt LEFT JOIN go_bumpr.tyre_booking_tb as tb on tt.id=tb.temp_id WHERE DATE(tt.log) BETWEEN '$startdate' and '$enddate' and tt.utm_campaign='tyres-payment' and tb.temp_id is NULL and tt.mobile_number!=''";
    $res_utm = mysqli_query($conn,$sql_utm);

      // $list_credits = array();
     $num_utm = mysqli_num_rows($res_utm);
    if($num_utm>0)
    {
    while($row_utm = mysqli_fetch_object($res_utm))
    {
        $mobile_number = $row_utm->mobile_number;
        $vehicle = $row_utm->vehicle;
        $brand = $row_utm->tyre_brand;
        $count = $row_utm->tyres_count;
        $locality = $row_utm->locality;
        $log = $row_utm->log;
        $id=$row_utm->id;
        ?>
        <tr>
            <td><?php echo $no=$no+1; ?></td>
      <td><?php echo $mobile_number;?></td>
      <td><?php echo $vehicle;?></td>
      <td><?php echo $brand;?></td>
      <td><?php echo $count;?></td>
      <td><?php echo $locality;?></td>
      <td><?php echo date('d M Y ', strtotime($log));?></td>

        </tr>
    
    <?php
}
}
else{
  ?>  
      <th colspan="7" style="font-size: 14px;text-align:center;padding-top: 40px;">No results found...! </th>
 <?php
}
?>

    </tbody>
    </table>
  </div>
<!-- search bar  -->
<script>
$(document).ready(function() {
  var no = "<?php echo $no; ?>";

if(no!='0')
{
$('.counter').text(no + ' shops');
}
  $(".search").keyup(function () {
    var searchTerm = $(".search").val();
    var listItem = $('.results tbody').children('tr');
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

  $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });

  $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','false');
  });

  $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','true');
  });
  var jobCount = $('.results tbody tr[visible="true"]').length;
    $('.counter').text(jobCount + ' shops');

  if(jobCount == '0') {$('.no-result').show();}
    else {$('.no-result').hide();
  }
 });
});
</script>


<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#table").tablesorter( {sortList: [[0,0], [1,0]]} );
    }
);
</script>
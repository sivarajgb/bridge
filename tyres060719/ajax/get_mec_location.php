<?php
header('Content-Type: application/json');
include("../config.php");
$conn = db_connect1();
$conn1 = db_connect2();

$booking_id = $_POST['book_id'];
$location = $_POST['selectloc'];
date_default_timezone_set('Asia/Kolkata');
$log= date('Y-m-d H-i-s');
$service_date= date('Y-m-d');
$this_month = date('n');
$city ='chennai';

$sql_loc = "SELECT lat,lng FROM localities WHERE localities='".$location."'";
$res_location = mysqli_query($conn,$sql_loc);
	while($row_loc = mysqli_fetch_object($res_location))
	{
	    $booking_lat = $row_loc->lat;
		$booking_lng = $row_loc->lng;
	}
function fetch($res_mecs,$brand)
	{
		while($row_mec = mysqli_fetch_object($res_mecs))
		{
			$brand_serviced = $row_mec->brand;
			$brandsArr = '';
			if($brand!= "")
			{
				if($brand_serviced != 'all' && $brand_serviced != '')
				{
					$brandsArr = explode(',',$brand_serviced);
					if(!in_array($brand,$brandsArr))
					{
						continue;
					}
				}
				$brand_not_serviced = $row_mec->brand_ns;
				$brandsNoArr = '';
				if($brand_not_serviced != 'all' && $brand_not_serviced != '')
				{
					$brandsNoArr = explode(',',$brand_not_serviced);
					if(in_array($brand,$brandsNoArr))
					{
						continue;
					}
				}				
			}
			$mec_id = $row_mec->mec_id;
			$axle_id = $row_mec->axle_id;
			$shop_name = $row_mec->shop_name;
			$mec_lat = $row_mec->lat;
			$mec_lng = $row_mec->lng;
			$pick_range = $row_mec->pick_range;
			$distance = $row_mec->dist;
			$address4 = $row_mec->address4;
			$premium = $row_mec->premium;
			$arr['mec_id'][] = $mec_id;
			$arr['axle_id'][] = $axle_id;
			$arr['shop_name'][] = $shop_name;
		}
		return $arr;
	}
	//change the vehicle type later - 4w to tyres
	$sql_mecs ="SELECT (CASE WHEN b2b.monthly_count IS NOT NULL THEN b2b.monthly_count ELSE 0 END) as monthly_count,m.*,(6371*ACOS(COS(RADIANS('$booking_lat'))*COS(RADIANS(m.lat))*COS(RADIANS(m.lng)-RADIANS('$booking_lng'))+SIN(RADIANS('$booking_lat'))*SIN(RADIANS(m.lat)))) as dist FROM go_bumpr.admin_mechanic_table m LEFT JOIN (SELECT b.b2b_shop_id,count(CASE WHEN b.b2b_booking_id is not null THEN b.b2b_booking_id ELSE 0 END) as monthly_count FROM b2b.b2b_booking_tbl_tyres b WHERE MONTH(b.b2b_log) = '$this_month' GROUP BY b.b2b_shop_id) b2b ON b2b.b2b_shop_id = m.axle_id WHERE m.status=0 AND m.type='tyres' AND m.address5='$city' AND m.axle_id NOT IN (1014,1035,1670)AND (6371*ACOS(COS(RADIANS('$booking_lat'))*COS(RADIANS(m.lat))*COS(RADIANS(m.lng)-RADIANS('$booking_lng'))+SIN(RADIANS('$booking_lat'))*SIN(RADIANS(m.lat))))<= 10 AND m.wkly_counter >0 HAVING monthly_count < 25 ORDER BY dist LIMIT 2";
	//echo $sql_mecs;
	
	$res_mecs = mysqli_query($conn,$sql_mecs);
	$rtn_arr = fetch($res_mecs,$tyre_brand);
	if(empty($rtn_arr))
	{
		$mec_id_leads = '';
		$axle_id_leads = '';
		$shop_name = '';
	}
	else
	{
		$mec_id_leads = implode(";",$rtn_arr['mec_id']);
		$axle_id_leads = implode(";",$rtn_arr['axle_id']);
		$shop_name = implode(";",$rtn_arr['shop_name']);
	}
	
	 if(!empty($mec_id_leads) || $mec_id_leads!=0)
          {
           $mec_id = explode(';', $mec_id_leads);
           //print_r($mec_id);
           $mec=implode(',',$mec_id);

           //print_r($mec);
       // $shops_name_arr = array();
       $shops = '';
        
       $mec_leads=implode(',',$mec_id_leads);
       //echo $mec_leads;
       $axle_leads=implode(',',$axle_id_leads);
      	$sql_shop="SELECT shop_name,mec_id,address4,axle_id from admin_mechanic_table m JOIN b2b.b2b_credits_tbl as b ON m.axle_id=b.b2b_shop_id where mec_id in ($mec) AND b.b2b_credits > 0 LIMIT 2 "; 
            // echo $sql_shop; 
            // die;
                $res_shop = mysqli_query($conn,$sql_shop);
              while($row_shop = mysqli_fetch_object($res_shop)){
                 
               $mec = $row_shop->mec_id;
               $shops = $row_shop->shop_name;
               $shops_address4 = $row_shop->address4;    
               $axle = $row_shop->axle_id;           
				    			
       		   $shops_name[] = $shops ;
               $axles_id[] = $axle;
               $mecs_id[] = $mec; 

        $mec_id_leads = implode(";",$mecs_id);
		$axle_id_leads = implode(";",$axles_id);
		$shop_name = implode(";",$shops_name);           
				    			
       $html1.="<option value='".$mec."'> ".$shops." - ".$shops_address4."</option>";
        
          }
       }

      
       $data = array('shopname'=>$html1,'mec_ref'=>$mec_id_leads,'axle_ref'=>$axle_id_leads,'shopname_ref'=>$shop_name);
       echo json_encode($data);

         ?>
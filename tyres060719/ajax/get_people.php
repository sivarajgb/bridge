  <?php
  include("../config.php");
  $conn = db_connect1();
  
  $city = $_GET['city'];
  $crm_cluster = $city!='Chennai'? 'all' : $_GET['cluster'];
  ?>
  <!--<div  class="btn-group" data-toggle="buttons" id="rad_btn">-->
<?php
//echo "SELECT crm_log_id,name FROM crm_admin WHERE crm_flag='1' AND city='$city'";
if($city == "all"){
    $sql_crm_people="SELECT crm_log_id,name FROM crm_admin WHERE crm_tyr_flag='1'";
}
else{
    $sql_crm_people= $crm_cluster == 'all' ? "SELECT crm_log_id,name FROM crm_admin WHERE crm_tyr_flag='1' AND city='$city'" :"SELECT crm_log_id,name FROM crm_admin WHERE crm_tyr_flag='1' AND city='$city' AND crm_cluster='$crm_cluster'";
}
echo "<option value=''>Assign Person</option>";
$res = mysqli_query($conn,$sql_crm_people);
while ($row_crm_people=mysqli_fetch_object($res)) {
    $crm_id_get = $row_crm_people->crm_log_id;
    $crm_name_get = $row_crm_people->name;
    //echo "<option value=".$crm_id_get.">".$crm_name_get."</option>"; 
    ?>
    <!--<label class="btn btn-default"  style="background-color: #B6A3BF;color:#000; font-size:14px; box-shadow: 0px 8px 8px 0px rgba(0,0,0,0.2);">
        <strong> <input name="crm_people" value="<?php echo $crm_id_get; ?>" type="radio"><?php echo $crm_name_get; ?></strong>
    </label>-->
      <option value="<?php echo $crm_id_get; ?>" ><?php echo $crm_name_get; ?></option>
<?php } 
?>
  <!--</div>-->
  <script>
 $('#crm_list1').on('change',function(){

     var crm_person_id = $(this).val();
     var modal = $('#Assign_myModal');
     //alert(crm_person_id);
    // console.log(crm_person_id);
     var check_boxes = [];
    $("input[name='check_veh[]']:checked").each(function() {
      check_boxes.push($(this).val());
    });
   //var check_boxes = document.querySelectorAll("#check_veh:checked").val();
  // console.log(check_boxes);
    if(check_boxes!=""){
    if(crm_person_id !=""){
      
      $.ajax({
        url : "ajax/get_crm_name.php",  // create a new php page to handle ajax request
        type : "POST",
        data : {"crm_person_id": crm_person_id},
        success : function(data) {
         
          modal.modal('show');
          $("#mes").html("Are you sure you want to allocate the selected bookings to "+data+"?");        
        },
      error: function (xhr, ajaxOptions, thrownError) {
      //  alert(xhr.status + " "+ thrownError);
     }
   });
      
    }else{
      alert("Please Select Person Name");
    }
  }else{
    alert("Please Select atleast one customer to assign");
    $('#crm_list1').val("");
  }
 });

 $('#assign_btn').on('click',function(){
  var crm_person_id = $('#crm_list1').val();
     var modal = $('#Assign_myModal');
     var modal_suc = $('#success_myModal');
     //alert(crm_person_id);
    // console.log(crm_person_id);
     var check_boxes = [];
    $("input[name='check_veh[]']:checked").each(function() {
      check_boxes.push($(this).val());
    });
  $.ajax({
        url : "ajax/allocate_crm_person.php",  // create a new php page to handle ajax request
        type : "POST",
        data : {"check_boxes":check_boxes, "crm_person_id": crm_person_id},
        success : function(data) {
        //alert(data);
        modal.modal('hide');
        modal_suc.modal('show');
        $("#mes_succ").html(data);
        $("#loading").hide();
        $("#table").show();
        viewtable();
        $('#crm_list1').val("");
        //modal.modal('hide');
        },
      error: function (xhr, ajaxOptions, thrownError) {
      //  alert(xhr.status + " "+ thrownError);
     }
   });
 });
</script>
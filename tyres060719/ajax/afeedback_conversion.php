<?php
include("../config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();
$today=date('Y-m-d');

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));
$shop_id = $_POST['shop_id'];
$city = $_POST['city'];

$_SESSION['crm_city'] = $city;

$cond =''; 
$cond = $cond.($city == 'all' ? "" : " AND f.city='$city'");
$cond = $cond.($shop_id == 'all' ? "" : " AND f.shop_id='$shop_id'");


$sql_booking = "SELECT f.booking_id,f.feedback_status,f.service_status,b2b_b.b2b_acpt_flg,b2b_b.b2b_check_in_report,b2b_b.b2b_vehicle_at_garage,b2b_b.b2b_vehicle_ready FROM feedback_track_tyres f LEFT JOIN b2b.b2b_booking_tbl_tyres b2b_b ON f.b2b_booking_id = b2b_b.b2b_booking_id WHERE f.flag = 0 {$cond} AND f.b2b_booking_id = b2b_b.b2b_booking_id AND b2b_b.b2b_acpt_flg = '1' AND f.shop_id NOT IN (1014,1035,1670) AND DATE(f.crm_update_time) BETWEEN '$startdate' AND '$enddate'";

$sql_booking1 = "SELECT sum(f.tyre_count) as tyrecount FROM feedback_track_tyres as f WHERE f.flag = 0 {$cond} AND f.shop_id NOT IN (1014,1035,1670) AND DATE(f.crm_update_time) BETWEEN '$startdate' AND '$enddate'";
//echo $sql_booking1;
$res_booking1 = mysqli_query($conn1,$sql_booking1);
$row_booking1 = mysqli_fetch_object($res_booking1);
$total_sold = $row_booking1->tyrecount;
if ($total_sold == '') {
	$total_sold = 0;
}

$res_booking = mysqli_query($conn1,$sql_booking);
$converted = 0;
$yet_to = 0;
$completed = 0;
$rnr = 0;
$not_contacted = 0;

//echo $sql_booking;
if(mysqli_num_rows($res_booking) >=1){
	$total=mysqli_num_rows($res_booking);
	while($row_booking = mysqli_fetch_object($res_booking)){
	$booking_id = $row_booking->booking_id;
	$feedback_status = $row_booking->feedback_status;
	$service_status = $row_booking->service_status;
	$check_in = $row_booking->b2b_check_in_report;
	$at_garage = $row_booking->b2b_vehicle_at_garage;
	$vehicle_ready = $row_booking->b2b_vehicle_ready;
	
	if($service_status == 'Purchased' || $check_in==1 || $at_garage == 1 || $vehicle_ready == 1)
	{
		$converted = $converted+1;
	}
	switch($service_status)
	{
		case 'Yet to Purchased': $yet_to = $yet_to + 1;
											break;
		case 'Purchased': $completed = $completed + 1;
							break;
		default: if($feedback_status>10 && $feedback_status<52)
					{
						$rnr = $rnr + 1;
					}
					elseif($feedback_status == 0)
					{
						$not_contacted = $not_contacted + 1;
					}
					break;
	}
	}
	$rate = ($converted/$total)*100;
} // if
else {
  //echo $sql_booking;
  $total = 0;
  $converted = 0;
  $rate = 0;
}

$conv_arr['total'] = $total;
$conv_arr['converted'] = $converted;
$conv_arr['rate'] = floor($rate)."%";
$conv_arr['yet_to'] = $yet_to;
$conv_arr['completed'] = $completed;
$conv_arr['rnr'] = $rnr;
$conv_arr['not_contacted'] = $not_contacted;
$conv_arr['total_sold'] = $total_sold;
echo json_encode($conv_arr);

?>
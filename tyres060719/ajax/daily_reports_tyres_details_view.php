<?php
//error_reporting(E_ALL); ini_set('display_errors', 1);
include("../config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();
$today=date('Y-m-d');

 $crm_log_id = $_SESSION['crm_log_id'] ;
 $crm_name = $_SESSION['crm_name'];
 $flag=$_SESSION['flag'];

$startdate = date('Y-m-d',strtotime($_POST['startdate']));
$enc_date = base64_encode($startdate);
$enddate =  date('Y-m-d',strtotime($_POST['enddate']));




$total_veh = 0;
$total_credits =0;


?>


<div style="width:90%; margin-left:20px;margin-left:60px;clear:both;">
<table id="example3" class="table table-striped table-bordered tablesorter table-hover results" style="max-width:95%;">
<thead style="background-color: #b2dfdb;">
<th style="text-align:center;">Service Center</th>
<th style="text-align:center;">Locality</th>
<th style="text-align:center;">VehiclesSent <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Credits <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">No.of quote <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
<th style="text-align:center;">Quote Selected <i class="fa fa-sort" aria-hidden="true" style="font-size:11px;"></i></th>
</thead>
<tbody id="tbody">
<?php


	$total_veh_garage = $total_credits_garage = $total_accepted = $total_rejected = $total_idle = $total_completed_garage = $total_end_garage = $total_tyres_credits = $total_bid_quote = $total_acpt_quote = $total_vehicle_sent= 0;

        $sql_booking_tyre =  "SELECT bb.b2b_shop_id,am.shop_name,bm.b2b_address4,count(bb.b2b_shop_id) as vehicles_sent,sum(CASE WHEN(bb.b2b_bid_flg = 1) THEN bb.b2b_credit_amt ELSE 0 END) as tyre_credits,count(CASE WHEN(bb.b2b_bid_flg = 1) THEN 1 END) as bid_quote,count(CASE WHEN(bb.b2b_acpt_flg = 1) THEN 1 END) as acpt_quote FROM b2b_booking_tbl_tyres as bb LEFT JOIN go_bumpr.tyre_booking_tb as tb ON tb.booking_id=bb.gb_booking_id LEFT JOIN b2b_mec_tbl as bm on bb.b2b_shop_id=bm.b2b_shop_id LEFT JOIN go_bumpr.admin_mechanic_table am ON am.axle_id = bm.b2b_shop_id WHERE bm.b2b_shop_id NOT IN (1014,1035,1670,'') AND DATE(bb.b2b_log) BETWEEN '$startdate' and '$enddate' group by bb.b2b_shop_id";



    $res_booking_tyre = mysqli_query($conn2,$sql_booking_tyre);
	 //echo $sql_booking_tyre;
	while($row_booking_tyre = mysqli_fetch_object($res_booking_tyre)){

		$b2b_shop_name = $row_booking_tyre->shop_name;
     
        $locality = $row_booking_tyre->b2b_address4;

        $vehicles_sent = $row_booking_tyre->vehicles_sent;
		$total_vehicle_sent=$total_vehicle_sent+$vehicles_sent;

        $tyre_credits = $row_booking_tyre->tyre_credits;
		$total_tyres_credits=$total_tyres_credits+$tyre_credits;

        $bid_quote = $row_booking_tyre->bid_quote;
		$total_bid_quote=$total_bid_quote+$bid_quote;

        $acpt_quote = $row_booking_tyre->acpt_quote;
		$total_acpt_quote=$total_acpt_quote+$acpt_quote;

?>

<tr>
<td style="text-align:center;"><?php echo $b2b_shop_name; ?></td>
<td style="text-align:center;"><?php echo $locality; ?></td>
<td style="text-align:center;"><?php echo $vehicles_sent; ?></td>
<td style="text-align:center;"><?php echo $tyre_credits/100; ?></td>
<td style="text-align:center;"><?php echo $bid_quote; ?></td>
<td style="text-align:center;"><?php echo $acpt_quote; ?></td>
</tr>
<?php
}
?>
</tbody>
<tfoot>
<td colspan=2 style="text-align:center;color: #000000;font-weight: bold;">Total</td>
<td style="text-align:center;background: #D3D3D3;"><?php echo $total_vehicle_sent; ?></td>
<td style="text-align:center;background: #D3D3D3;"><?php echo $total_tyres_credits/100; ?></td>
<td style="text-align:center;background: #D3D3D3;"><?php echo $total_bid_quote; ?></td>
<td style="text-align:center;background: #D3D3D3;"><?php echo $total_acpt_quote; ?></td>
</tfoot>

</table>
</div>


<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example1").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[3,3],[0,0]]} );
    }
);
</script>
<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example2").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[3,3],[0,0]]} );
    }
);
</script>
<!-- table sorter -->
<script>
$(document).ready(function()
    {
        $("#example3").tablesorter( {cssInfoBlock : "avoid-sort",sortList: [[4,4],[0,0]]} );
    }
);
</script>
<?php


?>
<?php
include("../config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();
$today=date('Y-m-d');

$crm_log_id = $_SESSION['crm_log_id'] ;
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];

 $startdate = date('Y-m-d',strtotime($_POST['startdate']));
 $enddate =  date('Y-m-d',strtotime($_POST['enddate']));
 $shop_id = $_POST['shop_id'];
 $city = $_POST['city'];
 $followup = $_POST['followup'];

 $_SESSION['crm_city'] = $city;

 $cond_city_all ='';
 $cond_city_all = $cond_city_all.($city == 'all' ? "" : "AND b.city='$city'");

//0-all && 1-particular
// class bookings {
//   private $shop_id;
//   private $city;

//   function __construct($shop_id,$city){
//     $this->shop_id = $shop_id;
//     $this->city = $city;
	
//   }
  
// }
// $shop_id_fun = $shop_id=='all' ? "" : $shop_id;

// $bookings_obj = new bookings($shop_id,$master_service_fun,$service_type_fun,$city);

// $shop_id_val = $shop_id=='all' ? "0" : "1";
// $city_val = $city=='all' ? "0" : "1";

// $cond = $bookings_obj->{"sh{$shop_id_val}_c{$city_val}"}();

$sql_booking = "SELECT b.booking_id,b.feedback_status,b.service_status,b2b_b.b2b_check_in_report,b2b_b.b2b_vehicle_at_garage,b2b_b.b2b_vehicle_ready FROM go_bumpr.tyre_booking_tb b JOIN b2b.b2b_booking_tbl_tyres b2b_b ON b.booking_id = b2b_b.gb_booking_id JOIN go_bumpr.user_register r ON b.user_id = r.reg_id JOIN b2b.b2b_credits_tbl b2b_c ON b2b_b.b2b_shop_id = b2b_c.b2b_shop_id WHERE b.axle_flag= 1 AND b2b_b.b2b_acpt_flg= 1 AND DATE(b2b_b.b2b_log) BETWEEN '$startdate' and '$enddate' AND b2b_b.b2b_shop_id NOT IN (1014,1035,1670) {$cond_city_all}";
//echo $sql_booking;
$res_booking = mysqli_query($conn2,$sql_booking);
$no = 0;
$yet_to = 0;
$completed = 0;
$in_progress = 0;
$rnr = 0;
$not_contacted = 0;

//echo $sql_booking;
if(mysqli_num_rows($res_booking) >=1){
	$total=mysqli_num_rows($res_booking);
	while($row_booking = mysqli_fetch_object($res_booking)){
	$booking_id = $row_booking->booking_id;
	$feedback_status = $row_booking->feedback_status;
	$service_status = $row_booking->service_status;
	$check_in = $row_booking->b2b_check_in_report;
	$at_garage = $row_booking->b2b_vehicle_at_garage;
	$vehicle_ready = $row_booking->b2b_vehicle_ready;
	if($service_status == 'Completed' || $service_status == 'In Progress' || $check_in==1 || $at_garage == 1 || $vehicle_ready == 1)
	{
		$no = $no+1;
	}
	switch($service_status)
	{
		case 'Yet to Service his Vehicle': $yet_to = $yet_to + 1;
											break;
		case 'Completed': $completed = $completed + 1;
							break;
		case 'In Progress': $in_progress = $in_progress + 1;
							break;
		default: if($feedback_status>10 && $feedback_status<52)
					{
						$rnr = $rnr + 1;
					}
					else
					{
						$not_contacted = $not_contacted + 1;
					}
					break;
	}
	}
	$rate = ($no/$total)*100;
} // if
else {
  //echo $sql_booking;
  $total = 0;
  $no = 0;
  $rate = 0;
}
 ?>
<table class="table table-bordered" style="background:white;">
<tr>
<td style="text-align:center;vertical-align: middle;"><strong>Total Go Axles </strong></td>
<td style="text-align:center;vertical-align: middle;"><?php echo $total;?></td>
</tr>
<tr>
<td style="text-align:center;vertical-align: middle;"><strong>Total Converted </strong></td>
<td style="text-align:center;vertical-align: middle;"><?php echo $no;?></td>
</tr>
<tr>
<td style="text-align:center;vertical-align: middle;"><strong>Conversion Rate </strong></td>
<td style="text-align:center;vertical-align: middle;"><?php echo floor($rate);?>%</td>
</tr>
<tr>
<td style="text-align:center;vertical-align: middle;"><strong>Yet to Service </strong></td>
<td style="text-align:center;vertical-align: middle;"><?php echo $yet_to;?></td>
</tr>
<tr>
<td style="text-align:center;vertical-align: middle;"><strong>In Progress </strong></td>
<td style="text-align:center;vertical-align: middle;"><?php echo $in_progress;?></td>
</tr>
<tr>
<td style="text-align:center;vertical-align: middle;"><strong>Completed </strong></td>
<td style="text-align:center;vertical-align: middle;"><?php echo $completed;?></td>
</tr>
<tr>
<td style="text-align:center;vertical-align: middle;"><strong>RNR </strong></td>
<td style="text-align:center;vertical-align: middle;"><?php echo $rnr;?></td>
</tr>
<tr>
<td style="text-align:center;vertical-align: middle;"><strong>Not Contacted </strong></td>
<td style="text-align:center;vertical-align: middle;"><?php echo $not_contacted;?></td>
</tr>
</table>

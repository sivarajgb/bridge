<?php
include("sidebar.php");
$conn = db_connect1();
$conn2 = db_connect2();
// login or not
if((empty($_SESSION['crm_log_id']))) {

  header('location:logout.php');
  die();
}

$s_date = base64_decode($_GET['sdt']);
$e_date = base64_decode($_GET['edt']);
$sid = base64_decode($_GET['sid']);
$city = $_SESSION['crm_city'];
?>

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>GoBumpr Bridge</title>


  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


  <!-- Include Date Range Picker -->
  <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<!-- date time picker -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

  <!-- table sorter -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js"></script>

<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>
<!-- star rating -->
<link rel="stylesheet" href="css/star-rating.min.css">
<script src="js/star-rating.min.js"></script>
<style>

.pollSlider{
    position:fixed;
    height:355px;
    background:#aaa;
    width:400px;
    right:0px;
    margin-right: -400px;
    top:240px;
}
#pollSlider-button{
  cursor:pointer;
    position:fixed;
    /*width:15px;
    height:30px;*/
    right:0px;
    //background:lightcoral;
    top:420px;
}

/*home page blocks */
.floating-box {
 display: inline-block;
 margin: 2px;
 padding-top: 12px;
 padding-left: 12px;
 width:300px;
}
.navbar-fixed-top{
  z-index:100 !important;
}
.upper-div{
  z-index:999999 !important;
}
#range > span:hover{cursor: pointer;}
 /* table */
#tbody{
   font-size:15px !important;
  border:1.5px solid #c4b8b8 !important;

}
thead:hover{
  cursor:pointer;
}

.results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px;
  color:#9E9E9E;
}
#tbody, tbody tr {
    -webkit-animation: opacity 5s ease-in-out;
    animation: opacity 5s ease-in-out;
}
.datepicker {
    cursor:pointer;
  }
.datepicker:before {
content: '';
display: inline-block;
border-left: 7px solid transparent;
border-right: 7px solid transparent;
border-bottom: 7px solid #ccc;
border-bottom-color: transparent !important;
position: absolute;
top: -7px;
left: 190px;  // I made a change here 
}

.datepicker:after {
content: '';
display: inline-block;
border-left: 6px solid transparent;
border-right: 6px solid transparent;
border-top-color: transparent !important;
border-top: 6px solid #ffffff;
position: absolute;
bottom: -6px;
left: 191px;  // I made a change here 
}

</style>

</head>
<body id="body">
<?php include_once("header.php"); ?>
<script>
  $(document).ready(function(){
    $('#city').show();
  })
</script>
<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>
<div class="container" style="margin-left: 35px;margin-right: 0px;    width: 1280px!important;">
<!-- date range picker -->
<div id="reportrange" class=" col-sm-3 " style="cursor: pointer; margin-top:28px;">
    <div class=" floating-box1">
        <div id="range" class="form-control" style="min-width:312px;margin-left: -55px;">
        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
        <span id="dateval"></span> <b class="caret"></b>
        </div>
    </div>
</div>
<!-- rating filter -->
<div class=" col-sm-2 col-lg-2" style="cursor: pointer; margin-top:28px;max-width:160px;">
    <div class=" floating-box1">
      <select id="ratings" name="ratings" class="form-control" style="max-width:160px;">
    <option value="all">All Ratings</option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="none">Not updated</option>
      </select>
    </div>
</div>
<!-- shop filter -->
<div class=" col-sm-2 col-lg-2" style="cursor: pointer; margin-top:28px;max-width:170px;">
    <div class=" floating-box1">
      <select id="shop_list" name="shop_list" class="form-control" style="max-width:170px;">
      </select>
    </div>
</div>
<!-- search bar -->
<div class=" col-sm-3" style="margin-top:28px;width: 22%;">
  <div  class="floating-box1" >
  <div class="form-group" >
        <input type="text" class="search form-control" id="su" name="mobile" placeholder="Search">   
  
  </div>
  </div>
  </div>
</div>

<div style="margin-left:10px;">
<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home"><strong>Home</strong></a></li>
    <li><a data-toggle="tab" href="#followup_tbl"><strong>FollowUps</strong></a></li>
</ul>
<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
  <div align="center" id = "table" style="margin-top:20px;margin-left:10px;margin-right:10px;">
<table id="example" class="table table-striped table-bordered tablesorter table-hover results" style="min-width:1800px;text-align:center;">
 <thead style="background-color: #D3D3D3;">
  <tr>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">No</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">GoAxle Date</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Lead Status</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">BookingId</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">CustomerName</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Mobile</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">ShopName</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Vehicle</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">TyreBrand</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">TyreCount</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Garage Bill</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Rating</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Review</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Final Bill Paid</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Comments</th>
  <th style="text-align:center;vertical-align: middle;" colspan="2">RTT</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Reason</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Status</th>
  </tr>
  <tr>
  <th style="text-align:center;vertical-align: middle;">BidAceepted</th>
  <th style="text-align:center;vertical-align: middle;">Delivered</th>
  </tr>
  </thead><tbody id="tbody">
    </tbody>
  </table>
</div>
    <div id="show" style="width:100%;display:none;">
    </div>
    <!-- loading -->
<div id="loading" style="display:none; margin-top:140px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>

  </div>
  <div id="followup_tbl" class="tab-pane fade">
  <div align="center" id = "table1" style="margin-top:20px;margin-left:10px;margin-right:10px;">
<table id="example1" class="table table-striped table-bordered tablesorter table-hover results" style="min-width:1800px;text-align:center;">
 <thead style="background-color: #D3D3D3;">
  <tr>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">No</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">GoAxle Date</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">BookingId</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">CustomerName</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Mobile</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">ShopName</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Vehicle</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">TyreBrand</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">TyreCount</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Garage Bill</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Rating</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Review</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Final Bill Paid</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Comments</th>
  <th style="text-align:center;vertical-align: middle;" colspan="2">RTT</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Reason</th>
  <th style="text-align:center;vertical-align: middle;" rowspan="2">Status</th>
  </tr>
  <tr>
  <th style="text-align:center;vertical-align: middle;">BidAceepted</th>
  <th style="text-align:center;vertical-align: middle;">Delivered</th>
  </tr>
  </thead><tbody id="tbody1">
    </tbody>
  </table>
</div>
    <div id="show_followup" style="width:100%;display:none;">
    </div>
    <!-- loading -->
<div id="loading1" style="display:none; margin-top:140px;" align="center">
  <div class='uil-default-css' style='transform:scale(0.58);'>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
  <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
  </div>
</div>
</div>

  </div>
</div>
</div>
<div class="pollSlider">
<div id="end-conversion" style="margin:20px;height: 100%;">

</div>
</div>
<i id="pollSlider-button" class="fa fa-chevron-circle-left" aria-hidden="true" style="font-size:40px;background-color: white;border-radius: 100%;"></i>
<!-- jQuery library -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- date range picker -->
<script>
$('input[name="daterange"]').daterangepicker({
 locale: {
      format: 'DD-MM-YYYY'
    }
});
</script>
<script type="text/javascript">
$(function() {

  //var start = moment().subtract(1, 'days');
  //var start = moment().startOf('month');
  var s_date = '<?php echo $s_date; ?>';
  var e_date = '<?php echo $e_date; ?>';
  if(s_date == '' || e_date == '')
  {
  e_date = s_date = moment().subtract(5, 'days');
  }
  var start = moment(s_date);
   var end = moment(e_date);
   // var start = moment();
    //var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});
</script>
<!-- default view -->
<script>
function viewEndConversion(){
  
}
function viewtable(){
  var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY/MM/DD');
  var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY/MM/DD');
  //var vehicle_type = $('#vehicle_type').val();
  var shop_id = $('#shop_list').val();
  var ratings = $('#ratings').val();
  var city = $('#city').val();
  //console.log(startDate);
  //console.log(endDate);
  var followup = "no";
  //alert(followup);
  if(target==="#followup_tbl")
  {
    followup = "yes";

    $("#tbody1").empty();
    $("#loading1").show();
    
  }
  else
  {
    $("#tbody").empty();
    $("#loading").show();
    
     }
  //alert("hi");

  //Make AJAX request, using the selected value as the POST
  $.ajax({
      url : "ajax/feedback_panel_view.php",  // create a new php page to handle ajax request
      type : "POST",
    //json : false,
    dataType : 'json',
      data : {"startdate": startDate , "enddate": endDate, "city":city,"shop_id":shop_id,"followup":followup,"ratings":ratings},
      success : function(data) {

        //alert(data);
        if(followup === "yes")
        {
      //$('#show_followup').html(data);
        
          $("#tbody1").empty();
      if(data.data2 === "no"){

       $("#tbody1").html("<h2 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h2>");
        }
        else{
          function displayInfo1(item,index){
                    $('#tbody1').append(item['tr']);
                    //console.log(item);
                  }
                  $.map(data.data2, displayInfo1);
          $("#example1").tablesorter( {sortList: [[0,0], [1,0]]} );
        }
      $('#show_followup').show();
      $('#show').hide();
      counterval1();  
      $("#loading1").hide();
      
    }

    else
    {
     // $('#show').html(data);

          $("#tbody").empty();
      if(data.data1 === "no"){
        $("#tbody").html("<h2 align='center' style='margin-top:165px;'><p>No Results Found!!!</p></h2>");
        }
        else{
          function displayInfo(item,index){
                    $('#tbody').append(item['tr']);
                    //console.log(item);
                  }
                  $.map(data.data1, displayInfo);
          $("#example").tablesorter( {sortList: [[0,0], [1,0]]} );
        }
      $('#show').show();
      $('#show_followup').hide();
      counterval();  
      $("#loading").hide();
      
    }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        //  alert(xhr.status + " "+ thrownError);
      }
  });
  $.ajax({
      url : "ajax/end_conversion.php",  // create a new php page to handle ajax request
      type : "POST",
      data : {"startdate": startDate , "enddate": endDate, "city":city,"shop_id":shop_id,"followup":followup,"ratings":ratings},
      success : function(data) {
      $('#end-conversion').html(data);
      $('#end-conversion').show();
       },
      error: function(xhr, ajaxOptions, thrownError) {
        //  alert(xhr.status + " "+ thrownError);
      }
  });
}
</script>

<script>
$(document).ready(function(){
  $('#show').hide();
  $('#show_followup').hide();
  $("#loading").show();
  $("#loading1").show();
  $("#tbody1").empty();
  $("#tbody").empty();
  get_shops();
});
</script>
<!-- on change of date -->
<script>
$(document).ready( function (){
  $('#dateval').on("DOMSubtreeModified", function (){
    //console.log("date changed");
     $('#show').hide();
     $('#show_followup').hide();
     $("#loading").show();
     $("#loading1").show();
     get_shops();
   });
});
</script>
<!--  on Selecting vehicles -->
<script>
var target;
$(document).ready(function() {
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    target = $(e.target).attr("href") // activated tab
    if(target == '#followup_tbl'){$("#tbody1").empty();}
    if(target == '#home'){$("#tbody").empty();}
    
    viewtable();
  });
/*$("#vehicle_type").change(function (){
  $('#show').hide();
  $("#loading").show();
  viewtable();
});*/
$("#shop_list").change(function (){
  $('#show').hide();
  $('#show_followup').hide();
  $("#loading").show();
  $("#loading1").show();
  $("#tbody1").empty();
  $("#tbody").empty();
  viewtable();
});
$("#ratings").change(function (){
  $('#show').hide();
  $('#show_followup').hide();
  $("#loading").show();
  $("#loading1").show();
  $("#tbody1").empty();
  $("#tbody").empty();
  viewtable();
});
});
</script>
<!--  on changing city -->
<script>
$(document).ready(function() {
$("#city").change(function (){
  $('#show').hide();
  $('#show_followup').hide();
  $("#loading").show();
  $("#loading1").show();
  $("#tbody1").empty();
  $("#tbody").empty();
  viewtable();
} );
});
</script>

<!-- search bar  -->
<script>
$(document).ready(function() {
  $(".search").keyup(function () {
    var searchTerm = $(".search").val();
    var listItem = $('.results tbody').children('tr');
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

  $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });

  $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','false');
  });

  $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','true');
  });

      });
});
</script>
<script>
function counterval(){
  var jobCount = $("#tbody tr").length;
   $('.counter').text(jobCount + ' item');
}
function counterval1(){
  var jobCount = $("#tbody1 tr").length;
   $('.counter').text(jobCount + ' item');
}
</script>

<script>
function get_shops(){
  var shop_id = '<?php echo $sid; ?>'
  if(shop_id == 'undefined')
  {
    shop_id = '';
  }
  //console.log(shop_id);
  var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
  var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
  $.ajax({
      url : "ajax/get_shops_fb_panel.php",  // create a new php page to handle ajax request
      type : "POST",
      data : {"start": startDate,"end": endDate,"sid":shop_id},
      success : function(data) {
    $("#shop_list").empty();
        $('#shop_list').append(data);
        //$("#table").hide();
    $("#loading").show();
    $("#loading1").show();
    viewtable();
  },
      error: function(xhr, ajaxOptions, thrownError) {
            //  alert(xhr.status + " "+ thrownError);
      }
  });
}
</script>

<script>
$(document).ready(function()
{
    var slider_width = Math.round($('.pollSlider').width());//get width automaticly
  $('#pollSlider-button').click(function() {
  
  $(this).toggleClass('fa-chevron-circle-right');
    if($(this).css("margin-right") == slider_width+"px" && !$(this).is(':animated'))
    {
        $('.pollSlider,#pollSlider-button').animate({"margin-right": '-='+slider_width});
    }
    else
    {
        if(!$(this).is(':animated'))//perevent double click to double margin
        {
            $('.pollSlider,#pollSlider-button').animate({"margin-right": '+='+slider_width});
        }
    }


  });
 });     
</script>
</body>
</html>

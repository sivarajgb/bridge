<?php
include("config.php");
$conn1 = db_connect1();
$conn2 = db_connect2();
session_start();
$crm_log_id = $_SESSION['crm_log_id'];
$crm_name = $_SESSION['crm_name'];
$flag=$_SESSION['flag'];
$feedback_admin=$_SESSION['feedback_admin'];
date_default_timezone_set("Asia/Kolkata");
$today=date('Y-m-d H:i:s');
$prev_feedback_status = mysqli_real_escape_string($conn1, $_REQUEST['feedback_status']);
$status = mysqli_real_escape_string($conn1, $_REQUEST['status']);
$reason = mysqli_real_escape_string($conn1, $_REQUEST['reason']);
$subreason = mysqli_real_escape_string($conn1, $_REQUEST['subreason']);
$RNRreason = mysqli_real_escape_string($conn1, $_REQUEST['RNRreason']);
$cancelledReason = mysqli_real_escape_string($conn1, $_REQUEST['cancelledReason']);
$followup_date = mysqli_real_escape_string($conn1, date('Y-m-d H:i:s',strtotime($_REQUEST['followup_date'])));
$delivery_date = mysqli_real_escape_string($conn1, date('Y-m-d H:i:s',strtotime($_REQUEST['delivery_date'])));
$rating = mysqli_real_escape_string($conn1, $_REQUEST['rating']);
$booking_id = mysqli_real_escape_string($conn1, $_REQUEST['book_id']);
$user_id = mysqli_real_escape_string($conn1, $_REQUEST['user_id']);
$b2b_booking_id = mysqli_real_escape_string($conn1, $_REQUEST['b2b_book_id']);
$final_bill_amount = mysqli_real_escape_string($conn1, $_REQUEST['final_bill_amount']);
$comments = mysqli_real_escape_string($conn1, $_REQUEST['comments']);
$rtt_updates = mysqli_real_escape_string($conn1, $_REQUEST['rtt_updates']);
$sdt = mysqli_real_escape_string($conn1, $_REQUEST['s_date']);
$edt = mysqli_real_escape_string($conn1, $_REQUEST['e_date']);
$goaxle_date = mysqli_real_escape_string($conn1, $_REQUEST['g_date']);
$page = mysqli_real_escape_string($conn1, $_REQUEST['page']);
$dt = mysqli_real_escape_string($conn1, $_REQUEST['dt']);
$tab = mysqli_real_escape_string($conn1, $_REQUEST['tab']);
$shopName = mysqli_real_escape_string($conn1, $_REQUEST['shop_name']);
$userName = mysqli_real_escape_string($conn1, $_REQUEST['user_name']);
$shop_id = mysqli_real_escape_string($conn1, $_REQUEST['shop_id']);
$gbQuoteAmt = mysqli_real_escape_string($conn1, $_REQUEST['gobumpr_quoted_amt']);
$garageQuoteAmt = mysqli_real_escape_string($conn1, $_REQUEST['garage_quoted_amt']);
$localGarageQuoteAmt = mysqli_real_escape_string($conn1, $_REQUEST['local_garage_quoted_amt']);
$svcPriority = mysqli_real_escape_string($conn1, $_REQUEST['service_priority']);
$lock_reason = '';
$completed_flag=0;

switch($status)
{
	case 'RNR': 		
				if($prev_feedback_status < 10)
				{
					$feedback_status = 11;
					$followup_date = date('Y-m-d H:i:s',time()+(3*60*60)); //3hrs from now --> RNR 12
				}
				if($prev_feedback_status == 11)
				{
					$feedback_status = 12;
					//$followup_date = date('Y-m-d H:i:s',strtotime("+1 days")); //1day from now --> RNR 21
					$followup_date = date('Y-m-d H:i:s',time()+(24*60*60)); //24hrs from now --> RNR21
				}
				if($prev_feedback_status == 12)
				{
					$feedback_status = 21;
					$followup_date = date('Y-m-d H:i:s',time()+(3*60*60)); //3hrs from now --> RNR 22
				}
				if($prev_feedback_status == 21)
				{
					$feedback_status = 22;
					$followup_date = date('Y-m-d H:i:s',time()+(24*60*60)); //24hrs from now --> RNR31
				}
				if($prev_feedback_status == 22)
				{
					$feedback_status = 31;
					$followup_date = date('Y-m-d H:i:s',time()+(24*60*60)); //24hrs from now --> RNR41
				}
				if($prev_feedback_status == 31)
				{
						if($RNRreason=='Cutting call Hearing GoBumpr'){
							$feedback_status = -2;
							$followup_date = date('Y-m-d H:i:s',strtotime("+90 days"));
						}else{
							$feedback_status = 41;
							$followup_date = date('Y-m-d H:i:s',time()+(72*60*60)); //72hrs from now --> RNR51
						}
					
				}
				if($prev_feedback_status == 41)
				{
					if($RNRreason=='Cutting call Hearing GoBumpr'){
						$feedback_status = -2;
						$followup_date = date('Y-m-d H:i:s',strtotime("+90 days"));
					}else{
						$feedback_status = 51;
						$followup_date = date('Y-m-d H:i:s',time()+(120*60*60)); //120hrs from now --> RNR51
					}
				}
				if($prev_feedback_status == 51)
				{	
					if($RNRreason=='Cutting call Hearing GoBumpr'){
						$feedback_status = -2;
						$followup_date = date('Y-m-d H:i:s',strtotime("+90 days"));
					}else{
						$feedback_status = -2;
						$followup_date = date('Y-m-d H:i:s',strtotime("+90 days")); //3months from now. this date can be used for re-eng date when re-eng panel is ready.  
					}
					
				}
				$sql = "UPDATE tyre_booking_tb SET feedback_followup='$followup_date',feedback_status='$feedback_status',service_status_reason='$RNRreason-$feedback_status',crm_feedback_id='$crm_log_id' WHERE booking_id='$booking_id'";
				//echo $sql;

				$sql_feedback = "UPDATE feedback_track_tyres SET feedback_followup='$followup_date',feedback_status='$feedback_status',service_status_reason='$RNRreason-$feedback_status',crm_log_id='$crm_log_id',crm_update_time='$today' WHERE b2b_booking_id='$b2b_booking_id' AND flag=0";
				
				// echo $sql_feedback;
			 //die;
				$category1 = $booking_id.' - '.$status.' - '.$RNRreason;
				$query1="INSERT INTO tyres_comments_tbl(user_id,crm_log_id,category,Follow_up_date,flag,log,status,book_id) VALUES ('$user_id','$crm_log_id','$category1','$followup_date','0','$today','FeedBack','$booking_id')";

				$res1 = mysqli_query($conn1,$query1) or die(mysqli_error($conn1));
				
				$user_query = "SELECT name,mobile_number FROM user_register WHERE reg_id = '$user_id'";
				$user_res = mysqli_query($conn1,$user_query) or die(mysqli_error($conn1));
				$user_row = mysqli_fetch_object($user_res);
				
				$user_name = $user_row->name;
				$to = $user_row->mobile_number;
				$support = $crm_name;
				switch($crm_log_id)
				{
					case 'crm085' : $contact = '6385171252'; //Pandeeswari
									break;
					case 'crm087' : $contact = '7825830639'; //Mukesh
									break;
					case 'crm208' : $contact = '9384034575'; //Nagenthran
									break;
					case 'crm114' : $contact = '9384034576'; //Dharma
									break;
					case 'crm125' : $contact = '7825830640'; //Naveen
									break;
					case 'crm128' : $contact = '7825830563'; //Avenjalo
									break;
					case 'crm223' : $contact = '9840893518'; //Shaikh
									break;
					case 'crm147' : $contact = '7825830638'; //Shravan
									break;
					case 'crm185' : $contact = '7825830562'; //Karthick
									break;
				
					default : $contact = '9003251754';
				}
				$sms = 	$sms = "Hi ".$user_name.". This is ".$support." from GoBumpr. We tried reaching out to you for your service feedback but couldn't connect with you. For any service related queries for your bike or car, you can Call/SMS us at ".$contact." or use our app - gobumpr.com/app";
				$support = str_replace(' ', '%20', $support);
				$to = str_replace(' ', '%20', $to);
				$user_name = str_replace(' ', '%20', $user_name);
				$sms = str_replace(' ', '%20', $sms);
				$ch=curl_init("http://203.212.70.200/smpp/sendsms?username=northautohttp&password=north123&to=%22".$to."%22&from=GOBMPR&udh=&text=".$sms."");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$result=curl_exec($ch);
				curl_close($ch);
				break;

	case 'Purchased':	
						
								$completed_flag=1;
								$followup_date = date('Y-m-d H:i:s',strtotime("+30 days")); //1months from now. this date can be used for re-eng date when re-eng panel is ready.
								$feedback_status = 1;
								$sql = "UPDATE tyre_booking_tb SET service_status='$status',final_bill_amt='$final_bill_amount',rating='$rating',feedback='$comments',get_rltime_updte='$rtt_updates',feedback_followup='$followup_date',feedback_status='$feedback_status',crm_feedback_id='$crm_log_id' WHERE booking_id='$booking_id'";
								$sql_feedback = "UPDATE feedback_track_tyres SET service_status='Purchased',final_bill_amt='$final_bill_amount',rating='$rating',comments='$comments',feedback_followup='$followup_date',feedback_status='$feedback_status',crm_log_id='$crm_log_id',crm_update_time='$today',reservice_flag=0 WHERE b2b_booking_id='$b2b_booking_id' AND flag=0";
								$category1 = $booking_id.' - '.$status;
								$query1="INSERT INTO tyres_comments_tbl(user_id,crm_log_id,category,comments,Follow_up_date,flag,log,status,book_id) VALUES ('$user_id','$crm_log_id','$category1','$comments','$followup_date','0','$today','FeedBack','$booking_id')";
								$res1 = mysqli_query($conn1,$query1) or die(mysqli_error($conn1));
							
		 
					  break;
	case 'Yet to Purchase':    $feedback_status = 2;
											// $shop_id = '1670';
										   if($subreason=='Delay in Pickup'){
											$send_notification="http://axle.gobumpr.com/b2b/gbpartnerfcm_feedback_warn.php?b2b_shop_id='$shop_id'&booking_id='$b2b_booking_id'&stage=2";
											$ch=curl_init($send_notification);
											curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
											$result=curl_exec($ch);
											curl_close($ch);
									 		}else if($subreason=='No Call received'){
											 $send_notification="http://axle.gobumpr.com/b2b/gbpartnerfcm_feedback_warn.php?b2b_shop_id='$shop_id'&booking_id='$b2b_booking_id'&stage=1";
											 $ch=curl_init($send_notification);
											 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
											 $result=curl_exec($ch);
											 curl_close($ch);
									 		}
									   		$category1 = $booking_id.' - '.$status;
											$reason=$reason.' - '.$subreason;
											if($subreason=='Busy/Held up'){
												if($pickup=='Will visit the garage' || $pickup=='Needs pick up'){
													$reason=$reason.' - '.$pickup;
												}
											}
											if(!empty($comments)){
												$reason=$reason.' - '.$comments;
											}
									   $query1="INSERT INTO tyres_comments_tbl(user_id,crm_log_id,category,comments,Follow_up_date,flag,log,status,book_id) VALUES ('$user_id','$crm_log_id','$category1','$reason','$followup_date','0','$today','FeedBack','$booking_id')";
									   $res1 = mysqli_query($conn1,$query1) or die(mysqli_error($conn1));
									   $sql = "UPDATE tyre_booking_tb SET service_status='Yet to Purchase',service_status_reason = '$reason',feedback_followup='$followup_date',feedback_status='$feedback_status',crm_feedback_id='$crm_log_id' WHERE booking_id='$booking_id'";
									   $sql_feedback = "UPDATE feedback_track_tyres SET service_status='Yet to Purchase',service_status_reason = '$reason',feedback_followup='$followup_date',feedback_status='$feedback_status',crm_log_id='$crm_log_id',crm_update_time='$today',svc_priority='$svcPriority' WHERE b2b_booking_id='$b2b_booking_id' AND flag=0";
									   break;
	
	case 'Cancelled'				:  $followup_date = $followup_date;
									   $feedback_status = 5;
									   $category1 = $booking_id.' - '.$status;
									   if(!empty($subreason)){
										$cancelledReason=$cancelledReason.' - '.$subreason;
									   }
									   $cancelledReason=$cancelledReason.' - '.$comments;
									   $query1="INSERT INTO tyres_comments_tbl(user_id,crm_log_id,category,comments,Follow_up_date,flag,log,status,book_id) VALUES ('$user_id','$crm_log_id','$category1','$cancelledReason','$followup_date','0','$today','FeedBack','$booking_id')";
									   $res1 = mysqli_query($conn1,$query1) or die(mysqli_error($conn1));
									   $sql = "UPDATE tyre_booking_tb SET service_status='Cancelled',service_status_reason = '$cancelledReason',feedback_followup='$followup_date',feedback_status='$feedback_status',crm_feedback_id='$crm_log_id' WHERE booking_id='$booking_id'";
									   $sql_feedback = "UPDATE feedback_track_tyres SET service_status='Cancelled',service_status_reason = '$cancelledReason',feedback_followup='$followup_date',feedback_status='$feedback_status',crm_log_id='$crm_log_id',crm_update_time='$today',gb_quoted_amt='$gbQuoteAmt',garage_quoted_amt='$garageQuoteAmt',local_garage_quote_amt='$localGarageQuoteAmt' WHERE b2b_booking_id='$b2b_booking_id' AND flag=0";
									   break;
}
// echo $sql;
$res = mysqli_query($conn1,$sql) or die(mysqli_error($conn1));
$res_feedback = mysqli_query($conn1,$sql_feedback) or die(mysqli_error($conn1));

if($status == 'Purchased' && $completed_flag==1){
//get details from gobumpr
$sql_book_gb = "SELECT user_id,vech_id,mec_id,service_type,city,vehicle_type FROM tyre_booking_tb WHERE booking_id='$booking_id'";
$res_book_gb = mysqli_query($conn1,$sql_book_gb) or die(mysqli_error($conn1));
$row_book_gb = mysqli_fetch_object($res_book_gb);

$user_id = $row_book_gb->user_id;
$vech_id = $row_book_gb->vech_id;
$mec_id = $row_book_gb->mec_id;
$service_type = $row_book_gb->service_type;
$vehicle_type = $row_book_gb->vehicle_type;
$city = $row_book_gb->city;

//get details from b2b
$sql_book_b2b = "SELECT b2b_shop_id,b2b_customer_name,b2b_cust_phone,brand,model,b2b_credit_amt,b2b_check_in_report,b2b_vehicle_at_garage,b2b_vehicle_ready,b2b_bill_amount FROM b2b_booking_tbl_tyres WHERE b2b_booking_id='$b2b_booking_id'";
$res_book_b2b = mysqli_query($conn2,$sql_book_b2b) or die(mysqli_error($conn2));
$row_book_b2b = mysqli_fetch_object($res_book_b2b);

$b2b_shop_id = $row_book_b2b->b2b_shop_id;
$user_name = $row_book_b2b->b2b_customer_name;
$user_mobile = $row_book_b2b->b2b_cust_phone;
$brand = $row_book_b2b->brand;
$model = $row_book_b2b->model;
$b2b_check_in_report = $row_book_b2b->b2b_check_in_report;
$b2b_vehicle_at_garage = $row_book_b2b->b2b_vehicle_at_garage;
$b2b_vehicle_ready = $row_book_b2b->b2b_vehicle_ready;
$b2b_bill_amount = $row_book_b2b->b2b_bill_amount;
$b2b_credit_amt = $row_book_b2b->b2b_credit_amt;
	

//insert or update user rating table in gobumpr
$sql_check_gb = "SELECT rating,feedback FROM user_rating_tbl WHERE axle_booking_id='$b2b_booking_id'";
$res_check_gb = mysqli_query($conn1,$sql_check_gb) or die(mysqli_error($conn1));
$num_rows = mysqli_num_rows($res_check_gb);
if($num_rows <= 0){
    $sql_rat_gb = "INSERT INTO user_rating_tbl (booking_id,mec_id,service_type,rating,log,source,axle_booking_id,name,brand,model,vech_id,user_id,status) VALUES('$booking_id','$mec_id','$service_type','$rating','$today','Feedback Call','$b2b_booking_id','$user_name','$brand','$model','$vech_id','$user_id','1') ";
	$sql_b2b = "UPDATE b2b_booking_tbl_tyres SET b2b_Rating='$rating' WHERE b2b_booking_id='$b2b_booking_id'";
	$res_b2b = mysqli_query($conn2,$sql_b2b) or die(mysqli_error($conn2));
} 
else{
	$rows = mysqli_fetch_object($res_check_gb);
	$cust_rating = $rows->rating;
	$cust_feedback = $rows->feedback;
    $sql_rat_gb = "UPDATE user_rating_tbl SET booking_id='$booking_id',rating='$cust_rating',log='$today',status='1',vech_id='$vech_id',user_id='$user_id' WHERE axle_booking_id='$b2b_booking_id' ";
	$sql_b2b = "UPDATE b2b_booking_tbl_tyres SET b2b_Rating='$cust_rating',b2b_Feedback='$cust_feedback' WHERE b2b_booking_id='$b2b_booking_id'";
	$res_b2b = mysqli_query($conn2,$sql_b2b) or die(mysqli_error($conn2));
}
//$sql_rat_gb;
$res_rat_gb = mysqli_query($conn1,$sql_rat_gb) or die(mysqli_error($conn1));
$sql_feedback_id = "SELECT rating_id FROM user_rating_tbl WHERE axle_booking_id='$b2b_booking_id' ";
$res_feedback_id = mysqli_query($conn1,$sql_feedback_id) or die(mysqli_error($conn1));
$row_feedback_id = mysqli_fetch_object($res_feedback_id);

$feedback_id = $row_feedback_id->rating_id;
// sendEmail($feedback_id,'yes',$enc_date,'bridge',$sdt,$edt);
/******************** php start ***************************/
//$feedback_id =mysqli_real_escape_string($con, $_POST['feedback_id']);
//$booking_id=$feedback_id;
//echo $booking_id;

 			
           //  $user_id=$row->user_id;
	           //$vech_id=$row->vech_id;
            


/********************* PHP End ****************************************/


}
$dt_dec = base64_decode($dt);
$pos = strpos($dt_dec,':');
$st = substr($dt_dec,0,$pos);
$ed = substr($dt_dec,$pos+1);
switch(base64_decode($page))
{
	case 'c' : if($feedback_admin == 1)
				{
					$redirect = 'afdbkcompleted.php';
				}
			   else
			   {
				   $redirect = 'fdbkcompleted.php';
			   }
			   break;

	case 'f' : if($feedback_admin == 1)
				{
					$redirect = 'afdbkfollowups.php?st='.$st.'&ed='.$ed;
				}
			   else
			   {
				   $redirect = 'fdbkfollowups.php?st='.$st.'&ed='.$ed;
			   }
			   break;
	case 'r' : if($feedback_admin == 1)
			   {
				   $redirect = 'afdbkreservice.php?st='.$st.'&ed='.$ed;
			   }
			  else
			  {
				  $redirect = 'fdbkreservice.php?st='.$st.'&ed='.$ed;
			  }
			  break;
	case 'g' : if($feedback_admin == 1)
				{
					$redirect = 'afdbkgoaxles.php?st='.$st.'&ed='.$ed;
				}
			   else
			   {
				   $redirect = 'fdbkgoaxles.php?st='.$st.'&ed='.$ed;
			   }
			   break;
	default : if($feedback_admin == 1)
				{
					$redirect = 'afdbkhome.php';
				}
			   else
			   {
				   $redirect = 'fdbkhome.php';
			   }
			   break;
}
/* $s_date = base64_encode($sdt);
$e_date = base64_encode($edt);
$redirect = 'feedback_panel.php?sdt='.$s_date.'&edt='.$e_date.'&tab='.base64_encode($tab); */
// echo "Lock".$lock_reason;
header('location:'.$redirect);
?>
<?php
 include("sidebar.php");

$conn = db_connect1();
// login or not
 if((empty($_SESSION['crm_log_id']))) {
  if ($_SESSION['crm_log_id']!='crm146' && $_SESSION['crm_log_id']!='crm195' && $_SESSION['crm_log_id']!='crm182' && $_SESSION['crm_log_id']!='crm201' && $_SESSION['crm_log_id']!='crm202') {
 	header('location:logout.php');
 	die();
 }
 }
 date_default_timezone_set('Asia/Kolkata');
$today = date('Y-m-d');
$crm_log_id = $_SESSION['crm_log_id'];
$_SESSION['flag']=$flag;
$booking_id = base64_decode($_GET['bi']);
$axleflag = base64_decode($_GET['fid']);
$type = base64_decode($_GET['t']);

$page_booking_id = $booking_id;


if($booking_id == ''){
  header('location:somethingwentwrong.php');
	die();
}
?>

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
  <title>GoBumpr Bridge</title>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.0.1/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" crossorigin="anonymous">

  


<!-- auto complete -->
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/jquery-ui.structure.min.css">
<link rel="stylesheet" href="css/jquery-ui.theme.min.css">
<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" rel="stylesheet">

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js.map"></script>

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>
<!-- date time picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.0.1/js/bootstrap-switch.js"></script>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="css/bootstrap-datetimepicker-standalone.css">
<style>
span.make-switch.switch-radio {
    float: left;
}
.bootstrap-switch-container
{
	height:30px;
}
</style>
<style>
	.multiselect {
 	max-width: 190pt;
 	background-color: initial;
  border: 1px solid #ced4da;
 	
}
.multiselect .caret {
  position: absolute;
  right: 5px;
  line-height: 34px;
  display: inline-block;
  top: 50%;
  margin-top: -3px;
}
.types {
    color: #000000 !important ;
    background-color: #c2c2a3 !important;
    text-align: center;
    font-weight: bold;
}
.checkbox {
	color: black;
}
</style>
  <style>
  
			
			.socket{
				display:none;
				width: 57%;
				height: 57%;
				position: absolute;
				left: 37%;
				margin-left: -17%;
				top: 33%;
				margin-top: -17%;
			}
			
			.hex-brick{
			  background: #FFA800;
			  width: 30px;
			  height: 17px;
			  position: absolute;
			  top: 5px;
        animation-name: fade;
				animation-duration: 2s;
				animation-iteration-count: infinite;
				-webkit-animation-name: fade;
				-webkit-animation-duration: 2s;
				-webkit-animation-iteration-count: infinite;
			}
			
			.h2{
				transform: rotate(60deg);
				-webkit-transform: rotate(60deg);
			}
			
			.h3{
				transform: rotate(-60deg);
				-webkit-transform: rotate(-60deg);
			}
			
			.gel{
				height: 30px;
				width: 30px;	
				transition: all .3s;
				-webkit-transition: all .3s;
				position: absolute;
        top: 50%;
        left: 50%;
			}
			
			.center-gel{
				margin-left: -15px;
				margin-top: -15px;
				
				animation-name: pulse;
				animation-duration: 2s;
				animation-iteration-count: infinite;
				-webkit-animation-name: pulse;
				-webkit-animation-duration: 2s;
				-webkit-animation-iteration-count: infinite;
			}
			
			.c1{
				margin-left: -47px;
				margin-top: -15px;
			}
			
			.c2{
				margin-left: -31px;
				margin-top: -43px;
			}
			
			.c3{
				margin-left: 1px;
				margin-top: -43px;
			}
			
			.c4{
				margin-left: 17px;
				margin-top: -15px;
			}
			.c5{
				margin-left: -31px;
				margin-top: 13px;
			}
			
			.c6{
				margin-left: 1px;
				margin-top: 13px;
			}
			
			.c7{
				margin-left: -63px;
				margin-top: -43px;
			}
			
			.c8{
				margin-left: 33px;
				margin-top: -43px;
			}
			
			.c9{
				margin-left: -15px;
				margin-top: 41px;
			}
			
			.c10{
				margin-left: -63px;
				margin-top: 13px;
			}
			
			.c11{
				margin-left: 33px;
				margin-top: 13px;
			}
			
			.c12{
				margin-left: -15px;
				margin-top: -71px;
			}
			
			.c13{
				margin-left: -47px;
				margin-top: -71px;
			}
			
			.c14{
				margin-left: 17px;
				margin-top: -71px;
			}
			
			.c15{
				margin-left: -47px;
				margin-top: 41px;
			}
			
			.c16{
				margin-left: 17px;
				margin-top: 41px;
			}
			

			.c17{
				margin-left: -79px;
				margin-top: -15px;
			}
			
			.c18{
				margin-left: 49px;
				margin-top: -15px;
			}
			
			.c19{
				margin-left: -63px;
				margin-top: -99px;
			}
			
			.c20{
				margin-left: 33px;
				margin-top: -99px;
			}
			
			.c21{
				margin-left: 1px;
				margin-top: -99px;
			}
			
			.c22{
				margin-left: -31px;
				margin-top: -99px;
			}
			
			.c23{
				margin-left: -63px;
				margin-top: 69px;
			}
			
			.c24{
				margin-left: 33px;
				margin-top: 69px;
			}
			
			.c25{
				margin-left: 1px;
				margin-top: 69px;
			}
			
			.c26{
				margin-left: -31px;
				margin-top: 69px;
			}
			
			.c27{
				margin-left: -79px;
				margin-top: -15px;
			}
			
			.c28{
				margin-left: -95px;
				margin-top: -43px;
			}
			
			.c29{
				margin-left: -95px;
				margin-top: 13px;
			}
			
			.c30{
				margin-left: 49px;
				margin-top: 41px;
			}
			
			.c31{
				margin-left: -79px;
				margin-top: -71px;
			}
			
			.c32{
				margin-left: -111px;
				margin-top: -15px;
			}
			
			.c33{
				margin-left: 65px;
				margin-top: -43px;
			}
			
			.c34{
				margin-left: 65px;
				margin-top: 13px;
			}
			
			.c35{
				margin-left: -79px;
				margin-top: 41px;
			}
			
			.c36{
				margin-left: 49px;
				margin-top: -71px;
			}
			
			.c37{
				margin-left: 81px;
				margin-top: -15px;
			}
			
			.r1{
				animation-name: pulse;
				animation-duration: 2s;
				animation-iteration-count: infinite;
				animation-delay: .2s;
				-webkit-animation-name: pulse;
				-webkit-animation-duration: 2s;
				-webkit-animation-iteration-count: infinite;
				-webkit-animation-delay: .2s;
			}
			
			.r2{
				animation-name: pulse;
				animation-duration: 2s;
				animation-iteration-count: infinite;
				animation-delay: .4s;
				-webkit-animation-name: pulse;
				-webkit-animation-duration: 2s;
				-webkit-animation-iteration-count: infinite;
				-webkit-animation-delay: .4s;
			}
			
			.r3{
				animation-name: pulse;
				animation-duration: 2s;
				animation-iteration-count: infinite;
				animation-delay: .6s;
				-webkit-animation-name: pulse;
				-webkit-animation-duration: 2s;
				-webkit-animation-iteration-count: infinite;
				-webkit-animation-delay: .6s;
			}
			
			.r1 > .hex-brick{
				animation-name: fade;
				animation-duration: 2s;
				animation-iteration-count: infinite;
				animation-delay: .2s;
				-webkit-animation-name: fade;
				-webkit-animation-duration: 2s;
				-webkit-animation-iteration-count: infinite;
				-webkit-animation-delay: .2s;
			}
			
			.r2 > .hex-brick{
				animation-name: fade;
				animation-duration: 2s;
				animation-iteration-count: infinite;
				animation-delay: .4s;
				-webkit-animation-name: fade;
				-webkit-animation-duration: 2s;
				-webkit-animation-iteration-count: infinite;
				-webkit-animation-delay: .4s;
			}
			
			.r3 > .hex-brick{
				animation-name: fade;
				animation-duration: 2s;
				animation-iteration-count: infinite;
				animation-delay: .6s;
				-webkit-animation-name: fade;
				-webkit-animation-duration: 2s;
				-webkit-animation-iteration-count: infinite;
				-webkit-animation-delay: .6s;
			}
			
			
			@keyframes pulse{
				0%{
					-webkit-transform: scale(1);
					transform: scale(1);
				}
				
				50%{
					-webkit-transform: scale(0.01);
					transform: scale(0.01);
				}
				
				100%{
					-webkit-transform: scale(1);
					transform: scale(1);
				}
			}
			
			@keyframes fade{
				0%{
					background: #FFA800;
				}
				
				50%{
					background: #f9cc70;
				}
				
				100%{
					background: #FFA800;
				}
			}
			
			@-webkit-keyframes pulse{
				0%{
					-webkit-transform: scale(1);
					transform: scale(1);
				}
				
				50%{
					-webkit-transform: scale(0.01);
					transform: scale(0.01);
				}
				
				100%{
					-webkit-transform: scale(1);
					transform: scale(1);
				}
			}
			
			@-webkit-keyframes fade{
				0%{
					background: #FFA800;
				}
				
				50%{
					background: #ffc759;
				}
				
				100%{
					background: #FFA800;
				}
			}
  /*.loader_overlay{
	display:none;
    height: 100%;
    width: 100%;
    margin-top: -113%;
    background: rgba(221, 221, 221,15);
    opacity: 0.5;
  }
  .loader {
	display:none;
	margin-left: 40%;
    position: fixed;
    margin-top: -65%;
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 90px;
    height: 90px;
    animation: spin 0.5s linear infinite;
	z-index:9999;
}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}*/



  nav,ol{
	  background: #009688 !important;
	  font-size:18px;
	   margin-top: -4px;
  }
   .navbar-fixed-top {
    top: 0;
    border-width: 0 0 0px;
  }
  body{
	  background: #fff !important;
	  color:black;
  }
	<!-- auto complete -->
	@charset "utf-8";.ui-autocomplete{z-index:1000 !important;cursor:default;list-style:none;}
	.ui-widget{}
	.ui-autocomplete{overflow-y:auto;overflow-x:hidden;}
	.ui-menu{width:0px;display:none;}
	.ui-autocomplete > li{padding:10px;padding-left:10px;}
	ul{margin-bottom:0;}
	.ui-autocomplete > li.ui-state-focus{background-color:#DDD;}
	.ui-autocomplete .ui-front .ui-menu .ui-widget .ui-widget-content{border:0;}
	.ui-helper-hidden-accessible{display:none;}
	.gobumpr-icon{font-style:normal;font-weight:normal;speak:none;display:inline-block;text-decoration:inherit;text-align:center;font-variant:normal;text-transform:none;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
	.ui-widget{background-color:white;width:100%;}
	.ui-widget-content{padding-left:1px;display:block;width:20px;position:relative;line-height:12px;max-height:210px;border:.5px solid #DADADA;}
	.ui-widget{}
	.ui-autocomplete { position: absolute; cursor: default;}


<!-- vehicle type -->
.bike,
.car{
  cursor: pointer;
  user-select: none;
  -webkit-user-select: none;
  -webkit-touch-callout: none;
}
.bike > input,
.car > input{ /* HIDE ORG RADIO & CHECKBOX */
  visibility: hidden;
  position: absolute;
}
/* RADIO & CHECKBOX STYLES */
.bike > i,
.car > i{     /* DEFAULT <i> STYLE */
  display: inline-block;
  vertical-align: middle;
  width:  16px;
  height: 16px;
  border-radius: 50%;
  transition: 0.2s;
  box-shadow: inset 0 0 0 8px #fff;
  border: 1px solid gray;
  background: gray;
}

label > input{ /* HIDE RADIO */
  visibility: hidden; /* Makes input not-clickable */
  position: absolute; /* Remove input from document flow */
}
label > input + img{ /* IMAGE STYLES */
  cursor:pointer;
  border:2px solid transparent;
}
label > input:checked + img { /* (RADIO CHECKED) IMAGE STYLES */
   border-radius:12px;
   padding:5px;
   background-color:#ffa800;
  box-shadow: 0 0 3px 0 #394;
}
.borderless td, .borderless th {
    border: none !important;
}
	/* anchor tags */
  a{
	  text-decoration:none;
	  color:black;
  }
   a:hover{
	  text-decoration:none;
	  color:#4B436A;
  }
    .datepicker {
	  cursor:pointer;
  }
.datepicker:before {
content: '';
display: inline-block;
border-left: 7px solid transparent;
border-right: 7px solid transparent;
border-bottom: 7px solid #ccc;
border-bottom-color: transparent !important;
position: absolute;
top: -7px;
left: 190px;  // I made a change here 
}

.datepicker:after {
content: '';
display: inline-block;
border-left: 6px solid transparent;
border-right: 6px solid transparent;
border-top-color: transparent !important;
border-top: 6px solid #ffffff;
position: absolute;
bottom: -6px;
left: 191px;  // I made a change here 
}

#datepick > span:hover{cursor: pointer;}

 .floating-box {
	 display: inline-block;
	 margin: 22px;
 padding: 22px;
 width:203px;
 height:105px;
 box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  font-size: 17px;
}

/* vertical menu */
/* Mixin */
@mixin vertical-align($position: relative) {
  position: $position;
  top: 50%;
  -webkit-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}

.ver_menu {
  @include vertical-align();
}

select{
    color: red;
}
option{
  height: 25px;
}
option:hover {
  box-shadow: 0 0 10px 100px #ddd inset;
}
</style>

</head>
<body>
 <?php include_once("header.php"); ?> 
<script>
$(document).ready(function(){
  $('#city').hide();
})
</script>
 <div class="overlay" data-sidebar-overlay></div>
 <div class="navbar-fixed-top" style=" margin-top:62px;border-top:52px;padding-top:5px;background-color:#fff;z-index:1037;">

<p class="col-lg-offset-1 col-sm-offset-1" style="background-color:#ffa800;padding:7px;width:30px;border-radius:18px;float:left;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p><p style="width:150px;padding:6px;float:left;">New Lead</p>
<p style="background-color:#69ED85;padding:7px;width:30px;border-radius:18px;float:left;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p><p style="width:150px;padding:6px;float:left;">Lead sent to garage</p>
<p style="background-color:#D25F34;padding:7px;width:30px;border-radius:18px;float:left;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p><p style="width:150px;padding:6px;float:left;">Bid Placed</p>
<p style="background-color:#8C7272;padding:7px;width:30px;border-radius:18px;float:left;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p><p style="width:150px;padding:6px;float:left;">Bid Placed By All</p>
<p style="background-color:#6FA3DE;padding:7px;width:30px;border-radius:18px;float:left;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p><p style="width:150px;padding:6px;float:left;">Bid Accepted</p>


</div>

<div class="padding"></div>
  <?php
    // get user id  from user_booking_table
       $sql_user_bk = "SELECT tb.user_id,tb.locality,u.name,u.mobile_number,u.mobile_number2,u.email_id,tb.user_vech_no,tb.tyre_brand,v.brand,v.model from tyre_booking_tb as tb  LEFT JOIN user_register as u ON u.reg_id=tb.user_id LEFT JOIN user_vehicle_table as v ON tb.user_veh_id=v.id Where tb.booking_id='$page_booking_id' group by tb.booking_id ";
      $res_user_bk = mysqli_query($conn,$sql_user_bk);
      $row_user_bk = mysqli_fetch_object($res_user_bk);

      $user_id = $row_user_bk->user_id;
      $locality=$row_user_bk->locality;
      $name = $row_user_bk->name;
      $mobile_number = $row_user_bk->mobile_number;
      $mobile_number2 = $row_user_bk->mobile_number2;
	  $email = $row_user_bk->email_id;
	  $vehicle_no = $row_user_bk->user_vehicle_no;
	  $tyre_brand = $row_user_bk->tyre_brand;
	  $brand = $row_user_bk->brand;
	  $model = $row_user_bk->model;
	  

  ?>
  <div  id="user" style=" margin-left:20px;border:2px solid #708090; border-radius:8px; width:25%;height:470px; padding:10px; margin-top:60px; float:left;">
    <table id="table1" class="table borderless">
       <tr><td><strong>User Id</strong></td><td><?php echo $user_id; ?></td></tr>
       <tr><td><strong>Name</strong></td><td><?php echo $name; ?></td></tr>
       <tr><td><strong>Phn No</strong></td><td><?php echo $mobile_number; ?></td></tr>
       <tr><td><strong>Alt Phn No</strong></td><td><?php echo $mobile_number2; ?></td></tr>
       <tr><td><strong>E-mail</strong></td><td><?php echo $email; ?></td></tr>
       <tr><td><strong>Veh No</strong></td> <td><?php echo $vehicle_no; ?></td></tr>
       <tr><td><strong>Tyre Brand</strong></td> <td><?php echo $tyre_brand; ?></td></tr>
        <tr><td><strong>Brand</strong></td> <td><?php echo $brand; ?>&nbsp<?php echo $model; ?></td></tr>
       <tr><td><strong>Current Location<strong></td><td><?php echo $locality; ?></td></tr>
  </table>
<!-- Edit User -->
<div  style="float:left; margin-left:30px;">
   <div id="edting_user" style="display:inline-block; align-items:center;" >
       <!-- Trigger the modal with a button -->
       <button id="eu" type="button" class="btn btn-sm" data-toggle="modal" data-target="#myModal_user" style="background-color:rgb(156, 197, 202);"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;Edit User</button>

       <!-- Modal -->
       <div class="modal fade" id="myModal_user" role="dialog" >
         <div class="modal-dialog" style="width:860px;">

          <!-- Modal content-->
          <div class="modal-content">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Edit User</h3>
             </div>
             <div class="modal-body">
								<?php
								$sql_edit_user ="SELECT name,mobile_number,mobile_number2,email_id,City,Locality_Home,Locality_Work,source,campaign,Last_service_date,Next_service_date,Last_called_on,Follow_up_date,comments FROM user_register WHERE reg_id='$user_id'";
								$res_edit_user = mysqli_query($conn, $sql_edit_user);
								$row_edit_user = mysqli_fetch_object($res_edit_user);
								$u_name = $row_edit_user->name;
								$u_mobile = $row_edit_user->mobile_number;
								$u_alt_mobile = $row_edit_user->mobile_number2;
								$u_mail  = $row_edit_user->email_id;
								$u_city  = $row_edit_user->City;
								$u_loc_home  = $row_edit_user->Locality_Home;
								$u_loc_work  = $row_edit_user->Locality_Work;
								$u_source = $row_edit_user->source;
								$u_campaign = $row_edit_user->campaign;
								$u_last_serviced  = $row_edit_user->Last_service_date;
								$u_next_serviced  = $row_edit_user->Next_service_date;
								$u_last_called  = $row_edit_user->Last_called_on;
								$u_followup  = $row_edit_user->Follow_up_date;
								$u_comments  = $row_edit_user->comments;
								?>
								<form id="edit_user" class="form" method="post" action="edit_user.php">
                  <div class="row">
                       <br>
                       <div class="col-xs-4 col-xs-offset-1 form-group">
                         <input class="form-control" type="text" id="mobile" name="mobile" placeholder="Mobile" readonly maxlength="10" value="<?php echo $u_mobile; ?>">
                       </div>
					   
					   <div class="col-xs-4 form-group">
						 <input class="form-control" type="text" id="mobile2" name="mobile2" placeholder="Alternate Mobile" maxlength="10" value="<?php echo $u_alt_mobile; ?>">
						 </div>
						 </div>
						 
						 <div class="row">

                      <div class="col-xs-5 col-xs-offset-1 form-group">
                          <input  class="form-control" type="text" id="user_name" name="user_name"  pattern="^[a-zA-Z0-9\s]+$" onchange="try{setCustomValidity('')}catch(e){}" placeholder="User Name" required  value="<?php echo $u_name; ?>">
                      </div>

                      <div class="col-xs-5 form-group">
                         <input type="email" class="form-control" id="email" name="email" placeholder="E-Mail"  value="<?php echo $u_mail; ?>">
                      </div>
                  </div>
                  <div class="row"></div>
                  <div class="row">
                      <div id="city_u" class="col-xs-4 col-xs-offset-1 form-group">
			                    <div class="ui-widget">
                             <select class="form-control" id="cityu" name="city" required>
                             <?php

                              $sql_city = "SELECT DISTINCT city FROM go_bumpr.localities ORDER BY city ASC";
                              $res_city = mysqli_query($conn,$sql_city);
                              while($row_city = mysqli_fetch_object($res_city)){
                                  ?>
                                  <option value="<?php echo $row_city->city; ?>"><?php echo $row_city->city; ?></option>
                                  <?php
                              }
                             ?>

                             </select>
							                 <script>
                               $(document).ready(function(){
                                 var city = '<?php echo $u_city; ?>' ;
                                 $('#cityu option[value='+city+']').attr('selected','selected');
                               })
                               </script>
			                    </div>
                      </div>
                      <div id="loc_home" class="col-xs-3 form-group">
                        <div class="ui-widget" id="loc_home">
        	                <input class="form-control autocomplete" id="location_home" type="text" name="location_home" placeholder="Home Locality" value="<?php echo $u_loc_home; ?>">
                       </div>
                      </div>

                      <div id="loc_work" class="col-xs-3 form-group">
                          <div class="ui-widget" id="loc_work">
        	                <input class="form-control autocomplete" id="location_work" type="text" name="location_work" placeholder="Work Locality" value="<?php echo $u_loc_work; ?>">
                       </div>
                      </div>
									</div>
                  <div class="row"></div>
                  <div class="row">
                    <div id="source_u" class="col-xs-5 col-xs-offset-1 form-group">
			                  <div class="ui-widget">
                           <select class="form-control" id="source" name="source">
                           <option value="<?php echo $u_source; ?>" selected><?php echo $u_source; ?></option>
                           <?php 
      $sql_sources = "SELECT user_source FROM user_source_tbl WHERE flag='0' AND user_source !='$u_source' ORDER BY  user_source ASC";
      $res_sources = mysqli_query($conn,$sql_sources);
      while($row_sources = mysqli_fetch_object($res_sources)){
        $source_name = $row_sources->user_source;
        ?>
        <option value="<?php echo $source_name; ?>"><?php echo $source_name; ?></option>
        <?php
      }
      ?>
                           </select>
			                  </div>
                    </div>

                   <div id="cam" class="col-xs-5 form-group">
			                <div class="ui-widget">
				                  <input class="form-control" id="campaign" type="text" name="campaign"  placeholder="Campaign"   value="<?php echo $u_campaign; ?>">
			                </div>
                   </div>
							    </div>
                  <div class="row"></div>
                  <div class="row">

                 <div class="col-xs-2 col-xs-offset-1 form-group">
                    <label> Last Serviced</label></div>
                    <div class="col-xs-3  form-group">
                    <?php
                    if($u_last_serviced == $today){
                      ?>
                      <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="last_service_date" name="last_service_date"  value="<?php echo date('d-m-Y',strtotime($u_last_serviced)); ?>">
                    <?php }
                    else{
                      ?>
                        <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="last_service_date" name="last_service_date"  value="<?php echo date('d-m-Y',strtotime($u_last_serviced)); ?>" readonly>
                    <?php } ?>
                   </div>
                   <div class="col-xs-2 form-group">
                       <label> Next Service On</label>
									 </div>
                   <div class="col-xs-3  form-group">
                      <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="next_service_date" name="next_service_date"  value="<?php echo date('d-m-Y',strtotime($u_next_serviced)); ?>">
                   </div>
                  </div>
                  <div class="row"></div>
                  <div class="row">
              <div class="col-xs-2 col-xs-offset-1 form-group">
                <label> Last Called On</label>
              </div>
              <div class="col-xs-3  form-group">
              <?php
              if($u_last_called == $today){
                ?>
                <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="last_called_on" name="last_called_on"  value="<?php echo date('d-m-Y',strtotime($u_last_called)); ?>">
             <?php  }
              else{
                ?>
                 <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="last_called_on" name="last_called_on"  value="<?php echo date('d-m-Y',strtotime($u_last_called)); ?>" readonly>
              <?php } ?>
              </div>
              <div class="col-xs-2 form-group">
                 <label> FollowUp Date</label>
							</div>
              <div class="col-xs-3  form-group">
                  <input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="follow_up_date" name="follow_up_date"  value="<?php echo date('d-m-Y',strtotime($u_followup)); ?>">
              </div>
                  </div>
                  <div class="row"></div>
                  <div class="row">
           <div class="col-xs-10 col-xs-offset-1 form-group">
              <textarea class="form-control" maxlength="100" id="comments" name="comments" placeholder="Comments..." ><?php echo $u_comments; ?></textarea>
          </div>
        </div>
                  <div class="row"></div>
          <div class="row">
           <br>
           <div class="form-group" align="center">
            <input class="form-control" type="submit" id="edit_user_submit" name="edit_user_submit" value="Update" style=" width:90px; background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
          </div>
				</div>
                   <input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
									 <input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
								   <input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
                   <input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
                 </form>
  </div> <!-- modal body -->
  </div> <!-- modal content -->
  </div>  <!-- modal dailog -->
  </div>  <!-- modal -->

  </div>
</div> <!-- edit user -->
<div  style="float:left; margin-left:30px;">
<!-- Add Vehicle -->
<!-- Trigger the modal with a button -->
<div id="edting_vehicle" style="display:inline-block; align-items:center;" >
<button data-bid="<?php echo $booking_id; ?>" type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_vehicle" style="background-color:rgb(156, 197, 202);"><i class="fa fa-car" aria-hidden="true"></i>&nbsp;&nbsp;Add Vehicle</button>
</div>
</div>
</div>

<!-- table 1 -->

<div  id="box1" style="float:left;margin-top:60px; margin-left:15px; border:2px solid #4CB192; border-radius:5px;width:43%;height:470px;">
<!--<h3 style="padding-left: 10px;"> Booking Id:<?php echo $booking_id; ?></h3>-->
<div style="  width:97%; height:450px; overflow-y:auto; margin: 10px;">
  <table id="tab" class="table table-bordered table-responsive " style="border-collapse:collapse; ">
	<thead style="background-color: #D3D3D3;">
		<th>No</th>
	  <th>Activity</th> 
	 <th>Date</th>
	 <th>Support</th>
	 </thead>
	 <tbody>
	 <?php
	 $reg_array=array();


		$sql1=mysqli_query($conn,"SELECT * FROM tyre_booking_tb WHERE user_id='$user_id' ORDER by log DESC");
		while($row1=mysqli_fetch_object($sql1)){
			$bking_id = $row1->booking_id;
			if(($row1->flag==1)&&($row1->flag_unwntd==1)&&($row1->activity_status==''))
			{
				continue;
			}
			$reg_array[]=$row1;
			if($row1->axle_flag==1)
			{
        $sql6=mysqli_query($conn,"SELECT b.gb_booking_id,b.b2b_log as log,m.b2b_shop_name,b.b2b_acpt_flg,b.b2b_bid_flg,b.b2b_bid_amt FROM b2b.b2b_booking_tbl_tyres b LEFT JOIN tyre_booking_tb u ON b.gb_booking_id = u.booking_id LEFT JOIN b2b.b2b_mec_tbl as m ON b.b2b_shop_id = m.b2b_shop_id WHERE b.gb_booking_id ='$bking_id'");
				//$sql6=mysqli_query($conn,"SELECT g.go_booking_id,g.b2b_shop_id,g.sent_log as log,b.b2b_shop_name,u.service_type FROM goaxle_track g,b2b.b2b_mec_tbl b,user_booking_tb u WHERE g.b2b_shop_id = b.b2b_shop_id AND g.go_booking_id = '$bking_id' AND u.booking_id = '$bking_id'");
				while($row6=mysqli_fetch_object($sql6))
				{
					$reg_array[]=$row6;
				}
			}
		}
		$sql2=mysqli_query($conn,"SELECT * FROM user_register WHERE reg_id='$user_id' ORDER by log DESC");
		while($row2=mysqli_fetch_object($sql2)){
			$reg_array[]=$row2;
		}

		$sql3=mysqli_query($conn,"SELECT * FROM user_vehicle_table WHERE user_id='$user_id' AND type='4w' ORDER by log DESC");
		while($row3=mysqli_fetch_object($sql3)){
			$reg_array[]=$row3;
		}
		$sql4=mysqli_query($conn,"SELECT * FROM user_activity_tbl WHERE user_id='$user_id' ORDER by log DESC");
		$status_count=mysqli_num_rows($sql4);
		while($row4=mysqli_fetch_object($sql4)){
			$reg_array[]=$row4;
		}
		$sql5=mysqli_query($conn,"SELECT c.*,c.Follow_up_date AS cmnt_followup_date,b.rating as rating,b.feedback as feedback FROM tyres_comments_tbl as c LEFT JOIN tyre_booking_tb as b ON c.book_id=b.booking_id WHERE c.user_id='$user_id' ORDER by c.log DESC");
		$status_comments=mysqli_num_rows($sql5);
		while($row5=mysqli_fetch_object($sql5)){
			$reg_array[]=$row5;
		}
		function do_compare($item1, $item2) {
	    $ts1 = strtotime($item1->log);
	    $ts2 = strtotime($item2->log);
	    return $ts2 - $ts1;
	     }
	usort($reg_array, 'do_compare');

		 $arr_count = count($reg_array);

		  $x=0;
		  $axle = false;
		 for($i=0;$i<$arr_count;$i++) {
					  $x=$x+1;
	  if(isset($reg_array[$i]->gb_booking_id))
		{
      $activity = '';
      //$accept = $reg_array[$i]->b2b_acpt_flg;
      $bidplaced = $reg_array[$i]->b2b_bid_flg;
      $bidamt = $reg_array[$i]->b2b_bid_amt;
      //$deny = $reg_array[$i]->b2b_deny_flag;
      if($bidplaced == 1 && $bidamt!=0){
                $activity = $activity.'Bid Accepted by <p style="background-color:#6FA3DE;padding:7px;width:30px;border-radius:18px;float:left;margin-right:3px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
    }
    else if($bidplaced == 0 && $bidamt==0){
      $activity = $activity.'GoAxled to <p style="background-color:#69ED85;padding:7px;width:30px;border-radius:18px;float:left;margin-right:3px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';
    } 
     else if($bidplaced == 1 && $bidamt == 0){
      $activity = $activity.'Bid Placed to <p style="background-color:#D25F34;padding:7px;width:30px;border-radius:18px;float:left;margin-right:3px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></p>';

    }
    	if($bidamt == 0){
      $activity = $activity.$reg_array[$i]->b2b_shop_name." - ".$reg_array[$i]->gb_booking_id;
    }
    	else
    	{
    		$activity = $activity.$reg_array[$i]->b2b_shop_name." - ".$reg_array[$i]->gb_booking_id."<br><strong>Amt: </strong>Rs.".$bidamt;
    	}
      $support = '';
      if($reg_array[$i]->log == '0000-00-00 00:00:00')
      $log_date=date("d-m-Y h:i a");
      else
      $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
		 }
	 elseif(isset($reg_array[$i]->booking_id)){
					      	 $activity =  'BOOKING -'.$reg_array[$i]->booking_id.' -'.$reg_array[$i]->shop_name ;
							 $service_type = $reg_array[$i]->service_type;
               $support= $reg_array[$i]->crm_update_id;
			   $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
			  
					      	  }
	 elseif(isset($reg_array[$i]->reg_id)){

					      	$activity = 'USER REGISTER -'.$reg_array[$i]->reg_id.'-'.$reg_array[$i]->name.'-'.$reg_array[$i]->email_id.'-'.$reg_array[$i]->mobile_number;
							    $service_type = '';
                  $support= $reg_array[$i]->crm_log_id;
				  $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));

					      	}
	 elseif(isset($reg_array[$i]->vehicle_id)){

					      	$activity = 'VEHICLE REGISTER -'.$reg_array[$i]->brand.'-'.$reg_array[$i]->model.'-'.$reg_array[$i]->type;
							    $service_type = '';
                  $support= $reg_array[$i]->crm_log_id;
				  $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));

					      	}
	 elseif(isset($reg_array[$i]->user_activity)){
      if($status_count<=1){

            $activity = 'User Activity-'.'Prospect to '.$reg_array[$i]->user_activity;
            $service_type = '';
          $support= $reg_array[$i]->crm_log_id;
              $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
        }
            else
            {
              $activity = 'User Activity-'. $reg_array[$i]->user_activity;
              $service_type = '';
              $support= $reg_array[$i]->crm_log_id;
              $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
            }
  }
	 elseif(isset($reg_array[$i]->com_id)){
     $cmt = $reg_array[$i]->comments;
     $support= $reg_array[$i]->crm_log_id;
	 if($reg_array[$i]->cmnt_followup_date!='0000-00-00')
	 {
		$followup_on='<br><strong>Next Follow Up On</strong>- '.date("d-M-Y",strtotime($reg_array[$i]->cmnt_followup_date));
	 }
	 else
	 {
		$followup_on="";
	 }
     if($cmt == ''){
       $cmt="";
     }
	      $act_status = $reg_array[$i]->status;
        if($act_status == ""){
          $act_status = "FollowUp";
        }
        if($act_status == "Cancelled"){
            $activity = $reg_array[$i]->category;
            if($cmt !=''){
              $activity = $activity.'-'.$cmt;
            }
            $activity = $activity.$followup_on;
			      $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
        }
        else if($act_status == "Reverted"){
          	$activity = $reg_array[$i]->category.$followup_on;
			      $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
        }
        else if($act_status == "FeedBack"){
          $activity = '';
          $rating = $reg_array[$i]->rating;
          $feedback = $reg_array[$i]->feedback;
          $activity = $activity.$reg_array[$i]->category;
          if($cmt !=''){
            $activity = $activity.'<br><strong>Comments</strong>-'.$cmt;
          }
          $activity = $activity.$followup_on;
          if($rating>0){
            $activity = $activity.'<br><strong>Rating</strong>-';
            switch($rating){
              case '1':$activity = $activity.'<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i>'; break;
              case '2':$activity = $activity.'<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i>'; break;
              case '3':$activity = $activity.'<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i>'; break;
              case '4':$activity = $activity.'<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i>'; break;
              case '5':$activity = $activity.'<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i>'; break;
              case '0.5':$activity = $activity.'<i class="fa fa-star-half" aria-hidden="true" style="color:#ffa800;"></i>'; break;
              case '1.5':$activity = $activity.'<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star-half" aria-hidden="true" style="color:#ffa800;"></i>'; break;
              case '2.5':$activity = $activity.'<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star-half" aria-hidden="true" style="color:#ffa800;"></i>'; break;
              case '3.5':$activity = $activity.'<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star-half" aria-hidden="true" style="color:#ffa800;"></i>'; break;
              case '4.5':$activity = $activity.'<i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star" aria-hidden="true" style="color:#ffa800;"></i><i class="fa fa-star-half" aria-hidden="true" style="color:#ffa800;"></i>'; break;
            }
            
          }
          $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
        }
        else{
            $activity = $act_status.'-'.$reg_array[$i]->category;
            if($cmt !=''){
              $activity = $activity.'-'.$cmt;
            }
            $activity = $activity.$followup_on;
			      $log_date=date("d-m-Y h:i a",strtotime($reg_array[$i]->log));
        }
		$service_type = '';

		}
	  /*if(isset($reg_array[$i]->booking_id)){
			$bkid = $reg_array[$i]->booking_id;
      if($booking_id == $bkid){
      ?>
      <tr style="background-color:#B2DFDB;">
      <?php
      }
      else{
      ?>
      <tr>
      <?php
      }
    }*/
	//$veh_no = $activity;
	if (strlen(stristr($activity, 'BOOKING -'))>0) {
		
		$bkid=explode("-",$activity);
		$v = explode(" ",$bkid[1]);
		$bookid = $v[0];
		
		if($booking_id===$bookid)
		{
		?>
			<tr style="background-color:#B2DFDB;">
			<?php
		}
		else{
		?>
		  <tr>
		  <?php
		}
		}
		else{
		?>
		  <tr>
		  <?php
		}
	 ?>
	 <td><?php echo $x;?></td>
	 <td><?php echo $activity ; ?></td>
	 <td><?php echo $log_date;?></td>
	 <td><?php
	//echo "SELECT crm_lod_id FROM admin_comments_tbl WHERE log='$x' ";
	  $sql_crm_name = mysqli_query($conn,"SELECT name FROM crm_admin WHERE crm_log_id = '$support' ");
	  $row_sup_name= mysqli_fetch_array($sql_crm_name);
	  echo $sup_name=$row_sup_name['name']; ?></td>
	 </tr>
	 <?php
	 //sleep(1);
	flush();
    ob_flush();
		 }
		 ?>
	 </tbody>
 </table>
</div>
</div>

<!--table 2-->

<div  id="box2" style="float:left;margin-top:60px; margin-left:15px; border:2px solid #4CB192; border-radius:5px;width:28%;height:470px;">
<!--<h3 style="padding-left: 10px;">History</h3>-->
<div style="  width:94%; height:450px; overflow-y:auto; margin: 10px;overflow-x:auto;overflow:auto !important;">
  <table id="table2" class="table table-bordered table-responsive " style="border-collapse:collapse; ">
	<thead style="background-color: #D3D3D3;">
	 <th>Booking Id</th>
	  <th style="width: 200px;">Activity</th>
	
	 <th>Timestamp</th>
	 </thead>
	 <tbody>
	 <?php
	 $sql_booking = "SELECT tb.booking_id,tb.user_id,tb.log,bb.b2b_shop_id,bb.b2b_booking_id,tb.tyre_brand,tb.tyre_count,v.brand,v.model,bb.b2b_bid_amt,tb.mec_id_leads,tb.axle_id_leads,bb.b2b_bid_flg,bb.b2b_acpt_flg,tb.vech_id,sum(bb.b2b_acpt_flg) as acpt_sum,sum(bb.b2b_bid_flg) as bid_sum,count(bb.b2b_bid_flg) as bid_count FROM tyre_booking_tb as tb LEFT JOIN b2b.b2b_booking_tbl_tyres as bb ON tb.booking_id=bb.gb_booking_id LEFT JOIN user_vehicle_table as v ON tb.user_veh_id=v.id WHERE tb.user_id='$user_id' group by tb.booking_id ORDER BY tb.log DESC";
//echo $sql_booking;
$res_booking = mysqli_query($conn,$sql_booking);
$count = mysqli_num_rows($res_booking);
if($count >0){
  while($row_booking = mysqli_fetch_object($res_booking)){
    $booking_id_tbl = $row_booking->booking_id;
    $b2b_booking_id = $row_booking->b2b_booking_id;
    $log = $row_booking->log;
    $shop_id = $row_booking->b2b_shop_id;
    $tyre_brand = $row_booking->tyre_brand;
    $tyre_count = $row_booking->tyre_count;
    $brand = $row_booking->brand;
    $model = $row_booking->model;
    $b2b_booking_id = $row_booking->b2b_booking_id;       
    $bid_amt = $row_booking->b2b_bid_amt;
    $mec_id_leads = $row_booking->mec_id_leads;
    $axle_id_leads = $row_booking->axle_id_leads;
    $b2b_bid_flg = $row_booking->b2b_bid_flg;
    $b2b_acpt_flg = $row_booking->b2b_acpt_flg;
    $bid_count = $row_booking->bid_count;
    $acpt_flg_sum = $row_booking->acpt_sum;
    $bid_sum=$row_booking->bid_sum;
    $b2b_bid_log=$row_booking->b2b_bid_log;
    $vehicle_id=$row_booking->vech_id;

     $td2 = '<div class="row">';
     $td1 = '<div class="row">';
     if($axle_id_leads == '' && $mec_id_leads == ''){

               $td2 = $td2.'<a href="user_details_tyres.php?t='.base64_encode("l").'&v58i4='.base64_encode($vehicle_id).'&bi='.base64_encode($booking_id).'" style="background-color:#ffa800;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
               
                $td1= $td1.'<a href="user_details_tyres.php?bi='.base64_encode($booking_id_tbl).'"><i id="'.$booking_id_tbl.'" class="fa fa-eye" aria-hidden="true"></i>';
               }
               elseif ($b2b_booking_id == '') {
               $td2 = $td2.'<a href="tyres_goaxle.php?bi='.base64_encode($booking_id).'" style="background-color:#ffa800;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
                $td1= $td1.'<a href="user_details_tyres.php?bi='.base64_encode($booking_id_tbl).'"><i id="'.$booking_id_tbl.'" class="fa fa-eye" aria-hidden="true"></i>';
               }
               else
              {
                if($b2b_acpt_flg==1 || $acpt_flg_sum > 0){
                       $td2 = $td2.'<a style="background-color:#6FA3DE;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>'; 
                       $td1= $td1.'<a href="user_details_tyres.php?bi='.base64_encode($booking_id_tbl).'"><i id="'.$booking_id_tbl.'" class="fa fa-eye" aria-hidden="true"></i>';
                      }
                   else if($b2b_bid_flg==1 && $bid_count != $bid_sum){ 
                      $td2 = $td2.'<a  style="background-color:#D25F34;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
                      $td1= $td1.'<a href="user_details_tyres.php?bi='.base64_encode($booking_id_tbl).'"><i id="'.$booking_id_tbl.'" class="fa fa-eye" aria-hidden="true"></i>';
                      
                  }
                  else if($bid_count == $bid_sum && $acpt_sum == 0 && $bid_amt == 0){ 
                      $td2 = $td2.'<a  style="background-color:#8C7272;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
                      $td1= $td1.'<a href="user_details_tyres.php?bi='.base64_encode($booking_id_tbl).'"><i id="'.$booking_id_tbl.'" class="fa fa-eye" aria-hidden="true"></i>';
                      
                  }
                  
                  else
                  {
                   $td2 = $td2.'<a style="background-color:#69ED85;padding:6px;padding-right:8px;border-radius:15px;float:left;margin-left:12px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
                   $td1= $td1.'<a href="user_details_tyres.php?bi='.base64_encode($booking_id_tbl).'&t='.base64_encode('g').'"><i id="'.$booking_id_tbl.'" class="fa fa-eye" aria-hidden="true"></i>';
                  }
                 }
      if(empty($tyre_brand))
    {
    	$tyre_brand = 'Any brand';
    }            
	 if($booking_id_tbl == $page_booking_id){?>
	<td style="background-color:#adccef;text-align: center;"><?php echo $booking_id_tbl;?></br><?php echo $td1; ?> </td>
	<td style="background-color:#adccef;padding-left:10px;text-align: left;"><?php echo $td2; ?>   <div style="text-align:justify;padding-left: 50px;"><?php echo $tyre_brand.'*'.$tyre_count.'<br>'.$brand.' '.$model;?> </div></td>
	<td style="background-color:#adccef;"><?php echo date('d M Y h.i A', strtotime($log)); ?></td>
	<?php 
	 }
	 else
	 {
	?>
	 <td style="text-align: center;"><?php echo $booking_id_tbl;?></br> <?php echo $td1; ?> </td>
	 <td style="padding-left:10px;text-align: left;"><?php echo $td2; ?>   <div style="text-align:justify;padding-left: 50px;"><?php echo $tyre_brand.' * '.$tyre_count.'<br>'.$brand.' '.$model;?> </div></td>
	 <td><?php echo date('d M Y h.i A', strtotime($log)); ?></td>
	 <?php 
	 }
	 ?>
	 <!-- <td>Rs:<?php //echo $bid_amt; ?> --> <!-- <button type="button" class="btn btn-link edit" data-shopid="<?php //echo $shop_id?>" data-toggle="modal" href="#myModal_others"><i class="fa fa-edit"></i></button> --> <!-- </td> -->
	 <!-- <td><a href="edit-course.php?id=<?php// echo $row->id;?>"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
<a href="manage-courses.php?del=<?php //echo $row->id;?>" onclick="return confirm("Do you want to delete");"><i class="fa fa-close"></i></a></td> -->
	 </tr>
	 <?php
	}
		 }
		 ?>
	 </tbody>
 </table>
</div>
</div>


<!-- move to  others -->
<!-- Modal -->
<div class="modal fade" id="myModal_others" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Move to Others (Id : <?php echo $booking_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="add_others" class="form" method="post" action="add_others.php">
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<br>
<select class="form-control" id="others_status" name="status" required>
<option selected value="">Select Reason</option>
<?php
$sql="SELECT activity FROM tyres_activity_tbl WHERE flag='2'";
$query=mysqli_query($conn,$sql);

while ($rows = mysqli_fetch_array($query)){ ?>
<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>
<?php } ?>
</select>
</div>
</div>
<div class="row"></div>

<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<textarea class="form-control" maxlength="1000" id="comments" name="comments" style="min-height:100px;" placeholder="Comments..."></textarea>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<br>
<input class="form-control" type="submit" id="others_submit" name="others_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
</form>
</div>
</div>
</div>
</div>

<div id="cb" style="position:fixed; margin-left:24%; bottom:20px;display:none;">
<button class="btn btn-md" data-bid="<?php echo $booking_id; ?>"  id="cancel" data-toggle="modal" data-target="#myModal_cancel_booking" style="background-color:#FF7043;"><i class="fa fa-ban" aria-hidden="true"></i>&nbsp;&nbsp;Cancel Booking</button>
</div>

<!-- followup booking -->
<div id="fb" style="position:fixed; margin-left:39%; bottom:20px;display: none;"><button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_unlimited_RNR"  style="background-color:#6ed0c7;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;FollowUp</button></div>
</div>
<!-- others booking -->
 <div id="ob" style="position:fixed; margin-left:50%; bottom:20px;display: none;"><button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_others" style="background-color:#D0D854;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Others</button></div>


<!-- edit booking booking -->
<div id="edb" style="position:fixed;margin-left:60%;bottom:20px;display:none;">
<div id="ebook" sytyle="position:fixed; margin-left:62.5%; bottom:20px;display:none;"><button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_edit_booking"  style="background-color:#bfbbf1;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Edit Bookings</button></div>
</div>
 <!-- <div style="position:fixed;margin-left:81.25%;bottom:20px;">
}
<div id="addbook" sytyle="position:fixed; margin-left:81.25%; bottom:20px;display:none;"><button type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_add_booking"  style="background-color:#B5A5C3;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;New Booking</button></div>
</div>
 <div id="pbook" style="position:fixed; margin-left:55%; bottom:20px;display:none;">
  <button data-bid="<?php echo $booking_id; ?>"  type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_unlimited_RNR"  style="position:relative;background-color:#58da80;width:180px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Push To FollowUp</button>
  <button  data-bid="<?php echo $booking_id; ?>"  type="button" class="btn btn-md" data-toggle="modal" data-target="#myModal_add_booking"  style="position:relative;background-color:#39B8AC;width:180px;margin:10px; margin-bottom:10px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;Push To Bookings</button>
</div>
<! revert cancelled bookings -->
<div id="revert" style="position:fixed; margin-left:55%; bottom:20px;display:none;">
  <a href="revert_cancelled_booking.php?id=<?php echo base64_encode($booking_id); ?>&t=<?php echo base64_encode($type); ?>"><button type="button" class="btn btn-md" style="position:relative;background-color:#39B8AC;width:130px;"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i>&nbsp;Revert Back</button></a>
</div>

 <!-- -->
<!-- jQuery library -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<!-- show 3 buttons -->

<script>
$(document).ready(function(){
		var t = "<?php echo $type; ?>" ;
    switch(t){
      case 'o': 	
      $("#cb").hide();
      $("#edb").hide();
      $("#ob").hide();
      $("#revert").hide();
      $("#fb").show();
      break;
      case 'c':	
      $("#cb").hide();
      $("#edb").hide();
      $("#ob").hide();
      $("#revert").show();
      $("#fb").show();
      break;
      case 'g':
      $("#cb").hide();
      $("#edb").hide();
      $("#ob").hide();
      $("#revert").hide();
      $("#fb").hide();
      break;
      default:
      $("#cb").show();
      $("#edb").show();
      $("#ob").show();
      $("#revert").hide();
      $("#fb").show();

    }
});
</script>

<!-- add vehicle -->
<!-- Modal -->
<div class="modal fade" id="myModal_vehicle" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Add Vehicle (<?php echo $user_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="add_vehicle" class="form" method="post" action="add_vehicle.php" >
<div class="row">
  <div class="col-xs-6 col-xs-offset-5 form-group">
          
          <label class="car">
            <button id="veh" class="veh" name="veh" value="4w" disabled />
            <img id="car" src="images/car.png">
          </label>
        </div>
</div>
<div class="row"></div>
<div class="row">
<div id="b_m" class="col-xs-6 col-xs-offset-3 form-group">
			 <div class="ui-widget">
				 <input class="form-control autocomplete" id="BrandModelid" type="text" name="BrandModelid"  required placeholder="Select Model">
			 </div>
</div>
</div>

<div class="row"></div>
<div class="row">
<div class="col-xs-6 col-xs-offset-3 form-group">
<select class="form-control" id="fuel" name="fuel">
<option selected >Fuel Type </option>
<option data-imagesrc="images/bike.png" value="Diesel">Diesel</option>
<option data-imagesrc="images/car.png" value="Petrol">Petrol</option>
</select>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-6 col-xs-offset-3 form-group">
<input class="form-control" type="text" id="regno" name="regno"  data-mask-reverse="true" maxlength="13" placeholder="Vehicle No">
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-6 col-xs-offset-3 form-group">
<input class="form-control" type="text" id="year" name="year" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" placeholder="Year">
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-6 col-xs-offset-3 form-group">
<input class="form-control" type="number" id="km" min="0" name="km" placeholder="Km Driven">
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<br>
<input class="form-control" type="submit" id="vehicle_submit" name="vehicle_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
 <input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" />
 <input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" />
 <input type="hidden" id="type" name="type" value="<?php echo $type; ?>" />
 <input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" />
 </form>
</div>
</div>
</div></div>

<!-- edit booking -->
<!-- Modal -->
<div class="modal fade" id="myModal_edit_booking" role="dialog">
<div class="modal-dialog" style="width:1050px;">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Edit Booking(<?php echo $booking_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="edit_booking" class="form" method="post" action="edit_booking.php">
  <?php
$sql_book = "SELECT vech_id,user_veh_id,vehicle_type,user_vech_no,tyre_brand,mec_id,tyre_count,tyres_req_time,shop_name,pickup_full_address,estimate_amt,service_date,pick_up,pickup_address,amt,followup_date,locality,pickup_date_time,mec_id_leads,axle_id_leads FROM tyre_booking_tb WHERE booking_id = '".$booking_id."'";
//echo $sql_book;
$res_book = mysqli_query($conn,$sql_book);
$row_book = mysqli_fetch_object($res_book);
$b_veh_id=$row_book->vech_id;
$b_user_veh_id = $row_book->user_veh_id;
$b_veh = $row_book->vehicle_type;
$b_veh_no = $row_book->user_vech_no;
$tyre_brand = $row_book->tyre_brand;
$b_mec_id = $row_book->mec_id;
$tyre_count = $row_book->tyre_count;
$tyres_req_time = $row_book->tyres_req_time;
$b_shop_name = $row_book->shop_name;
//echo $b_shop_name;
$b_desc = $row_book->service_description;
$b_service_date = $row_book->service_date;
// echo $b_service_date;
// die;
$b_pickup = $row_book->pick_up;
$b_amount = $row_book->amt;
$estimate_amt = $row_book->estimate_amt;
$next_service_date = $row_book->followup_date;
$b_locality = $row_book->locality;
$pickup_location = $row_book->pickup_address;
$pickup_full_address = $row_book->pickup_full_address;
$pickup_date_time = date('d-m-Y H:i',strtotime($row_book->pickup_date_time));
$mec_id_leads = $row_book->mec_id_leads;
//echo $mec_id_leads;
$axle_id_leads = $row_book->axle_id_leads;

// get brand model
$sql_model = "SELECT id,brand,model,reg_no FROM user_vehicle_table WHERE id='".$b_user_veh_id."'" ;
$res_model = mysqli_query($conn,$sql_model);

while($row_brand = mysqli_fetch_object($res_model)){
$brand_id = $row_brand->id;
$brandmodel = $row_brand->brand." ".$row_brand->model." ".$row_brand->reg_no;
//echo $brandmodel;
}
//get shop address
	
  ?>
<div class="col-xs-10" style="margin-left: 3%;">
<div class="row" align="center">
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
<label> Vehicle Model</label> 
</div>
<div id="b_me" class="col-xs-4 form-group">
<select class="form-control" name="veh_noe">
<?php
$sql_model_veh = "SELECT id,brand,model,reg_no FROM user_vehicle_table WHERE user_id='$user_id' and type='4w'" ;
$res_model_veh = mysqli_query($conn,$sql_model_veh);

while($row_brand_veh = mysqli_fetch_object($res_model_veh)){
$new_brand_id = $row_brand_veh->id;
$newbrandmodel = $row_brand_veh->brand." ".$row_brand_veh->model." ".$row_brand_veh->reg_no;
?>
<option value="<?php echo $new_brand_id; ?>" <?php if($new_brand_id==$brand_id) echo 'selected="selected"'; ?> ><?php echo $newbrandmodel; ?></option>
//echo $brandmodel;
<?php
}
?>
 </select>
</div>
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
<label> Tyre Brand</label> 
</div>
<div class="col-xs-3 form-group" id="service">
<div class="ui-widget">
<select class="form-control" name="tyre_brand" id="tyre_brand">
<!--<option selected value="<?php echo $tyre_brand; ?>"><?php echo $tyre_brand; ?></option>-->
<option value="Any Brand" <?php if($tyre_brand=="Any Brand") echo 'selected="selected"'; ?> >Any Brand</option>
<option value="JK Tyres" <?php if($tyre_brand=="JK Tyres") echo 'selected="selected"'; ?> >JK Tyres</option>
<option value="MRF" <?php if($tyre_brand=="MRF") echo 'selected="selected"'; ?>>MRF</option>
<option value="Good Year" <?php if($tyre_brand=="Good Year") echo 'selected="selected"'; ?>>Good Year</option>
<option value="CEAT" <?php if($tyre_brand=="CEAT") echo 'selected="selected"'; ?>>CEAT</option>
<option value="Bridgestone" <?php if($tyre_brand=="Bridgestone") echo 'selected="selected"'; ?>>Bridgestone</option>
<option value="Michelin" <?php if($tyre_brand=="Michelin") echo 'selected="selected"'; ?>>Michelin</option>
<option value="Apollo" <?php if($tyre_brand=="Apollo") echo 'selected="selected"'; ?>>Apollo</option>
<option value="Yokohama" <?php if($tyre_brand=="Yokohama") echo 'selected="selected"'; ?>>Yokohama</option>
<option value="Nexen" <?php if($tyre_brand=="Nexen") echo 'selected="selected"'; ?>>Nexen</option>
 </select>
</div>
</div>
</div>
<div class="row" align="center">
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
<label> Tyre Count</label> 
</div>
<div id="b_me" class="col-xs-4 form-group">
<select class="form-control" name="tyre_count">
<!--<option selected value="<?php //echo //$tyre_count; ?>"><?php //echo //$tyre_count; ?></option>-->
<option value="1" <?php if($tyre_count=="1") echo 'selected="selected"'; ?>>1</option>
<option value="2" <?php if($tyre_count=="2") echo 'selected="selected"'; ?>>2</option>
<option value="3" <?php if($tyre_count=="3") echo 'selected="selected"'; ?>>3</option>
<option value="4" <?php if($tyre_count=="4") echo 'selected="selected"'; ?>>4</option>

 </select>
</div>
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
<label> Tyre Request Time</label> 
</div>
<div class="col-xs-3 form-group" id="service">
<div class="ui-widget">
<select class="form-control" name="tyres_req_time" id="tyres_req_times">
<!--<option selected value="<?php //echo $tyres_req_time; ?>"><?php //echo $tyres_req_time; ?></option>-->
<option value="immediately" <?php if($tyres_req_time=="immediately") echo 'selected="selected"'; ?> >Immediately</option>
<option value="in1month" <?php if($tyres_req_time=="in1month") echo 'selected="selected"'; ?> >In 1 month</option>
<option value="in3months" <?php if($tyres_req_time=="in3months") echo 'selected="selected"'; ?> >In 3 month</option>
 </select>
</div>
</div>
</div>

<div class="row" align="center">
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
<label> Customer Location</label> 
</div>
<div class="col-xs-4 form-group" id="loce">
        <div class="ui-widget">
        	<input class="form-control autocomplete" id="locatione" type="text" name="locatione" placeholder="Start typing Location..." value="<?php echo $b_locality; ?>" readonly>
        </div>
        
</div>
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
<label> PickUp Location</label> 
</div>
<div class="col-xs-3 form-group" id="locpickup">
        <div class="ui-widget">
        	<input class="form-control autocomplete" id="location_pickup" type="text" name="location_pickup" placeholder="PickUp Location..." value="<?php if($pickup_location == "" || $pickup_location == "-"){ echo $b_locality;} else { echo $pickup_location;} ?>" required>
        	
        	 <!-- <input class="form-control autocomplete" id="location_pickup" type="text" name="location_pickup" placeholder="PickUp Location..." required> --> 
        </div>
        
</div>
<div class="col-xs-1 form-group" id="go-div"><p class="btn btn-sm" id="go" style="margin-left:-8px;background-color:#B2EBF2;">Go</p>
<input type="hidden" name="pick_loc" value="">
</div>
</div>
<div class="row" align="center">
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
<label> Service Center</label> 
</div>
<div class="col-xs-4 form-group" id="mece">


<?php if($mec_id_leads == '' || $mec_id_leads == '0'){ ?>
<select class="form-control" id="mechanice" name="mechanice" required>
<option selected value="">Select Mechanic</option>
</select>
<?php } 
else{ 
	?>
<select class="form-control" id="mechanice" name="mechanice[]" multiple required="required">
	
	<?php
	if(!empty($mec_id_leads) || $mec_id_leads!=0)
          {
           $mec_id = explode(';', $mec_id_leads);
           $mec=implode(',',$mec_id);
        
       $shops = '';

         $sql_shop="SELECT mec_id,shop_name,address4 from admin_mechanic_table where mec_id in ($mec)"; 
                // echo $sql_shop; 
                $res_shop = mysqli_query($conn,$sql_shop);
              while($row_shop = mysqli_fetch_assoc($res_shop)){
               
               $shops = $row_shop['shop_name'];
               $mecid = $row_shop['mec_id'];
               $mecsid[] = $mecid; 
               //$mecs_id = implode(";",$mecsid);
               //echo $mecid;
               $shop_address = $row_shop['address4'];
               
              ?>

<option selected value="<?php echo $mecid; ?>"><?php echo $shops.' - '.$shop_address; ?></option>

<?php 
} 
}
?>
</select>
<?php
}
?>


</div>


	<input type="hidden" name="mec_id_leads" id="mec_ref">
	<input type="hidden" name="axle_id_leads" id="axle_ref">
    <input type="hidden" name="shopname_ref" id="shopname_ref">


<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
<label> Service Date</label>
</div>
<div class="col-xs-3  form-group">

<input class="form-control datepicker" data-date-format='dd-mm-yyyy' type="text" id="service_datee" name="service_datee" value="<?php if($b_service_date=='0000-00-00'){ echo date("d-m-Y",strtotime($today));} else {  echo date("d-m-Y",strtotime($b_service_date)); } ?>" required>
</div>
</div>
<div class="row" align="center">
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
<label>PickUp</label>
</div>
<div class="col-xs-4 form-group" >
<span class="make-switch switch-radio">
  <input type="radio" <?php  if($b_pickup=="yes"||$b_pickup=="1"){ echo checked;} ?> data-on-text="Yes" data-off-text="No" data-on-color="success" data-off-color="danger" name="pickcheck" class="alert-status" id="pickup_switch">
</span>
<?php
// if($b_pickup == "yes"){
  ?>
<!--  <input class="form-group" type="radio" id="pickupe" name="pickupe" value="1" checked>&nbsp;Yes&nbsp;</input>
  <input class="form-group" type="radio" id="pickupe" name="pickupe" value="0">&nbsp;No</input>-->
  <?php
// }
// else{
  ?>
  <!--<input class="form-group" type="radio" id="pickupe" name="pickupe" value="1">&nbsp;Yes&nbsp;</input>
  <input class="form-group" type="radio" id="pickupe" name="pickupe" value="0" checked>&nbsp;No</input>-->
  <?php
// } ?>
</div>
<div id="pickdate" style="display: none;">
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: right;">
<label>PickUp Date</label>
</div>
<div class="col-xs-3 form-group">		
				<div class='input-group' id='datetimepicker7'>		
					<input type='text' class="form-control" id="pickup_date" name="pickup_date"/>		
					<span class="input-group-addon">		
					<span class="glyphicon glyphicon-calendar"></span>		
					</span>		
				</div>		
			</div>		

</div>
</div>
<div class="row" align="center" id="pickaddress" style="display: none;">
<div class="col-xs-2 form-group" style="margin-bottom: 0px;padding: 0px;margin-top: 7px;text-align: left;">
<label> Pickup Address</label> 
</div>
<div class="col-xs-4 form-group" >
<textarea class="form-control" maxlength="250" id="pickup_full_addresse" name="pickup_full_addresse" placeholder="Address..." style="resize: vertical;" ><?php echo $pickup_full_address;?></textarea>
</div>
</div>

</div>
</div>
<?php
//}?>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<input class="form-control" type="submit" id="edit_booking_submit" name="edit_booking_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
<input type="hidden" id="pickupe" name="pickupe" <?php  if($b_pickup=="yes"){ echo "value='1'";}else{ echo "value='0'";} ?>>
<input type="hidden" id="pickup_alt" name="pickupe_alt" <?php  if($b_pickup=="yes" || $b_pickup=="1"){ echo "value='1'";}else{ echo "value='0'";} ?>>
</form>
<!--<div class="loader_overlay">
</div>
<div class="loader">
</div>-->
		<div class="socket loader">
			<div class="gel center-gel">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c1 r1">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c2 r1">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c3 r1">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c4 r1">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c5 r1">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c6 r1">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			
			<div class="gel c7 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			
			<div class="gel c8 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c9 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c10 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c11 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c12 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c13 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c14 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c15 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c16 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c17 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c18 r2">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c19 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c20 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c21 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c22 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c23 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c24 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c25 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c26 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c28 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c29 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c30 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c31 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c32 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c33 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c34 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c35 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c36 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			<div class="gel c37 r3">
				<div class="hex-brick h1"></div>
				<div class="hex-brick h2"></div>
				<div class="hex-brick h3"></div>
			</div>
			
		</div>
</div>

</div>
</div>
</div>

<!-- add unlimited RNR -->		
<!-- Modal -->		
<div class="modal fade" id="myModal_unlimited_RNR" role="dialog">		
<div class="modal-dialog">		
<!-- Modal content-->		
<div class="modal-content">		
 <div class="modal-header">		
    <button type="button" class="close" data-dismiss="modal">&times;</button>		
    <h3 class="modal-title">Add Follow Up (Id : <?php echo $booking_id; ?>)</h3>		
 </div>		
 <div class="modal-body">		
 <?php 
 if($type == 'c' && $type == 'o'){
   ?> 
  <form role="form" method="post" action="add_followup.php">
  <?php
 }
 else{
   ?>
    <form role="form" method="post" action="add_rnr.php">	
   <?php
 } ?>
  	
	<div class="row">		
		<div class="col-xs-8 col-xs-offset-2 form-group">		
			<br>		
			<select class="form-control" id="followup_status" name="status" required>		
				<option selected value="">Select Reason</option>		
				<?php		
				$sql="SELECT activity FROM tyres_activity_tbl WHERE flag='0'";		
				$query=mysqli_query($conn,$sql);		
				while ($rows = mysqli_fetch_array($query)){ ?>		
				<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>		
				<?php } ?>		
			</select>		
		</div>		
	</div>		
	<div class="row"></div>		
	<div class="row">		
		<div class="col-xs-8 col-xs-offset-2 form-group">		
			<textarea class="form-control" maxlength="1000" id="comments" name="comments" style="min-height:100px;" placeholder="Comments..."></textarea>		
		</div>		
    </div>		
	<div class="row"></div>		
    <div class="row">		
		<div class="col-xs-3 col-xs-offset-2 form-group">		
			<label>&nbsp;FollowUp Date</label>		
		</div>		
		<div class="col-xs-5">		
			<div class="form-group">		
				<div class='input-group date' id='datetimepicker6'>		
					<input type='text' class="form-control" name="follow_date"/>		
					<span class="input-group-addon">		
					<span class="glyphicon glyphicon-calendar"></span>		
					</span>		
				</div>		
			</div>		
		</div>		
	</div>		
	<div class="row">		
		<div class="col-xs-3 col-xs-offset-2 form-group">		
			<label>&nbsp;Priority</label>		
		</div>		
		<div class="col-xs-5">		
			<div class="form-group">		
				<input type="radio" name="priority" value="1" style="width:30px;">Low</input>		
				<input type="radio" name="priority" value="2" style="width:30px;">Medium</input>		
				<input type="radio" name="priority" value="3" style="width:30px;" checked>High</input>	
					
			</div>		
		</div>		
	</div>					
	<div class="row"></div>		
	<div class="row">		
		<div class="col-xs-2 col-xs-offset-5 form-group">		
			<br>		
			<input class="form-control" type="submit" id="followup_submit" name="followup_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>		
		</div>		
	</div>		
	<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >		
	<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >		
	<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >		
	<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >		
   </form>		
 </div>
 
</div>
</div>
</div>

<!-- move to  others -->
<!-- Modal -->

<!-- <div class="modal fade" id="myModal_others" role="dialog">
<div class="modal-dialog"> -->

<!-- Modal content-->
<!-- <div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Move to Others (Id : <?php echo $user_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="add_others" class="form" method="post" action="add_others.php">
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<br>
<select class="form-control" id="others_status" name="status" required>
<option selected value="">Select Reason</option>
<?php
$sql="SELECT activity FROM tyres_activity_tbl WHERE flag='2'";
$query=mysqli_query($conn,$sql);

while ($rows = mysqli_fetch_array($query)){ ?>
<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>
<?php } ?>
</select>
</div>
</div>
<div class="row"></div>

<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<textarea class="form-control" maxlength="1000" id="comments" name="comments" style="min-height:100px;" placeholder="Comments..."></textarea>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<br>
<input class="form-control" type="submit" id="others_submit" name="others_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
</form>
</div>
</div>
</div>
</div> -->
<!-- add RNR 1 -->
<!-- Modal -->
<div class="modal fade" id="myModal_reschedule" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Reschedule (Id : <?php echo $booking_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="reschedule_booking" class="form" method="post" action="reschedule_booking.php">
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<br>
<select class="form-control" id="reschedule_status" name="status" required>
<option selected value="">Select Reason</option>
<?php
$sql="SELECT activity FROM tyres_activity_tbl WHERE flag='4'";
$query=mysqli_query($conn,$sql);

while ($rows = mysqli_fetch_array($query)){ ?>
<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>
<?php } ?>
</select>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<textarea class="form-control" maxlength="1000" id="comments" name="comments" style="min-height:100px;" placeholder="Comments..."></textarea>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-3 col-xs-offset-2 form-group">
<label>&nbsp;Rechedule To</label>
</div>
<div class="col-xs-5  form-group">
<div class="form-group">
                <div class='input-group date' id='datetimepicker4'>
                    <input type='text' class="form-control" name="reschedule_to"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
</div></div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<br>
<input class="form-control" type="submit" id="reschedule_submit" name="reschedule_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >

</form>
</div>
</div>
</div>
</div>

<!-- Cancel Bookings -->
<!-- Modal -->
<div class="modal fade" id="myModal_cancel_booking" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h3 class="modal-title">Cancel Booking (Id : <?php echo $booking_id; ?>)</h3>
</div>
<div class="modal-body">
<form id="cancel_booking" class="form" method="post" action="cancel_booking.php">
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<br>
<select class="form-control" id="cancel_status" name="status" required>
<option selected value="">Select Reason</option>
<?php
$sql="SELECT activity FROM tyres_activity_tbl WHERE flag='1'";
$query=mysqli_query($conn,$sql);

while ($rows = mysqli_fetch_array($query)){ ?>
<option value="<?php echo $rows['activity']; ?>"> <?php echo $rows['activity']; ?> </option>
<?php } ?>
</select>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-8 col-xs-offset-2 form-group">
<textarea class="form-control" maxlength="1000" id="cancel_comments" name="comments" style="min-height:100px;" placeholder="Comments..." required></textarea>
</div>
</div>
<div class="row"></div>
<div class="row">
<div class="col-xs-2 col-xs-offset-5 form-group">
<br>
<input class="form-control" type="submit" id="cancel_submit" name="cancel_submit" value="Submit" style="background-color:#0BBFEC; color:black; box-shadow:0 3px 3px 0 #000;"/>
</div></div>
<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" >
<input type="hidden" id="veh_id" name="veh_id" value="<?php echo $veh_id; ?>" >
<input type="hidden" id="log" name="log" value="<?php echo $log; ?>" >
<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" >
<input type="hidden" id="book_id" name="book_id" value="<?php echo $booking_id; ?>" >
</form>
</div>
</div>
</div>
</div>



<!-- brand and model -->
 <script>
$(function() {
		    $( "#BrandModelid" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_brandmodel.php",
                data: {
                    term: request.term,
					extraParams:$('#veh:checked').val()
                },
                dataType: 'json'
            }).done(function(data) {
				//console.log(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#BrandModelid").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "9999999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script>
<!-- user location -->
<script>
$("#location_home").on("click", function(){
var c = $('#cityu').val();
  //console.log(c);
  if(c==''){
    alert("Plese select City to get localityies!");
  }
  else{
$(function() {

			    $( "#location_home" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_user_locality.php",
				type : "GET",
                data: {
                    term: request.term,
          					city:$('#cityu').val()
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location_home").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

  }
});

</script>
<!-- locality work -->
<script>
$("#location_work").click(function(){
var c = $('#cityu').val();
  //console.log(c);
  if(c==''){
    alert("Plese select City to get localityies!");
  }
  else{
$(function() {
			    $( "#location_work" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_work_locality.php",
				type : "GET",
                data: {
                    term: request.term,
          					city:$('#cityu').val()
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location_work").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});
  }
});
</script>
<!-- empty autocompletes localities on city change -->
<script>
  $(document).ready(function(){
	  	  $('.alert-status').bootstrapSwitch();
$('#pickup_switch').on('switchChange.bootstrapSwitch', function (event, state) {
//console.log(state);
var s;
//console.log(state);
if(state==true){
	$("#pickaddress").show();
   $("#pickdate").show();
s = 1;
}
else
{
	$("#pickaddress").hide();
    	$("#pickdate").hide();
    	s = 0;
}

$("#pickupe").val(s);
$("#pickup_alt").val(s);
});
    $('#cityu').change(function(){
      $('#location_work').empty();
      $('#location_work').autocomplete('close').val('');
      $('#location_home').empty();
      $('#location_home').autocomplete('close').val('');
    });
  })
  </script>

<!-- reg number -->
<!-- masking -->
<script type="text/javascript" src="js/inputmask.js"></script>
<script type="text/javascript" src="js/jquery.inputmask.js"></script>
  <script type="text/javascript" src="js/inputmask.extensions.js"></script>

<script>
Inputmask("A{2}/9{2}/A{2}/9{4}").mask($("#regno"));
</script>

<!-- select Vehicle Number  -->
<script>
$(document).ready(function($) {
 $(document).on('change', '[name="veh_b"]' , function(){
  	var selectvalue = $('[name="veh_b"]:checked').val();
	var selectuser = <?php echo $user_id; ?>;
	$("#veh_no").empty();
		 $("div.veh").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_veh.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selectvalue": selectvalue , "selectuser" : selectuser},
            success : function(data) {
				// alert(data);
                $('#veh_no').append(data);
                 $("#location").autocomplete('close').val('');
                 $("#service_type").autocomplete('close').val('');
                  $("#mechanic").empty();
                  $("#mechanic").append('<option value="">Select Mechanic</option>');

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

    });
});
</script>
<!-- select vehicle on page load -->
<script>
$(document).ready(function($) {
  	var selectvalue = $('[name="veh_b"]:checked').val();
	var selectuser = <?php echo $user_id; ?>;
  var veh_no = $('#veh_no').val();
	$("#veh_no").empty();
		 $("div.veh").show();
      // $("#location").autocomplete('close').val('');
      // $("#mechanic").empty();
      // $("#mechanic").append('<option value="">Select Mechanic</option>');
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_veh.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selectvalue": selectvalue , "selectuser" : selectuser, "veh_no": veh_no},
            success : function(data) {
				// alert(data);
                $('#veh_no').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

});
</script>

<!-- select Service type -->
<!-- select Service type -->
 <script>
$(function() {
			    $( "#service_type" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_service_type.php",
                data: {
                    term: request.term,
					//type : $('#veh_b:checked').val()
						type : $('#veh_b:checked').val(),
					vtype: $('#veh_no').val(),
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#service_type").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script>

<!-- select Mechanic Based On Location -->
<script>
$(document).ready(function($) {
 $("#location").on("autocompletechange", function(){
  //	var selectservice = $("#service_type").val();
	var selecttype = $('[name="veh_b"]:checked').val();
	var selectloc = $("#location").val();
  var veh_no = $("#veh_no").val();
  var service = $("#service_type").val();
  var book_id = $("#book_id").val();
  var city = $("#city").val();
  var crm = '<?php echo $crm_log_id; ?>';
  var type = '<?php echo $type; ?>';
  console.log(crm);
	$("#mechanic").empty();
		 $("div.mec").show();
          //Make AJAX request, using the selected value as the POST
		  





		  // if(crm != 'crm036' && crm != 'crm012' && crm != 'crm041' && crm != 'crm018')
		  			  if(crm != 'crm036' && crm != 'crm012' && crm != 'crm041' && crm != 'crm046')
		  // if((selecttype === '2w' || service === 'Express Car Service 999' || service === 'Car dent / scratch removal' || service === 'Dent Removal 2499' || service === 'Dent 2999 1-2 Panels' || service === 'Dent 2999 3-4 Panels' || service === 'Dent 2999 5-6 Panels' || service === 'Dent 2999 7-8 Panels' || service === 'Dent Removal 2999') && crm != 'crm036' && city == 'Chennai' && crm != 'crm012' && crm != 'crm041')
		  // if(selecttype === '2w' && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
		  {
			  $('.loader').show();
			  $('.loader_overlay').show();
			  $.ajax({
            url : "ajax/grt_veh.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc ,"veh_no": veh_no ,"service":service,"book_id":book_id,"crm_log_id":crm},
            success : function(data) {
				$('.loader').hide();
				$('.loader_overlay').hide();
				// console.log(data);
				//alert(data);
                $('#mechanic').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });
			
		  }
		  else
		  {
		  $.ajax({
            url : "ajax/get_mechanic_loc.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc,"veh_no": veh_no , "service":service},
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanic').html(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });
		  }

    });
});
</script>


<!-- --------------------------------------- edit booking ------------------------------------------------------ -->
<!-- select Vehicle Number  -->
<script>
$(document).ready(function($) {
 $(document).on('change', '[name="veh_be"]' , function(){
  	var selectvalue = $('[name="veh_be"]:checked').val();
	var selectuser = <?php echo $user_id; ?>;
	$("#veh_noe").empty();
  
		 $("div.veh").show();
          //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_veh.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selectvalue": selectvalue , "selectuser" : selectuser},
            success : function(data) {
				// alert(data);
                $('#veh_noe').append(data);
                // $("#locatione").autocomplete('close').val('');
				$("#location_pickup").autocomplete('close').val('');
                $("#service_typee").autocomplete('close').val('');
                $("#mechanice").empty();
                $("#mechanice").append('<option value="">Select Mechanic</option>');

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

    });
});
</script>
<!-- select vehicle on page loaad -->
<script>
$(document).ready(function($) {
  	var selectvalue = $('[name="veh_be"]:checked').val();
	var selectuser = <?php echo $user_id; ?>;
	$("#veh_noe").empty();
		 $("div.veh").show();
      //Make AJAX request, using the selected value as the POST
		  $.ajax({
            url : "ajax/get_veh.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selectvalue": selectvalue , "selectuser" : selectuser},
            success : function(data) {
				// alert(data);
                $('#veh_noe').append(data);
               // $("#locatione").autocomplete('close').val('');
               // $("#mechanice").empty();
               // $("#mechanice").append('<option valu="">Select Mechanic</option>');

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });

});
</script>

<!-- select Service type -->
 <script>
$(function() {
			    $( "#service_typee" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_service_type.php",
                data: {
                    term: request.term,
					//type : $('#veh_b:checked').val()
						type : $('#veh_be:checked').val(),
					vtype: $('#veh_noe').val(),
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#service_typee").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script>
<!-- select Location of service center -->
 <script>
$(function() {
			    $( "#location" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_location.php",
                data: {
                    term: request.term,
					//type : $('#veh_b:checked').val()
					vtype: $('[name="veh_b"]:checked').val(),
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script> 

<!-- edit booking select Location of service center -->
 <script>
/*$(function() {
			    $( "#locatione" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_location.php",
                data: {
                    term: request.term,
					//type : $('#veh_b:checked').val()
					vtype: $('[name="veh_be"]:checked').val(),
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#locatione").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});*/

$(function() {
			    $( "#location_pickup" ).autocomplete({
source: function(request, response) {
            $.ajax({
                url: "ajax/get_location.php",
                data: {
                    term: request.term,
					//type : $('#veh_b:checked').val()
					vtype: $('[name="veh_be"]:checked').val(),
					 },
                dataType: 'json'
            }).done(function(data) {
				//alert(data);
                if (data!=null) {
                    response($.map(data, function(item) {
                        return item;
                    }));
                } else {
					response($.map(data, function(item) {
                        return "No Results";
                    }));
                }
            });
        },
appendTo : $("#location_pickup").next(),
        delay: 0,
        minLength: 0,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
            } else {
                $("#message").empty();
            }
        },
		 open: function(event, ui) {
						$(".ui-autocomplete").css("position", "absolute");
						$(".ui-autocomplete").css("z-index", "99999");
		},
		 change: function (event, ui) {
                if (!(ui.item)) event.target.value = "";
            }
        }).focus(function() {
			 $(this).autocomplete("search");
		});
});

</script> 

<!-- select Mechanic Based On Location -->
<script>
$(document).ready(function($) {
 $("#service_typee").on("autocompletechange", function(){
	 console.log('he');
	 $('#location_pickup').attr('value', '');
	 $("#mechanice").empty();
 });
	
 $("#location_pickup").on("autocompletechange", function(){
  	//var selectservice = $("#service_typee").val();
	var selecttype = $('[name="veh_be"]:checked').val();
	var selectloc = $("#location_pickup").val();
  var veh_no = $("#veh_noe").val();
  var service = $("#service_typee").val();
  var book_id = $("#book_id").val();
  var city = $("#city").val();
  var crm = '<?php echo $crm_log_id; ?>'; 
  var type = '<?php echo $type; ?>';
  var brand_type = $("#tyre_brand").val();
  //alert(brand_type);
  console.log(crm);
	$("#mechanice").empty();

		 $("div.mece").show();
          //Make AJAX request, using the selected value as the POST
		  // if(crm != 'crm036' && crm != 'crm012' && crm != 'crm041' && crm != 'crm018')

		  			  if(crm != 'crm036' && crm != 'crm012' && crm != 'crm041' && crm != 'crm046',&& crm != 'crm001')
		  // if((selecttype === '2w' || service === 'Express Car Service 999' || service === 'Car dent / scratch removal' || service === 'Dent Removal 2499' || service === 'Dent 2999 1-2 Panels' || service === 'Dent 2999 3-4 Panels' || service === 'Dent 2999 5-6 Panels' || service === 'Dent 2999 7-8 Panels' || service === 'Dent Removal 2999') && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
		  // if(selecttype === '2w' && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
		  {
			  $('.loader').show();
			  $('.loader_overlay').show();
			  $.ajax({
            url : "ajax/get_mec.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc ,"veh_no": veh_no ,"service":service,"book_id":book_id,"crm_log_id":crm,"brand_type" : brand_type},
            success : function(data) {
				// console.log(data);
				$('.loader').hide();
			  $('.loader_overlay').hide();
				//alert(data);
                $('#mechanice').html(data.shopname);
                $('#mec_ref').val(data.mec_ref);
                $('#axle_ref').val(data.axle_ref);
                $('#shopname_ref').val(data.shopname_ref);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrown	Error);
          }

        });
			
		  }
		  else
		  {
		  $.ajax({
            url : "ajax/get_mechanic_loc.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc ,"veh_no": veh_no ,"service":service},
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanice').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });
		}
    });
});
</script>
<!-- select Mechanic Based On Location  on page load-->
<!--<script>
$(document).ready(function($) {
 //$("#location_pickup").on("autocompletechange", function(){
  	//var selectservice = $("#service_typee").val();
	var selecttype = $('[name="veh_be"]:checked').val();
	var selectloc = $("#location_pickup").val();
  var veh_no = $("#veh_noe").val();
  var service = $("#service_typee").val();
  var book_id = $("#book_id").val();
  var city = $("#city").val();
  var crm = '<?php echo $crm_log_id; ?>'; 
  var type = '<?php echo $type; ?>';
  console.log(crm);
  if(selectloc != ""){
    $("#mechanice").empty();

		 $("div.mece").show();
          //Make AJAX request, using the selected value as the POST
		  if((selecttype === '2w' || service === 'Express Car Service 999' || service === 'Car dent / scratch removal' || service === 'Dent Removal 2499' || service === 'Dent 2999 1-2 Panels' || service === 'Dent 2999 3-4 Panels' || service === 'Dent 2999 5-6 Panels' || service === 'Dent 2999 7-8 Panels' || service === 'Dent Removal 2999') && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
		  // if(selecttype === '2w' && crm != 'crm036' && city == 'Chennai' && (crm != 'crm012' || crm != 'crm041'))
		  {
			  $('.loader').show();
			  $('.loader_overlay').show();
			  $.ajax({
            url : "ajax/project_crown.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc ,"veh_no": veh_no ,"service":service,"book_id":book_id},
            success : function(data) {
				// console.log(data);
				$('.loader').hide();
				$('.loader_overlay').hide();
				//alert(data);
                $('#mechanice').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });
			
		  }
		  else
		  {
		  $.ajax({
            url : "ajax/get_mechanic_loc.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc,"veh_no": veh_no , "service":service},
            success : function(data) {
				//console.log(data);
				//alert(data);
                $('#mechanice').append(data);

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });
		  }
  }
});
</script>-->


<!-- --------------------------------------- edit booking ----------------------------------------------- -->
<!-- validation -->
<script>
var userinput = document.getElementById('user_name');
userinput.oninvalid = function(event) {
	  document.getElementById("user_name").style.borderColor="#E42649";
    event.target.setCustomValidity('Only aplhabets and digits are allowed!');
}
</script>
<script>
 var date = new Date();
date.setDate(date.getDate());

$('.datepicker').datepicker({
   autoclose: true,
    startDate: date
});
// if($('input.datepicker').val()){

//     $('input.datepicker').datepicker('setDate', 'today');
//  }
 

	  
	  // $(document).ready(function() {
 
// $('#tyres_req_times').change(function(){
// 	var val = $('#tyres_req_times').val();
// 	console.log(val);
// 	 var date = new Date();
// 	 //console.log(date);

// 	  //date.setMonth(date.getMonth() + 1);
// 	 //console.log(date);
// 	 //var months = date.getMonth('3');

// 	if (val =='immediately'){
// 		$('.datepicker2').datepicker('remove');
// 		$('.datepicker2').datepicker({
//    autoclose: true,
//     startDate: date
// });
// 	}
// 		else if(val =='in1month')
// 		{
// 			 var one = new Date();
// 		one.setMonth(one.getMonth() + 1);
// 	console.log(one);
// 	$('.datepicker2').datepicker('remove');
// 			$('.datepicker2').datepicker({
//       autoclose: true,
//        startDate: one
// });
// 		}
// 		else if(val =='in3months')
// 		{
// 			var three = new Date();
// 		three.setMonth(three.getMonth() + 3);
// 		$('.datepicker2').datepicker('remove');
// 			$('.datepicker2').datepicker({
//       autoclose: true,
//        startDate: three	
// });
// 		}
	

// });


</script>
<!-- date time picker -->
<script type="text/javascript">
    $(function () {
    	//var pickDate = $("#service_datee").val();
        var dateNow = new Date();
        // console.log(dateNow);
        

	 $('#datetimepicker7').datetimepicker({
            
            format: 'DD-MM-YYYY HH:mm',
            defaultDate:dateNow,
            minDate:dateNow

        });
        $('#datetimepicker6').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            defaultDate:dateNow
        });
         $('#datetimepicker1').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            defaultDate:dateNow
        });
         $('#datetimepicker2').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            defaultDate:dateNow
        });
         $('#datetimepicker3').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            defaultDate:dateNow
        });
        $('#datetimepicker4').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            defaultDate:dateNow
        });
    });

</script>
 <!-- select veh before fetching models -->
 <!-- <script>
 $(document).ready(function(){
   $("#BrandModelid").click(function(){
     var veh = $('input[name=veh]:checked').val();
     //console.log(veh);
     if(veh == null){
       //console.log(veh);
       alert("Please select vehicle type to get vehicle models!");
     }
   });
 })
 </script> -->
 <script>
 $(document).ready(function(){
$('input[name=veh]').change(function(){
  $("#BrandModelid").val("");
});
 });
 </script>
 <!-- disable buttons on submit -->
 <script>
 $(document).ready(function(){
  $("#add_followup").submit(function(e){
    $('#followup_submit').attr('disabled','disabled');
    var option = document.getElementById('followup_status');
    option.oninvalid = function(event) {
        document.getElementById("followup_status").style.borderColor="#E42649";
        event.target.setCustomValidity('Please select a reason!');
        $('#followup_submit').removeAttr('disabled'); 
    }
  });
 });
 </script>
 <script>
 $(document).ready(function(){
  $("#add_rnr1").submit(function(e){
    $('#rnr1_submit').attr('disabled','disabled');
    var option = document.getElementById('rnr1_status');
    option.oninvalid = function(event) {
        document.getElementById("rnr1_status").style.borderColor="#E42649";
        event.target.setCustomValidity('Please select a reason!');
        $('#rnr1_submit').removeAttr('disabled'); 
    }
  });
 });
 </script>
  <script>
 $(document).ready(function(){
  $("#add_rnr2").submit(function(e){
    $('#rnr2_submit').attr('disabled','disabled');
    var option = document.getElementById('rnr2_status');
    option.oninvalid = function(event) {
        document.getElementById("rnr2_status").style.borderColor="#E42649";
        event.target.setCustomValidity('Please select a reason!');
        $('#rnr2_submit').removeAttr('disabled'); 
    }
  });
 });
 </script>
  <script>
 $(document).ready(function(){
  $("#add_others").submit(function(e){
    $('#others_submit').attr('disabled','disabled');
    var option = document.getElementById('others_status');
    userinput.oninvalid = function(event) {
        document.getElementById("others_status").style.borderColor="#E42649";
        event.target.setCustomValidity('Please select a reason!');
        $('#others_submit').removeAttr('disabled'); 
    }
  });
 });
 </script>
  <script>
 $(document).ready(function(){
  $("#cancel_booking").submit(function(e){
    $('#cancel_submit').attr('disabled','disabled');
    var option = document.getElementById('cancel_status');
    option.oninvalid = function(event) {
        document.getElementById("cancel_status").style.borderColor="#E42649";
        event.target.setCustomValidity('Please select a reason!');
        $('#cancel_submit').removeAttr('disabled'); 
    }
    var comt = document.getElementById('cancel_comments');
    comt.oninvalid = function(event) {
        document.getElementById("cancel_comments").style.borderColor="#E42649";
        event.target.setCustomValidity('Please explain the reason to cancel!');
        $('#cancel_submit').removeAttr('disabled'); 
    }
  });
 });
 </script>
 <!-- check if locality is empty -->
<script>
$(document).ready(function($) {
  $("#mechanic").click(function(){
    if($("#location").val()== ''){
      $("#mechanic").empty();
      $("#mechanic").append('<option value="">Select Mechanic</option>');
      alert("Please select a Location to get mechanics!!!");
    }
  });
});
</script>
<script>
$(document).ready(function($) {
  $("#mechanice").click(function(){
    if($("#location_pickup").val()== ''){
      $("#mechanice").empty();
      $("#mechanice").append('<option value="">Select Mechanic</option>');
      alert("Please select a Location to get mechanics!!!");
    }
  });
});
</script>
<script>
$(document).ready(function($) {
  
  $("#go-div").click(function(){
    if($("#location_pickup").val()== ''){
      alert("Oops no location has been selected!!!");
    }
	else
	{
	var selecttype = $('[name="veh_be"]:checked').val();
	var selectloc = $("#location_pickup").val();
  var veh_no = $("#veh_noe").val();
  var service = $("#service_typee").val();
  var book_id = $("#book_id").val();
  var city = $("#city").val();
  var crm = '<?php echo $crm_log_id; ?>'; 
  
  var type = '<?php echo $type; ?>';
  var brand_type = $("#tyre_brand").val();
  //alert (brand_type);
  console.log(crm);
	$("#mechanice").empty();

		 $("div.mece").show();
          //Make AJAX request, using the selected value as the POST
		  // if(crm != 'crm036' && crm != 'crm012' && crm != 'crm041' && crm != 'crm018')

	  if(crm != 'crm036' && crm != 'crm012' && crm != 'crm041' && crm != 'crm046' && crm != 'crm001')
	
		  {
			  $('.loader').show();
			  $('.loader_overlay').show();
			  $.ajax({
            url : "ajax/get_mec.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc ,"veh_no": veh_no ,"service":service,"book_id":book_id,"crm_log_id":crm,"brand_type":brand_type},
            success : function(data) {
				// console.log(data);
				$('.loader').hide();
			  $('.loader_overlay').hide();
				//alert(data);
                $('#mece').html(data.shopname);
                $('#mec_ref').val(data.mec_ref);
                $('#axle_ref').val(data.axle_ref);
                $('#shopname_ref').val(data.shopname_ref);

                $(function() {

$('#mechanice').multiselect({

includeSelectAllOption: false,
buttonWidth: '100%',

});

});

               
            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrown	Error);
          }

        });
			
		  }
		  else
		  {
		  	
		  $.ajax({
            url : "ajax/get_mec_override.php",  // create a new php page to handle ajax request
            type : "POST",
            data : {"selecttype": selecttype , "selectloc" : selectloc ,"veh_no": veh_no ,"service":service,"book_id":book_id,"crm_log_id":crm,"brand_type":brand_type},
            success : function(data) {
				//console.log(data);
				//alert(data);
               $('.loader').hide();
			  $('.loader_overlay').hide();
				//alert(data);
				
                $('#mece').html(data.shopname);
        		var a = new Array();
				$("#mechanice").children("option").each(function(x){
				test = false;
				b = a[x] = $(this).val();
				for (i=0;i<a.length-1;i++){
				if (b ==a[i]){
				test =true;
				}
				}
				if (test){
				$(this).remove();
				}
				});

                //alert(data.shopname);
                $('#mec_ref').val(data.mec_ref);
                $('#axle_ref').val(data.axle_ref);
                $('#shopname_ref').val(data.shopname_ref);
                //$('#mechanice').attr("multiple","multiple");
                $(function() {

//$('#mechanice').multiselect({


//});
$(document).ready(function() {
        $('#mechanice').multiselect({
includeSelectAllOption: false,
buttonWidth: '100%',
nSelectedText: 'selected',
            onChange: function(option, checked) {
                // Get selected options.
                var selectedOptions = $('#mechanice option:selected');
 
                if (selectedOptions.length >= 4) {
                    // Disable all other checkboxes.
                    var nonSelectedOptions = $('#mechanice option').filter(function() {
                        return !$(this).is(':selected');
                    });
 
                    nonSelectedOptions.each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
                else {
                    // Enable all checkboxes.
                    $('#mechanice option').each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', false);
                        input.parent('li').addClass('disabled');
                    });
                }
            }
        });
    });
 
});
            

            },
          error: function (xhr, ajaxOptions, thrownError) {
           // alert(xhr.status + " "+ thrownError);
          }

        });
		}
		 		  
	}
	});
	});


$('.btn').on("click",function(){
	var id = $(this).data("bid");
	if(id!=null)
	{
		//alert(id);
		$.ajax({
			type: 'GET',
			url: 'ajax/firstresponse1.php',
			data: {
				bi: id
			}
		});
	}
});
</script>
<script>
	$(document).ready(function() {
        $('#mechanice').multiselect({
includeSelectAllOption: false,
buttonWidth: '100%',
nSelectedText: 'selected',

        });
    });
 
</script>
<script>
	$('th').click(function(){
    var table = $(this).parents('table').eq(0)
    var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
    this.desc = !this.desc
    if (!this.desc){rows = rows.reverse()}
    for (var i = 0; i < rows.length; i++){table.append(rows[i])}
})
function comparer(index) {
    return function(a, b) {
        var valA = getCellValue(a, index), valB = getCellValue(b, index)
        return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.toString().localeCompare(valB)
    }
}
function getCellValue(row, index){ return $(row).children('td').eq(index).text() }
</script>

 <script type="text/javascript">

$(function() {
	var pickup = $('#pickup_alt').val();
	console.log(pickup);
	if(pickup == '1'){
		$("#pickaddress").show();
    	$("#pickdate").show();
	}
	else
	{
		$("#pickaddress").hide();
		$("#pickdate").hide()
	}
});

    
</script>
</body>
</html>

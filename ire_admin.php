<?php
include("../sidebar.php");
$conn = db_connect1();
// login or not
if ((empty($_SESSION['crm_log_id'])) || $flag != "1") {
    header('location:logout.php');
    die();
}
$_SESSION['modal_sess'] = 'open';
$city = $_SESSION['crm_city'];
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>GoBumpr Bridge</title>

    <!-- Facebook Pixel Code -->
    <script async>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '582926561860139');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
        /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    <!-- Google Analytics Code -->
    <script async>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-67994843-2', 'auto');
        ga('send', 'pageview');

    </script>

    <style>
        .borderless td, .borderless th {
            border: none !important;
        }

        * {
            box-sizing: border-box;
        }

        html {
            min-height: 100%;
        }

        body {
            color: black;
            margin: 0px;
            min-height: inherit;
            background: rgb(255, 255, 255) !important;
        }

        [data-sidebar-overlay] {
            display: none;
            position: fixed;
            top: 0px;
            bottom: 0px;
            left: 0px;
            opacity: 0;
            width: 100%;
            min-height: inherit;
        }

        .overlay {
            background-color: rgb(222, 214, 196);
            z-index: 999990 !important;
        }

        aside {
            position: relative;
            height: 100%;
            width: 200px;
            top: 0px;
            left: 0px;
            background-color: rgb(236, 239, 241);
            box-shadow: rgba(0, 0, 0, 0.8) 0px 6px 6px 0px;
            z-index: 999999 !important;
        }

        [data-sidebar] {
            display: none;
            position: absolute;
            height: 100%;
            z-index: 100;
        }

        .padding {
            padding: 2em;
        }

        .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
            font-family: inherit;
            font-weight: 500;
            line-height: 1.1;
            color: inherit;
        }

        .h4, .h5, .h6, h4, h5, h6 {
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .h4, h4 {
            font-size: 18px;
        }

        img {
            border: 0px;
            vertical-align: middle;
        }

        a {
            text-decoration: none;
            color: black;
        }

        aside a {
            color: rgb(0, 0, 0);
            font-size: 16px;
            text-decoration: none;
        }

        .fa {
            display: inline-block;
            font-style: normal;
            font-variant: normal;
            font-weight: normal;
            font-stretch: normal;
            line-height: 1;
            font-family: FontAwesome;
            font-size: inherit;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
        }

        nav, ol {
            font-size: 18px;
            margin-top: -4px;
            background: rgb(0, 150, 136) !important;
        }

        .navbar-fixed-top {
            z-index: 100 !important;
        }

        .container-fluid {
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        ol, ul {
            margin-top: 0px;
            margin-bottom: 10px;
        }

        .breadcrumb > li {
            display: inline-block;
        }

        ol ol, ol ul, ul ol, ul ul {
            margin-bottom: 0px;
        }

        .nav {
            padding-left: 0px;
            margin-bottom: 0px;
            list-style: none;
        }

        .navbar-nav {
            margin: 0px;
            float: left;
        }

        .navbar-right {
            margin-right: -15px;
            float: right !important;
        }

        .nav > li {
            position: relative;
            display: block;
        }

        .navbar-nav > li {
            float: left;
        }

        .dropdown {
            position: relative;
            display: inline-block;
        }

        .form-group {
            margin-bottom: 15px;
        }

        button, input, optgroup, select, textarea {
            margin: 0px;
            font-style: inherit;
            font-variant: inherit;
            font-weight: inherit;
            font-stretch: inherit;
            font-size: inherit;
            line-height: inherit;
            font-family: inherit;
            color: inherit;
        }

        button, select {
            text-transform: none;
        }

        button, input, select, textarea {
            font-family: inherit;
            font-size: inherit;
            line-height: inherit;
        }

        .form-control {
            display: block;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857;
            color: rgb(85, 85, 85);
            background-color: rgb(255, 255, 255);
            background-image: none;
            border: 1px solid rgb(204, 204, 204);
            border-radius: 4px;
            box-shadow: rgba(0, 0, 0, 0.075) 0px 1px 1px inset;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }

        p {
            margin: 0px 0px 10px;
        }

        b, strong {
            font-weight: 700;
        }

        .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
            position: relative;
            min-height: 1px;
            padding-right: 15px;
            padding-left: 15px;
        }

        .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9 {
            float: left;
        }

        .col-sm-2 {
            width: 16.6667%;
        }

        .col-sm-offset-1 {
            margin-left: 8.33333%;
        }

        .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9 {
            float: left;
        }

        .col-lg-3 {
            width: 25%;
        }

        .col-lg-offset-1 {
            margin-left: 8.33333%;
        }

        .col-sm-3 {
            width: 25%;
        }

        .glyphicon {
            position: relative;
            top: 1px;
            display: inline-block;
            font-family: "Glyphicons Halflings";
            font-style: normal;
            font-weight: 400;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
        }

        .caret {
            display: inline-block;
            width: 0px;
            height: 0px;
            margin-left: 2px;
            vertical-align: middle;
            border-top: 4px dashed;
            border-right: 4px solid transparent;
            border-left: 4px solid transparent;
        }

        .floating-box {
            display: inline-block;
            margin: 22px;
            padding: 22px;
            width: 203px;
            height: 105px;
            box-shadow: rgba(0, 0, 0, 0.2) 0px 8px 16px 0px;
            font-size: 20px;
        }

        .uil-default-css {
            position: relative;
            background: none;
            width: 200px;
            height: 200px;
        }

        .uil-default-css > div:nth-of-type(1) {
            animation: uil-default-anim 1s linear -0.5s infinite;
        }

        .uil-default-css > div:nth-of-type(2) {
            animation: uil-default-anim 1s linear -0.416667s infinite;
        }

        .uil-default-css > div:nth-of-type(3) {
            animation: uil-default-anim 1s linear -0.333333s infinite;
        }

        .uil-default-css > div:nth-of-type(4) {
            animation: uil-default-anim 1s linear -0.25s infinite;
        }

        .uil-default-css > div:nth-of-type(5) {
            animation: uil-default-anim 1s linear -0.166667s infinite;
        }

        .uil-default-css > div:nth-of-type(6) {
            animation: uil-default-anim 1s linear -0.0833333s infinite;
        }

        .uil-default-css > div:nth-of-type(7) {
            animation: uil-default-anim 1s linear 0s infinite;
        }

        .uil-default-css > div:nth-of-type(8) {
            animation: uil-default-anim 1s linear 0.0833333s infinite;
        }

        .uil-default-css > div:nth-of-type(9) {
            animation: uil-default-anim 1s linear 0.166667s infinite;
        }

        .uil-default-css > div:nth-of-type(10) {
            animation: uil-default-anim 1s linear 0.25s infinite;
        }

        .uil-default-css > div:nth-of-type(11) {
            animation: uil-default-anim 1s linear 0.333333s infinite;
        }

        .uil-default-css > div:nth-of-type(12) {
            animation: uil-default-anim 1s linear 0.416667s infinite;
        }

        .dropdown-menu {
            position: absolute;
            top: 100%;
            left: 0px;
            z-index: 1000;
            display: none;
            float: left;
            min-width: 160px;
            padding: 5px 0px;
            margin: 2px 0px 0px;
            font-size: 14px;
            text-align: left;
            list-style: none;
            background-color: rgb(255, 255, 255);
            -webkit-background-clip: padding-box;
            background-clip: padding-box;
            border: 1px solid rgba(0, 0, 0, 0.15);
            border-radius: 4px;
            box-shadow: rgba(0, 0, 0, 0.176) 0px 6px 12px;
        }

        button, html input[type="button"], input[type="reset"], input[type="submit"] {
            -webkit-appearance: button;
            cursor: pointer;
        }

        button {
            overflow: visible;
        }

        .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0px;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            touch-action: manipulation;
            cursor: pointer;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        .btn-success {
            color: rgb(255, 255, 255);
            background-color: rgb(92, 184, 92);
            border-color: rgb(76, 174, 76);
        }

        .btn-group-sm > .btn, .btn-sm {
            padding: 5px 10px;
            font-size: 12px;
            line-height: 1.5;
            border-radius: 3px;
        }

        button[disabled], html input[disabled] {
            cursor: default;
        }

        .btn.disabled, .btn[disabled], fieldset[disabled] .btn {
            cursor: not-allowed;
            box-shadow: none;
            opacity: 0.65;
        }

        .btn-default {
            color: rgb(51, 51, 51);
            background-color: rgb(255, 255, 255);
            border-color: rgb(204, 204, 204);
        }

        input {
            line-height: normal;
        }

        .panel-body1 {
            background-color: #bdc3c8;
        }

        .panel-danger {

            border-color: #af1c34;

        }

        .panel-danger > .panel-heading {
            color: #f2dede !important;
            background-color: #af1c34 !important;
            border-color: #af1c34 !important;
            background-image: linear-gradient(to bottom, #ef5959 0, #661b1b 100%) !important;

        }

        .panel-danger {
            border-color: #af1c34 !important;
        }

        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        input[type=number] {
            -moz-appearance: textfield;
        }

        .datepicker {
            cursor: pointer;
        }

        .datepicker:before {
            content: '';
            display: inline-block;
            border-left: 7px solid transparent;
            border-right: 7px solid transparent;
            border-top: 7px solid #ccc;
            border-bottom-color: transparent !important;
            position: absolute;
            top: -7px;
            left: 190px;
        / / I made a change here
        }

        .datepicker-dropdown:after {
            content: '';
            display: inline-block;
            border-left: 6px solid transparent;
            border-right: 6px solid transparent;
            border-bottom: 6px solid #fff;
            border-top: 0;
            position: absolute;
        }

    </style>
</head>
<body>
<?php include_once("../header.php"); ?>

<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>
<div style="margin-top:20px;margin-left:50px;font-size:20px;"></div>

<!-- date range picker -->
<form id="ire_admin_form" action="ajax/ire_admin_update.php" method="post">
    <div class="row">
        <div class="col-lg-4 col-sm-2 col-lg-offset-1 col-sm-offset-1">
            <div class="form-group" style="float:left;padding:8px;font-size:17px;">
                <label>From: </label>
            </div>
            <div class="form-group" style="float:left;width:150px; padding:5px;" title="Start Date" id="stdt">
                <input class="form-control datepicker " data-date-format='dd-mm-yyyy' type="text" id="start_date"
                       name="start_date">
            </div>
            <div class="form-group" style="float:left;padding:8px;font-size:17px;">
                <label>To : </label>
            </div>
            <!-- end date -->
            <div class="form-group" style="float:left;width:150px; padding:5px;" title="End Date" id="endt">
                <input class="form-control datepicker " data-date-format='dd-mm-yyyy' type="text" id="end_date"
                       name="end_date">
            </div>
        </div>

        <div class=" col-sm-1 col-lg-1" style="margin-left:20px; margin-top:8px;">
            <div class="floating-box1">
                <div style="max-width:130px;">
                    <select id="sel_city" class="form-control" style="width:130px;" name="city" required>
                        <option value="">Select City</option>
                        <option value="Chennai">Chennai</option>
                        <option value="Bangalore">Bangalore</option>
                        <option value="Hyderabad">Hyderabad</option>
                    </select>
                </div>
            </div>
        </div>

        <div class=" col-sm-1 col-lg-1" style="margin-left:20px; margin-top:8px;">
            <div class="floating-box1">
                <div style="max-width:130px;">
                    <select id="veh" name="veh" class="form-control" style="width:130px;" required>
                        <option value="">Select Type</option>
                        <option value="2w">2w</option>
                        <option value="4w">4w</option>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-4" style="margin-top:20px;font-size:14px;">
            <div class="panel panel-primary">
                <div class="panel-heading">Give Percentage Composition</div>
                <div class="panel-body">
                    <form>
                        <label>Level 1</label>
                        <input type="number" max="80" min="5" class="form-control" id="level1" name="data_limit1"
                               required>
                        <label>Level 2</label>
                        <input type="number" max="80" min="5" class="form-control" id="level2" name="data_limit2"
                               required>
                        <label>Level 3</label>
                        <input type="number" max="80" min="5" class="form-control" id="level3" name="data_limit3"
                               required>
                        <label>Level 4</label>
                        <input type="number" max="80" min="5" class="form-control" id="level4" name="data_limit4"
                               required>
                        <label>Level 5</label>
                        <input type="number" max="80" min="5" class="form-control" id="level5" name="data_limit5"
                               required><br>
                        <center><input type="submit" class="btn btn-info btn-md" value="Save" id="save"></center>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4" style="margin-top:20px;font-size:14px;">
            <div class="panel panel-danger">
                <div class="panel-heading">Note</div>
                <div class="panel-body panel-body1">
                    <p><i class="fa fa-hand-o-right" aria-hidden="true"></i> Level 1 - Not Go-Axled</p>
                    <p><i class="fa fa-hand-o-right" aria-hidden="true"></i> Level 2 - Go-Axled but service not
                        completed</p>
                    <p><i class="fa fa-hand-o-right" aria-hidden="true"></i> Level 3 - Service completed but no feedback
                    </p>
                    <p><i class="fa fa-hand-o-right" aria-hidden="true"></i> Level 4 - Service completed and Positive
                        Feedback</p>
                    <p><i class="fa fa-hand-o-right" aria-hidden="true"></i> Level 5 - Service completed and negative
                        feedback</p>
                </div>
            </div>
        </div>
    </div>
</form>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Message</h4>
            </div>
            <div class="modal-body">
                <span id="msg"></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<noscript id="async-styles">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
    <link rel="stylesheet" type="text/css" href="../css/style.css"/>
</noscript>

<script> // The CSS mentioned in "async-styles" will be loaded and styles will be applied.
    var loadDeferredStyles = function () {
        var addStylesNode = document.getElementById("async-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
        webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function () {
        window.setTimeout(loadDeferredStyles, 0);
    });
    else window.addEventListener('load', loadDeferredStyles);
</script>

<script async src="../js/jquery-3.2.1.min.js" onLoad="loadOtherScripts();"></script>

<script>
    function loadScript(src) {
        return new Promise(function (resolve, reject) {
            var s;
            s = document.createElement('script');
            s.src = src;
            s.onload = resolve;
            s.onerror = reject;
            document.head.appendChild(s);
        });
    }

    function loadOtherScripts() {
        loadScript('../js/moment.min.js')
            .then(function () {
                Promise.all([loadScript('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'), loadScript('../js/bootstrap-datetimepicker.min.js'), loadScript('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js'), loadScript('../js/sidebar.js'), loadScript('../js/jquery.table2excel.js'), loadScript('https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js')]).then(function () {
                    console.log('scripts are loaded');
                    dateTimePicker();
                    datePicker();
                    intialFunction();
                }).catch(function (error) {
                    console.log('some error!' + error)
                })
            }).catch(function (error) {
            console.log('Moment call error!' + error)
        })
    }
</script>

<!-- date range picker -->
<script>
    function dateTimePicker() {
        var date = new Date();
        date.setDate(date.getDate());
        var date2 = new Date();
        date2.setDate(date2.getDate() - 1);
        $('#start_date').datepicker({
            endDate: date,
            autoclose: true,
            orientation: 'auto'
        });
        $('#start_date').datepicker('setDate', date2);
        $('#end_date').datepicker({
            endDate: date,
            autoclose: true,
            orientation: 'auto'
        });
        $('#end_date').datepicker('setDate', 'today');
    }

</script>
<script>
    function datePicker() {
        $(function () {
            var dateNow = new Date();
            /* $('#datetimepicker1').datetimepicker({ format: 'HH:mm:ss',step:15}).val("00:00:00");*/
            $('#datetimepicker1').datetimepicker({
                format: 'hh:mm a',
                //defaultDate: dateNow
            });
            $('#start_time').val('06:30 pm');
            /*$('#datetimepicker2').datetimepicker({
              format: 'HH:mm:ss',
              defaultDate:dateNow
            });*/
            $('#datetimepicker2').datetimepicker({
                format: 'hh:mm a',
                //defaultDate:dateNow
            });
            $('#end_time').val('06:30 pm');
        });
    }
</script>

<script>
    function intialFunction() {
        dateTimePicker();
        $(document).on('submit', '#ire_admin_form', function () {
            var data_limit1 = parseInt($("#level1").val());
            var data_limit2 = parseInt($("#level2").val());
            var data_limit3 = parseInt($("#level3").val());
            var data_limit4 = parseInt($("#level4").val());
            var data_limit5 = parseInt($("#level5").val());
            var limits = (data_limit1 + data_limit2 + data_limit3 + data_limit4 + data_limit5);

            if (limits > '100') {
                $("#msg").html("Please enter sum of 100 percents");
                $('#myModal').modal("show");
                return false;
                //alert("Please enter sum of 100 percents");
            } else {
                //alert("ok");
                // $.ajax({
                //       url : "ajax/ire_admin_update.php",  // create a new php page to handle ajax request
                //       type : "GET",
                //       json : false,
                //       data : {"startdate": startDate , "enddate": endDate,"veh":veh,"city":city,"data_limit1":data_limit1,"data_limit2":data_limit2,"data_limit3":data_limit3,"data_limit4":data_limit4,"data_limit5":data_limit5},
                //       success : function(data) {
                //         alert("Success");
                //           $("#msg").html(data);
                //           $('#myModal').modal("show");
                //           setTimeout(function(){// wait for 5 secs(2)
                //             location.reload(); // then reload the page.(3)
                //           }, 5000);

                //       }
                //  });
            }
        });

    }
</script>
</body>
</html>

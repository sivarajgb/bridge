<?php
//error_reporting( error_reporting() & ~E_NOTICE );
include("sidebar.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id']))) {
  header('location:logout.php');
  die();
}

?> 

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>GoBumpr Bridge</title>

  <!-- Facebook Pixel Code -->
  <script async>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '582926561860139');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"/></noscript>
  <!-- DO NOT MODIFY -->
  <!-- End Facebook Pixel Code -->
  <!-- Google Analytics Code -->
  <script async>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-67994843-2', 'auto');
    ga('send', 'pageview');

  </script>

  <style>
html{min-height:100%;}*{box-sizing:border-box;}body{color:black;background:rgb(255, 255, 255) !important;margin:0px;min-height:inherit;}[data-sidebar-overlay]{display:none;position:fixed;top:0px;bottom:0px;left:0px;opacity:0;width:100%;min-height:inherit;}.overlay{background-color:rgb(222, 214, 196);z-index:999990 !important;}aside{position:relative;height:100%;width:200px;top:0px;left:0px;background-color:rgb(236, 239, 241);box-shadow:rgba(0, 0, 0, 0.8) 0px 6px 6px 0px;z-index:999999 !important;}[data-sidebar]{display:none;position:absolute;height:100%;z-index:100;}.padding{padding:2em;}.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6{font-family:inherit;font-weight:500;line-height:1.1;color:inherit;}.h4, .h5, .h6, h4, h5, h6{margin-top:10px;margin-bottom:10px;}.h4, h4{font-size:18px;}img{border:0px;vertical-align:middle;}a{text-decoration:none;color:black;}aside a{color:rgb(0, 0, 0);font-size:16px;text-decoration:none;}.fa{display:inline-block;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;line-height:1;font-family:FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;}nav, ol{font-size:18px;margin-top:-4px;background:rgb(0, 150, 136) !important;}.container-fluid{padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto;}.breadcrumb > li{display:inline-block;}ol, ul{margin-top:0px;margin-bottom:10px;}ol ol, ol ul, ul ol, ul ul{margin-bottom:0px;}.nav{padding-left:0px;margin-bottom:0px;list-style:none;}.navbar-nav{margin:0px;float:left;}.navbar-right{margin-right:-15px;float:right !important;}.nav > li{position:relative;display:block;}.navbar-nav > li{float:left;}.dropdown{position:relative;display:inline-block;}.form-group{margin-bottom:15px;}button, input, optgroup, select, textarea{margin:0px;font-style:inherit;font-variant:inherit;font-weight:inherit;font-stretch:inherit;font-size:inherit;line-height:inherit;font-family:inherit;color:inherit;}button, select{text-transform:none;}button, input, select, textarea{font-family:inherit;font-size:inherit;line-height:inherit;}.form-control{display:block;width:100%;height:34px;padding:6px 12px;font-size:14px;line-height:1.42857;color:rgb(85, 85, 85);background-color:rgb(255, 255, 255);background-image:none;border:1px solid rgb(204, 204, 204);border-radius:4px;box-shadow:rgba(0, 0, 0, 0.075) 0px 1px 1px inset;transition:border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;}.navbar-fixed-bottom, .navbar-fixed-top{position:fixed;right:0px;left:0px;z-index:1030;border-radius:0px;}.navbar-fixed-top{top:0px;border-width:0px 0px 1px;}label{display:inline-block;max-width:100%;margin-bottom:5px;font-weight:700;}.datepicker{cursor:pointer;}.input-group{position:relative;display:table;border-collapse:separate;}input{line-height:normal;}.input-group .form-control{position:relative;z-index:2;float:left;width:100%;margin-bottom:0px;}.input-group .form-control, .input-group-addon, .input-group-btn{display:table-cell;}.input-group .form-control:first-child, .input-group-addon:first-child, .input-group-btn:first-child > .btn, .input-group-btn:first-child > .btn-group > .btn, .input-group-btn:first-child > .dropdown-toggle, .input-group-btn:last-child > .btn-group:not(:last-child) > .btn, .input-group-btn:last-child > .btn:not(:last-child):not(.dropdown-toggle){border-top-right-radius:0px;border-bottom-right-radius:0px;}.input-group.date .input-group-addon{cursor:pointer;}.btn{display:inline-block;padding:6px 12px;margin-bottom:0px;font-size:14px;font-weight:normal;line-height:1.42857;text-align:center;white-space:nowrap;vertical-align:middle;touch-action:manipulation;cursor:pointer;user-select:none;background-image:none;border:1px solid transparent;border-radius:4px;}.fade{opacity:0;transition:opacity 0.15s linear;}.modal{position:fixed;top:0px;right:0px;bottom:0px;left:0px;z-index:1050;display:none;overflow:hidden;outline:0px;}.modal.fade .modal-dialog{transition:transform 0.3s ease-out;transform:translate(0px, -25%);}.modal-dialog{position:relative;width:600px;margin:30px auto;}.modal-content{position:relative;background-color:rgb(255, 255, 255);-webkit-background-clip:padding-box;background-clip:padding-box;border:1px solid rgba(0, 0, 0, 0.2);border-radius:6px;outline:0px;box-shadow:rgba(0, 0, 0, 0.5) 0px 5px 15px;}.modal-header{padding:15px;border-bottom:1px solid rgb(229, 229, 229);}button{overflow:visible;}button, html input[type="button"], input[type="reset"], input[type="submit"]{-webkit-appearance:button;cursor:pointer;}.close{float:right;font-size:21px;font-weight:700;line-height:1;color:rgb(0, 0, 0);text-shadow:rgb(255, 255, 255) 0px 1px 0px;opacity:0.2;}button.close{-webkit-appearance:none;padding:0px;cursor:pointer;background:0px 0px;border:0px;}.modal-header .close{margin-top:-2px;}.h1, .h2, .h3, h1, h2, h3{margin-top:20px;margin-bottom:10px;}.h3, h3{font-size:24px;}.modal-title{margin:0px;line-height:1.42857;}.modal-body{position:relative;padding:15px;}p{margin:0px 0px 10px;}b, strong{font-weight:700;}div.tab{width:80%;overflow:hidden;border:1px solid rgb(224, 242, 241);background-color:rgb(224, 242, 241);}div.tab button{background-color:inherit;float:left;border:none;outline:none;cursor:pointer;padding:14px 16px;transition:0.3s;}div.tab button.active{background-color:rgb(38, 166, 154);color:rgb(255, 255, 255);}.tabcontent{width:80%;display:none;padding:6px 12px;border-right:1px solid rgb(204, 204, 204);border-bottom:1px solid rgb(204, 204, 204);border-left:1px solid rgb(204, 204, 204);border-image:initial;border-top:none;animation:fadeEffect 1s;}table{border-spacing:0px;border-collapse:collapse;background-color:transparent;}.table{width:100%;max-width:100%;margin-bottom:20px;}td, th{padding:0px;}th{text-align:left;}.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th{padding:8px;line-height:1.42857;vertical-align:top;border-top:1px solid rgb(221, 221, 221);}.table > thead > tr > th{vertical-align:bottom;border-bottom:2px solid rgb(221, 221, 221);}.table > caption + thead > tr:first-child > td, .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table > thead:first-child > tr:first-child > th{border-top:0px;}.table-striped > tbody > tr:nth-of-type(2n+1){background-color:rgb(249, 249, 249);}div.tab2{width:94%;overflow:hidden;border:1px solid rgb(224, 242, 241);background-color:rgb(224, 242, 241);}div.tab2 button{background-color:inherit;float:left;border:none;outline:none;cursor:pointer;padding:14px 16px;transition:0.3s;}div.tab2 button.active{background-color:rgb(38, 166, 154);color:rgb(255, 255, 255);}.tabcontent2{width:94%;display:none;padding:6px 12px;border-right:1px solid rgb(204, 204, 204);border-bottom:1px solid rgb(204, 204, 204);border-left:1px solid rgb(204, 204, 204);border-image:initial;border-top:none;animation:fadeEffect 1s;}div.tab1{width:94%;overflow:hidden;border:1px solid rgb(224, 242, 241);background-color:rgb(224, 242, 241);}div.tab1 button{background-color:inherit;float:left;border:none;outline:none;cursor:pointer;padding:14px 16px;transition:0.3s;}div.tab1 button.active{background-color:rgb(38, 166, 154);color:rgb(255, 255, 255);}.tabcontent1{width:94%;display:none;padding:6px 12px;border-right:1px solid rgb(204, 204, 204);border-bottom:1px solid rgb(204, 204, 204);border-left:1px solid rgb(204, 204, 204);border-image:initial;border-top:none;animation:fadeEffect 1s;}.uil-default-css{position:relative;background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(1){animation:uil-default-anim 1s linear -0.5s infinite;}.uil-default-css > div:nth-of-type(2){animation:uil-default-anim 1s linear -0.416667s infinite;}.uil-default-css > div:nth-of-type(3){animation:uil-default-anim 1s linear -0.333333s infinite;}.uil-default-css > div:nth-of-type(4){animation:uil-default-anim 1s linear -0.25s infinite;}.uil-default-css > div:nth-of-type(5){animation:uil-default-anim 1s linear -0.166667s infinite;}.uil-default-css > div:nth-of-type(6){animation:uil-default-anim 1s linear -0.0833333s infinite;}.uil-default-css > div:nth-of-type(7){animation:uil-default-anim 1s linear 0s infinite;}.uil-default-css > div:nth-of-type(8){animation:uil-default-anim 1s linear 0.0833333s infinite;}.uil-default-css > div:nth-of-type(9){animation:uil-default-anim 1s linear 0.166667s infinite;}.uil-default-css > div:nth-of-type(10){animation:uil-default-anim 1s linear 0.25s infinite;}.uil-default-css > div:nth-of-type(11){animation:uil-default-anim 1s linear 0.333333s infinite;}.uil-default-css > div:nth-of-type(12){animation:uil-default-anim 1s linear 0.416667s infinite;}
/* home page blocks */
.floating-box {
 display: inline-block;
 margin: 22px;
 padding: 13px;
 width:140px;
 height:95px;
 box-shadow: 0px 1px 1px 0px rgba(0,0,0,0.2);
  font-size: 18px;
}
.card-color{
    position: relative;
    top: -14px;
    left: -13px;
    width: 140px;
    height: 6px;
    box-shadow: 0px 1px 1px 0px rgba(0,0,0,0.2);    
    border-radius: 2px 2px 0 0;
}
 .datepicker {
    cursor:pointer;
  }
.datepicker:before {
content: '';
display: inline-block;
border-left: 7px solid transparent;
border-right: 7px solid transparent;
border-top: 7px solid #ccc;
border-bottom-color: transparent !important;
position: absolute;
top: -7px;
left: 190px;  // I made a change here 
}
/* Style the tab */
div.tab {
    align:center;
    width:80%;
    overflow: hidden;
    border: 1px solid #E0F2F1;
    background-color: #E0F2F1;
}

/* Style the buttons inside the tab */
div.tab button {

    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
}

/* Change background color of buttons on hover */
div.tab button:hover {
    background-color: #4DB6AC;
}

/* Create an active/current tablink class */
div.tab button.active {
    background-color: #26A69A;
    color:#fff;
}

/* Style the tab content */
.tabcontent {
    align:center;
    width:80%;
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}

.tabcontent {
    -webkit-animation: fadeEffect 1s;
    animation: fadeEffect 1s; /* Fading effect takes 1 second */
}
/* nested tabs  */
/* Style the inside tab */
div.tab1 {
    align:center;
    width:94%;
    overflow: hidden;
    border: 1px solid #E0F2F1;
    background-color: #E0F2F1;
}

/* Style the buttons inside the tab */
div.tab1 button {

    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
}

/* Change background color of buttons on hover */
div.tab1 button:hover {
    background-color: #4DB6AC;
}

/* Create an active/current tablink class */
div.tab1 button.active {
    background-color: #26A69A;
    color:#fff;
}

/* Style the tab content */
.tabcontent1 {
    align:center;
    width:94%;
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}

.tabcontent1 {
    -webkit-animation: fadeEffect 1s;
    animation: fadeEffect 1s; /* Fading effect takes 1 second */
}
/* nested tabs in services tab */
/* Style the inside tab */
div.tab2 {
    align:center;
    width:94%;
    overflow: hidden;
    border: 1px solid #E0F2F1;
    background-color: #E0F2F1;
}

/* Style the buttons inside the tab */
div.tab2 button {

    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
}

/* Change background color of buttons on hover */
div.tab2 button:hover {
    background-color: #4DB6AC;
}

/* Create an active/current tablink class */
div.tab2 button.active {
    background-color: #26A69A;
    color:#fff;
}

/* Style the tab content */
.tabcontent2 {
    align:center;
    width:94%;
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}

.tabcontent2 {
    -webkit-animation: fadeEffect 1s;
    animation: fadeEffect 1s; /* Fading effect takes 1 second */
}

@-webkit-keyframes fadeEffect {
    from {opacity: 0;}
    to {opacity: 1;}
}

@keyframes fadeEffect {
    from {opacity: 0;}
    to {opacity: 1;}
}
.label {
    line-height: 2 !important;
}
.bootstrap-tagsinput{
  display: inline-flex !important;
}
.label-info{
  background-color: #4DB6AC !important;
}
.nav-tabs>li.active>a, 
.nav-tabs>li.active>a:hover, 
.nav-tabs>li.active>a:focus {
    background-color: #25a69a !important;
    color: white !important;
}
th.tablesorter-header{
cursor:pointer;
}
</style>
</head>
<body>
<?php include_once("header.php"); ?>

<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>
<div id="div1" class="navbar-fixed-top" style="margin-top:50px;padding-left:25px;background-color:#fff;padding-top:15px;" align="center" >
  <div class="form-group" style="float:left;width:90px; padding:8px;font-size:17px;">
  <label>From : </label>
  </div>

   <!-- start date -->
    <div class="form-group" style="float:left;width:150px; padding:5px;" title="Start Date" id="stdt">
       <input class="form-control datepicker " data-date-format='dd-mm-yyyy' type="text" id="start_date" name="start_date">
   </div>
   <div class="form-group" style="float:left;width:150px; padding:5px;" title="Start Time">
     <div class='input-group date' id='datetimepicker1'>
          <input type='text' class="form-control datepicker" name="start_time" id="start_time"/>
          <span class="input-group-addon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
      </div>
  </div>
 <div class="form-group" style="float:left;width:90px; padding:8px;font-size:17px;">
   <label>To : </label>
 </div>
 <!-- end date -->
  <div class="form-group" style="float:left;width:150px; padding:5px;" title="End Date" id="endt">
       <input class="form-control datepicker " data-date-format='dd-mm-yyyy' type="text" id="end_date" name="end_date">
  </div>
  <div class="form-group" style="float:left;width:150px; padding:5px;" title="End Time">
    <div class='input-group date' id='datetimepicker2'>
          <input type='text' class="form-control" name="end_time" id="end_time"/>
          <span class="input-group-addon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
    </div>
  </div>

  <div class="form-group" style="float:left;width:100px; padding:5px;" title="Filter Out">
        <button class="btn btn-md" id="submit" style="background-color:#26A69A;color:#fff;">Go!</button>
  </div>
  <div class="form-group" style="float:left;width:200px; padding:5px;" title="Help">
        <button class="btn btn-md" id="help" data-toggle="modal" data-target="#myModal_help" style="background-color:#26A69A;color:#fff;"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Help</button>
  </div>
  
</div>


<!-- select box -->
  <div class="navbar-fixed-top" style=" margin-top:110px; padding-top: 30px; padding-bottom:10px; background-color:#fff;">
    <!-- source filter -->
    <div class=" col-sm-2 col-lg-1" style="margin-left: 240px;">
      <div  class="floating-box1" >
        <div style="max-width:120px;">
            <select id="vehicle" name="vehicle" class="form-control" style="width:150px;">
              <option value="all" selected>All Vehicles</option>
              <option value="2w">2 Wheeler</option>
              <option value="4w">4 Wheeler</option>
            </select>
        </div>
      </div>
       </div>


    <!-- vehicle filter -->
    <div class=" col-sm-1 col-lg-1" style="margin-left: 52px;">
      <div  class="floating-box1" >

            <div style="max-width:120px;">
            <select id="master_service" name="master_service" class="form-control" style="width:150px;">
              
            </select>
        </div>
      </div>
    </div>

    <div class=" col-sm-1 col-lg-1" style="margin-left: 52px;">
      <div  class="floating-box1" >
        <div style="max-width:120px;">
            <select id="bservice" name="bservice" class="form-control" style="width:150px;">
             
            </select>
        </div>
      </div>
    </div>

    <div class=" col-sm-1 col-lg-1" style="margin-left: 62px;">
      <div  class="floating-box1" >
        <div style="max-width:120px;">
             <select id="person" name="person" class="form-control" style="width:150px;">

             
            </select>
        </div>
      </div>
    </div>

    <div class=" col-sm-1 col-lg-1" style="margin-left: 72px;">
      <div  class="floating-box1" >
        <div style="max-width:120px;">
            <select id="bsource" name="bsource" class="form-control" style="width:150px;">
              <option value="all" selected>All Bookings</option>
              <?php 
              $sql_sources = "SELECT user_source FROM user_source_tbl WHERE flag='0' ORDER BY  user_source ASC";
              $res_sources = mysqli_query($conn,$sql_sources);
              while($row_sources = mysqli_fetch_object($res_sources)){
                $source_name = $row_sources->user_source;
                ?>
                <option value="<?php echo $source_name; ?>"><?php echo $source_name; ?></option>
                <?php
              }
              ?>
            </select>
        </div>
      </div>
    </div>

   
  
    </div>
  </div>
<!--<img src="images/icon-testing.png" width="40" height="40" alt="Beta Testing"/> -->

</div>

<!-- Modal -->
<div class="modal fade" id="myModal_help" role="dialog" >
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Conversion Panel</h3>
      </div>
      <div class="modal-body">
      <p style="font-size:16px;color:#4E342E;"><i class="fa fa-ravelry" aria-hidden="true" style="color:#BCAAA4;"></i> The Bridge conversion panel shows you the life cycle of Leads in three different views. Follow the description below <i class="fa fa-arrow-down" aria-hidden="true" style="color:#FFD54F;"></i> to understand the terminology used in this panel:</p>
      <ul>
      <li><p> <span style="color:#004D40;"><strong>Leads</strong></span> : <span style="color:#00838F;">Newly acquired bookings(from Facebook, Google, App, Website...)</span></p></li>
      <li><p> <span style="color:#004D40;"><strong>GoAxled</strong></span> : <span style="color:#00838F;">Leads which are pushed to Axle.</span></p></li>
      <li><p> <span style="color:#004D40;"><strong>Followups</strong></span> : <span style="color:#00838F;">Customers that require a call back/RNR.</span></p></li>
      <li><p> <span style="color:#004D40;"><strong>Cancelled</strong></span> : <span style="color:#00838F;">Includes test bookings, duplicates & Enquiries.</span></p></li>
      <li><p> <span style="color:#004D40;"><strong>Others</strong></span> : <span style="color:#00838F;">If the customer is not responding to the calls or he is not interested,the boooking is pushed to Others bucket.</span></p></li>
      <li><p> <span style="color:#004D40;"><strong>Idle</strong></span> : <span style="color:#00838F;">Bookings on which no action has been taken or left idle.</span></p></li>
      </ul>
      <p style="color:#4E342E;"><i class="fa fa-ravelry" aria-hidden="true" style="color:#BCAAA4;"></i> The Non-Conversion tab gives a detailed view on the reason behind each non converted booking.</p>
      <p style="color:#4E342E;"><i class="fa fa-ravelry" aria-hidden="true" style="color:#BCAAA4;"></i> Click on any count value to get the list of bookings corresponding to that particular value.</p>
      <p style="color:#4E342E;"><i class="fa fa-ravelry" aria-hidden="true" style="color:#BCAAA4;"></i> In the popup, click on a booking id to navigate to the user history page.</p>
      </div> <!-- modal body -->
    </div> <!-- modal content -->
  </div>  <!-- modal dailog -->
</div>  <!-- modal -->

<ul class="nav nav-tabs" style="margin-top: 10%;margin-left: 4%;margin-right: 6%;height: 42px;">
  <li class="active"><a data-toggle="tab" href="#Service">Service</a></li>
  <li><a data-toggle="tab" href="#Person">Person</a></li>
  <li><a data-toggle="tab" href="#Source">Source</a></li>
  <li><a data-toggle="tab" href="#NonConversion">NonConversion</a></li>
  <div style="float:right;width:20%; padding:9px;font-size:17px;" title="Total Count">
    <label>Total : </label>&nbsp;&nbsp;&nbsp;<span id="count"></span>
  </div>
</ul>
<div class="tab-content" style="margin-left:4%;">
    <div id="Service" class="tab-pane fade in active">
    
    <div class="tab2" style="margin-top:10px;max-width:100%;">
        <button class="tablinks2" onclick="openTab2(event, 'all_service')" id="alltab_service"> All </button>
        <button class="tablinks2" onclick="openTab2(event, '2w_service')" id="2wtab_service"> 2W </button>
        <button class="tablinks2" onclick="openTab2(event, '4w_service')" id="4wtab_service"> 4W </button>
    </div>
    <!-- ----------- all------------------- -->
    <div id="all_service" class="tabcontent2" style="max-width:100%;">
    <?php
    if($flag == '1'){ ?>
    <button id="service_excel_all" class="btn btn-md" style="margin-top:1%;margin-left:88%;float-left;background-color:#00695C;color:#fafafa;"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;&nbsp;Export</button>
    <?php } ?>
    <div align="center">
      <div style="margin-top:10px;max-width:95%;overflow-y:auto;">
        <table id="service_table_all"  class="table table-striped table-hover tablesorter" >
          <thead style="background-color: #B2DFDB;">
            <th></th>
            <th style='text-align:center'>Type <i class="fa fa-sort" aria-hidden="true"></i></th>
            <th style='text-align:center'>Leads <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>GoAxled <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>EndConversion <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>Followups <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>Cancelled <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>Others <i class="fa fa-sort" aria-hidden="true"></th>       
            <th style='text-align:center'>Idle <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>Duplicate <i class="fa fa-sort" aria-hidden="true"></th>
          </thead>
          <tbody id="tbodySer1">
          </tbody>
          <tbody class="avoid-sortSer1">
          </tbody>
        </table>
      </div>
    </div>
    </div>
    <!-- ----------- all------------------- -->
    <!-- ----------- 2 wheelers------------------- -->
    <div id="2w_service" class="tabcontent2" style="max-width:100%;">
    <?php
    if($flag == '1'){ ?>
    <button id="service_excel_2w" class="btn btn-md" style="margin-top:1%;margin-left:88%;float-left;background-color:#00695C;color:#fafafa;"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;&nbsp;Export</button>
    <?php } ?>
    <div align="center">
      <div style="margin-top:10px;max-width:95%;overflow-y:auto;">
        <table id="service_table_2w"  class="table table-striped table-hover tablesorter" >
          <thead style="background-color: #B2DFDB;">
            <th></th>
            <th style='text-align:center'>Leads <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>GoAxled <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>EndConversion <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>Followups <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>Cancelled <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>Others <i class="fa fa-sort" aria-hidden="true"></th>       
            <th style='text-align:center'>Idle <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>Duplicate <i class="fa fa-sort" aria-hidden="true"></th>
          </thead>
          <tbody id="tbodySer2">
          </tbody>
          <tbody class="avoid-sortSer2">
          </tbody>
        </table>
      </div>
    </div>
    </div>
    <!-- ----------- 2w------------------- -->
    <!-- ----------- 4 wheelers------------------- -->
    <div id="4w_service" class="tabcontent2" style="max-width:100%;">
    <?php
    if($flag == '1'){ ?>
    <button id="service_excel_4w" class="btn btn-md" style="margin-top:1%;margin-left:88%;float-left;background-color:#00695C;color:#fafafa;"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;&nbsp;Export</button>
    <?php } ?>
    <div align="center">
      <div style="margin-top:10px;max-width:95%;overflow-y:auto;">
         <table id="service_table_4w"  class="table table-striped table-hover tablesorter" >
          <thead style="background-color: #B2DFDB;">
            <th></th>
              <th style='text-align:center'>Leads <i class="fa fa-sort" aria-hidden="true"></th>
              <th style='text-align:center'>GoAxled <i class="fa fa-sort" aria-hidden="true"></th>
              <th style='text-align:center'>EndConversion <i class="fa fa-sort" aria-hidden="true"></th>
              <th style='text-align:center'>Followups <i class="fa fa-sort" aria-hidden="true"></th>
              <th style='text-align:center'>Cancelled <i class="fa fa-sort" aria-hidden="true"></th>
              <th style='text-align:center'>Others <i class="fa fa-sort" aria-hidden="true"></th>       
              <th style='text-align:center'>Idle <i class="fa fa-sort" aria-hidden="true"></th>
              <th style='text-align:center'>Duplicate <i class="fa fa-sort" aria-hidden="true"></th>
          </thead>
          <tbody id="tbodySer3">
          </tbody>
          <tbody class="avoid-sortSer3">
          </tbody>
        </table>
      </div>
    </div>
    </div>
    <!-- ----------- 4 wheelers------------------- -->
    
    <!-- loading -->
    <div id="loading1" style="display:none; margin-top:140px;" align="center">
      <div class='uil-default-css' style='transform:scale(0.58);'>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
    </div>
    </div>
    </div>
    <div id="Person" class="tab-pane fade">
    <?php
    if($flag == '1'){ ?>
        <button id="person_excel" class="btn btn-md" style="margin-top:2%;margin-left:83%;float-left;background-color:#00695C;color:#fafafa;"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;&nbsp;Export</button>
        <?php } ?>
    <div style="margin-top:20px;max-width:94%;overflow-y:auto;">
      <table id="person_table" class="table table-striped table-hover tablesorter" >
        <thead style="background-color: #B2DFDB;">
          <th></th>
          <th style='text-align:center'>Leads <i class="fa fa-sort" aria-hidden="true"></th>
          <th style='text-align:center'>GoAxled <i class="fa fa-sort" aria-hidden="true"></th>
          <th style='text-align:center'>EndConversion <i class="fa fa-sort" aria-hidden="true"></th>
          <th style='text-align:center'>Followups <i class="fa fa-sort" aria-hidden="true"></th>
          <th style='text-align:center'>Cancelled <i class="fa fa-sort" aria-hidden="true"></th>
          <th style='text-align:center'>Others <i class="fa fa-sort" aria-hidden="true"></th>       
          <th style='text-align:center'>Idle <i class="fa fa-sort" aria-hidden="true"></th>
          <th style='text-align:center'>Duplicate <i class="fa fa-sort" aria-hidden="true"></th>
        </thead>
        <tbody id="tbodyPerson">
        </tbody>
        <tbody class="avoid-sortperson">
        </tbody>
      </table>
    </div>
    
    <!-- loading -->
    <div id="loading2" style="display:none; margin-top:140px;" align="center">
      <div class='uil-default-css' style='transform:scale(0.58);'>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
    </div>
    </div>
    </div>
    <div id="Source" class="tab-pane fade">
    <div class="tab1" style="margin-top:10px;">
      <button class="tablinks1" onclick="openTab1(event, 'all_source')" id="alltab_source"> All </button>
      <button class="tablinks1" onclick="openTab1(event, '2w_source')" id="2wtab_source"> 2W </button>
      <button class="tablinks1" onclick="openTab1(event, '4w_source')" id="4wtab_source"> 4W </button>
    </div>
    <!-- ----------- all------------------- -->
    <div id="all_source" class="tabcontent1">
    <?php
        if($flag == '1'){ ?>
        <button id="source_excel_all" class="btn btn-md" style="margin-top:10px;margin-left:86%;float-left;background-color:#00695C;color:#fafafa;"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;&nbsp;Export</button>
        <?php } ?>
    <div align="center">
      <div style="margin-top:10px;max-width:95%;overflow-y:auto;">
        <table id="source_table_all" class="table table-striped table-hover tablesorter" >
          <thead style="background-color: #B2DFDB;">
            <th style='text-align:center'></th>
            <th style='text-align:center'>Leads <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>GoAxled <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>EndConversion <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>Followups <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>Cancelled <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>Others <i class="fa fa-sort" aria-hidden="true"></th>       
            <th style='text-align:center'>Idle <i class="fa fa-sort" aria-hidden="true"></th>
                        <th style='text-align:center'>Duplicate <i class="fa fa-sort" aria-hidden="true"></th>
          </thead>
          <tbody id="tbodySrc1">
          </tbody>
          <tbody class="avoid-sortsrc1">
        </tbody>
        </table>
      </div>
    </div>
    </div>
    <!-- ----------- all------------------- -->
    <!-- ----------- 2wheelers------------------- -->
    <div id="2w_source" class="tabcontent1">
    <?php
        if($flag == '1'){ ?>
        <button id="source_excel_2w" class="btn btn-md" style="margin-top:10px;margin-left:86%;float-left;background-color:#00695C;color:#fafafa;"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;&nbsp;Export</button>
        <?php } ?>
    <div align="center">
      <div style="margin-top:10px;max-width:95%;overflow-y:auto;">
        <table id="source_table_2w" class="table table-striped table-hover tablesorter" >
          <thead style="background-color: #B2DFDB;">
            <th style='text-align:center'></th>
            <th style='text-align:center'>Leads <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>GoAxled <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>EndConversion <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>Followups <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>Cancelled <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>Others <i class="fa fa-sort" aria-hidden="true"></th>       
            <th style='text-align:center'>Idle <i class="fa fa-sort" aria-hidden="true"></th>
                        <th style='text-align:center'>Duplicate <i class="fa fa-sort" aria-hidden="true"></th>
          </thead>
          <tbody id="tbodySrc2">
          </tbody>
          <tbody class="avoid-sortsrc2">
        </tbody>          
        </table>
      </div>
    </div>
    </div>
    <!-- ----------- 2wheelers------------------- -->
    <!-- ----------- 4wheelers------------------- -->
    <div id="4w_source" class="tabcontent1">
    <?php
        if($flag == '1'){ ?>
        <button id="source_excel_4w" class="btn btn-md" style="margin-top:10px;margin-left:86%;float-left;background-color:#00695C;color:#fafafa;"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;&nbsp;Export</button>
        <?php } ?>
    <div align="center">
      <div style="margin-top:10px;max-width:95%;overflow-y:auto;">
        <table id="source_table_4w" class="table table-striped table-hover tablesorter" >
          <thead style="background-color: #B2DFDB;">
            <th style='text-align:center'></th>
            <th style='text-align:center'>Leads <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>GoAxled <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>EndConversion <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>Followups <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>Cancelled <i class="fa fa-sort" aria-hidden="true"></th>
            <th style='text-align:center'>Others <i class="fa fa-sort" aria-hidden="true"></th>       
            <th style='text-align:center'>Idle <i class="fa fa-sort" aria-hidden="true"></th>
                        <th style='text-align:center'>Duplicate <i class="fa fa-sort" aria-hidden="true"></th>
          </thead>
          <tbody id="tbodySrc3">
          </tbody>
          <tbody class="avoid-sortsrc3">
        </tbody>
        </table>
      </div>
    </div>
    </div>
    <!-- ----------- 4wheelers------------------- -->
    <!-- loading -->
    <div id="loading3" style="display:none; margin-top:140px;" align="center">
      <div class='uil-default-css' style='transform:scale(0.58);'>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
    </div>
    </div>
    </div>
    <div id="NonConversion" class="tab-pane fade">
    <?php
    if($flag == '1'){ ?>
    <button id="nonconversion_excel" class="btn btn-md" style="margin-top:10px;margin-left:76%;float-left;background-color:#00695C;color:#fafafa;"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;&nbsp;Export</button>
    <?php } ?>
    <div style="margin-top:20px;max-width:95%;overflow-y:auto;">
       <table id="nonconversion_table" class="table table-striped table-hover tablesorter" >
        <thead style="background-color: #B2DFDB;">
        <th></th>
          <th style='text-align:left;width:25%;word-break: break-word;'>2Wheelers<i class="fa fa-sort" aria-hidden="true"> </th>
          <th style='text-align:left;width:25%;word-break: break-word;'>4Wheelers<i class="fa fa-sort" aria-hidden="true"></th>
        </thead>
        <tr style='background-color:#E0F2F1;'><td style='font-size:18px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cancelled Bookings</td><td></td><td></td></tr>
        <tbody id="tbodyNonConv">
        </tbody>
      </table>
      <table id="nonconversion_table" class="table table-striped table-hover tablesorter" >
        <thead style="background-color: #B2DFDB;">
        <th></th>
          <th style='text-align:left;width:25%;word-break: break-word;'>2Wheelers<i class="fa fa-sort" aria-hidden="true"></th>
          <th style='text-align:left;width:25%;word-break: break-word;'>4Wheelers<i class="fa fa-sort" aria-hidden="true"></th>
        </thead>
        <tr style='background-color:#E0F2F1;'><td style='font-size:18px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other Bookings</td><td></td><td></td></tr>
        <tbody id="tbodyNonConvOther">
        </tbody>
      </table>
    </div>
    <!-- loading -->
    <div id="loading4" style="display:none; margin-top:140px;" align="center">
      <div class='uil-default-css' style='transform:scale(0.58);'>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
      <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'>
      </div>
    </div>
    </div>
    </div>
</div>



<noscript id="async-styles">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
  <link rel="stylesheet" href="css/bootstrap-datetimepicker.css"> 
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
  <link rel="stylesheet" href="css/bootstrap-datetimepicker-standalone.css">
    
   
  </noscript>
  <script> // The CSS mentioned in "async-styles" will be loaded and styles will be applied.
      var loadDeferredStyles = function() {
          var addStylesNode = document.getElementById("async-styles");
          var replacement = document.createElement("div");
          replacement.innerHTML = addStylesNode.textContent;
          document.body.appendChild(replacement)
          addStylesNode.parentElement.removeChild(addStylesNode);
      };
      var raf = requestAnimationFrame || mozRequestAnimationFrame ||
          webkitRequestAnimationFrame || msRequestAnimationFrame;
      if (raf) raf(function() {
          window.setTimeout(loadDeferredStyles, 0);
      });
      else window.addEventListener('load', loadDeferredStyles);
  </script>
  
  <script async src="js/jquery-3.2.1.min.js" onLoad="loadOtherScripts();"></script>

  <script>
    function loadScript(src) {
        return new Promise(function (resolve, reject) {
            var s;
            s = document.createElement('script');
            s.src = src;
            s.onload = resolve;
            s.onerror = reject;
            document.head.appendChild(s);
        });
    }

    function loadOtherScripts() {
      loadScript('//cdn.jsdelivr.net/momentjs/latest/moment.min.js')
      .then(function() {
      Promise.all([ loadScript('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'),loadScript('js/bootstrap-datetimepicker.min.js'),loadScript('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'),loadScript('js/sidebar.js'),loadScript('js/jquery.table2excel.js'),loadScript('https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.9.1/jquery.tablesorter.min.js')]).then(function () {
    // console.log('scripts are loaded');
      dateTimePicker();
      datePicker();
      intialFunction();
    tableSort();
      }).catch(function (error) {
      console.log('some error!' + error)
      })
      }).catch(function (error) {
     console.log('Moment call error!' + error)
      })
      }
  </script>

  <!-- date range picker -->
  <script>
  function dateTimePicker(){
    var date = new Date();
    date.setDate(date.getDate());
    var date2 = new Date();
    date2.setDate(date2.getDate()-1);
    $('#start_date').datepicker({
        endDate: date,
        autoclose: true,
        orientation: 'auto'
    });
    $('#start_date').datepicker('setDate', date2);
    $('#end_date').datepicker({
        endDate: date,
        autoclose: true,
        orientation: 'auto'
    });
    $('#end_date').datepicker('setDate', 'today');
    getmasterservice();
  }

  </script>

  
  <!-- masterservice filter -->
  <script>
   
  function getmasterservice(){
    //console.log("Get people");

    var city = $('#city').val();
    var cluster = $('#cluster').val();
    var vehicle = $('#vehicle').val();
    //console.log(city);
          $('#master_service').empty();
          $.ajax({
                url : "ajax/get_masterservice_data.php",  // create a new php page to handle ajax request
                type : "GET", json : false,
                data : {"city":city , "cluster" : cluster, "vehicle" : vehicle},
                success : function(data) {
              $('#master_service').append(data);  
              //console.log(data); 
              getservice();
              //intialFunction();
              
            },
              error: function(xhr, ajaxOptions, thrownError) {
              //  alert(xhr.status + " "+ thrownError);
            }
          });
            
  }
   
  </script>
 <!-- service filter -->
  <script>
  function getservice(){
    //console.log("Get people");
    var city = $('#city').val();
    var cluster = $('#cluster').val();
    var vehicle = $('#vehicle').val();
    var master_service = $('#master_service').val();
    //console.log(city);
          $('#bservice').empty();
          $.ajax({
                url : "ajax/get_service_data.php",  // create a new php page to handle ajax request
                type : "GET", json : false,
                data : {"city":city , "cluster" : cluster, "vehicle" : vehicle, "master_service" :master_service},
                success : function(data) {
              $('#bservice').append(data);  
              //console.log(data); 
              getpeople();
              //intialFunction();
              
            },
              error: function(xhr, ajaxOptions, thrownError) {
              //  alert(xhr.status + " "+ thrownError);
            }
          });
  }
  </script>
  <script>
  function getpeople(){
    //console.log("Get people");
    var city = $('#city').val();
    var cluster = $('#cluster').val();
    //console.log(city);
          $('#person').empty();
          $.ajax({
                url : "ajax/get_crm_people.php",  // create a new php page to handle ajax request
                type : "GET", json : false,
                data : {"city":city , "cluster" : cluster},
                success : function(data) {
              $('#person').append(data);  
              //console.log(data); 
              defaultview();
              //intialFunction();
              
            },
              error: function(xhr, ajaxOptions, thrownError) {
              //  alert(xhr.status + " "+ thrownError);
            }
          });
  }
  </script>
  <!-- Table sorter -->
  <script>
  function tableSort(){
    $(document).ready(function(){
      $('.table').tablesorter( {sortList: [0,0]} );
    });
  }

  </script>
  <script>
  function datePicker(){
      $(function() {
      var dateNow = new Date();
    /* $('#datetimepicker1').datetimepicker({ format: 'HH:mm:ss',step:15}).val("00:00:00");*/
      $('#datetimepicker1').datetimepicker({
        format: 'hh:mm a',
        //defaultDate: dateNow
      });
    $('#start_time').val('06:30 pm');
      /*$('#datetimepicker2').datetimepicker({
        format: 'HH:mm:ss',
        defaultDate:dateNow
      });*/
      $('#datetimepicker2').datetimepicker({
        format: 'hh:mm a',
        //defaultDate:dateNow
      });
      $('#end_time').val('06:30 pm');
    });
  }
  </script>
  <script>
  function openTab(evt, vehicle) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(vehicle).style.display = "block";
    evt.currentTarget.className += " active";
  }
  function openTab1(evt, vehicle) {
    // Declare all variables
    var i, tabcontent1, tablinks1;

    // Get all elements with class="tabcontent" and hide them
    tabcontent1 = document.getElementsByClassName("tabcontent1");
    for (i = 0; i < tabcontent1.length; i++) {
      tabcontent1[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks1 = document.getElementsByClassName("tablinks1");
    for (i = 0; i < tablinks1.length; i++) {
      tablinks1[i].className = tablinks1[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(vehicle).style.display = "block";
    evt.currentTarget.className += " active";
  }
  function openTab2(evt, vehicle2) {
    // Declare all variables
    var i, tabcontent2, tablinks2;

    // Get all elements with class="tabcontent" and hide them
    tabcontent2 = document.getElementsByClassName("tabcontent2");
    for (i = 0; i < tabcontent2.length; i++) {
      tabcontent2[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks2 = document.getElementsByClassName("tablinks2");
    for (i = 0; i < tablinks2.length; i++) {
      tablinks2[i].className = tablinks2[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(vehicle2).style.display = "block";
    evt.currentTarget.className += " active";
  }

function defaultview(){
  var startDate = $('#start_date').val();
  var endDate = $('#end_date').val();
  var startTime = $('#start_time').val();
  var endTime = $('#end_time').val();
  var city = $('#city').val();
  var bservice = $('#bservice').val();
  var vehicle = $('#vehicle').val();
  var master_service = $('#master_service').val();
  var person = $('#person').val();
  var bsource = $('#bsource').val();
  
  
  //Make AJAX request, using the selected value as the POST
  
  $.ajax({
        url : "ajax/conversion_panel_view.php",  // create a new php page to handle ajax request
        type : "POST", //json : false,
    dataType : "json",
        data : {"startdate": startDate , "enddate": endDate , "starttime": startTime , "endtime": endTime, "city":city, "target": target,"bservice":bservice,"vehicle":vehicle,"master_service":master_service,"person":person,"bsource":bsource },
        success : function(data) {
    
    function displayInfo11(item,index,table){     
    $(table).append(item['tr']).trigger("update");
    $('.avoid-sortSer1').append($('table tr.avoid-sortSer1').html());
    }
    
    function displayInfo12(item,index,table){     
    $(table).append(item['tr']).trigger("update");
    $('.avoid-sortSer2').append($('table tr.avoid-sortSer2').html());
    }
    
    function displayInfo13(item,index,table){     
    $(table).append(item['tr']).trigger("update");
    $('.avoid-sortSer3').append($('table tr.avoid-sortSer3').html());
    }
    
  switch(target)
  {
    case '#Service' : $('#view1').show();
              $("#loading1").hide();
              //$('#view1').html(data);
              document.getElementById("alltab_service").click();
              document.getElementById("alltab_source").click();
              $.map(data.table1, displayInfo11,'#tbodySer1');
              $.map(data.table2, displayInfo12,'#tbodySer2');
              $.map(data.table3, displayInfo13,'#tbodySer3');
              $('#count').html(data.count); 
              //console.log(data.count);        
              data = '';
              console.log(data);
              view1Data = 1;
              break;
    case '#Person' : $('#view2').show();
              $("#loading2").hide();
              //$('#view2').html(data);
              function displayInfo2(item,index,table){
              $(table).append(item['tr']).trigger("update");
              $('.avoid-sortperson').append($('table tr.avoid-sortperson').html());
              }
              $.map(data.person_data, displayInfo2,'#tbodyPerson');
              $('#count').html(data.count);
              view2Data = 1;
              break;
    case '#Source' : $('#view3').show();
              $("#loading3").hide();
              //$('#view3').html(data);
              function displayInfo31(item,index,table){
              $(table).append(item['tr']).trigger("update");
              $('.avoid-sortsrc1').append($('table tr.avoid-sortsrc1').html());
              //console.log(item);
              }
              function displayInfo32(item,index,table){
              $(table).append(item['tr']).trigger("update");
              $('.avoid-sortsrc2').append($('table tr.avoid-sortsrc2').html());
              }
              function displayInfo33(item,index,table){
              $(table).append(item['tr']).trigger("update");
              $('.avoid-sortsrc3').append($('table tr.avoid-sortsrc3').html());
              }
              $.map(data.src_table1, displayInfo31,'#tbodySrc1');
              $.map(data.src_table2, displayInfo32,'#tbodySrc2');
              $.map(data.src_table3, displayInfo33,'#tbodySrc3');
              $('#count').html(data.count);
              view3Data = 1;
              break;
    case '#NonConversion' : $('#view4').show();
              $("#loading4").hide();
              //$('#view4').html(data);
              //console.log(data.cancel);
              function displayInfo4(item,index,table){
              $(table).append(item).trigger("update");
              }
              function displayInfo5(item,index,table){
              $(table).append(item).trigger("update");
              }
              $.map(data.cancel,displayInfo4,'#tbodyNonConv');
              $.map(data.other,displayInfo5,'#tbodyNonConvOther');
              $('#count').html(data.count);
              view4Data = 1;
              break;
  }
    //totalcount();
  },
      error: function(xhr, ajaxOptions, thrownError) {
        // alert(xhr.status + " "+ thrownError);
      }
    });
}
</script>
  <!-- automatic refresh -->
<script>
var target = "#Service";
var view1Data = 0;
var view2Data = 0;
var view3Data = 0;
var view4Data = 0;
function intialFunction(){
  $(document).ready( function (){

    $('#city').show();
    $("#loading1").show();
    $("#loading2").show();
    $("#loading3").show();
    $("#loading4").show();
    $("#loading").show();
   
    

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    target = $(e.target).attr("href") // activated tab
    if(target == "#Service" && view1Data == 0)
    {
      $('#tbodySer1').empty();
      $('#tbodySer2').empty();
      $('#tbodySer3').empty();
      defaultview();
    }
    if(target == "#Person" && view2Data == 0)
    {
      $('#tbodyPerson').empty();
      defaultview();
    }
    if(target == "#Source" && view3Data == 0)
    {
      $('#tbodySrc1').empty();
      $('#tbodySrc2').empty();
      $('#tbodySrc3').empty();
      defaultview();
    }
    if(target == "#NonConversion" && view4Data == 0)
    {
      $('#tbodyNonConv').empty();
      $('#tbodyNonConvOther').empty();
      
      defaultview();
    }
    });
   // defaultview();

    
        $("#submit").click(function(e) {
        $("#loading1").show();
      $("#loading2").show();
      $("#loading3").show();
      $("#loading4").show();
        $("#loading").show();
      $('#tbodySer1').empty();
      $('#tbodySer2').empty();
      $('#tbodySer3').empty();
      $('#tbodyPerson').empty();
      $('#tbodySrc1').empty();
      $('#tbodySrc2').empty();
      $('#tbodySrc3').empty();
      $('#tbodyNonConv').empty();
      $('#tbodyNonConvOther').empty();
      $('.avoid-sortSer1').empty();
      $('.avoid-sortSer2').empty();
      $('.avoid-sortSer3').empty();
      $('.avoid-sortperson').empty();
      $('.avoid-sortsrc1').empty();
      $('.avoid-sortsrc2').empty();
      $('.avoid-sortsrc3').empty();
          defaultview();
        });

        $('#city').change(function(){
        $("#loading1").show();
      $("#loading2").show();
      $("#loading3").show();
      $("#loading4").show();
        $("#loading").show();
      $('#tbodySer1').empty();
      $('#tbodySer2').empty();
      $('#tbodySer3').empty();
      $('#tbodyPerson').empty();
      $('#tbodySrc1').empty();
      $('#tbodySrc2').empty();
      $('#tbodySrc3').empty();
      $('#tbodyNonConv').empty();
      $('#tbodyNonConvOther').empty();
      $('.avoid-sortSer1').empty();
      $('.avoid-sortSer2').empty();
      $('.avoid-sortSer3').empty();
      $('.avoid-sortperson').empty();
      $('.avoid-sortsrc1').empty();
      $('.avoid-sortsrc2').empty();
      $('.avoid-sortsrc3').empty();
        
         getmasterservice();
         
        });

        $("#person").on("change" , function (){
       $("#loading1").show();
      $("#loading2").show();
      $("#loading3").show();
      $("#loading4").show();
        $("#loading").show();
      $('#tbodySer1').empty();
      $('#tbodySer2').empty();
      $('#tbodySer3').empty();
      $('#tbodyPerson').empty();
      $('#tbodySrc1').empty();
      $('#tbodySrc2').empty();
      $('#tbodySrc3').empty();
      $('#tbodyNonConv').empty();
      $('#tbodyNonConvOther').empty();
      $('.avoid-sortSer1').empty();
      $('.avoid-sortSer2').empty();
      $('.avoid-sortSer3').empty();
      $('.avoid-sortperson').empty();
      $('.avoid-sortsrc1').empty();
      $('.avoid-sortsrc2').empty();
      $('.avoid-sortsrc3').empty();
        defaultview();
      });

    $("#bservice").on("change" , function (){
       $("#loading1").show();
      $("#loading2").show();
      $("#loading3").show();
      $("#loading4").show();
        $("#loading").show();
      $('#tbodySer1').empty();
      $('#tbodySer2').empty();
      $('#tbodySer3').empty();
      $('#tbodyPerson').empty();
      $('#tbodySrc1').empty();
      $('#tbodySrc2').empty();
      $('#tbodySrc3').empty();
      $('#tbodyNonConv').empty();
      $('#tbodyNonConvOther').empty();
      $('.avoid-sortSer1').empty();
      $('.avoid-sortSer2').empty();
      $('.avoid-sortSer3').empty();
      $('.avoid-sortperson').empty();
      $('.avoid-sortsrc1').empty();
      $('.avoid-sortsrc2').empty();
      $('.avoid-sortsrc3').empty();
        defaultview();
      });
    $("#master_service").on("change" , function (){
        $("#loading1").show();
      $("#loading2").show();
      $("#loading3").show();
      $("#loading4").show();
        $("#loading").show();
      $('#tbodySer1').empty();
      $('#tbodySer2').empty();
      $('#tbodySer3').empty();
      $('#tbodyPerson').empty();
      $('#tbodySrc1').empty();
      $('#tbodySrc2').empty();
      $('#tbodySrc3').empty();
      $('#tbodyNonConv').empty();
      $('#tbodyNonConvOther').empty();
      $('.avoid-sortSer1').empty();
      $('.avoid-sortSer2').empty();
      $('.avoid-sortSer3').empty();
      $('.avoid-sortperson').empty();
      $('.avoid-sortsrc1').empty();
      $('.avoid-sortsrc2').empty();
      $('.avoid-sortsrc3').empty();
        getservice();
      });
    $("#bsource").on("change" , function (){
        $("#loading1").show();
      $("#loading2").show();
      $("#loading3").show();
      $("#loading4").show();
        $("#loading").show();
      $('#tbodySer1').empty();
      $('#tbodySer2').empty();
      $('#tbodySer3').empty();
      $('#tbodyPerson').empty();
      $('#tbodySrc1').empty();
      $('#tbodySrc2').empty();
      $('#tbodySrc3').empty();
      $('#tbodyNonConv').empty();
      $('#tbodyNonConvOther').empty();
      $('.avoid-sortSer1').empty();
      $('.avoid-sortSer2').empty();
      $('.avoid-sortSer3').empty();
      $('.avoid-sortperson').empty();
      $('.avoid-sortsrc1').empty();
      $('.avoid-sortsrc2').empty();
      $('.avoid-sortsrc3').empty();
        defaultview();
      });
    $("#vehicle").on("change" , function (){
        $("#loading1").show();
      $("#loading2").show();
      $("#loading3").show();
      $("#loading4").show();
        $("#loading").show();
      $('#tbodySer1').empty();
      $('#tbodySer2').empty();
      $('#tbodySer3').empty();
      $('#tbodyPerson').empty();
      $('#tbodySrc1').empty();
      $('#tbodySrc2').empty();
      $('#tbodySrc3').empty();
      $('#tbodyNonConv').empty();
      $('#tbodyNonConvOther').empty();
      $('.avoid-sortSer1').empty();
      $('.avoid-sortSer2').empty();
      $('.avoid-sortSer3').empty();
      $('.avoid-sortperson').empty();
      $('.avoid-sortsrc1').empty();
      $('.avoid-sortsrc2').empty();
      $('.avoid-sortsrc3').empty();
        getmasterservice();
      });
   

       
      });
    $(function() {
    
    $("#service_excel_all").click(function(){
      var startDate = $('#start_date').val();
    var endDate = $('#end_date').val();
      $("#service_table_all").table2excel({
         // exclude: ".noExl",
        //name: "Excel Document Name",
        filename:  "Conversion(ServiceView) from " + startDate + " to " + endDate
      }); 
    });
     // $("#service_table_all").dataTable();
    });
   $(function() {
    
    $("#service_excel_2w").click(function(){
      var startDate = $('#start_date').val();
    var endDate = $('#end_date').val();
      $("#service_table_2w").table2excel({
         // exclude: ".noExl",
        //name: "Excel Document Name",
        filename:  "Conversion(ServiceView-2w) from " + startDate + " to " + endDate
      }); 
    });
    //$("#service_table_2w").dataTable();
    });
   $(function() {
    
    $("#service_excel_4w").click(function(){
      var startDate = $('#start_date').val();
    var endDate = $('#end_date').val();
      $("#service_table_4w").table2excel({
         // exclude: ".noExl",
        //name: "Excel Document Name",
        filename:  "Conversion(ServiceView-4w) from " + startDate + " to " + endDate
      }); 
    });
     // $("#service_table_4w").dataTable();
    });
    $(function() {
      
      $("#person_excel").click(function(){
        var startDate = $('#start_date').val();
      var endDate = $('#end_date').val();
          $("#person_table").table2excel({
             // exclude: ".noExl",
              //name: "Excel Document Name",
              filename:  "Conversion(PersonView) from " + startDate + " to " + endDate
          }); 
       });
     // $("#person_table").dataTable();
    });
     $(function() {
      
      $("#source_excel_all").click(function(){
        var startDate = $('#start_date').val();
      var endDate = $('#end_date').val();
          $("#source_table_all").table2excel({
             // exclude: ".noExl",
              //name: "Excel Document Name",
              filename:  "Conversion(SourceView) from " + startDate + " to " + endDate
          }); 
      });
     // $("#source_table_all").dataTable();
    });

 $(function() {
    
    $("#source_excel_2w").click(function(){
      var startDate = $('#start_date').val();
    var endDate = $('#end_date').val();
        $("#source_table_2w").table2excel({
           // exclude: ".noExl",
            //name: "Excel Document Name",
            filename:  "Conversion(SourceView-2w) from " + startDate + " to " + endDate
        }); 
    });
    //$("#source_table_2w").dataTable();
  });

 $(function() {
   
    $("#source_excel_4w").click(function(){
      var startDate = $('#start_date').val();
    var endDate = $('#end_date').val();
        $("#source_table_4w").table2excel({
           // exclude: ".noExl",
            //name: "Excel Document Name",
            filename:  "Conversion(SourceView-4w) from " + startDate + " to " + endDate
        }); 
    });
    //$("#source_table_4w").dataTable();
  });
  $(function() {
    
    $("#nonconversion_excel").click(function(){
      var startDate = $('#start_date').val();
    var endDate = $('#end_date').val();
        $("#nonconversion_table").table2excel({
           // exclude: ".noExl",
            //name: "Excel Document Name",
            filename:  "NonConversion from " + startDate + " to " + endDate
        }); 
    });
    //$("#nonconversion_table").dataTable();
  });
    //person modal 
    $("#myModalperson").on('hide.bs.modal', function(event){
        $("#table").addClass('body');
    });
    $("#myModalperson").on('show.bs.modal', function(event){
    $("#loadingperson").show();
    $("#tableperson").hide();
            
    var button=$(event.relatedTarget);
    loadModalperson(button);

    });
              
    function loadModalperson(button){
    var crmlogid=button.data('crmlogid');
    var startdate=button.data('startdate');
    var enddate=button.data('enddate');
    var status=button.data('status');
    var city= $('#city').val();
    var bservice = $('#bservice').val();
    var vehicle = $('#vehicle').val();
    var master_service = $('#master_service').val();
    var person = $('#person').val();
    var bsource = $('#bsource').val();

    $.ajax({
        url : "ajax/conversion_panel_data.php?tab=person",  // create a new php page to handle ajax request
        type : "POST",
        data : {"crmlogid":crmlogid, "startdate": startdate ,"enddate":enddate,"status" :status, "city":city,"bservice":bservice,"vehicle":vehicle,"master_service":master_service,"person":person,"bsource":bsource },
        success : function(data) {
          //console.log(data);
            
          $("#loadingperson").hide();
          $("#tableperson").show();
          $('#tableperson').empty().append(data);
        },
        error: function (xhr, ajaxOptions, thrownError) {
         // alert(xhr.status + " "+ thrownError);
        }
    });

    }
    //service modal 
    $("#myModalservice").on('hide.bs.modal', function(event){
        $("#table").addClass('body');
    });
    $("#myModalservice").on('show.bs.modal', function(event){
    $("#loadingservice").show();
    $("#tableservice").hide();
            
    var button=$(event.relatedTarget);
    loadModalservice(button);

    });
              
    function loadModalservice(button){
    var masterservice=button.data('service');
    var startdate=button.data('startdate');
    var enddate=button.data('enddate');
    var status=button.data('status');
    var vehicle=button.data('vehicle');
    var city= $('#city').val();
    var bservice = $('#bservice').val();
    var bvehicle = $('#vehicle').val();
    var master_service = $('#master_service').val();
    var person = $('#person').val();
    var bsource = $('#bsource').val();

    $.ajax({
        url : "ajax/conversion_panel_data.php?tab=service",  // create a new php page to handle ajax request
        type : "POST",
        data : {"masterservice": masterservice, "startdate": startdate ,"enddate":enddate,"status" :status ,"vehicle": vehicle, "city":city,"bservice":bservice,"bvehicle":bvehicle,"master_service":master_service,"person":person,"bsource":bsource },
        success : function(data) {
          //console.log(data);
            
          $("#loadingservice").hide();
          $("#tableservice").show();
          $('#tableservice').empty().append(data);
        },
        error: function (xhr, ajaxOptions, thrownError) {
          //alert(xhr.status + " "+ thrownError);
        }
    });

    }

    //source modal
    $("#myModalsource").on('hide.bs.modal', function(event){
          $("#table").addClass('body');
      });
      $("#myModalsource").on('show.bs.modal', function(event){
      $("#loadingsource").show();
      $("#tablesource").hide();
              
      var button=$(event.relatedTarget);
      loadModalsource(button);

      });
                
      function loadModalsource(button){
      var source=button.data('source');
      var startdate=button.data('startdate');
      var enddate=button.data('enddate');
      var status=button.data('status');
      var vehicle=button.data('vehicle');
      var city= $('#city').val();
      var bservice = $('#bservice').val();
      var bvehicle = $('#vehicle').val();
      var master_service = $('#master_service').val();
      var person = $('#person').val();
      var bsource = $('#bsource').val();

      $.ajax({
          url : "ajax/conversion_panel_data.php?tab=source",  // create a new php page to handle ajax request
          type : "POST",
          data : {"source":source, "startdate": startdate ,"enddate":enddate,"status" :status ,"vehicle": vehicle, "city":city,"bservice":bservice,"bvehicle":bvehicle,"master_service":master_service,"person":person,"bsource":bsource },
          success : function(data) {
            //console.log(data);
            //var modal = $(this)
              
            $("#loadingsource").hide();
            $("#tablesource").show();
            $('#tablesource').empty().append(data);
          },
          error: function (xhr, ajaxOptions, thrownError) {
            //alert(xhr.status + " "+ thrownError);
          }
      });

      }

      // non conv modal
      $("#myModalnonconv").on('hide.bs.modal', function(event){
        $("#table").addClass('body');
    });
    $("#myModalnonconv").on('show.bs.modal', function(event){
    $("#loadingnonconv").show();
    $("#tablenonconv").hide();
            
    var button=$(event.relatedTarget);
    loadModalnonconv(button);

    });
              
    function loadModalnonconv(button){
    var masterservice=button.data('service');
    var activity=button.data('activity');
    var startdate=button.data('startdate');
    var enddate=button.data('enddate');
    var bvehicle=button.data('vehicle');
    var city= $('#city').val();
    var ncv= button.data('ncv');
    var bservice = $('#bservice').val();
    var vehicle = $('#vehicle').val();
    var master_service = $('#master_service').val();
    var person = $('#person').val();
    var bsource = $('#bsource').val();

    $.ajax({
        url : "ajax/conversion_panel_data.php?tab=nonconv",  // create a new php page to handle ajax request
        type : "POST",
        data : {"masterservice":masterservice,"activity":activity, "startdate": startdate ,"enddate":enddate ,"bvehicle": bvehicle, "city":city,"ncv":ncv,"bservice":bservice,"vehicle":vehicle,"master_service":master_service,"person":person,"bsource":bsource },
        success : function(data) {
          //console.log(data);
          //var modal = $(this)
            
          $("#loadingnonconv").hide();
          $("#tablenonconv").show();
          $('#tablenonconv').empty().append(data);
        },
        error: function (xhr, ajaxOptions, thrownError) {
          //alert(xhr.status + " "+ thrownError);
        }
    });

    }

}

</script>


<!-- ------------------------------  pop ups -------------------------------------------- -->

<!-- ------------------------------ person tab start ------------------------------------------ -->
<!-- Modal -->
<div class="modal fade" id="myModalperson" role="dialog" >
  <div class="modal-dialog" style="width:65%;">

    <!-- Modal content-->
    <div class="modal-content" style="height:80%;overflow:auto;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Person View</h3>
      </div>
      <div class="modal-body">
        <div id="loadingperson" style="display:none; vertical-align:middle;margin-top:40px;" align="center">
          <div class='uil-default-css' style='transform:scale(0.58);'>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
          </div>
        </div>
        <div id="tableperson">
        
        </div>
      </div><!-- modal body -->
    </div><!-- modal content -->
  </div><!-- modal dailog -->
</div><!-- modal -->

<!-- ------------------------------ person tab end ------------------------------------------ -->
<!-- ------------------------------ all services tab start ------------------------------------------ -->
<!-- Modal -->
<div class="modal fade" id="myModalservice" role="dialog" >
  <div class="modal-dialog" style="width:65%;">

    <!-- Modal content-->
    <div class="modal-content" style="height:80%;overflow:auto;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Service View</h3>
      </div>
      <div class="modal-body">
        <div id="loadingservice" style="display:none; vertical-align:middle;margin-top:40px;" align="center">
          <div class='uil-default-css' style='transform:scale(0.58);'>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
          </div>
        </div>
        <div id="tableservice">
        
        </div>
      </div><!-- modal body -->
    </div><!-- modal content -->
  </div><!-- modal dailog -->
</div><!-- modal -->

<!-- ------------------------------ services tab end ------------------------------------------ -->

<!-- ------------------------------ sources tab start ------------------------------------------ -->
<!-- Modal -->
<div class="modal fade" id="myModalsource" role="dialog" >
  <div class="modal-dialog" style="width:65%;">

    <!-- Modal content-->
    <div class="modal-content" style="height:80%;overflow:auto;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Source View</h3>
      </div>
      <div class="modal-body">
        <div id="loadingsource" style="display:none;vertical-align:middle;margin-top:40px;" align="center">
          <div class='uil-default-css' style='transform:scale(0.58);'>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
          </div>
        </div>
        <div id="tablesource">
        
        </div>
      </div><!-- modal body -->
    </div><!-- modal content -->
  </div><!-- modal dailog -->
</div><!-- modal -->

<!-- ------------------------------  source tab end ------------------------------------------ -->

<!-- ------------------------------ non conversion tab start ------------------------------------------ -->
<!-- Modal -->
<div class="modal fade" id="myModalnonconv" role="dialog" >
  <div class="modal-dialog" style="width:65%;">

    <!-- Modal content-->
    <div class="modal-content" style="height:80%;overflow:auto;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Non Conversion View</h3>
      </div>
      <div class="modal-body">
        <div id="loadingnonconv" style="display:none; vertical-align:middle;margin-top:40px;" align="center">
          <div class='uil-default-css' style='transform:scale(0.58);'>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#ffa800;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
          </div>
        </div>
        <div id="tablenonconv">
        </div>
      </div><!-- modal body -->
    </div><!-- modal content -->
  </div><!-- modal dailog -->
</div><!-- modal -->

<!-- ------------------------------ non conversion tab end ------------------------------------------ -->

<!-- ------------------------------ pop ups end ------------------------------------------ -->

</body>
</html>
<?php
include("sidebar.php");
$conn = db_connect1();
// login or not
if((empty($_SESSION['crm_log_id']))) {
	
	header('location:logout.php');
	die();
}

$searchuser = base64_decode($_GET['u']);
$serach_type = base64_decode($_GET['t']);

?>

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta charset="utf-8">
  <title>GoBumpr Bridge</title>


  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


<!-- stylings -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- Facebook Pixel Code -->
<script async>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '582926561860139');
   fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
   /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Google Analytics Code -->
<script async>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67994843-2', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body>
<?php include_once("header.php"); ?>
<script>
	$(document).ready(function(){
		$('#city').hide();
	})
</script>
<div class="overlay" data-sidebar-overlay></div>
<div class="padding"></div>
<h4 style="margin-top:30px;margin-left:36px;color:#263238;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;&nbsp;For more details click on User Id !!!</h4>
  <!-- show list -->
<div id="show" style="margin-top:30px;" align="center">
<table class="table table-stripped table-bordered" id="table" style="width:86%;">
<thead style="background-color:#ccc;">
<th>No.</th>
<th>UserId</th>
<th>UserName</th>
<th>Mobile</th>
<th>E-Mail</th>
<th>Locality</th>
<th>Source</th>
<th>TimeStamp</th>
</thead>
<tbody>
<?php 

if($serach_type == 'username'){
    $searchterm = '%'.$searchuser.'%';
    $sql_user = "SELECT reg_id,name,mobile_number,email_id,Locality_Home,source,lat_lng,log FROM user_register WHERE name like '$searchterm' ";
}
else{
    $searchterm = explode('@',$searchuser);
    $email_address = '%'.$searchterm[0].'%';
    $doamin = '%'.$searchterm[1].'%';
    $sql_user = "SELECT reg_id,name,mobile_number,email_id,Locality_Home,source,lat_lng,log FROM user_register WHERE email_id like '$email_address'";
}
    $res_user = mysqli_query($conn,$sql_user);
    $num_rows = mysqli_num_rows($res_user);

    if($num_rows >= 1){
        $no = 0;
      while($row_user = mysqli_fetch_object($res_user)){
          $user_id = $row_user->reg_id;
          $username = $row_user->name;
          $mobile = $row_user->mobile_number;
          $email = $row_user->email_id;
          $localityhome = $row_user->Locality_Home;
          $lat_lng = $row_user->lat_lng;
          $source = $row_user->source;
          $log = $row_user->log;

          $u=base64_encode($user_id);
          ?>
          <tr>
          <td><?php echo $no = $no+1; ?></td>
          <td><a href="verified.php?u=<?php echo $u; ?>" style="text-decoration:none;"><?php echo $user_id; ?></td>
          <td><?php echo $username; ?></td>
          <td><?php echo $mobile; ?></td>
          <td><?php echo $email; ?></td>
          <td><?php 
               if($lat_lng == ''){
                   if($localityhome == ''){
                     echo 'NA'; 
                   }
                   else{
                       echo $localityhome;
                   }
               }
               else{
                   echo $lat_lng;
               }
               ?>
              </td>
          <td><?php echo $source; ?></td>
          <td><?php echo date('d M Y h.i A', strtotime($log)); ?></td>
          </tr>
          <?php
      }
    }
    else{
        header("Location:no_results_found.php");
        exit(1);
    }
   
?>
</tbody>
</table>
</div>


<!-- jQuery library -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- side bar -->
<script src="js/sidebar.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</body>
</html>

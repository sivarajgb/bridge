<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<?php 
include("config.php");
$conn = db_connect1();
$conn2 = db_connect2();

$today=date('Y-m-d H:i:s');
$booking_id=$_POST['bookingId'];
$booking_id = $_GET['booking_id'];
$booking_id = 101473;
$api_key = 'AIzaSyApbyUPYKQ08pihvsDK3EkbJ8pP6igO_Eo';

$select_booking = "SELECT l.lat as booking_lat,l.lng as booking_lng,b.*,v.brand,v.model FROM user_booking_tb b LEFT JOIN gobumpr_test.localities l ON l.localities = b.locality LEFT JOIN user_vehicle_table v ON v.id=b.user_veh_id WHERE booking_id = '$booking_id'";
// echo $select_booking;
// echo "<br>";
$res_booking = mysqli_query($conn,$select_booking);
$row_booking = mysqli_fetch_object($res_booking);
$booking_lat = $row_booking->booking_lat;
$booking_lng = $row_booking->booking_lng;
$city = $row_booking->city;
$brand = $row_booking->brand;
$vehicle_type = $row_booking->vehicle_type;
$service_type = $row_booking->service_type;
$radius = '0';

$select_service = "SELECT service_type_column FROM go_axle_service_price_tbl WHERE service_type  = '$service_type';";
// echo $select_mec;
// echo "<br>";
$res_service = mysqli_query($conn,$select_service);
$row_service = mysqli_fetch_object($res_service);
$service_type_column = $row_service->service_type_column;


$select_mec = "SELECT s.mec_id as mec,m.*,(6371*ACOS(COS(RADIANS('$booking_lat'))*COS(RADIANS(m.lat))*COS(RADIANS(m.lng)-RADIANS('$booking_lng'))+SIN(RADIANS('$booking_lat'))*SIN(RADIANS(m.lat)))) as dist FROM gobumpr_test.admin_mechanic_table m LEFT JOIN admin_service_type s ON m.mec_id = s.mec_id WHERE s.$service_type_column = 1 AND m.status=0 AND m.type='$vehicle_type' AND m.wkly_counter > 0 AND m.address5='$city' AND m.axle_id NOT IN (1014,1035,1670) ORDER BY dist";
// echo $select_mec;
// echo "<br>";

$res_mec = mysqli_query($conn,$select_mec);
$i = $k = $z = 0;
$count = mysqli_num_rows($res_mec);
$select_b2b = "SELECT query.b2b_shop_id,query.b2b_booking_id,AVG(CASE WHEN query.rating != 0 THEN query.rating END) as avg_rating,AVG(TIMESTAMPDIFF(MINUTE,query.b2b_log,query.b2b_mod_log)) AS avg_action,AVG(TIMESTAMPDIFF(MINUTE,query.b2b_mod_log,query.b2b_contacted_log)) AS avg_contact,TIMESTAMPDIFF(HOUR,query.b2b_log,DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 330 MINUTE)) diff_in_hrs,query.b2b_acpt_flag,query.b2b_deny_flag FROM (";
while($row_mec = mysqli_fetch_object($res_mec))
{
	$z = $z + 1;
	$mec_id = $row_mec->mec_id;
	$axle_id = $row_mec->axle_id;
	$shop_name = $row_mec->shop_name;
	$mec_lat = $row_mec->lat;
	$mec_lng = $row_mec->lng;
	$pick_range = $row_mec->pick_range;
	$distance = $row_mec->dist;
	$brand_serviced = $row_mec->brand;
	$address4 = $row_mec->address4;
	$premium = $row_mec->premium;
	$brandsArr = '';
	
	$url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$booking_lat.",".$booking_lng."&destinations=".$mec_lat.",".$mec_lng."&mode=driving&key=$api_key";
	$ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
	
	
	if($brand_serviced != 'all' && $brand_serviced != '')
	{
		$brandsArr = explode(',',$brand_serviced);
		if(!in_array($brand,$brandsArr))
		{
			continue;
		}
	}
	$brand_not_serviced = $row_mec->brand_ns;
	$brandsNoArr = '';
	if($brand_not_serviced != 'all' && $brand_not_serviced != '')
	{
		$brandsNoArr = explode(',',$brand_not_serviced);
		if(in_array($brand,$brandsNoArr))
		{
			continue;
		}
	}
	
	$dist = (float)str_replace("km","",$dist);
	$subquery = 0;
	if($dist <= 5)
	{
		$select_b2b_subquery = "(SELECT b.b2b_shop_id,b.b2b_booking_id,b.b2b_Rating as rating,b.b2b_log,s.b2b_acpt_flag,s.b2b_deny_flag,s.b2b_mod_log,b.b2b_contacted_log FROM b2b_booking_tbl b LEFT JOIN b2b_status s ON s.b2b_booking_id = b.b2b_booking_id WHERE b.b2b_shop_id = '$axle_id' ORDER BY b.b2b_booking_id DESC LIMIT 10)";
		$subquery = 1;
		$garages[$axle_id]['mec_id'] = $mec_id;
		$garages[$axle_id]['axle_id'] = $axle_id;
		$garages[$axle_id]['address4'] = $address4;
		$garages[$axle_id]['shop_name'] = $shop_name;
		$garages[$axle_id]['premium'] = $premium;
		$garages[$axle_id]['distance'] = round($distance,2).' Kms';
		$garages[$axle_id]['distance_navigation'] = round($dist,2).' Kms';
		$garages[$axle_id]['pick_range'] = $pick_range.' Kms';
		$i = $i + 1;
	}
	if($subquery == 1 && $z != 1)
	{
		$select_b2b = $select_b2b."UNION ALL".$select_b2b_subquery;
	}
	else if($subquery == 1 && $z == 1)
	{
		$select_b2b = $select_b2b.$select_b2b_subquery;
	}
}
if(sizeof($garages) < 2)
{
	$i = $k = $z = 0;
	$res_mec = mysqli_query($conn,$select_mec);
	while($row_mec = mysqli_fetch_object($res_mec))
	{
		$z = $z + 1;
		$mec_id = $row_mec->mec_id;
		$axle_id = $row_mec->axle_id;
		$shop_name = $row_mec->shop_name;
		$mec_lat = $row_mec->lat;
		$mec_lng = $row_mec->lng;
		$pick_range = $row_mec->pick_range;
		$distance = $row_mec->dist;
		$brand_serviced = $row_mec->brand;
		$address4 = $row_mec->address4;
		$premium = $row_mec->premium;
		$brandsArr = '';
		
		$url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$booking_lat.",".$booking_lng."&destinations=".$mec_lat.",".$mec_lng."&mode=driving&key=$api_key";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		curl_close($ch);
		$response_a = json_decode($response, true);
		$dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
		
		
		if($brand_serviced != 'all' && $brand_serviced != '')
		{
			$brandsArr = explode(',',$brand_serviced);
			if(!in_array($brand,$brandsArr))
			{
				continue;
			}
		}
		$brand_not_serviced = $row_mec->brand_ns;
		$brandsNoArr = '';
		if($brand_not_serviced != 'all' && $brand_not_serviced != '')
		{
			$brandsNoArr = explode(',',$brand_not_serviced);
			if(in_array($brand,$brandsNoArr))
			{
				continue;
			}
		}
		
		$dist = (float)str_replace("km","",$dist);
		$subquery = 0;
		if($dist <= $pick_range)
		{
			$select_b2b_subquery = "(SELECT b.b2b_shop_id,b.b2b_booking_id,b.b2b_Rating as rating,b.b2b_log,s.b2b_acpt_flag,s.b2b_deny_flag,s.b2b_mod_log,b.b2b_contacted_log FROM b2b_booking_tbl b LEFT JOIN b2b_credits_tbl c ON c.b2b_shop_id = b.b2b_shop_id LEFT JOIN b2b_status s ON s.b2b_booking_id = b.b2b_booking_id WHERE b.b2b_shop_id = '$axle_id' ORDER BY b.b2b_booking_id DESC LIMIT 10)";
			$subquery = 1;
			$garages[$axle_id]['mec_id'] = $mec_id;
			$garages[$axle_id]['axle_id'] = $axle_id;
			$garages[$axle_id]['address4'] = $address4;
			$garages[$axle_id]['shop_name'] = $shop_name;
			$garages[$axle_id]['premium'] = $premium;
			$garages[$axle_id]['distance'] = round($distance,2).' Kms';
			$garages[$axle_id]['distance_navigation'] = round($dist,2).' Kms';
			$garages[$axle_id]['pick_range'] = $pick_range.' Kms';
			$i = $i + 1;}
		if($subquery == 1 && $z != 1)
		{
			$select_b2b = $select_b2b."UNION ALL".$select_b2b_subquery;
		}
		else if($subquery == 1 && $z == 1)
		{
			$select_b2b = $select_b2b.$select_b2b_subquery;
		}
	}
}

$select_b2b = $select_b2b.") query GROUP BY query.b2b_shop_id;";
// echo $select_b2b;
$res_b2b = mysqli_query($conn2,$select_b2b);
$i = 0;
while($row_b2b = mysqli_fetch_object($res_b2b))
{
	$b2b_shop_id = $row_b2b->b2b_shop_id;
	$avg_rating = $row_b2b->avg_rating;
	$diff_in_hrs = $row_b2b->diff_in_hrs;
	$avg_action = $row_b2b->avg_action;
	$avg_contact = $row_b2b->avg_contact;
	$accept = $row_b2b->b2b_acpt_flag;
	$deny = $row_b2b->b2b_deny_flag;
	if($accept == 0 && $deny == 0)
	{
		continue;
	}
	else if($deny == 1)
	{
		continue;
	}
	$garages[$b2b_shop_id]['avg_rating'] = $avg_rating;
	$garages[$b2b_shop_id]['diff_in_hrs'] = $diff_in_hrs;
	$garages[$b2b_shop_id]['avg_action'] = $avg_action;
	$garages[$b2b_shop_id]['avg_contact'] = $avg_contact;
	$i = $i + 1;
}
$i = $k = 0;
// print_r($garages);
foreach($garages as $index => $garage)
{
	if(array_key_exists("premium",$garage) && $garage['premium']==1)
	{
		foreach($garage as $key=>$val)
		{
			$premiumPartners[$garage['axle_id']][$key] = $val;
		}
	}
	else
	{
		foreach($garage as $key=>$val)
		{
			$preCreditPartners[$garage['axle_id']][$key] = $val;
		}
	}
	$i = $i + 1;
}
echo "premium: <br>";
echo json_encode($premiumPartners);
echo "<br>";
echo "<br>";
echo "precredit <br>";
echo json_encode($preCreditPartners);

$premiumHighest = array_filter($premiumPartners, function ($var) {
    return ($var['avg_rating'] >= 4);
});
$premiumAverage = array_filter($premiumPartners, function ($var) {
    return ($var['avg_rating'] < 4 && $var['avg_rating'] >= 3.5 );
});
$premiumLow = array_filter($premiumPartners, function ($var) {
    return ($var['avg_rating'] < 3.5);
});

$preCreditHighest = array_filter($preCreditPartners, function ($var) {
    return ($var['avg_rating'] >= 4);
});
$preCreditAverage = array_filter($preCreditPartners, function ($var) {
    return ($var['avg_rating'] < 4 && $var['avg_rating'] >= 3.5 );
});
$preCreditLow = array_filter($preCreditPartners, function ($var) {
    return ($var['avg_rating'] < 3.5);
});

// $premiumPartners = array();
// $premiumHighest = array();
// $premiumAverage = array();
// $premiumLow = array();
// $preCreditPartners = array();
// $preCreditHighest = array();
// $preCreditAverage = array();
// $preCreditLow = array();

if(!empty($premiumPartners))
{
	if(!empty($premiumHighest))
	{
		$selectFrom = $premiumHighest;
	}
	else if(!empty($premiumAverage))
	{
		$selectFrom = $premiumAverage;
	}
	else
	{
		$selectFrom = $premiumLow;
	}
	echo "<br>";
	echo "<br>";
	echo "Initial Ranking";
	echo json_encode($selectFrom);
	if(sizeof($selectFrom)>1)
	{
		$last_goaxle_sort = array();
		foreach ($selectFrom as $key => $row)
		{
			$last_goaxle_sort[$key] = $row['diff_in_hrs'];
		}
		array_multisort($last_goaxle_sort, SORT_DESC, $selectFrom);
		$selectFrom = array_slice($selectFrom,0,round(sizeof($selectFrom)/2,0));
		echo "<br>";
		echo "<br>";
		echo "Ranking after last goaxle";
		echo json_encode($selectFrom);
		if(sizeof($selectFrom)>1)
		{
			$avg_action_sort = array();
			foreach ($selectFrom as $key => $row)
			{
				$avg_action_sort[$key] = $row['avg_action'];
			}
			array_multisort($avg_action_sort, SORT_ASC, $selectFrom);
			$selectFrom = array_slice($selectFrom,0,round(sizeof($selectFrom)/2,0));
			echo "<br>";
			echo "<br>";
			echo "Ranking after avg action";
			echo json_encode($selectFrom);
		}
		foreach($selectFrom as $arrValue)
		{
			$shop = $arrValue;
		}
	}
	else
	{
		foreach($selectFrom as $arrValue)
		{
			$shop = $arrValue;
		}
	}
}
else
{
	if(!empty($preCreditHighest))
	{
		$selectFrom = $preCreditHighest;
	}
	else if(!empty($preCreditAverage))
	{
		$selectFrom = $preCreditAverage;
	}
	else
	{
		$selectFrom = $preCreditLow;
	}
	echo "<br>";
	echo "<br>";
	echo "Initial Ranking";
	echo json_encode($selectFrom);
	if(sizeof($selectFrom)>1)
	{
		$last_goaxle_sort = array();
		foreach ($selectFrom as $key => $row)
		{
			$last_goaxle_sort[$key] = $row['diff_in_hrs'];
		}
		array_multisort($last_goaxle_sort, SORT_DESC, $selectFrom);
		$selectFrom = array_slice($selectFrom,0,round(sizeof($selectFrom)/2,0));
		echo "<br>";
		echo "<br>";
		echo "Ranking after last goaxle";
		echo json_encode($selectFrom);
		if(sizeof($selectFrom)>1)
		{
			$avg_action_sort = array();
			foreach ($selectFrom as $key => $row)
			{
				$avg_action_sort[$key] = $row['avg_action'];
			}
			array_multisort($avg_action_sort, SORT_ASC, $selectFrom);
			$selectFrom = array_slice($selectFrom,0,round(sizeof($selectFrom)/2,0));
			echo "<br>";
			echo "<br>";
			echo "Ranking after avg action";
			echo json_encode($selectFrom);
			if(sizeof($selectFrom)>1)
			{
				$avg_contact_sort = array();
				foreach ($selectFrom as $key => $row)
				{
					$avg_contact_sort[$key] = $row['avg_contact'];
				}
				array_multisort($avg_contact_sort, SORT_ASC, $selectFrom);
				echo "<br>";
				echo "<br>";
				echo "Ranking after avg contact";
				echo json_encode($selectFrom);
			}
		}
		foreach($selectFrom as $arrValue)
		{
			$shop = $arrValue;
			break;
		}
	}
	else
	{
		foreach($selectFrom as $arrValue)
		{
			$shop = $arrValue;
			break;
		}
	}
}

echo "<br>";
echo "<br>";
echo "Final Ranking";
echo json_encode($selectFrom);
echo "<br>";
echo "<br>";
echo "Final Shop";
echo json_encode($shop);
$axle_id = $shop['axle_id'];
$shop_name = $shop['shop_name'];
$mec_id = $shop['mec_id'];
$distance = $shop['distance'];
$dist = $shop['distance_navigation'];
$address4 = $shop['address4'];
$sql_crd = "SELECT b2b_credits FROM b2b_credits_tbl WHERE b2b_shop_id='$axle_id'";
$res_crd = mysqli_query($conn2,$sql_crd);
$row_crd = mysqli_fetch_object($res_crd);
if(mysqli_num_rows($res_crd)<=0){
	$credits='-';
}else{
	$credits = $row_crd->b2b_credits;
}
?>
<!--<div>
	<div class="w3-container" style="margin-top:20px;">
		<div class="w3-card-4" >
			<header class="w3-container" style="background:#ccc;">
				<h3><?php echo /*$val['axle_id'].*/$shop_name.' ('.$credits.')'; ?></h3>
			</header>

		<div class="w3-container" style="height:70px;">
			<input type="hidden" class="form-control" id="select-mechanic" value="<?php echo $mec_id;?>">
			<p style="float:left;margin: 20px 0 10px 0;"><?php echo $address4.", ".$dist." away."; ?></p>
			<p class="goaxle" data-mec-id="<?php echo $mec_id;?>" style="float: right;width: 40px;height: 40px;background: #ffa800;-moz-border-radius: 50px;-webkit-border-radius: 50px;border-radius: 50px;margin-top: 15px;"><i class="fa fa-rocket" aria-hidden="true" style="font-size: 26px;margin-top: 8px;margin-left: 6px;"></i></p>
		</div>
		</div>
	</div>
</div>-->



<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>GoBumpr Bike Service & Polish in Chennai</title>
        <!-- Latest compiled and minified JavaScript -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800" >
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="icon" type="image/png" sizes="16x16" href="https://static.gobumpr.com/web-app/fav-icons/16.png">
        <link rel="icon" type="image/png" sizes="32x32" href="https://static.gobumpr.com/web-app/fav-icons/32.png">
        <link rel="icon" type="image/png" sizes="57x57" href="https://static.gobumpr.com/web-app/fav-icons/57.png">
        <link rel="icon" type="image/png" sizes="76x76" href="https://static.gobumpr.com/web-app/fav-icons/76.png">
        <link rel="icon" type="image/png" sizes="96x96" href="https://static.gobumpr.com/web-app/fav-icons/96.png">
        <link rel="icon" type="image/png" sizes="114x114" href="https://static.gobumpr.com/web-app/fav-icons/114.png">
        <link rel="icon" type="image/png" sizes="144x144" href="https://static.gobumpr.com/web-app/fav-icons/144.png">
        <link rel="icon" type="image/png" sizes="152x152" href="https://static.gobumpr.com/web-app/fav-icons/152x152.png">
        <link rel="stylesheet" href="./css/confirmationPageCss.css">
		<!-- Google Analytics Code -->
		<script async>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-67994843-3', 'auto');
		  ga('send', 'pageview');

		</script>
		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '582926561860139');
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=582926561860139&ev=PageView&noscript=1"
		/></noscript>
		<!-- DO NOT MODIFY -->
		<!-- End Facebook Pixel Code -->
    </head>
    <body>
	<!-- Google Code for Google_Web_Conversion_Tracking Conversion Page -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 865788067;
	var google_conversion_label = "foFcCP6U-3AQo8HrnAM";
	var google_remarketing_only = false;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/865788067/?label=foFcCP6U-3AQo8HrnAM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>

        <nav>
           <div class="text-center">
               <center>
                   <a class="navbar-header" href="./index.php"><img src="https://static.gobumpr.com/img/logo-new-gb.svg" alt="GoBumpr Full Body Painting services Online" class="img-responsive gobumpr_logo" style="width:45%; margin-top: 5px;" /></a>                  
               </center>
            </div> 
        </nav> 
        <div id="enquiry_received" class="container">
            <div class="container text-center">
            	<br/>
            	<br/>
                <img src="https://static.gobumpr.com/tyres/confirmation/check.svg" class="check" alt="confirmed booking image" style="width:25%;">
            </div>
            <div  id="successMessage" style="text-align:center; width:100%;padding:0px 10px 3px 2px;">
    			<h3 class="text-success">Thanks for your interest! We will share the details shortly!</h3>
			</div>  
			<br/>
       	<br/><br/>
       	<br/>         
        </div>

        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 text-right">
                        <img src="https://static.gobumpr.com/tyres/confirmation/relax.svg" class="relax_image" alt="Hassle free booking img">
                    </div>
                    <div class="col-xs-6 text-left">
                        <h3 class="text_content">SIT BACK & RELAX</h3>
                        <h4>YOU'RE DONE FOR NOW.</h4>
                        <p>Our team will reach you for a delightful service experience with Gobumpr</p>
                    </div>
                </div>
            </div>
        </footer>
        
    </body>
</html>
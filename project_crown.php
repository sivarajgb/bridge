<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<?php 
include("config.php");
$conn = db_connect1();
$conn2 = db_connect2();

//$city = $_SESSION['city'];
$today=date('Y-m-d H:i:s');
$booking_id=$_POST['bookingId'];
$booking_id = $_GET['booking_id'];
$booking_id = 101108;

$select_booking = "SELECT l.lat as booking_lat,l.lng as booking_lng,b.*,v.brand,v.model FROM user_booking_tb b LEFT JOIN gobumpr_test.localities l ON l.localities = b.locality LEFT JOIN user_vehicle_table v ON v.id=b.user_veh_id WHERE booking_id = '$booking_id'";
// echo $select_booking;
// echo "<br>";
$res_booking = mysqli_query($conn,$select_booking);
$row_booking = mysqli_fetch_object($res_booking);
$booking_lat = $row_booking->booking_lat;
$booking_lng = $row_booking->booking_lng;
$city = $row_booking->city;
$brand = $row_booking->brand;
$vehicle_type = $row_booking->vehicle_type;
$service_type = $row_booking->service_type;
$radius = '0';

$select_service = "SELECT service_type_column FROM go_axle_service_price_tbl WHERE service_type  = '$service_type';";
$res_service = mysqli_query($conn,$select_service);
$row_service = mysqli_fetch_object($res_service);
$service_type_column = $row_service->service_type_column;


$select_mec = "SELECT s.mec_id as mec,m.*,(6371*ACOS(COS(RADIANS('$booking_lat'))*COS(RADIANS(m.lat))*COS(RADIANS(m.lng)-RADIANS('$booking_lng'))+SIN(RADIANS('$booking_lat'))*SIN(RADIANS(m.lat)))) as dist FROM gobumpr_test.admin_mechanic_table m LEFT JOIN admin_service_type s ON m.mec_id = s.mec_id WHERE s.$service_type_column = 1 AND m.status=0 AND m.type='$vehicle_type' AND m.wkly_counter > 0 AND m.address5='$city' ORDER BY dist";
// echo $select_mec;
// echo "<br>";

$res_mec = mysqli_query($conn,$select_mec);
$i = $k = 0;
while($row_mec = mysqli_fetch_object($res_mec))
{
	echo $mec_id = $row_mec->mec_id;
	$axle_id = $row_mec->axle_id;
	echo $shop_name = $row_mec->shop_name;
	$mec_lat = $row_mec->lat;
	$mec_lng = $row_mec->lng;
	$pick_range = $row_mec->pick_range;
	$distance = $row_mec->dist;
	$brand_serviced = $row_mec->brand;
	$address4 = $row_mec->address4;
	$brandsArr = '';
	
	$url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$booking_lat.",".$booking_lng."&destinations=".$mec_lat.",".$mec_lng."&mode=driving&key=AIzaSyCdFZjgK0cUARhpE_i3hgYADMge1FOHu88";
	$ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    echo $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
	echo "<br>";
	
	
	if($brand_serviced != 'all' && $brand_serviced != '')
	{
		$brandsArr = explode(',',$brand_serviced);
		if(!in_array($brand,$brandsArr))
		{
			continue;
		}
	}
	$brand_not_serviced = $row_mec->brand_ns;
	$brandsNoArr = '';
	if($brand_not_serviced != 'all' && $brand_not_serviced != '')
	{
		$brandsNoArr = explode(',',$brand_not_serviced);
		if(in_array($brand,$brandsNoArr))
		{
			continue;
		}
	}
	
	// $rad = M_PI / 180;
	// $distance = acos(sin($mec_lat*$rad) * sin($booking_lat*$rad) + cos($mec_lat*$rad) * cos($booking_lat*$rad) * cos($mec_lng*$rad - $booking_lng*$rad)) * 6371;
	
	if($dist <= 5)
	// if($dist > 5 && $dist <= $pick_range)
	{
		$radius = '5';
		$select_b2b = "SELECT b.b2b_booking_id,b.b2b_Rating as rating,b.b2b_log,c.b2b_partner_flag,s.b2b_acpt_flag,s.b2b_deny_flag,s.b2b_mod_log,b.b2b_contacted_log FROM b2b_booking_tbl b LEFT JOIN b2b_credits_tbl c ON c.b2b_shop_id = b.b2b_shop_id LEFT JOIN b2b_status s ON s.b2b_booking_id = b.b2b_booking_id WHERE b.b2b_shop_id = '$axle_id' ORDER BY b.b2b_booking_id DESC LIMIT 10;";
		// echo $select_b2b;
		// echo "<br>";
		$res_b2b = mysqli_query($conn2,$select_b2b);
		$j = $n = 0;
		$avg_rating = $avg_action = $avg_contact = 0;
		while($row_b2b = mysqli_fetch_object($res_b2b))
		{
			if($j == 0)
			{
				$last_b2b_booking_id = $row_b2b->b2b_booking_id;
				$last_goaxle_date = $row_b2b->b2b_log;
				$premium = $row_b2b->b2b_partner_flag;
				$date1=date_create($last_goaxle_date);
				$date2=date_create($today);
				$diff=date_diff($date1,$date2);
				$d1 = $diff->format("%a");
				$d2 = $diff->format("%h");
				$diff_in_hrs = $d1*24 + $d2	;
				$accept = $row_b2b->b2b_acpt_flag;
				$deny = $row_b2b->b2b_deny_flag;
				if($accept == 0 && $deny == 0)
				{
					continue;
				}
				else if($deny == 1)
				{
					continue;
				}
				$j++;
			}
			$b2b_log = $row_b2b->b2b_log;
			$b2b_mod_log = $row_b2b->b2b_mod_log;
			if($b2b_mod_log != '0000-00-00 00:00:00')
			{
				$date1=date_create($b2b_log);
				$date2=date_create($b2b_mod_log);
				$diff=date_diff($date1,$date2);
				$d1 = $diff->format("%a");
				$d2 = $diff->format("%h");
				$d3 = $diff->format("%i");
				$diff_in_mins = $d1*24 + $d2*60 + $d3;
				$avg_action = $avg_action + $diff_in_mins;
			}
			$b2b_contacted_log = $row_b2b->b2b_contacted_log;
			if($b2b_contacted_log != '0000-00-00 00:00:00')
			{
				$date1=date_create($b2b_mod_log);
				$date2=date_create($b2b_contacted_log);
				$diff=date_diff($date1,$date2);
				$d1 = $diff->format("%a");
				$d2 = $diff->format("%h");
				$d3 = $diff->format("%i");
				$diff_in_mins = $d1*24 + $d2*60 + $d3;
				$avg_contact = $avg_contact + $diff_in_mins;
			}
			$avg_contact = ($b2b_contacted_log == "0000-00-00 00:00:00" || $b2b_contacted_log == '') ? $avg_contact + $diff_in_mins : 0;
			$rating = $row_b2b->rating;
			if($rating > 0)
			{
				$n++;
			}
			$avg_rating = $avg_rating + $rating;
		}
		$avg_rating = $avg_rating!= 0 ? $avg_rating/$n : $avg_rating;
		$avg_action = $avg_action/10;
		$avg_contact = $avg_contact/10;
		
		// echo $mec_id;
		// echo "<br>";
		if($premium == 2)
		{
			$premiumPartners[$i]['mec_id'] = $mec_id;
			$premiumPartners[$i]['avg_rating'] = $avg_rating;
			$premiumPartners[$i]['axle_id'] = $axle_id;
			$premiumPartners[$i]['address4'] = $address4;
			$premiumPartners[$i]['shop_name'] = $shop_name;
			$premiumPartners[$i]['last_goaxle_date'] = $last_goaxle_date;
			$premiumPartners[$i]['diff_in_hrs'] = $diff_in_hrs;
			$premiumPartners[$i]['avg_action'] = $avg_action;
			$premiumPartners[$i]['distance'] = round($distance,2).' Kms';
			$premiumPartners[$i]['distance_navigation'] = round($dist,2).' Kms';
			$i = $i + 1;
			// echo ' premium';
		}
		else
		{
			$preCreditPartners[$k]['mec_id'] = $mec_id;
			$preCreditPartners[$k]['avg_rating'] = $avg_rating;
			$preCreditPartners[$k]['axle_id'] = $axle_id;
			$preCreditPartners[$k]['address4'] = $address4;
			$preCreditPartners[$k]['shop_name'] = $shop_name;
			$preCreditPartners[$k]['last_goaxle_date'] = $last_goaxle_date;
			$preCreditPartners[$k]['diff_in_hrs'] = $diff_in_hrs;
			$preCreditPartners[$k]['avg_action'] = $avg_action;
			$preCreditPartners[$k]['avg_contact'] = $avg_contact;
			$preCreditPartners[$k]['distance'] = round($distance,2).' Kms';
			$preCreditPartners[$k]['distance_navigation'] = round($dist,2).' Kms';
			$k = $k + 1;
			// echo ' pre credit';
		}
		// echo "<br>";
		// echo $avg_rating;
		// echo "<br>";
		// echo $axle_id;
		// echo "<br>";
		// echo $shop_name;
		// echo "<br>";
		// echo round($distance,2).' Kms';
		// echo "<br>";
		// echo "<br>";
	}
}
// $premiumPartners = array();
// $preCreditPartners = array();
$select_mec = "SELECT s.mec_id as mec,m.*,(6371*ACOS(COS(RADIANS('$booking_lat'))*COS(RADIANS(m.lat))*COS(RADIANS(m.lng)-RADIANS('$booking_lng'))+SIN(RADIANS('$booking_lat'))*SIN(RADIANS(m.lat)))) as dist FROM gobumpr_test.admin_mechanic_table m LEFT JOIN admin_service_type s ON m.mec_id = s.mec_id WHERE s.$service_type_column = 1 AND m.status=0 AND m.type='$vehicle_type' AND m.wkly_counter > 0 AND m.address5='$city' ORDER BY dist";
// echo $select_mec;
// echo "<br>";

$res_mec = mysqli_query($conn,$select_mec);
$i = $k = 0;
while($row_mec = mysqli_fetch_object($res_mec))
{
	$mec_id = $row_mec->mec_id;
	$axle_id = $row_mec->axle_id;
	$shop_name = $row_mec->shop_name;
	$mec_lat = $row_mec->lat;
	$mec_lng = $row_mec->lng;
	$pick_range = $row_mec->pick_range;
	$distance = $row_mec->dist;
	$brand_serviced = $row_mec->brand;
	$address4 = $row_mec->address4;
	$brandsArr = '';
	
	$url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$booking_lat.",".$booking_lng."&destinations=".$mec_lat.",".$mec_lng."&mode=driving&key=AIzaSyCdFZjgK0cUARhpE_i3hgYADMge1FOHu88";
	$ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    echo $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
	echo "<br>";
	
	
	if($brand_serviced != 'all' && $brand_serviced != '')
	{
		$brandsArr = explode(',',$brand_serviced);
		if(!in_array($brand,$brandsArr))
		{
			continue;
		}
	}
	$brand_not_serviced = $row_mec->brand_ns;
	$brandsNoArr = '';
	if($brand_not_serviced != 'all' && $brand_not_serviced != '')
	{
		$brandsNoArr = explode(',',$brand_not_serviced);
		if(in_array($brand,$brandsNoArr))
		{
			continue;
		}
	}
	
if(sizeof($premiumPartners) == 0 && sizeof($preCreditPartners) == 0)
{
	$i = $k = 0;
	// if($distance <= $pick_range)
	if($dist <= $pick_range)
	{
		$radius = 'pick range';
		$select_b2b = "SELECT b.b2b_booking_id,b.b2b_Rating as rating,b.b2b_log,c.b2b_partner_flag,s.b2b_acpt_flag,s.b2b_deny_flag,s.b2b_mod_log,b.b2b_contacted_log FROM b2b_booking_tbl b LEFT JOIN b2b_credits_tbl c ON c.b2b_shop_id = b.b2b_shop_id LEFT JOIN b2b_status s ON s.b2b_booking_id = b.b2b_booking_id WHERE b.b2b_shop_id = '$axle_id' ORDER BY b.b2b_booking_id DESC LIMIT 10;";
		// echo $select_b2b;
		// echo "<br>";
		$res_b2b = mysqli_query($conn2,$select_b2b);
		$j = $n = 0;
		$avg_rating = $avg_action = $avg_contact = 0;
		while($row_b2b = mysqli_fetch_object($res_b2b))
		{
			if($j == 0)
			{
				$last_b2b_booking_id = $row_b2b->b2b_booking_id;
				$last_goaxle_date = $row_b2b->b2b_log;
				$premium = $row_b2b->b2b_partner_flag;
				$date1=date_create($last_goaxle_date);
				$date2=date_create($today);
				$diff=date_diff($date1,$date2);
				$d1 = $diff->format("%a");
				$d2 = $diff->format("%h");
				$diff_in_hrs = $d1*24 + $d2	;
				$accept = $row_b2b->b2b_acpt_flag;
				$deny = $row_b2b->b2b_deny_flag;
				if($accept == 0 && $deny == 0)
				{
					continue;
				}
				else if($deny == 1)
				{
					continue;
				}
				$j++;
			}
			$b2b_log = $row_b2b->b2b_log;
			$b2b_mod_log = $row_b2b->b2b_mod_log;
			if($b2b_mod_log != '0000-00-00 00:00:00')
			{
				$date1=date_create($b2b_log);
				$date2=date_create($b2b_mod_log);
				$diff=date_diff($date1,$date2);
				$d1 = $diff->format("%a");
				$d2 = $diff->format("%h");
				$d3 = $diff->format("%i");
				$diff_in_mins = $d1*24 + $d2*60 + $d3;
				$avg_action = $avg_action + $diff_in_mins;
			}
			$b2b_contacted_log = $row_b2b->b2b_contacted_log;
			if($b2b_contacted_log != '0000-00-00 00:00:00')
			{
				$date1=date_create($b2b_mod_log);
				$date2=date_create($b2b_contacted_log);
				$diff=date_diff($date1,$date2);
				$d1 = $diff->format("%a");
				$d2 = $diff->format("%h");
				$d3 = $diff->format("%i");
				$diff_in_mins = $d1*24 + $d2*60 + $d3;
				$avg_contact = $avg_contact + $diff_in_mins;
			}
			$avg_contact = ($b2b_contacted_log == "0000-00-00 00:00:00" || $b2b_contacted_log == '') ? $avg_contact + $diff_in_mins : 0;
			$rating = $row_b2b->rating;
			if($rating > 0)
			{
				$n++;
			}
			$avg_rating = $avg_rating + $rating;
		}
		$avg_rating = $avg_rating!= 0 ? $avg_rating/$n : $avg_rating;
		$avg_action = $avg_action/10;
		$avg_contact = $avg_contact/10;
		
		// echo $mec_id;
		// echo "<br>";
		if($premium == 2)
		{
			$premiumPartners[$i]['mec_id'] = $mec_id;
			$premiumPartners[$i]['avg_rating'] = $avg_rating;
			$premiumPartners[$i]['axle_id'] = $axle_id;
			$premiumPartners[$i]['address4'] = $address4;
			$premiumPartners[$i]['shop_name'] = $shop_name;
			$premiumPartners[$i]['last_goaxle_date'] = $last_goaxle_date;
			$premiumPartners[$i]['diff_in_hrs'] = $diff_in_hrs;
			$premiumPartners[$i]['avg_action'] = $avg_action;
			$premiumPartners[$i]['distance'] = round($distance,2).' Kms';
			$premiumPartners[$i]['distance_navigation'] = round($dist,2).' Kms';
			$i = $i + 1;
			// echo ' premium';
		}
		else
		{
			$preCreditPartners[$k]['mec_id'] = $mec_id;
			$preCreditPartners[$k]['avg_rating'] = $avg_rating;
			$preCreditPartners[$k]['axle_id'] = $axle_id;
			$preCreditPartners[$k]['address4'] = $address4;
			$preCreditPartners[$k]['shop_name'] = $shop_name;
			$preCreditPartners[$k]['last_goaxle_date'] = $last_goaxle_date;
			$preCreditPartners[$k]['diff_in_hrs'] = $diff_in_hrs;
			$preCreditPartners[$k]['avg_action'] = $avg_action;
			$preCreditPartners[$k]['avg_contact'] = $avg_contact;
			$preCreditPartners[$k]['distance'] = round($distance,2).' Kms';
			$preCreditPartners[$k]['distance_navigation'] = round($dist,2).' Kms';
			$k = $k + 1;
			// echo ' pre credit';
		}
		// echo "<br>";
		// echo $avg_rating;
		// echo "<br>";
		// echo $axle_id;
		// echo "<br>";
		// echo $shop_name;
		// echo "<br>";
		// echo round($distance,2).' Kms';
		// echo "<br>";
		// echo "<br>";
	}
}
}
echo "premium: <br>";
// print_r($premiumPartners);
echo json_encode($premiumPartners);
echo "<br>";
echo "<br>";
echo "precredit <br>";
// print_r($preCreditPartners);
echo json_encode($preCreditPartners);


$premiumHighest = array_filter($premiumPartners, function ($var) {
    return ($var['avg_rating'] >= 4.5);
});
// echo "<br>";
// echo "<br>";
// echo ">4.5";
// print_r($premiumHighest);
$premiumHigh = array_filter($premiumPartners, function ($var) {
    return ($var['avg_rating'] < 4.5 && $var['avg_rating'] >= 4);
});
// echo "<br>";
// echo "<br>";
// echo "4-4.5";
// print_r($premiumHigh);
$premiumAverage = array_filter($premiumPartners, function ($var) {
    return ($var['avg_rating'] < 4 && $var['avg_rating'] >= 3.5 );
});
// echo "<br>";
// echo "<br>";
// echo "3.5-4";
// print_r($premiumAverage);
$premiumLow = array_filter($premiumPartners, function ($var) {
    return ($var['avg_rating'] < 3.5);
});
// echo "<br>";
// echo "<br>";
// echo "<3.5";
// print_r($premiumLow);

$preCreditHighest = array_filter($preCreditPartners, function ($var) {
    return ($var['avg_rating'] >= 4.5);
});
// echo "<br>";
// echo "<br>";
// echo ">4.5";
// print_r($preCreditHighest);
$preCreditHigh = array_filter($preCreditPartners, function ($var) {
    return ($var['avg_rating'] < 4.5 && $var['avg_rating'] >= 4);
});
// echo "<br>";
// echo "<br>";
// echo "4-4.5";
// print_r($preCreditHigh);
$preCreditAverage = array_filter($preCreditPartners, function ($var) {
    return ($var['avg_rating'] < 4 && $var['avg_rating'] >= 3.5 );
});
// echo "<br>";
// echo "<br>";
// echo "3.5-4";
// print_r($preCreditAverage);
$preCreditLow = array_filter($preCreditPartners, function ($var) {
    return ($var['avg_rating'] < 3.5);
});
// echo "<br>";
// echo "<br>";
// echo "<3.5";
// print_r($preCreditLow);

// $premiumPartners = array();
// $premiumHighest = array();
// $premiumHigh = array();
// $premiumAverage = array();
// $premiumLow = array();
// $preCreditPartners = array();
// $preCreditHighest = array();
// $preCreditHigh = array();
// $preCreditAverage = array();
// $preCreditLow = array();

// echo "<br>";
// echo "<br>";
// echo "Final Array";
// print_r($selectFrom);

if(!empty($premiumPartners))
{
	if(!empty($premiumHighest))
	{
		$selectFrom = $premiumHighest;
	}
	else if(!empty($premiumHigh))
	{
		$selectFrom = $premiumHigh;
	}
	else if(!empty($premiumAverage))
	{
		$selectFrom = $premiumAverage;
	}
	else
	{
		$selectFrom = $premiumLow;
	}
	echo "<br>";
	echo "<br>";
	echo "Initial Ranking";
	echo json_encode($selectFrom);
	if(sizeof($selectFrom)>1)
	{
		$last_goaxle_sort = array();
		foreach ($selectFrom as $key => $row)
		{
			$last_goaxle_sort[$key] = $row['diff_in_hrs'];
		}
		array_multisort($last_goaxle_sort, SORT_DESC, $selectFrom);
		$selectFrom = array_slice($selectFrom,0,5);
		echo "<br>";
		echo "<br>";
		echo "Ranking after last goaxle";
		echo json_encode($selectFrom);
		if(sizeof($selectFrom)>1)
		{
			$avg_action_sort = array();
			foreach ($selectFrom as $key => $row)
			{
				$avg_action_sort[$key] = $row['avg_action'];
			}
			array_multisort($avg_action_sort, SORT_ASC, $selectFrom);
			$selectFrom = array_slice($selectFrom,0,3);
			echo "<br>";
			echo "<br>";
			echo "Ranking after avg action";
			echo json_encode($selectFrom);
		}
		foreach($selectFrom as $arrValue)
		{
			$shop = $arrValue;
		}
	}
	else
	{
		foreach($selectFrom as $arrValue)
		{
			$shop = $arrValue;
		}
	}
}
else
{
	if(!empty($preCreditHighest))
	{
		$selectFrom = $preCreditHighest;
	}
	else if(!empty($preCreditHigh))
	{
		$selectFrom = $preCreditHigh;
	}
	else if(!empty($preCreditAverage))
	{
		$selectFrom = $preCreditAverage;
	}
	else
	{
		$selectFrom = $preCreditLow;
	}
	echo "<br>";
	echo "<br>";
	echo "Initial Ranking";
	echo json_encode($selectFrom);
	if(sizeof($selectFrom)>1)
	{
		$last_goaxle_sort = array();
		foreach ($selectFrom as $key => $row)
		{
			$last_goaxle_sort[$key] = $row['diff_in_hrs'];
		}
		array_multisort($last_goaxle_sort, SORT_DESC, $selectFrom);
		$selectFrom = array_slice($selectFrom,0,5);
		echo "<br>";
		echo "<br>";
		echo "Ranking after last goaxle";
		echo json_encode($selectFrom);
		if(sizeof($selectFrom)>1)
		{
			$avg_action_sort = array();
			foreach ($selectFrom as $key => $row)
			{
				$avg_action_sort[$key] = $row['avg_action'];
			}
			array_multisort($avg_action_sort, SORT_ASC, $selectFrom);
			$selectFrom = array_slice($selectFrom,0,3);
			echo "<br>";
			echo "<br>";
			echo "Ranking after avg action";
			echo json_encode($selectFrom);
			if(sizeof($selectFrom)>1)
			{
				$avg_contact_sort = array();
				foreach ($selectFrom as $key => $row)
				{
					$avg_contact_sort[$key] = $row['avg_contact'];
				}
				array_multisort($avg_contact_sort, SORT_ASC, $selectFrom);
				echo "<br>";
				echo "<br>";
				echo "Ranking after avg contact";
				echo json_encode($selectFrom);
			}
		}
		foreach($selectFrom as $arrValue)
		{
			$shop = $arrValue;
		}
	}
	else
	{
		foreach($selectFrom as $arrValue)
		{
			$shop = $arrValue;
		}
	}
}
// print_r($preCreditPartners);
// print_r($premiumPartners);
echo "<br>";
echo "<br>";
echo "Final Ranking";
echo json_encode($selectFrom);
echo "<br>";
echo "<br>";
echo "Final Shop";
echo json_encode($shop);
$axle_id = $shop['axle_id'];
$shop_name = $shop['shop_name'];
$mec_id = $shop['mec_id'];
$distance = $shop['distance'];
$dist = $shop['distance_navigation'];
$address4 = $shop['address4'];
$sql_crd = "SELECT b2b_credits FROM b2b_credits_tbl WHERE b2b_shop_id='$axle_id'";
$res_crd = mysqli_query($conn2,$sql_crd);
$row_crd = mysqli_fetch_object($res_crd);
if(mysqli_num_rows($res_crd)<=0){
	$credits='-';
}else{
	$credits = $row_crd->b2b_credits;
}
echo $radius;
?>
<div>
	<div class="w3-container" style="margin-top:20px;">
		<div class="w3-card-4" >
			<header class="w3-container" style="background:#ccc;">
				<h3><?php echo /*$val['axle_id'].*/$shop_name.' ('.$credits.')'; ?></h3>
			</header>

		<div class="w3-container" style="height:70px;">
			<input type="hidden" class="form-control" id="select-mechanic" value="<?php echo $mec_id;?>">
			<p style="float:left;margin: 20px 0 10px 0;"><?php echo $address4.", ".$dist." away."; ?></p>
			<p class="goaxle" data-mec-id="<?php echo $mec_id;?>" style="float: right;width: 40px;height: 40px;background: #ffa800;-moz-border-radius: 50px;-webkit-border-radius: 50px;border-radius: 50px;margin-top: 15px;"><i class="fa fa-rocket" aria-hidden="true" style="font-size: 26px;margin-top: 8px;margin-left: 6px;"></i></p>
		</div>
		</div>
	</div>
</div>